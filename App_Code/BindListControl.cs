using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Bitscrape.AppBlock.Database;

/// <summary>
/// Summary description for BindListControl
/// </summary>
public class BindListControl
{
	public BindListControl()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    /// <summary>
    /// Bind division in on dropdownlist
    /// </summary>
    /// <param name="_ddlDivision"></param>
    public void BindDivision(DropDownList _ddlDivision)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DataSet _Ds = conn.GetDataSet("select * from Lum_Erp_Division order by divisionName", "Division");
            _ddlDivision.ClearSelection();
            _ddlDivision.Items.Clear();
            _ddlDivision.AppendDataBoundItems = true;
            _ddlDivision.Items.Add("Please Select");
            _ddlDivision.Items[0].Value = "0";
            _ddlDivision.DataTextField = _Ds.Tables[0].Columns["divisionName"].ToString();
            _ddlDivision.DataValueField = _Ds.Tables[0].Columns["divisionId"].ToString();
            _ddlDivision.DataSource = _Ds;
            _ddlDivision.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="_ddlSubdivison"></param>
    /// <param name="divisionId"></param>
    public void BindSubDivision(DropDownList _ddlSubdivison,string divisionId)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {

            DataSet _Ds = conn.GetDataSet(string.Format("select * from Lum_Erp_SubDivision where divisionId={0} order by subdivisionName", divisionId), "SubDivision");
            _ddlSubdivison.ClearSelection();
            _ddlSubdivison.Items.Clear();
            _ddlSubdivison.AppendDataBoundItems = true;
            _ddlSubdivison.Items.Add("Please Select");
            _ddlSubdivison.Items[0].Value = "0";
            _ddlSubdivison.DataTextField = _Ds.Tables[0].Columns["subdivisionName"].ToString();
            _ddlSubdivison.DataValueField = _Ds.Tables[0].Columns["subdivisionId"].ToString();
            _ddlSubdivison.DataSource = _Ds;
            _ddlSubdivison.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="_ddlOUM"></param>
    public void BindpOUM(DropDownList _ddlOUM)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DataSet _Ds = conn.GetDataSet("select * from Lum_Erp_UOM order by uomName", "UOM");
            _ddlOUM.ClearSelection();
            _ddlOUM.Items.Clear();
            _ddlOUM.AppendDataBoundItems = true;
            _ddlOUM.Items.Add("Please Select");
            _ddlOUM.Items[0].Value = "0";
            _ddlOUM.DataTextField = _Ds.Tables[0].Columns["uomName"].ToString();
            _ddlOUM.DataValueField = _Ds.Tables[0].Columns["uomId"].ToString();
            _ddlOUM.DataSource = _Ds;
            _ddlOUM.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="_ddlStores"></param>
    public void BindStores(DropDownList _ddlStores)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DataSet _Ds = conn.GetDataSet("select * from Lum_Erp_Storelocation order by storCode", "Store");
            _ddlStores.ClearSelection();
            _ddlStores.Items.Clear();
            _ddlStores.AppendDataBoundItems = true;
            _ddlStores.Items.Add("Please Select");
            _ddlStores.Items[0].Value = "0";
            _ddlStores.DataTextField = _Ds.Tables[0].Columns["storCode"].ToString();
            _ddlStores.DataValueField = _Ds.Tables[0].Columns["storeId"].ToString();
            _ddlStores.DataSource = _Ds;
            _ddlStores.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="_ddlBillingAddress"></param>
    /// <param name="customerId"></param>
    public void BindBillingAddress(DropDownList _ddlBillingAddress,int customerId)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            SpParam _spCostomerId = new SpParam("@intCustomerId", customerId, SqlDbType.Int);
            DataSet _Ds = conn.GetDataSet(string.Format("SELECT billingAddressId,address FROM Lum_Erp_BillingAddress WHERE customerId={0}", customerId), "BillingAddress");
            _ddlBillingAddress.ClearSelection();
            _ddlBillingAddress.Items.Clear();
            _ddlBillingAddress.AppendDataBoundItems = true;
            _ddlBillingAddress.Items.Add("Please Select");
            _ddlBillingAddress.Items[0].Value = "0";
            _ddlBillingAddress.DataTextField = _Ds.Tables[0].Columns["address"].ToString();
            _ddlBillingAddress.DataValueField = _Ds.Tables[0].Columns["billingAddressId"].ToString();
            _ddlBillingAddress.DataSource = _Ds;
            _ddlBillingAddress.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="_ddlShippingAddress"></param>
    /// <param name="customerId"></param>
    public void BindShippingAddress(DropDownList _ddlShippingAddress, int customerId)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DataSet _Ds = conn.GetDataSet(string.Format("SELECT shippingAddressId,address FROM Lum_Erp_ShippingAddress WHERE customerId={0}", customerId), "ShippingAddress");
            _ddlShippingAddress.ClearSelection();
            _ddlShippingAddress.Items.Clear();
            _ddlShippingAddress.AppendDataBoundItems = true;
            _ddlShippingAddress.Items.Add("Please Select");
            _ddlShippingAddress.Items[0].Value = "0";
            _ddlShippingAddress.DataTextField = _Ds.Tables[0].Columns["address"].ToString();
            _ddlShippingAddress.DataValueField = _Ds.Tables[0].Columns["shippingAddressId"].ToString();
            _ddlShippingAddress.DataSource = _Ds;
            _ddlShippingAddress.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="_ddlOrderType"></param>
    public void BindOrderType(DropDownList _ddlOrderType)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DataSet _Ds = conn.GetDataSet("SELECT poTypeId, poTypeCode FROM Lum_Erp_PurchaseOrderType order by poTypeCode", "OrderType");
            _ddlOrderType.ClearSelection();
            _ddlOrderType.Items.Clear();
            _ddlOrderType.AppendDataBoundItems = true;
            _ddlOrderType.Items.Add("Please Select");
            _ddlOrderType.Items[0].Value = "0";
            _ddlOrderType.DataTextField = _Ds.Tables[0].Columns["poTypeCode"].ToString();
            _ddlOrderType.DataValueField = _Ds.Tables[0].Columns["poTypeId"].ToString();
            _ddlOrderType.DataSource = _Ds;
            _ddlOrderType.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="_ddlServiceContract"></param>
    public void BindServiceContract(DropDownList _ddlServiceContract)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DataSet _Ds = conn.GetDataSet(string.Format("SELECT serviceContractId,serviceContractType FROM Lum_Erp_ServiceContract order by serviceContractType"), "serviceAgreement");
            _ddlServiceContract.ClearSelection();
            _ddlServiceContract.Items.Clear();
            _ddlServiceContract.AppendDataBoundItems = true;
            _ddlServiceContract.Items.Add("Please Select");
            _ddlServiceContract.Items[0].Value = "0";
            _ddlServiceContract.DataTextField = _Ds.Tables[0].Columns["serviceContractType"].ToString();
            _ddlServiceContract.DataValueField = _Ds.Tables[0].Columns["serviceContractId"].ToString();
            _ddlServiceContract.DataSource = _Ds;
            _ddlServiceContract.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="_ddlItemCode"></param>
    /// <param name="_subdivisionid"></param>
    public void BindItems(DropDownList _ddlItemCode,string _subdivisionid)
    {       
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DataSet _Ds = conn.GetDataSet(string.Format("select itemId, itemCode from Lum_Erp_Item where subDivisionId={0} order by itemCode", Convert.ToInt32(_subdivisionid)), "Item");
            _ddlItemCode.ClearSelection();
            _ddlItemCode.Items.Clear();
            _ddlItemCode.AppendDataBoundItems = true;
            _ddlItemCode.Items.Add("Please Select");
            _ddlItemCode.Items[0].Value = "0";
            _ddlItemCode.DataTextField = _Ds.Tables[0].Columns["itemCode"].ToString();
            _ddlItemCode.DataValueField = _Ds.Tables[0].Columns["itemId"].ToString();
            _ddlItemCode.DataSource = _Ds;
            _ddlItemCode.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="_dllVendor"></param>
    
    public void BindVendorAccount(DropDownList _dllVendor)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DataSet _Ds = conn.GetDataSet("SELECT vendorId, vendorName FROM Lum_Erp_Vendor WHERE acountType=0 order by vendorName", "Vendor");
            _dllVendor.ClearSelection();
            _dllVendor.Items.Clear();
            _dllVendor.AppendDataBoundItems = true;
            _dllVendor.Items.Add("Please Select");
            _dllVendor.Items[0].Value = "0";
            _dllVendor.DataTextField = _Ds.Tables[0].Columns["vendorName"].ToString();
            _dllVendor.DataValueField = _Ds.Tables[0].Columns["vendorId"].ToString();
            _dllVendor.DataSource = _Ds;
            _dllVendor.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="_dllVendor"></param>
    public void BindVendor(DropDownList _dllVendor)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DataSet _Ds = conn.GetDataSet("SELECT vendorId, vendorName FROM Lum_Erp_Vendor order by vendorName", "Vendor");
            _dllVendor.ClearSelection();
            _dllVendor.Items.Clear();
            _dllVendor.AppendDataBoundItems = true;
            _dllVendor.Items.Add("Please Select");
            _dllVendor.Items[0].Value = "0";
            _dllVendor.DataTextField = _Ds.Tables[0].Columns["vendorName"].ToString();
            _dllVendor.DataValueField = _Ds.Tables[0].Columns["vendorId"].ToString();
            _dllVendor.DataSource = _Ds;
            _dllVendor.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="_dllWarranty"></param>
    public void BindWarranty(DropDownList _dllWarranty)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DataSet _Ds = conn.GetDataSet(string.Format("SELECT warrantyId,warrantyPeriod FROM Lum_Erp_Warranty order by warrantyPeriod"), "Warranty");
            _dllWarranty.ClearSelection();
            _dllWarranty.Items.Clear();
            _dllWarranty.AppendDataBoundItems = true;
            _dllWarranty.Items.Add("Please Select");
            _dllWarranty.Items[0].Value = "0";
            _dllWarranty.DataTextField = _Ds.Tables[0].Columns["warrantyPeriod"].ToString();
            _dllWarranty.DataValueField = _Ds.Tables[0].Columns["warrantyId"].ToString();
            _dllWarranty.DataSource = _Ds;
            _dllWarranty.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="_dllCustomerSegment"></param>
    public void BindCustomerSegment(DropDownList _dllCustomerSegment)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DataSet _Ds = conn.GetDataSet("SELECT segmentId, segmentType FROM Lum_Erp_CustomerSegment order by segmentType", "CustomerSegment");
            _dllCustomerSegment.ClearSelection();
            _dllCustomerSegment.Items.Clear();
            _dllCustomerSegment.AppendDataBoundItems = true;
            _dllCustomerSegment.Items.Add("Please Select");
            _dllCustomerSegment.Items[0].Value = "0";
            _dllCustomerSegment.DataTextField = _Ds.Tables[0].Columns["segmentType"].ToString();
            _dllCustomerSegment.DataValueField = _Ds.Tables[0].Columns["segmentId"].ToString();
            _dllCustomerSegment.DataSource = _Ds;
            _dllCustomerSegment.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="_dllSalesPerson"></param>
    public void BindSalesPerson(DropDownList _dllSalesPerson)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DataSet _Ds = conn.GetDataSet("select salesPersonId,(initial+firstName+' '+lastName) as [Name] from Lum_Erp_SalesPerson order by firstName", "salesperson");
            _dllSalesPerson.ClearSelection();
            _dllSalesPerson.Items.Clear();
            _dllSalesPerson.AppendDataBoundItems = true;
            _dllSalesPerson.Items.Add("Please Select");
            _dllSalesPerson.Items[0].Value = "0";
            _dllSalesPerson.DataTextField = _Ds.Tables[0].Columns["Name"].ToString();
            _dllSalesPerson.DataValueField = _Ds.Tables[0].Columns["salesPersonId"].ToString();
            _dllSalesPerson.DataSource = _Ds;
            _dllSalesPerson.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="_dllCustomer"></param>
    public void BindCustomerAccount(DropDownList _dllCustomer)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DataSet _Ds = conn.GetDataSet("SELECT customerId, customerName FROM Lum_Erp_Customer WHERE acountType=0 and isdeleted = 0 order by customerName", "Customer");
            _dllCustomer.ClearSelection();
            _dllCustomer.Items.Clear();
            _dllCustomer.AppendDataBoundItems = true;
            _dllCustomer.Items.Add("Please Select");
            _dllCustomer.Items[0].Value = "0";
            _dllCustomer.DataTextField = _Ds.Tables[0].Columns["customerName"].ToString();
            _dllCustomer.DataValueField = _Ds.Tables[0].Columns["customerId"].ToString();
            _dllCustomer.DataSource = _Ds;
            _dllCustomer.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="_dllCustomer"></param>
    public void BindCustomer(DropDownList _dllCustomer)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DataSet _Ds = conn.GetDataSet("SELECT customerId, customerName FROM Lum_Erp_Customer where isdeleted = 0 order by customerName", "Customer");
            _dllCustomer.ClearSelection();
            _dllCustomer.Items.Clear();
            _dllCustomer.AppendDataBoundItems = true;
            _dllCustomer.Items.Add("Please Select");
            _dllCustomer.Items[0].Value = "0";
            _dllCustomer.DataTextField = _Ds.Tables[0].Columns["customerName"].ToString();
            _dllCustomer.DataValueField = _Ds.Tables[0].Columns["customerId"].ToString();
            _dllCustomer.DataSource = _Ds;
            _dllCustomer.DataBind();
        }
    }
    public void BindCustomerAll(DropDownList _dllCustomer)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DataSet _Ds = conn.GetDataSet("SELECT customerId, customerName FROM Lum_Erp_Customer where len(customerName) > 0 order by customerName", "Customer");
            _dllCustomer.ClearSelection();
            _dllCustomer.Items.Clear();
            _dllCustomer.AppendDataBoundItems = true;
            _dllCustomer.Items.Add("Please Select");
            _dllCustomer.Items[0].Value = "0";
            _dllCustomer.DataTextField = _Ds.Tables[0].Columns["customerName"].ToString();
            _dllCustomer.DataValueField = _Ds.Tables[0].Columns["customerId"].ToString();
            _dllCustomer.DataSource = _Ds;
            _dllCustomer.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="_dllTransport"></param>
    

    public void BindTransport(DropDownList _dllTransport)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DataSet _Ds = conn.GetDataSet("select transporterId,transporterName from Lum_Erp_Transporter order by transporterName", "SOIRTransporter");
            _dllTransport.ClearSelection();
            _dllTransport.Items.Clear();
            _dllTransport.AppendDataBoundItems = true;
            _dllTransport.Items.Add("Please Select");
            _dllTransport.Items[0].Value = "0";
            _dllTransport.DataTextField = _Ds.Tables[0].Columns["transporterName"].ToString();
            _dllTransport.DataValueField = _Ds.Tables[0].Columns["transporterId"].ToString();
            _dllTransport.DataSource = _Ds;
            _dllTransport.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="_dllInvoiceType"></param>
    public void BindInvoiceType(DropDownList _dllInvoiceType)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DataSet _Ds = conn.GetDataSet("stp_selectinvoicetype", "InvoiceType");
            _dllInvoiceType.ClearSelection();
            _dllInvoiceType.Items.Clear();
            _dllInvoiceType.AppendDataBoundItems = true;
            _dllInvoiceType.Items.Add("Please Select");
            _dllInvoiceType.Items[0].Value = "0";
            _dllInvoiceType.DataTextField = _Ds.Tables[0].Columns["strInvoiceType"].ToString();
            _dllInvoiceType.DataValueField = _Ds.Tables[0].Columns["intInvoiceTypeId"].ToString();
            _dllInvoiceType.DataSource = _Ds;
            _dllInvoiceType.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="_dllPaymentTerms"></param>
    public void BindPaymentTerms(DropDownList _dllPaymentTerms)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DataSet _Ds = conn.GetDataSet("stp_SelectPaymentTerms", "PaymentTerm");
            _dllPaymentTerms.ClearSelection();
            _dllPaymentTerms.Items.Clear();
            _dllPaymentTerms.AppendDataBoundItems = true;
            _dllPaymentTerms.Items.Add("Please Select");
            _dllPaymentTerms.Items[0].Value = "0";
            _dllPaymentTerms.DataTextField = _Ds.Tables[0].Columns["strPaymentTerms"].ToString();
            _dllPaymentTerms.DataValueField = _Ds.Tables[0].Columns["intPaymentTermsId"].ToString();
            _dllPaymentTerms.DataSource = _Ds;
            _dllPaymentTerms.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="_dllDeductionType"></param>
    public void BindDeductionType(DropDownList _dllDeductionType)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DataSet _Ds = conn.GetDataSet("stp_SelectDeductionType", "AdmissibleDeduction ");
            _dllDeductionType.ClearSelection();
            _dllDeductionType.Items.Clear();
            _dllDeductionType.AppendDataBoundItems = true;
            _dllDeductionType.Items.Add("Please Select");
            _dllDeductionType.Items[0].Value = "0";
            _dllDeductionType.DataTextField = _Ds.Tables[0].Columns["strDeduction"].ToString();
            _dllDeductionType.DataValueField = _Ds.Tables[0].Columns["intDeductionTypeId"].ToString();
            _dllDeductionType.DataSource = _Ds;
            _dllDeductionType.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="_dllItemSerialNo"></param>
    /// <param name="itemCode"></param>
    public void BindItemSerialNo(DropDownList _dllItemSerialNo,string itemCode)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            SpParam _spItemCode = new SpParam("@intItemCode", Convert.ToInt32(itemCode), SqlDbType.Int);
            DataSet _Ds = conn.GetDataSet("stp_SelectItemSerialNo", _spItemCode);
            _dllItemSerialNo.ClearSelection();
            _dllItemSerialNo.Items.Clear();
            _dllItemSerialNo.AppendDataBoundItems = true;
            _dllItemSerialNo.Items.Add("Please Select");
            _dllItemSerialNo.Items[0].Value = "0";
            _dllItemSerialNo.DataTextField = _Ds.Tables[0].Columns["strSerialNo"].ToString();
            _dllItemSerialNo.DataValueField = _Ds.Tables[0].Columns["strSerialNo"].ToString();
            _dllItemSerialNo.DataSource = _Ds;
            _dllItemSerialNo.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="_ddlServiceTax"></param>
    public void BindServiceTax(DropDownList _ddlServiceTax)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DataSet _Ds = conn.GetDataSet("SELECT serviceTaxId, serviceTax FROM Lum_Erp_ServiceTaxMaster order by serviceTax", "ServiceTaxMaster");
            _ddlServiceTax.ClearSelection();
            _ddlServiceTax.Items.Clear();
            _ddlServiceTax.AppendDataBoundItems = true;
            _ddlServiceTax.Items.Add("Please Select");
            _ddlServiceTax.Items[0].Value = "0";
            _ddlServiceTax.DataTextField = _Ds.Tables[0].Columns["serviceTax"].ToString();
            _ddlServiceTax.DataValueField = _Ds.Tables[0].Columns["serviceTaxId"].ToString();
            _ddlServiceTax.DataSource = _Ds;
            _ddlServiceTax.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="_dllRateContractType"></param>
    public void BindRateContractType(DropDownList _dllRateContractType)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DataSet _Ds = conn.GetDataSet("SELECT rateContractTypeId, contractType FROM Lum_Erp_RateContractType order by contractType", "RateContractType");
            _dllRateContractType.ClearSelection();
            _dllRateContractType.Items.Clear();
            _dllRateContractType.AppendDataBoundItems = true;
            _dllRateContractType.Items.Add("Please Select");
            _dllRateContractType.Items[0].Value = "0";
            _dllRateContractType.DataTextField = _Ds.Tables[0].Columns["contractType"].ToString();
            _dllRateContractType.DataValueField = _Ds.Tables[0].Columns["rateContractTypeId"].ToString();
            _dllRateContractType.DataSource = _Ds;
            _dllRateContractType.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="_dllPorder"></param>

    public void BindPOrder(DropDownList _dllPorder)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DataSet _Ds = conn.GetDataSet("SELECT poId, strPoId FROM Lum_Erp_PurchaseOrder WHERE createdByUser=1 and  grnFulfilled='False' And isDeleted =0 order by strPoId", "PoOrder");
            _dllPorder.ClearSelection();
            _dllPorder.Items.Clear();
            _dllPorder.AppendDataBoundItems = true;
            _dllPorder.Items.Add("Please Select");
            _dllPorder.Items[0].Value = "0";
            _dllPorder.DataTextField = _Ds.Tables[0].Columns["strPoId"].ToString();
            _dllPorder.DataValueField = _Ds.Tables[0].Columns["poId"].ToString();
            _dllPorder.DataSource = _Ds;
            _dllPorder.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="_ddlSoir"></param>
    public void BindSoir(DropDownList _ddlSoir)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DataSet _Ds = conn.GetDataSet("select soirId,strSoirId from Lum_Erp_SOIR where isDeleted=0 and isLocked=0 order by strSoirId", "SOIR");
            _ddlSoir.ClearSelection();
            _ddlSoir.Items.Clear();
            _ddlSoir.AppendDataBoundItems = true;
            _ddlSoir.Items.Add("Please Select");
            _ddlSoir.Items[0].Value = "0";
            _ddlSoir.DataTextField = _Ds.Tables[0].Columns["strSoirId"].ToString();
            _ddlSoir.DataValueField = _Ds.Tables[0].Columns["soirId"].ToString();
            _ddlSoir.DataSource = _Ds;
            _ddlSoir.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="_ddlEducationCess"></param>

    public void BindEducationCess(DropDownList _ddlEducationCess)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DataSet _Ds = conn.GetDataSet("SELECT educationCessId, eduCess FROM Lum_Erp_EducationCess order by eduCess", "EducationCess");
            _ddlEducationCess.ClearSelection();
            _ddlEducationCess.Items.Clear();
            _ddlEducationCess.AppendDataBoundItems = true;
            _ddlEducationCess.Items.Add("Please Select");
            _ddlEducationCess.Items[0].Value = "0";
            _ddlEducationCess.DataTextField = _Ds.Tables[0].Columns["eduCess"].ToString();
            _ddlEducationCess.DataValueField = _Ds.Tables[0].Columns["educationCessId"].ToString();
            _ddlEducationCess.DataSource = _Ds;
            _ddlEducationCess.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="_ddlhEducationCess"></param>
    public void BindHEducationCess(DropDownList _ddlhEducationCess)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DataSet _Ds = conn.GetDataSet("SELECT higherEducationCessId, educationCess FROM Lum_Erp_HigherEducationCess order by educationCess ", "HigherEducationCess");
            _ddlhEducationCess.ClearSelection();
            _ddlhEducationCess.Items.Clear();
            _ddlhEducationCess.AppendDataBoundItems = true;
            _ddlhEducationCess.Items.Add("Please Select");
            _ddlhEducationCess.Items[0].Value = "0";
            _ddlhEducationCess.DataTextField = _Ds.Tables[0].Columns["educationCess"].ToString();
            _ddlhEducationCess.DataValueField = _Ds.Tables[0].Columns["higherEducationCessId"].ToString();
            _ddlhEducationCess.DataSource = _Ds;
            _ddlhEducationCess.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="_ddlServiceCenter"></param>
    public void BindServiceCenter(DropDownList _ddlServiceCenter)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DataSet _Ds = conn.GetDataSet("SELECT serviceCenterId, centerName FROM Lum_Erp_ServiceCenter order by centerName", "ServiceCenter");
            _ddlServiceCenter.ClearSelection();
            _ddlServiceCenter.Items.Clear();
            _ddlServiceCenter.AppendDataBoundItems = true;
            _ddlServiceCenter.Items.Add("Please Select");
            _ddlServiceCenter.Items[0].Value = "0";
            _ddlServiceCenter.DataTextField = _Ds.Tables[0].Columns["centerName"].ToString();
            _ddlServiceCenter.DataValueField = _Ds.Tables[0].Columns["serviceCenterId"].ToString();
            _ddlServiceCenter.DataSource = _Ds;
            _ddlServiceCenter.DataBind();
        }
    }
    public void BindFormRecoverable(DropDownList _dllFormRecoverable)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DataSet _Ds = conn.GetDataSet("select FormRecoverableId,RecoverableFormName from Lum_Erp_RecoverableForm order by RecoverableFormName", "FormRecoverable");
            _dllFormRecoverable.ClearSelection();
            _dllFormRecoverable.Items.Clear();
            _dllFormRecoverable.AppendDataBoundItems = true;
            _dllFormRecoverable.Items.Add("Please Select");
            _dllFormRecoverable.Items[0].Value = "0";
            _dllFormRecoverable.DataTextField = _Ds.Tables[0].Columns["RecoverableFormName"].ToString();
            _dllFormRecoverable.DataValueField = _Ds.Tables[0].Columns["FormRecoverableId"].ToString();
            _dllFormRecoverable.DataSource = _Ds;
            _dllFormRecoverable.DataBind();
        }
    }
}
