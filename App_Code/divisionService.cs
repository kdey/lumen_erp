﻿using System;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.Services;
using System.Web.Services.Protocols;
using AjaxControlToolkit;
using System.Data;
using System.Data.SqlClient;
using Bitscrape.AppBlock.Database;

/// <summary>
/// Summary description for divisionService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService()]

// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class divisionService : System.Web.Services.WebService {

    public divisionService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld() {
        return "Hello World";
    }

    [WebMethod]
    public CascadingDropDownNameValue[] GetDivision(
      string knownCategoryValues,
      string category)
    {
        //int divisionId;


        //if (knownCategoryValues.Contains("undefined"))
        //{
        //    knownCategoryValues = knownCategoryValues.Replace("undefined", "division");
        //}

        //StringDictionary kv = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues);

        //if (!kv.ContainsKey("division") ||
        //    !Int32.TryParse(kv["division"], out divisionId))
        //{
        //    return null;
        //}
        List<CascadingDropDownNameValue> values = new List<CascadingDropDownNameValue>();

        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select DivisionId,DivisionName from Lum_Erp_Division  Order By DivisionName "))
            {
                while (reader.Read())
                {

                    string DivisionName = reader["DivisionName"].ToString();

                    string DivisionId = reader["DivisionId"].ToString();

                    values.Add(new CascadingDropDownNameValue(DivisionName, DivisionId));

                }
            }
        }
        return values.ToArray();
    }

    [WebMethod]
    public CascadingDropDownNameValue[] GetStore(
      string knownCategoryValues,
      string category)
    {
        //int divisionId;


        //if (knownCategoryValues.Contains("undefined"))
        //{
        //    knownCategoryValues = knownCategoryValues.Replace("undefined", "division");
        //}

        //StringDictionary kv = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues);

        //if (!kv.ContainsKey("division") ||
        //    !Int32.TryParse(kv["division"], out divisionId))
        //{
        //    return null;
        //}
        List<CascadingDropDownNameValue> values = new List<CascadingDropDownNameValue>();

        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select DivisionId,DivisionName from Lum_Erp_Division  Order By DivisionName "))
            {
                while (reader.Read())
                {

                    string DivisionName = reader["DivisionName"].ToString();

                    string DivisionId = reader["DivisionId"].ToString();

                    values.Add(new CascadingDropDownNameValue(DivisionName, DivisionId));

                }
            }
        }
        return values.ToArray();
    }
    [WebMethod]
    public CascadingDropDownNameValue[] GetSubdividsionForDivision(
      string knownCategoryValues,
      string category)
    {
        int divisionId;


        if (knownCategoryValues.Contains("undefined"))
        {
            knownCategoryValues = knownCategoryValues.Replace("undefined", "division");
        }

        StringDictionary kv = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues);

        if (!kv.ContainsKey("division") ||
            !Int32.TryParse(kv["division"], out divisionId))
        {
            return null;
        }
        List<CascadingDropDownNameValue> values = new List<CascadingDropDownNameValue>();

        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select subDivisionId, subDivisionName from Lum_Erp_SubDivision where divisionId = " + divisionId + " Order By subDivisionName "))
            {
                while (reader.Read())
                {

                    string subDivisionName = reader["subDivisionName"].ToString();

                    string subDivisionId = reader["subDivisionId"].ToString();

                    values.Add(new CascadingDropDownNameValue(subDivisionName, subDivisionId));

                }
            }
        }
        return values.ToArray();
    }


    [WebMethod]
    public CascadingDropDownNameValue[] GetItemForSubdividsion(
      string knownCategoryValues,
      string category)
    {
        int SubdividsionId;


        if (knownCategoryValues.Contains("undefined"))
        {
            knownCategoryValues = knownCategoryValues.Replace("undefined", "division");
        }

        StringDictionary kv = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues);

        if (!kv.ContainsKey("subDivision") ||
            !Int32.TryParse(kv["subDivision"], out SubdividsionId))
        {
            return null;
        }
        List<CascadingDropDownNameValue> values = new List<CascadingDropDownNameValue>();

        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select itemId, itemCode from Lum_Erp_Item where subDivisionId = " + SubdividsionId + " Order By itemCode"))
            {
                while (reader.Read())
                {

                    string itemCode = reader["itemCode"].ToString();

                    string itemId = reader["itemId"].ToString();

                    values.Add(new CascadingDropDownNameValue(itemCode, itemId));

                }
            }
        }
        return values.ToArray();
    }

   
}

