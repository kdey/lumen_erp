﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for Serialno
/// </summary>
public class Serialno
{
    protected string m_itemId;
    protected string m_srno;

    public string itemId
    {
        get
        {
            return m_itemId;
        }
        set
        {
            m_itemId = value;
        }
    }
    public string srno
    {
        get
        {
            return m_srno;
        }
        set
        {
            m_srno = value;
        }
    }

	public Serialno()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}
