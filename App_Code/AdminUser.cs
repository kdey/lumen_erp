﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

/// <summary>
/// Summary description for AdminUser
/// </summary>
public class AdminUser
{
    public const int __NORMALUSER = 0;
    public const int __ADMINUSER = 1;

    private int _userId;
    private int _userType;

    public int UserType
    {
        get { return _userType; }
    }

    public int UserId
    {
        get { return _userId; }
    }

	public AdminUser(int userId, int userType)
	{
        _userId = userId;
        _userType = userType;
	}
}
