﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using Bitscrape.AppBlock.Database;

/// <summary>
/// Summary description for BasePage
/// </summary>
public class BasePage : Page
{
    private const string __USERSESSIONKEY = "adminUser";
    private const string __LOGINCOOKIENAME = "loginCookie";


    /// <summary>
    /// 
    /// </summary>
    /// <param name="userName"></param>
    /// <param name="password"></param>
    /// <returns></returns>
    protected bool Login(string userName, string password, bool remember)
    {
        bool retValue = false;

        string hashedPassword = HashPassword(password);
        // see if a user exists with this password and userName
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader =
                conn.ExecuteQuery(
                    string.Format(@"SELECT adminuserId, userTypeId ,passwordHash
	                                    FROM Lum_Erp_AdminUser 
	                                    WHERE userName = '{0}' and 
	                                    passwordHash = (SELECT EncryptedPwd FROM dbo.FNEncryptPwd('{1}')) and isActive='1'",
                    DataUtility.CleanString(userName), DataUtility.CleanString(hashedPassword))))
            {
                if (reader.Read())
                {
                    AdminUser user = new AdminUser(reader.GetInt32(0), reader.GetInt32(1));
                    // put this in the session
                    Session[__USERSESSIONKEY] = user;
                    retValue = true;
                    reader.Close();

                    // also update the users statistics.
                    conn.ExecuteCommand("update Lum_Erp_AdminUser set lastLoginDate = DATEADD(MI,330,GETUTCDATE() ) where adminuserId = " + user.UserId);
                    conn.ExecuteCommand(@"IF NOT EXISTS (SELECT CurrentSysdate FROM  Lum_Erp_Sysdate WHERE CurrentSysdate = CONVERT(VARCHAR,DATEADD(MI,330,GETUTCDATE() ),106))
                                                INSERT Lum_Erp_Sysdate (LumenDate,CurrentSysdate)
				                                            SELECT TOP 1 CONVERT(VARCHAR,(DATEADD(D,1,lumendate)),106),CONVERT(VARCHAR,DATEADD(MI,330,GETUTCDATE() ),106)
					                                            FROM Lum_Erp_Sysdate ORDER BY sysdateid DESC");


                    // if the user has to be remembered for next visit then update cookie etc.
                    if (remember)
                    {
                        string cookieValue = Guid.NewGuid().ToString().Replace("-", string.Empty);
                        HttpCookie loginCookie = new HttpCookie(__LOGINCOOKIENAME, cookieValue);
                        loginCookie.Expires = DateTime.Today.AddDays(15);
                        Response.Cookies.Add(loginCookie);
                        conn.ExecuteCommand(
                            string.Format(
                                    @"update Lum_Erp_AdminUser 
                                    set 
                                        isLoggedIn = 1,
                                        cookie = '{0}' 
                                  where adminuserId = {1}", cookieValue, user.UserId));
                    }
                    else
                    {
                        conn.ExecuteCommand("update Lum_Erp_AdminUser set isLoggedIn = 0 where adminuserId = " + user.UserId);
                    }
                }
                else
                {
                    // error message about wrong username or password.
                }
            }
        }


        return retValue;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="password"></param>
    /// <returns></returns>
    private string HashPassword(string password)
    {
        return password;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    protected bool IsLoggedIn()
    {
        bool retValue = false;
        // first check whether there is a user object in the session. if so then we are logged in
        if (Session[__USERSESSIONKEY] != null)
        {
            retValue = true;
        }
        else
        {
            // next check if the user is automatically logged in.
            HttpCookie loginCookie = Request.Cookies[__LOGINCOOKIENAME];
            if (loginCookie != null)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    string cookie = loginCookie.Value;
                    using (IDataReader reader =
                        conn.ExecuteQuery(string.Format("select adminUserId, userTypeId from Lum_Erp_AdminUser where isLoggedIn = 1 and cookie = '{0}'", cookie)))
                    {
                        if (reader.Read())
                        {
                            AdminUser user = new AdminUser(reader.GetInt32(0), reader.GetInt32(1));
                            // put this in the session
                            Session[__USERSESSIONKEY] = user;
                            retValue = true;
                        }
                    }
                }
            }
        }
        return retValue;
    }

    protected AdminUser GetLoggedinUser()
    {
        AdminUser retValue = null;

        if (Session[__USERSESSIONKEY] != null)
        {
            retValue = (AdminUser) Session[__USERSESSIONKEY];
        }

        return retValue;
    }

    public String changeNumericToWords(double numb)
    {

        String num = numb.ToString();

        return changeToWords(num, false);

    }

    public String changeCurrencyToWords(String numb)
    {

        return changeToWords(numb, true);

    }

    public String changeNumericToWords(String numb)
    {

        return changeToWords(numb, false);

    }

    public String changeCurrencyToWords(double numb)
    {

        return changeToWords(numb.ToString(), true);

    }

    private String changeToWords(String numb, bool isCurrency)
    {

        String val = "", wholeNo = numb, points = "", andStr = "", pointStr = "";

        String endStr = (isCurrency) ? ("Only") : ("");

        try
        {

            int decimalPlace = numb.IndexOf(".");

            if (decimalPlace > 0)
            {

                wholeNo = numb.Substring(0, decimalPlace);

                points = numb.Substring(decimalPlace + 1);

                if (Convert.ToInt32(points) > 0)
                {

                    andStr = (isCurrency) ? ("") : ("point");// just to separate whole numbers from points/cents

                    endStr = (isCurrency) ? ("Paisa " + endStr) : ("");
                    if (points.Length > 1)
                    {
                        pointStr = " and " + translateWholeNumber(points);
                    }
                    else
                    {
                        pointStr = " and " + translateWholeNumber(points + "0");
                    }

                }


            }
            else
            {
                andStr = (isCurrency) ? ("") : ("point");
            }

            val = String.Format("{0} {1}{2} {3}", "Rupees " + translateWholeNumber(wholeNo).Trim(), andStr, pointStr, endStr);

        }

        catch { ;}

        return val;

    }
    private String translateWholeNumber(String number)
    {

        string word = "";

        try
        {

            bool beginsZero = false;//tests for 0XX

            bool isDone = false;//test if already translated

            double dblAmt = (Convert.ToDouble(number));

            //if ((dblAmt > 0) && number.StartsWith("0"))

            if (dblAmt > 0)
            {//test for zero or digit zero in a nuemric

                beginsZero = number.StartsWith("0");

                int numDigits = number.Length;

                int pos = 0;//store digit grouping

                String place = "";//digit grouping name:hundres,thousand,etc...

                switch (numDigits)
                {

                    case 1://ones' range

                        word = ones(number);

                        isDone = true;

                        break;

                    case 2://tens' range

                        word = tens(number);

                        isDone = true;

                        break;

                    case 3://hundreds' range

                        pos = (numDigits % 3) + 1;
                        if (beginsZero)
                        {
                            break;
                        }
                        place = " Hundred ";

                        break;

                    case 4://thousands' range

                        pos = (numDigits % 4) + 1;
                        if (beginsZero)
                        {
                            break;
                        }
                        place = " Thousand ";

                        break;

                    case 5:

                        pos = (numDigits % 4) + 1;
                        //if (beginsZero)
                        //{
                        //    break;
                        //}
                        place = " Thousand ";

                        break;

                    case 6:

                        pos = (numDigits % 6) + 1;
                        //if (beginsZero)
                        //{
                        //    break;
                        //}
                        place = " Lakh ";

                        break;

                    case 7://millions' range

                        pos = (numDigits % 6) + 1;
                        //if (beginsZero)
                        //{
                        //    break;
                        //}
                        place = " Lakh ";

                        break;
                    case 8:

                        pos = (numDigits % 8) + 1;
                        //if (beginsZero)
                        //{
                        //    break;
                        //}
                        place = " Crore ";

                        break;

                    case 9:

                        pos = (numDigits % 8) + 1;
                        //if (beginsZero)
                        //{
                        //    break;
                        //}
                        place = " Crore ";

                        break;


                    //case 10://Billions's range



                    //add extra case options for anything above Billion...

                    default:

                        isDone = true;

                        break;

                }

                if (!isDone)
                {//if transalation is not done, continue...(Recursion comes in now!!)

                    word = translateWholeNumber(number.Substring(0, pos)) + place + translateWholeNumber(number.Substring(pos));

                    //check for trailing zeros

                    if (beginsZero) word = " and " + word.Trim();

                }

                //ignore digit grouping names

                if (word.Trim().Equals(place.Trim())) word = "";

            }

        }

        catch { ;}

        return word.Trim();

    }

    private String tens(String digit)
    {

        int digt = Convert.ToInt32(digit);

        String name = null;

        switch (digt)
        {

            case 10:

                name = "Ten";

                break;

            case 11:

                name = "Eleven";

                break;

            case 12:

                name = "Twelve";

                break;

            case 13:

                name = "Thirteen";

                break;

            case 14:

                name = "Fourteen";

                break;

            case 15:

                name = "Fifteen";

                break;

            case 16:

                name = "Sixteen";

                break;

            case 17:

                name = "Seventeen";

                break;

            case 18:

                name = "Eighteen";

                break;

            case 19:

                name = "Nineteen";

                break;

            case 20:

                name = "Twenty";

                break;

            case 30:

                name = "Thirty";

                break;

            case 40:

                name = "Forty";

                break;

            case 50:

                name = "Fifty";

                break;

            case 60:

                name = "Sixty";

                break;

            case 70:

                name = "Seventy";

                break;

            case 80:

                name = "Eighty";

                break;

            case 90:

                name = "Ninety";

                break;

            default:

                if (digt > 0)
                {

                    name = tens(digit.Substring(0, 1) + "0") + " " + ones(digit.Substring(1));

                }

                break;

        }

        return name;

    }

    private String ones(String digit)
    {

        int digt = Convert.ToInt32(digit);

        String name = "";

        switch (digt)
        {

            case 1:

                name = "One";

                break;

            case 2:

                name = "Two";

                break;

            case 3:

                name = "Three";

                break;

            case 4:

                name = "Four";

                break;

            case 5:

                name = "Five";

                break;

            case 6:

                name = "Six";

                break;

            case 7:

                name = "Seven";

                break;

            case 8:

                name = "Eight";

                break;

            case 9:

                name = "Nine";

                break;

        }

        return name;

    }

    private String translateCents(String cents)
    {

        String cts = "", digit = "", engOne = "";

        for (int i = 0; i < cents.Length; i++)
        {

            digit = cents[i].ToString();

            if (digit.Equals("0"))
            {

                engOne = "Zero";

            }

            else
            {

                engOne = ones(digit);

            }

            cts += " " + engOne;

        }

        return cts;

    }
}
