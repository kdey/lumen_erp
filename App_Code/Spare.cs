﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for Spare
/// </summary>
public class Spare
{
    protected string m_itemId;
    protected string m_spareid;
    protected int m_inspare;
    protected int m_outspare;
    public string itemId
    {
        get
        {
            return m_itemId;
        }
        set
        {
            m_itemId = value;
        }
    }
    public string spareid
    {
        get
        {
            return m_spareid;
        }
        set
        {
            m_spareid = value;
        }
    }
    public int inspare
    {
        get
        {
            return m_inspare;
        }
        set
        {
            m_inspare = value;
        }
    }
    public int outspare
    {
        get
        {
            return m_outspare;
        }
        set
        {
            m_outspare = value;
        }
    }
	public Spare()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}
