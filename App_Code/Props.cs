﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for Props
/// </summary>
public class Props
{

    protected string m_installmentDate;
    protected string m_installmentamt;
    protected string m_id;

    public string id
    {
        get
        {
            return m_id;
        }
        set
        {
            m_id = value;
        }
    }
    public string installmentDate
    {
        get
        {
            return m_installmentDate;
        }
        set
        {
            m_installmentDate = value;
        }
    }

    public string installmentamt
    {
        get
        {
            return m_installmentamt;
        }
        set
        {
            m_installmentamt = value;
        }
    }

    
    public Props()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}


