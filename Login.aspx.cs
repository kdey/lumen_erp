﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using Bitscrape.AppBlock.Database;

public partial class Login : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void loginButton_Click(object sender, EventArgs e)
    {
        if (base.Login(userName.Text, password.Text, false))
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            Page.RegisterClientScriptBlock("message", "<script>alert('Login faild.');</script>");
        }

    }
}
