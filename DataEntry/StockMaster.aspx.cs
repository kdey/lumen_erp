﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;

public partial class DataEntry_StockMaster : BasePage
{
    protected int stockId = -1;
  
    protected void Page_Load(object sender, EventArgs e)
    {
        message.Text = "";
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!this.IsPostBack)
        {
            bindControl();
           BindGrid();
           //using (IConnection conn = DataConnectionFactory.GetConnection())
           //{
           //    using (IDataReader reader = conn.ExecuteQuery("select * from AdminUser where userName= 'demo'"))
           //    {
           //        while (reader.Read())
           //        {
           //            AdminUser s = GetLoggedinUser();
           //            int a = s.UserId;
           //            if (a == Convert.ToInt32(reader["adminuserId"]))

           //                add.Enabled = false;
           //        }
           //    }
           //}
        }
        else
        {
            // check if a division id has been passed
            try
            {
                stockId = int.Parse(Request["stockId"]);
                //using (IConnection conn = DataConnectionFactory.GetConnection())
                //{
                //    using (IDataReader reader = conn.ExecuteQuery("select * from AdminUser where userName= 'demo'"))
                //    {
                //        while (reader.Read())
                //        {
                //            AdminUser s = GetLoggedinUser();
                //            int a = s.UserId;
                //            if (a == Convert.ToInt32(reader["adminuserId"]))

                //                add.Enabled = false;
                //        }
                //    }
                //}

               
            }
            catch { };
        }
        if (GetLoggedinUser().UserType == 2)
        {
            add.Visible  = false;

        }
    }
    public void bindControl()
    {
        BindListControl _control = new BindListControl();
        _control.BindDivision(ddlDivision);
        _control.BindStores(ddlStore);
      
    }
    private void BindGrid()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
//            string strQuery = @"select ST.*, SL.storeName,I.itemCode from  Lum_Erp_Stock ST
//                                inner join Lum_Erp_Storelocation SL on SL.storeId=ST.storeId
//                                inner join Lum_Erp_Item I on I.itemId=ST.itemId order by SL.storeName";
            string strQuery = @"SELECT ST.itemid,ST.quantity, ST.STOCKID,SL.storeName,st.[update],I.itemCode ,l1.quantity AS closingQuantity
	FROM  Lum_Erp_Stock ST
         INNER JOIN Lum_Erp_Storelocation SL on SL.storeId=ST.storeId
         INNER JOIN Lum_Erp_Item I on I.itemId=ST.itemId 
                                
		LEFT JOIN                                  
			(
			SELECT itemId,sum(quantity) AS quantity,MAX(itemCode) AS itemCode,storeId,'' as serialNo
				FROM (
					SELECT GS.itemId ,ISNULL(serialNo,'') serialNo,D.divisionId,Sd.subdivisionId ,SL.storeId,I.itemCode,gs.quantity
								FROM Lum_Erp_GrndetailsSerialNumber GS, Lum_Erp_Stock ST ,Lum_Erp_Item I ,
									 Lum_Erp_Subdivision SD,Lum_Erp_Division D,
									 Lum_Erp_Storelocation SL
								WHERE GS.itemId = I.itemId AND 
								SD.subdivisionId = I.subdivisionId AND
								D.divisionId = SD.divisionId  AND
								SL.storeId =ST.storeId  AND ST.STOCKID=GS.OPSTOCKID  AND
								isLocked =0 AND isDispatch = 0 AND inComplete=0 

					UNION ALL

					SELECT GS.itemId ,ISNULL(serialNo,'') serialNo,D.divisionId,Sd.subdivisionId ,G.storeId,I.itemCode,gs.quantity
						FROM Lum_Erp_GrndetailsSerialNumber GS, Lum_Erp_Grn G ,Lum_Erp_Item I ,
							 Lum_Erp_Subdivision SD,Lum_Erp_Division D,
							 Lum_Erp_Storelocation SL
						WHERE GS.itemId = I.itemId AND 
						SD.subdivisionId = I.subdivisionId AND
						D.divisionId = SD.divisionId  AND
						SL.storeId =G.storeId  AND G.grnId=GS.grnId  AND
						isLocked =0 AND isDispatch = 0 AND inComplete=0 

					UNION ALL

					SELECT GS.itemId ,ISNULL(GS.serialNo,'') serialNo,D.divisionId,Sd.subdivisionId,G.storeId,I.itemCode,gs.quantity
						FROM Lum_Erp_GrndetailsSerialNumber GS, Lum_Erp_Grn G ,Lum_Erp_Item I ,
							 Lum_Erp_Subdivision SD,Lum_Erp_Division D,
							 Lum_Erp_Storelocation SL,Lum_Erp_GrndetailsSerialNumber GS1 
						WHERE GS.itemId = I.itemId AND 
						GS.From_Detach= GS1.grndetailsSerialNoId AND 
						SD.subdivisionId = I.subdivisionId AND
						D.divisionId = SD.divisionId  AND
						SL.storeId =G.storeId  
						AND G.grnId=GS1.grnId  AND
						GS.isLocked =0 AND GS.isDispatch = 0 AND GS.inComplete=0 )L1
				
				GROUP BY storeId,itemId) L1

			ON st.itemid = l1.itemid
ORDER BY STORENAME";
            using (IDataReader reader = conn.ExecuteQuery(strQuery))
            {
                stockGrid.DataSource = reader;
                stockGrid.DataBind();
            }
        }
    }

    protected void add_Click(object sender, EventArgs e)
    {
  
        //System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-GB");
        DateTime dtDate = Convert.ToDateTime(update.Text.Split(' ')[0]);
        int _quantity;
        int itemId;
        string serialNo;
        int _opStockID;
        _quantity = Convert.ToInt32(quantity.Text);
        itemId = Convert.ToInt32(ddlItem.SelectedValue);
        serialNo = txtserialNo.Text; 
        
        if (ddlDivision.Items[ddlDivision.SelectedIndex].Text.ToUpper() == "FINISHED GOODS")
        {
            string[] param = serialNo.Split(';');
            if (_quantity != param.Length)
            {
                errmsg.Text = "Quantity is not equal to total serialNo";
                errmsg.ForeColor = Color.Red;
                return;
            }
        }
        if (stockId == -1)
        {
            try
            {
                // we are in add mode

                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    object obj = conn.ExecuteScalarProc("Lum_Erp_Stp_AddStock", new SpParam("@storeId", int.Parse(ddlStore.SelectedValue), SqlDbType.Int), new SpParam("@itemId", int.Parse(ddlItem.SelectedValue), SqlDbType.Int), new SpParam("@quantity", int.Parse(quantity.Text), SqlDbType.Int), new SpParam("@update", Convert.ToDateTime(update.Text), SqlDbType.DateTime), new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int));
                    _opStockID = (int)obj;
                    if (_opStockID == -1)
                    {
                        message.Text = "item Code " + ddlItem.Items[ddlItem.SelectedIndex].Text + " already exist in store " + ddlStore.Items[ddlStore.SelectedIndex].Text;
                        message.ForeColor = Color.Red;
                    }
                    else
                    {
                        if (ddlDivision.Items[ddlDivision.SelectedIndex].Text.ToUpper() == "FINISHED GOODS")
                        {
                            string[] param = serialNo.Split(';');

                            InsertItemSerialNo(itemId, txtserialNo.Text, _opStockID, conn, Convert.ToInt32(quantity.Text));
                        }
                        else
                        {
                            InsertItemSerialNo(itemId, txtserialNo.Text, _opStockID, conn, Convert.ToInt32(quantity.Text));

                        }
                        message.Text = "New Stock Added  for store : " + ddlStore.Items[ddlStore.SelectedIndex].Text;
                        message.ForeColor = Color.Green;
                    }
                }

            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
                message.ForeColor = Color.Red;
                return;
            }
        }
        BindGrid();
        ResetControl();
    }

    public void InsertItemSerialNo(int itemId, string serialNo, int opStockID, IConnection conn, int receiveQty)
    {
        //conn.ExecuteCommandProc("Lum_Erp_Stp_DeleteGrnItemSerialNo", new SpParam("@grnLineId", grnlineId, SqlDbType.Int),
        //                              new SpParam("@grnId", grnId, SqlDbType.Int));
        if (serialNo != string.Empty)
        {
            string[] param = serialNo.Split(';');
            try
            {
                foreach (string str in param)
                {

                    conn.ExecuteCommandProc("Lum_Erp_Stp_InsertSerialNumber_fromOPStock",
                            new SpParam("@itemId", itemId, SqlDbType.Int),
                            new SpParam("@serialNo", str, SqlDbType.VarChar),
                            new SpParam("@opStockID", opStockID, SqlDbType.Int));
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }
        else
        {
            try
            {
                for (int _i = 0; _i < receiveQty; _i++)
                {
                    conn.ExecuteCommandProc("Lum_Erp_Stp_InsertSerialNumber_fromOPStock", 
                            new SpParam("@itemId", itemId, SqlDbType.Int),
                            new SpParam("@serialNo", string.Empty, SqlDbType.VarChar),
                            new SpParam("@opStockID", opStockID, SqlDbType.Int));

                }

            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }

    }

    protected void stockGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        int opquantity;
        int clquantity;
        if (e.Item.Cells[2].Text.ToString() != "&nbsp;")
        {

             opquantity = Convert.ToInt32(e.Item.Cells[2].Text.ToString().Replace(".00", ""));
        }
        else { opquantity = 0; }
        if (e.Item.Cells[3].Text.ToString() != "&nbsp;")
        {
            clquantity = Convert.ToInt32(e.Item.Cells[3].Text.ToString().Replace(".00", ""));
        }
        else { clquantity = 0; }
      
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            if (e.CommandName.Equals("View"))
            {
                stockId = (int)stockGrid.DataKeys[e.Item.ItemIndex];
                LoadStock();
                add.Visible = false;
            }
           
                        if (e.CommandName.Equals("Edit"))
                        {
                            stockId = (int)stockGrid.DataKeys[e.Item.ItemIndex];
                            LoadStock();
                            add.Visible = true;
                        }
                        else if (e.CommandName.Equals("Delete"))
                        {
                            if (opquantity != clquantity)
                            {
                                message.Text ="Opening Stock Cannot be Deleted !! Items already used ";
                                message.ForeColor = Color.Red;
                            }
                            stockId = (int)stockGrid.DataKeys[e.Item.ItemIndex];
                            add.Visible = true;
                            try
                            {
                

                                conn.ExecuteCommand("delete from Lum_Erp_StockActivityLog where stockId = " + stockId);
                                conn.ExecuteCommand("delete from Lum_Erp_stock where stockId = " + stockId);
                                conn.ExecuteCommand("delete from Lum_Erp_GrndetailsSerialNumber where opstockId = " + stockId);
                                message.Text = "Stock deleted";
                                message.ForeColor = Color.Green;
                            }
                            catch(Exception ex)
                            {
                                message.Text = "Stock cannot be deleted because of existing related data";
                                message.ForeColor = Color.Red;
                            }
                            ResetControl();


                            BindGrid();
                        }
              
        
        }
    }

    private void LoadStock()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            string strQuery = @"select ST.closingQuantity, S.subdivisionId,D.divisionId,ST.storeId,ST.quantity,ST.[update],I.itemId,U.uomName from Lum_Erp_Stock ST
                        inner join Lum_Erp_Item I on I.itemId=ST.itemId 
                        inner join  Lum_Erp_Subdivision S  on  S.subdivisionId=I.subdivisionId
                        inner join Lum_Erp_division D on D.divisionId=S.divisionId
                        inner join Lum_Erp_UOM U on U.uomId=I.uomId
                        Where  stockId = " + stockId;
            using (IDataReader reader = conn.ExecuteQuery(strQuery))
            {
                if (reader.Read())
                {
                    ddlStore.SelectedValue = Convert.ToString(reader["storeId"]);
                    ddlDivision.SelectedValue = Convert.ToString(reader["divisionId"]);
                    BindListControl _control = new BindListControl();
                    _control.BindSubDivision(ddlSubdivision, ddlDivision.SelectedValue);
                    ddlSubdivision.SelectedValue = Convert.ToString(reader["subdivisionId"]);
                   _control.BindItems(ddlItem,  ddlSubdivision.SelectedValue);
                   ddlItem.SelectedValue = Convert.ToString(reader["itemid"]);
                   update.Text = Convert.ToString(reader["update"]).Split(' ')[0];
                   quantity.Text = Convert.ToString(reader["quantity"]);
                   ddlStore.Enabled = false;
                   ddlItem.Enabled = false;
                   ddlDivision.Enabled=false;
                   ddlSubdivision.Enabled = false;
                   uomName.Text = Convert.ToString(reader["uomName"]);

                    add.Text = "Update";
                    cancel.Visible = true;
                }
            }
        }
    }

    public void ResetControl()
    {
        ddlStore.Enabled = true;
        ddlDivision.Enabled = true;
        ddlSubdivision.Enabled = true;
        ddlItem.Enabled = true;
        add.Text = "Add";
        ddlStore.SelectedIndex = 0;
        ddlDivision.SelectedIndex = 0;
      
        ddlSubdivision.SelectedIndex = 0;
        ddlItem.SelectedIndex = 0;
        quantity.Text = string.Empty;
        update.Text = string.Empty;
        stockId = -1;
        txtserialNo.Text = "";
        errmsg.Text = "";

    }
    protected void cancel_Click(object sender, EventArgs e)
    {
        ResetControl();
    }
    protected void stockGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton btnview = (LinkButton)e.Item.Cells[5].Controls[0];
            btnview.CssClass = "GridLink";
            //LinkButton btnEdit = (LinkButton)e.Item.Cells[6].Controls[0];
            LinkButton btn = (LinkButton)e.Item.Cells[6].Controls[0];
            btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this?')");
            btn.CssClass = "GridLink";
            //btnEdit.CssClass = "GridLink";
            if (GetLoggedinUser().UserType == 2)
            {
                btn.Visible = false;
               // btnEdit.Visible = false;
            }
        }
    }

    protected void ddlDivision_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindListControl _control = new BindListControl();
        _control.BindSubDivision(ddlSubdivision, ddlDivision.SelectedValue);
    }
    protected void ddlSubdivision_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindListControl _control = new BindListControl();
        _control.BindItems(ddlItem, ddlSubdivision.SelectedValue);
    }
}
