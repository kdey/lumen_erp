﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Drawing;
using Bitscrape.AppBlock.Database;
public partial class DataEntry_Vendor : BasePage
{
    protected int vendorId = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Params["vendorId"] != null)
        {
            vendorId = int.Parse(Request.Params["vendorId"]);
        }
        message.Text = "";
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!IsPostBack)
        {
            BindGrid();
            try
            {
               
                if (vendorId!=-1)
                {
                    LoadVendor();
                }
            }
            catch { };
            //using (IConnection conn = DataConnectionFactory.GetConnection())
            //{
            //    using (IDataReader reader = conn.ExecuteQuery("select * from AdminUser where userName= 'demo'"))
            //    {
            //        while (reader.Read())
            //        {
            //            AdminUser s = GetLoggedinUser();
            //            int a = s.UserId;
            //            if (a == Convert.ToInt32(reader["adminuserId"]))

            //                add.Enabled = false;
            //        }
            //    }
            //}
        }

        if (GetLoggedinUser().UserType == 2)
        {
            add.Visible  = false;

        }
    }
    protected void add_Click(object sender, EventArgs e)
    {
        if (vendorId == -1)
        {

            InsertVendor();
            BindGrid();
        }
        else
        {
            updateVendor();
        }
    }
    public void updateVendor()
    {
        int AcountId = 0;
        if (rbstatus.SelectedValue != "0")
        {
            AcountId = Convert.ToInt32(ddlVendor.SelectedValue);
        }

        SpParam[] _Sparray = new SpParam[19];

        _Sparray[0] = new SpParam("@vendorName", vendorName.Text.Trim(), SqlDbType.VarChar);
        _Sparray[1] = new SpParam("@address1", address1.Text.Trim(), SqlDbType.VarChar);
        _Sparray[2] = new SpParam("@address2", address2.Text.Trim(), SqlDbType.VarChar);
        _Sparray[3] = new SpParam("@phoneNo", phoneNo.Text.Trim(), SqlDbType.VarChar);
        _Sparray[4] = new SpParam("@mobileNo", mobileNo.Text.Trim(), SqlDbType.VarChar);
        _Sparray[5] = new SpParam("@zipCode", zipCode.Text.Trim(), SqlDbType.VarChar);
        _Sparray[6] = new SpParam("@emailId", emailId.Text.Trim(), SqlDbType.VarChar);
        _Sparray[7] = new SpParam("@webSite", webSite.Text.Trim(), SqlDbType.VarChar);
        _Sparray[8] = new SpParam("@contactPersonName", contactPersonName.Text.Trim(), SqlDbType.VarChar);
        _Sparray[9] = new SpParam("@contactNo", contactNo.Text.Trim(), SqlDbType.VarChar);
        _Sparray[10] = new SpParam("@VATRegistrationNo", VATRegistrationNo.Text.Trim(), SqlDbType.VarChar);
        _Sparray[11] = new SpParam("@CSTRegistrationNo", CSTRegistrationNo.Text.Trim(), SqlDbType.VarChar);
        _Sparray[12] = new SpParam("@faxNo", faxNo.Text.Trim(), SqlDbType.VarChar);
        _Sparray[13] = new SpParam("@serviceTaxRegNo", serviceTaxRegNo.Text.Trim(), SqlDbType.VarChar);
        _Sparray[14] = new SpParam("@panNo", panNo.Text.Trim(), SqlDbType.VarChar);
        _Sparray[15] = new SpParam("@acountType", Convert.ToInt32(rbstatus.SelectedValue), SqlDbType.Int);
        _Sparray[16] = new SpParam("@acountId", AcountId, SqlDbType.Int);
        _Sparray[17] = new SpParam("@otherDetails", otherDetails.Text.Trim(), SqlDbType.VarChar);
        _Sparray[18] = new SpParam("@vendorId", vendorId, SqlDbType.Int);
        try
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {


                int _i = conn.ExecuteCommandProc("Lum_Erp_Stp_UpdateVendor", _Sparray);

                if (_i == 1)
                {
                    message.Text = "Vendor  Updated : " + vendorName.Text;
                    message.ForeColor = Color.Green;
                    vendorId = -1;
                    ResetControl();

                }

            }
        }
        catch (Exception ex)
        {
            message.Text = ex.Message;
        }
        BindGrid();
    }
    public void InsertVendor()
    {
        int AcountId=0;
        if(rbstatus.SelectedValue != "0")
        {
        AcountId = Convert.ToInt32(ddlVendor.SelectedValue);
        }
        
        SpParam[] _Sparray = new SpParam[18];

        _Sparray[0] = new SpParam("@vendorName",vendorName.Text.Trim(),SqlDbType.VarChar);
        _Sparray[1] = new SpParam("@address1", address1.Text.Trim(), SqlDbType.VarChar);
        _Sparray[2] = new SpParam("@address2", address2.Text.Trim(), SqlDbType.VarChar);
        _Sparray[3] = new SpParam("@phoneNo", phoneNo.Text.Trim(), SqlDbType.VarChar);
        _Sparray[4] = new SpParam("@mobileNo", mobileNo.Text.Trim(), SqlDbType.VarChar);
        _Sparray[5] = new SpParam("@zipCode", zipCode.Text.Trim(), SqlDbType.VarChar);
        _Sparray[6] = new SpParam("@emailId", emailId.Text.Trim(), SqlDbType.VarChar);
        _Sparray[7] = new SpParam("@webSite", webSite.Text.Trim(), SqlDbType.VarChar);
        _Sparray[8] = new SpParam("@contactPersonName", contactPersonName.Text.Trim(), SqlDbType.VarChar);
        _Sparray[9] = new SpParam("@contactNo", contactNo.Text.Trim(), SqlDbType.VarChar);
        _Sparray[10] = new SpParam("@VATRegistrationNo", VATRegistrationNo.Text.Trim(), SqlDbType.VarChar);
        _Sparray[11] = new SpParam("@CSTRegistrationNo", CSTRegistrationNo.Text.Trim(), SqlDbType.VarChar);
        _Sparray[12] = new SpParam("@faxNo", faxNo.Text.Trim(), SqlDbType.VarChar);
        _Sparray[13] = new SpParam("@serviceTaxRegNo", serviceTaxRegNo.Text.Trim(), SqlDbType.VarChar);
        _Sparray[14] = new SpParam("@panNo", panNo.Text.Trim(), SqlDbType.VarChar);
        _Sparray[15] = new SpParam("@acountType", Convert.ToInt32(rbstatus.SelectedValue), SqlDbType.Int);
        _Sparray[16] = new SpParam("@acountId",  AcountId, SqlDbType.Int);
        _Sparray[17] = new SpParam("@otherDetails", otherDetails.Text.Trim(), SqlDbType.VarChar);
       
        try
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {


                int _i = conn.ExecuteCommandProc("[Lum_Erp_Stp_AddVendor]", _Sparray);

                if (_i == 1)
                {
                    message.Text = "Vendor  Added : " + vendorName.Text;
                    message.ForeColor = Color.Green;
                    ResetControl();
                   
                }

            }
        }
        catch (Exception ex)
        {
            message.Text = ex.Message;
        }

    }
    private void BindGrid()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select * from Lum_Erp_Vendor order by vendorName"))
            {
                vendorGrid.DataSource = reader;
                vendorGrid.DataBind();
            }
        }

    }

    protected void rbstatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbstatus.SelectedValue == "0")
        {
            ddlVendor.Enabled = false;
        }
        else
        {
            ddlVendor.Items.Clear();
            ddlVendor.Enabled = true;
            BindListControl _control = new BindListControl();
            _control.BindVendorAccount(ddlVendor);
            if (vendorId != -1)
            {
                for (int _index = 0; _index < ddlVendor.Items.Count; _index++)
                {
                    if (ddlVendor.Items[_index].Value == vendorId.ToString())
                    {

                        ddlVendor.Items.RemoveAt(_index);
                    }
                }
            }
        }
    }
    public void ResetControl()
    {
        vendorName.Text = string.Empty;
        address1.Text = string.Empty;
        address2.Text = string.Empty;
        zipCode.Text = string.Empty;
        phoneNo.Text = string.Empty;
        mobileNo.Text = string.Empty;
        emailId.Text = string.Empty;
        webSite.Text = string.Empty;
        contactPersonName.Text = string.Empty;
        contactNo.Text = string.Empty;
        VATRegistrationNo.Text = string.Empty;
        CSTRegistrationNo.Text = string.Empty;
        faxNo.Text = string.Empty;
        serviceTaxRegNo.Text = string.Empty;
        panNo.Text = string.Empty;
        otherDetails.Text = string.Empty;
        rbstatus.SelectedIndex = 0;
        ddlVendor.SelectedIndex = 0;
        ddlVendor.Enabled = false;

    }
    private void LoadVendor()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select * from Lum_Erp_Vendor where vendorId = " + vendorId))
            {
                if (reader.Read())
                {
                    vendorName.Text = Convert.ToString(reader["vendorName"]);
                    address1.Text = Convert.ToString(reader["address1"]);
                    address2.Text = Convert.ToString(reader["address2"]);
                    zipCode.Text = Convert.ToString(reader["zipCode"]);
                    phoneNo.Text = Convert.ToString(reader["phoneNo"]);
                    mobileNo.Text = Convert.ToString(reader["mobileNo"]);
                    emailId.Text = Convert.ToString(reader["emailId"]);
                    webSite.Text = Convert.ToString(reader["webSite"]);
                    contactPersonName.Text = Convert.ToString(reader["contactPersonName"]);
                    contactNo.Text = Convert.ToString(reader["contactNo"]);
                    VATRegistrationNo.Text = Convert.ToString(reader["VATRegistrationNo"]);
                    CSTRegistrationNo.Text = Convert.ToString(reader["CSTRegistrationNo"]);
                    faxNo.Text = Convert.ToString(reader["faxNo"]);
                    serviceTaxRegNo.Text = Convert.ToString(reader["serviceTaxRegNo"]);
                    panNo.Text = Convert.ToString(reader["panNo"]);
                    otherDetails.Text = Convert.ToString(reader["otherDetails"]);
                    rbstatus.SelectedIndex = Convert.ToInt32(reader["acountType"]) ;

                    if (Convert.ToInt32(reader["acountType"]) == 1)
                    {
                        ddlVendor.Enabled = true;
                        BindListControl _control = new BindListControl();
                        _control.BindVendorAccount(ddlVendor);
                        ddlVendor.SelectedValue = Convert.ToString(reader["acountId"]);
                    }
                    else
                    {
                        ddlVendor.Enabled = false;
                    }
                    add.Text = "Update";
                    cancel.Visible = true;
                }
            }
        }
    }

    protected void cancel_Click(object sender, EventArgs e)
    {
        ResetControl();
    }
    protected void vendorGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName.Equals("View"))
        {
            vendorId = (int)vendorGrid.DataKeys[e.Item.ItemIndex];
            LoadVendor();
            add.Visible = false;
            BindGrid();
        }
        if (e.CommandName.Equals("Edit"))
        {
            vendorId = (int)vendorGrid.DataKeys[e.Item.ItemIndex];
            LoadVendor();
            add.Visible = true;
            BindGrid();
        }
        else if (e.CommandName.Equals("Delete"))
        {
            add.Visible = true;

            vendorId = (int)vendorGrid.DataKeys[e.Item.ItemIndex];
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                try
                {
                    conn.ExecuteCommand("delete from Lum_Erp_Vendor where vendorId = " + vendorId);
                    message.Text = "Vendor deleted";
                    message.ForeColor = Color.Green;
                }
                catch
                {
                    message.Text = "Vendor cannot be deleted because of existing related data";
                    message.ForeColor = Color.Red;
                }

                vendorId = -1;
            }
            BindGrid();
        }
    }
    protected void vendorGrid_PreRender(object sender, System.EventArgs e)
    {
        for (int _index = 0; _index < vendorGrid.Items.Count; _index++)
        {
            string str = vendorGrid.Items[_index].Cells[1].Text;
            if (str == "0")
            {
                vendorGrid.Items[_index].Cells[1].Text = "Main";
            }
            else
            {
                vendorGrid.Items[_index].Cells[1].Text = "Branch";
            }
        }
    }
    protected void vendorGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton btnview = (LinkButton)e.Item.Cells[2].Controls[0];
            btnview.CssClass = "GridLink"; ;
            LinkButton btnEdit = (LinkButton)e.Item.Cells[3].Controls[0];
            LinkButton btn = (LinkButton)e.Item.Cells[4].Controls[0];
            btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this?')");
            btn.CssClass = "GridLink";
            btnEdit.CssClass = "GridLink";
            if (GetLoggedinUser().UserType == 2)
            {
                btn.Visible = false;
                btnEdit.Visible = false;
            }
        }
    }
}
