﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
public partial class DataEntry_SubDivisions : BasePage
{
    protected int subdivisionId = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        message.Text = "";
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!this.IsPostBack)
        {
            bindControl();
            BindGrid();
            //using (IConnection conn = DataConnectionFactory.GetConnection())
            //{
            //    using (IDataReader reader = conn.ExecuteQuery("select * from AdminUser where userName= 'demo'"))
            //    {
            //        while (reader.Read())
            //        {
            //            AdminUser s = GetLoggedinUser();
            //            int a = s.UserId;
            //            if (a == Convert.ToInt32(reader["adminuserId"]))

            //                add.Enabled = false;
            //        }
            //    }
            //}
        }
        else
        {
            // check if a division id has been passed
            try
            {
                subdivisionId = int.Parse(Request["subdivisionId"]);
            }
            catch { };
        }
        if (GetLoggedinUser().UserType == 2)
        {
            add.Visible  = false;

        }
    }
    public void bindControl()
    {
        BindListControl _control = new BindListControl();
        _control.BindDivision(ddlDivision);
    }
    private void BindGrid()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select S.subdivisionName,S.subdivisionId,D.divisionName from Lum_Erp_Subdivision S, Lum_Erp_Division D WHERE S.divisionId=D.divisionId order by D.divisionName"))
            {
                subdivisionGrid.DataSource = reader;
                subdivisionGrid.DataBind();
            }
        }

    }

    protected void add_Click(object sender, EventArgs e)
    {
        try
        {
            if (subdivisionId == -1)
            {
                // we are in add mode
                if (SubdivisionName.Text.Trim().Length > 0)
                {
                    using (IConnection conn = DataConnectionFactory.GetConnection())
                    {
                        int i = conn.ExecuteCommandProc("Lum_Erp_Stp_AddSubDivision", new SpParam("@divisionId", int.Parse(ddlDivision.SelectedValue), SqlDbType.Int), new SpParam("@subdivisionName", SubdivisionName.Text.Trim(), SqlDbType.VarChar));

                        if (i > 0)
                        {
                            message.Text = "Sub Division Added : " + SubdivisionName.Text;
                            message.ForeColor = Color.Green;
                            SubdivisionName.Text = string.Empty;
                            ddlDivision.SelectedIndex = 0;
                        }
                        else
                        {
                            message.Text = "Sub Division Already Exists : " + SubdivisionName.Text;
                            message.ForeColor = Color.Red;
                            SubdivisionName.Text = string.Empty;
                            ddlDivision.SelectedIndex = 0;
                        }
                    }
                }
            }
            else
            {
                // we are in edit mode.
                if (SubdivisionName.Text.Trim().Length > 0)
                {
                    using (IConnection conn = DataConnectionFactory.GetConnection())
                    {
                        int i = conn.ExecuteCommand(string.Format(@" if(not exists(select * from Lum_Erp_Subdivision where subdivisionName = '{0}' AND divisionId={1} and subdivisionId!={5} ))
                              update Lum_Erp_Subdivision set subdivisionName = '{2}',divisionId={3} where subdivisionId = {4}",
                               DataUtility.CleanString(SubdivisionName.Text.Trim()), int.Parse(ddlDivision.SelectedValue), DataUtility.CleanString(SubdivisionName.Text.Trim()), int.Parse(ddlDivision.SelectedValue), subdivisionId, subdivisionId));
                        if (i > 0)
                        {
                            add.Text = "Add";
                            message.Text = "Sub Division Changed to : " + SubdivisionName.Text;
                            message.ForeColor = Color.Green;
                            SubdivisionName.Text = string.Empty;
                            ddlDivision.SelectedIndex = 0;
                            subdivisionId = -1;
                        }
                        else
                        {
                            message.Text = "Sub Division Already Exists : " + SubdivisionName.Text;
                            message.ForeColor = Color.Red;
                        }

                    }
                }

            }

            BindGrid();

            #region stored proc code
            //create proc [dbo].[AddDivision] @divisionName varchar(255)
            //as
            //begin
            //    if(not exists(select * from Division where divisionName = @divisionName))
            //        insert into Division(divisionName) values(@divisionName)
            //end
            #endregion

        }
        catch { }

    }

    protected void subdivisionGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            if (e.CommandName.Equals("View"))
            {
                subdivisionId = (int)subdivisionGrid.DataKeys[e.Item.ItemIndex];
                LoadSubDivision();
                add.Visible = false;
            }
             
                        if (e.CommandName.Equals("Edit"))
                        {
                            subdivisionId = (int)subdivisionGrid.DataKeys[e.Item.ItemIndex];
                            LoadSubDivision();
                            add.Visible = true;
                        }
                        else if (e.CommandName.Equals("Delete"))
                        {
                            add.Visible = true;

                            subdivisionId = (int)subdivisionGrid.DataKeys[e.Item.ItemIndex];
                           
                                try
                                {
                                    conn.ExecuteCommand("delete from Lum_Erp_Subdivision where subdivisionId = " + subdivisionId);
                                    message.Text = "Sub Division deleted";
                                    message.ForeColor = Color.Green;
                                }
                                catch
                                {
                                    message.Text = "Sub Division cannot be deleted because of existing related data";
                                    message.ForeColor = Color.Red;
                                }
                                SubdivisionName.Text = string.Empty;
                                subdivisionId = -1;
                          
                            BindGrid();
                        }
                  
            
           
        }
    }

    private void LoadSubDivision()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select subdivisionName,divisionId from Lum_Erp_Subdivision where subdivisionId = " + subdivisionId))
            {
                if (reader.Read())
                {
                    SubdivisionName.Text = reader.GetString(0);
                    ddlDivision.SelectedValue = Convert.ToString(reader["divisionId"]);
                    add.Text = "Update";
                    cancel.Visible = true;
                }
            }
        }
    }

    protected void cancel_Click(object sender, EventArgs e)
    {
        SubdivisionName.Text = "";
        ddlDivision.SelectedIndex = 0;
        add.Text = "Add";
        subdivisionId = -1;
    }



    protected void subdivisionGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton btnview = (LinkButton)e.Item.Cells[2].Controls[0];
            btnview.CssClass = "GridLink";
            LinkButton btn = (LinkButton)e.Item.Cells[4].Controls[0];
            LinkButton btnEdit = (LinkButton)e.Item.Cells[3].Controls[0];
            btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this?')");
            btn.CssClass = "GridLink";
            btnEdit.CssClass = "GridLink";
            SubdivisionName.Text = "";
            ddlDivision.SelectedIndex = 0;
            add.Text = "Add";
            subdivisionId = -1;
            if (GetLoggedinUser().UserType == 2)
            {
                btn.Visible = false;
                btnEdit.Visible = false;
            }
        }
    }
}
