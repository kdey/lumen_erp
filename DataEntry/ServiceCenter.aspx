﻿<%@ Page Language="C#" MasterPageFile="~/DataEntry/DataEntry.master" AutoEventWireup="true"
    CodeFile="ServiceCenter.aspx.cs" Inherits="DataEntry_ServiceCenter" Title="Service Center" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<script type="text/javascript">
//Edit the counter/limiter value as your wish
   //Example: var count = "175";
   var count = 250;
   function limiter(id) {
       var tex = document.getElementById(id).value;
       var len = tex.length;
       var count = 250;
      
       if (len > count) {
           tex = tex.substring(0, count);
           document.getElementById(id).value = tex;
           return false;
       }
   }
   
   //
  

</script>
    <input type="hidden" name="serviceCenterId" value="<%=serviceCenterId%>" />
    <div class="PageMargin">
        <div class="PageHeading">Service Center</div>
        <div class="Line"></div>
        <!--Content Table Start-->
        <div class="PageContent">
        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
        <tr><td width="12%" align="right" valign="middle">Service Center&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="16%" align="left" valign="middle">
                <asp:TextBox ID="centerName" runat="server"   CssClass="TextboxSmall" MaxLength="50" />
            </td>
            <td width="10%" align="right" valign="middle">Address&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="16%" align="left" valign="middle">
                <asp:TextBox ID="address" runat="server" onkeypress="return limiter(this.id);" 
                    TextMode="MultiLine" CssClass="TextAreaSmall" Height="50px" />
            </td>
            <td width="10%" align="right" valign="middle">Zip Code&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="16%" align="left" valign="middle">
                <asp:TextBox ID="zipCode" runat="server"  CssClass="TextboxSmall" MaxLength="50" />
                 <br />
                   <asp:RegularExpressionValidator ID="quanityRegexValidator" runat="server"
                    Display="Dynamic"  ErrorMessage="Zip Code must be a integer"
                    Text="" ControlToValidate="zipCode" ValidationExpression="\d+" />
            </td>
            <td width="20%" align="center" valign="middle">
                <asp:Button ID="add" Text="Add" runat="server" OnClick="add_Click" CssClass="Button" />&nbsp;
                <asp:Button ID="cancel" Text="Cancel" runat="server" OnClick="cancel_Click" CausesValidation="false"
                    CssClass="Button" /></td>
        </tr>
        <tr><td align="left" valign="middle"><asp:Label ID="message" runat="server" /></td>
            <td align="left" valign="middle"><asp:RequiredFieldValidator ControlToValidate="centerName" ErrorMessage="Field cannot be empty"
                    ID="RequiredFieldValidator1" runat="server" /></td>
            <td>&nbsp;</td>
            <td align="left" valign="middle"><asp:RequiredFieldValidator ControlToValidate="address" ErrorMessage="Field cannot be empty"
                    ID="RequiredFieldValidator3" runat="server" /></td>
            <td>&nbsp;</td>
            <td align="left" valign="middle"><asp:RequiredFieldValidator ControlToValidate="zipCode" ErrorMessage="Field cannot be empty"
                    ID="nameValidator" runat="server" /></td>
            <td>&nbsp;</td>
        </tr></table>
        </div>
        <!--Content Table End-->
        <div class="Line">
        </div>
        <!--Grid Table Start-->
        <asp:DataGrid ID="serviceCenterGrid" runat="server" AutoGenerateColumns="false" DataKeyField="serviceCenterId"
            CellPadding="0" OnItemCommand="serviceCenterGrid_ItemCommand" HeaderStyle-CssClass="GridHeading"
            ItemStyle-CssClass="GridData" onitemcreated="serviceCenterGrid_ItemCreated" 
            GridLines="None" Width="100%">
            <Columns>             
                <asp:BoundColumn DataField="centerName" HeaderText="Center Name" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="left" />
                <asp:ButtonColumn  ButtonType="LinkButton" CausesValidation="false" 
                    CommandName="View" Text="View" 
                 ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="GridLink" 
                    ItemStyle-Width="10%" >
<ItemStyle HorizontalAlign="Left" CssClass="GridLink" Width="10%"></ItemStyle>
                </asp:ButtonColumn>
                <asp:ButtonColumn  ButtonType="LinkButton" CommandName="Edit" Text="Edit" ItemStyle-CssClass="GridLink" ItemStyle-Width="10%" />
                <asp:ButtonColumn ButtonType="LinkButton"  CommandName="Delete" Text="Delete" ItemStyle-CssClass="GridLink" ItemStyle-Width="10%" />
            </Columns>
        </asp:DataGrid>
        <!--Grid Table End-->
        <div class="Line">
        </div>
        <!--Page Body End-->
    </div>
</asp:Content>
