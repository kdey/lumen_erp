﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;
public partial class DataEntry_UOMMaster : BasePage
{
    protected int uomId = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        message.Text = "";
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!this.IsPostBack)
        {
            BindGrid();
            //using (IConnection conn = DataConnectionFactory.GetConnection())
            //{
            //    using (IDataReader reader = conn.ExecuteQuery("select * from AdminUser where userName= 'demo'"))
            //    {
            //        while (reader.Read())
            //        {
            //            AdminUser s = GetLoggedinUser();
            //            int a = s.UserId;
            //            if (a == Convert.ToInt32(reader["adminuserId"]))

            //                add.Enabled = false;
            //        }
            //    }
            //}
        }
        else
        {
            // check if a division id has been passed
            try
            {
                uomId = int.Parse(Request["uomId"]);
                //using (IConnection conn = DataConnectionFactory.GetConnection())
                //{
                //    using (IDataReader reader = conn.ExecuteQuery("select * from AdminUser where userName= 'demo'"))
                //    {
                //        while (reader.Read())
                //        {
                //            AdminUser s = GetLoggedinUser();
                //            int a = s.UserId;
                //            if (a == Convert.ToInt32(reader["adminuserId"]))

                //                add.Enabled = false;
                //        }
                //    }
                //}
            }
            catch { };
        }
        if (GetLoggedinUser().UserType == 2)
        {
            add.Visible  = false;

        }
    }
    private void BindGrid()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select * from Lum_Erp_UOM order by uomName"))
            {
                uomGrid.DataSource = reader;
                uomGrid.DataBind();
            }
        }

    }

    protected void add_Click(object sender, EventArgs e)
    {
        if (uomId == -1)
        {
            // we are in add mode
            if (uomName.Text.Trim().Length > 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    int i = conn.ExecuteCommandProc("Lum_Erp_Stp_AddUom", new SpParam("@uomName", uomName.Text.Trim(), SqlDbType.VarChar));
                   if (i > 0)
                   {
                       message.Text = "UOM Added : " + uomName.Text;
                       message.ForeColor = Color.Green;
                       uomName.Text = string.Empty;
                   }
                   else
                   {
                       message.Text = "UOM Already Exists : " + uomName.Text;
                       message.ForeColor = Color.Green;
                       uomName.Text = string.Empty;
                   }
                }
            }
        }
        else
        {
            // we are in edit mode.
            if (uomName.Text.Trim().Length > 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                  int i=  conn.ExecuteCommand(string.Format(
                           "if(not exists(select * from Lum_Erp_UOM where uomName = '{0}' and uomId={3}))update Lum_Erp_UOM set uomName = '{1}' where uomId = {2}",
                           DataUtility.CleanString(uomName.Text.Trim()), DataUtility.CleanString(uomName.Text.Trim()), uomId, uomId));
                  if (i > 0)
                  {
                      add.Text = "Add";
                      message.Text = "UOM Changed to : " + uomName.Text;
                      message.ForeColor = Color.Green;
                      uomName.Text = string.Empty;
                      uomId = -1;
                  }
                  else
                  {
                      message.Text = "UOM Already Exists : " + uomName.Text;
                      message.ForeColor = Color.Green;
                  }

                }
            }

        }

        BindGrid();

        #region stored proc code
        //create proc [dbo].[AddDivision] @uomName varchar(255)
        //as
        //begin
        //    if(not exists(select * from Division where uomName = @uomName))
        //        insert into Division(uomName) values(@uomName)
        //end
        #endregion
    }

    protected void uomGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {

            if (e.CommandName.Equals("View"))
            {
                uomId = (int)uomGrid.DataKeys[e.Item.ItemIndex];
                Loaduom();
                add.Visible = false;
            }
           
                        if (e.CommandName.Equals("Edit"))
                        {
                            uomId = (int)uomGrid.DataKeys[e.Item.ItemIndex];
                            Loaduom();
                            add.Visible = true;
                        }
                        else if (e.CommandName.Equals("Delete"))
                        {
                            add.Visible = true;
                            uomId = (int)uomGrid.DataKeys[e.Item.ItemIndex];
                           
                                try
                                {
                                    conn.ExecuteCommand("delete from Lum_Erp_UOM where uomId = " + uomId);
                                    message.Text = "UOM deleted";
                                    message.ForeColor = Color.Green;
                                }
                                catch
                                {
                                    message.Text = "UOM cannot be deleted because of existing related data";
                                    message.ForeColor = Color.Red;
                                }
                                uomName.Text = string.Empty;
                                uomId = -1;
                       
                            BindGrid();
                        }
              
            
          
        }
    }

    private void Loaduom()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select uomName from Lum_Erp_UOM where uomId = " + uomId))
            {
                if (reader.Read())
                {
                    uomName.Text = reader.GetString(0);
                    add.Text = "Update";
                    cancel.Visible = true;
                }
            }
        }
    }

    protected void cancel_Click(object sender, EventArgs e)
    {
        uomName.Text = "";
        add.Text = "Add";
        uomId = -1;
    }
    protected void uomGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton btnview = (LinkButton)e.Item.Cells[1].Controls[0];
            btnview.CssClass = "GridLink";
            LinkButton btnEdit = (LinkButton)e.Item.Cells[2].Controls[0];
            LinkButton btn = (LinkButton)e.Item.Cells[3].Controls[0];
            btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this?')");
            btn.CssClass = "GridLink";
            btnEdit.CssClass = "GridLink";
            uomName.Text = "";
            add.Text = "Add";
            uomId = -1;
            if (GetLoggedinUser().UserType == 2)
            {
                btn.Visible = false;
                btnEdit.Visible = false;
            }
        }
    }
}
