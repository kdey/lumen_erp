﻿<%@ Page Language="C#" MasterPageFile="~/DataEntry/DataEntry.master" AutoEventWireup="true"
    CodeFile="Transport.aspx.cs" Inherits="DataEntry_Transport" Title="Transport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <input type="hidden" name="transporterId" value="<%=transporterId%>" />
    <div class="PageMargin">
        <div class="PageHeading">Transporter</div>
        <div class="Line"></div>
        <!--Content Table Start-->
        <div class="PageContent">
        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
        <tr><td width="40%" align="right" valign="middle">Transporter Name&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="20%" align="left" valign="middle">
                <asp:TextBox ID="transporterName" MaxLength="50" runat="server" CssClass="TextboxSmall" />
            </td>
            <td align="left" valign="middle" width="40%">
                <asp:Button ID="add" Text="Add" runat="server" OnClick="add_Click" CssClass="Button" />
                <asp:Button ID="cancel" Text="Cancel" runat="server" OnClick="cancel_Click" CausesValidation="false"
                    CssClass="Button" />
            </td>
        </tr>
        <tr><td align="left" valign="middle"><asp:Label ID="message" runat="server" /></td>
            <td align="left" valign="middle"><asp:RequiredFieldValidator ControlToValidate="transporterName" ErrorMessage="Field cannot be empty"
                    ID="nameValidator" runat="server" /></td>
            <td>&nbsp;</td>
        </tr></table>
        </div>
        <!--Content Table End-->
        <div class="Line"></div>
        <!--Grid Table Start-->
        <asp:DataGrid ID="transporterGrid" runat="server" AutoGenerateColumns="false" DataKeyField="transporterId"
            CellPadding="0" OnItemCommand="transporterGrid_ItemCommand" HeaderStyle-CssClass="GridHeading"
            ItemStyle-CssClass="GridData" onitemcreated="transporterGrid_ItemCreated" GridLines="None" Width="100%">
            <Columns>
                <asp:BoundColumn DataField="transporterName" HeaderText="Transporter Name" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="left" />
                   <asp:ButtonColumn  ButtonType="LinkButton" CausesValidation="false" 
                    CommandName="View" Text="View" 
                 ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="GridLink" 
                    ItemStyle-Width="10%" >
<ItemStyle HorizontalAlign="Left" CssClass="GridLink" Width="10%"></ItemStyle>
                </asp:ButtonColumn>
                <asp:ButtonColumn ButtonType="LinkButton" CommandName="Edit" Text="Edit" ItemStyle-CssClass="GridLink" ItemStyle-Width="10%" />
                <asp:ButtonColumn ButtonType="LinkButton" CommandName="Delete" Text="Delete" ItemStyle-CssClass="GridLink" ItemStyle-Width="10%" />
            </Columns>
        </asp:DataGrid>
        <!--Grid Table End-->
        <div class="Line">
        </div>
        <!--Page Body End-->
    </div>
</asp:Content>

