﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DataEntry/DataEntry.master" AutoEventWireup="true" CodeFile="VendorList.aspx.cs" Inherits="DataEntry_VendorList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  
    <div class="PageMargin">
        <div class="PageHeading">
            Vendor List</div>
        <div class="Line">
        </div>
        <!--Content Table Start-->
        <div class="PageContent" align="center">
         <asp:Label ID="message" runat="server" />
         <br />
          <br />
               <asp:DataGrid ID="vendorGrid" runat="server" AutoGenerateColumns="false" DataKeyField="vendorId"
            CellPadding="3" CellSpacing="3" OnItemCommand="vendorGrid_ItemCommand" HeaderStyle-CssClass="GridHeading"
            ItemStyle-CssClass="GridData" onprerender="vendorGrid_PreRender" 
                onitemcreated="vendorGrid_ItemCreated">
            <Columns>
                <asp:BoundColumn DataField="vendorName" HeaderText="Vendor Name" HeaderStyle-Font-Bold="true" />
                <asp:BoundColumn DataField="acountType" HeaderText="Account Type" HeaderStyle-Font-Bold="true" />
                <asp:ButtonColumn ButtonType="LinkButton" CommandName="Edit" Text="Edit" ItemStyle-CssClass="GridLink" />
                <asp:ButtonColumn ButtonType="LinkButton" CommandName="Delete" Text="Delete" ItemStyle-CssClass="GridLink" />
            </Columns>
        </asp:DataGrid>
            <br />
        </div>
        <!--Content Table End-->
        <div class="Line">
        </div>
        <!--Grid Table Start-->
     
        <!--Grid Table End-->
        <div class="Line">
        </div>
        <!--Page Body End-->
    </div>
</asp:Content>

