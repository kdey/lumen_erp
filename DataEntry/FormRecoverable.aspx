﻿<%@ Page Language="C#" MasterPageFile="~/DataEntry/DataEntry.master" AutoEventWireup="true" CodeFile="FormRecoverable.aspx.cs" Inherits="DataEntry_FormRecoverable" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" name="FormRecoverableId" value="<%=FormRecoverableId%>" />
    <div class="PageMargin">
        <div class="PageHeading">Form Recoverable</div>
        <div class="Line"></div>
        <!--Content Table Start-->
        <div class="PageContent">
        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
        <tr><td align="right" valign="middle" width="40%">Form Recoverable :&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="20%" align="left" valign="middle">
                <asp:TextBox ID="FormRecoverableName" runat="server" CssClass="TextboxSmall" />
                
            </td>
            <td align="left" valign="middle" width="40%">
                <asp:Button ID="add" Text="Add" runat="server" OnClick="add_Click" CssClass="Button" />&nbsp;
                <asp:Button ID="cancel" Text="Cancel" runat="server" OnClick="cancel_Click" CausesValidation="false"
                    CssClass="Button" />
            </td>
        </tr>
        <tr><td align="left" valign="middle"><asp:Label ID="message" runat="server" /></td>
            <td align="left" valign="middle"><asp:RequiredFieldValidator ControlToValidate="FormRecoverableName" ErrorMessage="Field cannot be empty"
                ID="nameValidator" runat="server" /></td>
            <td>&nbsp;</td>
        </tr></table>
        </div>
        <!--Content Table End-->
        <div class="Line"></div>
        <!--Grid Table Start-->
        <asp:DataGrid ID="FormrecoverableGrid" runat="server" AutoGenerateColumns="False" DataKeyField="FormRecoverableId"
            CellPadding="0" OnItemCommand="FormRecovrerableGrid_ItemCommand" HeaderStyle-CssClass="GridHeading"
            ItemStyle-CssClass="GridData" onitemcreated="FormRecovrerableGrid_ItemCreated" GridLines="None" Width="100%">
            <ItemStyle CssClass="GridData"></ItemStyle>
            <Columns>
                <asp:BoundColumn DataField="RecoverableFormName"  HeaderStyle-HorizontalAlign="left"
                    HeaderText="Existing Recoverable Forms" HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Left">
                <HeaderStyle Font-Bold="True"></HeaderStyle>
                </asp:BoundColumn>
                <asp:ButtonColumn  ButtonType="LinkButton" CausesValidation="false" 
                    CommandName="View" Text="View" 
                 ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="GridLink" 
                    ItemStyle-Width="10%" >
<ItemStyle HorizontalAlign="Left" CssClass="GridLink" Width="10%"></ItemStyle>
                </asp:ButtonColumn>
                <asp:ButtonColumn ButtonType="LinkButton" CommandName="Edit" Text="Edit" 
                    ItemStyle-CssClass="GridLink" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
                <ItemStyle CssClass="GridLink"></ItemStyle>
                </asp:ButtonColumn>
                <asp:ButtonColumn ButtonType="LinkButton" CommandName="Delete" Text="Delete" 
                    ItemStyle-CssClass="GridLink" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
                <ItemStyle CssClass="GridLink"></ItemStyle>
                </asp:ButtonColumn>                
            </Columns>
            <HeaderStyle CssClass="GridHeading"></HeaderStyle>
        </asp:DataGrid>
        <!--Grid Table End-->
        <div class="Line"></div>
        <!--Page Body End-->
    </div>
</asp:Content>
