﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using Bitscrape.AppBlock.Database;

public partial class DataEntry_OrderType :BasePage
{
    protected int orderTypeId = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        message.Text = "";
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!this.IsPostBack)
        {
            BindGrid();
           
        }
        else
        {
            // check if a HigherEducationCess id has been passed
            try
            {
                orderTypeId = int.Parse(Request["orderTypeId"]);
               
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
                message.ForeColor = Color.Green;
            }
        }
        if (GetLoggedinUser().UserType == 2)
        {
            add.Visible  = false;
           
        }
    }
    private void BindGrid()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select * from Lum_Erp_PurchaseOrderType  order by poTypeName"))
            {
                orderTypeGrid.DataSource = reader;
                orderTypeGrid.DataBind();
            }
        }

    }

    protected void add_Click(object sender, EventArgs e)
    {
        if (orderTypeId == -1)
        {
            // we are in add mode
            if (orderCode.Text.Trim().Length > 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    int i = conn.ExecuteCommandProc("Lum_Erp_Stp_AddPurchaseOrderType", new SpParam("@poTypeCode", orderCode.Text.Trim(), SqlDbType.VarChar), new SpParam("@poTypeName", orderName.Text.Trim(), SqlDbType.VarChar));
                   if (i > 0)
                   {
                       message.Text = "Purchase Order Type Added : " + orderCode.Text;
                       message.ForeColor = Color.Green;
                       orderCode.Text = string.Empty;
                       orderName.Text = string.Empty;
                   }
                   else
                   {
                       message.Text = "Purchase Order Type already exist : " + orderCode.Text;
                       message.ForeColor = Color.Green;
                   }
                }
            }
        }
        else
        {
            // we are in edit mode.
            if (orderCode.Text.Trim().Length > 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    conn.ExecuteCommand(string.Format(
                           "  if(not exists(select * from Lum_Erp_PurchaseOrderType where poTypeCode = '{0}' and poTypeName='{1}' and poTypeId!={5}))update Lum_Erp_PurchaseOrderType set poTypeCode = '{2}',poTypeName='{3}' where poTypeId = {4}",
                           DataUtility.CleanString(orderCode.Text.Trim()), DataUtility.CleanString(orderName.Text.Trim()), DataUtility.CleanString(orderCode.Text.Trim()), DataUtility.CleanString(orderName.Text.Trim()), orderTypeId, orderTypeId));
                    add.Text = "Add";
                    message.Text = "Purchase Order Type Changed to : " + orderCode.Text;
                    message.ForeColor = Color.Green;
                    orderCode.Text = string.Empty;
                    orderName.Text = string.Empty;
                    orderTypeId = -1;

                }
            }

        }

        BindGrid();

        #region stored proc code
        //create proc [dbo].[AddHigherEducationCess] @higherEducationCess varchar(255)
        //as
        //begin
        //    if(not exists(select * from HigherEducationCess where higherEducationCess = @higherEducationCess))
        //        insert into HigherEducationCess(higherEducationCess) values(@higherEducationCess)
        //end
        #endregion
    }

    protected void orderTypeGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            if (e.CommandName.Equals("View"))
            {
                orderTypeId = (int)orderTypeGrid.DataKeys[e.Item.ItemIndex];
                LoadOrderType();
                add.Visible = false;
            }
                        if (e.CommandName.Equals("Edit"))
                        {
                            orderTypeId = (int)orderTypeGrid.DataKeys[e.Item.ItemIndex];
                            LoadOrderType();
                            add.Visible = true;
                        }
                        else if (e.CommandName.Equals("Delete"))
                        {
                            add.Visible = true;
                            orderTypeId = (int)orderTypeGrid.DataKeys[e.Item.ItemIndex];

                            try
                            {
                                conn.ExecuteCommand("delete from Lum_Erp_PurchaseOrderType where poTypeId = " + orderTypeId);
                                message.Text = "PurchaseOrderType Type deleted";
                                message.ForeColor = Color.Green;
                            }
                            catch(Exception ex)
                            {
                                message.Text =  "PurchaseOrderType cannot be deleted because of existing related data";
                                message.ForeColor = Color.Red;
                            }
                            orderCode.Text = string.Empty;
                            orderName.Text = string.Empty;
                            orderTypeId = -1;

                            BindGrid();
                        }
                  
             
          
        }
    }

    private void LoadOrderType()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select poTypeCode,poTypeName from Lum_Erp_PurchaseOrderType where poTypeId = " + orderTypeId))
            {
                if (reader.Read())
                {
                    orderCode.Text = Convert.ToString(reader["poTypeCode"]);
                    orderName.Text = Convert.ToString(reader["poTypeName"]); ;
                    add.Text = "Update";
                    cancel.Visible = true;
                }
            }
        }
    }

    protected void cancel_Click(object sender, EventArgs e)
    {
        orderCode.Text = "";
        orderName.Text = "";
        add.Text = "Add";
        orderTypeId = -1;
    }
    protected void orderTypeGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton btnview = (LinkButton)e.Item.Cells[2].Controls[0];
            btnview.CssClass = "GridLink";
            LinkButton btnedit = (LinkButton)e.Item.Cells[3].Controls[0];
            LinkButton btn = (LinkButton)e.Item.Cells[4].Controls[0];
            btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this?')");
            btn.CssClass = "GridLink";
            btnedit.CssClass = "GridLink";

            orderCode.Text = "";
            orderName.Text = "";
            add.Text = "Add";
            orderTypeId = -1;
            if (GetLoggedinUser().UserType == 2)
            {
                btn.Visible = false;
                btnedit.Visible = false;
            }
        }
    }
}
