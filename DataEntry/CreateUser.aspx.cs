﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Drawing;
using Bitscrape.AppBlock.Database;
public partial class DataEntry_CreateUser : BasePage
{
    protected int adminuserId = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        message.Text = "";
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }

        if (GetLoggedinUser().UserType == 2)
        {
          Response.Redirect("./../Default.aspx");
        }
        if (!this.IsPostBack)
        {

            BindGrid();

        
        }
        else
        {
            // check if a division id has been passed
            try
            {
                adminuserId = int.Parse(Request["adminuserId"]);
            }
            catch(Exception ex)
            {
                message.Text = ex.Message;
                message.ForeColor = Color.Green;
            }
            add.Visible = true;
        }


    }

    private void BindGrid()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("SELECT * FROM Lum_Erp_AdminUser order by firstName"))
            {
                userGrid.DataSource = reader;
                userGrid.DataBind();

            }
        }

    }
    protected void userGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton btnView = (LinkButton)e.Item.Cells[3].Controls[0];
            LinkButton btnEdit = (LinkButton)e.Item.Cells[4].Controls[0];
           
          
            btnEdit.CssClass = "GridLink";
            btnView.CssClass = "GridLink";
            firstName.Text = string.Empty;
            lastName.Text = string.Empty;
            userId.Text = string.Empty;
            passWord.Text = string.Empty;
            adminuserId = -1;
            add.Text = "Add";

        }
    }
    protected void userGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        adminuserId = (int)userGrid.DataKeys[e.Item.ItemIndex];
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                using (IDataReader reader = conn.ExecuteQuery("select * from Lum_Erp_AdminUser where adminuserId = " + adminuserId))
                {
                    while (reader.Read())
                    {
                        AdminUser s = GetLoggedinUser();
                        int a = s.UserId;
                        if (a != Convert.ToInt32(reader["adminuserId"]))
                        {
                            if (e.CommandName.Equals("Edit"))
                            {
                                //adminuserId = (int)userGrid.DataKeys[e.Item.ItemIndex];
                                LoadUser();
                                add.Visible = true;
                            }
                            if (e.CommandName.Equals("View"))
                            {
                                //adminuserId = (int)userGrid.DataKeys[e.Item.ItemIndex];
                                LoadUser();
                                add.Visible = false;
                            }
                           
                        }
                    }
                }
            }
       
    }
    protected void add_Click(object sender, EventArgs e)
    {
        if (adminuserId == -1)
        {
            // we are in add mode

            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                int i = conn.ExecuteCommandProc("[Lum_Erp_Stp_AddNewUser]",
                                        new SpParam("@firstName", firstName.Text.Trim(), SqlDbType.VarChar),
                                        new SpParam("@lastName", lastName.Text.Trim(), SqlDbType.VarChar),
                                        new SpParam("@userName", userId.Text.Trim(), SqlDbType.VarChar),
                                        new SpParam("@passwordHash", passWord.Text.Trim(), SqlDbType.VarChar),
                                        new SpParam("@userTypeId", ddlUserType.SelectedValue, SqlDbType.Int),
                                        new SpParam("@isActive", int.Parse(rbActive.SelectedValue), SqlDbType.Bit));
                if (i > 0)
                {
                    message.Text = "New user is created : " + firstName.Text + " " + lastName.Text;
                    message.ForeColor = Color.Green;
                    firstName.Text = string.Empty;
                    lastName.Text = string.Empty;
                    userId.Text = string.Empty;
                    passWord.Text = string.Empty;
                }
                else
                {
                    message.Text = "";
                }
            }

        }
        else
        {
            // we are in edit mode.

            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                int i = conn.ExecuteCommandProc("[Lum_Erp_Stp_UpdateUser]",
                                                      new SpParam("@firstName", firstName.Text.Trim(), SqlDbType.VarChar),
                                                      new SpParam("@lastName", lastName.Text.Trim(), SqlDbType.VarChar),
                                                      new SpParam("@userName", userId.Text.Trim(), SqlDbType.VarChar),
                                                      new SpParam("@passwordHash", passWord.Text.Trim(), SqlDbType.VarChar),
                                                      new SpParam("@userTypeId", ddlUserType.SelectedValue, SqlDbType.Int),
                                                      new SpParam("@isActive", int.Parse(rbActive.SelectedValue), SqlDbType.Bit),
                                                      new SpParam("@adminuserId", adminuserId, SqlDbType.Int ));


                //conn.ExecuteCommand(string.Format(
                //       "update Lum_Erp_AdminUser set firstName='{0}',lastName='{1}',userName='{2}',passwordHash='{3}',userTypeId={4},isActive={5} where adminuserId={6}",
                //       DataUtility.CleanString(firstName.Text.Trim()),DataUtility.CleanString(lastName.Text.Trim()), DataUtility.CleanString(userId.Text.Trim()),
                //       DataUtility.CleanString(passWord.Text.Trim()),ddlUserType.SelectedValue,rbActive.SelectedValue,
                //       adminuserId));
                add.Text = "Add";
                message.Text = "user has been changed";
                message.ForeColor = Color.Green;
                firstName.Text = string.Empty;
                lastName.Text = string.Empty;
                userId.Text = string.Empty;
                passWord.Text = string.Empty;


                adminuserId = -1;

            }
        }

        

        BindGrid();
    }
    private void LoadUser()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQueryProc("Lum_Erp_Stp_getAdminUser",
                                         new SpParam("@adminuserid", adminuserId, SqlDbType.Int)))

            //using (IDataReader reader = conn.ExecuteQuery("select firstName,lastName,userName,passwordHash,userTypeId,isActive from Lum_Erp_AdminUser where adminuserId = " + adminuserId))
            {
                if (reader.Read())
                {


                  
                    firstName.Text = reader.GetString(0);
                    lastName.Text = reader.GetString(1);
                    userId.Text = Convert.ToString(reader["userName"]);
                    passWord.Text = Convert.ToString(reader["passwordHash"]);
                    ddlUserType.SelectedValue = Convert.ToString(reader["userTypeId"]);

                    if (Convert.ToString(reader["isActive"]) == "False")
                    {
                        //rbActive.Items[0].Selected = true;
                        rbActive.SelectedIndex = 1;
                    }
                    else

                        rbActive.SelectedIndex = 0;
                    add.Text = "Update";
                    cancel.Visible = true;
                }
            }
        }
    }
    protected void cancel_Click(object sender, EventArgs e)
    {

        firstName.Text = string.Empty;
        lastName.Text = string.Empty;
        userId.Text = string.Empty;
        passWord.Text = string.Empty;
        adminuserId = -1;
        add.Text = "Add";
    }
}
