﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Drawing;
using Bitscrape.AppBlock.Database;


public partial class DataEntry_ServiceTaxMaster : BasePage
{
    protected int serviceTaxId = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        message.Text = "";
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!this.IsPostBack)
        {

            BindGrid();
            //using (IConnection conn = DataConnectionFactory.GetConnection())
            //{
            //    using (IDataReader reader = conn.ExecuteQuery("select * from AdminUser where userName= 'demo'"))
            //    {
            //        while (reader.Read())
            //        {
            //            AdminUser s = GetLoggedinUser();
            //            int a = s.UserId;
            //            if (a == Convert.ToInt32(reader["adminuserId"]))

            //                add.Enabled = false;
            //        }
            //    }
            //}
        }
        else
        {
            // check if a division id has been passed
            try
            {
                serviceTaxId = int.Parse(Request["serviceTaxId"]);
                //using (IConnection conn = DataConnectionFactory.GetConnection())
                //{
                //    using (IDataReader reader = conn.ExecuteQuery("select * from AdminUser where userName= 'demo'"))
                //    {
                //        while (reader.Read())
                //        {
                //            AdminUser s = GetLoggedinUser();
                //            int a = s.UserId;
                //            if (a == Convert.ToInt32(reader["adminuserId"]))

                //                add.Enabled = false;
                //        }
                //    }
                //}
            }
            catch { };
        }
        if (GetLoggedinUser().UserType == 2)
        {
            add.Visible  = false;

        }
    }

    private void BindGrid()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("SELECT * FROM Lum_Erp_ServiceTaxMaster order by serviceTax"))
            {
                serviceTaxGrid.DataSource = reader;
                serviceTaxGrid.DataBind();
            }
        }

    }

    protected void add_Click(object sender, EventArgs e)
    {
        if (serviceTaxId == -1)
        {
            // we are in add mode
            if (serviceTax.Text.Trim().Length > 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    int i = conn.ExecuteCommandProc("Lum_Erp_Stp_AddServiceTaxMaster", new SpParam("@serviceTax", Convert.ToDecimal(serviceTax.Text.Trim()), SqlDbType.Decimal), new SpParam("@description", description.Text.Trim(), SqlDbType.VarChar));
                   if (i > 0)
                   {
                       message.Text = "Service Tax  Added : " + serviceTax.Text;
                       message.ForeColor = Color.Green;
                       serviceTax.Text = string.Empty;
                       description.Text = string.Empty;
                   }
                   else
                   {
                       message.Text = "Service Tax  Already Exists : " + serviceTax.Text;
                       message.ForeColor = Color.Red;
                      
                   }

                }
            }
        }
        else
        {
            // we are in edit mode.
            if (serviceTax.Text.Trim().Length > 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                  int i=  conn.ExecuteCommand(string.Format(
                           "if(not exists(select * from Lum_Erp_ServiceTaxMaster where serviceTax = '{0}'and serviceTaxId!={4}))update Lum_Erp_ServiceTaxMaster set serviceTax = '{1}',description='{2}' where serviceTaxId = {3}",
                           DataUtility.CleanString(serviceTax.Text.Trim()), DataUtility.CleanString(serviceTax.Text.Trim()), DataUtility.CleanString(description.Text.Trim()), serviceTaxId, serviceTaxId));
                  if (i > 0)
                  {
                      add.Text = "Add";
                      message.Text = "service Tax  Changed to : " + serviceTax.Text;
                      message.ForeColor = Color.Green;
                      serviceTax.Text = string.Empty;
                      description.Text = string.Empty;

                      serviceTaxId = -1;
                  }
                  else
                  {
                      message.Text = "Service Tax  Already Exists : " + serviceTax.Text;
                      message.ForeColor = Color.Red;
                  }

                }
            }

        }

        BindGrid();

        #region stored proc code
        //create proc [dbo].[AddDivision] @divisionName varchar(255)
        //as
        //begin
        //    if(not exists(select * from Division where divisionName = @divisionName))
        //        insert into Division(divisionName) values(@divisionName)
        //end
        #endregion
    }

    protected void serviceTaxGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
             if (e.CommandName.Equals("View"))
            {
                serviceTaxId = (int)serviceTaxGrid.DataKeys[e.Item.ItemIndex];
                LoadserviceTax();
                add.Visible = false;
            }
                        if (e.CommandName.Equals("Edit"))
                        {
                            serviceTaxId = (int)serviceTaxGrid.DataKeys[e.Item.ItemIndex];
                            LoadserviceTax();
                            add.Visible = true;
                        }
                        else if (e.CommandName.Equals("Delete"))
                        {
                            add.Visible = true;
                            serviceTaxId = (int)serviceTaxGrid.DataKeys[e.Item.ItemIndex];

                            try
                            {
                                conn.ExecuteCommand("delete from Lum_Erp_ServiceTaxMaster where serviceTaxId = " + serviceTaxId);
                                message.Text = "Service Tax  deleted";
                                message.ForeColor = Color.Green;
                            }
                            catch
                            {
                                message.Text = "Service Tax cannot be deleted because of existing related data";
                                message.ForeColor = Color.Red;
                            }
                            serviceTax.Text = string.Empty;
                            description.Text = string.Empty;
                            serviceTaxId = -1;

                            BindGrid();
                        }
                 

               
          
        }
    }

    private void LoadserviceTax()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select serviceTax,description from Lum_Erp_ServiceTaxMaster where serviceTaxId = " + serviceTaxId))
            {
                if (reader.Read())
                {

                    serviceTax.Text = reader.GetDecimal(0).ToString();
                    description.Text = reader.GetString(1);

                    add.Text = "Update";
                    cancel.Visible = true;
                }
            }
        }
    }

    protected void cancel_Click(object sender, EventArgs e)
    {
        serviceTax.Text = string.Empty;
        description.Text = string.Empty;
        serviceTaxId = -1;
        add.Text = "Add";

    }
    protected void serviceTaxGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton btnview = (LinkButton)e.Item.Cells[1].Controls[0];
            btnview.CssClass = "GridLink";
            LinkButton btnEdit = (LinkButton)e.Item.Cells[2].Controls[0];
            LinkButton btn = (LinkButton)e.Item.Cells[3].Controls[0];
            btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this?')");
            btn.CssClass = "GridLink";
            btnEdit.CssClass = "GridLink";
            if (GetLoggedinUser().UserType == 2)
            {
                btn.Visible = false;
                btnEdit.Visible = false;
            }

        }
    }
}
