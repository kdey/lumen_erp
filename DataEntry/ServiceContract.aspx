﻿<%@ Page Language="C#" MasterPageFile="~/DataEntry/DataEntry.master" AutoEventWireup="true"
    CodeFile="ServiceContract.aspx.cs" Inherits="DataEntry_ServiceContract" Title="Service Contract" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<script type="text/javascript">
//Edit the counter/limiter value as your wish
   //Example: var count = "175";
   var count = 250;
   function limiter(id) {
       var tex = document.getElementById(id).value;
       var len = tex.length;
       var count = 250;
      
       if (len > count) {
           tex = tex.substring(0, count);
           document.getElementById(id).value = tex;
           return false;
       }
   }

</script>
    <input type="hidden" name="serviceContractId" value="<%=serviceContractId%>" />
    <div class="PageMargin">
        <div class="PageHeading">Service Contract</div>
        <div class="Line"></div>
        <!--Content Table Start-->
        <div class="PageContent">
        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
        <tr><td width="20%" align="right" valign="middle">Service Contract Type&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="20%" align="left" valign="middle">
                <asp:TextBox ID="serviceContractType" MaxLength="50" runat="server"   CssClass="TextboxSmall" />                
            </td>
            <td width="20%" align="right" valign="middle">Description</td>
            <td width="20%" align="left" valign="middle">
                <asp:TextBox ID="serviceContractDescription" 
                    onkeypress="return limiter(this.id);" runat="server" TextMode="MultiLine" 
                    CssClass="TextAreaSmall" />
            </td>
            <td align="center" valign="middle" width="20%">
                <asp:Button ID="add" Text="Add" runat="server" OnClick="add_Click" CssClass="Button" />&nbsp;
                <asp:Button ID="cancel" Text="Cancel" runat="server" OnClick="cancel_Click" CausesValidation="false"
                    CssClass="Button" />
            </td>
        </tr>
        <tr><td align="left" valign="middle"><asp:Label ID="message" runat="server" /></td>
            <td align="left" valign="middle"><asp:RequiredFieldValidator ControlToValidate="serviceContractType" ErrorMessage="Field cannot be empty"
                    ID="RequiredFieldValidator1" runat="server" /></td>
            <td colspan="3">&nbsp;</td>
        </tr></table>
        </div>
        <!--Content Table End-->
        <div class="Line">
        </div>
        <!--Grid Table Start-->
        <asp:DataGrid ID="serviceContractGrid" runat="server" 
            AutoGenerateColumns="false" DataKeyField="serviceContractId"
            CellPadding="0" OnItemCommand="serviceContractGrid_ItemCommand" HeaderStyle-CssClass="GridHeading"
            ItemStyle-CssClass="GridData" onitemcreated="serviceContractGrid_ItemCreated" 
            GridLines="None" Width="100%" >
            <Columns>
                <asp:BoundColumn DataField="serviceContractType" HeaderText="Contract Type" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="left" />
                <asp:ButtonColumn  ButtonType="LinkButton" CausesValidation="false" 
                    CommandName="View" Text="View" 
                 ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="GridLink" 
                    ItemStyle-Width="10%" >
<ItemStyle HorizontalAlign="Left" CssClass="GridLink" Width="10%"></ItemStyle>
                </asp:ButtonColumn>
                <asp:ButtonColumn  ButtonType="LinkButton" CommandName="Edit" Text="Edit" ItemStyle-CssClass="GridLink" ItemStyle-Width="10%" />
                <asp:ButtonColumn ButtonType="LinkButton" CommandName="Delete" Text="Delete" ItemStyle-CssClass="GridLink" ItemStyle-Width="10%" />
            </Columns>
        </asp:DataGrid>
        <!--Grid Table End-->
        <div class="Line">
        </div>
        <!--Page Body End-->
    </div>
</asp:Content>
