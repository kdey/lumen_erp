﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Drawing;
using Bitscrape.AppBlock.Database;

public partial class DataEntry_ServiceContract :BasePage
{
    protected int serviceContractId = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        message.Text = "";
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!this.IsPostBack)
        {

            BindGrid();
            //using (IConnection conn = DataConnectionFactory.GetConnection())
            //{
            //    using (IDataReader reader = conn.ExecuteQuery("select * from AdminUser where userName= 'demo'"))
            //    {
            //        while (reader.Read())
            //        {
            //            AdminUser s = GetLoggedinUser();
            //            int a = s.UserId;
            //            if (a == Convert.ToInt32(reader["adminuserId"]))

            //                add.Enabled = false;
            //        }
            //    }
            //}
        }
        else
        {
            // check if a division id has been passed
            try
            {
                serviceContractId = int.Parse(Request["serviceContractId"]);
                //using (IConnection conn = DataConnectionFactory.GetConnection())
                //{
                //    using (IDataReader reader = conn.ExecuteQuery("select * from AdminUser where userName= 'demo'"))
                //    {
                //        while (reader.Read())
                //        {
                //            AdminUser s = GetLoggedinUser();
                //            int a = s.UserId;
                //            if (a == Convert.ToInt32(reader["adminuserId"]))

                //                add.Enabled = false;
                //        }
                //    }
                //}
            }
            catch { };
        }
        if (GetLoggedinUser().UserType == 2)
        {
            add.Visible  = false;

        }
    }

    private void BindGrid()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("SELECT * FROM Lum_Erp_ServiceContract order by serviceContractType"))
            {
                serviceContractGrid.DataSource = reader;
                serviceContractGrid.DataBind();
            }
        }

    }

    protected void add_Click(object sender, EventArgs e)
    {
        if (serviceContractId == -1)
        {
            // we are in add mode
            if (serviceContractType.Text.Trim().Length > 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    int i = conn.ExecuteCommandProc("Lum_Erp_Stp_AddServiceContract", new SpParam("@serviceContractType", serviceContractType.Text.Trim(), SqlDbType.VarChar), new SpParam("@serviceContractDescription", serviceContractDescription.Text.Trim(), SqlDbType.VarChar));
                  if (i > 0)
                  {
                      message.Text = "Sevice Contract Type  Added : " + serviceContractType.Text;
                      message.ForeColor = Color.Green;
                      serviceContractType.Text = string.Empty;
                      serviceContractDescription.Text = string.Empty;
                  }
                  else
                  {
                      message.Text = "Sevice Contract Type Already Exists : " + serviceContractType.Text;
                      message.ForeColor = Color.Red;
                      serviceContractType.Text = string.Empty;
                      serviceContractDescription.Text = string.Empty;
                  }
                   
                }
            }
        }
        else
        {
            // we are in edit mode.
            if (serviceContractType.Text.Trim().Length > 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                  int i=  conn.ExecuteCommand(string.Format(
                           " if(not exists(select * from Lum_Erp_ServiceContract where serviceContractType = '{0}' and serviceContractId!={4} ))update Lum_Erp_ServiceContract set serviceContractType = '{1}',serviceContractDescription='{2}' where serviceContractId = {3}",
                           DataUtility.CleanString(serviceContractType.Text.Trim()), DataUtility.CleanString(serviceContractType.Text.Trim()), DataUtility.CleanString(serviceContractDescription.Text.Trim()), serviceContractId, serviceContractId));
                  if (i > 0)
                  {
                      add.Text = "Add";
                      message.Text = "Service Contract  Changed to : " + serviceContractType.Text;
                      message.ForeColor = Color.Green;
                      serviceContractType.Text = string.Empty;
                      serviceContractDescription.Text = string.Empty;

                      serviceContractId = -1;
                  }
                  else
                  {
                      message.Text = "Sevice Contract Type Already Exists : " + serviceContractType.Text;
                      message.ForeColor = Color.Red;
                  }

                }
            }

        }

        BindGrid();

        #region stored proc code
        //create proc [dbo].[AddDivision] @divisionName varchar(255)
        //as
        //begin
        //    if(not exists(select * from Division where divisionName = @divisionName))
        //        insert into Division(divisionName) values(@divisionName)
        //end
        #endregion
    }

    protected void serviceContractGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
           if (e.CommandName.Equals("View"))
            {
                serviceContractId = (int)serviceContractGrid.DataKeys[e.Item.ItemIndex];
                LoadServiceContract();
                add.Visible = false;
            }
                        if (e.CommandName.Equals("Edit"))
                        {
                            add.Visible = true;
                            serviceContractId = (int)serviceContractGrid.DataKeys[e.Item.ItemIndex];
                            LoadServiceContract();
                        }
                        else if (e.CommandName.Equals("Delete"))
                        {
                            add.Visible = true;
                            serviceContractId = (int)serviceContractGrid.DataKeys[e.Item.ItemIndex];

                            try
                            {
                                conn.ExecuteCommand("delete from Lum_Erp_ServiceContract where serviceContractId = " + serviceContractId);
                                message.Text = "service Contract  deleted";
                                message.ForeColor = Color.Green;
                            }
                            catch
                            {
                                message.Text = "service Contract cannot be deleted because of existing related data";
                                message.ForeColor = Color.Red;
                            }
                            serviceContractType.Text = string.Empty;
                            serviceContractDescription.Text = string.Empty;
                            serviceContractId = -1;

                            BindGrid();
                        }
                   
             
          
        }
    }

    private void LoadServiceContract()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select serviceContractType,serviceContractDescription from Lum_Erp_ServiceContract where serviceContractId = " + serviceContractId))
            {
                if (reader.Read())
                {

                    serviceContractType.Text = reader.GetString(0);
                    serviceContractDescription.Text = reader.GetString(1);
                   
                    add.Text = "Update";
                    cancel.Visible = true;
                }
            }
        }
    }

    protected void cancel_Click(object sender, EventArgs e)
    {
        serviceContractType.Text = string.Empty;
        serviceContractDescription.Text = string.Empty;
        serviceContractId = -1;
        add.Text = "Add";

    }
    protected void serviceContractGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton btnview = (LinkButton)e.Item.Cells[1].Controls[0];
            btnview.CssClass = "GridLink";
            LinkButton btnEdit = (LinkButton)e.Item.Cells[2].Controls[0];
            LinkButton btn = (LinkButton)e.Item.Cells[3].Controls[0];
            btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this?')");
            btn.CssClass = "GridLink";
            btnEdit.CssClass = "GridLink";
            serviceContractType.Text = string.Empty;
            serviceContractDescription.Text = string.Empty;
            serviceContractId = -1;
            add.Text = "Add";
            if (GetLoggedinUser().UserType == 2)
            {
                btn.Visible = false;
                btnEdit.Visible = false;
            }
        }
    }
}
