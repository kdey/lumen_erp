﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using Bitscrape.AppBlock.Database;

public partial class DataEntry_HigherEducationCess :BasePage
{
    protected int higherEducationCessId = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        message.Text = "";
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!this.IsPostBack)
        {
            BindGrid();
          
        }
        else
        {
            // check if a HigherEducationCess id has been passed
            try
            {
                higherEducationCessId = int.Parse(Request["higherEducationCessId"]);
               
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
                message.ForeColor = Color.Green;
            }
        }
        if (GetLoggedinUser().UserType == 2)
        {
            add.Visible  = false;
        }
    }
    private void BindGrid()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select * from Lum_Erp_HigherEducationCess order by educationCess"))
            {
                higherEducationCessGrid.DataSource = reader;
                higherEducationCessGrid.DataBind();
            }
        }

    }

    protected void add_Click(object sender, EventArgs e)
    {
        if (higherEducationCessId == -1)
        {
            // we are in add mode
            if (higherEducationCess.Text.Trim().Length > 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    int i = conn.ExecuteCommandProc("Lum_Erp_Stp_AddHigherEducationCess", new SpParam("@educationCses", higherEducationCess.Text.Trim(), SqlDbType.VarChar));
                if (i > 0)
                {
                    message.Text = "HigherEducationCess Added : " + higherEducationCess.Text;
                    message.ForeColor = Color.Green;
                    higherEducationCess.Text = string.Empty;
                }
                else
                {
                    message.Text = "HigherEducationCess Already Exists : " + higherEducationCess.Text;
                    message.ForeColor = Color.Red;
                    higherEducationCess.Text = string.Empty;
                }
                }
            }
        }
        else
        {
            // we are in edit mode.
            if (higherEducationCess.Text.Trim().Length > 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                   int i= conn.ExecuteCommand(string.Format(
                           " if(not exists(select * from Lum_Erp_HigherEducationCess where educationCess = '{0}' and higherEducationCessId!={3}))update Lum_Erp_HigherEducationCess set educationCess = '{1}' where higherEducationCessId = {2}",
                           DataUtility.CleanString(higherEducationCess.Text.Trim()), DataUtility.CleanString(higherEducationCess.Text.Trim()), higherEducationCessId, higherEducationCessId));
                   if (i > 0)
                   {
                       add.Text = "Add";
                       message.Text = "HigherEducationCess Changed to : " + higherEducationCess.Text;
                       message.ForeColor = Color.Green;
                       higherEducationCess.Text = string.Empty;
                       higherEducationCessId = -1;
                   }
                   else
                   {
                       message.Text = "HigherEducationCess Already Exists : " + higherEducationCess.Text;
                       message.ForeColor = Color.Red;
                   }

                }
            }

        }

        BindGrid();

        #region stored proc code
        //create proc [dbo].[AddHigherEducationCess] @higherEducationCess varchar(255)
        //as
        //begin
        //    if(not exists(select * from HigherEducationCess where higherEducationCess = @higherEducationCess))
        //        insert into HigherEducationCess(higherEducationCess) values(@higherEducationCess)
        //end
        #endregion
    }

    protected void higherEducationCessGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            if (e.CommandName.Equals("View"))
            {
                higherEducationCessId = (int)higherEducationCessGrid.DataKeys[e.Item.ItemIndex];
                LoadHigherEducationCess();
                add.Visible = false;
            }
                        if (e.CommandName.Equals("Edit"))
                        {
                            higherEducationCessId = (int)higherEducationCessGrid.DataKeys[e.Item.ItemIndex];
                            LoadHigherEducationCess();
                            add.Visible = true;
                        }
                        else if (e.CommandName.Equals("Delete"))
                        {
                            higherEducationCessId = (int)higherEducationCessGrid.DataKeys[e.Item.ItemIndex];
                            add.Visible = true;
                            try
                            {
                                conn.ExecuteCommand("delete from Lum_Erp_HigherEducationCess where higherEducationCessId = " + higherEducationCessId);
                                message.Text = "HigherEducationCess deleted";
                                message.ForeColor = Color.Green;
                            }
                            catch
                            {
                                message.Text = "HigherEducationCess cannot be deleted because of existing related data";
                                message.ForeColor = Color.Red;
                            }
                            higherEducationCess.Text = string.Empty;
                            higherEducationCessId = -1;

                            BindGrid();
                        }
                    }
                
            
        
    }

    private void LoadHigherEducationCess()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select educationCess from Lum_Erp_HigherEducationCess where higherEducationCessId = " + higherEducationCessId))
            {
                if (reader.Read())
                {
                    higherEducationCess.Text = Convert.ToString(reader["educationCess"]); ;
                    add.Text = "Update";
                    cancel.Visible = true;
                }
            }
        }
    }

    protected void cancel_Click(object sender, EventArgs e)
    {
        higherEducationCess.Text = "";
        add.Text = "Add";
        higherEducationCessId = -1;
    }
    protected void higherEducationCessGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton btnview = (LinkButton)e.Item.Cells[1].Controls[0];

            LinkButton btnedit = (LinkButton)e.Item.Cells[2].Controls[0];
            LinkButton btn = (LinkButton)e.Item.Cells[3].Controls[0];
            btnview.CssClass = "GridLink";
            btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this?')");
            btn.CssClass = "GridLink";
            btnedit.CssClass = "GridLink";
            higherEducationCess.Text = "";
            add.Text = "Add";
            higherEducationCessId = -1;
            if (GetLoggedinUser().UserType == 2)
            {
                btn.Visible = false;
                btnedit.Visible = false;
            }
        }
    }
}
