<%@ Page Language="C#" MasterPageFile="~/DataEntry/DataEntry.master" AutoEventWireup="true" CodeFile="LumenSysdate.aspx.cs" Inherits="DataEntry_LumenSysdate"  %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            height: 26px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="PageMargin">
        <div class="PageHeading">Change the Current Systemdate</div>
        <div class="Line"></div>
        <!--Content Table Start-->
        <div class="PageContent">
        
        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
        <tr><td width="40%" align="right" valign="middle">Today&#39;s Date&nbsp; :&nbsp;&nbsp;</td>
            <td width="20%" align="center" valign="middle"><asp:TextBox ID="txttodaysdate" 
                    runat="server" CssClass="TextboxSmall" ReadOnly="True" />
             </td>
             <td>&nbsp;</td>
        </tr>
        <tr><td width="40%" align="right" valign="middle" class="style1">DataChangedPassword&nbsp; :&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="20%" align="center" valign="middle" class="style1">
                <asp:TextBox ID="txtdtpassword" runat="server" CssClass="TextboxSmall" TextMode="Password" 
                    MaxLength="10" />
             </td>
             <td><asp:RequiredFieldValidator ControlToValidate="txtdtpassword" ErrorMessage="Field cannot be empty" ID="RequiredFieldValidator1"
                 runat="server" /></td>
        </tr>
        <tr><td width="40%" align="right" valign="middle">Changed Date To&nbsp; :&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="20%" align="center" valign="middle"><asp:TextBox ID="txtnewsysdate" runat="server" CssClass="TextboxSmall" />
            <cc1:calendarextender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="txtnewsysdate">
                </cc1:calendarextender>
             </td>
             <td><asp:RequiredFieldValidator ControlToValidate="txtdtpassword" ErrorMessage="Field cannot be empty" ID="nameValidator"
                 runat="server" /></td>
        </tr>

        <tr>
            <td  valign="middle" width="40%" colspan="3" align="center">
                <asp:Button ID="add" Text="Update New Date" runat="server" OnClick="add_Click" CssClass="ButtonBig" />
                &nbsp;&nbsp;
                <asp:Button ID="cancel" Text="Cancel" runat="server" OnClick="cancel_Click" CausesValidation="false"
                    CssClass="Button" />
            </td>
        </tr>
   
        <tr><td align="left" valign="middle" colspan="3"><asp:Label ID="message" runat="server" /></td>
        </tr></table>
        
        </div>
</asp:Content>

