﻿<%@ Page Language="C#" MasterPageFile="~/DataEntry/DataEntry.master" AutoEventWireup="true"
    CodeFile="EducationCess.aspx.cs" Inherits="DataEntry_EducationCess" Title="Education Cess" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<input type="hidden" name="educationCessId" value="<%=educationCessId%>" />
    <div class="PageMargin">
        <div class="PageHeading">Education Cess</div>
        <div class="Line"></div>
        <!--Content Table Start-->
        <div class="PageContent">
        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
        <tr><td width="40%" align="right" valign="middle">Education Cess&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="20%" align="center" valign="middle">
                <asp:TextBox ID="educationCess" runat="server" CssClass="TextboxSmall" />
            </td>
            <td align="left" valign="middle" width="40%">
                <asp:Button ID="add" Text="Add" runat="server" OnClick="add_Click" CssClass="Button" />
                &nbsp;
                <asp:Button ID="cancel" Text="Cancel" runat="server" OnClick="cancel_Click" CausesValidation="false"
                    CssClass="Button" />
            </td>
        </tr>
        <tr><td align="left" valign="middle"><asp:Label ID="message" runat="server" /></td>
            <td align="center" valign="middle"><asp:RequiredFieldValidator ControlToValidate="educationCess" ErrorMessage="Field cannot be empty"
                    ID="nameValidator" runat="server" /><br />
                <asp:RangeValidator ID="RangeValidator2" ControlToValidate="educationCess" runat="server" ErrorMessage="Please give correct decimal number <=100" Type="Double" MaximumValue="100"></asp:RangeValidator></td>
            <td>&nbsp;</td>
        </tr></table>
        </div>
        <!--Content Table End-->
        <div class="Line">
        </div>
        <!--Grid Table Start-->
        <asp:DataGrid ID="educationCessGrid" runat="server" AutoGenerateColumns="false" DataKeyField="educationCessId"
            CellPadding="0" OnItemCommand="educationCessGrid_ItemCommand" HeaderStyle-CssClass="GridHeading"
            ItemStyle-CssClass="GridData" onitemcreated="educationCessGrid_ItemCreated" 
            GridLines="None" Width="100%">
            <ItemStyle CssClass="GridData"></ItemStyle>
            <Columns>
                <asp:BoundColumn DataField="eduCess" HeaderText="Education Cess" HeaderStyle-HorizontalAlign="left"
                    HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="left">
                <HeaderStyle Font-Bold="True"></HeaderStyle>
                </asp:BoundColumn>
                <asp:ButtonColumn  ButtonType="LinkButton" CausesValidation="false" 
                    CommandName="View" Text="View" 
                 ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="GridLink" 
                    ItemStyle-Width="10%" >
<ItemStyle HorizontalAlign="Left" CssClass="GridLink" Width="10%"></ItemStyle>
                </asp:ButtonColumn>
                
                <asp:ButtonColumn ButtonType="LinkButton" CommandName="Edit" Text="Edit" ItemStyle-Width="10%" 
                    ItemStyle-CssClass="GridLink" ItemStyle-HorizontalAlign="left">
                <ItemStyle CssClass="GridLink"></ItemStyle>
                </asp:ButtonColumn>
                
                <asp:ButtonColumn ButtonType="LinkButton" CommandName="Delete" Text="Delete" ItemStyle-Width="10%"
                    ItemStyle-CssClass="GridLink" ItemStyle-HorizontalAlign="left">
                <ItemStyle CssClass="GridLink"></ItemStyle>
                </asp:ButtonColumn>
            </Columns>
        <HeaderStyle CssClass="GridHeading"></HeaderStyle>
        </asp:DataGrid>
        <!--Grid Table End-->
        <div class="Line">
        </div>
        <!--Page Body End-->
    </div>
</asp:Content>
