﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using Bitscrape.AppBlock.Database;

public partial class DataEntry_RateContractType : BasePage
{
    protected int rateContractTypeId = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        message.Text = "";
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!this.IsPostBack)
        {
            BindGrid();
           
        }
        else
        {
            // check if a division id has been passed
            try
            {
                rateContractTypeId = int.Parse(Request["rateContractTypeId"]);
               
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
                message.ForeColor = Color.Green;
            }
        }
        if (GetLoggedinUser().UserType == 2)
        {
            add.Visible  = false;
            
        }
    }
    private void BindGrid()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select * from Lum_Erp_RateContractType order by contractType"))
            {
                rateContractTypeGrid.DataSource = reader;
                rateContractTypeGrid.DataBind();
            }
        }

    }

    protected void add_Click(object sender, EventArgs e)
    {
        if (rateContractTypeId == -1)
        {
            // we are in add mode
            if (contractType.Text.Trim().Length > 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                  int i=  conn.ExecuteCommandProc("Lum_Erp_Stp_AddRateContractType", new SpParam("@contractType", contractType.Text.Trim(), SqlDbType.VarChar));
                  if (i > 0)
                  {
                      message.Text = "Rate ContractType Added : " + contractType.Text;
                      message.ForeColor = Color.Green;
                      contractType.Text = string.Empty;
                  }
                  else
                  {
                      message.Text = "Rate ContractType Already Exists : " + contractType.Text;
                      message.ForeColor = Color.Red;
                      contractType.Text = string.Empty;
                  }
                }
            }
        }
        else
        {
            // we are in edit mode.
            if (contractType.Text.Trim().Length > 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                   int i= conn.ExecuteCommand(string.Format(
                           "if(not exists(select * from Lum_Erp_RateContractType where contractType ='{0}' and rateContractTypeId={3}))update Lum_Erp_RateContractType set contractType = '{1}' where rateContractTypeId = {2}",
                           DataUtility.CleanString(contractType.Text.Trim()), DataUtility.CleanString(contractType.Text.Trim()), rateContractTypeId, rateContractTypeId));
                   if (i > 0)
                   {
                       add.Text = "Add";
                       message.Text = "Rate ContractType Changed to : " + contractType.Text;
                       message.ForeColor = Color.Green;
                       contractType.Text = string.Empty;
                       rateContractTypeId = -1;
                   }
                   else
                   {
                       message.Text = "Rate ContractType Already Exists : " + contractType.Text;
                       message.ForeColor = Color.Red;
                   }

                }
            }

        }

        BindGrid();

        #region stored proc code
        //create proc [dbo].[AddDivision] @contractType varchar(255)
        //as
        //begin
        //    if(not exists(select * from Division where contractType = @contractType))
        //        insert into Division(contractType) values(@contractType)
        //end
        #endregion
    }

    protected void rateContractTypeGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            if (e.CommandName.Equals("View"))
            {
                rateContractTypeId = (int)rateContractTypeGrid.DataKeys[e.Item.ItemIndex];
                LoadRateContractType();
                add.Visible = false;
            }
            if (e.CommandName.Equals("Edit"))
            {
                rateContractTypeId = (int)rateContractTypeGrid.DataKeys[e.Item.ItemIndex];
                LoadRateContractType();
                add.Visible = true;
            }
            else if (e.CommandName.Equals("Delete"))
            {
                add.Visible = true;
                rateContractTypeId = (int)rateContractTypeGrid.DataKeys[e.Item.ItemIndex];

                try
                {
                    conn.ExecuteCommand("delete from Lum_Erp_RateContractType where rateContractTypeId = " + rateContractTypeId);
                    message.Text = "Rate ContractType deleted";
                    message.ForeColor = Color.Green;
                }
                catch
                {
                    message.Text = "Rate ContractType cannot be deleted because of existing related data";
                    message.ForeColor = Color.Red;
                }
                contractType.Text = string.Empty;
                rateContractTypeId = -1;

                BindGrid();
            }




        }
    }

    private void LoadRateContractType()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select contractType from Lum_Erp_RateContractType where rateContractTypeId = " + rateContractTypeId))
            {
                if (reader.Read())
                {
                    contractType.Text = reader.GetString(0);
                    add.Text = "Update";
                    cancel.Visible = true;
                }
            }
        }
    }

    protected void cancel_Click(object sender, EventArgs e)
    {
        contractType.Text = "";
        add.Text = "Add";
        rateContractTypeId = -1;
    }
    protected void rateContractTypeGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton btnview = (LinkButton)e.Item.Cells[1].Controls[0];
            btnview.CssClass = "GridLink";
            LinkButton btnedit = (LinkButton)e.Item.Cells[2].Controls[0];
            LinkButton btn = (LinkButton)e.Item.Cells[3].Controls[0];
            btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this?')");
            btn.CssClass = "GridLink";
            btnedit.CssClass = "GridLink";
            contractType.Text = "";
            add.Text = "Add";
            rateContractTypeId = -1;
            if (GetLoggedinUser().UserType == 2)
            {
                btn.Visible = false;
                btnedit.Visible = false;
            }
        }
    }
}
