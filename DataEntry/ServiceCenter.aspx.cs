﻿using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Drawing;
using System;
using Bitscrape.AppBlock.Database;

public partial class DataEntry_ServiceCenter : BasePage
{
    protected int serviceCenterId = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        message.Text = "";
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!this.IsPostBack)
        {

            BindGrid();
            //using (IConnection conn = DataConnectionFactory.GetConnection())
            //{
            //    using (IDataReader reader = conn.ExecuteQuery("select * from AdminUser where userName= 'demo'"))
            //    {
            //        while (reader.Read())
            //        {
            //            AdminUser s = GetLoggedinUser();
            //            int a = s.UserId;
            //            if (a == Convert.ToInt32(reader["adminuserId"]))

            //                add.Enabled = false;
            //        }
            //    }
            //}
        }
        else
        {
            // check if a division id has been passed
            try
            {
                serviceCenterId = int.Parse(Request["serviceCenterId"]);
                //using (IConnection conn = DataConnectionFactory.GetConnection())
                //{
                //    using (IDataReader reader = conn.ExecuteQuery("select * from AdminUser where userName= 'demo'"))
                //    {
                //        while (reader.Read())
                //        {
                //            AdminUser s = GetLoggedinUser();
                //            int a = s.UserId;
                //            if (a == Convert.ToInt32(reader["adminuserId"]))

                //                add.Enabled = false;
                //        }
                //    }
                //}
            }
            catch(Exception ex)
            {
                message.Text = ex.Message;
                message.ForeColor = Color.Green;
            }
        }
        if (GetLoggedinUser().UserType == 2)
        {
            add.Visible  = false;

        }
    }

    private void BindGrid()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("SELECT * FROM Lum_Erp_ServiceCenter order by centerName"))
            {
                serviceCenterGrid.DataSource = reader;
                serviceCenterGrid.DataBind();
            }
        }

    }

    protected void add_Click(object sender, EventArgs e)
    {
        if (serviceCenterId == -1)
        {
            // we are in add mode
            if (centerName.Text.Trim().Length > 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    int i = conn.ExecuteCommandProc("Lum_Erp_sTP_AddServiceCenter", new SpParam("@CenterName", centerName.Text.Trim(), SqlDbType.VarChar), new SpParam("@address", address.Text.Trim(), SqlDbType.VarChar), new SpParam("@zipCode", zipCode.Text.Trim(), SqlDbType.VarChar));

                  if (i > 0)
                  {
                      message.Text = "Sevice Center  Added : " + centerName.Text;
                      message.ForeColor = Color.Green;
                      centerName.Text = string.Empty;
                      address.Text = string.Empty;
                      zipCode.Text = string.Empty;
                  }
                  else
                  {

                      message.Text = "Sevice Center  Already Exists : " + centerName.Text;
                      message.ForeColor = Color.Red;
                      centerName.Text = string.Empty;
                      address.Text = string.Empty;
                      zipCode.Text = string.Empty;
                  }
                }
            }
        }
        else
        {
            // we are in edit mode.
            if (centerName.Text.Trim().Length > 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                   int i= conn.ExecuteCommand(string.Format(
                           "if(not exists(select * from Lum_Erp_ServiceCenter where CenterName = '{0}' and serviceCenterId!={5} ))update Lum_Erp_ServiceCenter set centerName = '{1}',address='{2}',zipCode='{3}' where serviceCenterId = {4}",
                           DataUtility.CleanString(centerName.Text.Trim()), DataUtility.CleanString(centerName.Text.Trim()), DataUtility.CleanString(address.Text.Trim()), DataUtility.CleanString(zipCode.Text.Trim()), serviceCenterId, serviceCenterId));
                   if (i > 0)
                   {
                       add.Text = "Add";
                       message.Text = "Service Center  Changed to : " + centerName.Text;
                       message.ForeColor = Color.Green;
                       centerName.Text = string.Empty;
                       address.Text = string.Empty;
                       zipCode.Text = string.Empty;
                       serviceCenterId = -1;
                   }
                   else
                   {
                       message.Text = "Sevice Center  Already Exists : " + centerName.Text;
                       message.ForeColor = Color.Red;
                   }

                }
            }

        }

        BindGrid();

        #region stored proc code
        //create proc [dbo].[AddDivision] @divisionName varchar(255)
        //as
        //begin
        //    if(not exists(select * from Division where divisionName = @divisionName))
        //        insert into Division(divisionName) values(@divisionName)
        //end
        #endregion
    }

    protected void serviceCenterGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            if (e.CommandName.Equals("View"))
            {
                serviceCenterId = (int)serviceCenterGrid.DataKeys[e.Item.ItemIndex];
                LoadServiceCenter();
                add.Visible = false;
            }
                        if (e.CommandName.Equals("Edit"))
                        {
                            serviceCenterId = (int)serviceCenterGrid.DataKeys[e.Item.ItemIndex];
                            LoadServiceCenter();
                            add.Visible = true;
                        }
                        else if (e.CommandName.Equals("Delete"))
                        {
                            add.Visible = true;

                            serviceCenterId = (int)serviceCenterGrid.DataKeys[e.Item.ItemIndex];

                            try
                            {
                                conn.ExecuteCommand("delete from Lum_Erp_ServiceCenter where serviceCenterId = " + serviceCenterId);
                                message.Text = "service Center  deleted";
                                message.ForeColor = Color.Green;
                            }
                            catch
                            {
                                message.Text = "service Center cannot be deleted because of existing related data";
                                message.ForeColor = Color.Red;
                            }
                            centerName.Text = string.Empty;
                            address.Text = string.Empty;
                            zipCode.Text = string.Empty;
                            serviceCenterId = -1;

                            BindGrid();
                        }
                 
                
           
        }
    }

    private void LoadServiceCenter()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select centerName,address,zipCode from Lum_Erp_ServiceCenter where serviceCenterId = " + serviceCenterId))
            {
                if (reader.Read())
                {

                    centerName.Text = reader.GetString(0);
                    address.Text = reader.GetString(1);
                    zipCode.Text = reader.GetString(2);
                   
                    add.Text = "Update";
                    cancel.Visible = true;
                }
            }
        }
    }

    protected void cancel_Click(object sender, EventArgs e)
    {
        centerName.Text = string.Empty;
        address.Text = string.Empty;
        zipCode.Text = string.Empty;
        add.Text = "Add";
        serviceCenterId = -1;
    }
    protected void serviceCenterGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton btnview = (LinkButton)e.Item.Cells[1].Controls[0];
            btnview.CssClass = "GridLink";
            LinkButton btnEdit = (LinkButton)e.Item.Cells[2].Controls[0];
            LinkButton btn = (LinkButton)e.Item.Cells[3].Controls[0];
            btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this?')");
            btn.CssClass = "GridLink";
            btnEdit.CssClass = "GridLink";
            centerName.Text = string.Empty;
            address.Text = string.Empty;
            zipCode.Text = string.Empty;
            add.Text = "Add";
            serviceCenterId = -1;
            if (GetLoggedinUser().UserType == 2)
            {
                btn.Visible = false;
                btnEdit.Visible = false;
            }
        }
    }
}
