﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Drawing;
using Bitscrape.AppBlock.Database;

public partial class DataEntry_Customer : BasePage
{
    public DataTable aTable = new DataTable("Billingaddress");
    public DataTable bTable = new DataTable("Shippingaddress");
    protected int customerId = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        message.Text = "";
        customerName.Focus();
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        //BindGrid();
        try
        {
            if (Request.Params["customerId"] != null)
            {
                customerId = int.Parse(Request.Params["customerId"]);
            }
        }
        catch (Exception ex)
        {
            message.Text = ex.Message;
            message.ForeColor = Color.Green;
        }
        
       
        if (!IsPostBack)
        {
            BindListControl _control = new BindListControl();
            _control.BindCustomerSegment(ddlCustomerSegment);
           

                if (customerId != -1)
                {
                 LoadCustomer();
               
                    
                }
                BindGrid();
           
        }
        
        if (GetLoggedinUser().UserType == 2)
        {
            add.Visible  = false;
         
        }
    }
    protected void add_Click(object sender, EventArgs e)
    {
        if (customerId == -1)
        {
            InsertCustomer();
            BindGrid();
        }
        else
        {
            UpdateCustomer();
        }
    }
    public void UpdateCustomer()
    {
        int AcountId = 0;
        if (rbstatus.SelectedValue != "0")
        {
            AcountId = Convert.ToInt32(ddlCustomer.SelectedValue);
        }

        SpParam[] _Sparray = new SpParam[25];

        _Sparray[0] = new SpParam("@customerName", customerName.Text.Trim(), SqlDbType.VarChar);
        _Sparray[1] = new SpParam("@address1", address1.Text.Trim(), SqlDbType.VarChar);
        _Sparray[2] = new SpParam("@address2", address2.Text.Trim(), SqlDbType.VarChar);
        _Sparray[3] = new SpParam("@phoneNo", phoneNo.Text.Trim(), SqlDbType.VarChar);
        _Sparray[4] = new SpParam("@mobileNo", mobileNo.Text.Trim(), SqlDbType.VarChar);
        _Sparray[5] = new SpParam("@zipCode", zipCode.Text.Trim(), SqlDbType.VarChar);
        _Sparray[6] = new SpParam("@emailId", emailId.Text.Trim(), SqlDbType.VarChar);
        _Sparray[7] = new SpParam("@webSite", webSite.Text.Trim(), SqlDbType.VarChar);
        _Sparray[8] = new SpParam("@contactPersonName", contactPersonName.Text.Trim(), SqlDbType.VarChar);
        _Sparray[9] = new SpParam("@contactNo", contactNo.Text.Trim(), SqlDbType.VarChar);
        _Sparray[10] = new SpParam("@VATRegistrationNo", VATRegistrationNo.Text.Trim(), SqlDbType.VarChar);
        _Sparray[11] = new SpParam("@CSTRegistrationNo", CSTRegistrationNo.Text.Trim(), SqlDbType.VarChar);
        _Sparray[12] = new SpParam("@faxNo", faxNo.Text.Trim(), SqlDbType.VarChar);
        _Sparray[13] = new SpParam("@serviceTaxRegNo", serviceTaxRegNo.Text.Trim(), SqlDbType.VarChar);
        _Sparray[14] = new SpParam("@panNo", panNo.Text.Trim(), SqlDbType.VarChar);
        _Sparray[15] = new SpParam("@acountType", Convert.ToInt32(rbstatus.SelectedValue), SqlDbType.Int);
        _Sparray[16] = new SpParam("@acountId", AcountId, SqlDbType.Int);
        _Sparray[17] = new SpParam("@customerSegmentId",Convert.ToInt32(ddlCustomerSegment.SelectedValue), SqlDbType.Int);
        _Sparray[18] = new SpParam("@customerId", customerId, SqlDbType.Int);
       
        _Sparray[19] = new SpParam("@Shipping_address1", address3.Text.Trim(), SqlDbType.VarChar);
        _Sparray[20] = new SpParam("@Shipping_address2", address4.Text.Trim(), SqlDbType.VarChar);
        _Sparray[21] = new SpParam("@Shipping_phoneNo", phoneNo0.Text.Trim(), SqlDbType.VarChar);
        _Sparray[22] = new SpParam("@Shipping_mobileNo", mobileNo0.Text.Trim(), SqlDbType.VarChar);
        _Sparray[23] = new SpParam("@Shipping_zipCode", zipCode0.Text.Trim(), SqlDbType.VarChar);
        _Sparray[24] = new SpParam("@Shipping_faxNo", faxNo0.Text.Trim(), SqlDbType.VarChar);

        try
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {


                int _i = conn.ExecuteCommandProc("Lum_Erp_Stp_UpdateCustomer", _Sparray);

                if (_i == 1)
                {
                   
                    message.Text = "Customer  Updated : " + customerName.Text;
                    message.ForeColor = Color.Green;
                    customerId = -1;
                    ResetControl();
                    BindGrid();

                }

            }
        }
        catch (Exception ex)
        {
            message.Text = ex.Message;
        }

    }
    public void InsertCustomer()
    {
        int AcountId = 0;
        if (rbstatus.SelectedValue != "0")
        {
            AcountId = Convert.ToInt32(ddlCustomer.SelectedValue);
        }

        SpParam[] _Sparray = new SpParam[24];

        _Sparray[0] = new SpParam("@customerName", customerName.Text.Trim(), SqlDbType.VarChar);
        _Sparray[1] = new SpParam("@address1", address1.Text.Trim(), SqlDbType.VarChar);
        _Sparray[2] = new SpParam("@address2", address2.Text.Trim(), SqlDbType.VarChar);
        _Sparray[3] = new SpParam("@phoneNo", phoneNo.Text.Trim(), SqlDbType.VarChar);
        _Sparray[4] = new SpParam("@mobileNo", mobileNo.Text.Trim(), SqlDbType.VarChar);
        _Sparray[5] = new SpParam("@zipCode", zipCode.Text.Trim(), SqlDbType.VarChar);
        _Sparray[6] = new SpParam("@emailId", emailId.Text.Trim(), SqlDbType.VarChar);
        _Sparray[7] = new SpParam("@webSite", webSite.Text.Trim(), SqlDbType.VarChar);
        _Sparray[8] = new SpParam("@contactPersonName", contactPersonName.Text.Trim(), SqlDbType.VarChar);
        _Sparray[9] = new SpParam("@contactNo", contactNo.Text.Trim(), SqlDbType.VarChar);
        _Sparray[10] = new SpParam("@VATRegistrationNo", VATRegistrationNo.Text.Trim(), SqlDbType.VarChar);
        _Sparray[11] = new SpParam("@CSTRegistrationNo", CSTRegistrationNo.Text.Trim(), SqlDbType.VarChar);
        _Sparray[12] = new SpParam("@faxNo", faxNo.Text.Trim(), SqlDbType.VarChar);
        _Sparray[13] = new SpParam("@serviceTaxRegNo", serviceTaxRegNo.Text.Trim(), SqlDbType.VarChar);
        _Sparray[14] = new SpParam("@panNo", panNo.Text.Trim(), SqlDbType.VarChar);
        _Sparray[15] = new SpParam("@acountType", Convert.ToInt32(rbstatus.SelectedValue), SqlDbType.Int);
        _Sparray[16] = new SpParam("@acountId", AcountId, SqlDbType.Int);
        _Sparray[17] = new SpParam("@customerSegmentId",Convert.ToInt32(ddlCustomerSegment.SelectedValue), SqlDbType.Int);
     
        _Sparray[18] = new SpParam("@Shipping_address1", address3.Text.Trim(), SqlDbType.VarChar);
        _Sparray[19] = new SpParam("@Shipping_address2", address4.Text.Trim(), SqlDbType.VarChar);
        _Sparray[20] = new SpParam("@Shipping_phoneNo", phoneNo0.Text.Trim(), SqlDbType.VarChar);
        _Sparray[21] = new SpParam("@Shipping_mobileNo", mobileNo0.Text.Trim(), SqlDbType.VarChar);
        _Sparray[22] = new SpParam("@Shipping_zipCode", zipCode0.Text.Trim(), SqlDbType.VarChar);
        _Sparray[23] = new SpParam("@Shipping_faxNo", faxNo0.Text.Trim(), SqlDbType.VarChar);
        try
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {


                object _obj = conn.ExecuteScalarProc("[Lum_Erp_Stp_AddCustomer]", _Sparray);
                if (_obj != null)
                {
                    int _customerId = Convert.ToInt32(_obj);
                   
                    message.Text = "Customer  Added : " + customerName.Text;
                    message.ForeColor = Color.Green;
                    ResetControl();

                }

            }
        }
        catch (Exception ex)
        {
            message.Text = ex.Message;
        }

    }


    protected void rbstatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbstatus.SelectedValue == "0")
        {
            ddlCustomer.Visible  = false;
        }
        else
        {
            ddlCustomer.Enabled = true;
            BindListControl _control = new BindListControl();
            _control.BindCustomerAccount(ddlCustomer);
        }
    }
    public void ResetControl()
    {
        customerName.Text = string.Empty;
        address1.Text = string.Empty;
        address2.Text = string.Empty;
        zipCode.Text = string.Empty;
        phoneNo.Text = string.Empty;
        mobileNo.Text = string.Empty;
        address3.Text = string.Empty;
        address4.Text = string.Empty;
        zipCode0.Text = string.Empty;
        phoneNo0.Text = string.Empty;
        mobileNo0.Text = string.Empty;
        emailId.Text = string.Empty;
        webSite.Text = string.Empty;
        contactPersonName.Text = string.Empty;
        contactNo.Text = string.Empty;
        VATRegistrationNo.Text = string.Empty;
        CSTRegistrationNo.Text = string.Empty;
        faxNo.Text = string.Empty;
        faxNo0.Text = string.Empty;
        serviceTaxRegNo.Text = string.Empty;
        panNo.Text = string.Empty;
        ddlCustomerSegment.SelectedIndex=0;
        rbstatus.SelectedIndex = 0;
        ddlCustomer.SelectedIndex = 0;
        ddlCustomer.Visible  = false;
        add.Text = "Add";
        chk.Checked = false;
        customerId = -1;
      
     

    }
    private void LoadCustomer()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select * from Lum_Erp_Customer where customerId = " + customerId))
            {
                if (reader.Read())
                {
                    customerName.Text = Convert.ToString(reader["customerName"]);
                    address1.Text = Convert.ToString(reader["Billing_address1"]);
                    address2.Text = Convert.ToString(reader["Billing_address2"]);
                    zipCode.Text = Convert.ToString(reader["Billing_zipCode"]);
                    phoneNo.Text = Convert.ToString(reader["Billing_phoneNo"]);
                    mobileNo.Text = Convert.ToString(reader["Billing_mobileNo"]);
                    emailId.Text = Convert.ToString(reader["emailId"]);
                    webSite.Text = Convert.ToString(reader["webSite"]);
                    contactPersonName.Text = Convert.ToString(reader["contactPersonName"]);
                    contactNo.Text = Convert.ToString(reader["contactNo"]);
                    VATRegistrationNo.Text = Convert.ToString(reader["VATRegistrationNo"]);
                    CSTRegistrationNo.Text = Convert.ToString(reader["CSTRegistrationNo"]);
                    faxNo.Text = Convert.ToString(reader["Billing_faxNo"]);
                    serviceTaxRegNo.Text = Convert.ToString(reader["serviceTaxRegNo"]);
                    panNo.Text = Convert.ToString(reader["panNo"]);
                    ddlCustomerSegment.SelectedValue = Convert.ToString(reader["customerSegmentId"]);
                    rbstatus.SelectedIndex = Convert.ToInt32(reader["acountType"]);
                 
                    address3.Text = Convert.ToString(reader["Shipping_address1"]);
                    address4.Text = Convert.ToString(reader["Shipping_address2"]);
                    zipCode0.Text = Convert.ToString(reader["Shipping_zipCode"]);
                    phoneNo0.Text = Convert.ToString(reader["Shipping_phoneNo"]);
                    mobileNo0.Text = Convert.ToString(reader["Shipping_mobileNo"]);
                    faxNo0.Text = Convert.ToString(reader["Shipping_faxNo"]);
                    if (Convert.ToInt32(reader["acountType"]) == 1)
                    {
                        ddlCustomer.Enabled = true;
                        BindListControl _control = new BindListControl();
                        _control.BindCustomerAccount(ddlCustomer);
                        ddlCustomer.SelectedValue = Convert.ToString(reader["acountId"]);
                    }
                    else
                    {
                        ddlCustomer.Enabled = false;
                    }
                    add.Text = "Update";
                    cancel.Visible = true;
                }
            }
        }
    }

    protected void cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Customer.aspx");
        //ResetControl();
    }
  

    
 


   
    private void BindGrid()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select * from Lum_Erp_Customer where isDeleted=0 order by customerName "))
            {
                customerGrid.DataSource = reader;
                customerGrid.DataBind();
            }
        }

    }
    protected void customerGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName.Equals("Edit"))
        {
            customerId = (int)customerGrid.DataKeys[e.Item.ItemIndex];
            LoadCustomer();
          
            add.Visible = true;
        }
        if (e.CommandName.Equals("view"))
        {
            customerId = (int)customerGrid.DataKeys[e.Item.ItemIndex];
            LoadCustomer();
          
            add.Visible = false;
        }
        else if (e.CommandName.Equals("Delete"))
        {
            add.Visible = true;
            customerId = (int)customerGrid.DataKeys[e.Item.ItemIndex];
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {


                using (IDataReader reader = conn.ExecuteQuery(@"SELECT COUNT(*) cn FROM Lum_Erp_Soir WHERE Customerid = " + customerId))
                {
                    if (reader.Read())
                    {
                        if (Convert.ToInt32(reader["cn"]) > 0)
                        {
                            message.Text = "Customer cannot be deleted because of existing related data";
                            message.ForeColor = Color.Red;
                            return;
                        }
                    }
                }
                try
                {
                    conn.ExecuteCommand("Update  Lum_Erp_Customer SET isDeleted=1 where customerId = " + customerId);
                    message.Text = "Customer flag is deleted ";
                    message.ForeColor = Color.Green;
                }
                catch
                {
                    message.Text = "Customer cannot be deleted because of existing related data";
                    message.ForeColor = Color.Red;
                    return;
                }

                customerId = -1;
            }
            BindGrid();
        }
    }
    protected void customerGrid_PreRender(object sender, System.EventArgs e)
    {
        for (int _index = 0; _index < customerGrid.Items.Count; _index++)
        {
            string str = customerGrid.Items[_index].Cells[1].Text;
            if (str == "0")
            {
                customerGrid.Items[_index].Cells[1].Text = "Main";
            }
            else
            {
                customerGrid.Items[_index].Cells[1].Text = "Branch";
            }
        }
    }
    protected void customerGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton btnview = (LinkButton)e.Item.Cells[2].Controls[0];
            btnview.CssClass = "GridLink";
            LinkButton btnedit = (LinkButton)e.Item.Cells[3].Controls[0];
            LinkButton btn = (LinkButton)e.Item.Cells[4].Controls[0];
            btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this?')");
            btn.CssClass = "GridLink";
            btnedit.CssClass = "GridLink";

            if (GetLoggedinUser().UserType == 2)
            {
                btn.Visible = false;
                btnedit.Visible = false;
            }

         

        }
    }
    protected void btnShowall_Click(object sender, EventArgs e)
    {
        BindGrid();
        ResetControl();
        txtSearch.Text = "";
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if(txtSearch.Text!=string.Empty)
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select * from Lum_Erp_Customer where customerName like '" + txtSearch.Text.Trim() + "%' order by customerName "))
            {
                customerGrid.DataSource = reader;
                customerGrid.DataBind();
            }
        }
        ResetControl();
        txtSearch.Text = "";
    }
    protected void chk_CheckedChanged(object sender, EventArgs e)
    {
        if (chk.Checked == true)
        {
            address3.Text = address1.Text;
            address4.Text = address2.Text;
            zipCode0.Text = zipCode.Text;
            phoneNo0.Text = phoneNo.Text;
            mobileNo0.Text = mobileNo.Text;
            faxNo0.Text = faxNo.Text;
        }
       
    }
}
