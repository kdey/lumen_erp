﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Drawing;
using Bitscrape.AppBlock.Database;
public partial class DataEntry_Sales :BasePage
{

    protected int salesPersonId = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        message.Text = "";
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!this.IsPostBack)
        {

            BindGrid();
         
        }
        else
        {
            // check if a division id has been passed
            try
            {
                salesPersonId = int.Parse(Request["salesPersonId"]);
               
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
                message.ForeColor = Color.Green;
            }
        }
        if (GetLoggedinUser().UserType == 2)
        {
            add.Visible  = false;

        }
    }

    private void BindGrid()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("SELECT * FROM Lum_Erp_SalesPerson order by firstName"))
            {
                salesPersonGrid.DataSource = reader;
                salesPersonGrid.DataBind();
            }
        }

    }

    protected void add_Click(object sender, EventArgs e)
    {
        if (salesPersonId == -1)
        {
            // we are in add mode
            if (initial.Text.Trim().Length > 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    conn.ExecuteCommandProc("Lum_Erp_Stp_AddSalesPerson", new SpParam("@initial", initial.Text.Trim(), SqlDbType.Char), new SpParam("@firstName", firstName.Text.Trim(), SqlDbType.VarChar), new SpParam("@lastName", lastName.Text.Trim(), SqlDbType.VarChar), new SpParam("@address", address.Text.Trim(), SqlDbType.VarChar), new SpParam("@phoneNo", phoneNo.Text.Trim(), SqlDbType.VarChar), new SpParam("@mobileNo", mobileNo.Text.Trim(), SqlDbType.VarChar), new SpParam("@zipCode", zipCode.Text.Trim(), SqlDbType.VarChar), new SpParam("@emailId", emailId.Text.Trim(), SqlDbType.VarChar));
                    message.Text = "Sales Person  Added : " + initial.Text+" "+firstName.Text+" "+lastName.Text;
                    message.ForeColor = Color.Green;
                    initial.Text = string.Empty;
                    firstName.Text = string.Empty;
                    lastName.Text = string.Empty;
                    address.Text = string.Empty;
                    phoneNo.Text = string.Empty;
                    mobileNo.Text = string.Empty;
                    zipCode.Text = string.Empty;
                    emailId.Text = string.Empty;

                }
            }
        }
        else
        {
            // we are in edit mode.
            if (initial.Text.Trim().Length > 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    conn.ExecuteCommand(string.Format(
                           "update Lum_Erp_SalesPerson set initial = '{0}',firstName='{1}',lastName='{2}',address='{3}',phoneNo='{4}',mobileNo='{5}',zipCode='{6}',emailId='{7}' where salesPersonId = {8}",
                           DataUtility.CleanString(initial.Text.Trim()), DataUtility.CleanString(firstName.Text.Trim()),
                           DataUtility.CleanString(lastName.Text.Trim()), DataUtility.CleanString(address.Text.Trim()),
                           DataUtility.CleanString(phoneNo.Text.Trim()), DataUtility.CleanString(mobileNo.Text.Trim()),
                           DataUtility.CleanString(zipCode.Text.Trim()), DataUtility.CleanString(emailId.Text.Trim()),
                           salesPersonId));
                    add.Text = "Add";
                    message.Text = "Sales Person  Changed to : " + initial.Text + " " + firstName.Text + " " + lastName.Text;
                    message.ForeColor = Color.Green;
                    initial.Text = string.Empty;
                    firstName.Text = string.Empty;
                    lastName.Text = string.Empty;
                    address.Text = string.Empty;
                    phoneNo.Text = string.Empty;
                    mobileNo.Text = string.Empty;
                    zipCode.Text = string.Empty;
                    emailId.Text = string.Empty;

                    salesPersonId = -1;

                }
            }

        }

        BindGrid();

        #region stored proc code
        //create proc [dbo].[AddDivision] @divisionName varchar(255)
        //as
        //begin
        //    if(not exists(select * from Division where divisionName = @divisionName))
        //        insert into Division(divisionName) values(@divisionName)
        //end
        #endregion
    }

    protected void salesPersonGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            if (e.CommandName.Equals("View"))
            {
                salesPersonId = (int)salesPersonGrid.DataKeys[e.Item.ItemIndex];
                LoadSalesPerson();
                add.Visible = false;
            }
            if (e.CommandName.Equals("Edit"))
            {
                salesPersonId = (int)salesPersonGrid.DataKeys[e.Item.ItemIndex];
                LoadSalesPerson();
                add.Visible = true;
            }
            else if (e.CommandName.Equals("Delete"))
            {
                add.Visible = true;

                salesPersonId = (int)salesPersonGrid.DataKeys[e.Item.ItemIndex];

                try
                {
                    conn.ExecuteCommand("delete from Lum_Erp_SalesPerson where salesPersonId = " + salesPersonId);
                    message.Text = "Sales Person  deleted";
                    message.ForeColor = Color.Green;
                }
                catch
                {
                    message.Text = "Sales Person cannot be deleted because of existing related data";
                    message.ForeColor = Color.Red;
                }
                initial.Text = string.Empty;
                firstName.Text = string.Empty;
                lastName.Text = string.Empty;
                address.Text = string.Empty;
                phoneNo.Text = string.Empty;
                mobileNo.Text = string.Empty;
                zipCode.Text = string.Empty;
                emailId.Text = string.Empty;
                salesPersonId = -1;

                BindGrid();
            }

        }
    }

    private void LoadSalesPerson()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select initial,firstName,lastName,address,phoneNo,mobileNo,zipCode,emailId from Lum_Erp_SalesPerson where salesPersonId = " + salesPersonId))
            {
                if (reader.Read())
                {


                    initial.Text = reader.GetString(0);
                    firstName.Text = reader.GetString(1);
                    lastName.Text = reader.GetString(2);
                    address.Text = Convert.ToString(reader["address"]);
                    phoneNo.Text = Convert.ToString(reader["phoneNo"]);
                    mobileNo.Text = Convert.ToString(reader["mobileNo"]);
                    zipCode.Text = Convert.ToString(reader["zipCode"]);
                    emailId.Text = Convert.ToString(reader["emailId"]);
                    add.Text = "Update";
                    cancel.Visible = true;
                }
            }
        }
    }

    protected void cancel_Click(object sender, EventArgs e)
    {
        initial.Text = string.Empty;
        firstName.Text = string.Empty;
        lastName.Text = string.Empty;
        address.Text = string.Empty;
        phoneNo.Text = string.Empty;
        mobileNo.Text = string.Empty;
        zipCode.Text = string.Empty;
        emailId.Text = string.Empty;
        salesPersonId = -1;
        add.Text = "Add";

    }
    protected void salesPersonGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton btnview = (LinkButton)e.Item.Cells[6].Controls[0];
            btnview.CssClass = "GridLink";
            LinkButton btnedit = (LinkButton)e.Item.Cells[7].Controls[0];
            LinkButton btn = (LinkButton)e.Item.Cells[8].Controls[0];
            btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this?')");
            btn.CssClass = "GridLink";
            btnedit.CssClass = "GridLink";
          
            salesPersonId = -1;
            add.Text = "Add";
            if (GetLoggedinUser().UserType == 2)
            {
                btn.Visible = false;
                btnedit.Visible = false;
            }

        }
    }
}
