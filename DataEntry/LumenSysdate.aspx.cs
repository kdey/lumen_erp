﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;

public partial class DataEntry_LumenSysdate : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        message.Text = "";
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (GetLoggedinUser().UserType == 2)
        {
            add.Visible = false;

        }
        loadtodaysdate();
    }
    private void loadtodaysdate()
    {
        try
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                using (IDataReader reader = conn.ExecuteQuery("select CONVERT(VARCHAR,LumenDate,106) AS Lumendate from  Lum_Erp_Sysdate WHERE CurrentSysdate = CONVERT(VARCHAR,DATEADD(MI,330,GETUTCDATE() ),106)"))
                {
                    if (reader.Read())
                    {
                        txttodaysdate.Text = reader["Lumendate"].ToString();
                    }


                }
            }
        }
        catch (Exception ex)
        {
            message.Text = ex.Message;
            message.ForeColor = Color.Red;
        }
    }


    protected void add_Click(object sender, EventArgs e)
    {
        DateTime _date = DateTime.Today;
        if (string.IsNullOrEmpty(txtnewsysdate.Text))
        {
            txtnewsysdate.Text = DateTime.Today.ToString("dd/MM/yyyy");
        }
        else
        {
            _date = DateTime.ParseExact(txtnewsysdate.Text.Trim(), "dd/MM/yyyy", null);
        }

        if (txtdtpassword.Text.Trim().Length > 0)
        {
            try
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    using (IDataReader reader = conn.ExecuteQueryProc("Lum_Stp_UpdateTodays",
                         new SpParam("@dtChangePass", txtdtpassword.Text.Trim(), SqlDbType.VarChar),
                         new SpParam("@LumenDate", _date, SqlDbType.SmallDateTime)))
                    {
                        if (reader.Read())
                        {
                            txttodaysdate.Text = reader["LumenDate"].ToString();
                            message.Text = "System date Changed  ";
                            message.ForeColor = Color.Green;
                            txtdtpassword.Text = string.Empty;
                            txtnewsysdate.Text = string.Empty;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
                message.ForeColor = Color.Red;
            }
        }
    }
    protected void cancel_Click(object sender, EventArgs e)
    {
        txtdtpassword.Text = "";
        txtnewsysdate.Text = "";
    }
}
