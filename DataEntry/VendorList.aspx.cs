﻿using System.Data;
using System;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;
public partial class DataEntry_VendorList : BasePage
{
    int vendorId = -1;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }

    }
    private void BindGrid()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select * from Vendor"))
            {
                vendorGrid.DataSource = reader;
                vendorGrid.DataBind();
            }
        }

    }
    protected void vendorGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName.Equals("Edit"))
        {
            vendorId = (int)vendorGrid.DataKeys[e.Item.ItemIndex];

            Response.Redirect("Vendor.aspx?vendorId=" + vendorId);
          
        }
        else if (e.CommandName.Equals("Delete"))
        {
            vendorId = (int)vendorGrid.DataKeys[e.Item.ItemIndex];
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                try
                {
                    conn.ExecuteCommand("delete from Vendor where vendorId = " + vendorId);
                    message.Text = "Vendor deleted";
                    message.ForeColor = Color.Green;
                }
                catch
                {
                    message.Text = "Vendor cannot be deleted because of existing related data";
                    message.ForeColor = Color.Red;
                }

                vendorId = -1;
            }
            BindGrid();
        }
    }
    protected void vendorGrid_PreRender(object sender, System.EventArgs e)
    {
        for (int _index = 0; _index < vendorGrid.Items.Count; _index++)
        {
            string str = vendorGrid.Items[_index].Cells[1].Text;
            if (str == "0")
            {
                vendorGrid.Items[_index].Cells[1].Text = "Main";
            }
            else
            {
                vendorGrid.Items[_index].Cells[1].Text = "Branch";
            }
        }
    }
    protected void vendorGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType ==ListItemType.AlternatingItem)
        {

            LinkButton btn = (LinkButton)e.Item.Cells[3].Controls[0];
            btn.Attributes.Add("onclick", "return confirm('are you sure you want to delete this')");
        }
    }
}
