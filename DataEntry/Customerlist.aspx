﻿<%@ Page Title="Customer List" Language="C#" MasterPageFile="~/DataEntry/DataEntry.master" AutoEventWireup="true" CodeFile="Customerlist.aspx.cs" Inherits="DataEntry_Customerlist" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <div class="PageMargin">
        <div class="PageHeading">
            Customer List</div>
        <div class="Line">
        </div>
        <!--Content Table Start-->
        <div class="PageContent" align="center">
         <br />
          <br />
         <asp:Label ID="message" runat="server" />
               <asp:DataGrid ID="customerGrid" runat="server" AutoGenerateColumns="false" DataKeyField="customerId"
            CellPadding="3" CellSpacing="3" OnItemCommand="customerGrid_ItemCommand" HeaderStyle-CssClass="GridHeading"
            ItemStyle-CssClass="GridData" onprerender="customerGrid_PreRender">
            <Columns>
                <asp:BoundColumn DataField="customerName" HeaderText="Customer Name" HeaderStyle-Font-Bold="true" />
                <asp:BoundColumn DataField="acountType" HeaderText="Account Type" HeaderStyle-Font-Bold="true" />
                <asp:ButtonColumn ButtonType="LinkButton" CommandName="Edit" Text="Edit" ItemStyle-CssClass="GridLink" />
                <asp:ButtonColumn ButtonType="LinkButton" CommandName="Delete" Text="Delete" ItemStyle-CssClass="GridLink" />
            </Columns>
        </asp:DataGrid>
            <br />
        </div>
        <!--Content Table End-->
        <div class="Line">
        </div>
        <!--Grid Table Start-->
     
        <!--Grid Table End-->
        <div class="Line">
        </div>
        <!--Page Body End-->
    </div>
</asp:Content>

