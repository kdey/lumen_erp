﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using Bitscrape.AppBlock.Database;

public partial class DataEntry_EducationCess : BasePage
{
    protected int educationCessId = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        message.Text = "";
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!this.IsPostBack)
        {
            BindGrid();
            
        }
        else
        {
            // check if a HigherEducationCess id has been passed
            try
            {
                educationCessId = int.Parse(Request["educationCessId"]);
               
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
                message.ForeColor = Color.Green;
            }
        }
        if (GetLoggedinUser().UserType == 2)
        {
            add.Visible  = false;
        }
    }
    private void BindGrid()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select * from Lum_Erp_EducationCess order by eduCess"))
            {
                educationCessGrid.DataSource = reader;
                educationCessGrid.DataBind();
            }
        }

    }

    protected void add_Click(object sender, EventArgs e)
    {
        if (educationCessId == -1)
        {
            // we are in add mode
            if (educationCess.Text.Trim().Length > 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    int i = conn.ExecuteCommandProc("Lum_Erp_Stp_AddEducationCess", new SpParam("@educationCses", educationCess.Text.Trim(), SqlDbType.VarChar));

                   if (i > 0)
                   {
                       message.Text = "EducationCess Added : " + educationCess.Text;
                       message.ForeColor = Color.Green;
                       educationCess.Text = string.Empty;
                   }
                   else
                   {
                       message.Text = "EducationCess Already Exists : " + educationCess.Text;
                       message.ForeColor = Color.Red;
                       educationCess.Text = string.Empty;
                   }
                }
            }
        }
        else
        {
            // we are in edit mode.
            if (educationCess.Text.Trim().Length > 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                  int i=  conn.ExecuteCommand(string.Format(
                           "if(not exists(select * from Lum_Erp_EducationCess where eduCess ='{0}' and educationCessId!={3}))update Lum_Erp_EducationCess set eduCess = '{1}' where educationCessId = {2}",
                           DataUtility.CleanString(educationCess.Text.Trim()), DataUtility.CleanString(educationCess.Text.Trim()), educationCessId, educationCessId));
                   
                    if (i > 0)
                    {
                        add.Text = "Add";
                        message.Text = "EducationCess Changed to : " + educationCess.Text;
                        message.ForeColor = Color.Green;
                        educationCess.Text = string.Empty;
                        educationCessId = -1;
                    }
                    else
                    {
                        message.Text = "EducationCess Already Exists : " + educationCess.Text;
                        message.ForeColor = Color.Red;

                    }

                }
            }

        }

        BindGrid();

        #region stored proc code
        //create proc [dbo].[AddHigherEducationCess] @educationCess varchar(255)
        //as
        //begin
        //    if(not exists(select * from HigherEducationCess where educationCess = @educationCess))
        //        insert into HigherEducationCess(educationCess) values(@educationCess)
        //end
        #endregion
    }

    protected void educationCessGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
           
                        if (e.CommandName.Equals("View"))
                        {
                            educationCessId = (int)educationCessGrid.DataKeys[e.Item.ItemIndex];
                            LoadHigherEducationCess();
                            add.Visible = false;
                        }
                        if (e.CommandName.Equals("Edit"))
                        {
                            educationCessId = (int)educationCessGrid.DataKeys[e.Item.ItemIndex];
                            LoadHigherEducationCess();
                            add.Visible = true;
                        }
                        else if (e.CommandName.Equals("Delete"))
                        {
                            educationCessId = (int)educationCessGrid.DataKeys[e.Item.ItemIndex];

                            try
                            {
                                conn.ExecuteCommand("delete from Lum_Erp_EducationCess where educationCessId = " + educationCessId);
                                message.Text = "EducationCess deleted";
                                message.ForeColor = Color.Green;
                            }
                            catch
                            {
                                message.Text = "EducationCess cannot be deleted because of existing related data";
                                message.ForeColor = Color.Red;
                            }
                            educationCess.Text = string.Empty;
                            educationCessId = -1;

                            BindGrid();
                        }
                   
              
           
        }
    }

    private void LoadHigherEducationCess()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select eduCess from Lum_Erp_EducationCess where educationCessId = " + educationCessId))
            {
                if (reader.Read())
                {
                    educationCess.Text = Convert.ToString(reader["eduCess"]);
                    add.Text = "Update";
                    cancel.Visible = true;
                }
            }
        }
    }

    protected void cancel_Click(object sender, EventArgs e)
    {
        educationCess.Text = "";
        add.Text = "Add";
        educationCessId = -1;
    }
    protected void educationCessGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton btnview = (LinkButton)e.Item.Cells[1].Controls[0];

            LinkButton btnedit = (LinkButton)e.Item.Cells[2].Controls[0];
            LinkButton btn = (LinkButton)e.Item.Cells[3].Controls[0];
            btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this?')");
            btnview.CssClass = "GridLink";
            btn.CssClass = "GridLink";
            btnedit.CssClass = "GridLink";
            educationCess.Text = "";
            add.Text = "Add";
            educationCessId = -1;

            if (GetLoggedinUser().UserType == 2)
            {
                btn.Visible = false;
                btnedit.Visible = false;
            }
        }
    }
}
