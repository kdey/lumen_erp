﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

using Bitscrape.AppBlock.Database;

public partial class DataEntry_DataEntry : System.Web.UI.MasterPage
{
    public string MenuJS = string.Empty;

    public int Level1 = 0;
    public int Level2 = 0;
    public int Level3 = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        LoadMenu();
        if (Level2 == 1)
        {
            MenuJS = MenuJS + string.Format(@"
                </ul></li>");
        }
        h1.InnerHtml = MenuJS;
    }
    public void LoadMenu()
    {
        int parent = 0;
        int oldparent = 0;
        int start = 0;
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            try
            {
                using (DataSet ds = conn.GetDataSet("Lum_Erp_stp_GetMenu", "table"))
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        start = 1;
                        if (ds.Tables[0].Rows[i]["url"] == DBNull.Value)
                        {
                            if (Level2 == 1)
                            {
                                MenuJS = MenuJS + string.Format(@"
                                        </ul></li>");
                                Level2 = 0;
                                Level3 = 0;
                            }
                            if (ds.Tables[0].Rows[i]["menuname"].ToString().Replace("'", "´").Length <= 4)
                            {
                                MenuJS = MenuJS + string.Format(@"
                                        <li class='Small'><a href='javascript:%20null(0)'><strong>" + ds.Tables[0].Rows[i]["menuname"].ToString().Replace("'", "´") + @"</strong></a><ul>");
                            }
                            else
                            {
                                if (ds.Tables[0].Rows[i]["menuname"].ToString().Replace("'", "´").Length <= 8)
                                {
                                    MenuJS = MenuJS + string.Format(@"
                                            <li class='Medium'><a href='javascript:%20null(0)'><strong>" + ds.Tables[0].Rows[i]["menuname"].ToString().Replace("'", "´") + @"</strong></a><ul>");
                                }
                                else
                                {
                                    if (ds.Tables[0].Rows[i]["menuname"].ToString().Replace("'", "´").Length <= 12)
                                    {
                                        MenuJS = MenuJS + string.Format(@"
                                                <li class='Large'><a href='javascript:%20null(0)'><strong>" + ds.Tables[0].Rows[i]["menuname"].ToString().Replace("'", "´") + @"</strong></a><ul>");
                                    }
                                    else
                                    {
                                        MenuJS = MenuJS + string.Format(@"
                                                <li class='Xlarge'><a href='javascript:%20null(0)'><strong>" + ds.Tables[0].Rows[i]["menuname"].ToString().Replace("'", "´") + @"</strong></a><ul>");
                                    }
                                }
                            }
                            Level1 = 1;
                        }
                        else
                        {
                            parent = Convert.ToInt32(ds.Tables[0].Rows[i]["ParentId"]);
                            if ((start == 1) && (oldparent != parent))
                            {
                                if (Level2 == 1)
                                {
                                    MenuJS = MenuJS + string.Format(@"
                                        </ul></li>");
                                    Level2 = 0;
                                    Level3 = 0;
                                }
                            }
                            string MenuChanged = string.Empty;
                            if (ds.Tables[0].Rows[i]["Url"].ToString().Contains("DataEntry/"))
                            {
                                MenuChanged = ds.Tables[0].Rows[i]["Url"].ToString().Replace("DataEntry/", "");
                            }
                            else
                            {
                                MenuChanged = "../" + ds.Tables[0].Rows[i]["Url"].ToString();
                            }

                            MenuJS = MenuJS + string.Format(@"
                                <li><a href='" + MenuChanged + @"' style='border-top: 1px dotted #993366;'>" + ds.Tables[0].Rows[i]["menuname"].ToString().Replace("'", "´") + @"</a></li>");
                            Level2 = 1;

                            oldparent = Convert.ToInt32(ds.Tables[0].Rows[i]["ParentId"]);
                            start = 1;

                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Page.RegisterClientScriptBlock("message", "<script>alert('" + ex.Message + ".');</script>");
            }
        }
    }
}