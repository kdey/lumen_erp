﻿<%@ Page Language="C#" MasterPageFile="~/DataEntry/DataEntry.master" AutoEventWireup="true"
    CodeFile="CustomerSegment.aspx.cs" Inherits="DataEntry_CustomerSegment" Title="Customer Segment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" name="segmentId" value="<%=segmentId%>" />
    <div class="PageMargin">
        <div class="PageHeading">Customer Segment</div>
        <div class="Line"></div>
        <!--Content Table Start-->
        <div class="PageContent">
        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
        <tr><td align="right" valign="middle" width="20%">Customer Segment type :&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="20%" align="left" valign="middle">
                <asp:TextBox ID="segmentType" runat="server" CssClass="TextboxSmall" MaxLength="50" />
            </td>
            <td align="right" valign="middle" width="20%">Customer Segment Description&nbsp;&nbsp; :</td>
            <td width="20%" align="left" valign="middle">
                <asp:TextBox ID="segmentDescription" runat="server" Height="40" 
                    onkeypress="return limiter(this.id);" CssClass="TextAreaSmall" />
            </td>
            <td align="center" valign="middle" width="20%">
                <asp:Button ID="add" Text="Add" runat="server" OnClick="add_Click" CssClass="Button" />
                &nbsp;&nbsp;
                <asp:Button ID="cancel" Text="Cancel" runat="server" OnClick="cancel_Click" CausesValidation="false"
                    CssClass="Button" />
            </td>
        </tr>
        <tr><td align="left" valign="middle"><asp:Label ID="message" runat="server" /></td>
            <td align="left" valign="middle"><asp:RequiredFieldValidator ControlToValidate="segmentType" ErrorMessage="Field cannot be empty"
                    ID="nameValidator" runat="server" /></td>
            <td colspan="3">&nbsp;</td>
        </tr></table>
        </div>
        <!--Content Table End-->
        <div class="Line">
        </div>
        <!--Grid Table Start-->
        <asp:DataGrid ID="segmentGrid" runat="server" AutoGenerateColumns="false" DataKeyField="segmentId"
            CellPadding="0" OnItemCommand="segmentGrid_ItemCommand" HeaderStyle-CssClass="GridHeading"
            ItemStyle-CssClass="GridData" onitemcreated="segmentGrid_ItemCreated" 
            GridLines="None" Width="100%">
            <ItemStyle CssClass="GridData"></ItemStyle>
            <Columns>
                <asp:BoundColumn DataField="segmentType" HeaderText="Customer Segment Type" HeaderStyle-HorizontalAlign="left"
                    HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Left">
                <HeaderStyle Font-Bold="True"></HeaderStyle>
                </asp:BoundColumn>
                  <asp:BoundColumn DataField="segmentDescription" HeaderStyle-HorizontalAlign="left" 
                    HeaderText="Customer Segment Description" HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Left">
                <HeaderStyle Font-Bold="True"></HeaderStyle>
                </asp:BoundColumn>
                 <asp:ButtonColumn  ButtonType="LinkButton" CausesValidation="false" 
                    CommandName="View" Text="View" 
                 ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="GridLink" 
                    ItemStyle-Width="10%" >
<ItemStyle HorizontalAlign="Left" CssClass="GridLink" Width="10%"></ItemStyle>
                </asp:ButtonColumn>
                <asp:ButtonColumn ButtonType="LinkButton" CommandName="Edit" Text="Edit" ItemStyle-Width="10%" 
                    ItemStyle-CssClass="GridLink" ItemStyle-HorizontalAlign="Left">
                <ItemStyle CssClass="GridLink"></ItemStyle>
                </asp:ButtonColumn>
                <asp:ButtonColumn ButtonType="LinkButton" CommandName="Delete" Text="Delete" ItemStyle-Width="10%" 
                    ItemStyle-CssClass="GridLink" ItemStyle-HorizontalAlign="Left">
                <ItemStyle CssClass="GridLink"></ItemStyle>
                </asp:ButtonColumn>
            </Columns>

<HeaderStyle CssClass="GridHeading"></HeaderStyle>
        </asp:DataGrid>
        <!--Grid Table End-->
        <div class="Line">
        </div>
        <!--Page Body End-->
    </div>
</asp:Content>
