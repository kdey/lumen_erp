﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using Bitscrape.AppBlock.Database;
public partial class DataEntry_CustomerSegment : BasePage
{

    protected int segmentId = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        message.Text = "";
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!this.IsPostBack)
        {
            BindGrid();
           
        }
        else
        {
            // check if a division id has been passed
            try
            {
                segmentId = int.Parse(Request["segmentId"]);
                
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
                message.ForeColor = Color.Green;
            }
        }
        if (GetLoggedinUser().UserType == 2)
        {
            add.Visible  = false;
           
        }
    }
    private void BindGrid()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select * from Lum_Erp_CustomerSegment order by segmentType"))
            {
                segmentGrid.DataSource = reader;
                segmentGrid.DataBind();
            }
        }

    }

    protected void add_Click(object sender, EventArgs e)
    {
        if (segmentId == -1)
        {
            // we are in add mode
            if (segmentType.Text.Trim().Length > 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    int i = conn.ExecuteCommandProc("Lum_Erp_Stp_AddCustomerSegment", new SpParam("@segmentType", segmentType.Text.Trim(), SqlDbType.VarChar), new SpParam("@segmentDescription", segmentDescription.Text.Trim(), SqlDbType.VarChar));
                  if (i > 0)
                  {
                      message.Text = "Customer Segment Added : " + segmentType.Text;
                      message.ForeColor = Color.Green;
                      segmentType.Text = string.Empty;
                      segmentDescription.Text = string.Empty;
                  }
                  else
                  {
                      message.Text = "Customer Segment Already Exists: " + segmentType.Text;
                      message.ForeColor = Color.Red;
                      segmentType.Text = string.Empty;
                      segmentDescription.Text = string.Empty;
                  }
                }
            }
        }
        else
        {
            // we are in edit mode.
            if (segmentType.Text.Trim().Length > 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                   int i= conn.ExecuteCommand(string.Format(
                           "if(not exists(select * from Lum_Erp_CustomerSegment where segmentType = '{0}' and segmentId!={4}))update Lum_Erp_CustomerSegment set segmentType = '{1}',segmentDescription='{2}' where segmentId = {3}",
                           DataUtility.CleanString(segmentType.Text.Trim()), DataUtility.CleanString(segmentType.Text.Trim()), DataUtility.CleanString(segmentDescription.Text.Trim()), segmentId, segmentId));
                   if (i > 0)
                   {
                       add.Text = "Add";
                       message.Text = "Customer Segment Changed to : " + segmentType.Text;
                       message.ForeColor = Color.Green;
                       segmentType.Text = string.Empty;
                       segmentDescription.Text = string.Empty;
                       segmentId = -1;
                   }
                   else
                   {
                       message.Text = "Customer Segment Already Exists: " + segmentType.Text;
                       message.ForeColor = Color.Red;
                       segmentType.Text = string.Empty;
                       segmentDescription.Text = string.Empty;
                   }

                }
            }

        }

        BindGrid();

        #region stored proc code
        //create proc [dbo].[AddDivision] @divisionName varchar(255)
        //as
        //begin
        //    if(not exists(select * from Division where divisionName = @divisionName))
        //        insert into Division(divisionName) values(@divisionName)
        //end
        #endregion
    }

    protected void segmentGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
          
                        if (e.CommandName.Equals("View"))
                        {
                            segmentId = (int)segmentGrid.DataKeys[e.Item.ItemIndex];
                            LoadSegment();
                            add.Visible = false;
                        }
                        if (e.CommandName.Equals("Edit"))
                        {
                            segmentId = (int)segmentGrid.DataKeys[e.Item.ItemIndex];
                            LoadSegment();
                            add.Visible = true;
                        }
                        else if (e.CommandName.Equals("Delete"))
                        {
                            segmentId = (int)segmentGrid.DataKeys[e.Item.ItemIndex];

                            try
                            {
                                conn.ExecuteCommand("delete from Lum_Erp_CustomerSegment where segmentId = " + segmentId);
                                message.Text = "Customer Segment deleted";
                                message.ForeColor = Color.Green;
                            }
                            catch
                            {
                                message.Text = "Customer Segment  cannot be deleted because of existing related data";
                                message.ForeColor = Color.Red;
                            }
                            segmentType.Text = string.Empty;
                            segmentId = -1;

                            BindGrid();
                        }
                   
                
            
        }
    }

    private void LoadSegment()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select segmentType,segmentDescription from Lum_Erp_CustomerSegment where segmentId = " + segmentId))
            {
                if (reader.Read())
                {
                    segmentType.Text = reader.GetString(0);
                    segmentDescription.Text = reader.GetString(1);
                    add.Text = "Update";
                    cancel.Visible = true;
                    add.Visible = true;
                }
            }
        }
    }

    protected void cancel_Click(object sender, EventArgs e)
    {
        segmentType.Text = "";
        segmentDescription.Text = string.Empty;
        add.Text = "Add";
        segmentId = -1;
    }
    protected void segmentGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            
            LinkButton btnview = (LinkButton)e.Item.Cells[2].Controls[0];

            LinkButton btnedit = (LinkButton)e.Item.Cells[3].Controls[0];
            LinkButton btn = (LinkButton)e.Item.Cells[4].Controls[0];
            btn.CssClass = "GridLink";
            btnview.CssClass = "GridLink";
            btnedit.CssClass = "GridLink";
            segmentType.Text = "";
            segmentDescription.Text = string.Empty;
            add.Text = "Add";
            segmentId = -1;
            btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this?')");

            if (GetLoggedinUser().UserType == 2)
            {
                btn.Visible = false;
                btnedit.Visible = false;
            }
          
        }
    }
}
