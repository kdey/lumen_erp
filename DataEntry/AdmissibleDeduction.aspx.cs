﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using Bitscrape.AppBlock.Database;

public partial class DataEntry_AdmissableDeduction : BasePage
{
    protected int deductionId = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        message.Text = "";
        if (!IsLoggedIn())
        {
           
           Response.Redirect("./../Login.aspx");
        }

       
        if (!this.IsPostBack)
        {
            BindGrid();
           
            
           
        }
        else
        {
            // check if a division id has been passed
            try
            {
                deductionId = int.Parse(Request["deductionId"]);
                
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
                message.ForeColor = Color.Green;
            }
            add.Visible = true;
        }
        if (GetLoggedinUser().UserType == 2)
        {
            add.Visible  = false;
        }
    }
    private void BindGrid()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select * from Lum_Erp_AdmissibleDeduction order by deductionType"))
            {
                deductionGrid.DataSource = reader;
                deductionGrid.DataBind();
            }
        }

    }

    protected void add_Click(object sender, EventArgs e)
    {
        if (deductionId == -1)
        {
            // we are in add mode
            if (deductionType.Text.Trim().Length > 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    int i=conn.ExecuteCommandProc("Lum_Erp_Stp_AddDeduction", new SpParam("@deductionType", deductionType.Text.Trim(), SqlDbType.VarChar));
                    if (i > 0)
                    {
                        message.Text = "Admissible Deduction Type Added : " + deductionType.Text;
                        message.ForeColor = Color.Green;
                        deductionType.Text = string.Empty;
                    }
                    else
                    {
                        message.Text = "Admissible Deduction Type Already Exists : " + deductionType.Text;
                        message.ForeColor = Color.Red;
                        deductionType.Text = string.Empty;
                    }
                }
            }
        }
        else
        {
            // we are in edit mode.
            if (deductionType.Text.Trim().Length > 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                  int i=  conn.ExecuteCommand(string.Format(
                           "if(not exists(select * from Lum_Erp_AdmissibleDeduction where deductionType = '{0}')) update Lum_Erp_AdmissibleDeduction set deductionType = '{1}' where deductionId = {2}",
                           DataUtility.CleanString(deductionType.Text.Trim()), DataUtility.CleanString(deductionType.Text.Trim()), deductionId));
                  if (i > 0)
                  {
                      add.Text = "Add";
                      message.Text = "Admissible Deduction Type Changed to : " + deductionType.Text;
                      message.ForeColor = Color.Green;
                      deductionType.Text = string.Empty;
                      deductionId = -1;
                  }
                  else
                  {
                      message.Text = "Admissible Deduction Type Already Exists : " + deductionType.Text;
                      message.ForeColor = Color.Red;
                  }

                }
            }

        }

        BindGrid();

        #region stored proc code
        //create proc [dbo].[AddDivision] @divisionName varchar(255)
        //as
        //begin
        //    if(not exists(select * from Division where divisionName = @divisionName))
        //        insert into Division(divisionName) values(@divisionName)
        //end
        #endregion
    }

    protected void deductionGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            //using (IDataReader reader = conn.ExecuteQuery("select * from Lum_Erp_AdminUser where userName= 'demo'"))
            //{
            //    while (reader.Read())
            //    {
            //        AdminUser s = GetLoggedinUser();
            //        int a = s.UserId;
            //        if (a != Convert.ToInt32(reader["adminuserId"]))
            //        {
                        if (e.CommandName.Equals("Edit"))
                        {
                            deductionId = (int)deductionGrid.DataKeys[e.Item.ItemIndex];
                            LoadDeduction();
                            add.Visible = true;
                        }
                        if (e.CommandName.Equals("View"))
                        {
                            add.Visible = false;
                            deductionId = (int)deductionGrid.DataKeys[e.Item.ItemIndex];
                            LoadDeduction();
                        }
                        else if (e.CommandName.Equals("Delete"))
                        {
                            deductionId = (int)deductionGrid.DataKeys[e.Item.ItemIndex];

                            try
                            {
                                conn.ExecuteCommand("delete from Lum_Erp_AdmissibleDeduction where deductionId = " + deductionId);
                                message.Text = "Division deleted";
                                message.ForeColor = Color.Green;
                                Response.Redirect("AdmissibleDeduction.aspx");
                            }
                            catch(Exception ex)
                            {
                                message.Text = ex.ToString(); //"Admissible Deduction Type cannot be deleted because of existing related data";
                                message.ForeColor = Color.Red;
                            }
                            deductionType.Text = string.Empty;
                            deductionId = -1;

                            BindGrid();
                        }
                    }
        //        }
        //    }
        //}
    }

    private void LoadDeduction()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select deductionType from Lum_Erp_AdmissibleDeduction where deductionId = " + deductionId))
            {
                if (reader.Read())
                {
                    deductionType.Text = reader.GetString(0);
                    add.Text = "Update";
                    cancel.Visible = true;
                }
            }
        }
    }

    protected void cancel_Click(object sender, EventArgs e)
    {
        deductionType.Text = "";
        add.Text = "Add";
        deductionId = -1;
    }
    protected void deductionGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
         if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton btnview = (LinkButton)e.Item.Cells[1].Controls[0];
                LinkButton btnedit = (LinkButton)e.Item.Cells[2].Controls[0];
                LinkButton btn = (LinkButton)e.Item.Cells[3].Controls[0];
                btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this?')");
                btn.CssClass = "GridLink";
                btnedit.CssClass = "GridLink";
                btnview.CssClass = "GridLink";
                deductionType.Text = "";
                add.Text = "Add";
                deductionId = -1;
                if (GetLoggedinUser().UserType ==2)
                {
                    btn.Visible = false;
                    btnedit.Visible = false;
                }
            }
        
    }
}
