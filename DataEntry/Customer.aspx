<%@ Page Title="Customer Master" Language="C#" MasterPageFile="~/DataEntry/DataEntry.master" AutoEventWireup="true" CodeFile="Customer.aspx.cs" Inherits="DataEntry_Customer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   
    <style type="text/css">
        .style1
        {
            font-size: medium;
        }
    </style>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <input type="hidden" name="customerId" value="<%=customerId%>" />
    <div class="PageMargin">
        <div class="PageHeading">Customer <%--  <asp:RequiredFieldValidator ControlToValidate="zipCode" ErrorMessage="Field cannot be empty"
                    ID="RequiredFieldValidator11" runat="server" />--%> </div>
        <div class="Line"></div>
        <!--Content Table Start-->
        <div class="PageContent">
        <table width="100%" border="0"  cellpadding="5" cellspacing="0" 
                class="NormalText">
        <tr><td width="16%"  valign="middle">Customer Name&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="18%"  valign="middle">
                <asp:TextBox ID="customerName" runat="server" CssClass="TextboxSmall" 
                    MaxLength="100" TabIndex ="1" Width="100%"/>
            </td>
            <td width="16%"  valign="middle">Customer Segment&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="17%"  valign="middle">
                <asp:DropDownList CssClass="DropdownSmall" ID="ddlCustomerSegment"
                 runat="server" Width="100%" TabIndex="1"></asp:DropDownList>
            </td>
            <td width="16%"  valign="middle">Contact Person Name</td>
            <td width="17%"  valign="middle">
                <asp:TextBox ID="contactPersonName" runat="server" CssClass="TextboxSmall" 
                    MaxLength="50" Width="100%" TabIndex="2" />
                <%-- <asp:RequiredFieldValidator ControlToValidate="VATRegistrationNo" ErrorMessage="Field cannot be empty"
                ID="RequiredFieldValidator8" runat="server" />--%>
            </td>
        </tr>
        <tr><td>&nbsp;</td>
            <td><asp:RequiredFieldValidator ControlToValidate="customerName" ErrorMessage="Field cannot be empty"
                    ID="RequiredFieldValidator1" runat="server" /></td>
            <td>&nbsp;</td>
            <td><asp:RequiredFieldValidator ControlToValidate="ddlCustomerSegment" InitialValue="0" ErrorMessage="Please Select Customer Segment"
                    ID="RequiredFieldValidator2" runat="server" /></td>
            <td colspan="2">&nbsp;</td>
        </tr>
            <tr>
                <td   valign="middle">WebSite</td>
            <td   valign="middle">
                <asp:TextBox ID="webSite" runat="server" CssClass="TextboxSmall" MaxLength="50" 
                    Width="100%" TabIndex="3" />
            </td>
            <td   valign="middle">Email Id</td>
            <td   valign="middle">
                <asp:TextBox ID="emailId" runat="server" CssClass="TextboxSmall" 
                    MaxLength="50"  Width="100%" TabIndex="4"/>
            </td>
            <td   valign="middle">Contact No</td>
            <td   valign="middle">
                <asp:TextBox ID="contactNo" runat="server" CssClass="TextboxSmall" 
                    MaxLength="50" Width="100%" TabIndex="5" />
                <%--<asp:RequiredFieldValidator ControlToValidate="zipCode" ErrorMessage="Field cannot be empty"
                        ID="RequiredFieldValidator9" runat="server" />--%>
            </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            <td>Ex:http://www.abc.com<br />
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                    ErrorMessage="Invalid URL" ControlToValidate="webSite" 
                    ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?"></asp:RegularExpressionValidator></td>
            <td>&nbsp;</td>
            <td>Ex:abc@abc.com<br />
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                    ErrorMessage="Invalid Email id" ControlToValidate="emailId" 
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></td>
            <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td   valign="middle">VAT Registration No</td>
            <td   valign="middle">
                <asp:TextBox ID="VATRegistrationNo" runat="server" CssClass="TextboxSmall" 
                    MaxLength="50" Width="100%" TabIndex="6" />
                <%-- <asp:RequiredFieldValidator ControlToValidate="zipCode" ErrorMessage="Field cannot be empty"
                        ID="RequiredFieldValidator10" runat="server" />--%>
            </td>
            <td   valign="middle">CST Registration No</td>
            <td   valign="middle">
                <asp:TextBox ID="CSTRegistrationNo" runat="server" CssClass="TextboxSmall" 
                    MaxLength="50"  Width="100%" TabIndex="7"/>
                <%--  <asp:RequiredFieldValidator ControlToValidate="zipCode" ErrorMessage="Field cannot be empty"
                    ID="RequiredFieldValidator11" runat="server" />--%>
            </td>
            <td   valign="middle">ST Registration No</td>
            <td   valign="middle">
                <asp:TextBox ID="serviceTaxRegNo" runat="server" CssClass="TextboxSmall" 
                    MaxLength="50"  Width="100%" TabIndex="8"/>
                <%-- <asp:DataGrid ID="storelocationGrid" runat="server" AutoGenerateColumns="false" DataKeyField="storeId"
            CellPadding="3" CellSpacing="3" 
            OnItemCommand="storelocationGrid_ItemCommand" HeaderStyle-CssClass="GridHeading"
            ItemStyle-CssClass="GridData" 
             >
            <Columns>
             <asp:BoundColumn DataField="storCode" HeaderText="Division" HeaderStyle-Font-Bold="true" />
                <asp:BoundColumn DataField="storeName" HeaderText="Sub Division" HeaderStyle-Font-Bold="true" />
                <asp:ButtonColumn  ButtonType="LinkButton" CommandName="Edit" Text="Edit" ItemStyle-CssClass="GridLink" />
                <asp:ButtonColumn ButtonType="LinkButton"  CommandName="Delete" Text="Delete" ItemStyle-CssClass="GridLink" />
            </Columns>
        </asp:DataGrid>--%>
            </td>
            </tr>
            <tr>
                <td colspan="6">&nbsp;</td>
            </tr>
            <tr>
                <td   valign="middle">PAN No</td>
            <td   valign="middle">
                <asp:TextBox ID="panNo" runat="server" CssClass="TextboxSmall" TabIndex="9"  MaxLength="50" Width="100%"/>
                <%-- <asp:RequiredFieldValidator ControlToValidate="contactNo" ErrorMessage="Field cannot be empty"
                    ID="RequiredFieldValidator7" runat="server" />--%>
            </td>
            <td   valign="middle">Acount Status</td>
            <td   valign="middle">
                <asp:RadioButtonList ID="rbstatus" AutoPostBack="true" runat="server" RepeatDirection="Horizontal" 
                    onselectedindexchanged="rbstatus_SelectedIndexChanged">
                    <asp:ListItem Value="0" Selected="True">Main</asp:ListItem>
                    <asp:ListItem Value="1">Branch</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td><asp:DropDownList CssClass="DropdownSmall" ID="ddlCustomer" Enabled="false"  runat="server" Width="154px">
                    <asp:ListItem Text="--SELECT--"></asp:ListItem>
               </asp:DropDownList>
            </td>
            <td  valign="middle">&nbsp;</td>
            </tr>
        <tr><td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
       <tr align="center">
       <td align="center" colspan="6">
        <table class="NormalText"   >
        <tr><td   valign="middle">&nbsp;</td>
            <td   valign="middle" class="style1">
                Billing Address</td>    <td   valign="middle">&nbsp;</td>
            <td   valign="middle">
               </td>
            <td   valign="middle">
                &nbsp;</td>
            <td   valign="middle">&nbsp;</td>
            <td   valign="middle" class="style1">
                Installation Address</td>
        </tr>
        <tr><td   valign="middle">&nbsp;</td>
            <td   valign="middle">
                &nbsp;</td>
            <td   valign="middle">&nbsp;</td>
            <td   valign="middle">
                &nbsp;</td>
            <td   valign="middle" colspan="3">
                <strong>
            <asp:CheckBox ID="chk" runat="server" AutoPostBack="True" 
                oncheckedchanged="chk_CheckedChanged" />
                Is Installation address same as Billing address</strong></td>
        </tr>
        <tr><td   valign="middle">&nbsp;</td>
            <td   valign="middle">
                &nbsp;</td>
            <td   valign="middle">&nbsp;</td>
            <td   valign="middle">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td   valign="middle">
                &nbsp;</td>
            <td   valign="middle">&nbsp;</td>
            <td   valign="middle">
                &nbsp;</td>
        </tr>
        <tr><td   valign="middle">&nbsp;</td>
            <td   valign="middle">
                &nbsp;</td>
            <td   valign="middle">&nbsp;</td>
            <td   valign="middle">
                &nbsp;</td>
            <td   valign="middle">
                &nbsp;</td>
            <td   valign="middle">&nbsp;</td>
            <td   valign="middle">
                &nbsp;</td>
        </tr>
        <tr><td   valign="middle">Address1&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td   valign="middle">
                <asp:TextBox ID="address1" runat="server" TabIndex="10" onkeypress="return limiter(this.id);" 
                    TextMode="MultiLine" CssClass="TextAreaSmall" />
            </td>
            <td   valign="middle">&nbsp;</td>
            <td   valign="middle">
                &nbsp;</td>
            <td   valign="middle">
                &nbsp;</td>
            <td   valign="middle">Address1&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td   valign="middle">
                <asp:TextBox ID="address3" runat="server" onkeypress="return limiter(this.id);" 
                    TextMode="MultiLine" CssClass="TextAreaSmall" TabIndex="16" />
                 </td>
        </tr>
        <tr><td   valign="middle">&nbsp;</td>
            <td   valign="middle">
                <asp:RequiredFieldValidator ControlToValidate="address1" ErrorMessage="Field cannot be empty"
                    ID="RequiredFieldValidator4" runat="server" /></td>
            <td   valign="middle">&nbsp;</td>
            <td   valign="middle">
                &nbsp;</td>
            <td   valign="middle">
                &nbsp;</td>
            <td   valign="middle">&nbsp;</td>
            <td   valign="middle">
                <asp:RequiredFieldValidator ControlToValidate="address3" ErrorMessage="Field cannot be empty"
                    ID="RequiredFieldValidator5" runat="server" /></td>
        </tr>
        <tr><td   valign="middle">Address2</td>
            <td   valign="middle">
                <asp:TextBox ID="address2" runat="server" onkeypress="return limiter(this.id);" 
                    TextMode="MultiLine" CssClass="TextAreaSmall" TabIndex="11" />
            </td>
            <td   valign="middle">&nbsp;</td>
            <td   valign="middle">
                &nbsp;</td>
            <td   valign="middle">
                &nbsp;</td>
            <td   valign="middle">Address2</td>
            <td   valign="middle">
                <asp:TextBox ID="address4" runat="server" onkeypress="return limiter(this.id);" 
                    TextMode="MultiLine" CssClass="TextAreaSmall" TabIndex="17" />
            </td>
        </tr>
        <tr><td   valign="middle">Zip Code&nbsp;&nbsp;</td>
            <td   valign="middle">
                <asp:TextBox ID="zipCode" runat="server" CssClass="TextboxSmall" MaxLength="50" 
                    TabIndex="12" />
                 <br />
                   <asp:RegularExpressionValidator ID="quanityRegexValidator" runat="server"
                    Display="Dynamic"  ErrorMessage="Zip Code must be a integer"
                    Text="" ControlToValidate="zipCode" ValidationExpression="\d+" />
            </td>
            <td   valign="middle">&nbsp;</td>
            <td   valign="middle">
                &nbsp;</td>
            <td   valign="middle">
                &nbsp;</td>
            <td   valign="middle">Zip Code&nbsp;</td>
            <td   valign="middle">
                <asp:TextBox ID="zipCode0" runat="server" CssClass="TextboxSmall" 
                    MaxLength="50" TabIndex="18" />
                 <br />
                   <asp:RegularExpressionValidator ID="quanityRegexValidator0" runat="server"
                    Display="Dynamic"  ErrorMessage="Zip Code must be a integer"
                    Text="" ControlToValidate="zipCode0" ValidationExpression="\d+" />
                 </td>
        </tr>

        <tr><td   valign="middle">Phone No&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td   valign="middle">
                <asp:TextBox ID="phoneNo" runat="server" CssClass="TextboxSmall" MaxLength="50" 
                    TabIndex="13" />
            </td>
            <td   valign="middle">&nbsp;</td>
            <td   valign="middle">
                &nbsp;</td>
            <td   valign="middle">
                &nbsp;</td>
            <td   valign="middle">Phone No&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td   valign="middle">
                <asp:TextBox ID="phoneNo0" runat="server" CssClass="TextboxSmall" 
                    MaxLength="50" TabIndex="19" />
            </td>
        </tr>
        <tr><td   valign="middle">&nbsp;</td>
            <td   valign="middle">
                <asp:RequiredFieldValidator ControlToValidate="phoneNo" ErrorMessage="Field cannot be empty"
                    ID="RequiredFieldValidator3" runat="server" />
            </td>
            <td   valign="middle">&nbsp;</td>
            <td   valign="middle">
                &nbsp;</td>
            <td   valign="middle">
                &nbsp;</td>
            <td   valign="middle">&nbsp;</td>
            <td   valign="middle">
                <asp:RequiredFieldValidator ControlToValidate="phoneNo0" ErrorMessage="Field cannot be empty"
                    ID="RequiredFieldValidator6" runat="server" />
            </td>
        </tr>
        <tr><td   valign="middle">Mobile No</td>
            <td   valign="middle">
                <asp:TextBox ID="mobileNo" runat="server" CssClass="TextboxSmall" 
                    MaxLength="50" TabIndex="14" />
            </td>
            <td   valign="middle">&nbsp;</td>
            <td   valign="middle">
                &nbsp;</td>
            <td   valign="middle">
                &nbsp;</td>
            <td   valign="middle">Mobile No</td>
            <td   valign="middle">
                <asp:TextBox ID="mobileNo0" runat="server" CssClass="TextboxSmall" 
                    MaxLength="50" TabIndex="20" />
            </td>
        </tr>
        <tr><td   valign="middle">FAX No</td>
            <td   valign="middle">
                <asp:TextBox ID="faxNo" runat="server" CssClass="TextboxSmall" MaxLength="50" 
                    TabIndex="15" />
            </td>
            <td   valign="middle">&nbsp;</td>
            <td   valign="middle">
                &nbsp;</td>
            <td   valign="middle">
                &nbsp;</td>
            <td   valign="middle">FAX No</td>
            <td   valign="middle">
                <asp:TextBox ID="faxNo0" runat="server" CssClass="TextboxSmall" 
                    MaxLength="50" TabIndex="22" />
            </td>
        </tr>
        <tr><td   valign="middle">&nbsp;</td>
            <td   valign="middle">
                &nbsp;</td>
            <td   valign="middle">&nbsp;</td>
            <td   valign="middle">
                &nbsp;</td>
            <td   valign="middle">
                &nbsp;</td>
            <td>&nbsp;</td>
            <td  valign="middle"><asp:Label ID="message" runat="server" /></td>
        </tr>
        <tr><td colspan="3" align="center">
          
         
            </td>
            <td colspan="4" align="center">
               
            </td>
        </tr>
        <tr><td align="center" valign="middle" colspan="7">
                <asp:Button ID="add" Text="Add" runat="server"  CssClass="Button" 
                    onclick="add_Click" TabIndex="23" />&nbsp;
                <asp:Button ID="cancel" Text="Cancel" runat="server"  CausesValidation="false"
                    CssClass="Button" onclick="cancel_Click" />
            </td>
        </tr>
        </table>
        </td>
        </tr>
     </table>
        </div>
        <!--Content Table End-->
        <div class="Line">
        </div>
        <div class="PageContent">
        
            <table >
                <tr>
                    <td>
                        Search Customer:
                        <asp:TextBox ID="txtSearch" runat="server" CssClass="TextboxSmall"></asp:TextBox>
                    &nbsp;
                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="Button" 
                            onclick="btnSearch_Click" CausesValidation="False" />&nbsp;
                         <asp:Button ID="btnShowall" runat="server" Text="Show All" CssClass="Button" 
                            onclick="btnShowall_Click" CausesValidation="False" />
                    </td>
                    <td>
                        <asp:Label ID="Label1" runat="server" /></td>
                    <td>
                         &nbsp;</td>
                </tr>
            </table>
        
        </div>
        <!--Grid Table Start-->
       <%-- <asp:DataGrid ID="storelocationGrid" runat="server" AutoGenerateColumns="false" DataKeyField="storeId"
            CellPadding="3" CellSpacing="3" 
            OnItemCommand="storelocationGrid_ItemCommand" HeaderStyle-CssClass="GridHeading"
            ItemStyle-CssClass="GridData" 
             >
            <Columns>
             <asp:BoundColumn DataField="storCode" HeaderText="Division" HeaderStyle-Font-Bold="true" />
                <asp:BoundColumn DataField="storeName" HeaderText="Sub Division" HeaderStyle-Font-Bold="true" />
                <asp:ButtonColumn  ButtonType="LinkButton" CommandName="Edit" Text="Edit" ItemStyle-CssClass="GridLink" />
                <asp:ButtonColumn ButtonType="LinkButton"  CommandName="Delete" Text="Delete" ItemStyle-CssClass="GridLink" />
            </Columns>
        </asp:DataGrid>--%>
        <asp:DataGrid ID="customerGrid" runat="server" AutoGenerateColumns="False" DataKeyField="customerId"
            CellPadding="0" OnItemCommand="customerGrid_ItemCommand" HeaderStyle-CssClass="GridHeading"
            ItemStyle-CssClass="GridData" onprerender="customerGrid_PreRender" 
            GridLines="None" Width="100%" onitemcreated="customerGrid_ItemCreated">
            <ItemStyle CssClass="GridData" HorizontalAlign="Center"></ItemStyle>
            <Columns>
                <asp:BoundColumn DataField="customerName" HeaderText="Customer Name" HeaderStyle-HorizontalAlign="Left"
                    HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Left" >
                <HeaderStyle Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="acountType" HeaderText="Account Type" HeaderStyle-HorizontalAlign="Left" 
                    HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Left">
                <HeaderStyle Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                 <asp:ButtonColumn ButtonType="LinkButton" CommandName="view" Text="View"  
                    HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>
<ItemStyle CssClass="GridLink"></ItemStyle>
                </asp:ButtonColumn>
                <asp:ButtonColumn ButtonType="LinkButton" CommandName="Edit" Text="Edit" 
                    HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

                <ItemStyle CssClass="GridLink"></ItemStyle>
                </asp:ButtonColumn>
                <asp:ButtonColumn ButtonType="LinkButton" CommandName="Delete" Text="Delete" 
                     ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
               
                </asp:ButtonColumn>
            </Columns>
        <HeaderStyle CssClass="GridHeading"></HeaderStyle>
        </asp:DataGrid>
        <!--Grid Table End-->
        <div class="Line">
        </div>
        <!--Page Body End-->
    </div>
</asp:Content>

