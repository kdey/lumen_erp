﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;

public partial class DataEntry_FormRecoverable : BasePage
{
    protected int FormRecoverableId = -1;
    protected void Page_Load(object sender, EventArgs e)
    {
        message.Text = "";
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!this.IsPostBack)
        {
            BindGrid();
           
        }
        else
        {
            // check if a division id has been passed
            try
            {
                FormRecoverableId = int.Parse(Request["FormRecoverableId"]);
              
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
                message.ForeColor = Color.Green;
            }
        }
        if (GetLoggedinUser().UserType == 2)
        {
            add.Visible  = false;
        }
    }
    private void BindGrid()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select * from Lum_Erp_RecoverableForm order by RecoverableFormName"))
            {
                FormrecoverableGrid.DataSource = reader;
                FormrecoverableGrid.DataBind();
            }
        }

    }

    protected void add_Click(object sender, EventArgs e)
    {
        if (FormRecoverableId == -1)
        {
            // we are in add mode
            if (FormRecoverableName.Text.Trim().Length > 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    int i = conn.ExecuteCommandProc("[Lum_Erp_Stp_AddFormRecoverable]", new SpParam("@FormRecoverableName", FormRecoverableName.Text.Trim(), SqlDbType.VarChar));
                 if (i > 0)
                 {
                     message.Text = "FormRecoverable Added : " + FormRecoverableName.Text;
                     message.ForeColor = Color.Green;
                     FormRecoverableName.Text = string.Empty;
                 }
                 else
                 {
                     message.Text = "FormRecoverable already exist : " + FormRecoverableName.Text;
                     message.ForeColor = Color.Red;
                 }
                }
            }
        }
        else
        {
            // we are in edit mode.
            if (FormRecoverableName.Text.Trim().Length > 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                  int i=  conn.ExecuteCommand(string.Format(
                           "if(not exists(select * from Lum_Erp_RecoverableForm where RecoverableFormName = '{0}' and FormRecoverableId!={3}))update Lum_Erp_RecoverableForm set RecoverableFormName = '{1}' where FormRecoverableId = {2}",
                           DataUtility.CleanString(FormRecoverableName.Text.Trim()), DataUtility.CleanString(FormRecoverableName.Text.Trim()), FormRecoverableId, FormRecoverableId));
                  if (i > 0)
                  {
                      add.Text = "Add";
                      message.Text = "Recoverable Form Changed to : " + FormRecoverableName.Text;
                      message.ForeColor = Color.Green;
                      FormRecoverableName.Text = string.Empty;
                      FormRecoverableId = -1;
                  }
                  else
                  {
                      message.Text = "FormRecoverable already exist : " + FormRecoverableName.Text;
                      message.ForeColor = Color.Red;
                  }


                }
            }

        }

        BindGrid();
    }
    protected void cancel_Click(object sender, EventArgs e)
    {
        FormRecoverableName.Text = "";
        add.Text = "Add";
        FormRecoverableId = -1;
    }

    protected void FormRecovrerableGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            if (e.CommandName.Equals("View"))
            {
                FormRecoverableId = (int)FormrecoverableGrid.DataKeys[e.Item.ItemIndex];
                LoadDivision();
                add.Visible = false;
            }
                        if (e.CommandName.Equals("Edit"))
                        {
                            FormRecoverableId = (int)FormrecoverableGrid.DataKeys[e.Item.ItemIndex];
                            LoadDivision();
                            add.Visible = true ;
                        }
                        else if (e.CommandName.Equals("Delete"))
                        {
                            add.Visible = true;
                            FormRecoverableId = (int)FormrecoverableGrid.DataKeys[e.Item.ItemIndex];

                            try
                            {
                                conn.ExecuteCommand("delete from Lum_Erp_RecoverableForm where FormRecoverableId = " + FormRecoverableId);
                                message.Text = "Recoverable Form deleted";
                                message.ForeColor = Color.Green;
                            }
                            catch
                            {
                                message.Text = "Recoverable Form cannot be deleted because of existing related data";
                                message.ForeColor = Color.Red;
                            }
                            FormRecoverableName.Text = string.Empty;
                            FormRecoverableId = -1;

                            BindGrid();
                        }

                    
               
            
        }

    }
    protected void FormRecovrerableGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton btnview = (LinkButton)e.Item.Cells[1].Controls[0];

            LinkButton btnedit = (LinkButton)e.Item.Cells[2].Controls[0];
            LinkButton btn = (LinkButton)e.Item.Cells[3].Controls[0];
            btnview.CssClass = "GridLink";
            btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this?')");
            btn.CssClass = "GridLink";
            btnedit.CssClass = "GridLink";
            FormRecoverableName.Text = "";
            add.Text = "Add";
            FormRecoverableId = -1;
            if (GetLoggedinUser().UserType == 2)
            {
                btn.Visible = false;
                btnedit.Visible = false;
            }
        }
    }

    private void LoadDivision()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select RecoverableFormName from Lum_Erp_RecoverableForm where FormRecoverableId = " + FormRecoverableId))
            {
                if (reader.Read())
                {
                    FormRecoverableName.Text = reader.GetString(0);
                    add.Text = "Update";
                    cancel.Visible = true;
                }
            }
        }
    }
}
