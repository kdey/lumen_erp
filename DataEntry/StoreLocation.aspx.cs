﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Drawing;
using Bitscrape.AppBlock.Database;
public partial class DataEntry_StoreLocation :BasePage
{
    protected int storelocationId = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        message.Text = "";
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!this.IsPostBack)
        {
           
            BindGrid();
            //using (IConnection conn = DataConnectionFactory.GetConnection())
            //{
            //    using (IDataReader reader = conn.ExecuteQuery("select * from AdminUser where userName= 'demo'"))
            //    {
            //        while (reader.Read())
            //        {
            //            AdminUser s = GetLoggedinUser();
            //            int a = s.UserId;
            //            if (a == Convert.ToInt32(reader["adminuserId"]))

            //                add.Enabled = false;
            //        }
            //    }
            //}
        }
        else
        {
            // check if a division id has been passed
            try
            {
                storelocationId = int.Parse(Request["storelocationId"]);
                //using (IConnection conn = DataConnectionFactory.GetConnection())
                //{
                //    using (IDataReader reader = conn.ExecuteQuery("select * from AdminUser where userName= 'demo'"))
                //    {
                //        while (reader.Read())
                //        {
                //            AdminUser s = GetLoggedinUser();
                //            int a = s.UserId;
                //            if (a == Convert.ToInt32(reader["adminuserId"]))

                //                add.Enabled = false;
                //        }
                //    }
                //}
            }
            catch { };
        }
        if (GetLoggedinUser().UserType == 2)
        {
            add.Visible  = false;

        }
    }
   
    private void BindGrid()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            //using (IDataReader reader = conn.ExecuteQuery("SELECT * FROM Lum_Erp_Storelocation order by storeName"))
            using (IDataReader reader = conn.ExecuteQuery("SELECT [storeId],[storCode] AS storCode,[storeName],[storeAddress],[zipCode] FROM Lum_Erp_Storelocation order by storeName"))
            {
                storelocationGrid.DataSource = reader;
                storelocationGrid.DataBind();
            }
        }

    }

    protected void add_Click(object sender, EventArgs e)
    {
        if (storelocationId == -1)
        {
            // we are in add mode
            if (storeCode.Text.Trim().Length == 3)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    int i = conn.ExecuteCommandProc("Lum_Erp_Stp_AddStorelocation", new SpParam("@storCode", storeCode.Text.Trim(), SqlDbType.VarChar), new SpParam("@storeName", storeName.Text.Trim(), SqlDbType.VarChar), new SpParam("@storeAddress", storeAddress.Text.Trim(), SqlDbType.VarChar), new SpParam("@zipCode", zipCode.Text.Trim(), SqlDbType.VarChar));

                  if (i > 0)
                  {
                      message.Text = "Store  Added : " + storeName.Text;
                      message.ForeColor = Color.Green;
                      storeCode.Text = string.Empty;
                      storeName.Text = string.Empty;
                      storeAddress.Text = string.Empty;
                      zipCode.Text = string.Empty;
                      err.Visible = false;
                  }
                  else
                  {
                      message.Text = "Store  Name Already Exists : " + storeName.Text;
                      message.ForeColor = Color.Red;
                      storeCode.Text = string.Empty;
                      storeName.Text = string.Empty;
                      storeAddress.Text = string.Empty;
                      zipCode.Text = string.Empty;
                      err.Visible = false;
                  }
                }
            }
            else
            {
                err.Visible = true;
            }
        }
        else
        {
            // we are in edit mode.
            if (storeCode.Text.Trim().Length ==3)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                  int i=  conn.ExecuteCommand(string.Format(
                           " if(not exists(select * from Lum_Erp_Storelocation where storeName = '{0}' and  storeId!={6}))update Lum_Erp_Storelocation set storCode = '{1}',storeName='{2}',storeAddress='{3}',zipCode='{4}' where storeId = {5}",
                            DataUtility.CleanString(storeName.Text.Trim()), DataUtility.CleanString(storeCode.Text.Trim() + sCode.Value.Trim()), DataUtility.CleanString(storeName.Text.Trim()), DataUtility.CleanString(storeAddress.Text.Trim()), DataUtility.CleanString(zipCode.Text.Trim()), storelocationId, storelocationId));
                  if (i > 0)
                  {
                      add.Text = "Add";
                      message.Text = "Store  Changed to : " + storeName.Text;
                      message.ForeColor = Color.Green;
                      storeCode.Text = string.Empty;
                      storeName.Text = string.Empty;
                      storeAddress.Text = string.Empty;
                      zipCode.Text = string.Empty;
                      storelocationId = -1;
                      err.Visible = false;
                  }
                  else
                  {
                      message.Text = "Store  Name Already Exists : " + storeName.Text;
                      message.ForeColor = Color.Red;
                  }

                }
            }
            else
            {
                err.Visible = true;
            }

        }

        BindGrid();

        #region stored proc code
        //create proc [dbo].[AddDivision] @divisionName varchar(255)
        //as
        //begin
        //    if(not exists(select * from Division where divisionName = @divisionName))
        //        insert into Division(divisionName) values(@divisionName)
        //end
        #endregion
    }

    protected void storelocationGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            if (e.CommandName.Equals("View"))
            {
                storelocationId = (int)storelocationGrid.DataKeys[e.Item.ItemIndex];
                LoadStoreLocation();
                add.Visible = false;
            }
                        if (e.CommandName.Equals("Edit"))
                        {
                            storelocationId = (int)storelocationGrid.DataKeys[e.Item.ItemIndex];
                            LoadStoreLocation();
                            add.Visible = true;
                        }
                        else if (e.CommandName.Equals("Delete"))
                        {
                            add.Visible = true;
                            storelocationId = (int)storelocationGrid.DataKeys[e.Item.ItemIndex];
                             try
                                {
                                    conn.ExecuteCommand("delete from Lum_Erp_Storelocation where storeId = " + storelocationId);
                                    message.Text = "Store  deleted";
                                    message.ForeColor = Color.Green;
                                }
                                catch
                                {
                                    message.Text = "Store cannot be deleted because of existing related data";
                                    message.ForeColor = Color.Red;
                                }
                                storeCode.Text = string.Empty;
                                storeName.Text = string.Empty;
                                storeAddress.Text = string.Empty;
                                zipCode.Text = string.Empty;
                                storelocationId = -1;
                          
                            BindGrid();
                        }
                
      
     
        }
    }

    private void LoadStoreLocation()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select storCode,storeName,storeAddress,zipCode from Lum_Erp_Storelocation where storeId = " + storelocationId))
            {
                if (reader.Read())
                {

                    storeCode.Text = reader.GetString(0).Substring(0,3);
                    sCode.Value = reader.GetString(0).Substring(3);
                    storeName.Text = reader.GetString(1);
                    storeAddress.Text = reader.GetString(2);
                    zipCode.Text = reader.GetString(3);
                    add.Text = "Update";
                    cancel.Visible = true;
                }
            }
        }
    }

    protected void cancel_Click(object sender, EventArgs e)
    {
        storeCode.Text = string.Empty;
        storeName.Text = string.Empty;
        storeAddress.Text = string.Empty;
        zipCode.Text = string.Empty;
        add.Text = "Add";
        storelocationId = -1;
    }
    protected void storelocationGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton btnview = (LinkButton)e.Item.Cells[2].Controls[0];
            btnview.CssClass = "GridLink";
            LinkButton btnEdit = (LinkButton)e.Item.Cells[3].Controls[0];
            LinkButton btn = (LinkButton)e.Item.Cells[4].Controls[0];
            btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this?')");
            btn.CssClass = "GridLink";
            btnEdit.CssClass = "GridLink";
            //storeCode.Text = string.Empty;
            //storeName.Text = string.Empty;
            //storeAddress.Text = string.Empty;
            //zipCode.Text = string.Empty;
            add.Text = "Add";
            storelocationId = -1;
            if (GetLoggedinUser().UserType == 2)
            {
                btn.Visible = false;
                btnEdit.Visible = false;
            }
        }
    }
}
