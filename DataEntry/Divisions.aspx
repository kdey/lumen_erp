﻿<%@ Page Language="C#" MasterPageFile="~/DataEntry/DataEntry.master" AutoEventWireup="true" CodeFile="Divisions.aspx.cs" Inherits="DataEntry_Division" Title="Division Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" name="divisionId" value="<%=divisionId%>" />
    <div class="PageMargin">
        <div class="PageHeading">Divisions</div>
        <div class="Line"></div>
        <!--Content Table Start-->
        <div class="PageContent">
        
        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
        <tr><td width="40%" align="right" valign="middle">Add/Edit new Division&nbsp; :&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="20%" align="center" valign="middle"><asp:TextBox ID="divisionName" runat="server" CssClass="TextboxSmall" />
                </td>
            <td align="left" valign="middle" width="40%">
                <asp:Button ID="add" Text="Add" runat="server" OnClick="add_Click" CssClass="Button" />
                &nbsp;&nbsp;
                <asp:Button ID="cancel" Text="Cancel" runat="server" OnClick="cancel_Click" CausesValidation="false"
                    CssClass="Button" />
            </td>
        </tr>
        <%--<tr><td align="right" valign="middle">Spares&nbsp; :&nbsp;</td>
            <td><asp:CheckBox ID="chkSpares" Checked= "false" runat="server"  />
            </td>
            <td>&nbsp;</td>
        </tr>   --%>     
        <tr><td align="left" valign="middle"><asp:Label ID="message" runat="server" /></td>
            <td><asp:RequiredFieldValidator ControlToValidate="divisionName" ErrorMessage="Field cannot be empty" ID="nameValidator"
                 runat="server" /></td>
            <td>&nbsp;</td>
        </tr></table>
        
        </div>
        <!--Content Table End-->
        <div class="Line">
        </div>
        <!--Grid Table Start-->
        <asp:DataGrid ID="divisionGrid" runat="server" AutoGenerateColumns="false" DataKeyField="divisionId"
            CellPadding="0" OnItemCommand="divisionGrid_ItemCommand" HeaderStyle-CssClass="GridHeading"
            ItemStyle-CssClass="GridData" onitemcreated="divisionGrid_ItemCreated" 
            GridLines="None" Width="100%">
            <ItemStyle CssClass="GridData" HorizontalAlign="Center"></ItemStyle>
            <Columns>
                <asp:BoundColumn DataField="divisionName" HeaderText="Existing divisions" HeaderStyle-HorizontalAlign="Left"
                    HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Left">
                <HeaderStyle Font-Bold="True"></HeaderStyle>
                </asp:BoundColumn>
                <%--<asp:BoundColumn  DataField="spares" HeaderText="Spares" HeaderStyle-HorizontalAlign="Left"
                    HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Left">
                <HeaderStyle Font-Bold="True"></HeaderStyle>
                </asp:BoundColumn>  --%>              
                  <asp:ButtonColumn  ButtonType="LinkButton" CausesValidation="false" 
                    CommandName="View" Text="View" 
                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="GridLink" 
                    ItemStyle-Width="10%" >
                <ItemStyle HorizontalAlign="Left" CssClass="GridLink" Width="10%"></ItemStyle>
                </asp:ButtonColumn>
                <asp:ButtonColumn ButtonType="LinkButton" CommandName="Edit" Text="Edit" ItemStyle-Width="10%" 
                    ItemStyle-CssClass="GridLink" ItemStyle-HorizontalAlign="Left">
                <ItemStyle CssClass="GridLink"></ItemStyle>
                </asp:ButtonColumn>
                <asp:ButtonColumn ButtonType="LinkButton" CommandName="Delete" Text="Delete" ItemStyle-Width="10%" 
                    ItemStyle-CssClass="GridLink" ItemStyle-HorizontalAlign="Left">
                <ItemStyle CssClass="GridLink"></ItemStyle>
                </asp:ButtonColumn>                
            </Columns>
        <HeaderStyle CssClass="GridHeading"></HeaderStyle>
        </asp:DataGrid>
        <!--Grid Table End-->
        <div class="Line">
        </div>
        <!--Page Body End-->
    </div>
</asp:Content>
