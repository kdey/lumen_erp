﻿<%@ Page Title="Stock Master" Language="C#" MasterPageFile="~/DataEntry/DataEntry.master" Culture="en-GB" AutoEventWireup="true" EnableEventValidation="false" CodeFile="StockMaster.aspx.cs" Inherits="DataEntry_StockMaster" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
    function isNumberKey(evt)   //Checks textbox contains only numeric value
    {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode != 46)) {
            return false;
        }
        return true;
    }
    function isNumberKey_WO_Decimal(evt)   //Checks textbox contains only numeric value
    {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    function letternumber(e) {
        var key;
        var keychar;

        if (window.event)
            key = window.event.keyCode;
        else if (e)
            key = e.which;
        else
            return true;
        keychar = String.fromCharCode(key);
        keychar = keychar.toLowerCase();

    // control keys
        if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
            return true;

    // alphas and numbers
        else if ((("abcdefghijklmnopqrstuvwxyz0123456789;").indexOf(keychar) > -1))
            return true;
        else
            return false;
    }
     
</script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <input type="hidden" name="stockId" value="<%=stockId%>" />
    <div class="PageMargin">
        <div class="PageHeading">Stock Master</div>
        <div class="Line"></div>
        <!--Content Table Start-->
        <div class="PageContent">
        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
        <tr><td width="15%" align="center" valign="middle">Select Store&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="15%" align="center" valign="middle">Date (dd/mm/yyyy)&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="15%" align="center" valign="middle">Select Division</td>
            <td width="15%" align="center" valign="middle">Select Subdivision&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="15%" align="center" valign="middle">Item Name/Part Code&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="15%" align="center" valign="middle">Opening Stock&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="15%" align="center" valign="middle">Serial Number will be  separated by (;)</td>
            <td width="10%" align="center" valign="middle">UOM</td>
        </tr>
        <tr><td align="center" valign="middle">
                <asp:DropDownList CssClass="DropdownExtraSmall" ID="ddlStore" CausesValidation="false" runat="server">
                </asp:DropDownList>
            </td>
            <td align="center" valign="middle">
                <asp:TextBox ID="update" runat="server" onkeyup="document.getElementById(this.id).value=''" CssClass="TextboxExtraSmall" ValidationGroup="a" />
                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="update">
                </cc1:CalendarExtender>
            </td>
            <td align="center" valign="middle">
                <asp:DropDownList CssClass="DropdownExtraSmall" ID="ddlDivision" runat="server" 
                    onselectedindexchanged="ddlDivision_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
            </td>
            <td align="center" valign="middle">
                <asp:DropDownList CssClass="DropdownExtraSmall" ID="ddlSubdivision" 
                    runat="server" onselectedindexchanged="ddlSubdivision_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
            </td>
            <td align="center" valign="middle">
                    <asp:DropDownList CssClass="DropdownExtraSmall" ID="ddlItem" runat="server">
                       </asp:DropDownList>
            </td>
            <td align="center" valign="middle">
                    <asp:TextBox ID="quantity" onkeypress="return isNumberKey(event)" runat="server"  CssClass="TextboxExtraSmall"  />
            </td>
            <td valign="top"><asp:TextBox ID="txtserialNo" TextMode="MultiLine" onkeypress="return letternumber(event);" Width="100"
                 runat="server" CssClass="TextAreaSmall" /></td>            
            <td align="center" valign="middle"><asp:Label ID="uomName" runat="server"></asp:Label></td>
            
            <%--<cc1:CascadingDropDown ID="CascadingDropDown3"
            runat="server" 
            TargetControlID="ddlDivision" LoadingText="[Loading...]"
            Category="Division"
            PromptText="Select a Division"
            ServicePath ="~/divisionService.asmx"
            ServiceMethod="GetDivision" >   </cc1:CascadingDropDown>
              
           <cc1:CascadingDropDown ID="CascadingDropDown1"
                runat="server" ParentControlID = "ddlDivision" 
                TargetControlID="ddlSubdivision" LoadingText="[Loading...]"
                Category="subDivision"
                PromptText="Select a subDivision"
                ServicePath ="~/divisionService.asmx"
                ServiceMethod="GetSubdividsionForDivision" >   </cc1:CascadingDropDown>

          <cc1:CascadingDropDown ID="CascadingDropDown2"
                runat="server" ParentControlID = "ddlSubdivision" 
                TargetControlID="ddlItem" LoadingText="[Loading...]"
                Category="Item"
                PromptText="Select Item"
                ServicePath ="~/divisionService.asmx"
                ServiceMethod="GetItemForSubdividsion" >   </cc1:CascadingDropDown> --%>
        </tr>
        <tr><td align="center" valign="middle"><asp:RequiredFieldValidator ControlToValidate="ddlStore" ErrorMessage="Select Store"
                ID="RequiredFieldValidator2" InitialValue="0" runat="server" /></td>
            <td align="center" valign="middle"><asp:RequiredFieldValidator ControlToValidate="update" ErrorMessage="Field cannot be empty"
                ID="nameValidator" runat="server" /></td>
            <td align="center" valign="middle"><asp:RequiredFieldValidator ControlToValidate="ddlDivision" ErrorMessage="Select Division"
                ID="divisionValidator" InitialValue="Please Select" runat="server" /></td>
            <td align="center" valign="middle"><asp:RequiredFieldValidator ControlToValidate="ddlSubdivision" ErrorMessage="Select Division"
                ID="RequiredFieldValidator1" InitialValue="0" runat="server" /></td>
            <td align="center" valign="middle"><asp:RequiredFieldValidator InitialValue="0" ControlToValidate="ddlItem" ErrorMessage="Select Item Code"
                ID="RequiredFieldValidator3"  runat="server" /></td>
            <td align="center" valign="middle"><asp:RequiredFieldValidator  ControlToValidate="quantity" ErrorMessage="Select Item Code"
                ID="RequiredFieldValidator4"  runat="server" />
                <asp:RegularExpressionValidator ID="quanityRegexValidator" runat="server" EnableClientScript="true"
                    ErrorMessage="Quantity must be a integer" Text="" ControlToValidate="quantity" ValidationExpression="\d+" /></td>
            <td align="left" valign="middle"></td>
        </tr>
        <tr>
        <td>
            <asp:Label ID="errmsg" runat="server" Text="Label"></asp:Label>
        </td>
        <td align="center" valign="middle" colspan="7">
        <asp:Label ID="message" runat="server" />
        <br />
                <asp:Button ID="add" Text="Add" runat="server"   CssClass="Button" 
                    onclick="add_Click"  />
                <asp:Button ID="cancel" Text="Cancel" runat="server"  CausesValidation="false"
                    CssClass="Button" onclick="cancel_Click" />
            </td>
        </tr></table>
        </div>
          <!--Content Table End-->
        <div class="Line"></div>
        <!--Grid Table Start-->
        <asp:DataGrid ID="stockGrid" runat="server" AutoGenerateColumns="false" DataKeyField="stockId"
            CellPadding="0" OnItemCommand="stockGrid_ItemCommand" HeaderStyle-CssClass="GridHeading"
            ItemStyle-CssClass="GridData" onitemcreated="stockGrid_ItemCreated" 
            GridLines="None" Width="100%" >
            <Columns>
                <asp:BoundColumn DataField="storeName" HeaderText="Store Name" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="left" />
                <asp:BoundColumn DataField="itemCode" HeaderText="Item Code" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="left" />
                <asp:BoundColumn DataField="quantity" HeaderText="Opening Quantity" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="left" />
                <asp:BoundColumn DataField="closingQuantity" HeaderText="Closing Quantity" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="left" />
                <asp:BoundColumn DataField="update"  DataFormatString="{0:dd/MM/yyyy}"  HeaderText="Last Updated" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="left" />
                   <asp:ButtonColumn  ButtonType="LinkButton" CausesValidation="false" 
                    CommandName="View" Text="View" 
                 ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="GridLink" 
                    ItemStyle-Width="10%" >
<ItemStyle HorizontalAlign="Left" CssClass="GridLink" Width="10%"></ItemStyle>
                </asp:ButtonColumn>
                <asp:ButtonColumn ButtonType="LinkButton"  CommandName="Delete" Text="Delete" ItemStyle-CssClass="GridLink" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="left" />
            </Columns>
        </asp:DataGrid>
        <!--Grid Table End-->
        <div class="Line">
        </div>
        <!--Page Body End-->
    </div>
</asp:Content>

