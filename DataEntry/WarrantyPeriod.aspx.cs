﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;

public partial class DataEntry_WarrantyPeriod : BasePage
{
    protected int warrantyId = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        message.Text = "";
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!this.IsPostBack)
        {

            BindGrid();
            //using (IConnection conn = DataConnectionFactory.GetConnection())
            //{
            //    using (IDataReader reader = conn.ExecuteQuery("select * from AdminUser where userName= 'demo'"))
            //    {
            //        while (reader.Read())
            //        {
            //            AdminUser s = GetLoggedinUser();
            //            int a = s.UserId;
            //            if (a == Convert.ToInt32(reader["adminuserId"]))

            //                add.Enabled = false;
            //        }
            //    }
            //}
        }
        else
        {
            // check if a division id has been passed
            try
            {
                warrantyId = int.Parse(Request["warrantyId"]);
                //using (IConnection conn = DataConnectionFactory.GetConnection())
                //{
                //    using (IDataReader reader = conn.ExecuteQuery("select * from AdminUser where userName= 'demo'"))
                //    {
                //        while (reader.Read())
                //        {
                //            AdminUser s = GetLoggedinUser();
                //            int a = s.UserId;
                //            if (a == Convert.ToInt32(reader["adminuserId"]))

                //                add.Enabled = false;
                //        }
                //    }
                //}
            }
            catch { };
        }
        if (GetLoggedinUser().UserType == 2)
        {
            add.Visible  = false;

        }
    }

    private void BindGrid()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("SELECT * FROM Lum_Erp_Warranty order by warrantyPeriod"))
            {
                warrantyGrid.DataSource = reader;
                warrantyGrid.DataBind();
            }
        }

    }

    protected void add_Click(object sender, EventArgs e)
    {
        if (warrantyId == -1)
        {
            // we are in add mode
            if (warrantyPeriod.Text.Trim().Length > 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    int i = conn.ExecuteCommandProc("Lum_Erp_AddWarranty", new SpParam("@warrantyPeriod", warrantyPeriod.Text.Trim(), SqlDbType.VarChar), new SpParam("@warrantyDescription", warrantyDescription.Text.Trim(), SqlDbType.VarChar));
                 if (i > 0)
                 {
                     message.Text = "Warranty Period  Added : " + warrantyPeriod.Text;
                     message.ForeColor = Color.Green;
                     warrantyPeriod.Text = string.Empty;
                     warrantyDescription.Text = string.Empty;
                 }
                 else
                 {
                     message.Text = "Warranty Period  alredy exist : " + warrantyPeriod.Text;
                     message.ForeColor = Color.Red;
                 }

                }
            }
        }
        else
        {
            // we are in edit mode.
            if (warrantyPeriod.Text.Trim().Length > 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                  int i=  conn.ExecuteCommand(string.Format(
                           " if(not exists(select * from Lum_Erp_Warranty where warrantyPeriod = '{0}' and warrantyId!={4} ))update Lum_Erp_Warranty set warrantyPeriod = '{1}',warrantyDescription='{2}' where warrantyId = {3}",
                           DataUtility.CleanString(warrantyPeriod.Text.Trim()), DataUtility.CleanString(warrantyPeriod.Text.Trim()), DataUtility.CleanString(warrantyDescription.Text.Trim()), warrantyId, warrantyId));
                  if (i > 0)
                  {
                      add.Text = "Add";
                      message.Text = "Warranty Period to : " + warrantyPeriod.Text;
                      message.ForeColor = Color.Green;
                      warrantyPeriod.Text = string.Empty;
                      warrantyDescription.Text = string.Empty;

                      warrantyId = -1;
                  }
                  else
                  {
                      message.Text = "Warranty Period  alredy exist : " + warrantyPeriod.Text;
                      message.ForeColor = Color.Red;

                  }

                }
            }

        }

        BindGrid();

        #region stored proc code
        //create proc [dbo].[AddDivision] @divisionName varchar(255)
        //as
        //begin
        //    if(not exists(select * from Division where divisionName = @divisionName))
        //        insert into Division(divisionName) values(@divisionName)
        //end
        #endregion
    }

    protected void warrantyGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            if (e.CommandName.Equals("View"))
        {
            warrantyId = (int)warrantyGrid.DataKeys[e.Item.ItemIndex];
            LoadWarranty();
            add.Visible = false;
        }
                        if (e.CommandName.Equals("Edit"))
                        {
                            warrantyId = (int)warrantyGrid.DataKeys[e.Item.ItemIndex];
                            LoadWarranty();
                            add.Visible = true;
                        }
                        else if (e.CommandName.Equals("Delete"))
                        {
                            add.Visible = true;
                            warrantyId = (int)warrantyGrid.DataKeys[e.Item.ItemIndex];

                            try
                            {
                                conn.ExecuteCommand("delete from Lum_Erp_Warranty where warrantyId = " + warrantyId);
                                message.Text = "Warranty Period  deleted";
                                message.ForeColor = Color.Green;
                            }
                            catch
                            {
                                message.Text = "Warranty Period cannot be deleted because of existing related data";
                                message.ForeColor = Color.Red;
                            }
                            warrantyPeriod.Text = string.Empty;
                            warrantyDescription.Text = string.Empty;
                            warrantyId = -1;

                            BindGrid();
                        }
                  
                
           
        }
    }

    private void LoadWarranty()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select warrantyPeriod,warrantyDescription from Lum_Erp_Warranty where warrantyId = " + warrantyId))
            {
                if (reader.Read())
                {

                    warrantyPeriod.Text = reader.GetString(0);
                    warrantyDescription.Text = reader.GetString(1);

                    add.Text = "Update";
                    cancel.Visible = true;
                }
            }
        }
    }

    protected void cancel_Click(object sender, EventArgs e)
    {
        warrantyPeriod.Text = string.Empty;
        warrantyDescription.Text = string.Empty;
        warrantyId = -1;
        add.Text = "Add";

    }
    protected void warrantyGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton btnview= (LinkButton)e.Item.Cells[1].Controls[0];
            btnview.CssClass = "GridLink";
            LinkButton btnEdit = (LinkButton)e.Item.Cells[2].Controls[0];
            LinkButton btn = (LinkButton)e.Item.Cells[3].Controls[0];
            btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this?')");
            btn.CssClass = "GridLink";
            btnEdit.CssClass = "GridLink";
            warrantyPeriod.Text = string.Empty;
            warrantyDescription.Text = string.Empty;
            warrantyId = -1;
            add.Text = "Add";
            if (GetLoggedinUser().UserType == 2)
            {
                btn.Visible = false;
                btnEdit.Visible = false;
            }
        }
    }
}
