﻿<%@ Page Language="C#" MasterPageFile="~/DataEntry/DataEntry.master" AutoEventWireup="true"
    CodeFile="OrderType.aspx.cs" Inherits="DataEntry_OrderType" Title="Order Type" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" name="orderTypeId" value="<%=orderTypeId%>" />
    <div class="PageMargin">
        <div class="PageHeading">Purchase Order Type</div>
        <div class="Line"></div>
        <!--Content Table Start-->
        <div class="PageContent">
        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
        <tr><td width="20%" align="right" valign="middle">Purchase Order Type Code&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="20%" align="left" valign="middle">
                <asp:TextBox ID="orderCode" runat="server"  CssClass="TextboxSmall" MaxLength="50" />
            </td>
            <td width="20%" align="right" valign="middle">Purchase Order Type Name</td>
            <td width="20%" align="left" valign="middle">
                <asp:TextBox ID="orderName" MaxLength="50" runat="server" CssClass="TextboxSmall" /></td>
            <td align="left" valign="middle" width="20%">
                <asp:Button ID="add" Text="Add" runat="server" OnClick="add_Click" CssClass="Button" />&nbsp;
                <asp:Button ID="cancel" Text="Cancel" runat="server" OnClick="cancel_Click" CausesValidation="false"
                    CssClass="Button" />
            </td>
        </tr>
        <tr><td align="left" valign="middle"><asp:Label ID="message" runat="server" /></td>
            <td align="left" valign="middle"><asp:RequiredFieldValidator ControlToValidate="orderCode" ErrorMessage="Field cannot be empty"
                    ID="nameValidator" runat="server" /></td>
            <td colspan="3">&nbsp;</td>
        </tr></table>
        </div>
        <!--Content Table End-->
        <div class="Line">
        </div>
        <!--Grid Table Start-->
        <asp:DataGrid ID="orderTypeGrid" runat="server" AutoGenerateColumns="False" DataKeyField="poTypeId"
            CellPadding="0" OnItemCommand="orderTypeGrid_ItemCommand" HeaderStyle-CssClass="GridHeading"
            ItemStyle-CssClass="GridData" onitemcreated="orderTypeGrid_ItemCreated" 
            GridLines="None" Width="100%">
            <ItemStyle CssClass="GridData"></ItemStyle>
            <Columns>
                     <asp:BoundColumn DataField="poTypeName" HeaderText="Purchase Order Type Name" HeaderStyle-HorizontalAlign="left" 
                    HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Left">
                <HeaderStyle Font-Bold="True"></HeaderStyle>
                         <ItemStyle HorizontalAlign="Left" />
                </asp:BoundColumn>
                <asp:BoundColumn DataField="poTypeCode" HeaderText="Purcahse Order Type Code" HeaderStyle-HorizontalAlign="left"
                    HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Left">
                <HeaderStyle Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                  <asp:ButtonColumn  ButtonType="LinkButton" CausesValidation="false" 
                    CommandName="View" Text="View" 
                 ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="GridLink" 
                    ItemStyle-Width="10%" >
<ItemStyle HorizontalAlign="Left" CssClass="GridLink" Width="10%"></ItemStyle>
                </asp:ButtonColumn>
                <asp:ButtonColumn ButtonType="LinkButton" CommandName="Edit" Text="Edit" ItemStyle-Width="10%" 
                    ItemStyle-CssClass="GridLink" ItemStyle-HorizontalAlign="Left">
                <ItemStyle CssClass="GridLink"></ItemStyle>
                </asp:ButtonColumn>
                <asp:ButtonColumn ButtonType="LinkButton" CommandName="Delete" Text="Delete" ItemStyle-Width="10%" 
                    ItemStyle-CssClass="GridLink" ItemStyle-HorizontalAlign="Left">
                <ItemStyle CssClass="GridLink"></ItemStyle>
                </asp:ButtonColumn>
            </Columns>
        <HeaderStyle CssClass="GridHeading"></HeaderStyle>
        </asp:DataGrid>
        <!--Grid Table End-->
        <div class="Line">
        </div>
        <!--Page Body End-->
    </div>
</asp:Content>
