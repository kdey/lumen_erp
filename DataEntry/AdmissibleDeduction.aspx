﻿<%@ Page Language="C#" MasterPageFile="~/DataEntry/DataEntry.master" AutoEventWireup="true"
    CodeFile="AdmissibleDeduction.aspx.cs" Inherits="DataEntry_AdmissableDeduction"
    Title="Admissable Deduction" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" name="deductionId" value="<%=deductionId%>" />
    <div class="PageMargin">
        <div class="PageHeading">Admissible Deduction</div>
        <div class="Line"></div>
        <!--Content Table Start-->
        <div class="PageContent">
        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
        <tr><td width="40%" align="right" valign="middle">Admissible Deduction Type&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="20%" align="left" valign="middle">
                <asp:TextBox ID="deductionType" runat="server" CssClass="TextboxSmall" MaxLength="50" />
            </td>
            <td align="left" valign="middle" width="40%">
                <asp:Button ID="add" Text="Add" runat="server" OnClick="add_Click" CssClass="Button" />&nbsp;
                <asp:Button ID="cancel" Text="Cancel" runat="server" OnClick="cancel_Click" CausesValidation="false"
                    CssClass="Button" />
            </td>
        </tr>
        <tr><td align="left" valign="middle"><asp:Label ID="message" runat="server" /></td>
            <td align="left" valign="middle"><asp:RequiredFieldValidator ControlToValidate="deductionType" ErrorMessage="Field cannot be empty"
                    ID="nameValidator" runat="server" /></td>
            <td>&nbsp;</td>
        </tr></table>
        </div>
        <!--Content Table End-->
        <div class="Line">
        </div>
        <!--Grid Table Start-->
        <asp:DataGrid ID="deductionGrid" runat="server" AutoGenerateColumns="false" DataKeyField="deductionId"
            CellPadding="0" OnItemCommand="deductionGrid_ItemCommand" HeaderStyle-CssClass="GridHeading"
            ItemStyle-CssClass="GridData" onitemcreated="deductionGrid_ItemCreated" 
            Width="100%" GridLines="None">
            <ItemStyle CssClass="GridData"></ItemStyle>
            <Columns>
                <asp:BoundColumn DataField="deductionType" HeaderText="Existing Admissible Deduction Type" HeaderStyle-HorizontalAlign="Left"
                    HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Left" >
                <HeaderStyle Font-Bold="True"></HeaderStyle>
                </asp:BoundColumn>
                 <asp:ButtonColumn ButtonType="LinkButton" CommandName="View" Text="View"  
                    HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
<ItemStyle CssClass="GridLink"></ItemStyle>
                </asp:ButtonColumn>
                <asp:ButtonColumn ButtonType="LinkButton" CommandName="Edit" Text="Edit" 
                    ItemStyle-CssClass="GridLink" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
                <ItemStyle CssClass="GridLink"></ItemStyle>
                </asp:ButtonColumn>
                <asp:ButtonColumn ButtonType="LinkButton" CommandName="Delete" Text="Delete" 
                    ItemStyle-CssClass="GridLink" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
                <ItemStyle CssClass="GridLink"></ItemStyle>
                </asp:ButtonColumn>
            </Columns>
        <HeaderStyle CssClass="GridHeading"></HeaderStyle>
        </asp:DataGrid>
        <!--Grid Table End-->
        <div class="Line">
        </div>
        <!--Page Body End-->
    </div>
</asp:Content>
