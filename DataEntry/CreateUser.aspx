﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DataEntry/DataEntry.master" AutoEventWireup="true" CodeFile="CreateUser.aspx.cs" Inherits="DataEntry_CreateUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <input type="hidden" name="adminuserId" value="<%=adminuserId%>" />
    <div class="PageMargin">
        <div class="PageHeading">Create User</div>
        <div class="Line"></div>
        <!--Content Table Start-->
        <div class="PageContent" runat ="server">
        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
        <tr><td width="16%" align="right" valign="middle">First Name&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="16%" align="left" valign="middle">
                <asp:TextBox ID="firstName" runat="server" MaxLength="50"  CssClass="TextboxSmall" />
            </td>
            <td width="16%" align="right" valign="middle">Last Name&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="16%" align="left" valign="middle">
                <asp:TextBox ID="lastName" runat="server" MaxLength="50"  CssClass="TextboxSmall" />
            </td>
            <td width="16%" align="right" valign="middle">User Id&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="16%" align="left" valign="middle">
                <asp:TextBox ID="userId" runat="server" MaxLength="20" CssClass="TextboxSmall"  />
            </td>
        </tr>
        <tr><td>&nbsp;</td>
            <td><asp:RequiredFieldValidator ControlToValidate="firstName" ErrorMessage="Field cannot be empty"
                    ID="RequiredFieldValidator2" runat="server" /></td>
            <td>&nbsp;</td>
            <td><asp:RequiredFieldValidator ControlToValidate="lastName" ErrorMessage="Field cannot be empty"
                    ID="RequiredFieldValidator3" runat="server" /></td>
            <td>&nbsp;</td>
            <td><asp:RequiredFieldValidator ControlToValidate="userId" ErrorMessage="Field cannot be empty"
                    ID="RequiredFieldValidator1" runat="server" /></td>
        </tr>
        <tr><td align="right" valign="middle">Pass Word&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td align="left" valign="middle">
                <asp:TextBox ID="passWord" runat="server" MaxLength="50"  CssClass="TextboxSmall" />
            </td>
            <td align="right" valign="middle">User Type</td>
            <td align="left" valign="middle">
                <asp:DropDownList ID="ddlUserType" runat="server" CssClass="DropdownSmall">
                    <asp:ListItem Value="1" Text="Admin"></asp:ListItem>
                    <asp:ListItem Value="2" Text="User"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td align="left" valign="middle" colspan="2">
                <asp:RadioButtonList ID="rbActive" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Selected="True" Value="1">Active</asp:ListItem>
                    <asp:ListItem Value="0">In Active</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr><td align="left" valign="middle"><asp:Label ID="message" runat="server" /></td>
            <td align="left" valign="middle"><asp:RequiredFieldValidator ControlToValidate="passWord" ErrorMessage="Field cannot be empty"
                    ID="RequiredFieldValidator4" runat="server" /></td>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr><td align="center" valign="middle" colspan="6">
            <asp:Button ID="add" Text="Add" runat="server" OnClick="add_Click" CssClass="Button" />&nbsp;
            <asp:Button ID="cancel" Text="Cancel" runat="server" OnClick="cancel_Click" CausesValidation="false"
                CssClass="Button" />
            </td>
        </tr></table>
        </div>
        <!--Content Table End-->
        <div class="Line"></div>
        <!--Grid Table Start-->
        <asp:DataGrid ID="userGrid" runat="server" AutoGenerateColumns="False" DataKeyField="adminuserId"
            CellPadding="0" OnItemCommand="userGrid_ItemCommand" HeaderStyle-CssClass="GridHeading"
            ItemStyle-CssClass="GridData" onitemcreated="userGrid_ItemCreated" 
            GridLines="None" Width="100%">
<ItemStyle CssClass="GridData"></ItemStyle>
            <Columns>             
                <asp:BoundColumn DataField="firstName" HeaderText="First Name" HeaderStyle-HorizontalAlign="Left"
                 ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true" >
<HeaderStyle HorizontalAlign="Left" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="lastName" HeaderText="Last Name" HeaderStyle-HorizontalAlign="Left"
                 ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true" >
<HeaderStyle HorizontalAlign="Left" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="userName" HeaderText="User Name" HeaderStyle-HorizontalAlign="Left"
                 ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true" 
                    SortExpression="userName" >
<HeaderStyle HorizontalAlign="Left" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                  <asp:ButtonColumn  ButtonType="LinkButton" CausesValidation="false" 
                    CommandName="View" Text="View" 
                 ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="GridLink" 
                    ItemStyle-Width="10%" HeaderText="View" >
<ItemStyle HorizontalAlign="Left" CssClass="GridLink" Width="10%"></ItemStyle>
                </asp:ButtonColumn>
                <asp:ButtonColumn  ButtonType="LinkButton" CausesValidation="false" 
                    CommandName="Edit" Text="Edit" 
                 ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="GridLink" 
                    ItemStyle-Width="10%" HeaderText="Edit" >
<ItemStyle HorizontalAlign="Left" CssClass="GridLink" Width="10%"></ItemStyle>
                </asp:ButtonColumn>
            </Columns>

<HeaderStyle CssClass="GridHeading"></HeaderStyle>
        </asp:DataGrid>
        <!--Grid Table End-->
        <div class="Line">
        </div>
        <!--Page Body End-->
    </div>
</asp:Content>

