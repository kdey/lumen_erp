﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using Bitscrape.AppBlock.Database;

public partial class DataEntry_Division : BasePage
{
    protected int divisionId = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        message.Text = "";
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!this.IsPostBack)
        {
            BindGrid();
           
        }
        else
        {
            // check if a division id has been passed
            try
            {
                divisionId = int.Parse(Request["divisionId"]);
            }
            catch(Exception ex) {
                message.Text = ex.Message;
                message.ForeColor = Color.Green;
            }
        }
        if (GetLoggedinUser().UserType == 2)
        {
            add.Visible  = false;
            
        }
    }
    private void BindGrid()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select * from Lum_Erp_Division order by divisionName"))
            {
                divisionGrid.DataSource = reader;
                divisionGrid.DataBind();
            }
        }

    }

    protected void add_Click(object sender, EventArgs e)
    {
        bool spares = false;
        //if (chkSpares.Checked == true)
        //    spares = false;

        if (divisionId == -1)
        {
            // we are in add mode
            if (divisionName.Text.Trim().Length > 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                      int i= conn.ExecuteCommandProc("Lum_Erp_Stp_AddDivision", 
                            new SpParam("@divisionName", divisionName.Text.Trim(), SqlDbType.VarChar),
                            new SpParam("@spares", spares, SqlDbType.Bit));
                  if (i > 0)
                  {
                      message.Text = "Division Added : " + divisionName.Text;
                      message.ForeColor = Color.Green;
                      divisionName.Text = string.Empty;
                  }
                  else
                  {
                      message.Text = "Division Already Exists : " + divisionName.Text;
                      message.ForeColor = Color.Red;
                      divisionName.Text = string.Empty;
                  }
                }
            }
        }
        else
        {
            // we are in edit mode.
            if (divisionName.Text.Trim().Length > 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    string sql=string.Format(
                           @"if(not exists(select * from Lum_Erp_Division where divisionName ='{0}' and divisionId!={3}))
                            begin 
                                update Lum_Erp_Division set divisionName = '{1}' ,spares = {3} where divisionId = {3}
                            end",
                           DataUtility.CleanString(divisionName.Text.Trim()), 
                           DataUtility.CleanString(divisionName.Text.Trim()),
                           DataUtility.CleanString(divisionName.Text.Trim()), 
                           divisionId, divisionId,spares );
                   int i= conn.ExecuteCommand(sql);

                   if (i > 0)
                   {
                       add.Text = "Add";
                       message.Text = "Division Changed to : " + divisionName.Text;
                       message.ForeColor = Color.Green;
                       divisionName.Text = string.Empty;
                       divisionId = -1;
                   }
                   else
                   {
                       message.Text = "Division Already Exists : " + divisionName.Text;
                       message.ForeColor = Color.Red;
                   }

                }
            }

        }

        BindGrid();

        #region stored proc code
        //create proc [dbo].[AddDivision] @divisionName varchar(255)
        //as
        //begin
        //    if(not exists(select * from Division where divisionName = @divisionName))
        //        insert into Division(divisionName) values(@divisionName)
        //end
        #endregion
    }

    protected void divisionGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            if (e.CommandName.Equals("View"))
            {
                divisionId = (int)divisionGrid.DataKeys[e.Item.ItemIndex];
                LoadDivision();
                add.Visible = false;
            }
                        if (e.CommandName.Equals("Edit"))
                        {
                            divisionId = (int)divisionGrid.DataKeys[e.Item.ItemIndex];
                            LoadDivision();
                            add.Visible = true;
                        }
                        else if (e.CommandName.Equals("Delete"))
                        {
                            add.Visible = true;
                            divisionId = (int)divisionGrid.DataKeys[e.Item.ItemIndex];
                           
                                try
                                {
                                    conn.ExecuteCommand("delete from Lum_Erp_Division where divisionId = " + divisionId);
                                    message.Text = "Division deleted";
                                    message.ForeColor = Color.Green;
                                }
                                catch
                                {
                                    message.Text = "Division cannot be deleted because of existing related data";
                                    message.ForeColor = Color.Red;
                                }
                                divisionName.Text = string.Empty;
                                divisionId = -1;
                           
                            BindGrid();
                        }
              
            
        }
    }

    private void LoadDivision()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select divisionName,spares from Lum_Erp_Division where divisionId = '" + divisionId + "' order by divisionName"))
            {
                if (reader.Read())
                {
                    divisionName.Text = reader.GetString(0);
                    //if (Convert.ToBoolean(reader.GetBoolean(1)) == true )
                    //    chkSpares.Checked = true;
                    //else
                    //    chkSpares.Checked = false;
                    add.Text = "Update";
                    cancel.Visible = true;
                }
            }
        }
        add.Visible = true;
    }

    protected void cancel_Click(object sender, EventArgs e)
    {
        divisionName.Text = "";
        add.Text = "Add";
        divisionId = -1;
    }
    protected void divisionGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //LinkButton btnview = (LinkButton)e.Item.Cells[2].Controls[0];
            //LinkButton btnedit = (LinkButton)e.Item.Cells[3].Controls[0];
            //LinkButton btn = (LinkButton)e.Item.Cells[4].Controls[0];

            LinkButton btnview = (LinkButton)e.Item.Cells[1].Controls[0];
            LinkButton btnedit = (LinkButton)e.Item.Cells[2].Controls[0];
            LinkButton btn = (LinkButton)e.Item.Cells[3].Controls[0];
            btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this?')");
            btnview.CssClass = "GridLink";
            btn.CssClass = "GridLink";
            btnedit.CssClass = "GridLink";
            divisionName.Text = "";
            add.Text = "Add";
            divisionId = -1;

            if (GetLoggedinUser().UserType == 2)
            {
                btn.Visible = false;
                btnedit.Visible = false;
            }
        }
    }
}
