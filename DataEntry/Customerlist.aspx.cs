﻿using System.Data;
using System;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;
public partial class DataEntry_Customerlist : BasePage
{
    int customerId = -1;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }

    }
    private void BindGrid()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select * from Customer"))
            {
                customerGrid.DataSource = reader;
                customerGrid.DataBind();
            }
        }

    }
    protected void customerGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName.Equals("Edit"))
        {
            customerId = (int)customerGrid.DataKeys[e.Item.ItemIndex];

            Response.Redirect("Customer.aspx?customerId=" + customerId);

        }
        else if (e.CommandName.Equals("Delete"))
        {
            customerId = (int)customerGrid.DataKeys[e.Item.ItemIndex];
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                try
                {
                    conn.ExecuteCommand("delete from Customer where customerId = " + customerId);
                    message.Text = "Customer deleted";
                    message.ForeColor = Color.Green;
                }
                catch
                {
                    message.Text = "Customer cannot be deleted because of existing related data";
                    message.ForeColor = Color.Red;
                }

                customerId = -1;
            }
            BindGrid();
        }
    }
    protected void customerGrid_PreRender(object sender, System.EventArgs e)
    {
        for (int _index = 0; _index < customerGrid.Items.Count; _index++)
        {
            string str = customerGrid.Items[_index].Cells[1].Text;
            if (str == "0")
            {
                customerGrid.Items[_index].Cells[1].Text = "Main";
            }
            else
            {
                customerGrid.Items[_index].Cells[1].Text = "Branch";
            }
        }
    }
}
