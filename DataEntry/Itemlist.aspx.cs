﻿using System.Data;
using System;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;

public partial class DataEntry_Itemlist : BasePage
{
    int itemId = -1;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!IsPostBack)
        {
            BindGrid();
        }

    }
    private void BindGrid()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(@"SELECT I.itemId,D.divisionName,S.subdivisionName,I.itemCode  FROM Item I,Division D,Subdivision S

WHERE I.divisionId=D.divisionId AND I.subdivisionId=S.subdivisionId"))
            {
                itemGrid.DataSource = reader;
                itemGrid.DataBind();
            }
        }

    }
    protected void itemGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName.Equals("Edit"))
        {
            itemId = (int)itemGrid.DataKeys[e.Item.ItemIndex];

            Response.Redirect("Items.aspx?itemId=" + itemId);

        }
        else if (e.CommandName.Equals("Delete"))
        {
            itemId = (int)itemGrid.DataKeys[e.Item.ItemIndex];
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                try
                {
                    conn.ExecuteCommand("delete from Item where itemId = " + itemId);
                    conn.ExecuteCommand("delete from AttachedItemParts where itemId = " + itemId);
                    message.Text = "Item deleted";
                    message.ForeColor = Color.Green;
                }
                catch
                {
                    message.Text = "Item cannot be deleted because of existing related data";
                    message.ForeColor = Color.Red;
                }

                itemId = -1;
            }
            BindGrid();
        }
    }
    
}
