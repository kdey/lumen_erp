﻿<%@ Page Language="C#" MasterPageFile="~/DataEntry/DataEntry.master" AutoEventWireup="true" EnableEventValidation="false"
    CodeFile="Items.aspx.cs" Inherits="DataEntry_ItemMaster" Title="Item Master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

<script language="javascript">
function isNumberKey(evt)   //Checks textbox contains only numeric value
    {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        
        if (charCode > 31 && (charCode < 48 || charCode > 57))
        {
            return false;
        }
        return true;
    }
    function isNumberKey_W_Decimal(evt)   //Checks textbox contains only numeric value
    {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode != 46)) {
            return false;
        }
        return true;
    }
</script>

    <input type="hidden" name="itemId" value="<%=itemId%>" />
    <div class="PageMargin">
        <div class="PageHeading">
          Item Master </div>
        <div class="Line">
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
        </div>
        <!--Content Table Start-->
        <div class="PageContent">
               <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
               
               
                <tr><td width="16%" align="right" valign="middle"> Select Division&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="18%" align="left" valign="middle">
                 <asp:DropDownList ID="ddlDivision" CssClass="TextboxSmall" runat="server" 
                     Width="150px" onselectedindexchanged="ddlDivision_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
            </td>
            <td width="16%" align="right" valign="middle"> Select Subdivision&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="17%" align="left" valign="middle">
                <asp:DropDownList CssClass="TextboxSmall" ID="ddlSubdivision"  runat="server" Width="150px"></asp:DropDownList>
            </td>
            <td width="16%" align="right" valign="middle"> Item Name/Part Code&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="17%" align="left" valign="middle">
                <asp:TextBox ID="itemCode" runat="server"   CssClass="TextboxSmall" MaxLength="50" />
            </td>
            
            <%--<cc1:CascadingDropDown ID="CascadingDropDown3"
            runat="server" 
            TargetControlID="ddlDivision" LoadingText="[Loading...]"
            Category="Division"
            PromptText="Select a Division"
            ServicePath ="~/divisionService.asmx"
            ServiceMethod="GetDivision" >   </cc1:CascadingDropDown>
              
           <cc1:CascadingDropDown ID="CascadingDropDown1"
                runat="server" ParentControlID = "ddlDivision" 
                TargetControlID="ddlSubdivision" LoadingText="[Loading...]"
                Category="subDivision"
                PromptText="Select a subDivision"
                ServicePath ="~/divisionService.asmx"
                ServiceMethod="GetSubdividsionForDivision" >   </cc1:CascadingDropDown>--%>
          
        </tr>
               
                <tr><td>&nbsp</td>
            <td> <asp:RequiredFieldValidator ControlToValidate="ddlDivision" ErrorMessage="Select Division"
                            ID="divisionValidator" InitialValue="0" runat="server" /></td>
            <td>&nbsp</td>
            <td><asp:RequiredFieldValidator ControlToValidate="ddlSubdivision" ErrorMessage="Select Sub Division"
                            ID="RequiredFieldValidator1" InitialValue="0" runat="server" /></td>
            <td colspan="2"><asp:RequiredFieldValidator ControlToValidate="itemCode" ErrorMessage="Field cannot be empty"
                    ID="RequiredFieldValidator6" runat="server" /></td>
        </tr>
               
                     <tr><td width="16%" align="Right" valign="middle">Description</td>
            <td width="18%" align="left" valign="middle">
                 <asp:TextBox ID="itemDescription" runat="server" 
                            onkeypress="return limiter(this.id);" TextMode="MultiLine" 
                            CssClass="TextboxMiddle" Height="50px" />
            </td>
            <td width="16%" align="Right" valign="middle">Select UOM&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="17%" align="left" valign="middle">
                <asp:DropDownList  ID="ddlUom" CssClass="TextboxSmall" runat="server" Width="150px"></asp:DropDownList>
            </td>
            <td width="16%" align="Right" valign="middle">  VAT(%)</td>
            <td width="17%" align="left" valign="middle">
               <asp:TextBox ID="vat" runat="server"   CssClass="TextboxSmall" onkeypress="return isNumberKey_W_Decimal(event);" />
            </td>
        </tr>
               
                <tr><td>&nbsp</td>
            <td></td>
            <td>&nbsp</td>
            <td>   <asp:RequiredFieldValidator ControlToValidate="ddlUom" ErrorMessage="Select UOM"
                            ID="RequiredFieldValidator2" InitialValue="0" runat="server" /></td>
            <td colspan="2"> <asp:RangeValidator ID="RangeValidator1" ControlToValidate="vat" runat="server" ErrorMessage="Please give correct decimal number <=100" Type="Double" MaximumValue="100"></asp:RangeValidator></td>
        </tr>
           
                      <tr><td width="16%" align="Right" valign="middle">     CST (%) </td>
            <td width="18%" align="left" valign="middle">
                  <asp:TextBox ID="cst" runat="server"   CssClass="TextboxSmall"  onkeypress="return isNumberKey_W_Decimal(event);" 
                      ontextchanged="cst_TextChanged" />
            </td>
            <td width="16%" align="Right" valign="middle"> CST Against Form C (%)</td>
            <td width="17%" align="left" valign="middle"><asp:TextBox ID="cstAgainstFormC" runat="server"   CssClass="TextboxSmall" onkeypress="return isNumberKey_W_Decimal(event);"  />
            </td>
            <td width="16%" align="Right" valign="middle"> MRP&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="17%" align="left" valign="middle">
               <asp:TextBox ID="MRP" runat="server"   CssClass="TextboxSmall" onkeypress="return isNumberKey_W_Decimal(event);"  />
                <br />
            </td>
        </tr>
               
                <tr><td>&nbsp</td>
            <td></td>
            <td>&nbsp</td>
            <td>    
            <asp:GridView ID="itemsGrid"    ItemStyle-CssClass="GridData" 
                    CellPadding="3" CellSpacing="3" 
                            AutoGenerateColumns="false" runat="server" OnDataBound="itemsGrid_DataBound" 
                            HeaderStyle-CssClass="GridHeading" 
                    onrowcommand="itemsGrid_RowCommand" 
                    onselectedindexchanged="itemsGrid_SelectedIndexChanged">
        
                    <Columns>
                
                     <asp:TemplateField HeaderText="Select Parts">
                    <ItemTemplate>
                     <asp:DropDownList CssClass="TextboxSmall" ID="ddlItems"  runat="server"   ></asp:DropDownList>
                    </ItemTemplate>
                      </asp:TemplateField>
                     <asp:TemplateField HeaderText="No of Parts">
                    <ItemTemplate>
                     <asp:TextBox CssClass="TextboxSmall" ID="ItemsQty"  runat="server"  onkeypress="return isNumberKey(event);"  />
                    </ItemTemplate>
                      </asp:TemplateField>
                                
                      <asp:TemplateField>
                    <ItemTemplate>
                     <asp:LinkButton  ID="lnkDelete" CssClass="GridLink" Text="Delete" CommandName="Del" CommandArgument='<%#Eval("rowId")%>'  runat="server"   ></asp:LinkButton>
                    </ItemTemplate>
                      </asp:TemplateField>
                      
                      
                    
                    </Columns>

<HeaderStyle CssClass="GridHeading"></HeaderStyle>
        </asp:GridView>    <asp:RangeValidator ID="RangeValidator2" ControlToValidate="cst" runat="server" ErrorMessage="Please give correct decimal number <=100" Type="Double" MaximumValue="100"></asp:RangeValidator></td>
            <td colspan="2"> 
         <cc1:filteredtextboxextender ID="FilteredTextBoxExtender1" runat="server" 
                    TargetControlID="MRP" ValidChars="1234567890." FilterMode="ValidChars" 
                    FilterType="Custom"></cc1:filteredtextboxextender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                    ControlToValidate="MRP" ErrorMessage="Please Submit MRP"></asp:RequiredFieldValidator>
                    </td>
         
        </tr>
        
                
                  
                   <tr>
                    <td   align="center" colspan="6" valign="middle">
                       Add Detachable Parts
                    </td>
                </tr>
                <tr>
                <td align="center" colspan="6" valign="middle"> &nbsp;</td>
                </tr>
       
     <tr>
     <td align="center" colspan="6" valign="middle">
        <asp:Button ID="btnAddSpares" runat="server" Text="Add New Parts" 
             CssClass="Button" OnClick="btnAddSpares_Click" Width="147px" />
                      
                                                                      
                    </td>
                </tr>
                <tr>
                    <td   align="center" colspan="6" valign="middle">
                        <asp:Button ID="add" Text="Add" runat="server" OnClick="add_Click" CssClass="Button" />
                        &nbsp;
                        <asp:Button ID="cancel" Text="Cancel" runat="server" OnClick="cancel_Click" CausesValidation="false"
                            CssClass="Button" />
                    </td>
                </tr>
                <tr>
                   <td   align="center" colspan="6" valign="middle">
            <asp:Label ID="message" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
        <!--Content Table End-->
       <div class="Line">
        </div>
        <!--Content Table Start-->
        <div class="PageContent">
        
            <table >
                <tr>
                    <td>
                        Item Name/Part Code:
                        <asp:TextBox ID="txtSearch" runat="server" CssClass="TextboxSmall"></asp:TextBox>
                    &nbsp;
                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="Button" 
                            onclick="btnSearch_Click" CausesValidation="False" />&nbsp;
                         <asp:Button ID="btnShowall" runat="server" Text="Show All" CssClass="Button" 
                            onclick="btnShowall_Click" CausesValidation="False" />
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                         &nbsp;</td>
                </tr>
            </table>
        
        </div>
               <asp:DataGrid ID="itemGrid" runat="server" AutoGenerateColumns="False" DataKeyField="itemId"
            CellPadding="0" OnItemCommand="itemGrid_ItemCommand" HeaderStyle-CssClass="GridHeading"
            ItemStyle-CssClass="GridData" onitemcreated="itemGrid_ItemCreated" 
                GridLines="None" Width="100%" >
<ItemStyle CssClass="GridData"></ItemStyle>
            <Columns>
                 <asp:BoundColumn DataField="itemCode" HeaderText="Item Code" 
                    HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Left">
<HeaderStyle Font-Bold="True"></HeaderStyle>
                     <ItemStyle HorizontalAlign="Left" />
                </asp:BoundColumn>
                <asp:BoundColumn DataField="divisionName" HeaderText="Divistion" 
                    HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Left">
<HeaderStyle Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="subdivisionName" HeaderText="Subdivision" 
                    HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Left">
<HeaderStyle Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                
                 <asp:ButtonColumn  ButtonType="LinkButton" CausesValidation="false" 
                    CommandName="View" Text="View" 
                 ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="GridLink" 
                    ItemStyle-Width="10%" >
<ItemStyle HorizontalAlign="Left" CssClass="GridLink" Width="10%"></ItemStyle>
                </asp:ButtonColumn>
                
                <asp:ButtonColumn ButtonType="LinkButton" CommandName="Edit" Text="Edit" 
                    ItemStyle-CssClass="GridLink" ItemStyle-HorizontalAlign="Left">
<ItemStyle CssClass="GridLink"></ItemStyle>
                </asp:ButtonColumn>
                <asp:ButtonColumn ButtonType="LinkButton" CommandName="Delete" Text="Delete" 
                    ItemStyle-CssClass="GridLink" ItemStyle-HorizontalAlign="Left">
<ItemStyle CssClass="GridLink"></ItemStyle>
                </asp:ButtonColumn>
            </Columns>

<HeaderStyle CssClass="GridHeading"></HeaderStyle>
        </asp:DataGrid>
            
       
        <!--Content Table End-->
        <div class="Line">
        </div>
        <!--Grid Table Start-->
     
        <!--Grid Table End-->
        <div class="Line">
        </div>
        <!--Page Body End-->
    </div>
</asp:Content>
