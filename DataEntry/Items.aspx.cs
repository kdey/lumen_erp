﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
public partial class DataEntry_ItemMaster : BasePage
{
    protected int itemId = -1;
    public DataTable aTable = new DataTable("Spares");
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Params["itemId"] != null)
        {
            itemId = int.Parse(Request.Params["itemId"]);
        }
        message.Text = "";
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!IsPostBack)
        {
            bindControl();
            BindGrid();
            try
            {

                if (itemId != -1)
                {
                    LoadItem();
                    BindAttachParts();
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
                message.ForeColor = Color.Green;
            }
            
        }
        if (GetLoggedinUser().UserType == 2)
        {
            add.Visible  = false;
            btnAddSpares.Visible  = false;
        }

    }
    protected void add_Click(object sender, EventArgs e)
    {
        Validate();
        if (IsValid)
        {
            if (itemId == -1)
            {
                InsertItem();
            }
            else
            {
                UpdateItem();
            }
        }
    }
    public void UpdateItem()
    {

        string _vat = "0";
        string _cst = "0";
        string _cstAgainstFormC = "0";
        if (vat.Text != "")
        {
            _vat = vat.Text.Trim();
        }
        if (cst.Text != "")
        {
            _cst = cst.Text.Trim();
        }
        if (cstAgainstFormC.Text != "")
        {
            _cstAgainstFormC = cstAgainstFormC.Text.Trim();
        }

        SpParam[] _Sparray = new SpParam[9];

        _Sparray[0] = new SpParam("@itemCode", itemCode.Text.Trim(), SqlDbType.VarChar);
        _Sparray[1] = new SpParam("@subdivisionId", Convert.ToInt32(ddlSubdivision.SelectedValue), SqlDbType.Int);
        _Sparray[2] = new SpParam("@itemDescription", itemDescription.Text.Trim(), SqlDbType.VarChar);
        _Sparray[3] = new SpParam("@uomId", Convert.ToInt32(ddlUom.SelectedValue), SqlDbType.Int);
        _Sparray[4] = new SpParam("@vat", Convert.ToDecimal(_vat), SqlDbType.Decimal);
        _Sparray[5] = new SpParam("@cst", Convert.ToDecimal(_cst), SqlDbType.Decimal);
        _Sparray[6] = new SpParam("@cstAgainstFormC", Convert.ToDecimal(_cstAgainstFormC), SqlDbType.Decimal);
        _Sparray[7] = new SpParam("@itemId", itemId, SqlDbType.Int);
        _Sparray[8] = new SpParam("@MRP", Convert.ToDecimal(MRP.Text), SqlDbType.Decimal);

        try
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {


                int _i = conn.ExecuteCommandProc("Lum_Erp_Stp_UpdateItem", _Sparray);

                if (_i >= 1)
                {
                    InsertAttachParts(itemId);
                    message.Text = "Item  Updated : " + itemCode.Text;
                    message.ForeColor = Color.Green;
                    
                    ResetControl();

                }

            }
        }
        catch (Exception ex)
        {
            message.Text = ex.Message;
        }

    }
    public void InsertItem()
    {

        string _vat = "0";
        string _cst = "0";
        string _cstAgainstFormC = "0";
        if (vat.Text != "")
        {
            _vat = vat.Text.Trim();
        }
        if(cst.Text!="")
        {
            _cst=cst.Text.Trim();
        }
        if (cstAgainstFormC.Text!="")
        {
            _cstAgainstFormC = cstAgainstFormC.Text.Trim();
        }

        SpParam[] _Sparray = new SpParam[8];

        _Sparray[0] = new SpParam("@itemCode", itemCode.Text.Trim(), SqlDbType.VarChar);
        _Sparray[1] = new SpParam("@subdivisionId", Convert.ToInt32(ddlSubdivision.SelectedValue), SqlDbType.Int);
        _Sparray[2] = new SpParam("@itemDescription", itemDescription.Text.Trim(), SqlDbType.VarChar);
        _Sparray[3] = new SpParam("@uomId",Convert.ToInt32(ddlUom.SelectedValue), SqlDbType.Int);
        _Sparray[4] = new SpParam("@vat", Convert.ToDecimal(_vat), SqlDbType.Decimal);
        _Sparray[5] = new SpParam("@cst", Convert.ToDecimal(_cst), SqlDbType.Decimal);
        _Sparray[6] = new SpParam("@cstAgainstFormC", Convert.ToDecimal(_cstAgainstFormC), SqlDbType.Decimal);
        _Sparray[7] = new SpParam("@MRP", Convert.ToDecimal(MRP.Text), SqlDbType.Decimal);


        try
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {


                object _obj = conn.ExecuteScalarProc("Lum_Erp_Stp_AddItem", _Sparray);

                if (_obj != null)
                {
                    int _itemId = Convert.ToInt32(_obj);
                    InsertAttachParts(_itemId);

                    message.Text = "Item  Added : " + itemCode.Text;
                    message.ForeColor = Color.Green;
                    ResetControl();

                }
                else
                {
                    message.Text = "Item  Code Already Exists : " + itemCode.Text;
                    message.ForeColor = Color.Red;
                    ResetControl();
                }

            }
        }
        catch (Exception ex)
        {
            message.Text = ex.Message;
        }
        BindGrid();
    }


  
    public void ResetControl()
    {
        itemCode.Text = string.Empty;
        itemDescription.Text = string.Empty;
        vat.Text = string.Empty;
        cst.Text = string.Empty;
        cstAgainstFormC.Text = string.Empty;
        ddlUom.SelectedIndex = 0;
        ddlDivision.SelectedIndex = 0;
        ddlSubdivision.Items.Clear();
        ddlSubdivision.Items.Add("--Select--");
        ddlSubdivision.SelectedIndex = 0;
        itemsGrid.DataSource = null;
        itemsGrid.DataBind();
        add.Text = "Add";
        MRP.Text = "";
        itemId = -1;
    }
    private void LoadItem()
    {

        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(@"select I.*,D.divisionId from Lum_Erp_Item I inner join Lum_Erp_subDivision S on S.subdivisionId=I.subdivisionId
               inner join Lum_Erp_Division D on D.divisionId=S.divisionId where itemId = " + itemId))
            {
                if (reader.Read())
                {
                    ddlDivision.SelectedValue = Convert.ToString(reader["divisionId"]);
                    BindListControl _control = new BindListControl();
                    _control.BindSubDivision(ddlSubdivision, Convert.ToString(reader["divisionId"]));
                    ddlSubdivision.SelectedValue=Convert.ToString(reader["subdivisionId"]);
                    itemCode.Text = Convert.ToString(reader["itemCode"]);
                    itemDescription.Text = Convert.ToString(reader["itemDescription"]);
                    ddlUom.SelectedValue = Convert.ToString(reader["uomId"]);
                    vat.Text = Convert.ToString(reader["vat"]);
                    cst.Text = Convert.ToString(reader["cst"]);
                    cstAgainstFormC.Text = Convert.ToString(reader["cstAgainstFormC"]);
                    add.Text = "Update";
                    cancel.Visible = true;
                    MRP.Text = Convert.ToString(reader["MRP"]);
                }
            }
        }
    }

    protected void cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("items.aspx");
       // ResetControl();
    }
    public void bindControl()
    {
        BindListControl _control = new BindListControl();
        _control.BindDivision(ddlDivision);
        _control.BindpOUM(ddlUom);
    }
    protected void ddlDivision_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindListControl _control = new BindListControl();
        _control.BindSubDivision(ddlSubdivision, ddlDivision.SelectedValue);
    }
    public void CreateDataTable()
    {
        DataRow dtRow;
        DataColumn dtCol;

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.Int");
        dtCol.ColumnName = "rowId";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "Item";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "ItemsQty";
        aTable.Columns.Add(dtCol);

        dtRow = aTable.NewRow();
        dtRow["rowId"] = 0;
        dtRow["Item"] = "";
        dtRow["ItemsQty"] = "22";

        aTable.Rows.Add(dtRow);

        itemsGrid.DataSource = aTable;
        itemsGrid.DataBind();
    }
    protected void itemsGrid_DataBound(object sender, EventArgs e)
    {
        for (int _index = 0; _index < itemsGrid.Rows.Count; _index++)
        {
            DropDownList _ddlItems = (DropDownList)itemsGrid.Rows[_index].FindControl("ddlItems");
            TextBox _ItemsQty = (TextBox)itemsGrid.Rows[_index].FindControl("ItemsQty");
            int _Qty = 1;
            if (_ItemsQty.Text != "")
                _Qty = Convert.ToInt32(_ItemsQty.Text);

            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                string sqlQuery = "";
                if (itemId > 0)
                {
                    sqlQuery = string.Format(@"SELECT itemId,itemCode ,1 AS noofitem FROM Lum_Erp_Item I,Lum_Erp_Division D,Lum_Erp_SubDivision SD 
	                                            WHERE I.subdivisionId = SD.subdivisionId AND SD.divisionId = D.divisionId
	                                            AND d.spares = 0 AND  itemId!={0}", itemId);
                }
                else
                {
                    sqlQuery = string.Format(@"SELECT itemId,itemCode ,1 AS noofitem FROM Lum_Erp_Item I,Lum_Erp_Division D,Lum_Erp_SubDivision SD 
	                                            WHERE I.subdivisionId = SD.subdivisionId AND SD.divisionId = D.divisionId
	                                            AND d.spares = 0");
                }
                DataSet _Ds = conn.GetDataSet(sqlQuery, "ITEMS");
                _ddlItems.ClearSelection();
                _ddlItems.Items.Clear();
                _ddlItems.AppendDataBoundItems = true;
                _ddlItems.Items.Add("Please Select");
                _ddlItems.Items[0].Value = "0";
                _ddlItems.DataTextField = _Ds.Tables[0].Columns["itemCode"].ToString();
                _ddlItems.DataValueField = _Ds.Tables[0].Columns["itemId"].ToString();
                _ddlItems.DataSource = _Ds;
                _ddlItems.DataBind();
                _ddlItems.SelectedValue = aTable.Rows[_index]["Item"].ToString();
                _ItemsQty.Text = aTable.Rows[_index]["ItemsQty"].ToString();
            }
        }
    }
    protected void btnAddSpares_Click(object sender, EventArgs e)
    {
        aTable.Rows.Clear();
        DataRow dtRow;
        DataColumn dtCol;
        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.Int32");
        dtCol.ColumnName = "rowId";
        aTable.Columns.Add(dtCol);
        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "Item";

        aTable.Columns.Add(dtCol);
        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "ItemsQty";

        aTable.Columns.Add(dtCol);
        int _Qty = 1;

        for (int _index = 0; _index < itemsGrid.Rows.Count; _index++)
        {
            DropDownList _ddlItems = (DropDownList)itemsGrid.Rows[_index].FindControl("ddlItems");
            TextBox _ItemsQty = (TextBox)itemsGrid.Rows[_index].FindControl("ItemsQty");
            
            if (_ItemsQty.Text != "")
                _Qty = Convert.ToInt32(_ItemsQty.Text);

            dtRow = aTable.NewRow();
            dtRow["rowId"] = _index;
            dtRow["Item"] = _ddlItems.SelectedValue;
            dtRow["ItemsQty"] = _Qty.ToString();
            aTable.Rows.Add(dtRow);

        }
        dtRow = aTable.NewRow();
        if (itemsGrid.Rows.Count>0)
        {
             dtRow["rowId"] = itemsGrid.Rows.Count;
        }
        else
        {
        dtRow["rowId"] = 0;
        }
        dtRow["Item"] = "";
        dtRow["ItemsQty"] = "1";
        aTable.Rows.Add(dtRow);
        itemsGrid.DataSource = aTable;
        itemsGrid.DataBind();
    }
    protected void itemsGrid_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        aTable.Rows.Clear();
        DataRow dtRow;
        DataColumn dtCol;
        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.Int32");
        dtCol.ColumnName = "rowId";
        aTable.Columns.Add(dtCol);
        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "Item";
        aTable.Columns.Add(dtCol);
        
        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "ItemsQty";

        aTable.Columns.Add(dtCol);
        int _Qty = 1;
        if (e.CommandName.Equals("Del"))
        {
            for (int _index = 0; _index < itemsGrid.Rows.Count; _index++)
            {
                DropDownList _ddlItems = (DropDownList)itemsGrid.Rows[_index].FindControl("ddlItems");
                TextBox _ItemsQty = (TextBox)itemsGrid.Rows[_index].FindControl("ItemsQty");

                if (_ItemsQty.Text != "")
                    _Qty = Convert.ToInt32(_ItemsQty.Text);
                dtRow = aTable.NewRow();
                dtRow["rowId"] = _index;
                dtRow["Item"] = _ddlItems.SelectedValue;
                dtRow["ItemsQty"] = _Qty.ToString(); 
                aTable.Rows.Add(dtRow);

            }
            int rowId = 0;
            if (itemsGrid.Rows.Count != 1)
            {
                 rowId = Convert.ToInt32(e.CommandArgument);
            }
           
            aTable.Rows[rowId].Delete();
            itemsGrid.DataSource = aTable;
            itemsGrid.DataBind();
        }
    }

    public void InsertAttachParts(int _itemId)
    {
        if (itemId != -1)
        {
            try
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    string sqlQuery = string.Format("DELETE FROM Lum_Erp_AttachedItemParts WHERE itemId={0}", itemId);
                    int i = conn.ExecuteCommand(sqlQuery);
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }
        if (itemsGrid.Rows.Count > 0)
        {

            for (int _index = 0; _index < itemsGrid.Rows.Count; _index++)
            {
                DropDownList _ddlItems = (DropDownList)itemsGrid.Rows[_index].FindControl("ddlItems");
                TextBox _ItemsQty = (TextBox)itemsGrid.Rows[_index].FindControl("ItemsQty");
                int _Qty = 1;
                if (_ItemsQty.Text != "")
                    _Qty = Convert.ToInt32(_ItemsQty.Text);

                if (_ddlItems.SelectedIndex != 0)
                {
                    for (int _Qindex = 0; _Qindex < _Qty; _Qindex++)
                    {
                        try
                        {
                            using (IConnection conn = DataConnectionFactory.GetConnection())
                            {
                                string sqlQuery = string.Format("INSERT INTO Lum_Erp_AttachedItemParts(itemId,sparesId) VALUES({0},{1})", _itemId, _ddlItems.SelectedValue);
                                int i = conn.ExecuteCommand(sqlQuery);
                            }
                        }
                        catch (Exception ex)
                        {
                            message.Text = ex.Message;
                        }
                    }

                }
            }

        }
    }

    public void BindAttachParts()
    {
        aTable.Rows.Clear();
        DataRow dtRow;
        DataColumn dtCol;
        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.Int32");
        dtCol.ColumnName = "rowId";
        aTable.Columns.Add(dtCol);
        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "Item";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "ItemsQty";
        aTable.Columns.Add(dtCol);

        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            try
            {
                using (DataSet _ds = conn.GetDataSet("select itemid,sparesId,count(sparesId)NoOFSpares 	from Lum_Erp_AttachedItemParts where Itemid = " + itemId + "	group by itemid,sparesId ", "spares"))
                {

                    if (_ds.Tables[0].Rows.Count > 0)
                    {
                        for (int _index = 0; _index < _ds.Tables[0].Rows.Count; _index++)
                        {
                            dtRow = aTable.NewRow();
                            dtRow["rowId"] = _index;
                            dtRow["Item"] = Convert.ToString(_ds.Tables[0].Rows[_index]["sparesId"]); ;
                            dtRow["ItemsQty"] = Convert.ToString(_ds.Tables[0].Rows[_index]["NoOFSpares"]); ;
                            aTable.Rows.Add(dtRow);
                        }
                        itemsGrid.DataSource = aTable;
                        itemsGrid.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }
            
        
        
    }

    private void BindGrid()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(@"SELECT I.itemId,D.divisionName,S.subdivisionName,I.itemCode  FROM Lum_Erp_Item I
                                inner join Lum_Erp_Subdivision S on S.subdivisionId=I.subdivisionId
                                inner join Lum_Erp_Division D on D.divisionId=S.divisionId  order by I.itemCode"))
            {
                itemGrid.DataSource = reader;
                itemGrid.DataBind();
            }
        }

    }
    protected void itemGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            if (e.CommandName.Equals("View"))
            {
                itemId = (int)itemGrid.DataKeys[e.Item.ItemIndex];

                LoadItem();
                BindAttachParts();
                add.Visible = false;
                btnAddSpares.Visible = false;
            }
            if (e.CommandName.Equals("Edit"))
            {
                add.Visible = true;
                itemId = (int)itemGrid.DataKeys[e.Item.ItemIndex];

                LoadItem();
                BindAttachParts();

            }
            else if (e.CommandName.Equals("Delete"))
            {
                add.Visible = true;
                itemId = (int)itemGrid.DataKeys[e.Item.ItemIndex];

                try
                {
                    conn.ExecuteCommand("delete from Lum_Erp_Item where itemId = " + itemId);
                    conn.ExecuteCommand("delete from Lum_Erp_AttachedItemParts where itemId = " + itemId);
                    message.Text = "Item deleted";
                    message.ForeColor = Color.Green;
                }
                catch
                {
                    message.Text = "Item cannot be deleted because of existing related data";
                    message.ForeColor = Color.Red;
                }

                itemId = -1;

                BindGrid();
            }

        }
          
       
    }

    protected void itemGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton btnview = (LinkButton)e.Item.Cells[3].Controls[0];

            btnview.CssClass = "GridLink";
            LinkButton btnedit = (LinkButton)e.Item.Cells[4].Controls[0];
            LinkButton btn = (LinkButton)e.Item.Cells[5].Controls[0];
            btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this?')");
            btn.CssClass = "GridLink";
            btnedit.CssClass = "GridLink";
            if (GetLoggedinUser().UserType == 2)
            {
                btn.Visible = false;
                btnedit.Visible = false;
            }
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(@"SELECT I.itemId,D.divisionName,S.subdivisionName,I.itemCode  FROM Lum_Erp_Item I
                            inner join Lum_Erp_Subdivision S on S.subdivisionId=I.subdivisionId
                            inner join Lum_Erp_Division D on D.divisionId=S.divisionId where I.itemCode like '" + txtSearch.Text.Trim() + "%' order by I.itemCode"

                            ))
            {
                itemGrid.DataSource = reader;
                itemGrid.DataBind();
            }
        }
        ResetControl();
        txtSearch.Text = "";
    }
    protected void btnShowall_Click(object sender, EventArgs e)
    {
        BindGrid();
        ResetControl();
        txtSearch.Text = "";

    }
    protected void cst_TextChanged(object sender, EventArgs e)
    {

    }
    protected void itemsGrid_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
