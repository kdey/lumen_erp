﻿<%@ Page Title="Vendor Master" Language="C#" MasterPageFile="~/DataEntry/DataEntry.master" AutoEventWireup="true" CodeFile="Vendor.aspx.cs" Inherits="DataEntry_Vendor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <input type="hidden" name="vendorId" value="<%=vendorId%>" />
    <div class="PageMargin">
        <div class="PageHeading">Vendor <%--<div class="RightDiv"><a href="VendorList.aspx" class="GridLink">Vendor List</a></div>--%></div>
        <div class="Line"></div>
        <!--Content Table Start-->
        <div class="PageContent">
        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
        <tr><td align="right" valign="middle" width="16%">Vendor Name&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td  align="left" valign="middle" width="16%" style="margin-left: 920px">
                <asp:TextBox ID="vendorName" runat="server"   CssClass="TextboxSmall" 
                    MaxLength="50" TabIndex="1" />                    
            </td>
            <td  align="right" valign="middle" width="16%">Contact Person Name</td>
            <td  align="left" valign="middle" width="16%">
                <asp:TextBox ID="contactPersonName" runat="server" CssClass="TextboxSmall" 
                    MaxLength="50" TabIndex="2" />
                    <%--<asp:RequiredFieldValidator ControlToValidate="contactPersonName" ErrorMessage="Field cannot be empty"
                        ID="RequiredFieldValidator6" runat="server" />--%>
            </td>
            <td  align="right" valign="middle" width="16%">Contact No</td>
            <td  align="left" valign="middle" width="16%">
                <asp:TextBox ID="contactNo" runat="server" CssClass="TextboxSmall" 
                    MaxLength="50" TabIndex="3" />
                   <%-- <asp:RequiredFieldValidator ControlToValidate="contactNo" ErrorMessage="Field cannot be empty"
                        ID="RequiredFieldValidator7" runat="server" />--%>
            </td>
        </tr>
        <tr><td>&nbsp;</td>
            <td><asp:RequiredFieldValidator ControlToValidate="vendorName" ErrorMessage="Field cannot be empty"
                        ID="RequiredFieldValidator1" runat="server" /></td>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr><td  align="right" valign="middle">Address1&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td  align="left" valign="middle">
                <asp:TextBox ID="address1" TextMode="MultiLine" Height="50px" 
                    onkeypress="return limiter(this.id);" runat="server" 
                    CssClass="TextAreaSmall" TabIndex="4" />
            </td>
            <td  align="right" valign="middle">Address2</td>
            <td  align="left" valign="middle">
                <asp:TextBox ID="address2" runat="server" TextMode="MultiLine" onkeypress="return limiter(this.id);"
                    CssClass="TextAreaSmall" Height="50px" MaxLength="250" TabIndex="5" />                   
            </td>
            <td  align="right" valign="middle">Zip Code&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td  align="left" valign="middle">
                <asp:TextBox ID="zipCode" runat="server" CssClass="TextboxSmall" MaxLength="7" 
                    TabIndex="6" />
                  <br />
                   <asp:RegularExpressionValidator ID="quanityRegexValidator" runat="server"
                    Display="Dynamic"  ErrorMessage="Zip Code be a integer"
                    Text="" ControlToValidate="zipCode" ValidationExpression="\d+" />
            </td>               
        </tr>
        <tr><td>&nbsp;</td>
            <td><asp:RequiredFieldValidator ControlToValidate="address1" TextMode="MultiLine" ErrorMessage="Field cannot be empty"
                    ID="RequiredFieldValidator2" runat="server" /></td>
            <td colspan="3">&nbsp;</td>
            <td><asp:RequiredFieldValidator ControlToValidate="zipCode" ErrorMessage="Field cannot be empty"
                    ID="nameValidator" runat="server" /></td>
        </tr>
        <tr><td  align="right" valign="middle">Phone No&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td  align="left" valign="middle">
                <asp:TextBox ID="phoneNo" runat="server" CssClass="TextboxSmall" MaxLength="50" 
                    TabIndex="7" />
            </td>
            <td  align="right" valign="middle">Mobile No</td>
            <td  align="left" valign="middle">
                <asp:TextBox ID="mobileNo" runat="server" CssClass="TextboxSmall" 
                    MaxLength="50" TabIndex="8" />
            </td>
            <td  align="right" valign="middle">FAX No</td>
            <td  align="left" valign="middle">
                <asp:TextBox ID="faxNo" runat="server" CssClass="TextboxSmall" MaxLength="50" 
                    TabIndex="9" />
              <%--  <asp:RequiredFieldValidator ControlToValidate="zipCode" ErrorMessage="Field cannot be empty"
                    ID="RequiredFieldValidator11" runat="server" />--%>
            </td>
        </tr>
        <tr><td>&nbsp;</td>
            <td><asp:RequiredFieldValidator ControlToValidate="phoneNo" ErrorMessage="Field cannot be empty"
                    ID="RequiredFieldValidator3" runat="server" /></td>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr><td  align="right" valign="middle">Email Id</td>
            <td  align="left" valign="middle">
                <asp:TextBox ID="emailId" runat="server" CssClass="TextboxSmall" MaxLength="50" 
                    TabIndex="10" />
            </td>
            <td  align="right" valign="middle">WebSite</td>
            <td  align="left" valign="middle">
                <asp:TextBox ID="webSite" runat="server" CssClass="TextboxSmall" MaxLength="50" 
                    TabIndex="11" />
            </td>
            <td  align="right" valign="middle">PAN No</td>
            <td  align="left" valign="middle">
                <asp:TextBox ID="panNo" runat="server" CssClass="TextboxSmall" MaxLength="10" 
                    TabIndex="12" />
              <%--  <asp:RequiredFieldValidator ControlToValidate="zipCode" ErrorMessage="Field cannot be empty"
                    ID="RequiredFieldValidator11" runat="server" />--%>
            </td>
        </tr>
        <tr><td>&nbsp</td>
            <td><asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                    ErrorMessage="Invalid Email id" ControlToValidate="emailId" 
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></td>
            <td>&nbsp</td>
            <td><asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                    ErrorMessage="Invalid URL" ControlToValidate="webSite" 
                    ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?"></asp:RegularExpressionValidator></td>
            <td colspan="2">&nbsp</td>
        </tr>
        <tr><td  align="right" valign="middle">VAT Registration No</td>
            <td  align="left" valign="middle">
                <asp:TextBox ID="VATRegistrationNo" runat="server" CssClass="TextboxSmall" 
                    MaxLength="50" TabIndex="13" />
               <%-- <asp:RequiredFieldValidator ControlToValidate="VATRegistrationNo" ErrorMessage="Field cannot be empty"
                    ID="RequiredFieldValidator8" runat="server" />--%>
            </td>
            <td  align="right" valign="middle">CST Registration No</td>
            <td  align="left" valign="middle">
                <asp:TextBox ID="CSTRegistrationNo" runat="server" CssClass="TextboxSmall" 
                    MaxLength="50" TabIndex="14" />
                <%--<asp:RequiredFieldValidator ControlToValidate="zipCode" ErrorMessage="Field cannot be empty"
                    ID="RequiredFieldValidator9" runat="server" />--%>
            </td>
            <td  align="right" valign="middle">ST Registration No</td>
            <td  align="left" valign="middle">
                <asp:TextBox ID="serviceTaxRegNo" runat="server" CssClass="TextboxSmall" 
                    MaxLength="50" TabIndex="15" />
               <%-- <asp:RequiredFieldValidator ControlToValidate="zipCode" ErrorMessage="Field cannot be empty"
                    ID="RequiredFieldValidator10" runat="server" />--%>
            </td>
        </tr>
        <tr><td  align="right" valign="middle">Other Details</td>
            <td  align="left" valign="middle">
                <asp:TextBox ID="otherDetails" TextMode="MultiLine" Height="50px"  onkeypress="return limiter(this.id);"
                    runat="server" CssClass="TextAreaSmall" MaxLength="250" TabIndex="16" />
              <%--  <asp:RequiredFieldValidator ControlToValidate="zipCode" ErrorMessage="Field cannot be empty"
                    ID="RequiredFieldValidator11" runat="server" />--%>
            </td>
            <td  align="right" valign="middle">Acount Status</td>
            <td  align="left" valign="middle">
                <asp:RadioButtonList ID="rbstatus" AutoPostBack="true" runat="server" RepeatDirection="Horizontal" 
                    onselectedindexchanged="rbstatus_SelectedIndexChanged" TabIndex="17">
                    <asp:ListItem Value="0" Selected="True">Main</asp:ListItem>
                    <asp:ListItem Value="1">Branch</asp:ListItem>
                </asp:RadioButtonList></td><td>
                <asp:DropDownList CssClass="TextboxSmall" ID="ddlVendor" Enabled="false"  
                    runat="server" Width="154px" TabIndex="18">
                    <asp:ListItem Text="--SELECT--"></asp:ListItem>
                </asp:DropDownList></td>
               <%--  <asp:RequiredFieldValidator ControlToValidate="zipCode" ErrorMessage="Field cannot be empty"
                    ID="RequiredFieldValidator11" runat="server" />--%>
            <td><asp:Label ID="message" runat="server" /></td>
        </tr>
        <tr><td align="center" valign="middle" colspan="6">
                <asp:Button ID="add" Text="Add" runat="server"  CssClass="Button" 
                    onclick="add_Click" TabIndex="19" />
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="cancel" Text="Cancel" runat="server"  CausesValidation="false"
                    CssClass="Button" onclick="cancel_Click" TabIndex="20" />
            </td>
        </tr></table>
        </div>
        <!--Content Table End-->
        <div class="Line"></div>
        <!--Grid Table Start-->
       <%-- <asp:DataGrid ID="storelocationGrid" runat="server" AutoGenerateColumns="false" DataKeyField="storeId"
            CellPadding="3" CellSpacing="3" 
            OnItemCommand="storelocationGrid_ItemCommand" HeaderStyle-CssClass="GridHeading"
            ItemStyle-CssClass="GridData" 
             >
            <Columns>
             <asp:BoundColumn DataField="storCode" HeaderText="Division" HeaderStyle-Font-Bold="true" />
                <asp:BoundColumn DataField="storeName" HeaderText="Sub Division" HeaderStyle-Font-Bold="true" />
                <asp:ButtonColumn  ButtonType="LinkButton" CommandName="Edit" Text="Edit" ItemStyle-CssClass="GridLink" />
                <asp:ButtonColumn ButtonType="LinkButton"  CommandName="Delete" Text="Delete" ItemStyle-CssClass="GridLink" />
            </Columns>
        </asp:DataGrid>--%>
            <asp:DataGrid ID="vendorGrid" runat="server" AutoGenerateColumns="false" DataKeyField="vendorId"
                CellPadding="0" OnItemCommand="vendorGrid_ItemCommand" HeaderStyle-CssClass="GridHeading"
                ItemStyle-CssClass="GridData" onprerender="vendorGrid_PreRender" onitemcreated="vendorGrid_ItemCreated"
                GridLines="None" Width="100%">
                <Columns>
                    <asp:BoundColumn DataField="vendorName" HeaderText="Vendor Name" HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />
                    <asp:BoundColumn DataField="acountType" HeaderText="Account Type" HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />
                     <asp:ButtonColumn  ButtonType="LinkButton" CausesValidation="false" 
                    CommandName="View" Text="View" 
                 ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="GridLink" 
                    ItemStyle-Width="10%" >
<ItemStyle HorizontalAlign="Left" CssClass="GridLink" Width="10%"></ItemStyle>
                </asp:ButtonColumn>
                    
                    <asp:ButtonColumn ButtonType="LinkButton" CommandName="Edit" Text="Edit" ItemStyle-HorizontalAlign="Left"
                        ItemStyle-CssClass="GridLink" ItemStyle-Width="10%" />
                    <asp:ButtonColumn ButtonType="LinkButton" CommandName="Delete" Text="Delete" ItemStyle-HorizontalAlign="Left"
                        ItemStyle-CssClass="GridLink" ItemStyle-Width="10%" />
                </Columns>
            </asp:DataGrid>
        <!--Grid Table End-->
        <div class="Line"></div>
        <!--Page Body End-->
    </div>
</asp:Content>



