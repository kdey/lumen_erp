﻿<%@ Page Language="C#" MasterPageFile="~/DataEntry/DataEntry.master" AutoEventWireup="true"
    CodeFile="StoreLocation.aspx.cs" Inherits="DataEntry_StoreLocation" Title="Store" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

 <input type="hidden" name="storelocationId" value="<%=storelocationId%>" />
    <div class="PageMargin">
        <div class="PageHeading">Store Location</div>
        <div class="Line"></div>
        <!--Content Table Start-->
        <div class="PageContent">
        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
        <tr><td width="20%" align="right" valign="middle">Store Code&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="20%" align="left" valign="middle" style="margin-left: 40px">
                <asp:TextBox ID="storeCode" runat="server" MaxLength="3" 
                    CssClass="TextboxSmall" TabIndex="1" />
                <input type="hidden" id="sCode" runat="server" />
            </td>
            <td width="20%" align="right" valign="middle">Store Name&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="20%" align="left" valign="middle">
                <asp:TextBox ID="storeName" runat="server" CssClass="TextboxSmall" 
                    MaxLength="50" TabIndex="2" />                
            </td>
            <td rowspan="4" width="20%" align="center" valign="middle"><asp:Button ID="add" 
                    Text="Add"  runat="server" OnClick="add_Click" CssClass="Button" TabIndex="5" />
                &nbsp;
                <asp:Button ID="cancel" Text="Cancel" runat="server" OnClick="cancel_Click" CausesValidation="false"
                    CssClass="Button" TabIndex="6" /></td>
        </tr>
        <tr><td>&nbsp;</td>
            <td><asp:RequiredFieldValidator ControlToValidate="storeCode" ErrorMessage="Field cannot be empty"
                    ID="RequiredFieldValidator1" runat="server" />
                    <br />
                    <span id="err" runat="server" visible="false" style="color:Red">Store Code Must be 3 Character long</span></td>
            <td>&nbsp;</td>
            <td><asp:RequiredFieldValidator ControlToValidate="storeName" ErrorMessage="Field cannot be empty"
                    ID="RequiredFieldValidator2" runat="server" /></td>            
        </tr>
        <tr><td align="right" valign="middle">Store Address&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td align="left" valign="middle">
                <asp:TextBox ID="storeAddress" runat="server" 
                    onkeypress="return limiter(this.id);" TextMode="MultiLine" 
                CssClass="TextAreaSmall" MaxLength="250" TabIndex="3" />            
            </td>
            <td align="right" valign="middle">Zip Code&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td align="left" valign="middle">
                <asp:TextBox ID="zipCode" runat="server" CssClass="TextboxSmall" 
                    MaxLength="50" TabIndex="4" />   
                     <br />
                   <asp:RegularExpressionValidator ID="quanityRegexValidator" runat="server"
                    Display="Dynamic"  ErrorMessage="Zip Code must be a integer"
                    Text="" ControlToValidate="zipCode" ValidationExpression="\d+" />             
            </td>
        </tr>
        <tr><td><asp:Label ID="message" runat="server" /></td>
            <td><asp:RequiredFieldValidator ControlToValidate="storeAddress" ErrorMessage="Field cannot be empty"
                ID="RequiredFieldValidator3" runat="server" /></td>
            <td>&nbsp;</td>
            <td><asp:RequiredFieldValidator ControlToValidate="zipCode" ErrorMessage="Field cannot be empty"
                    ID="nameValidator" runat="server" /></td>
        </tr></table>
        </div>
        <!--Content Table End-->
        <div class="Line">
        </div>
        <!--Grid Table Start-->
        <asp:DataGrid ID="storelocationGrid" runat="server" AutoGenerateColumns="False" DataKeyField="storeId"
            CellPadding="0" OnItemCommand="storelocationGrid_ItemCommand" HeaderStyle-CssClass="GridHeading"
            ItemStyle-CssClass="GridData" 
            onitemcreated="storelocationGrid_ItemCreated" GridLines="None" Width="100%">
<ItemStyle CssClass="GridData"></ItemStyle>
            <Columns>
                <asp:BoundColumn DataField="storeName" HeaderText="Store Name" 
                    HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="left" >
                    <HeaderStyle Font-Bold="True" HorizontalAlign="Left" />
                </asp:BoundColumn>
                <asp:BoundColumn DataField="storCode" HeaderText="Store Code" 
                    HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="left" >
<HeaderStyle HorizontalAlign="Left" Font-Bold="True"></HeaderStyle>
                </asp:BoundColumn>
                 <asp:ButtonColumn  ButtonType="LinkButton" CausesValidation="false" 
                    CommandName="View" Text="View" 
                 ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="GridLink" 
                    ItemStyle-Width="10%" >
<ItemStyle HorizontalAlign="Left" CssClass="GridLink" Width="10%"></ItemStyle>
                </asp:ButtonColumn>
                <asp:ButtonColumn  ButtonType="LinkButton" CommandName="Edit" Text="Edit" 
                    ItemStyle-CssClass="GridLink" ItemStyle-Width="10%" >
<ItemStyle CssClass="GridLink" Width="10%"></ItemStyle>
                </asp:ButtonColumn>
                <asp:ButtonColumn ButtonType="LinkButton"  CommandName="Delete" Text="Delete" 
                    ItemStyle-CssClass="GridLink" ItemStyle-Width="10%" >
<ItemStyle CssClass="GridLink" Width="10%"></ItemStyle>
                </asp:ButtonColumn>
            </Columns>

<HeaderStyle CssClass="GridHeading"></HeaderStyle>
        </asp:DataGrid>
        <!--Grid Table End-->
        <div class="Line">
        </div>
        <!--Page Body End-->
    </div>
</asp:Content>
