<%@ Page Language="C#" MasterPageFile="~/DataEntry/DataEntry.master" AutoEventWireup="true"
    CodeFile="Sales.aspx.cs" Inherits="DataEntry_Sales" Title="Sales" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <input type="hidden" name="salesPersonId" value="<%=salesPersonId%>" />
    <div class="PageMargin">
        <div class="PageHeading">Sales Person</div>
        <div class="Line"></div>
        <!--Content Table Start-->
        <div class="PageContent">
        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
        <tr><td width="16%" align="right" valign="middle">Initial&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="16%" align="left" valign="middle">
                <asp:TextBox ID="initial" runat="server" MaxLength="10" Width="70px"  CssClass="TextboxSmall" />
            </td>
            <td width="16%" align="right" valign="middle">First Name&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="16%" align="left" valign="middle">
                <asp:TextBox ID="firstName" runat="server" MaxLength="50"  CssClass="TextboxSmall" />
            </td>
            <td width="16%" align="right" valign="middle">Last Name&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="16%" align="left" valign="middle">
                <asp:TextBox ID="lastName" runat="server" MaxLength="50"  CssClass="TextboxSmall" />
            </td>
        </tr>
        <tr><td>&nbsp;</td>
            <td align="left" valign="middle"><asp:RequiredFieldValidator ControlToValidate="initial" ErrorMessage="Field cannot be empty"
                    ID="RequiredFieldValidator1" runat="server" /></td>
            <td>&nbsp;</td>
            <td align="left" valign="middle"><asp:RequiredFieldValidator ControlToValidate="firstName" ErrorMessage="Field cannot be empty"
                    ID="RequiredFieldValidator2" runat="server" /></td>
            <td>&nbsp;</td>
            <td align="left" valign="middle"><asp:RequiredFieldValidator ControlToValidate="lastName" ErrorMessage="Field cannot be empty"
                    ID="RequiredFieldValidator3" runat="server" /></td>
        </tr>
        <tr>
            <td align="right" valign="middle">Address</td>
            <td align="left" valign="middle">
                <asp:TextBox ID="address" runat="server" onkeypress="return limiter(this.id);" 
                    TextMode="MultiLine" CssClass="TextAreaSmall" Height="50px" /></td>
            <td align="right" valign="middle">Zip Code</td>
            <td align="left" valign="middle">
                <asp:TextBox ID="zipCode" runat="server"   CssClass="TextboxSmall" />
                <br />
                   <asp:RegularExpressionValidator ID="quanityRegexValidator" runat="server"
                    Display="Dynamic"  ErrorMessage="Zip Code must be a integer"
                    Text="" ControlToValidate="zipCode" ValidationExpression="\d+" />
            </td>
            <td align="right" valign="middle">Email Id</td>
            <td align="left" valign="middle">
                <asp:TextBox ID="emailId" MaxLength="50" runat="server"   CssClass="TextboxSmall" />
            </td>
        </tr>
        <tr><td colspan="5">&nbsp;</td>
            <td align="left" valign="middle"><asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="emailId"
             ErrorMessage="Invalid Email Id." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></td>
        </tr>       
        <tr><td align="right" valign="middle">Mobile No</td>
            <td align="left" valign="middle"><asp:TextBox ID="mobileNo" runat="server" CssClass="TextboxSmall" /></td>
            <td align="right" valign="middle">Phone No&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td align="left" valign="middle">
                <asp:TextBox ID="phoneNo" runat="server" MaxLength="50"  CssClass="TextboxSmall" />
            </td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr><td align="left" valign="middle"><asp:Label ID="message" runat="server" /></td>
            <td colspan="2">&nbsp;</td>
            <td align="left" valign="middle"><asp:RequiredFieldValidator ControlToValidate="phoneNo" ErrorMessage="Field cannot be empty"
                    ID="RequiredFieldValidator4" runat="server" /></td> 
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr><td align="center" valign="middle" colspan="6">
                <asp:Button ID="add" Text="Add" runat="server" OnClick="add_Click" CssClass="Button" />
                &nbsp;
                <asp:Button ID="cancel" Text="Cancel" runat="server" OnClick="cancel_Click" CausesValidation="false"
                    CssClass="Button" />
            </td>
        </tr></table>
        </div>
        <!--Content Table End-->
        <div class="Line"></div>
        <!--Grid Table Start-->
        <asp:DataGrid ID="salesPersonGrid" runat="server" AutoGenerateColumns="False" DataKeyField="salesPersonId"
            CellPadding="0" OnItemCommand="salesPersonGrid_ItemCommand" HeaderStyle-CssClass="GridHeading"
            ItemStyle-CssClass="GridData" onitemcreated="salesPersonGrid_ItemCreated" 
            GridLines="None" Width="100%" >
        <ItemStyle CssClass="GridData"></ItemStyle>
            <Columns>
                <asp:BoundColumn DataField="firstName" HeaderText="First Name" HeaderStyle-HorizontalAlign="left" 
                    HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Left">
                <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundColumn>
                 <asp:BoundColumn DataField="lastName" HeaderText="Last Name" HeaderStyle-HorizontalAlign="left" 
                    HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Left">
                <HeaderStyle Font-Bold="True"></HeaderStyle>
                     <ItemStyle HorizontalAlign="Left" />
                </asp:BoundColumn>
             <asp:BoundColumn DataField="initial" HeaderText="Initial" HeaderStyle-HorizontalAlign="left" 
                    HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Left">
                <HeaderStyle Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                    <asp:BoundColumn DataField="mobileNo" HeaderText="Mobile No" HeaderStyle-HorizontalAlign="left" 
                    HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Left">
                <HeaderStyle Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                             <asp:BoundColumn DataField="phoneNo" HeaderText="Phone No" HeaderStyle-HorizontalAlign="left" 
                    HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Left">
                <HeaderStyle Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                     <asp:BoundColumn DataField="emailId" HeaderText="Email Id" HeaderStyle-HorizontalAlign="left" 
                    HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Left">
                <HeaderStyle Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:ButtonColumn  ButtonType="LinkButton" CausesValidation="false" 
                    CommandName="View" Text="View" 
                 ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="GridLink" 
                    ItemStyle-Width="10%" >
<ItemStyle HorizontalAlign="Left" CssClass="GridLink" Width="10%"></ItemStyle>
                </asp:ButtonColumn>
                <asp:ButtonColumn  ButtonType="LinkButton" CausesValidation="false" ItemStyle-Width="10%" 
                    CommandName="Edit" Text="Edit" ItemStyle-CssClass="GridLink" ItemStyle-HorizontalAlign="Left">
                <ItemStyle CssClass="GridLink"></ItemStyle>
                </asp:ButtonColumn>
                <asp:ButtonColumn ButtonType="LinkButton" CausesValidation="false" ItemStyle-Width="10%" 
                    CommandName="Delete" Text="Delete" ItemStyle-CssClass="GridLink" ItemStyle-HorizontalAlign="Left">
                <ItemStyle CssClass="GridLink"></ItemStyle>
                </asp:ButtonColumn>
            </Columns>
        <HeaderStyle CssClass="GridHeading"></HeaderStyle>
        </asp:DataGrid>
        <!--Grid Table End-->
        <div class="Line">
        </div>
        <!--Page Body End-->
    </div>
</asp:Content>
