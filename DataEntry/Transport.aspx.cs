﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;

public partial class DataEntry_Transport : BasePage
{
    protected int transporterId = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        message.Text = "";
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!this.IsPostBack)
        {
            BindGrid();
            //using (IConnection conn = DataConnectionFactory.GetConnection())
            //{
            //    using (IDataReader reader = conn.ExecuteQuery("select * from AdminUser where userName= 'demo'"))
            //    {
            //        while (reader.Read())
            //        {
            //            AdminUser s = GetLoggedinUser();
            //            int a = s.UserId;
            //            if (a == Convert.ToInt32(reader["adminuserId"]))

            //                add.Enabled = false;
            //        }
            //    }
            //}
        }
        else
        {
            // check if a division id has been passed
            try
            {
                transporterId = int.Parse(Request["transporterId"]);
                //using (IConnection conn = DataConnectionFactory.GetConnection())
                //{
                //    using (IDataReader reader = conn.ExecuteQuery("select * from AdminUser where userName= 'demo'"))
                //    {
                //        while (reader.Read())
                //        {
                //            AdminUser s = GetLoggedinUser();
                //            int a = s.UserId;
                //            if (a == Convert.ToInt32(reader["adminuserId"]))

                //                add.Enabled = false;
                //        }
                //    }
                //}
            }
            catch { };
        }
        if (GetLoggedinUser().UserType == 2)
        {
            add.Visible  = false;

        }
    }
    private void BindGrid()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select * from Lum_Erp_Transporter order by transporterName"))
            {
                transporterGrid.DataSource = reader;
                transporterGrid.DataBind();
            }
        }

    }

    protected void add_Click(object sender, EventArgs e)
    {
        if (transporterId == -1)
        {
            // we are in add mode
            if (transporterName.Text.Trim().Length > 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    int i = conn.ExecuteCommandProc("Lum_Erp_Stp_AddTransporter", new SpParam("@transporterName", transporterName.Text.Trim(), SqlDbType.VarChar));
                  if (i > 0)
                  {
                      message.Text = "Transporter Added : " + transporterName.Text;
                      message.ForeColor = Color.Green;
                      transporterName.Text = string.Empty;
                  }
                  else
                  {
                      message.Text = "Transporter Already Exists : " + transporterName.Text;
                      message.ForeColor = Color.Red;
                      transporterName.Text = string.Empty;
                  }
                }
            }
        }
        else
        {
            // we are in edit mode.
            if (transporterName.Text.Trim().Length > 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                  int i=  conn.ExecuteCommand(string.Format(
                           "  if(not exists(select * from Lum_Erp_Transporter where transporterName = '{0}' and transporterId={3}))update Lum_Erp_Transporter set transporterName = '{1}' where transporterId = {2}",
                           DataUtility.CleanString(transporterName.Text.Trim()), DataUtility.CleanString(transporterName.Text.Trim()), transporterId, transporterId));
                  if (i > 0)
                  {
                      add.Text = "Add";
                      message.Text = "Transporter Changed to : " + transporterName.Text;
                      message.ForeColor = Color.Green;
                      transporterName.Text = string.Empty;
                      transporterId = -1;
                  }
                  else
                  {
                      message.Text = "Transporter Already Exists : " + transporterName.Text;
                      message.ForeColor = Color.Red;
                  }

                }
            }

        }

        BindGrid();

        #region stored proc code
        //create proc [dbo].[AddDivision] @transporterName varchar(255)
        //as
        //begin
        //    if(not exists(select * from Division where transporterName = @transporterName))
        //        insert into Division(transporterName) values(@transporterName)
        //end
        #endregion
    }

    protected void transporterGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            if (e.CommandName.Equals("View"))
            {
                transporterId = (int)transporterGrid.DataKeys[e.Item.ItemIndex];
                Loaduom();
                add.Visible = false;
            }
           
                        if (e.CommandName.Equals("Edit"))
                        {
                            transporterId = (int)transporterGrid.DataKeys[e.Item.ItemIndex];
                            Loaduom();
                            add.Visible = true;
                        }
                        else if (e.CommandName.Equals("Delete"))
                        {
                            transporterId = (int)transporterGrid.DataKeys[e.Item.ItemIndex];
                            add.Visible = true;
                            try
                            {
                                conn.ExecuteCommand("delete from Lum_Erp_Transporter where transporterId = " + transporterId);
                                message.Text = "UOM deleted";
                                message.ForeColor = Color.Green;
                            }
                            catch
                            {
                                message.Text = "Transporter cannot be deleted because of existing related data";
                                message.ForeColor = Color.Red;
                            }
                            transporterName.Text = string.Empty;
                            transporterId = -1;

                            BindGrid();
                        }
                  
              
           
        }
    }

    private void Loaduom()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select transporterName from Lum_Erp_Transporter where transporterId = " + transporterId))
            {
                if (reader.Read())
                {
                    transporterName.Text = reader.GetString(0);
                    add.Text = "Update";
                    cancel.Visible = true;
                }
            }
        }
    }

    protected void cancel_Click(object sender, EventArgs e)
    {
        transporterName.Text = "";
        add.Text = "Add";
        transporterId = -1;
    }
    protected void transporterGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton btnview = (LinkButton)e.Item.Cells[1].Controls[0];
            btnview.CssClass = "GridLink";
            LinkButton btnEdit = (LinkButton)e.Item.Cells[2].Controls[0];
            LinkButton btn = (LinkButton)e.Item.Cells[3].Controls[0];
            btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this?')");
            btn.CssClass = "GridLink";
            btnEdit.CssClass = "GridLink";
            transporterName.Text = "";
            add.Text = "Add";
            transporterId = -1;
            if (GetLoggedinUser().UserType == 2)
            {
                btn.Visible = false;
                btnEdit.Visible = false;
            }
        }
    }
}
