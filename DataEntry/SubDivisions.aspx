﻿<%@ Page Language="C#" MasterPageFile="~/DataEntry/DataEntry.master" AutoEventWireup="true"
    CodeFile="SubDivisions.aspx.cs" Inherits="DataEntry_SubDivisions" Title="Sub division" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <input type="hidden" name="subdivisionId" value="<%=subdivisionId%>" />
    <div class="PageMargin">
        <div class="PageHeading">Sub Divisions</div>
        <div class="Line"></div>
        <!--Content Table Start-->
        <div class="PageContent">
        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
        <tr><td width="20%" align="right" valign="middle">Select Division</td>
            <td width="20%" align="left" valign="middle">
                <asp:DropDownList CssClass="TextboxSmall" ID="ddlDivision" runat="server" 
                    Width="150px" CausesValidation="True" ValidationGroup="a">
                </asp:DropDownList>
            </td>
            <td width="20%" align="right" valign="middle">Sub Division&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="20%" align="left" valign="middle">
                <asp:TextBox ID="SubdivisionName" runat="server" CssClass="TextboxSmall" 
                    ValidationGroup="a" /></td>
            <td align="left" valign="middle" width="20%">
                <asp:Button ID="add" Text="Add" runat="server" OnClick="add_Click" 
                    CssClass="Button" ValidationGroup="a" />
                <asp:Button ID="cancel" Text="Cancel" runat="server" OnClick="cancel_Click" CausesValidation="false"
                    CssClass="Button" />
            </td>
        </tr>
        <tr><td align="left" valign="middle"><asp:Label ID="message" runat="server" /></td>
            <td><asp:RequiredFieldValidator ControlToValidate="ddlDivision" ErrorMessage="Select Division"
                    ID="divisionValidator" InitialValue="Please Select" runat="server" 
                    SetFocusOnError="True" ValidationGroup="a" /></td>
            <td>&nbsp;</td>
            <td><asp:RequiredFieldValidator ControlToValidate="SubdivisionName" ErrorMessage="Field cannot be empty"
                    ID="nameValidator" runat="server" ValidationGroup="a" /></td>
            <td>&nbsp;</td>
        </tr>
        </table>
        </div>
        <!--Content Table End-->
        <div class="Line">
        </div>
        <!--Grid Table Start-->
        <asp:DataGrid ID="subdivisionGrid" runat="server" AutoGenerateColumns="false" DataKeyField="subdivisionId"
            CellPadding="0" OnItemCommand="subdivisionGrid_ItemCommand" HeaderStyle-CssClass="GridHeading"
            ItemStyle-CssClass="GridData" onitemcreated="subdivisionGrid_ItemCreated"
            GridLines="None" Width="100%">
        <ItemStyle CssClass="GridData" HorizontalAlign="Center"></ItemStyle>
            <Columns>
             <asp:BoundColumn DataField="divisionName" HeaderText="Division" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="left" />
                <asp:BoundColumn DataField="SubdivisionName" HeaderText="Sub Division" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="left" />
                <asp:ButtonColumn  ButtonType="LinkButton" CausesValidation="false" 
                    CommandName="View" Text="View" 
                 ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="GridLink" 
                    ItemStyle-Width="10%" >
<ItemStyle HorizontalAlign="Left" CssClass="GridLink" Width="10%"></ItemStyle>
                </asp:ButtonColumn>
                <asp:ButtonColumn  ButtonType="LinkButton" CommandName="Edit" Text="Edit" ItemStyle-CssClass="GridLink" ItemStyle-Width="10%">
                    <ItemStyle CssClass="GridLink"></ItemStyle></asp:ButtonColumn>
                <asp:ButtonColumn ButtonType="LinkButton"  CommandName="Delete" Text="Delete" ItemStyle-CssClass="GridLink" ItemStyle-Width="10%">
                    <ItemStyle CssClass="GridLink"></ItemStyle></asp:ButtonColumn>
            </Columns>
        <HeaderStyle CssClass="GridHeading"></HeaderStyle>
        </asp:DataGrid>
        <!--Grid Table End-->
        <div class="Line">
        </div>
        <!--Page Body End-->
    </div>
</asp:Content>
