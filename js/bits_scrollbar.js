// JavaScript Document
function bits_Slidebar(barId, trackId, axis, x, y) {
    var bar = document.getElementById(barId);
    var track = document.getElementById(trackId);
    this.barId = barId; this.trackId = trackId;
    this.axis = axis; this.x = x || 0; this.y = y || 0;
    bits_Slidebar.col[this.barId] = this;
    this.bar = bar;  this.shiftTo(x, y);
    
    this.trkHt = track.offsetHeight; 
    this.trkWd = track.offsetWidth; 
  
    if (axis == 'v') {
        this.maxY = this.trkHt - bar.offsetHeight - y; 
        this.maxX = x; this.minX = x; this.minY = y;
    } else {
        this.maxX = this.trkWd - bar.offsetWidth - x; 
        this.minX = x; this.maxY = y; this.minY = y;
    }
    
    this.on_drag_start =  this.on_drag =   this.on_drag_end = 
    this.on_slide_start = this.on_slide =  this.on_slide_end = function() {}
    
    bar.onmousedown = bits_Slidebar.prepDrag; 
    track.onmousedown = function(e) { bits_Slidebar.prepSlide(barId, e); }
    this.bar = bar = null; track = null; 
}

bits_Slidebar.col = {}; 
bits_Slidebar.current = null; 

bits_Slidebar.prototype.slideDur = 500;

bits_Slidebar.prepSlide = function(barId, e) {
    var _this = bits_Slidebar.col[barId];
    bits_Slidebar.current = _this;
    var bar = _this.bar = document.getElementById(barId);
    
    if ( _this.timer ) { clearInterval(_this.timer); _this.timer = 0; }
    e = e? e: window.event;
    
    e.offX = (typeof e.layerX != "undefined")? e.layerX: e.offsetX;
    e.offY = (typeof e.layerY != "undefined")? e.layerY: e.offsetY;
    _this.startX = parseInt(bar.style.left); _this.startY = parseInt(bar.style.top);

    if (_this.axis == "v") {
        _this.destX = _this.startX;
        _this.destY = (e.offY < _this.startY)? e.offY: e.offY - bar.offsetHeight;
        _this.destY = Math.min( Math.max(_this.destY, _this.minY), _this.maxY );
    } else {
        _this.destX = (e.offX < _this.startX)? e.offX: e.offX - bar.offsetWidth;
        _this.destX = Math.min( Math.max(_this.destX, _this.minX), _this.maxX );
        _this.destY = _this.startY;
    }
    _this.distX = _this.destX - _this.startX; _this.distY = _this.destY - _this.startY;
    _this.per = Math.PI/(2 * _this.slideDur);
    _this.slideStartTime = new Date().getTime();
    _this.on_slide_start(_this.startX, _this.startY);
    _this.timer = setInterval("bits_Slidebar.doSlide()", 10);
}

bits_Slidebar.doSlide = function() {
    var _this = bits_Slidebar.current;
    var elapsed = new Date().getTime() - _this.slideStartTime;
    if (elapsed < _this.slideDur) {
        var x = _this.startX + _this.distX * Math.sin(_this.per*elapsed);
        var y = _this.startY + _this.distY * Math.sin(_this.per*elapsed);
        _this.shiftTo(x,y);
        _this.on_slide(x, y);
    } else {	
        clearInterval(_this.timer);
        _this.shiftTo(_this.destX,  _this.destY);
        _this.on_slide(_this.destX,  _this.destY);
        _this.on_slide_end(_this.destX, _this.destY);
        bits_Slidebar.current = null;
    }    
}

bits_Slidebar.prepDrag = function (e) { 
    var bar = this; 
    var barId = this.id; 
    var _this = bits_Slidebar.col[barId]; 
    bits_Slidebar.current = _this;
    _this.bar = bar;
    e = bits_Event.DOMit(e);
    if ( _this.timer ) { clearInterval(_this.timer); _this.timer = 0; }
    _this.downX = e.clientX; _this.downY = e.clientY;
    _this.startX = parseInt(bar.style.left);
    _this.startY = parseInt(bar.style.top);
    _this.on_drag_start(_this.startX, _this.startY);
    bits_Event.add( document, "mousemove", bits_Slidebar.doDrag, true );
    bits_Event.add( document, "mouseup",   bits_Slidebar.endDrag,  true );
    e.stopPropagation(); e.preventDefault();
}

bits_Slidebar.doDrag = function(e) {
    if ( !bits_Slidebar.current ) return; 
    var _this = bits_Slidebar.current;
    var bar = _this.bar;
    e = bits_Event.DOMit(e);
    var nx = _this.startX + e.clientX - _this.downX;
    var ny = _this.startY + e.clientY - _this.downY;
    nx = Math.min( Math.max( _this.minX, nx ), _this.maxX);
    ny = Math.min( Math.max( _this.minY, ny ), _this.maxY);
    _this.shiftTo(nx, ny);
    _this.on_drag(nx, ny);
    e.preventDefault(); e.stopPropagation();
}

bits_Slidebar.endDrag = function() {
    if ( !bits_Slidebar.current ) return; 
    var _this = bits_Slidebar.current;
    var bar = _this.bar;
    bits_Event.remove( document, "mousemove", bits_Slidebar.doDrag, true );
    bits_Event.remove( document, "mouseup",   bits_Slidebar.endDrag,  true );
    _this.on_drag_end( parseInt(bar.style.left), parseInt(bar.style.top) );
    bits_Slidebar.current = null;
}

bits_Slidebar.prototype.shiftTo = function(x, y) {
    if ( this.bar ) {
        this.bar.style.left = x + "px";
        this.bar.style.top = y + "px";
    }
}

bits_scrollObj.prototype.setUpScrollbar = function(barId, trkId, axis, offx, offy, bSize) {
    var scrollbar = new bits_Slidebar(barId, trkId, axis, offx, offy);
    if (axis == "v") {
        this.vBarId = barId; 
    } else {
        this.hBarId = barId;
    }
    scrollbar.wndoId = this.id;
    scrollbar.bSizeDragBar = (bSize == false)? false: true; 
    if (scrollbar.bSizeDragBar) {
        bits_Scrollbar_Co.setBarSize(this, scrollbar);
    }
    bits_Scrollbar_Co.setEvents(this, scrollbar);
}

bits_Scrollbar_Co = {
    
    setBarSize: function(scrollObj, barObj) {
        var lyr = document.getElementById(scrollObj.lyrId);
        var wn = document.getElementById(scrollObj.id);
        if ( barObj.axis == 'v' ) {
            var bar = document.getElementById(scrollObj.vBarId);
            bar.style.height = (lyr.offsetHeight > wn.offsetHeight)? 
                barObj.trkHt / ( lyr.offsetHeight / wn.offsetHeight ) + "px":             
                barObj.trkHt - ( 2 * barObj.minY ) + "px";
            barObj.maxY = barObj.trkHt - bar.offsetHeight - barObj.minY; 
        } else if ( barObj.axis == 'h' ) {
            var bar = document.getElementById(scrollObj.hBarId);
            bar.style.width = (scrollObj.wd > wn.offsetWidth)? 
                barObj.trkWd / ( scrollObj.wd / wn.offsetWidth ) + "px": 
                barObj.trkWd - ( 2 * barObj.minX ) + "px";
            barObj.maxX = barObj.trkWd - bar.offsetWidth - barObj.minX;
        }
    },
    
    resetBars: function(scrollObj) {
        var barObj, bar;
        if (scrollObj.vBarId) {
            barObj = bits_Slidebar.col[scrollObj.vBarId];
            bar = document.getElementById(scrollObj.vBarId);
            bar.style.left = barObj.minX + "px"; bar.style.top = barObj.minY + "px";
            if (barObj.bSizeDragBar) {
                bits_Scrollbar_Co.setBarSize(scrollObj, barObj);
            }
        }
        if (scrollObj.hBarId) {
            barObj = bits_Slidebar.col[scrollObj.hBarId];
            bar = document.getElementById(scrollObj.hBarId);
            bar.style.left = barObj.minX + "px"; bar.style.top = barObj.minY + "px";
            if (barObj.bSizeDragBar) {
                bits_Scrollbar_Co.setBarSize(scrollObj, barObj);
            }
        }
    },
    
    setEvents: function(scrollObj, barObj) {
        // scrollObj
        this.addEvent(scrollObj, 'on_load', function() { bits_Scrollbar_Co.resetBars(scrollObj); } );
        this.addEvent(scrollObj, 'on_scroll_start', function() { bits_Scrollbar_Co.getBarRefs(scrollObj) } );
        this.addEvent(scrollObj, 'on_glidescroll_start', function() { bits_Scrollbar_Co.getBarRefs(scrollObj) } );
        this.addEvent(scrollObj, 'on_scroll', function(x,y) { bits_Scrollbar_Co.updateScrollbar(scrollObj, x, y) } );
        this.addEvent(scrollObj, 'on_glidescroll', function(x,y) { bits_Scrollbar_Co.updateScrollbar(scrollObj, x, y) } );
        this.addEvent(scrollObj, 'on_scroll_stop', function(x,y) { bits_Scrollbar_Co.updateScrollbar(scrollObj, x, y); } );
        this.addEvent(scrollObj, 'on_glidescroll_stop', function(x,y) { bits_Scrollbar_Co.updateScrollbar(scrollObj, x, y); } );
        this.addEvent(scrollObj, 'on_scroll_end', function(x,y) { bits_Scrollbar_Co.updateScrollbar(scrollObj, x, y); } );
        this.addEvent(scrollObj, 'on_glidescroll_end', function(x,y) { bits_Scrollbar_Co.updateScrollbar(scrollObj, x, y); } );
            
        // barObj 
        this.addEvent(barObj, 'on_slide_start', function() { bits_Scrollbar_Co.getWndoLyrRef(barObj) } );
        this.addEvent(barObj, 'on_drag_start', function() { bits_Scrollbar_Co.getWndoLyrRef(barObj) } );
        this.addEvent(barObj, 'on_slide', function(x,y) { bits_Scrollbar_Co.updateScrollPosition(barObj, x, y) } );
        this.addEvent(barObj, 'on_drag', function(x,y) { bits_Scrollbar_Co.updateScrollPosition(barObj, x, y) } );
        this.addEvent(barObj, 'on_slide_end', function(x,y) { bits_Scrollbar_Co.updateScrollPosition(barObj, x, y); } );
        this.addEvent(barObj, 'on_drag_end', function(x,y) { bits_Scrollbar_Co.updateScrollPosition(barObj, x, y); } );
    
    },
    
     addEvent: function(o, ev, fp) {
        var oldEv = o[ev];
        if ( typeof oldEv != 'function' ) {
            o[ev] = function (x,y) { fp(x,y); }
        } else {
            o[ev] = function (x,y) {
                  oldEv(x,y );
                  fp(x,y);
            }
        }
    },

    updateScrollbar: function(scrollObj, x, y) { // 
        var nx, ny;
        if ( scrollObj.vBar && scrollObj.maxY ) { 
            var vBar = scrollObj.vBar;
            ny = -( y * ( (vBar.maxY - vBar.minY) / scrollObj.maxY ) - vBar.minY );
            ny = Math.min( Math.max(ny, vBar.minY), vBar.maxY);  
            if (vBar.bar) { // ref to bar el
                nx = parseInt(vBar.bar.style.left);
                vBar.shiftTo(nx, ny);
            }
        }
        if ( scrollObj.hBar && scrollObj.maxX ) {
            var hBar = scrollObj.hBar;
            nx = -( x * ( (hBar.maxX - hBar.minX) / scrollObj.maxX ) - hBar.minX );
            nx = Math.min( Math.max(nx, hBar.minX), hBar.maxX);
            if (hBar.bar) {
                ny = parseInt(hBar.bar.style.top);
                hBar.shiftTo(nx, ny);
            }
        }
    },

    updateScrollPosition: function(barObj, x, y) { 
        var nx, ny; var wndo = barObj.wndo; 
        if ( !wndo.lyr ) {
            wndo.lyr = document.getElementById(wndo.lyrId);
        }
        if (barObj.axis == "v") {
            nx = wndo.x; 
            ny = -(y - barObj.minY) * ( wndo.maxY / (barObj.maxY - barObj.minY) ) || 0;
        } else {
            ny = wndo.y;
            nx = -(x - barObj.minX) * ( wndo.maxX / (barObj.maxX - barObj.minX) ) || 0;
        }
        wndo.shiftTo(nx, ny);
    },
    
    getBarRefs: function(scrollObj) { 
        if ( scrollObj.vBarId ) {
            scrollObj.vBar = bits_Slidebar.col[scrollObj.vBarId];
            scrollObj.vBar.bar = document.getElementById(scrollObj.vBarId);
        }
        if ( scrollObj.hBarId ) {
            scrollObj.hBar = bits_Slidebar.col[scrollObj.hBarId];
            scrollObj.hBar.bar = document.getElementById(scrollObj.hBarId);
        }
    },
    
    getWndoLyrRef: function(barObj) {
        var wndo = barObj.wndo = bits_scrollObj.col[ barObj.wndoId ];
        if ( wndo && !wndo.lyr ) {
            wndo.lyr = document.getElementById(wndo.lyrId);
        }
    }

}
