// JavaScript Document
bits_scrollObj.loadLayer = function(wndoId, lyrId, horizId) {
    if ( bits_scrollObj.col[wndoId] ) bits_scrollObj.col[wndoId].load(lyrId, horizId);
}

bits_scrollObj.initScroll = function(wndoId, dir, speed) {
    var deg = dir == 'up'? 90: dir == 'down'? 270: dir == 'left'? 180: dir == 'right'? 0: dir;
    if ( deg != null && bits_scrollObj.col[wndoId] ) {
        bits_scrollObj.col[wndoId].initScrollVals(deg, speed);
    }
}

bits_scrollObj.stopScroll = function(wndoId) {
    if ( bits_scrollObj.col[wndoId] ) bits_scrollObj.col[wndoId].ceaseScroll();
}

bits_scrollObj.doubleSpeed = function(wndoId) {
    if ( bits_scrollObj.col[wndoId] ) bits_scrollObj.col[wndoId].speed *= 2;
}

bits_scrollObj.resetSpeed = function(wndoId) {
    if ( bits_scrollObj.col[wndoId] ) bits_scrollObj.col[wndoId].speed /= 2;
}

bits_scrollObj.scrollBy = function(wndoId, x, y, dur) {
    if ( bits_scrollObj.col[wndoId] ) bits_scrollObj.col[wndoId].initScrollByVals(x, y, dur);
}

bits_scrollObj.scrollTo = function(wndoId, x, y, dur) {
    if ( bits_scrollObj.col[wndoId] ) bits_scrollObj.col[wndoId].initScrollToVals(x, y, dur);
}