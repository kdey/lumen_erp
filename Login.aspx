<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link href="style/style.css" rel="stylesheet" type="text/css" />
    <title>Lumen Tele System Pvt. Ltd.</title>
</head>
<body class="LoginBody">
    <form id="form1" runat="server">
    <table align="center" cellspacing="0" class="LoginBgTable">
        <tr>
            <td align="right" valign="top">
                <table align="right" cellspacing="0" class="LoginTextboxTable">
                    <tr>
                        <td align="right" valign="middle" class="LoginText">
                            Username :
                        </td>
                        <td align="right" valign="middle">
                            <asp:TextBox ID="userName" runat="server" CssClass="LoginTextbox" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right" valign="middle" class="LoginText">
                            Password :
                        </td>
                        <td align="right" valign="middle">
                            <asp:TextBox ID="password" runat="server" TextMode="Password" CssClass="LoginTextbox" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right" valign="middle">
                            &nbsp;
                        </td>
                        <td align="right" valign="middle">
                            <asp:Button ID="loginButton" runat="server" Text="Login" OnClick="loginButton_Click"
                                CssClass="LoginButton" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
