﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/Reports.master" AutoEventWireup="true"
    CodeFile="POPrint_ReportViewer.aspx.cs" Inherits="Reports_POPrint_ReportViewer" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt"
        Height="400px" Width="645px">
        <LocalReport ReportPath="Reports\Rpt_POPrint.rdlc" EnableExternalImages="True" EnableHyperlinks="True">
        
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DataSet_POPrint_Lum_Erp_Stp_GetPOforPrint" />
            </DataSources>
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource2" Name="DataSet_POPrint_Lum_Erp_PurchaseOrder" />
            </DataSources>
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource3" Name="DataSet_POPrint_Lum_Erp_PurchaseOrderType" />
            </DataSources>
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource4" Name="DataSet_POPrint_Lum_Erp_Vendor" />
            </DataSources>
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource5" Name="DataSet_POPrint_Lum_Erp_CompanyMaster" />
            </DataSources>
            
        </LocalReport>
    </rsweb:ReportViewer>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData"
        TypeName="DataSet_POPrintTableAdapters.Lum_Erp_Stp_GetPOforPrintTableAdapter">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" SelectMethod="GetData"
        TypeName="DataSet_POPrintTableAdapters.Lum_Erp_PurchaseOrderTableAdapter">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" SelectMethod="GetData"
        TypeName="DataSet_POPrintTableAdapters.Lum_Erp_PurchaseOrderTypeTableAdapter">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource4" runat="server" SelectMethod="GetData"
        TypeName="DataSet_POPrintTableAdapters.Lum_Erp_VendorTableAdapter">
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource5" runat="server" SelectMethod="GetData"
        TypeName="DataSet_POPrintTableAdapters.Lum_Erp_CompanyMasterTableAdapter">
    </asp:ObjectDataSource>
</asp:Content>
