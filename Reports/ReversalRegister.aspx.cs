﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;
using System.Data.SqlClient;

public partial class Reports_ReversalRegister : BasePage
{
    //private double totalAmount = 0;
    double totalAmtBeforeVat = 0;
    double totalVat = 0;
    double totalAmtAfterVat = 0;
    double totalBuyBack = 0;
    double totalPacking = 0;

    double totals_price= 0;
    double totals_tamt = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");

        }
        if (!IsPostBack)
        {

        }
    }
    public void bindGrid()
    {
        lblMsg.Text = "";
        ReportGridSALES.Visible = false;
        ReportGridSERVICE.Visible = false;
        ReportGridCSASERVICE.Visible = false;
        try
        {
            string fromDate = Convert.ToDateTime(txtFromDate.Text).ToString("dd MMM yyyy");
            // DateTime fromDate = DateTime.ParseExact(txtFromDate.Text, "dd/MM/yyyy", null);
            string toDate = Convert.ToDateTime(txtToDate.Text).ToString("dd MMM yyyy");
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                SpParam[] _Sparray = new SpParam[3];

                _Sparray[0] = new SpParam("@fromDate", fromDate, SqlDbType.SmallDateTime);
                _Sparray[1] = new SpParam("@toDate", toDate, SqlDbType.SmallDateTime);
                _Sparray[2] = new SpParam("@ChallanTypeId", drInvoiceType.SelectedValue, SqlDbType.Int);

                DataSet _Ds = conn.GetDataSet("Lum_Erp_Stp_GetReverseRegister", _Sparray);
                if (drInvoiceType.SelectedValue == "2") //sales
                {
                    lblheading.Text = "Reversal Sales Register";
                    ReportGridSALES.Visible = true;
                    ReportGridSALES.DataSource = _Ds;
                    ReportGridSALES.DataBind();
                    for (int _index = 0; _index <= _Ds.Tables[0].Rows.Count - 1; _index++)
                    {
                        totalAmtBeforeVat = totalAmtBeforeVat + Convert.ToDouble(_Ds.Tables[0].Rows[_index]["AmtBeforeVat"].ToString());
                        totalVat = totalVat + Convert.ToDouble(_Ds.Tables[0].Rows[_index]["Tax"].ToString());
                        totalAmtAfterVat = totalAmtAfterVat + Convert.ToDouble(_Ds.Tables[0].Rows[_index]["Amt"].ToString());
                        if (_Ds.Tables[0].Rows[_index]["buyBackAmount"].ToString() !="")
                            totalBuyBack = totalBuyBack + Convert.ToDouble(_Ds.Tables[0].Rows[_index]["buyBackAmount"].ToString());
                        if (_Ds.Tables[0].Rows[_index]["packingCharges"].ToString() != "")
                            totalPacking = totalPacking + Convert.ToDouble(_Ds.Tables[0].Rows[_index]["packingCharges"].ToString());
                    }

                    
                }
                if (drInvoiceType.SelectedValue == "1") //service
                {
                    ReportGridSERVICE.Visible = true;
                    ReportGridSERVICE.DataSource = _Ds;
                    ReportGridSERVICE.DataBind();
                    lblheading.Text = "Reversal Service Register";
                }

                if (drInvoiceType.SelectedValue == "3") //CSAService
                {
                    ReportGridCSASERVICE.Visible = true;
                    ReportGridCSASERVICE.DataSource = _Ds;
                    ReportGridCSASERVICE.DataBind();
                    lblheading.Text = "Reversal CSAService Register";
                }

                if (_Ds.Tables[0].Rows.Count != 0)
                {
                    btnexpToExcel.Enabled = true;
                }
                else
                    btnexpToExcel.Enabled = false;
 
            }
        }
        catch (Exception ex)
        {
            if (Convert.ToString(ex.Message) == "String was not recognized as a valid DateTime.")
            {
                Page.RegisterClientScriptBlock("message", "<script>alert('Date was not in correct Format...');</script>");
                //lblMsg.Text = "Date was not in correct Format";
            }
            else
            {
                lblMsg.Text = Convert.ToString(ex.Message);
            }

        }
    }

    protected void btnReport_Click(object sender, EventArgs e)
    {
        bindGrid();
    }


    private double CalcTotalAmount(string _netVal)
    {
        double retVal;
        if (double.TryParse(_netVal, out retVal))
        {
            retVal = Double.Parse(_netVal);

        }
        return retVal;
    }
    protected void btnexptoexcel_Click(object sender, EventArgs e)
    {

        if (drInvoiceType.SelectedValue == "2") //sales
        {
            GridViewExportUtil.Export("SalesRegister.xls", ReportGridSALES);  
        }
        if (drInvoiceType.SelectedValue == "1") //sales
        {
            GridViewExportUtil.Export("SalesRegister.xls", ReportGridSERVICE);  
        }
        if (drInvoiceType.SelectedValue == "3") //CSAService
        {
            GridViewExportUtil.Export("SalesRegister.xls", ReportGridCSASERVICE);
        }
        
        




    }

    //protected void ReportGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    //{
    //    if (e.Row.RowType == DataControlRowType.DataRow)
    //    {
    //        totalAmtBeforeVat += CalcTotalAmount(e.Row.Cells[5].Text);
    //        totalVat += CalcTotalAmount(e.Row.Cells[6].Text);
    //        totalAmtAfterVat += CalcTotalAmount(e.Row.Cells[7].Text);
    //        totalBuyBack += CalcTotalAmount(e.Row.Cells[8].Text);
    //        totalPacking += CalcTotalAmount(e.Row.Cells[9].Text);
    //    }
    //    if (e.Row.RowType == DataControlRowType.Footer)
    //    {
    //        e.Row.Cells[3].Text = "Total";
    //        e.Row.Cells[6].Text = string.Format("{0:N}", totalAmtBeforeVat); //lblAmtBeforeVat.Text;// totalAmtBeforeVat.ToString();
    //        e.Row.Cells[7].Text = string.Format("{0:N}", totalVat);
    //        e.Row.Cells[8].Text = string.Format("{0:N}", totalAmtAfterVat);
    //        e.Row.Cells[9].Text = string.Format("{0:N}", totalBuyBack);
    //        e.Row.Cells[10].Text = string.Format("{0:N}", totalPacking);
    //    }
    //}
    //protected void ReportGridSERVICE_RowDataBound(object sender, GridViewRowEventArgs e)
    //{
    //    if (e.Row.RowType == DataControlRowType.DataRow)
    //    {
    //        totals_price += CalcTotalAmount(e.Row.Cells[5].Text);
    //        totals_tamt += CalcTotalAmount(e.Row.Cells[6].Text);

    //    }
    //    if (e.Row.RowType == DataControlRowType.Footer)
    //    {
    //        e.Row.Cells[2].Text = "Total";
    //        e.Row.Cells[5].Text = string.Format("{0:N}", totals_price); 
    //        e.Row.Cells[7].Text = string.Format("{0:N}", totals_tamt);

    //    }
    //}
}
