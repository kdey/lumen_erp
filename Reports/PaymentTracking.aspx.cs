﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;
using System.Data.SqlClient;

public partial class Reports_PaymentTracking : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");

        }
        if (!IsPostBack)
        {
            bind_Customer();
         
        
        }
    }
    public void bind_Customer()
    {
        try
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                string Sqlquery = string.Format("SELECT * FROM Customer order by customerName");
                DataSet _Ds = conn.GetDataSet(Sqlquery, "CustomerBind");
                drCustomer.DataValueField = "customerId";
                drCustomer.DataTextField = "customerName";
                drCustomer.DataSource = _Ds;
                drCustomer.DataBind();
                drCustomer.Items.Insert(0, "--Please Select--");


            }
        }
        catch (Exception ex)
        {
           
        }
    
    
    }
    public void bind_InvoiceType()
    {
        try
        {
            drInvoiceNo.Items.Clear();
            if (drInvoiceType.SelectedItem.Text == "Sales")
            {
             
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    string Sqlquery = string.Format("SELECT s.*,ci.strChallanId FROM Lum_Erp_SOIR s,Lum_Erp_ChallancumInvoice ci where customerId='" + drCustomer.SelectedValue + "' and ci.soirId=s.soirId");
                    DataSet _Ds = conn.GetDataSet(Sqlquery, "InvoiceNO");
                    drInvoiceNo.Items.Clear();
                    drInvoiceNo.DataValueField = "soirId";
                    drInvoiceNo.DataTextField = "strChallanId";
                    drInvoiceNo.DataSource = _Ds;
                    drInvoiceNo.DataBind();
                    drInvoiceNo.Items.Insert(0, "--Please Select--");
                  


                }

            }

            if (drInvoiceType.SelectedItem.Text == "Services")
            {
               

                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    string Sqlquery = string.Format("SELECT * FROM Lum_Erp_Services where customerId='" + drCustomer.SelectedValue + "' ");
                    DataSet _Ds = conn.GetDataSet(Sqlquery, "InvoiceNOServices");
                    drInvoiceNo.Items.Clear();
                    drInvoiceNo.DataValueField = "serviceId";
                    drInvoiceNo.DataTextField = "strServiceId";
                    drInvoiceNo.DataSource = _Ds;
                    drInvoiceNo.DataBind();

                    drInvoiceNo.Items.Insert(0, "--Please Select--");

                }

            }
        }
        catch (Exception ex)
        {
           
        }
    
    }
    protected void drCustomer_SelectedIndexChanged(object sender, EventArgs e)
    {
        ResetControl();

        if(drCustomer.SelectedIndex!=0)
        bind_InvoiceType();
     

    }
    protected void drInvoiceType_SelectedIndexChanged(object sender, EventArgs e)
    {
        ResetControl();
        
    }
    protected void drInvoiceNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        ResetControl();
        bind_Information();
        bind_PaymentDetails();
    }
    public void bind_Information()
    {
        string sql = "";
        try
        {
            if (drInvoiceType.SelectedItem.Text == "Sales")
            {

                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    string Sqlquery = string.Format("SELECT so.*,(sa.firstName+' '+sa.lastName)as Primaryname,cs.segmentType,pot.poTypeName,ch.CNDetails,ch.taxTypeId FROM SOIR so, SalesPerson sa,CustomerSegment cs,Customer cu,PurchaseOrderType pot,ChallancumInvoice ch where so.customerId='" + drCustomer.SelectedValue + "' and sa.salesPersonId=so.primarySalesPersonId and  cu.customerSegmentId=cs.segmentId and so.soirId='" + drInvoiceNo.SelectedValue + "' and so.customerId=cu.customerId and pot.poTypeId=so.orderTypeId and ch.soirId=so.soirId");
                    string Sqlquery1 = string.Format("SELECT so.*,(sa.firstName+' '+sa.lastName)as Additionalname FROM SOIR so, SalesPerson sa where customerId='" + drCustomer.SelectedValue + "' and sa.salesPersonId=so.aditionalSalesPersonId and  soirId='" + drInvoiceNo.SelectedValue + "'");

                    DataSet _Ds = conn.GetDataSet(Sqlquery, "InvoiceNO");
                    DataSet _Ds1 = conn.GetDataSet(Sqlquery1, "InvoiceNO1");

                    if (_Ds.Tables[0].Rows.Count != 0)
                    {
                        txtCustomerOredrNo.Text = _Ds.Tables[0].Rows[0]["customerPoNo"].ToString();
                        txtPODate.Text = Convert.ToDateTime(_Ds.Tables[0].Rows[0]["poDate"].ToString()).ToShortDateString();
                        txtPrimarySalesPerson.Text = _Ds.Tables[0].Rows[0]["Primaryname"].ToString();
                        txtAdditionalSalesPerson.Text = _Ds1.Tables[0].Rows[0]["Additionalname"].ToString();
                        txtCustomerSegment.Text = _Ds.Tables[0].Rows[0]["segmentType"].ToString();
                        txtOrderType.Text = _Ds.Tables[0].Rows[0]["poTypeName"].ToString();
                        txtCNDetails.Text = _Ds.Tables[0].Rows[0]["CNDetails"].ToString();
                        txtTotalAmount.Text = _Ds.Tables[0].Rows[0]["buyBackAmount"].ToString();
                        int _soirId = Int32.Parse(drInvoiceNo.SelectedValue);
                        int taxTypeId = Int32.Parse(_Ds.Tables[0].Rows[0]["taxTypeId"].ToString());

                        if (taxTypeId == 1)
                        {
                            sql = @"select soirLineId,sd.itemId, d.divisionName, s.subdivisionName, i.itemDescription, sd.quantity,  u.uomName, sd.price, i.vat as taxType, 
                        sd.discount,sd.price * sd.quantity * 0.01 * i.vat 'vatAmount',(sd.price * sd.quantity+ sd.price * sd.quantity * 0.01 * i.vat-sd.discount) as total 
                        from SoirItemDetails sd 
                        inner join Item i on sd.itemId = i.itemId
                        inner join Subdivision s on s.subdivisionId = i.subdivisionId
                        inner join Division d on d.divisionId = s.divisionId
                        inner join dbo.UOM u on i.uomId = u.uomId
                        where itemType=0 and soirId = " + _soirId;


                        }
                        else if (taxTypeId == 2)
                        {
                            sql = @"select soirLineId,sd.itemId, d.divisionName, s.subdivisionName, i.itemDescription, sd.quantity,  u.uomName, sd.price, i.cst as taxType, 
                        sd.discount,sd.price * sd.quantity * 0.01 * i.cst 'vatAmount',(sd.price * sd.quantity+ sd.price * sd.quantity * 0.01 * i.cst-sd.discount) as total 
                        from SoirItemDetails sd 
                        inner join Item i on sd.itemId = i.itemId
                        inner join Subdivision s on s.subdivisionId = i.subdivisionId
                        inner join Division d on d.divisionId = s.divisionId
                        inner join dbo.UOM u on i.uomId = u.uomId
                        where itemType=0 and soirId = " + _soirId;

                        }
                        else if (taxTypeId == 3)
                        {
                            sql = @"select soirLineId,sd.itemId, d.divisionName, s.subdivisionName, i.itemDescription, sd.quantity,  u.uomName, sd.price, i.cstAgainstFormC as taxType, 
                        sd.discount,sd.price * sd.quantity * 0.01 * i.cstAgainstFormC 'vatAmount',(sd.price * sd.quantity+ sd.price * sd.quantity * 0.01 * i.cstAgainstFormC-sd.discount) as total 
                        from SoirItemDetails sd 
                        inner join Item i on sd.itemId = i.itemId
                        inner join Subdivision s on s.subdivisionId = i.subdivisionId
                        inner join Division d on d.divisionId = s.divisionId
                        inner join dbo.UOM u on i.uomId = u.uomId
                        where itemType=0 and soirId = " + _soirId;

                        }
                        DataSet _DsTotalAmount = conn.GetDataSet(sql, "TotalAmount");
                        decimal TotalAmount = 0;
                        for (int _index = 0; _index < _DsTotalAmount.Tables[0].Rows.Count; _index++)
                        {
                            TotalAmount = TotalAmount + Convert.ToDecimal(_DsTotalAmount.Tables[0].Rows[_index]["total"].ToString());


                        }
                        txtTotalAmount.Text = string.Format("{0:N}", TotalAmount);

                    }
                    else
                    {
                        ResetControl();

                    }

                }

            }

            if (drInvoiceType.SelectedItem.Text == "Services")
            {

                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    string Sqlquery = string.Format("SELECT so.*,cs.segmentDescription,t.* FROM Services so, SalesPerson sa,CustomerSegment cs,Customer cu,ServiceTaxMaster t where so.customerId='" + drCustomer.SelectedValue + "'  and  cu.customerSegmentId=cs.segmentId and so.serviceId='" + drInvoiceNo.SelectedValue + "' and so.customerId=cu.customerId and so.servicetaxId=t.servicetaxId");

                    DataSet _Ds = conn.GetDataSet(Sqlquery, "InvoiceNOServices");
                    if (_Ds.Tables[0].Rows.Count != 0)
                    {

                        txtCustomerSegment.Text = _Ds.Tables[0].Rows[0]["segmentDescription"].ToString();
                      
                        decimal TotalAmount = Convert.ToDecimal(_Ds.Tables[0].Rows[0]["total"].ToString());
                        txtTotalAmount.Text = string.Format("{0:N}", TotalAmount);
                    }
                    else
                    {
                        ResetControl();

                    }

                }

            }
        }
        catch (Exception ex)
        { 
        
        }
    
    
    }
    public void ResetControl()
    {
        txtCustomerOredrNo.Text = "";
        txtPODate.Text = "";
        txtPrimarySalesPerson.Text = "";
        txtAdditionalSalesPerson.Text = "";
        txtCustomerSegment.Text = "";
        txtOrderType.Text = "";
        txtCNDetails.Text = "";
        txtTotalAmount.Text = "";
    
    }
    public void bind_PaymentDetails()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            string Sqlquery = string.Format("SELECT i.*,pt.paymentTypeDesc from InstallmentDetails i,ChallancumInvoice ci,PaymentType pt where pt.paymentTypeId=i.paymentTypeId and ci.challanId=i.challanId and ci.soirId='"+drInvoiceNo.SelectedValue+"'");

            DataSet _Ds = conn.GetDataSet(Sqlquery, "PaymentDetails");
            if (_Ds.Tables[0].Rows.Count != 0)
            {
                GVPaymentDetails.DataSource = _Ds;
                GVPaymentDetails.DataBind();
                GVPaymentDetails.Visible = true;
                btnexpToExcel.Enabled = true;
            }
            else
            {
                GVPaymentDetails.Visible = false;
                btnexpToExcel.Enabled = false;
            }

        }
    
    
    }
    protected void btnexptoexcel_Click(object sender, EventArgs e)
    {


        GridViewExportUtil.Export("PurchaseOrderRegister.xls", GVPaymentDetails);

    }
   
}
