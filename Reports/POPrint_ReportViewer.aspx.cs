﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.Reporting.WebForms;
using Bitscrape.AppBlock.Database;

public partial class Reports_POPrint_ReportViewer : BasePage
{
    protected int _poId = -1;
    //string _poid;
    protected int _poStateId = -1;
  
    int _poTypeId = -1;
    int _vendorId = -1;
   

    //protected void Page_Load(object sender, EventArgs e)
    //{
    //    if (!IsLoggedIn())
    //    {
    //        Response.Redirect("./../Login.aspx");
    //    }
    //    //if (!string.IsNullOrEmpty(Request["PurchaseOrderId"]))
    //    //{
    //    //    _poId = int.Parse(Request["PurchaseOrderId"]);
    //    //}
    //    _poId = 50;
    //    //_poid = "50";
    //    _poTypeId = 1;
    //    _vendorId = 10015;

    //    if (!this.IsPostBack)
    //    {
    //        using (IConnection conn = DataConnectionFactory.GetConnection())
    //        {

    //            SpParam _isdeleted1 = new SpParam("@isdeleted", 0, SqlDbType.Int);
    //            SpParam _poId1 = new SpParam("@_poId", _poId, SqlDbType.Int);
    //            SpParam _poId2 = new SpParam("@_poId", _poId, SqlDbType.Int);
    //            SpParam _poTypeId1 = new SpParam("@_poTypeId", _poTypeId, SqlDbType.Int);
    //            SpParam _vendorId1 = new SpParam("@_vendorId", _vendorId, SqlDbType.Int);
                
               
    //            DataSet _Ds1 = new DataSet();
    //            DataSet _Ds2 = new DataSet();
    //            DataSet _Ds3 = new DataSet();
    //            DataSet _Ds4 = new DataSet();
    //            DataSet _Ds5 = new DataSet();

    //            _Ds1 = conn.GetDataSet("Lum_Erp_Stp_GetPOforPrint", _isdeleted1, _poId1);
    //            //_Ds2 = conn.GetDataSet("select strPoId,poDate from Lum_Erp_PurchaseOrder where poId={0}", _poid);
    //            //_Ds3 = conn.GetDataSet("", _poTypeId1);
    //            //_Ds4 = conn.GetDataSet("", _vendorId1);
    //            //_Ds5 = conn.GetDataSet("");


              

    //            ReportDataSource datasource1 = new ReportDataSource("DataSet_POPrint_Lum_Erp_Stp_GetPOforPrint", _Ds1.Tables[0]);
    //            //ReportDataSource datasource2 = new ReportDataSource("DataSet_POPrint_Lum_Erp_PurchaseOrder", _Ds2.Tables[0]);
    //            //ReportDataSource datasource3 = new ReportDataSource("DataSet_POPrint_Lum_Erp_PurchaseOrderType", _poTypeId1);
    //            //ReportDataSource datasource4 = new ReportDataSource("DataSet_POPrint_Lum_Erp_Vendor", _vendorId1);
    //            //ReportDataSource datasource5 = new ReportDataSource("DataSet_POPrint_Lum_Erp_CompanyMaster");

    //            ReportViewer1.LocalReport.DataSources.Clear();

    //            ReportViewer1.LocalReport.DataSources.Add(datasource1);
    //            //ReportViewer1.LocalReport.DataSources.Add(datasource2);
    //            //ReportViewer1.LocalReport.DataSources.Add(datasource3);
    //            //ReportViewer1.LocalReport.DataSources.Add(datasource4);
    //            //ReportViewer1.LocalReport.DataSources.Add(datasource5);

    //            if (_Ds1.Tables[0].Rows.Count == 0)
    //            {
    //                //lblMessage.Text = "Sorry, no products under this category!";
    //            }

    //            //using (IDataReader reader = conn.ExecuteQuery("select poId,strPoId,poDate from Lum_Erp_PurchaseOrder where poId = " + _poId1))
    //            //{
    //            //    if (reader.Read())
    //            //    {

    //            //    }
    //            //}
    //        }   

    //        ReportViewer1.LocalReport.Refresh();

    //    }
    //}
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        _poId = 50;

        if (!this.IsPostBack)
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                SpParam _isdeleted1 = new SpParam("@isdeleted", 0, SqlDbType.Int);
                SpParam _poId1 = new SpParam("@_poId", _poId, SqlDbType.Int);
                DataSet _Ds = new DataSet();
                _Ds = conn.GetDataSet("Lum_Erp_Stp_GetPOforPrint", _isdeleted1, _poId1);

                ReportDataSource datasource = new
                        ReportDataSource("DataSet_POPrint_Lum_Erp_Stp_GetPOforPrint", _Ds.Tables[0]);

                //ReportViewer1.Reset();
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(datasource);
                
                if (_Ds.Tables[0].Rows.Count == 0)
                {
                    //lblMessage.Text = "Sorry, no products under this category!";
                }
            }

            ReportViewer1.LocalReport.Refresh();

        }
    }
}
