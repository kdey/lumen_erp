﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.Reporting.WebForms;
using Bitscrape.AppBlock.Database;


public partial class Reports_ReportViewer : BasePage
{
    protected int _poId = -1;
    protected int _vendorId = -1;
    protected int _poStateId = -1;
    private double totalAmount = 0;
    private double totalVATAmount = 0;
    private double GrandtotalAmount = 0;
    private double totalwithtax = 0;
    bool isdeleted = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
         _poId = 50;
         _vendorId = 1000;

         if (!this.IsPostBack)
         {
             using (IConnection conn = DataConnectionFactory.GetConnection())
             {
                 //Added_Start
                 string _staxrate = "";
                 double t1 = 0;
                 double t2 = 0;
                 DataTable aTable = new DataTable();
                 
                 DataRow dtRow;
                 DataColumn dtCol;

                 dtCol = new DataColumn();
                 dtCol.DataType = System.Type.GetType("System.String");
                 dtCol.ColumnName = "poLineId";
                 aTable.Columns.Add(dtCol);

                 dtCol = new DataColumn();
                 dtCol.DataType = System.Type.GetType("System.String");
                 dtCol.ColumnName = "itemCode";
                 aTable.Columns.Add(dtCol);

                 dtCol = new DataColumn();
                 dtCol.DataType = System.Type.GetType("System.String");
                 dtCol.ColumnName = "itemDescription";
                 aTable.Columns.Add(dtCol);


                 dtCol = new DataColumn();
                 dtCol.DataType = System.Type.GetType("System.String");
                 dtCol.ColumnName = "quantity";
                 aTable.Columns.Add(dtCol);


                 dtCol = new DataColumn();
                 dtCol.DataType = System.Type.GetType("System.String");
                 dtCol.ColumnName = "uomName";
                 aTable.Columns.Add(dtCol);

                 dtCol = new DataColumn();
                 dtCol.DataType = System.Type.GetType("System.String");
                 dtCol.ColumnName = "price";
                 aTable.Columns.Add(dtCol);
                 dtCol = new DataColumn();
                 dtCol.DataType = System.Type.GetType("System.String");
                 dtCol.ColumnName = "total";
                 aTable.Columns.Add(dtCol);

                 dtCol = new DataColumn();
                 dtCol.DataType = System.Type.GetType("System.String");
                 dtCol.ColumnName = "vatAmount";
                 aTable.Columns.Add(dtCol);

                 dtCol = new DataColumn();
                 dtCol.DataType = System.Type.GetType("System.String");
                 dtCol.ColumnName = "CurrencyToWords";
                 aTable.Columns.Add(dtCol);

                 DataRow drProduct;
                 //Added_End

                 SpParam _isdeleted1 = new SpParam("@isdeleted", 0, SqlDbType.Int);
                 SpParam _poId1 = new SpParam("@_poId", _poId, SqlDbType.Int);
                 DataSet _Ds = new DataSet();
                 DataSet _Ds1 = new DataSet();
                 DataSet _Ds2 = new DataSet();
                 DataSet _Ds3 = new DataSet();
                 _Ds = conn.GetDataSet("Lum_Erp_Stp_GetPOforPrint", _isdeleted1, _poId1);

                 //Added_Start
                 for (int _index = 0; _index < _Ds.Tables[0].Rows.Count; _index++)
                 {
                     drProduct = aTable.NewRow();

                     drProduct["poLineId"] = _Ds.Tables[0].Rows[_index]["poLineId"].ToString();
                     drProduct["itemCode"] = _Ds.Tables[0].Rows[_index]["itemCode"].ToString();
                     drProduct["itemDescription"] = _Ds.Tables[0].Rows[_index]["itemDescription"].ToString();
                     drProduct["quantity"] = _Ds.Tables[0].Rows[_index]["quantity"].ToString();
                     drProduct["uomName"] = _Ds.Tables[0].Rows[_index]["uomName"].ToString();
                     drProduct["price"] = string.Format("{0:#,##0.00}", (Convert.ToDouble(_Ds.Tables[0].Rows[_index]["price"])));
                     drProduct["total"] = string.Format("{0:#,##0.00}", (Convert.ToDouble(_Ds.Tables[0].Rows[_index]["total"])));
                     drProduct["vatAmount"] = _Ds.Tables[0].Rows[_index]["vatAmount"].ToString();
                     CalcTotalAmount(drProduct["total"].ToString());
                     CalcTotalVATAmount(drProduct["vatAmount"].ToString());
                     CalcTotalwithtax(_Ds.Tables[0].Rows[_index]["totalwithtax"].ToString());
                     _staxrate = (_Ds.Tables[0].Rows[0]["vat"].ToString());
                     aTable.Rows.Add(drProduct);
                 }
                 drProduct = aTable.NewRow();
                 drProduct["price"] = "Total";
                 drProduct["total"] = string.Format("{0:#,##0.00}", (totalAmount));

                 aTable.Rows.Add(drProduct);

                 drProduct = aTable.NewRow();
                 drProduct["price"] = "Tax @" + " " + _staxrate;
                 drProduct["total"] = string.Format("{0:#,##0.00}", (totalVATAmount));

                 aTable.Rows.Add(drProduct);

                 GrandtotalAmount = totalAmount + totalVATAmount;

                 drProduct = aTable.NewRow();
                 drProduct["price"] = "GrandTotal";
                 drProduct["total"] = string.Format("{0:#,##0.00}", (totalwithtax));

                 aTable.Rows.Add(drProduct);
                 t1 = Convert.ToDouble(totalwithtax);
                 t2 = Math.Round(totalwithtax);

                 if (t2 > t1)
                 {
                     drProduct = aTable.NewRow();
                     drProduct["price"] = "Round Off ";
                     drProduct["total"] = "(+)" + string.Format("{0:#,##0.00}", (t2 - t1));
                     aTable.Rows.Add(drProduct);
                 }
                 if (t2 < t1)
                 {
                     drProduct = aTable.NewRow();
                     drProduct["price"] = "Round Off ";
                     drProduct["total"] = "(-)" + string.Format("{0:#,##0.00}", (t1 - t2));
                     aTable.Rows.Add(drProduct);
                 }

                 drProduct = aTable.NewRow();
                 drProduct["price"] = "Total";
                 drProduct["total"] = string.Format("{0:#,##0.00}", (Math.Round(totalwithtax)));

                 aTable.Rows.Add(drProduct);
                 //String Price = changeCurrencyToWords(Math.Round(totalwithtax));

                 drProduct = aTable.NewRow();
                 drProduct["CurrencyToWords"] = changeCurrencyToWords(Math.Round(totalwithtax)); 

                 aTable.Rows.Add(drProduct);                
                 //Added_End


                 ReportDataSource datasource = new
                         ReportDataSource("DataSet1_Lum_Erp_Stp_GetPOforPrint", aTable);//_Ds.Tables[0]);

                 ReportViewer1.LocalReport.DataSources.Clear();
                 ReportViewer1.LocalReport.DataSources.Add(datasource);
                 if (_Ds.Tables[0].Rows.Count == 0)
                 {
                     //lblMessage.Text = "Sorry, no products under this category!";
                 }
                  _Ds1 = conn.GetDataSet(string.Format(@"select TIN,ServiceTaxRegnNo,* FROM Lum_Erp_CompanyMaster where 
                                                                fromdate <=DATEADD(MI,330,GETUTCDATE() ) and 
                                                                isnull(todate,DATEADD(MI,330,GETUTCDATE() )) >=
                                                                    DATEADD(MI,330,GETUTCDATE() )"),"CM");

                 ReportDataSource datasource1 = new
                         ReportDataSource("DataSet1_Lum_Erp_CompanyMaster", _Ds1.Tables[0]);

                 ReportViewer1.LocalReport.DataSources.Clear();
                 ReportViewer1.LocalReport.DataSources.Add(datasource);
                 ReportViewer1.LocalReport.DataSources.Add(datasource1);

                  _Ds2 = conn.GetDataSet(string.Format(@"select poId,strPoId,poDate from Lum_Erp_PurchaseOrder where poId ={0}", _poId), "PO");

                 ReportDataSource datasource2 = new
                         ReportDataSource("DataSet1_Lum_Erp_PurchaseOrder", _Ds2.Tables[0]);

                 ReportViewer1.LocalReport.DataSources.Clear();
                 ReportViewer1.LocalReport.DataSources.Add(datasource);
                 ReportViewer1.LocalReport.DataSources.Add(datasource1);
                 ReportViewer1.LocalReport.DataSources.Add(datasource2);

                 _Ds3 = conn.GetDataSet(string.Format(@"select vendorId, vendorName,(address1+' '+address2)as address ,VATRegistrationNo from Lum_Erp_Vendor where vendorId={0}", _vendorId), "VENDOR");

                 ReportDataSource datasource3 = new
                         ReportDataSource("DataSet1_Lum_Erp_Vendor", _Ds3.Tables[0]);

                 ReportViewer1.LocalReport.DataSources.Clear();
                 ReportViewer1.LocalReport.DataSources.Add(datasource);
                 ReportViewer1.LocalReport.DataSources.Add(datasource1);
                 ReportViewer1.LocalReport.DataSources.Add(datasource2);
                 ReportViewer1.LocalReport.DataSources.Add(datasource3);
             }

             ReportViewer1.LocalReport.Refresh();

         }
    }
    //Added_Start
    private void CalcTotalAmount(string _netVal)
    {
        if (_netVal != "")
        {
            totalAmount += Double.Parse(_netVal);
        }

    }
    private void CalcTotalVATAmount(string _VatAmount)
    {
        if (_VatAmount != "")
        {
            totalVATAmount += Double.Parse(_VatAmount);
        }

    }
    private void CalcTotalwithtax(string _totatwithtax)
    {
        if (_totatwithtax != "")
        {
            totalwithtax += Double.Parse(_totatwithtax);
        }

    }
    //Added_End
}
