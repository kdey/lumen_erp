﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;
using System.Data.SqlClient;

public partial class Reports_SalesRegister : BasePage
{
    //private double totalAmount = 0;
    double totalAmtBeforeVat = 0;
    double totalVat = 0;
    double totalAmtAfterVat = 0;
    double totalBuyBack = 0;
    double totalPacking = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");

        }
        if (!IsPostBack)
        {
           
        }
    }
    public void bindGrid()
    {
        try
        {
          //  string fromDate = Convert.ToDateTime(txtFromDate.Text).ToString("dd MMM yyyy"); 
          //// DateTime fromDate = DateTime.ParseExact(txtFromDate.Text, "dd/MM/yyyy", null);
          //  string toDate = Convert.ToDateTime(txtToDate.Text).ToString("dd MMM yyyy");
            DateTime fromDate1 = DateTime.ParseExact(txtFromDate.Text, "dd/MM/yyyy", null);
            DateTime toDate1 = DateTime.ParseExact(txtToDate.Text, "dd/MM/yyyy", null);
            string fromDate = fromDate1.ToString("dd MMM yyyy");
            string toDate = toDate1.ToString("dd MMM yyyy");

            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                SpParam _spFdate = new SpParam("@Fdate", fromDate, SqlDbType.SmallDateTime);
                SpParam _spTdate = new SpParam("@Tdate", toDate, SqlDbType.SmallDateTime);
                //string Sqlquery = string.Format(@"", fromDate, toDate);
                DataSet _Ds = conn.GetDataSet("Lum_Erp_Stp_GetSalesRegisterDetails", _spFdate, _spTdate);
                ReportGrid.DataSource = _Ds;
                ReportGrid.DataBind();
                if (_Ds.Tables[0].Rows.Count != 0)
                {
                    
                    btnexpToExcel.Enabled = true;
                }
                else
                    btnexpToExcel.Enabled = false;
                
              
            }
        }
        catch (Exception ex)
        {
            if (Convert.ToString(ex.Message) == "String was not recognized as a valid DateTime.")
            {
                Page.RegisterClientScriptBlock("message", "<script>alert('Date was not in correct Format...');</script>");
                //lblMsg.Text = "Date was not in correct Format";
            }
            else
            {
                lblMsg.Text = Convert.ToString(ex.Message);
            }

        }
    }
    
    protected void btnReport_Click(object sender, EventArgs e)
    {
        bindGrid();
    }

    
    private double CalcTotalAmount(string _netVal)
    {
        double  retVal;
        if (double.TryParse(_netVal, out retVal))
          {
              retVal = Double.Parse(_netVal);
           
          }
        return retVal;
    }
    protected void btnexptoexcel_Click(object sender, EventArgs e)
    {

        GridViewExportUtil.Export("SalesRegister.xls", ReportGrid);




    }

    protected void ReportGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    totalAmtBeforeVat += CalcTotalAmount(e.Row.Cells[5].Text);
        //    totalVat += CalcTotalAmount(e.Row.Cells[6].Text);
        //    totalAmtAfterVat += CalcTotalAmount(e.Row.Cells[7].Text);
        //    totalBuyBack += CalcTotalAmount(e.Row.Cells[8].Text);
        //    totalPacking += CalcTotalAmount(e.Row.Cells[9].Text);
        //}
        //if (e.Row.RowType == DataControlRowType.Footer)
        //{
        //    e.Row.Cells[2].Text = "Total";
        //    e.Row.Cells[5].Text = string.Format("{0:N}", totalAmtBeforeVat); //lblAmtBeforeVat.Text;// totalAmtBeforeVat.ToString();
        //    e.Row.Cells[6].Text = string.Format("{0:N}", totalVat);
        //    e.Row.Cells[7].Text = string.Format("{0:N}", totalAmtAfterVat);
        //    e.Row.Cells[8].Text = string.Format("{0:N}", totalBuyBack);
        //    e.Row.Cells[9].Text = string.Format("{0:N}", totalPacking);
        //}
    }
}
