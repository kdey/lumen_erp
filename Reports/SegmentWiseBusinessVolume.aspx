﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/Reports.master" AutoEventWireup="true" CodeFile="SegmentWiseBusinessVolume.aspx.cs" Inherits="Reports_SegmentWiseBusinessVolume" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
   
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="PageMargin">
<div class="PageHeading">Segment Wise Business Volume</div>
<div class="Line"></div>
<!--Content Table Start-->
<div class="PageContent"><asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>

      <table width="100%" border="0" cellspacing="5" cellpadding="0">
    <tr>
      <td align="left" valign="middle" >Select Customer Segment :</td>
      <td align="left" valign="middle">
                <asp:DropDownList ID="DrSegment" 
                    runat="server" Width="150px" CssClass="TextboxSmall">
                </asp:DropDownList>
      </td>
      <td align="left" valign="middle"  >
                Select Date:From&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
      <td align="left" valign="middle" >
                <span >
                <asp:TextBox ID="txtDatefrom" runat="server" Width="150px" CssClass="TextboxSmall" onkeyup="document.getElementById(this.id).value=''" autocomplete='off'></asp:TextBox>
                        <cc1:calendarextender ID="CalendarExtender3" runat="server" 
                            TargetControlID="txtDatefrom" PopupButtonID="txtDatefrom">
                        </cc1:calendarextender>
                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" 
                            TargetControlID="txtDatefrom" PopupButtonID="txtDatefrom">
                          
                        </cc1:CalendarExtender>
                </span>
            </td>
      <td align="left" valign="middle" >Select Date :To&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
      <td  align="right" valign="middle"><asp:TextBox ID="txtDateTo" runat="server" Width="150px" CssClass="TextboxSmall" onkeyup="document.getElementById(this.id).value=''" autocomplete='off'></asp:TextBox>
               <cc1:CalendarExtender ID="CalendarExtender2" runat="server" 
                            TargetControlID="txtDateTo" PopupButtonID="txtDateTo">
                        </cc1:CalendarExtender>
                        
                          </td>
      <td  align="left" valign="middle">
                <asp:Button ID="btnSearch" runat="server" Text="Report" 
                    onclick="btnSearch_Click"  style="width: 61px" CssClass="Button"/>
               
                        <br />
        </td>
    </tr>
    <tr>
      <td align="left" valign="middle" >&nbsp;</td>
      <td  align="left" valign="middle">
          &nbsp;</td>
      <td align="left" valign="middle"  >
          &nbsp;</td>
      <td align="left" valign="middle" >
        
                            <asp:RequiredFieldValidator ControlToValidate="txtDatefrom" Display="Dynamic"  ErrorMessage="Start Date cannot be empty"
                            ID="RequiredFieldValidator2" runat="server" />
               
                </td>
      <td align="left" valign="middle">&nbsp;</td>
      <td  align="right" valign="middle">
                            <asp:RequiredFieldValidator ControlToValidate="txtDateTo" Display="Dynamic" ErrorMessage="End Date cannot be empty"
                            ID="RequiredFieldValidator1" runat="server" />
                </td>
      <td align="left" valign="middle">
                        

                <asp:Button ID="btnexpToExcel" runat="server" Text="Export to Excel" 
                    onclick="btnexptoexcel_Click" ValidationGroup="report" 
              style="width: 120px; height: 19px;" CssClass="Button" Enabled="False"/>
               </td>
    </tr>
    <tr>
      <td align="left" valign="middle" >&nbsp;</td>
      <td  align="left" valign="middle">
          &nbsp;</td>
      <td align="left" valign="middle"  >
          &nbsp;</td>
      <td align="left" valign="middle" >
        
                            &nbsp;</td>
      <td align="left" valign="middle">&nbsp;</td>
      <td  align="right" valign="middle" colspan="2">
                        
                          <asp:CompareValidator ID="CompareValidator1"  runat="server" 
                            ErrorMessage="End date should be greater than start date" 
                            ControlToCompare="txtDatefrom" ControlToValidate="txtDateTo"
                            Type="Date" Operator="GreaterThanEqual"></asp:CompareValidator>
                </td>
    </tr>
    </table>                 
   
</div>
<!--Content Table End-->
<div class="Line"></div>
<div class="PageContent">
<table width="100%" align="center" cellpadding="0" cellspacing="5">
  	<tr>
          <asp:GridView  ID="GVsegmentWiseBusiness" runat="server" 
                    AutoGenerateColumns="true" CellPadding="0" CssClass="ReportGridHeading"
                    BorderColor="#6699CC" BorderStyle="Solid" BorderWidth="0px"
                    Width="100%" BackColor="Blue" 
              CaptionAlign="Bottom" CellSpacing="1" 
              onselectedindexchanged="GVsegmentWiseBusiness_SelectedIndexChanged">
              <RowStyle CssClass="ReportGridData" />
              <HeaderStyle CssClass="ReportGridHeading" />
          </asp:GridView>
                
    </tr>
	<tr>
	<td>
	    &nbsp;</td>
  </tr>
</table> 
</div>

<!--Page Body End-->
</div>
</asp:Content>

