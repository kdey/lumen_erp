﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;
using System.Data.SqlClient;

public partial class Reports_SegmentWiseBusinessVolume : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");

        }
        if (!IsPostBack)
        {
            bindControl();

        }
    }
    public void bindControl()
    {



        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DataSet _Ds = conn.GetDataSet("select * from lum_erp_CustomerSegment order by segmentType", "CustomerSegment");
            DrSegment.ClearSelection();
            DrSegment.Items.Clear();
            DrSegment.AppendDataBoundItems = true;
            DrSegment.Items.Add("--ALL--");
            DrSegment.Items[0].Value = "0";
            DrSegment.DataTextField = _Ds.Tables[0].Columns["segmentType"].ToString();
            DrSegment.DataValueField = _Ds.Tables[0].Columns["segmentId"].ToString();
            DrSegment.DataSource = _Ds;
            DrSegment.DataBind();
        }

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        Bind_SegmentWise_Business();
    }
    public void Bind_SegmentWise_Business()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {

            String dt1 = Convert.ToDateTime(txtDatefrom.Text).ToString("dd MMM yyyy");
            String dt2 = Convert.ToDateTime(txtDateTo.Text).ToString("dd MMM yyyy");

            String dtfrom = String.Format("{0:dd MMM yyyy}", dt1);
            String dtTo = String.Format("{0:dd MMM yyyy}", dt2);


            int segmentId = Convert.ToInt32(DrSegment.SelectedValue);

            SpParam _spsegmentId = new SpParam("@segmentId", segmentId, SqlDbType.VarChar);
            SpParam _spdtfrom = new SpParam("@FROMDATE", dt1, SqlDbType.VarChar);
            SpParam _spdtTo = new SpParam("@TODATE", dt2, SqlDbType.VarChar);

            DataSet _Ds = conn.GetDataSet("Lum_Erp_Stp_GetSEGMENT_Performance", _spsegmentId, _spdtfrom, _spdtTo);

            //decimal TotalAmount = 0;
            //for (int _index1 = 0; _index1 < _Ds.Tables[0].Rows.Count; _index1++)
            //{
            //    if (_Ds.Tables[0].Rows[_index1]["Netamt"].ToString() != "")

            //        TotalAmount = TotalAmount + Convert.ToDecimal(_Ds.Tables[0].Rows[_index1]["Netamt"].ToString());
            //}
            //GrandTotal = GrandTotal + TotalAmount;
            if (_Ds.Tables[0].Rows.Count != 0)
            {
                GVsegmentWiseBusiness.DataSource = _Ds;
                GVsegmentWiseBusiness.DataBind();
                GVsegmentWiseBusiness.Visible = true;
                btnexpToExcel.Enabled = true;
            }
            else
            {
                GVsegmentWiseBusiness.Visible = false;
                btnexpToExcel.Enabled = false;

            }

        }
    }
    protected void btnexptoexcel_Click(object sender, EventArgs e)
    {
        GridViewExportUtil.Export("SegmentWiseBusinessVolume.xls", GVsegmentWiseBusiness);



    }

    protected void GVsegmentWiseBusiness_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
