﻿<%@ Page Language="C#" MasterPageFile="~/Reports/Reports.master" AutoEventWireup="true" CodeFile="Outstanding.aspx.cs" Inherits="Reports_Outstanding" Title="Lumen ERP.....Outstanding" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            height: 33px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="PageMargin">
<div class="PageHeading">OutStanding</div>
<div class="Line">
      <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    
                     </div>
<!--Content Table Start-->
<div class="PageContent">
   <table width="100%" border="0" cellspacing="5" cellpadding="0">
    <tr>
  
      <td align="left" valign="middle" class="style1">Invoice Type :</td>
      <td  align="left" valign="middle" class="style1">
                            <asp:DropDownList ID="drInvoiceType" runat="server" Width="150px" 
                                AutoPostBack="True"  CssClass="TextboxSmall">
                                <asp:ListItem Value="2">Sales</asp:ListItem>
                                <asp:ListItem Value="1">Services</asp:ListItem>
                                <asp:ListItem Value="3">CSA Services</asp:ListItem>
                            </asp:DropDownList>
                        </td>

                     <td align="left" valign="middle" class="style1" >
                         Outstanding AS ON</td>
                     <td class="style1">     
                             <asp:TextBox ID="txtoutdate" runat="server" onkeyup="document.getElementById(this.id).value=''" 
                    autocomplete='off'  CssClass="TextboxSmall" Width="150px"></asp:TextBox>
                    <br />(dd/MM/yyyy)<cc1:calendarextender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="txtoutdate">
                            </cc1:calendarextender>                                   
                    </td>
                    <td align="center" valign="middle"  colspan="8">
                        <asp:Button ID="btnsave" runat="server" Text="Report" 
                    ValidationGroup="report" 
              style="width: 120px; height: 19px;" CssClass="Button" onclick="btnsave_Click" />
                    </td>    
       <td align="center" valign="middle"  colspan="8">
                        <asp:Button ID="btnexpToExcel" runat="server" Text="Export To Excel" 
                    ValidationGroup="report" Enabled ="false"
              style="width: 120px; height: 19px;" CssClass="Button" onclick="btnexptoexcel_Click" />
                    </td>                                       
    </tr>                 
     </table>
  </div>               
<!--Content Table End-->
    </div>
    <div class="Line">
        <asp:Label ID="gridMessage" runat="server" />
    </div>
    <div class="PageContent">
        <table width="100%" align="center" cellpadding="0" cellspacing="5">
            <tr>
                <td width="15%" align="left" valign="top">
                    Payment Details:</td>
            </tr>
            <tr>
                <td width="15%" align="left" valign="top">
                    <asp:GridView ID="GVoutstandingService" runat="server" 
                        AutoGenerateColumns="False" BorderColor="#6699CC" BorderStyle="Solid" 
                        BorderWidth="0px" CellPadding="0" CssClass="ReportGridHeading" 
                        ShowFooter="True" Width="100%" >
                        <RowStyle HorizontalAlign="Center" />
                        <Columns>
                            <asp:BoundField DataField="ServiceDate" HeaderText="Invoice Date" 
                            HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                                                       
                            <asp:BoundField DataField="servicebillno" HeaderText="Invoice No" 
                            HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                                       
                            <asp:BoundField DataField="customerName" HeaderText="Customer Name" HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>                            
                            <asp:BoundField DataField="RoundTAMT" HeaderText="Amt Receivable (till date)" 
                              HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" 
                                ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="AMOUNTRECOCOVER" HeaderText="Amt Recover (till date)" HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="OUTSTANDING" HeaderText="OutStanding" HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="age" HeaderText="Overdue (In Days)" HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="segmentType" HeaderText="Customer Segment" HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="centerName" HeaderText="Service Center" HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="serviceContractType" HeaderText="Service Bill Type" HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>                            
                                               
                        </Columns>
                        <HeaderStyle CssClass="ReportGridHeading" />
                    </asp:GridView>
                    
            <asp:GridView ID="GVoutstandingSales" runat="server" 
            AutoGenerateColumns="False" BorderColor="#6699CC" BorderStyle="Solid" 
            BorderWidth="0px" CellPadding="0" CssClass="ReportGridHeading" 
            ShowFooter="True" Width="100%" >
                        <RowStyle HorizontalAlign="Center" />
                        <Columns>
                            <asp:BoundField DataField="activityDateTime" HeaderText="Invoice Date" 
                            HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="P_salespersonname" HeaderText="P_salespersonname" 
                            HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="A_salespersonname" HeaderText="A_salespersonname" 
                            HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                                                        
                            <asp:BoundField DataField="strChallanId" HeaderText="Invoice No" 
                            HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                                       
                            <asp:BoundField DataField="customerName" HeaderText="Customer Name" HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>                            
                            
                            <asp:BoundField DataField="AMOUNTRECEIVABLE" HeaderText="Amt Receivable (till date)" 
                            HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" 
                                ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="AMOUNTRECOCOVER" HeaderText="Amt Recover (till date)" HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="OUTSTANDING" HeaderText="OutStanding" HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="age" HeaderText="Overdue (In Days)" HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>                            
                            <asp:BoundField DataField="segmentType" HeaderText="Customer Segment" HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>                                               
                        </Columns>
                        <HeaderStyle CssClass="ReportGridHeading" />
                    </asp:GridView> 
                    
                    <asp:GridView ID="GVoutstandingCSAService" runat="server" 
                            AutoGenerateColumns="False" BorderColor="#6699CC" BorderStyle="Solid" 
                            BorderWidth="0px" CellPadding="0" CssClass="ReportGridHeading" 
                            ShowFooter="True" Width="100%" >
                        <RowStyle HorizontalAlign="Center" />
                        <Columns>
                            <asp:BoundField DataField="CODATE" HeaderText="Invoice Date" 
                            HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData" DataFormatString="{0:dd/MMM/yyyy}">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="strServiceId" HeaderText="Invoice No." 
                            HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="customerName" HeaderText="Customer Name" 
                            HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" 
                                ItemStyle-CssClass="ReportGridData" >
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="TAmt" HeaderText="Amount To Be Received" 
                            HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="PAmt" HeaderText="Amount Recovered" HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="DAmt" HeaderText="Outstanding" HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="age" HeaderText="Overdue (In Days)" HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="CONO" HeaderText="Customer Order No." 
                            HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                            
                                                 
                        </Columns>
                        <HeaderStyle CssClass="ReportGridHeading" />
                    </asp:GridView>
                                       
                </td>
            </tr>
        </table>
    </div>
</asp:Content>



