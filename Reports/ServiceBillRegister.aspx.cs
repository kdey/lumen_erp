﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;
using System.Data.SqlClient;

public partial class Reports_ServiceBillRegister : BasePage
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");

        }
        if (!IsPostBack)
        {

        }
    }
    public void bindGrid()
    {
        try
        {
            ////string fromDate = Convert.ToDateTime(txtFromDate.Text).ToString("dd MMM yyyy");
            //string fromDate = Convert.ToDateTime(txtFromDate.Text).ToString(CalendarExtender2.Format);
            ////fromDate = DateTime.Now.ToString(CalendarExtender2.Format);
            //string toDate = Convert.ToDateTime(txtToDate.Text).ToString("dd MMM yyyy");
            DateTime fromDate1 = DateTime.ParseExact(txtFromDate.Text, "dd/MM/yyyy", null);
            DateTime toDate1 = DateTime.ParseExact(txtToDate.Text, "dd/MM/yyyy", null);
            string fromDate = fromDate1.ToString("dd MMM yyyy");
            string toDate = toDate1.ToString("dd MMM yyyy");

            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                string Sqlquery = string.Format(@"SELECT CONO,CONVERT(VARCHAR,ChallancreateDate,106) AS ServiceDate ,
                strServiceId as servicebillno,price,CAST (Servicetax AS DECIMAL(18,2)) AS Servicetax,CAST (Educess AS DECIMAL(18,2)) AS Educess,CAST (HEducess AS DECIMAL(18,2)) AS HEducess
                ,VAT,
                CAST (TAmt AS DECIMAL(18,2)) AS TAMT, CAST (ROUND(TAmt,0) AS DECIMAL(18,2)) AS RoundTAMT, 
                cast (CASE WHEN TAmt > ROUND(TAmt,0) THEN  (TAmt - ROUND(TAmt,0)) ELSE (ROUND(TAmt,0) - TAmt) END AS DECIMAL(18,2)) AS ROUNFOFFAMT
                ,C.customerName ,centerName,
                (Billing_address1+ ' '+ Billing_address2 +' '+ Billing_zipCode) AS CAddress ,
                LUMS.serviceContractId,serviceContractType , serviceContractDescription,
                LUMS.serialNo,LUMS.customerOrderNo,LUMS.isDeleted,slog.ChallancreateDate
                FROM
	                (SELECT CONO,CODATE AS ServiceDate,customerId,ChallanId,strServiceId,price , 
				                ((serviceTax*.01) * price) AS Servicetax,
				                (((serviceTax*.01) * price)*(ISNULL(eduCess,0)*.01)) AS Educess,
				                (((serviceTax*.01) * price)*(educationCess*.01)) AS HEducess,VAT,serviceContractId ,serialNo,customerOrderNo,isDeleted,serviceCenterId,
				                price + ((serviceTax*.01) * price) + (((serviceTax*.01) * price)*(ISNULL(eduCess,0)*.01)) + (((serviceTax*.01) * price)*(educationCess*.01)) AS TAmt
				                FROM
				                (SELECT CONO,CODATE,customerId,ChallanId,strServiceId,HigherEducationCessId,educationCessId
					                ,serviceTaxId ,price,educationCess,ISNULL(serviceTax,0) AS serviceTax ,VAT,serviceContractId,serialNo,customerOrderNo,isDeleted,serviceCenterId
					                FROM
					                (SELECT CONO,CODATE,customerId,ChallanId,strServiceId,HigherEducationCessId,educationCessId,serviceTaxId ,price
						                ,ISNULL(educationCess,0) AS educationCess ,VAT,serviceContractId,serialNo,customerOrderNo,isDeleted,serviceCenterId
						                FROM
						                (SELECT customerOrderNo AS CONO,POdate AS CODATE,customerId,serviceId AS ChallanId ,strServiceId,HigherEducationCessId,educationCessId
							                ,serviceTaxId ,price,VAT     ,serviceContractId  ,serialNo,customerOrderNo,isDeleted  ,serviceCenterId      
							                FROM Lum_Erp_Services) T1	LEFT OUTER JOIN
						                (SELECT higherEducationCessId AS HECId,educationCess 
							                FROM Lum_Erp_HigherEducationCess) T2
						                ON T1.higherEducationCessId=T2.HECId) T3	LEFT OUTER JOIN 
					                (SELECT serviceTaxId AS STId,serviceTax,description 
						                FROM Lum_Erp_ServiceTaxMaster) T4
					                ON T3.serviceTaxId=T4.STId)T5	LEFT OUTER JOIN
				                (SELECT educationCessId AS ECId,eduCess 
					                FROM Lum_Erp_EducationCess) T6
				                ON T5.educationCessId=T6.ECId) 
			                AS LUMS,Lum_Erp_Customer C,Lum_Erp_ServiceContract SC,Lum_Erp_ServiceCenter AS SCENTER,
                            (SELECT serviceId,activityDateTime ChallancreateDate from Lum_Erp_ServiceActivityLog  WHERE activityTypeId=1) slog

                                                WHERE C.customerId=LUMS.customerId AND LUMS.isDeleted =0 
								                AND SC.serviceContractId = LUMS.serviceContractId
                                                AND LUMS.serviceCenterId = SCENTER.serviceCenterId
                                                AND LUMS.ChallanId = SLOG.serviceId
                                                AND slog.ChallancreateDate>='{0}' AND slog.ChallancreateDate<='{1}' 
								                ORDER BY customerName,ServiceDate", fromDate, toDate);
                DataSet _Ds = conn.GetDataSet(Sqlquery, "ReportBind");
                ReportGrid.DataSource = _Ds;
                ReportGrid.DataBind();
                if (_Ds.Tables[0].Rows.Count != 0)
                {
                   
                    btnexpToExcel.Enabled = true;
                }
                else
                    btnexpToExcel.Enabled = false;
                //for (int _index = 0; _index <= _Ds.Tables[0].Rows.Count - 1; _index++)
                //{
                //    totalAmtBeforeVat = totalAmtBeforeVat + Convert.ToDouble(_Ds.Tables[0].Rows[_index]["AmtBeforeVat"].ToString());
                //    totalVat = totalVat + Convert.ToDouble(_Ds.Tables[0].Rows[_index]["Tax"].ToString());
                //    totalAmtAfterVat = totalAmtAfterVat + Convert.ToDouble(_Ds.Tables[0].Rows[_index]["Amt"].ToString());
                //    totalBuyBack = totalBuyBack + Convert.ToDouble(_Ds.Tables[0].Rows[_index]["buyBackAmount"].ToString());
                //    totalPacking = totalPacking + Convert.ToDouble(_Ds.Tables[0].Rows[_index]["packingCharges"].ToString());
                //}

                //ReportGrid.Columns[2].FooterText = "Total";


            }
        }
        catch (Exception ex)
        {
            if (Convert.ToString(ex.Message) == "String was not recognized as a valid DateTime.")
            {
                Page.RegisterClientScriptBlock("message", "<script>alert('Date was not in correct Format...');</script>");
                //lblMsg.Text = "Date was not in correct Format";
            }
            else
            {
                lblMsg.Text = Convert.ToString(ex.Message);
            }

        }
    }

    protected void btnReport_Click(object sender, EventArgs e)
    {
        bindGrid();
    }


    private double CalcTotalAmount(string _netVal)
    {
        double retVal;
        if (double.TryParse(_netVal, out retVal))
        {
            retVal = Double.Parse(_netVal);

        }
        return retVal;
    }
    protected void btnexptoexcel_Click(object sender, EventArgs e)
    {

        GridViewExportUtil.Export("ServiceRegister.xls", ReportGrid);




    }

    protected void ReportGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    totalAmtBeforeVat += CalcTotalAmount(e.Row.Cells[5].Text);
        //    totalVat += CalcTotalAmount(e.Row.Cells[6].Text);
        //    totalAmtAfterVat += CalcTotalAmount(e.Row.Cells[7].Text);
        //    totalBuyBack += CalcTotalAmount(e.Row.Cells[8].Text);
        //    totalPacking += CalcTotalAmount(e.Row.Cells[9].Text);
        //}
        //if (e.Row.RowType == DataControlRowType.Footer)
        //{
        //    e.Row.Cells[2].Text = "Total";
        //    e.Row.Cells[5].Text = string.Format("{0:N}", totalAmtBeforeVat); //lblAmtBeforeVat.Text;// totalAmtBeforeVat.ToString();
        //    e.Row.Cells[6].Text = string.Format("{0:N}", totalVat);
        //    e.Row.Cells[7].Text = string.Format("{0:N}", totalAmtAfterVat);
        //    e.Row.Cells[8].Text = string.Format("{0:N}", totalBuyBack);
        //    e.Row.Cells[9].Text = string.Format("{0:N}", totalPacking);
        //}
    }
}

