﻿<%@ Page Language="C#" MasterPageFile="~/Reports/Reports.master" AutoEventWireup="true" CodeFile="ReversalRegister.aspx.cs" Inherits="Reports_ReversalRegister" Title="Lumen Erp Reports........ReversalRegister" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
        
    
 </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
      <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
        <!--Page Body Start-->
<div class="PageMargin">

<div class="PageHeading">
    <asp:Label ID="lblheading" runat="server"></asp:Label>
    </div>
<div class="Line"></div>
<!--Content Table Start-->
<div class="PageContent">
  <table width="40%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
  	 <tr>
  	      <td align="left" valign="middle" colspan="2">Invoice Type :</td>
          <td  align="left" valign="middle" colspan="2">
                            <asp:DropDownList ID="drInvoiceType" runat="server" Width="150px" 
                                AutoPostBack="True"  CssClass="TextboxSmall">
                                <asp:ListItem Value="2">Sales</asp:ListItem>
                                <asp:ListItem Value="1">Services</asp:ListItem>
                                <asp:ListItem Value="3">CSA Services</asp:ListItem>
                            </asp:DropDownList>
                        </td>
    </tr>
    <tr>                        
	    <td align="left" valign="middle" colspan="4">Select Date:</td>	
    </tr>
    <tr>
      <td width="15%" align="left" valign="middle">From&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
      <td width="35%" align="left" valign="middle"><asp:TextBox ID="txtFromDate"  runat="server" onkeyup="document.getElementById(this.id).value=''" autocomplete='off' CssClass="TextboxSmall" />
      <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="MM/dd/yyyy" TargetControlID="txtFromDate">
      </cc1:CalendarExtender>
      </td>      
	  <td width="15%" align="left" valign="middle">To&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
      <td width="35%" align="left" valign="middle"><asp:TextBox ID="txtToDate"  runat="server" onkeyup="document.getElementById(this.id).value=''" autocomplete='off' CssClass="TextboxSmall"></asp:TextBox>
      <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="MM/dd/yyyy" TargetControlID="txtToDate">
      </cc1:CalendarExtender>
      </td>
    </tr>
    <tr>
      <td width="15%" align="left" valign="middle">&nbsp;</td>
      <td width="35%" align="left" valign="middle">
      <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ControlToValidate="txtFromDate" ErrorMessage="From Date can't be blank" Text="From Date can't be blank"  ></asp:RequiredFieldValidator>
      </td>      
	  <td width="15%" align="left" valign="middle">&nbsp;</td>
      <td width="35%" align="left" valign="middle">
      <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ControlToValidate="txtToDate" ErrorMessage="To Date can't be blank" Text="To Date can't be blank"  ></asp:RequiredFieldValidator>
      </td>
    </tr>
    <tr>
      <td width="15%" align="left" valign="middle">&nbsp;</td>
      <td align="left" valign="middle" colspan="3" style="width: 70%">
       <asp:CompareValidator ID="CompareValidator1"  runat="server" 
                            ErrorMessage="End date should be greater than start date" 
                            ControlToCompare="txtFromDate" ControlToValidate="txtToDate"
                            Type="Date" Operator="GreaterThanEqual"></asp:CompareValidator>
        </td>      
    </tr>
    <tr>
      <td align="right" valign="middle" colspan="2">               
                     <asp:Button ID="btnReport" runat="server" OnClick="btnReport_Click" Text="Report" CssClass="Button"/>
                     </td>
      <td align="left" valign="middle" colspan="2">
                     <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="Button" /></td>
      
    </tr>
    <tr>
      <td align="center" valign="middle" colspan="4">               
      

                <asp:Button ID="btnexpToExcel" runat="server" Text="Export to Excel" 
                    onclick="btnexptoexcel_Click" ValidationGroup="report" 
              style="width: 120px; height: 19px;" CssClass="Button" Enabled="False"/>
                     </td>
      
    </tr>
  </table>
</div>
<!--Content Table End-->
<div class="Line"></div>
<!--Grid Table Start-->
<asp:GridView ID="ReportGridSALES" runat="server" AutoGenerateColumns="false" 
        CellPadding="0" CellSpacing="0" 
HeaderStyle-CssClass="ReportGridHeading" BorderColor="#6699CC" 
    BorderStyle="Solid" BorderWidth="0px"
    Width="100%" CssClass="ReportGridHeading"
        onitemdatabound="ReportGrid_ItemDataBound">
<FooterStyle CssClass="ReportGridHeading" Font-Bold="True"></FooterStyle>

<RowStyle CssClass="ReportGridData" Width="10%"></RowStyle>
            <Columns>
                <asp:BoundField DataField="ChallancreateDate" HeaderText="ChallancreateDate" 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>

                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="customerName" HeaderText="Customer" 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>

                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                                
                <asp:BoundField DataField="strChallanId" HeaderText="Invoice No." 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>

                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="itemCode" HeaderText="Item Part No." 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" FooterText="Total" ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>

                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="itemDescription" HeaderText="Item Description" 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>

                    <ItemStyle Width="3%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="quantity" HeaderText="Qty" 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>

                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="AmtBeforeVat" HeaderText="Amt before VAT (Rs.)" 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>

                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField  DataField="Tax" HeaderText="VAT (Rs.)" 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>

                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="Amt" HeaderText="Amt after VAT (Rs.)" 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>

                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="buyBackAmount" HeaderText="Buy Back" 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                        <HeaderStyle Font-Bold="True"></HeaderStyle>

                        <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="packingCharges" HeaderText="Packing Charges" 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">                
                        <HeaderStyle Font-Bold="True"></HeaderStyle>

                        <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="ChallanreverseDate" HeaderText="Reverse Date" 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>

                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>                
            </Columns>            

<HeaderStyle CssClass="ReportGridHeading"></HeaderStyle>
</asp:GridView>

<asp:GridView ID="ReportGridSERVICE" runat="server" AutoGenerateColumns="false" 
        CellPadding="0" CellSpacing="0" 
HeaderStyle-CssClass="ReportGridHeading" BorderColor="#6699CC" 
    BorderStyle="Solid" BorderWidth="0px"
    Width="100%" CssClass="ReportGridHeading"
        onitemdatabound="ReportGrid_ItemDataBound">
<FooterStyle CssClass="ReportGridHeading" Font-Bold="True"></FooterStyle>

<RowStyle CssClass="ReportGridData" Width="10%"></RowStyle>
            <Columns>
                <asp:BoundField DataField="ChallancreateDate" HeaderText="ChallancreateDate" 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>

                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="customerName" HeaderText="Customer" 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>

                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                                
                <asp:BoundField DataField="strChallanId" HeaderText="Invoice No." 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>

                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="itemCode" HeaderText="Item Part No." 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" FooterText="Total" ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>

                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="itemDescription" HeaderText="Item Description" 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>

                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="eduCess" HeaderText="Amount Before Tax" 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>

                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField  DataField="educationCess" HeaderText="Service Tax" 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>

                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="serviceTax" HeaderText="Education Cess" 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>

                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="price" HeaderText="Higher Education Cess" 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                        <HeaderStyle Font-Bold="True"></HeaderStyle>

                        <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="TAmt" HeaderText="Amt after TAX (Rs.)" 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">                
                        <HeaderStyle Font-Bold="True"></HeaderStyle>

                        <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="ChallanreverseDate" HeaderText="Reverse Date" 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>

                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>                
            </Columns>            

<HeaderStyle CssClass="ReportGridHeading"></HeaderStyle>
</asp:GridView>


<asp:GridView ID="ReportGridCSASERVICE" runat="server" AutoGenerateColumns="false" 
        CellPadding="0" CellSpacing="0" 
HeaderStyle-CssClass="ReportGridHeading" BorderColor="#6699CC" 
    BorderStyle="Solid" BorderWidth="0px"
    Width="100%" CssClass="ReportGridHeading"
        onitemdatabound="ReportGrid_ItemDataBound">
<FooterStyle CssClass="ReportGridHeading" Font-Bold="True"></FooterStyle>

<RowStyle CssClass="ReportGridData" Width="10%"></RowStyle>
            <Columns>
                <asp:BoundField DataField="ServiceBillDate" HeaderText="ChallancreateDate" 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>

                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="customerName" HeaderText="Customer Name" 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>

                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                                
                <asp:BoundField DataField="ServiceBillNo" HeaderText="Invoice No." 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>

                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="itemCode" HeaderText="Item Part No." 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" FooterText="Total" ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>

                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="BillableAmount" HeaderText="Billable Amount " 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>

                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="EducationalCess" HeaderText="Edu.Cess" 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>

                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField  DataField="VAT" HeaderText="VAT" 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>

                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="ServiceTax" HeaderText="Service Tax" 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>

                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <%--<asp:BoundField DataField="InvoiceAmount" HeaderText="Invoice Amount" 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                        <HeaderStyle Font-Bold="True"></HeaderStyle>

                        <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>--%>
                <asp:BoundField DataField="NetPayable" HeaderText="Amt after TAX (Rs.)" 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">                
                        <HeaderStyle Font-Bold="True"></HeaderStyle>

                        <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <%--<asp:BoundField DataField="ChallanreverseDate" HeaderText="Reverse Date" 
                    HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>

                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>--%>                
            </Columns>            

<HeaderStyle CssClass="ReportGridHeading"></HeaderStyle>
</asp:GridView>




<!--Grid Table End-->

<!--Page Body End-->
</div>
  <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>      
        
</asp:Content>

