﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;
using System.Data.SqlClient;

public partial class Reports_customerhistory : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");

        }
        if (!IsPostBack)
        {

        }

    }

    public void bindGrid()
    {
        try
        {

            DateTime fromDate1 = DateTime.ParseExact(txtFromDate.Text, "dd/MM/yyyy", null);
            DateTime toDate1 = DateTime.ParseExact(txtToDate.Text, "dd/MM/yyyy", null);
            string fromDate = fromDate1.ToString("dd MMM yyyy");
            string toDate = toDate1.ToString("dd MMM yyyy");

            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                SpParam[] SP = new SpParam[2];
                SP[0] = new SpParam("@Fdate", fromDate1, SqlDbType.SmallDateTime);
                SP[1] = new SpParam("@Tdate", toDate1, SqlDbType.SmallDateTime);
                using (DataSet DS = conn.GetDataSet("Lum_Erp_Stp_GetCustomerHistoryDetails", SP))
                {
                    ReportGrid.DataSource = DS;
                    ReportGrid.DataBind();
                    if (DS.Tables[0].Rows.Count != 0)
                    {
                        btnexpToExcel.Enabled = true;
                    }
                    else
                        
                        btnexpToExcel.Enabled = false;

                }
            }
        }
        catch (Exception ex)
        {
            if (Convert.ToString(ex.Message) == "String was not recognized as a valid DateTime.")
            {
                Page.RegisterClientScriptBlock("message", "<script>alert('Date was not in correct Format...');</script>");
            }
            else
            {
                lblMsg.Text = Convert.ToString(ex.Message);
            }

        }
    }
    protected void btnReport_Click(object sender, EventArgs e)
    {
        bindGrid();
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {

    }
    protected void btnexptoexcel_Click(object sender, EventArgs e)
    {
        GridViewExportUtil.Export("CustomerRegister.xls", ReportGrid);

    }

    protected void ReportGrid_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            //Build custom header.
            GridView oGridView = (GridView)sender;
            GridViewRow oGridViewRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
            TableCell oTableCell = new TableCell();

            //Add Billing Details
            oTableCell.Text = "Billing Details";
            oTableCell.ColumnSpan = 4;
            oGridViewRow.Cells.Add(oTableCell);

            //Add Payment Details
            oTableCell = new TableCell();
            oTableCell.Text = "Payment Details";
            oTableCell.ColumnSpan = 10;
            oGridViewRow.Cells.Add(oTableCell);
            oGridView.Controls[0].Controls.AddAt(0, oGridViewRow);
        }

    }

}
