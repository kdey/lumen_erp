﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Data.SqlClient;

public partial class Reports_ServiceBillPrint : BasePage
{
    private double totalAmount = 0;
    private double totalNetAmount = 0;
    private double TotalVat = 0;
    private double TotalServiceTax = 0;
    private double TotalEduCess = 0;
    private double TotalHgEduCess = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");

        }
        if (ViewState["previousPage"] == null)
        {
            ViewState["previousPage"] = Convert.ToString(Request.UrlReferrer);
        }
        if (!IsPostBack)
        {
            string serviceid = Request.QueryString["serviceid"].ToString();
            bindbill();

        }
    }
    public void bindbill()
    {
        double t1 = 0;
        double t2 = 0;
        totalAmount = 0;
        totalNetAmount = 0;
        TotalServiceTax = 0;
        TotalEduCess = 0;
        TotalHgEduCess = 0;
        string _staxrate = "";
        string _hcessrate = "";
        string _ecessrate = "";
        string _vat = "";

        string serviceid = Request.QueryString["serviceid"].ToString();
        try
        {
        DataTable aTable = new DataTable();

        DataColumn dtCol;

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "serviceDesc";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "price";

        aTable.Columns.Add(dtCol);
        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "Amount";
        aTable.Columns.Add(dtCol);



        DataRow drProduct;
        SpParam _spServiceId = new SpParam("@serviceID", Convert.ToInt32(serviceid), SqlDbType.Int);
        DataSet _Ds = new DataSet();


        using (IConnection conn = DataConnectionFactory.GetConnection())
        {

            _Ds = conn.GetDataSet("Lum_Erp_Stp_SelectBillInfo", _spServiceId);
        }

                if (_Ds.Tables[0].Rows.Count != 0)
                {
                    for (int _index = 0; _index < _Ds.Tables[0].Rows.Count; _index++)
                    {
                        drProduct = aTable.NewRow();

                        lbl_date.Text = _Ds.Tables[0].Rows[0]["Servicebilldate"].ToString();
                        lbl_billno.Text = _Ds.Tables[0].Rows[0]["strServiceId"].ToString();
                        lblcustomer.Text = _Ds.Tables[0].Rows[0]["customerName"].ToString();
                        lblAdd1.Text = _Ds.Tables[0].Rows[0]["Billing_address1"].ToString();
                        lbl_add2.Text = _Ds.Tables[0].Rows[0]["Billing_address2"].ToString();
                        //lblAdd2.Text = _Ds.Tables[0].Rows[0]["Shipping_address1"].ToString();
                        //lbl_add3.Text = _Ds.Tables[0].Rows[0]["Shipping_address2"].ToString();
                        lblzip.Text = _Ds.Tables[0].Rows[0]["Billing_zipCode"].ToString();
                        //lbl_zip4.Text = _Ds.Tables[0].Rows[0]["Shipping_zipCode"].ToString();
                        //lbldesc.Text = _Ds.Tables[0].Rows[0]["serviceDesc"].ToString();
                        //lblprice.Text = _Ds.Tables[0].Rows[0]["price"].ToString();
                        //lblamount.Text = _Ds.Tables[0].Rows[0]["price"].ToString();

                        //lbltax.Text = _Ds.Tables[0].Rows[0]["serviceTax"].ToString() + "%";
                        double _serviceTax = 0.0;
                        if (_Ds.Tables[0].Rows[0]["serviceTax"].ToString() == "")
                            _serviceTax = 0.0;
                        else
                            _serviceTax = Convert.ToDouble(_Ds.Tables[0].Rows[0]["serviceTax"].ToString()) * .01;


                        double educationCess = 0.0;
                        double heducationCess = 0.0;
                        if (_Ds.Tables[0].Rows[0]["eduCess"].ToString() == "")
                            educationCess = 0.0;
                        else
                            educationCess = Convert.ToDouble(_Ds.Tables[0].Rows[0]["eduCess"].ToString()) * .01 *_serviceTax;

                        educationCess = _serviceTax * educationCess;

                        if (_Ds.Tables[0].Rows[0]["educationCess"].ToString() == "")
                            heducationCess = 0.0;
                        else
                            heducationCess = Convert.ToDouble(_Ds.Tables[0].Rows[0]["educationCess"].ToString()) * .01 *_serviceTax;


                        //double _serviceTax = double.Parse(_Ds.Tables[0].Rows[0]["serviceTax"].ToString()) * .01;
                        //double educationCess = double.Parse(_Ds.Tables[0].Rows[0]["eduCess"].ToString()) * .01 * _serviceTax;
                       // double heducationCess = double.Parse(_Ds.Tables[0].Rows[0]["educationCess"].ToString()) * .01 * _serviceTax;
                        double _total = Convert.ToDouble(_Ds.Tables[0].Rows[0]["price"].ToString()) + _serviceTax + educationCess + heducationCess;
                        string  _totalamt = _Ds.Tables[0].Rows[0]["price"].ToString() + _serviceTax + educationCess + heducationCess;
                        //lbltotal.Text = string.Format("{0:N}", _total);
                        //lbltaxamount.Text = (Convert.ToDecimal(lbltotal.Text) - Convert.ToDecimal(lblamount.Text)).ToString();
                        //lblTotalWord.Text = changeCurrencyToWords(Convert.ToDouble(lbltotal.Text));

                        lblTin.Text = _Ds.Tables[0].Rows[0]["TIN"].ToString();
                        lblServiceTaxRegnNo.Text = _Ds.Tables[0].Rows[0]["ServiceTaxRegnNo"].ToString();
                        lblCSTNo.Text = _Ds.Tables[0].Rows[0]["cstNo"].ToString();

                        CustOrderNo.Text = _Ds.Tables[0].Rows[0]["customerOrderNo"].ToString();
                        CustOrderDate.Text = _Ds.Tables[0].Rows[0]["pdate"].ToString();


                        drProduct["serviceDesc"] = _Ds.Tables[0].Rows[_index]["serviceDesc"].ToString();
                        drProduct["price"] = "(Rs) "+ _Ds.Tables[0].Rows[_index]["price"].ToString();
                        drProduct["Amount"] = _Ds.Tables[0].Rows[_index]["Amount"].ToString();


                        CalcTotalAmount(_Ds.Tables[0].Rows[0]["Amount"].ToString());
                        CalcTotalServiceTax(_Ds.Tables[0].Rows[0]["STax"].ToString());
                        CalcTotalEduCess(_Ds.Tables[0].Rows[0]["ECessAmt"].ToString());
                        CalcTotalHgEduCess(_Ds.Tables[0].Rows[0]["hCessAmt"].ToString());


                        _staxrate = (_Ds.Tables[0].Rows[0]["serviceTax"].ToString());
                        _ecessrate=(_Ds.Tables[0].Rows[0]["eduCess"].ToString());
                        _hcessrate=(_Ds.Tables[0].Rows[0]["educationCess"].ToString());
                        _vat = (_Ds.Tables[0].Rows[0]["vat"].ToString());
                        aTable.Rows.Add(drProduct);
                    }
                }
                //drProduct = aTable.NewRow();
                //drProduct["serviceDesc"] = "Total";

                //drProduct["Amount"] = string.Format("{0:#,##0.00}", (totalAmount));
                //aTable.Rows.Add(drProduct);

                if (_staxrate != "")
                {

                    drProduct = aTable.NewRow();
                    drProduct["serviceDesc"] = "Service Tax @" ;
                    drProduct["price"] = _staxrate +" %";
                    drProduct["Amount"] = string.Format("{0:#,##0.00}", (TotalServiceTax));
                    aTable.Rows.Add(drProduct);
                }
                if (_ecessrate != "")
                {
                    drProduct = aTable.NewRow();
                    drProduct["serviceDesc"] = "Education Cess @" ;
                    drProduct["price"] = _ecessrate +" %";
                    drProduct["Amount"] = string.Format("{0:#,##0.00}", (TotalEduCess));
                    aTable.Rows.Add(drProduct);
                }
                if (_hcessrate != "")
                {
                    drProduct = aTable.NewRow();
                    drProduct["serviceDesc"] = "Higher Education Cess @"  ;
                    drProduct["price"] = _hcessrate+ " %";
                    drProduct["Amount"] = string.Format("{0:#,##0.00}", (TotalHgEduCess));
                    aTable.Rows.Add(drProduct);
                }
                if (_vat != "" && _vat !="0.00")
                {
                    drProduct = aTable.NewRow();
                    drProduct["serviceDesc"] = "VAT  ";
                    drProduct["price"] = "(Rs)" + _vat;
                    drProduct["Amount"] = string.Format("{0:#,##0.00}", (_vat));
                    aTable.Rows.Add(drProduct);
                    totalAmount = totalAmount + Convert.ToDouble(_vat);
                }
                totalNetAmount = totalAmount + TotalServiceTax + TotalEduCess + TotalHgEduCess ;
                drProduct = aTable.NewRow();
                drProduct["price"] = "Total";
                drProduct["Amount"] = string.Format("{0:#,##0.00}", (totalNetAmount));
                aTable.Rows.Add(drProduct);


                t1 = Convert.ToDouble(totalNetAmount);
                t2 = Math.Round(totalNetAmount);

                if (t2 > t1)
                {
                    drProduct = aTable.NewRow();
                    drProduct["price"] = "Round Off ";
                    drProduct["Amount"] = "(+)" + string.Format("{0:#,##0.00}", (t2 - t1));
                    aTable.Rows.Add(drProduct);

                    drProduct = aTable.NewRow();
                    drProduct["price"] = "Total";
                    drProduct["Amount"] = string.Format("{0:#,##0.00}", (Math.Round(totalNetAmount)));
                    aTable.Rows.Add(drProduct);
                }
                if (t2 < t1)
                {
                    drProduct = aTable.NewRow();
                    drProduct["price"] = "Round Off ";
                    drProduct["Amount"] = "(-)" + string.Format("{0:#,##0.00}", (t1 - t2));
                    aTable.Rows.Add(drProduct);

                    drProduct = aTable.NewRow();
                    drProduct["price"] = "Total";
                    drProduct["Amount"] = string.Format("{0:#,##0.00}", (Math.Round(totalNetAmount)));
                    aTable.Rows.Add(drProduct);

                }


                poLineGrid.DataSource = aTable;
                poLineGrid.DataBind();

                lblNetAmtTextFormat.Text = changeCurrencyToWords(Math.Round(totalNetAmount));
            
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }
    private void CalcTotalServiceTax(string _ServiceTax)
    {
        double ta = 0;
        if (double.TryParse(_ServiceTax, out ta))
        {
            TotalServiceTax += Double.Parse(_ServiceTax);
        }

    }
    private void CalcTotalEduCess(string _EduCess)
    {
        double ta = 0;
        if (double.TryParse(_EduCess, out ta))
        {
            TotalEduCess += Double.Parse(_EduCess);
        }

    }
    private void CalcTotalHgEduCess(string _HgEduCess)
    {
        double ta = 0;
        if (double.TryParse(_HgEduCess, out ta))
        {
            TotalHgEduCess += Double.Parse(_HgEduCess);
        }

    }
    private void CalcTotalAmount(string _netVal)
    {
        double ta = 0;
        if (double.TryParse(_netVal, out ta))
        {
            totalAmount += Double.Parse(_netVal);
        }

    }
    protected void btnCancel_ServerClick(object sender, EventArgs e)
    {
        if (ViewState["previousPage"] != null)
        {
            Response.Redirect(Convert.ToString(ViewState["previousPage"]));
        }

    }
    public String changeNumericToWords(double numb)
    {

        String num = numb.ToString();

        return changeToWords(num, false);

    }

    public String changeCurrencyToWords(String numb)
    {

        return changeToWords(numb, true);

    }

    public String changeNumericToWords(String numb)
    {

        return changeToWords(numb, false);

    }

    public String changeCurrencyToWords(double numb)
    {

        return changeToWords(numb.ToString(), true);

    }

    private String changeToWords(String numb, bool isCurrency)
    {

        String val = "", wholeNo = numb, points = "", andStr = "", pointStr = "";

        String endStr = (isCurrency) ? ("Only") : ("");

        try
        {

            int decimalPlace = numb.IndexOf(".");

            if (decimalPlace > 0)
            {

                wholeNo = numb.Substring(0, decimalPlace);

                points = numb.Substring(decimalPlace + 1);

                if (Convert.ToInt32(points) > 0)
                {

                    andStr = (isCurrency) ? ("") : ("point");// just to separate whole numbers from points/cents

                    endStr = (isCurrency) ? ("Paisa " + endStr) : ("");
                    if (points.Length > 1)
                    {
                        pointStr = " and " + translateWholeNumber(points);
                    }
                    else
                    {
                        pointStr = " and " + translateWholeNumber(points + "0");
                    }

                }


            }
            else
            {
                andStr = (isCurrency) ? ("") : ("point");
            }

            val = String.Format("{0} {1}{2} {3}", "Rupees " + translateWholeNumber(wholeNo).Trim(), andStr, pointStr, endStr);

        }

        catch { ;}

        return val;

    }

    private String translateWholeNumber(String number)
    {

        string word = "";

        try
        {

            bool beginsZero = false;//tests for 0XX

            bool isDone = false;//test if already translated

            double dblAmt = (Convert.ToDouble(number));

            //if ((dblAmt > 0) && number.StartsWith("0"))

            if (dblAmt > 0)
            {//test for zero or digit zero in a nuemric

                beginsZero = number.StartsWith("0");

                int numDigits = number.Length;

                int pos = 0;//store digit grouping

                String place = "";//digit grouping name:hundres,thousand,etc...

                switch (numDigits)
                {

                    case 1://ones' range

                        word = ones(number);

                        isDone = true;

                        break;

                    case 2://tens' range

                        word = tens(number);

                        isDone = true;

                        break;

                    case 3://hundreds' range

                        pos = (numDigits % 3) + 1;

                        place = " Hundred ";

                        break;

                    case 4://thousands' range
                        pos = (numDigits % 4) + 1;

                        place = " Thousand ";

                        break;

                    case 5:
                        pos = (numDigits % 4) + 1;

                        place = " Thousand ";

                        break;

                    case 6:

                        pos = (numDigits % 6) + 1;

                        place = " Lakh ";

                        break;

                    case 7://millions' range

                        pos = (numDigits % 6) + 1;

                        place = " Lakh ";

                        break;
                    case 8:
                        pos = (numDigits % 8) + 1;

                        place = " Crore ";

                        break;

                    case 9:

                        pos = (numDigits % 8) + 1;

                        place = " Crore ";

                        break;


                    //case 10://Billions's range



                    //add extra case options for anything above Billion...

                    default:

                        isDone = true;

                        break;

                }

                if (!isDone)
                {//if transalation is not done, continue...(Recursion comes in now!!)

                    word = translateWholeNumber(number.Substring(0, pos)) + place + translateWholeNumber(number.Substring(pos));

                    //check for trailing zeros

                    if (beginsZero) word = " and " + word.Trim();

                }

                //ignore digit grouping names

                if (word.Trim().Equals(place.Trim())) word = "";

            }

        }

        catch { ;}

        return word.Trim();

    }

    private String tens(String digit)
    {

        int digt = Convert.ToInt32(digit);

        String name = null;

        switch (digt)
        {

            case 10:

                name = "Ten";

                break;

            case 11:

                name = "Eleven";

                break;

            case 12:

                name = "Twelve";

                break;

            case 13:

                name = "Thirteen";

                break;

            case 14:

                name = "Fourteen";

                break;

            case 15:

                name = "Fifteen";

                break;

            case 16:

                name = "Sixteen";

                break;

            case 17:

                name = "Seventeen";

                break;

            case 18:

                name = "Eighteen";

                break;

            case 19:

                name = "Nineteen";

                break;

            case 20:

                name = "Twenty";

                break;

            case 30:

                name = "Thirty";

                break;

            case 40:

                name = "Forty";

                break;

            case 50:

                name = "Fifty";

                break;

            case 60:

                name = "Sixty";

                break;

            case 70:

                name = "Seventy";

                break;

            case 80:

                name = "Eighty";

                break;

            case 90:

                name = "Ninety";

                break;

            default:

                if (digt > 0)
                {

                    name = tens(digit.Substring(0, 1) + "0") + " " + ones(digit.Substring(1));

                }

                break;

        }

        return name;

    }

    private String ones(String digit)
    {

        int digt = Convert.ToInt32(digit);

        String name = "";

        switch (digt)
        {

            case 1:

                name = "One";

                break;

            case 2:

                name = "Two";

                break;

            case 3:

                name = "Three";

                break;

            case 4:

                name = "Four";

                break;

            case 5:

                name = "Five";

                break;

            case 6:

                name = "Six";

                break;

            case 7:

                name = "Seven";

                break;

            case 8:

                name = "Eight";

                break;

            case 9:

                name = "Nine";

                break;

        }

        return name;

    }

    private String translateCents(String cents)
    {

        String cts = "", digit = "", engOne = "";

        for (int i = 0; i < cents.Length; i++)
        {

            digit = cents[i].ToString();

            if (digit.Equals("0"))
            {

                engOne = "Zero";

            }

            else
            {

                engOne = ones(digit);

            }

            cts += " " + engOne;

        }

        return cts;

    }


    protected void poLineGrid_ItemDataBound(object sender, DataGridItemEventArgs e)
    {

    }
}
