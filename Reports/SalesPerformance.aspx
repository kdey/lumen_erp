﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SalesPerformance.aspx.cs" Inherits="Reports_SalesPerformance" MasterPageFile="~/Reports/Reports.master" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
   
   
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  
  <div class="PageMargin">
<div class="PageHeading">Salesman Wise sales Report</div>
<div class="Line">
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
                </div>
<!--Content Table Start-->
<div class="PageContent">
   <table width="100%" border="0" cellspacing="5" cellpadding="0">
    <tr>
      <td align="left" valign="middle" >Primary Sales Person :</td>
      <td align="left" valign="middle">
                <asp:ListBox ID="ddlprimary" runat="server" Height="76px" Rows="1" 
                    SelectionMode="Multiple" ValidationGroup="search" CssClass="TextboxSmall"></asp:ListBox>
            </td>
      <td align="left" valign="middle"  >
                Select Date:From&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
      <td align="left" valign="middle" >
                <span >
                <asp:TextBox ID="txtDatefrom" runat="server" Width="150px" CssClass="TextboxSmall" onkeyup="document.getElementById(this.id).value=''" autocomplete='off'></asp:TextBox>
                        <cc1:calendarextender ID="CalendarExtender3" runat="server" 
                            TargetControlID="txtDatefrom" PopupButtonID="txtDatefrom">
                        </cc1:calendarextender>
                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" 
                            TargetControlID="txtDatefrom" PopupButtonID="txtDatefrom">
                          
                        </cc1:CalendarExtender>
                </span>
            </td>
      <td align="left" valign="middle" >Select Date :To&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
      <td  align="right" valign="middle"><asp:TextBox ID="txtDateTo" runat="server" Width="150px" CssClass="TextboxSmall" onkeyup="document.getElementById(this.id).value=''" autocomplete='off'></asp:TextBox>
               <cc1:CalendarExtender ID="CalendarExtender2" runat="server" 
                            TargetControlID="txtDateTo" PopupButtonID="txtDateTo">
                        </cc1:CalendarExtender>
                        
                          </td>
      <td  align="left" valign="middle">
                <asp:Button ID="btnSearch" runat="server" Text="Report" 
                    onclick="btnSearch_Click"  style="width: 61px" CssClass="Button"/>
               
                        <br />
        </td>
    </tr>
    <tr>
      <td align="left" valign="middle" >&nbsp;</td>
      <td  align="left" valign="middle">
          &nbsp;</td>
      <td align="left" valign="middle"  >
          &nbsp;</td>
      <td align="left" valign="middle" >
        
                            <asp:RequiredFieldValidator ControlToValidate="txtDatefrom" Display="Dynamic"  ErrorMessage="Start Date cannot be empty"
                            ID="RequiredFieldValidator2" runat="server" />
               
                </td>
      <td align="left" valign="middle">&nbsp;</td>
      <td  align="right" valign="middle">
                            <asp:RequiredFieldValidator ControlToValidate="txtDateTo" Display="Dynamic" ErrorMessage="End Date cannot be empty"
                            ID="RequiredFieldValidator1" runat="server" />
                </td>
      <td align="left" valign="middle">
                        

                <asp:Button ID="btnexpToExcel" runat="server" Text="Export to Excel" 
                    onclick="btnexptoexcel_Click" ValidationGroup="report" 
              style="width: 120px; height: 19px;" CssClass="Button" Enabled="False"/>
               </td>
    </tr>
    <tr>
      <td align="left" valign="middle" >&nbsp;</td>
      <td  align="left" valign="middle">
          &nbsp;</td>
      <td align="left" valign="middle"  >
          &nbsp;</td>
      <td align="left" valign="middle" >
        
                            &nbsp;</td>
      <td align="left" valign="middle">&nbsp;</td>
      <td  align="right" valign="middle" colspan="2">
                        
                          <asp:CompareValidator ID="CompareValidator1"  runat="server" 
                            ErrorMessage="End date should be greater than start date" 
                            ControlToCompare="txtDatefrom" ControlToValidate="txtDateTo"
                            Type="Date" Operator="GreaterThanEqual"></asp:CompareValidator>
                </td>
    </tr>
    </table>
</div>
<!--Content Table End-->
<div class="Line"></div>
<div class="PageContent">
<table width="100%" align="center" cellpadding="0" cellspacing="5">
  	<tr>
  	
    <asp:GridView ID="GVSalesPersonSearch" runat="server" 
                    AutoGenerateColumns="False" CellPadding="0" CssClass="ReportGridHeading"
                    BorderColor="#6699CC" BorderStyle="Solid" BorderWidth="0px"
                    Width="100%" ShowFooter="True">
                  
                    <RowStyle HorizontalAlign="Center" />
                  
         <Columns>
                   <asp:BoundField DataField="salespersonname" HeaderText="Sales Man" 
                            HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                            <HeaderStyle Font-Bold="True"></HeaderStyle>
                            <ItemStyle Width="10%"></ItemStyle>
                    </asp:BoundField>
                    
                    <asp:BoundField DataField="customerName" HeaderText="Customer" 
                            HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                            <HeaderStyle Font-Bold="True"></HeaderStyle>
                            <ItemStyle Width="10%"></ItemStyle>
                    </asp:BoundField>
                        
                    <asp:BoundField DataField="Date" HeaderText="Date" 
                            HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" 
                            ItemStyle-CssClass="ReportGridData" HtmlEncodeFormatString="False">
                            <HeaderStyle Font-Bold="True"></HeaderStyle>
                            <ItemStyle Width="10%"></ItemStyle>
                     </asp:BoundField>
                     
                     <asp:BoundField DataField="strChallanId" HeaderText="Invoice No." 
                            HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                            <HeaderStyle Font-Bold="True"></HeaderStyle>
                            <ItemStyle Width="10%"></ItemStyle>
                     </asp:BoundField>

                     <asp:BoundField DataField="itemCode" HeaderText="Item" 
                            HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" 
                            ItemStyle-CssClass="ReportGridData">
                            <HeaderStyle Font-Bold="True"></HeaderStyle>
                            <ItemStyle Width="10%"></ItemStyle>
                    </asp:BoundField>                     

                     <asp:BoundField DataField="Qty" HeaderText="Quantity" 
                            HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" 
                            ItemStyle-CssClass="ReportGridData">
                            <HeaderStyle Font-Bold="True"></HeaderStyle>
                            <ItemStyle Width="10%"></ItemStyle>
                    </asp:BoundField>
                            
                    <asp:BoundField DataField="totpricebeforevat" 
                            HeaderText="Amount Before VAT(Rs)" HeaderStyle-Font-Bold="true" 
                            ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                            <HeaderStyle Font-Bold="True"></HeaderStyle>
                            <ItemStyle Width="10%"></ItemStyle>
                   </asp:BoundField>

                   <asp:BoundField DataField="buyBackAmount" HeaderText="Buy Back" 
                            HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                            <HeaderStyle Font-Bold="True"></HeaderStyle>
                            <ItemStyle Width="10%"></ItemStyle>
                  </asp:BoundField>

                  <asp:BoundField DataField="Netamt" HeaderText="Net Amount (Rs)."
                            HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                            <HeaderStyle Font-Bold="True"></HeaderStyle>
                            <ItemStyle Width="10%"></ItemStyle>
                  </asp:BoundField>                  
           </Columns>
                   
                   
                   
<HeaderStyle CssClass="ReportGridHeading"></HeaderStyle>
                   
                   
                   
                </asp:GridView>
                
    </tr>
  	</table> 
</div>

<!--Page Body End-->
</div>
    
</asp:Content>
