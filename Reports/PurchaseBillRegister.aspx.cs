﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;
using System.Data.SqlClient;

public partial class Reports_PurchaseBillRegister : BasePage
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");

        }
        if (!IsPostBack)
        {

        }
    }
    public void bindGrid()
    {
        try
        {

            DateTime fromDate1 = DateTime.ParseExact(txtFromDate.Text, "dd/MM/yyyy", null);
            DateTime toDate1 = DateTime.ParseExact(txtToDate.Text, "dd/MM/yyyy", null);
            string fromDate = fromDate1.ToString("dd MMM yyyy");
            string toDate = toDate1.ToString("dd MMM yyyy");

            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                string Sqlquery = string.Format(@"
                    SELECT GrnDate,strGrnId,storename,vendorName,vendorInvoiceNo,invoiceDate AS vendorinvoiceDate

	                    ,divisionName, subdivisionName,itemCode ,REPLACE(SerialNo,';','   ') AS SerialNo, poqty,L5.quantity ,DBO.Lum_Erp_Udfqtycheck(L5.poId,grnId,L6.itemId) AS QTYDUE
                        ,L6.price, L6.taxpercentage,
	                    CAST((L6.price * L5.quantity * 0.01 * L6.taxpercentage) AS DECIMAL(18,2)) as vatAmount ,
	                    CAST(((L6.price * L5.quantity)+(L6.price * L5.quantity * 0.01 * L6.taxpercentage))AS DECIMAL(18,0)) AS totalwithtax,StrPoid,
	                    CONVERT(VARCHAR,poDate,106) AS poDate,contractType
                        

	                    FROM
	                    (SELECT L3.grnId,strGrnId,poId,vendorInvoiceNo,invoiceDate,storeId,isDeleted,GrnDate,
		                    L3.grnLineId,itemId,quantity,SerialNo
		                    FROM
		                    (SELECT L1.grnId,strGrnId,poId,vendorInvoiceNo,invoiceDate,storeId,isDeleted,GrnDate,
			                    l2.grnLineId,itemId,quantity
			                    FROM 
			                    (SELECT grnId,strGrnId,poId,vendorInvoiceNo,invoiceDate,storeId,isDeleted,DateCreated AS GrnDate
				                    FROM Lum_Erp_Grn WHERE isdeleted = 0) L1
				                    LEFT JOIN Lum_Erp_GrnDetails l2 
				                    ON L1.grnId = L2.grnId )L3


		                    LEFT JOIN 
		                    (SELECT grnId,grnLineId,DBO.fn_GetSerialNoList_ForGrn(grnId,grnLineId) SerialNo
				                    FROM Lum_Erp_GrndetailsSerialNumber  
				                    Group by grnId,grnLineId) L4
                    		
		                    ON	L4.GrnId = L3.grnId AND
		                    L4.grnLineId = l3.grnLineId 


                    )L5
                    LEFT JOIN
                    (SELECT poLineId, d.divisionName, sd.subdivisionName,i.itemCode, i.itemDescription,pd.itemId , 
	                    (pd.quantity-pd.quantityReceived) AS quantity ,pd.quantity as poqty,  u.uomName, pd.price, PD.taxpercentage,i.vat, 
	                    0 as vatAmount, 0 AS total,poid
	                    FROM Lum_Erp_PurchaseOrderDetails pd 
	                    INNER JOIN Lum_Erp_Item i ON pd.itemId = i.itemId
	                    INNER JOIN Lum_Erp_Subdivision sd ON sd.subdivisionId = i.subdivisionId
	                    INNER JOIN Lum_Erp_Division d ON d.divisionId = sd.divisionId
	                    INNER JOIN Lum_Erp_UOM u ON i.uomId = u.uomId )L6
                        
                    ON L5.poId = L6.POID
                    AND L5.ITEMID = L6.ITEMID

                    LEFT JOIN  Lum_Erp_PurchaseOrder L7
                    ON L5.poId = L7.POID

                    LEFT JOIN Lum_Erp_Storelocation  SL 
                    ON SL.storeId =L5.storeId

                    LEFT JOIN Lum_Erp_Vendor V
                    ON L7.vendorId = V.vendorId

                    LEFT JOIN Lum_Erp_RateContractType RCT
                    ON L7.rateContractTypeId = RCT.rateContractTypeId




	                WHERE GrnDate >= '{0}' AND GrnDate <= dateadd(d,1,'{1}') 
                ORDER BY GRNID", fromDate, toDate);
                DataSet _Ds = conn.GetDataSet(Sqlquery, "ReportBind");
                ReportGrid.DataSource = _Ds;
                if (_Ds.Tables[0].Rows.Count != 0)
                {
                    ReportGrid.DataBind();
                    btnexpToExcel.Enabled = true;
                }
                else
                    btnexpToExcel.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            if (Convert.ToString(ex.Message) == "String was not recognized as a valid DateTime.")
            {
                Page.RegisterClientScriptBlock("message", "<script>alert('Date was not in correct Format...');</script>");
            }
            else
            {
                lblMsg.Text = Convert.ToString(ex.Message);
            }

        }
    }

    protected void btnReport_Click(object sender, EventArgs e)
    {
        bindGrid();
    }


    private double CalcTotalAmount(string _netVal)
    {
        double retVal;
        if (double.TryParse(_netVal, out retVal))
        {
            retVal = Double.Parse(_netVal);

        }
        return retVal;
    }
    protected void btnexptoexcel_Click(object sender, EventArgs e)
    {

        GridViewExportUtil.Export("PurchaseBillRegister.xls", ReportGrid);




    }

    protected void ReportGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }
}

