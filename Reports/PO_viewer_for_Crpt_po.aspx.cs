﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;

public partial class Reports_PO_viewer_for_Crpt_po : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void CrystalReportViewer1_Init(object sender, EventArgs e)
    {

    }
    protected void CrystalReportViewer1_PreRender(object sender, EventArgs e)
    {
        // attached our report to viewer and set database login.
        ReportDocument report = new ReportDocument();
        report.Load(Server.MapPath("Crpt_PO.rpt"));
        //report.SetDatabaseLogon("username", "pwd", @"server", "database");
        CrystalReportViewer1.ReportSource = report;
    }
}
