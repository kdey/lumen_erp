﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;
using System.Data.SqlClient;

public partial class Reports_TrackItemBySerialNumber : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");

        }
        if (!IsPostBack)
        {

          
            lblMsg.Visible = false;
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        ResetControl();
        bind_ItemInfo();

        

     }
    public void bind_ItemInfo()
    {

        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            try
            {
                DataSet _Ds = conn.GetDataSet(@"select i.itemCode,i.itemDescription,CONVERT(VARCHAR,g.invoiceDate,106) AS invoiceDate,g.strGrnId,g.grnId,v.vendorName,ci.strChallanId,c.customerName ,S.SOIRID,gd.serialNo
                                    from Lum_erp_Item i LEFT JOIN 
                                    Lum_erp_grndetailsSerialNumber gd ON i.itemId=gd.itemId LEFT JOIN
                                    Lum_erp_grn g ON gd.grnId=g.grnId LEFT JOIN
                                    Lum_erp_PurchaseOrder po  ON g.poId=po.poId LEFT JOIN
                                    Lum_erp_Vendor v ON v.vendorId=po.vendorId  LEFT JOIN
                                    Lum_erp_ChallancumInvoice ci ON ci.challanId=gd.challanId  LEFT JOIN
                                    Lum_erp_SOIR s ON ci.soirId=s.soirId LEFT JOIN
                                    Lum_erp_Customer c ON c.customerId=s.customerId
                            WHERE  gd.serialNo='" + txtSerialNo.Text + "' ", "SearchResult1");

                txtDateOfReceipt.Text = _Ds.Tables[0].Rows[0]["invoiceDate"].ToString();
                txtGRNNo.Text = _Ds.Tables[0].Rows[0]["strGrnId"].ToString();
                txtInvoiceNo.Text = _Ds.Tables[0].Rows[0]["strChallanId"].ToString();
                txtIssuedto.Text = _Ds.Tables[0].Rows[0]["customerName"].ToString();
                txtVendor.Text = _Ds.Tables[0].Rows[0]["vendorName"].ToString();
                txtItemdesc.Text = _Ds.Tables[0].Rows[0]["itemDescription"].ToString();
                txtitemPartno.Text = _Ds.Tables[0].Rows[0]["itemCode"].ToString();
               
                lblMsg.Visible = false;
            }
            catch (Exception ex)
            {
                ResetControl();

               
                lblMsg.Text = "No Match Found...";
                lblMsg.Visible = true;
            }
        
        }
    }
    public void ResetControl()
    {

        txtDateOfReceipt.Text = "";
        txtGRNNo.Text = "";
        txtInvoiceNo.Text ="";
        txtIssuedto.Text = "";
        txtVendor.Text = "";
        txtItemdesc.Text = "";
        txtitemPartno.Text = "";
    }
}
