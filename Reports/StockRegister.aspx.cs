﻿using System.Data;
using System.Data.SqlClient;
using System;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;


public partial class Reports_StockRegister : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }

        if (!this.IsPostBack)
        {
             bindControl();
        }
 
    }
    protected void divisionDdl_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ddlsubDivision_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ddlitem_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void storeDdl_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void addItemLine_Click(object sender, EventArgs e)
    {
        // Get selected values
        string division = ddlDivision.SelectedItem.Text;
        string subdivision = ddlsubDivision.SelectedItem.Text;
        string item = ddlitem.SelectedItem.Text;

        // Output result string based on which values are specified
        if (string.IsNullOrEmpty(division))
        {
            Label1.Text = "Select a Division";
            return;
        }
        else if (string.IsNullOrEmpty(subdivision))
        {
            Label1.Text = "Select a subDivision";
            return;
        }
        else if (string.IsNullOrEmpty(item))
        {
            Label1.Text = "Please select item";
            return;
        }
        if (ddlstore.SelectedValue == "0")
        {
            Label1.Text = "Please select Store";
            return;
        }
        if (chkqty.Checked == false)
        {
            try
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    using (IDataReader reader = conn.ExecuteQueryProc("Lum_Erp_Stp_GetStockRegister",
                                    new SpParam("@divisionId", ddlDivision.SelectedValue, SqlDbType.Int),
                                    new SpParam("@subdivisionId", ddlsubDivision.SelectedValue, SqlDbType.Int),
                                    new SpParam("@itemId", ddlitem.SelectedValue, SqlDbType.Int),
                                    new SpParam("@storeId", ddlstore.SelectedValue, SqlDbType.Int),
                                    new SpParam("@BYQTY", 0, SqlDbType.TinyInt)))
                    {
                        GVstockRegister.Columns[1].Visible = true ;
                        GVstockRegister.DataSource = reader;
                        GVstockRegister.DataBind();
                    }
                }
            }
            catch (SqlException ex)
            {
                Label1.Text = ex.Message;


            }
        }
        else
        {
            try
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    using (IDataReader reader = conn.ExecuteQueryProc("Lum_Erp_Stp_GetStockRegister",
                                    new SpParam("@divisionId", ddlDivision.SelectedValue, SqlDbType.Int),
                                    new SpParam("@subdivisionId", ddlsubDivision.SelectedValue, SqlDbType.Int),
                                    new SpParam("@itemId", ddlitem.SelectedValue, SqlDbType.Int),
                                    new SpParam("@storeId", ddlstore.SelectedValue, SqlDbType.Int),
                                    new SpParam("@BYQTY", 1, SqlDbType.TinyInt)))
                    {
                        GVstockRegister.Columns[1].Visible = false;
                        //GVstockRegister.Columns[1].HeaderText = "No.Of.Qty";
                        GVstockRegister.DataSource = reader;
                        GVstockRegister.DataBind();
                    }
                }
            }
            catch (SqlException ex)
            {
                Label1.Text = ex.Message;


            }
        }
    
    }
    public void bindControl()
    {
        BindListControl _control = new BindListControl();
        _control.BindStores(ddlstore);
       // _control.BindDivision( ddlDivision);
        

    }
    private void LoadSubDivisions(IConnection conn)
    {
        if (ddlDivision.SelectedItem != null)
        {
            using (IDataReader reader = conn.ExecuteQuery("select subDivisionId, subDivisionName from Lum_Erp_SubDivision where divisionId = " + ddlDivision.SelectedValue + " Order By subDivisionName "))
            {
                ddlsubDivision.DataSource = reader;
                ddlsubDivision.DataValueField = "subDivisionId";
                ddlsubDivision.DataTextField = "subDivisionName";
                ddlsubDivision.DataBind();
            }
            if (ddlsubDivision.Items.Count > 0)
            {
                ddlsubDivision.SelectedIndex = 0;
            }
        }
    }
    private void LoadItems(IConnection conn)
    {
        if (ddlsubDivision.SelectedItem != null)
        {
            using (IDataReader reader = conn.ExecuteQuery("select itemId, itemCode from Lum_Erp_Item where subDivisionId = " + ddlsubDivision.SelectedValue + " Order By itemCode"))
            {
                ddlitem.DataSource = reader;
                ddlitem.DataTextField = "itemCode";
                ddlitem.DataValueField = "itemId";
                ddlitem.DataBind();
            }
            if (ddlitem.Items.Count > 0)
            {
                ddlitem.SelectedIndex = 0;
            }
            
        }
    }
    protected void subDivisionDdl_SelectedIndexChanged(object sender, EventArgs e)
    {
        //using (IConnection conn = DataConnectionFactory.GetConnection())
        //{
        //    LoadItems(conn);
        //}
    }
}
