﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;
using System.Data.SqlClient;
public partial class Reports_ContractBillRegister : BasePage
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");

        }
        if (!IsPostBack)
        {

        }
    }
    public void bindGrid()
    {
        try
        {
            DateTime fromDate1 = DateTime.ParseExact(txtFromDate.Text, "dd/MM/yyyy", null);
            DateTime toDate1 = DateTime.ParseExact(txtToDate.Text, "dd/MM/yyyy", null);
            string fromDate = fromDate1.ToString("dd MMM yyyy");
            string toDate = toDate1.ToString("dd MMM yyyy");


            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                string Sqlquery = string.Format(@"SELECT ServiceBillNo,CONVERT(VARCHAR,ServiceBillDate,106) AS  ServiceBillDate,customerName,segmentType,CAddress,SRNo,itemCode,GrossCopies,ServiceCopies,NetBillableCopies,
	                MinBillingCharges,NetBillableCharges,BillableAmount,CAST(VAT AS DECIMAL(18,2)) VAT,CAST(ServiceTax AS DECIMAL(18,2)) ServiceTax,
	                CAST((ServiceTax*.03) AS DECIMAL(18,2)) AS EducationalCess,
	                CAST((BillableAmount + Vat + ServiceTax + (ServiceTax*.03)) AS DECIMAL(18,2)) AS InvoiceAmount,
	                NetPayable,id,PrevMeterReading,custMacId,currMeterReading,currMeterDate,
	                freeCopy,copyRate,minCopy,isBlocked,itemId
	                FROM (

		                SELECT ServiceBillNo,ServiceBillDate,customerName,segmentType,CAddress,SRNo,itemCode,GrossCopies,ServiceCopies,
			                NetBillableCopies,MinBillingCharges,NetBillableCharges,
			                CASE WHEN NetBillableCharges > MinBillingCharges THEN NetBillableCharges ELSE MinBillingCharges END  AS BillableAmount,
                            CASE WHEN ServiceBillDate >= '1 apr 2013' THEN (((CASE WHEN NetBillableCharges > MinBillingCharges THEN NetBillableCharges ELSE MinBillingCharges END) * .8) * .05)
			                ELSE (((CASE WHEN NetBillableCharges > MinBillingCharges THEN NetBillableCharges ELSE MinBillingCharges END) * .8) * .04)End AS VAT,
			                ((CASE WHEN NetBillableCharges > MinBillingCharges THEN NetBillableCharges ELSE MinBillingCharges END)  * .1) AS ServiceTax,

			                id,PrevMeterReading,custMacId,currMeterReading,currMeterDate,
			                freeCopy,copyRate,minCopy,isBlocked,NetPayable,itemId
			                FROM (
				                SELECT ServiceBillNo,ServiceBillDate,customerName,segmentType,CAddress,SRNo,itemCode,
					                GrossCopies,ServiceCopies,NetBillableCopies,MinBillingCharges,
					                CASE WHEN NetBillableCopies > minCopy THEN NetBillableCopies * copyRate ELSE minCopy * copyRate END AS NetBillableCharges,
					                id,PrevMeterReading,
					                custMacId,currMeterReading,currMeterDate,freeCopy,copyRate,minCopy,isBlocked,NetPayable,itemId
					                FROM
						                (
						                SELECT ServiceBillNo,ServiceBillDate,C.customerName ,segmentType,
						                (Billing_address1+ ' '+ Billing_address2 +' '+ Billing_zipCode) AS CAddress,SRNo,itemCode ,
						                (currMeterReading-dbo.Lum_Erp_Udf_GetCSABilling_Prev(id))- freeCopy AS GrossCopies, 
						                CAST((((currMeterReading-dbo.Lum_Erp_Udf_GetCSABilling_Prev(id))- freeCopy) *.01) AS DECIMAL(18,0)) AS ServiceCopies,
						                CAST((((currMeterReading-dbo.Lum_Erp_Udf_GetCSABilling_Prev(id))- freeCopy ) -(((currMeterReading-dbo.Lum_Erp_Udf_GetCSABilling_Prev(id)- freeCopy)) *.01) ) AS DECIMAL(18,0)) AS NetBillableCopies,
						                (CSA.copyRate * CSA.minCopy) as MinBillingCharges,
						                id,dbo.Lum_Erp_Udf_GetCSABilling_Prev(id) AS PrevMeterReading,
						                CSA.custMacId,currMeterReading,currMeterDate,freeCopy,copyRate,minCopy,isBlocked,NetPayable,CM.itemId
                							

							                 FROM Lum_Erp_CSABill CSA , Lum_Erp_CustomerMachine CM,Lum_Erp_Customer C,Lum_Erp_CustomerSegment cg,
								                Lum_Erp_Item I 
							                 WHERE CSA.IsBlocked = 0 AND
								                CSA.custMacId=CM.custMacId AND
								                C.customerId=CM.customerId AND C.customerSegmentId = cg.segmentId AND 
								                I.itemId=CM.itemId)L1
								                )L2)L3

                WHERE  ServiceBillDate >='{0}' AND ServiceBillDate<=dateadd(d,1,'{1}') 
                ORDER BY ServiceBillNo", fromDate, toDate);
                DataSet _Ds = conn.GetDataSet(Sqlquery, "ReportBind");
                ReportGrid.DataSource = _Ds;
                ReportGrid.DataBind();
                if (_Ds.Tables[0].Rows.Count != 0)
                {
                    
                    btnexpToExcel.Enabled = true;
                }
                else
                    btnexpToExcel.Enabled = false;

            }
        }
        catch (Exception ex)
        {
            if (Convert.ToString(ex.Message) == "String was not recognized as a valid DateTime.")
            {
                Page.RegisterClientScriptBlock("message", "<script>alert('Date was not in correct Format...');</script>");
                //lblMsg.Text = "Date was not in correct Format";
            }
            else
            {
                lblMsg.Text = Convert.ToString(ex.Message);
            }

        }
    }

    protected void btnReport_Click(object sender, EventArgs e)
    {
        bindGrid();
    }


    private double CalcTotalAmount(string _netVal)
    {
        double retVal;
        if (double.TryParse(_netVal, out retVal))
        {
            retVal = Double.Parse(_netVal);

        }
        return retVal;
    }
    protected void btnexptoexcel_Click(object sender, EventArgs e)
    {

        GridViewExportUtil.Export("ContractBillRegister.xls", ReportGrid);




    }

    protected void ReportGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }
}

