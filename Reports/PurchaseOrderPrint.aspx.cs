﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Drawing;

using Bitscrape.AppBlock.Database;

public partial class EventEntry_PurchaseOrderPrint :BasePage
{
    protected int _poId = -1;
    protected int _poStateId = -1;
    private double totalAmount = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!string.IsNullOrEmpty(Request["PurchaseOrderId"]))
        {
            _poId = int.Parse(Request["PurchaseOrderId"]);
        }
         if (!this.IsPostBack)
         {
             if (_poId > 0)
             {
                 LoadPo();
             }
         }
    }
    private void LoadPo()
    {
          int poTypeId = -1;
                    int vendorId =-1;
                    int rateContractTypeId = -1;
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select * from PurchaseOrder where poId = " + _poId))
            {
                if (reader.Read())
                {
                    poNumber.Text = reader["strPoId"].ToString();
                    _poStateId = Convert.ToInt32(reader["poStateId"]);
                    poTypeId = Convert.ToInt32(reader["poTypeId"].ToString());
                     vendorId = Convert.ToInt32(reader["vendorId"].ToString());
                    rateContractTypeId = Convert.ToInt32(reader["rateContractTypeId"].ToString());
                    //if purchase order Is locekd
                    //if (_poStateId == 2)
                    //{
                    //    tblItem.Visible = false;
                    //}
                    // now set the poType in the drop down
                     poDate.Text = ((DateTime)reader["poDate"]).ToString("dd/MM/yyyy");
                }
                reader.Close();
            }
            // load the purchase order type
                    using (IDataReader reader1 = conn.ExecuteQuery("select poTypeId,poTypeCode+'('+poTypeName+')' as poTypeCode from PurchaseOrderType where poTypeId='" + poTypeId + "'"))
            {
                        if(reader1.Read())
                        {
                    string poTypeCode = reader1["poTypeCode"].ToString();
                    poTypeDdl.Text = poTypeCode.ToString();
                        }
                        reader1.Close();
            }    
                    // now set the vendor company in the drop down.
                    using (IDataReader reader2 = conn.ExecuteQuery("select vendorId, vendorName from Vendor where vendorId='" + vendorId + "'"))
                    {
                        if(reader2.Read())
                        {
                        string vendorName = reader2["vendorName"].ToString();
                        vendorDdl.Text = vendorName.ToString();
                        }
                        reader2.Close();
                    }
                   
                    // get teh rate contract type
                    using (IDataReader reader3 = conn.ExecuteQuery("select rateContractTypeId, contractType FROM RateContractType where rateContractTypeId='" + rateContractTypeId + "'"))
                    {
                        if(reader3.Read())
                        {
                        rateContractDdl.Text = reader3["contractType"].ToString();
                        }
                        reader3.Close();
                    }
                    //vendorDdl.Enabled = false;
                    //poTypeDdl.Enabled = false;
                    //rateContractDdl.Enabled = false;
               
            
            BindGrid(conn);
            lblTotal.Text = string.Format("{0:N}", totalAmount);
            lblPrice.Text = changeCurrencyToWords(totalAmount);
            lblquantity.Text = GetTotalQuantity();
            
        }
    }
    private void BindGrid(IConnection conn)
    {
        string sql = @"select poLineId, d.divisionName, sd.subdivisionName, i.itemDescription, pd.quantity,  u.uomName, pd.price, i.vat, 
                        pd.price * pd.quantity * 0.01 * i.vat 'vatAmount' ,(pd.price * pd.quantity+pd.price * pd.quantity * 0.01 * i.vat) AS total
                        from PurchaseOrderDetails pd 
                        inner join Item i on pd.itemId = i.itemId
                        inner join Subdivision sd on sd.subdivisionId = i.subdivisionId
                        inner join Division d on d.divisionId = sd.divisionId
                        inner join dbo.UOM u on i.uomId = u.uomId
                        where poId = " + _poId;
        using (IDataReader reader = conn.ExecuteQuery(sql))
        {
            poLineGrid.DataSource = reader;
            poLineGrid.DataBind();
        }
    }
   
    public String changeNumericToWords(double numb)
    {

        String num = numb.ToString();

        return changeToWords(num, false);

    }

    public String changeCurrencyToWords(String numb)
    {

        return changeToWords(numb, true);

    }

    public String changeNumericToWords(String numb)
    {

        return changeToWords(numb, false);

    }

    public String changeCurrencyToWords(double numb)
    {

        return changeToWords(numb.ToString(), true);

    }

    private String changeToWords(String numb, bool isCurrency)
    {

        String val = "", wholeNo = numb, points = "", andStr = "", pointStr = "";

        String endStr = (isCurrency) ? ("Only") : ("");

        try
        {

            int decimalPlace = numb.IndexOf(".");

            if (decimalPlace > 0)
            {

                wholeNo = numb.Substring(0, decimalPlace);

                points = numb.Substring(decimalPlace + 1);

                if (Convert.ToInt32(points) > 0)
                {

                    andStr = (isCurrency) ? ("") : ("point");// just to separate whole numbers from points/cents

                    endStr = (isCurrency) ? ("Paisa " + endStr) : ("");
                    if (points.Length > 1)
                    {
                        pointStr = " and " + translateWholeNumber(points);
                    }
                    else
                    {
                        pointStr = " and " + translateWholeNumber(points + "0");
                    }

                }


            }
            else
            {
                andStr = (isCurrency) ? ("") : ("point");
            }

            val = String.Format("{0} {1}{2} {3}", "Rupees " + translateWholeNumber(wholeNo).Trim(), andStr, pointStr, endStr);

        }

        catch { ;}

        return val;

    }

    private String translateWholeNumber(String number)
    {

        string word = "";

        try
        {

            bool beginsZero = false;//tests for 0XX

            bool isDone = false;//test if already translated

            double dblAmt = (Convert.ToDouble(number));

            //if ((dblAmt > 0) && number.StartsWith("0"))

            if (dblAmt > 0)
            {//test for zero or digit zero in a nuemric

                beginsZero = number.StartsWith("0");

                int numDigits = number.Length;

                int pos = 0;//store digit grouping

                String place = "";//digit grouping name:hundres,thousand,etc...

                switch (numDigits)
                {

                    case 1://ones' range

                        word = ones(number);

                        isDone = true;

                        break;

                    case 2://tens' range

                        word = tens(number);

                        isDone = true;

                        break;

                    case 3://hundreds' range

                        pos = (numDigits % 3) + 1;

                        place = " Hundred ";

                        break;

                    case 4://thousands' range
                        pos = (numDigits % 4) + 1;

                        place = " Thousand ";

                        break;

                    case 5:
                        pos = (numDigits % 4) + 1;

                        place = " Thousand ";

                        break;

                    case 6:

                        pos = (numDigits % 6) + 1;

                        place = " Lakh ";

                        break;

                    case 7://millions' range

                        pos = (numDigits % 6) + 1;

                        place = " Lakh ";

                        break;
                    case 8:
                        pos = (numDigits % 8) + 1;

                        place = " Crore ";

                        break;

                    case 9:

                        pos = (numDigits % 8) + 1;

                        place = " Crore ";

                        break;


                    //case 10://Billions's range



                    //add extra case options for anything above Billion...

                    default:

                        isDone = true;

                        break;

                }

                if (!isDone)
                {//if transalation is not done, continue...(Recursion comes in now!!)

                    word = translateWholeNumber(number.Substring(0, pos)) + place + translateWholeNumber(number.Substring(pos));

                    //check for trailing zeros

                    if (beginsZero) word = " and " + word.Trim();

                }

                //ignore digit grouping names

                if (word.Trim().Equals(place.Trim())) word = "";

            }

        }

        catch { ;}

        return word.Trim();

    }

    private String tens(String digit)
    {

        int digt = Convert.ToInt32(digit);

        String name = null;

        switch (digt)
        {

            case 10:

                name = "Ten";

                break;

            case 11:

                name = "Eleven";

                break;

            case 12:

                name = "Twelve";

                break;

            case 13:

                name = "Thirteen";

                break;

            case 14:

                name = "Fourteen";

                break;

            case 15:

                name = "Fifteen";

                break;

            case 16:

                name = "Sixteen";

                break;

            case 17:

                name = "Seventeen";

                break;

            case 18:

                name = "Eighteen";

                break;

            case 19:

                name = "Nineteen";

                break;

            case 20:

                name = "Twenty";

                break;

            case 30:

                name = "Thirty";

                break;

            case 40:

                name = "Fourty";

                break;

            case 50:

                name = "Fifty";

                break;

            case 60:

                name = "Sixty";

                break;

            case 70:

                name = "Seventy";

                break;

            case 80:

                name = "Eighty";

                break;

            case 90:

                name = "Ninety";

                break;

            default:

                if (digt > 0)
                {

                    name = tens(digit.Substring(0, 1) + "0") + " " + ones(digit.Substring(1));

                }

                break;

        }

        return name;

    }

    private String ones(String digit)
    {

        int digt = Convert.ToInt32(digit);

        String name = "";

        switch (digt)
        {

            case 1:

                name = "One";

                break;

            case 2:

                name = "Two";

                break;

            case 3:

                name = "Three";

                break;

            case 4:

                name = "Four";

                break;

            case 5:

                name = "Five";

                break;

            case 6:

                name = "Six";

                break;

            case 7:

                name = "Seven";

                break;

            case 8:

                name = "Eight";

                break;

            case 9:

                name = "Nine";

                break;

        }

        return name;

    }

    private String translateCents(String cents)
    {

        String cts = "", digit = "", engOne = "";

        for (int i = 0; i < cents.Length; i++)
        {

            digit = cents[i].ToString();

            if (digit.Equals("0"))
            {

                engOne = "Zero";

            }

            else
            {

                engOne = ones(digit);

            }

            cts += " " + engOne;

        }

        return cts;

    }
    public string GetTotalQuantity()
    {
        string retVal = "";
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(@"select Sum(quantity) as TotalQty from PurchaseOrderDetails WHERE  poId=" + _poId))
            {
                if (reader.Read())
                {


                    retVal = Convert.ToString(reader["TotalQty"]);
                }
            }
        }
        return retVal;
    }
    private void CalcTotalAmount(string _netVal)
    {
        if (_netVal != "")
        {
            totalAmount += Double.Parse(_netVal);
        }

    }
    protected void poLineGrid_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CalcTotalAmount(e.Item.Cells[7].Text);

        }
    }
}
