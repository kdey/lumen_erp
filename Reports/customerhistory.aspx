﻿<%@ Page Title="" Language="C#" Culture="en-GB" MasterPageFile="~/Reports/Reports.master"
    AutoEventWireup="true" CodeFile="customerhistory.aspx.cs" Inherits="Reports_customerhistory" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <!--Page Body Start-->
    <div class="PageMargin">
        <div class="PageHeading">
            Customer History&nbsp; Register
        </div>
        <div class="Line">
        </div>
        <!--Content Table Start-->
        <div class="PageContent">
            <table width="40%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
                <tr>
                    <td align="left" valign="middle" colspan="4">
                        Select Date:
                    </td>
                </tr>
                <tr>
                    <td width="15%" align="left" valign="middle">
                        From&nbsp;&nbsp;<span style="color: Red; font-weight: bold">*</span>
                    </td>
                    <td width="35%" align="left" valign="middle">
                        <asp:TextBox ID="txtFromDate" runat="server" onkeyup="document.getElementById(this.id).value=''"
                            autocomplete='off' CssClass="TextboxSmall" />
                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="txtFromDate">
                        </cc1:CalendarExtender>
                    </td>
                    <td width="15%" align="left" valign="middle">
                        To&nbsp;&nbsp;<span style="color: Red; font-weight: bold">*</span>
                    </td>
                    <td width="35%" align="left" valign="middle">
                        <asp:TextBox ID="txtToDate" runat="server" onkeyup="document.getElementById(this.id).value=''"
                            autocomplete='off' CssClass="TextboxSmall"></asp:TextBox>
                        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" TargetControlID="txtToDate">
                        </cc1:CalendarExtender>
                    </td>
                </tr>
                <tr>
                    <td width="15%" align="left" valign="middle">
                        &nbsp;
                    </td>
                    <td width="35%" align="left" valign="middle">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" EnableClientScript="true"
                            ControlToValidate="txtFromDate" ErrorMessage="From Date can't be blank" Text="From Date can't be blank"></asp:RequiredFieldValidator>
                    </td>
                    <td width="15%" align="left" valign="middle">
                        &nbsp;
                    </td>
                    <td width="35%" align="left" valign="middle">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" EnableClientScript="true"
                            ControlToValidate="txtToDate" ErrorMessage="To Date can't be blank" Text="To Date can't be blank"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td width="15%" align="left" valign="middle">
                        &nbsp;
                    </td>
                    <td align="left" valign="middle" colspan="3" style="width: 70%">
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="End date should be greater than start date"
                            ControlToCompare="txtFromDate" ControlToValidate="txtToDate" Type="Date" Operator="GreaterThanEqual"></asp:CompareValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="middle" colspan="2">
                        <asp:Button ID="btnReport" runat="server" OnClick="btnReport_Click" Text="Report"
                            CssClass="Button" />
                    </td>
                    <td align="left" valign="middle" colspan="2">
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="Button" OnClick="btnCancel_Click" />
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="middle" colspan="4">
                        <asp:Button ID="btnexpToExcel" runat="server" Text="Export to Excel" OnClick="btnexptoexcel_Click"
                            ValidationGroup="report" Style="width: 120px; height: 19px;" CssClass="Button"
                            Enabled="False" />
                    </td>
                </tr>
            </table>
        </div>
        <!--Content Table End-->
        <div class="Line">
        </div>
        <!--Grid Table Start-->
            <asp:GridView ID="ReportGrid" runat="server" AutoGenerateColumns="False" CellPadding="0"
                HeaderStyle-CssClass="ReportGridHeading" BorderColor="#6699CC" BorderStyle="Solid"
                BorderWidth="0px" Width="100%" ShowFooter="True" CssClass="ReportGridHeading"
                onitemdatabound="ReportGrid_ItemDataBound" 
            onrowcreated="ReportGrid_RowCreated">
                <rowstyle cssclass="ReportGridData" width="10%"></rowstyle>
                
                <columns>
                <asp:BoundField DataField="customerName" HeaderText="Customer Name" HeaderStyle-Font-Bold="true"
                    ItemStyle-Width="10%"  ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="COdate" HeaderText="Date" HeaderStyle-Font-Bold="true"
                    ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="strServiceId" HeaderText="Ref. No." HeaderStyle-Font-Bold="true"
                    ItemStyle-Width="10%"  ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>                              
                <asp:BoundField DataField="TAmt" HeaderText="Net Amount (Rs)" HeaderStyle-Font-Bold="true"
                    ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="PaymentAmt" HeaderText="Received Amount (Rs)" HeaderStyle-Font-Bold="true"
                    ItemStyle-Width="10%"  ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="TdsAmount" HeaderText="TDS Deduction" HeaderStyle-Font-Bold="true"
                    ItemStyle-Width="10%"  ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="OthDednAmount" HeaderText="Other Deduction" HeaderStyle-Font-Bold="true"
                    ItemStyle-Width="10%"  ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="OthDednReason" HeaderText="Other Deduction Reason" HeaderStyle-Font-Bold="true"
                    ItemStyle-Width="10%"  ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="PaymentDate" HeaderText="Payment Date" HeaderStyle-Font-Bold="true"
                    ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="paymentmode" HeaderText="Payment Mode" HeaderStyle-Font-Bold="true"
                    ItemStyle-Width="10%"  ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="BankName" HeaderText="Bank Name" HeaderStyle-Font-Bold="true"
                    ItemStyle-Width="10%"  ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="BranchName" HeaderText="Bank Branch" HeaderStyle-Font-Bold="true"
                    ItemStyle-Width="10%"  ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="ChqNo" HeaderText="Chq. No" HeaderStyle-Font-Bold="true"
                    ItemStyle-Width="10%"  ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="Chqdate" HeaderText="Chq. Date" HeaderStyle-Font-Bold="true"
                    ItemStyle-Width="10%"  ItemStyle-CssClass="ReportGridData">
                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                    <ItemStyle Width="10%"></ItemStyle>
                </asp:BoundField>
            </columns>
            </asp:GridView>
            <!--Grid Table End-->
            <!--Page Body End-->
    </div>
    <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
</asp:Content>
