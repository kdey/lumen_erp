<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/Reports.master" AutoEventWireup="true" CodeFile="TrackItemBySerialNumber.aspx.cs" Inherits="Reports_TrackItemBySerialNumber" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <div class="PageMargin"></div>
<div class="PageHeading">Serial No. Wise Item Report</div>
<div class="Line"></div>
<!--Content Table Start-->
<div class="PageContent">
   <table width="100%" border="0" cellspacing="5" cellpadding="0">
    <tr>
      <td align="left" valign="middle">
                Enter Serial Number :&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
      <td  align="left" valign="middle">
                <asp:TextBox ID="txtSerialNo" runat="server" 
                     ValidationGroup="report" CssClass="TextboxSmall"></asp:TextBox>
            </td>
      <td align="left" valign="middle" >
          <asp:Button ID="btnSearch" runat="server" Text="Report" 
                    onclick="btnSearch_Click" ValidationGroup="report" style="width: 61px; height: 19px;" 
                    CssClass="Button"/>
               
                </td>
      <td align="left" valign="middle" >
                &nbsp;</td><td align="left" valign="middle">&nbsp;</td><td  align="right" valign="middle">&nbsp;</td><td align="left" valign="middle">
                        <br />
        </td>
      <td  align="center" valign="middle">
               &nbsp;&nbsp;</td></tr><tr>
      <td align="left" valign="middle">
                            &nbsp;</td><td  align="left" valign="middle">
                <asp:RequiredFieldValidator ID="rfvserialno" runat="server" 
                    ControlToValidate="txtSerialNo" ErrorMessage="Please Enter Serial Number" 
                    SetFocusOnError="True" ValidationGroup="report"></asp:RequiredFieldValidator></td><td align="left" valign="middle" >
  <asp:Label ID="lblMsg" runat="server" ForeColor="Red" Visible="False"></asp:Label></td><td align="left" valign="middle" >
                &nbsp;</td><td  align="left" valign="middle">&nbsp;</td><td  align="right" valign="middle">&nbsp;</td><td align="left" valign="middle">
                        &nbsp;</td><td  align="center" valign="middle">
               &nbsp;</td></tr><tr>
      <td align="left" valign="middle">
                                            Item Part No #</td><td  align="left" valign="middle">
                                            <asp:TextBox ID="txtitemPartno" runat="server" CssClass="TextboxSmall" 
                                                ReadOnly="True"></asp:TextBox></td><td align="left" valign="middle" >
          Item Description:</td><td align="left" valign="middle" >
                                            <asp:TextBox ID="txtItemdesc" runat="server" ReadOnly="True" 
                                                CssClass="TextboxSmall"></asp:TextBox></td><td  align="left" valign="middle">Vendor:</td><td  align="right" valign="middle">
                                            <asp:TextBox ID="txtVendor" runat="server" ReadOnly="True" 
                                                CssClass="TextboxSmall"></asp:TextBox></td><td align="left" valign="middle">
                        &nbsp;</td><td  align="center" valign="middle">
               &nbsp;</td></tr><tr>
      <td align="left" valign="middle">
                                            Date Of Receipt:</td><td  align="left" valign="middle">
                                            <asp:TextBox ID="txtDateOfReceipt" runat="server" ReadOnly="True" 
                                                 CssClass="TextboxSmall"></asp:TextBox></td><td align="left" valign="middle" >
                                            GRN No #</td><td align="left" valign="middle" >
                                            <asp:TextBox ID="txtGRNNo" runat="server" ReadOnly="True" 
                                                CssClass="TextboxSmall"></asp:TextBox></td><td  align="left" valign="middle">Issued to:</td><td  align="right" valign="middle">
                                            <asp:TextBox ID="txtIssuedto" runat="server" CssClass="TextboxSmall" 
                                                ReadOnly="True"></asp:TextBox></td><td align="left" valign="middle">
                        &nbsp;</td><td  align="center" valign="middle">
               &nbsp;</td></tr><tr>
      <td align="left" valign="middle">
                                            Invoice No #</td><td  align="left" valign="middle">
                                            <asp:TextBox ID="txtInvoiceNo" runat="server" 
                                                ReadOnly="True"  CssClass="TextboxSmall"></asp:TextBox></td><td align="left" valign="middle" >
          &nbsp;</td><td align="left" valign="middle" >
                &nbsp;</td><td  align="left" valign="middle">&nbsp;</td><td  align="right" valign="middle">&nbsp;</td><td align="left" valign="middle">
                        &nbsp;</td><td  align="center" valign="middle">
               &nbsp;</td></tr><tr>
      <td align="center" valign="middle" colspan="8" >
                         
                        </td>
    </tr>
    </table>
</div>


  
    </asp:Content>

