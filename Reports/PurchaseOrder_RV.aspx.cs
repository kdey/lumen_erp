﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.Reporting.WebForms;
using Bitscrape.AppBlock.Database;


public partial class Reports_purchase_ReportViewer : BasePage
{
    protected int _poId = -1;
    protected int _poStateId = -1;
    private double totalAmount = 0;
    private double totalVATAmount = 0;
    private double GrandtotalAmount = 0;
    private double totalwithtax = 0;
    bool isdeleted = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        _poId = 50;

        if (!this.IsPostBack)
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                SpParam _isdeleted1 = new SpParam("@isdeleted", 0, SqlDbType.Int);
                SpParam _poId1 = new SpParam("@_poId", _poId, SqlDbType.Int);
                DataSet _Ds = new DataSet();
                _Ds = conn.GetDataSet("Lum_Erp_Stp_GetPOforPrint", _isdeleted1, _poId1);

                ReportDataSource datasource = new
                        ReportDataSource("DataSet_purchase_Lum_Erp_Stp_GetPOforPrint", _Ds.Tables[0]);

                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(datasource);
                if (_Ds.Tables[0].Rows.Count == 0)
                {
                    //lblMessage.Text = "Sorry, no products under this category!";
                }
//                DataSet _Ds1 = conn.GetDataSet(string.Format(@"select TIN,ServiceTaxRegnNo,* FROM Lum_Erp_CompanyMaster where 
//                                                                fromdate <=DATEADD(MI,330,GETUTCDATE() ) and 
//                                                                isnull(todate,DATEADD(MI,330,GETUTCDATE() )) >=
//                                                                    DATEADD(MI,330,GETUTCDATE() )"), "CM");

//                ReportDataSource datasource1 = new
//                        ReportDataSource("DataSet1_Lum_Erp_CompanyMaster", _Ds1.Tables[0]);

//                ReportViewer1.LocalReport.DataSources.Clear();
//                ReportViewer1.LocalReport.DataSources.Add(datasource);
//                ReportViewer1.LocalReport.DataSources.Add(datasource1);
            }

            ReportViewer1.LocalReport.Refresh();

        }

        
    }
}
