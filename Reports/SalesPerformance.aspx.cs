﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;
using System.Data.SqlClient;

public partial class Reports_SalesPerformance : BasePage
{
    decimal GrandTotal = 0;
    decimal GrandTotal_WOTAX = 0;//Added
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!IsPostBack)
        {
            bindControl();
        }
    }
    public void bindControl()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DataSet _Ds = conn.GetDataSet("select salesPersonId,(initial+firstName+' '+lastName) as [Name] from dbo.Lum_Erp_SalesPerson order by Name", "salesperson");
            ddlprimary.ClearSelection();
            ddlprimary.Items.Clear();
            ddlprimary.AppendDataBoundItems = true;
            ddlprimary.Items.Add("--All--");
            ddlprimary.Items[0].Value = "0";
            ddlprimary.DataTextField = _Ds.Tables[0].Columns["Name"].ToString();
            ddlprimary.DataValueField = _Ds.Tables[0].Columns["salesPersonId"].ToString();
            ddlprimary.DataSource = _Ds;
            ddlprimary.DataBind();
            
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        String salesPersonIds = String.Empty;
        for (int _itemsCount = 0; _itemsCount <= ddlprimary.Items.Count - 1; _itemsCount++)
        {
            if (ddlprimary.Items[_itemsCount].Selected)
            {
               salesPersonIds += "," +  ddlprimary.Items[_itemsCount].Value;
            }
        }
        salesPersonIds = salesPersonIds.Substring(1);
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            String dt1 = Convert.ToDateTime(txtDatefrom.Text).ToString("dd MMM yyyy");
            String dt2 = Convert.ToDateTime(txtDateTo.Text).ToString("dd MMM yyyy"); 

            String dtfrom = String.Format("{0:dd MMM yyyy}", dt1);
            String dtTo = String.Format("{0:dd MMM yyyy}", dt2);

            SpParam _spsalesPersonId = new SpParam("@salesPersonIds", salesPersonIds, SqlDbType.VarChar);
            SpParam _spdtfrom = new SpParam("@FROMDATE", dt1, SqlDbType.VarChar);
            SpParam _spdtTo = new SpParam("@TODATE", dt2, SqlDbType.VarChar);

            DataSet _Ds = conn.GetDataSet("Lum_Erp_Stp_GetSalesPerson_Performance", _spsalesPersonId, _spdtfrom, _spdtTo);

            decimal TotalAmount = 0;
            for (int _index1 = 0; _index1 < _Ds.Tables[0].Rows.Count; _index1++)
            {
                if (_Ds.Tables[0].Rows[_index1]["Netamt"].ToString() != "")

                    TotalAmount = TotalAmount + Convert.ToDecimal(_Ds.Tables[0].Rows[_index1]["Netamt"].ToString());
            }
            GrandTotal = GrandTotal + TotalAmount;

            //Add started
                decimal TotalAmount_WOTAX = 0;
                for (int _index1 = 0; _index1 < _Ds.Tables[0].Rows.Count; _index1++)
                {
                    if (_Ds.Tables[0].Rows[_index1]["totpricebeforevat"].ToString() != "")

                        TotalAmount_WOTAX = TotalAmount_WOTAX + Convert.ToDecimal(_Ds.Tables[0].Rows[_index1]["totpricebeforevat"].ToString());
                }
                GrandTotal_WOTAX = GrandTotal_WOTAX + TotalAmount_WOTAX;
            //Add Ended

            if (_Ds.Tables[0].Rows.Count != 0)
            {
                GVSalesPersonSearch.DataSource = _Ds;
                GVSalesPersonSearch.DataBind();
                GVSalesPersonSearch.Visible = true;
                btnexpToExcel.Enabled = true;
            }
            else
            {
                GVSalesPersonSearch.Visible = false;
                btnexpToExcel.Enabled = false;

            }
        }
    }
    
    protected void btnexptoexcel_Click(object sender, EventArgs e)
    {
        GridViewExportUtil.Export("SalesPerformance.xls", GVSalesPersonSearch);
       
    }
    }