﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;
using System.Data.SqlClient;

public partial class Reports_Collection : BasePage
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");

        }
        if (!IsPostBack)
        {

        }
    }
    public void bindGrid()
    {
        try
        {

            DateTime fromDate1 = DateTime.ParseExact(txtFromDate.Text, "dd/MM/yyyy", null);
            DateTime toDate1 = DateTime.ParseExact(txtToDate.Text, "dd/MM/yyyy", null);
            string fromDate = fromDate1.ToString("dd MMM yyyy");
            string toDate = toDate1.ToString("dd MMM yyyy");

            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                string Sqlquery = string.Format(@"SELECT CONVERT(VARCHAR,PaymentDate,106) AS pdate,PaymentDate,CONO,CONVERT(VARCHAR,CODATE,106) AS  CODATE,
                InvoiceType,strServiceId,customerName,CAddress,CAST(TAmt AS DECIMAL(18,2)) AS TAmt,CAST(PaymentAmt AS DECIMAL(18,2)) AS PaymentAmt,CAST(TdsAmount AS DECIMAL(18,2)) AS TdsAmount,
                CAST(OthDednAmount AS DECIMAL(18,2)) AS  OthDednAmount,OthDednReason,paymentmode,BankName,BranchName,Chqdate,ChqNo
                from (
                ---------//2
                SELECT PaymentDate,CONO,CODATE,'SALES' InvoiceType ,strServiceId,customerName,(Billing_address1+ ' '+ Billing_address2 +' '+ Billing_zipCode) AS CAddress,TAmt,PaymentAmt,TdsAmount,OthDednAmount, 
                OthDednReason, paymentmode,
                BankName,BranchName,Chqdate,ChqNo
                FROM
	                (
		                SELECT PaymentDate,CONO,CODATE,strServiceId,TAmt,PaymentAmt,TdsAmount,OthDednAmount, 
		                OthDednReason,CASE WHEN CASH = 1 THEN 'Cash' else 'Cheque' END AS paymentmode,
		                BankName,BranchName,Chqdate,ChqNo,customerId
		                FROM 
		                (SELECT ChallanTypeId,ChallanId,PaymentDate,PaymentAmt,TdsAmount,OthDednAmount,Cash,OthDednReason,BankName,BranchName,Chqdate,ChqNo
			                FROM Lum_Erp_Payment WHERE ChallanTypeId =2 AND  ISNULL(PayReverse,0) = 0 
			                ) L1 LEFT JOIN

		                (SELECT ChallanId, customerId,MAX(CONO) AS CONO,MAX(CODATE) AS CODATE, serviceId,strServiceId,SUM(taMT) AS Tamt 
			                FROM(
				                SELECT ChallanId,customerPoNo AS CONO,poDate AS CODATE, c.soirid,customerId,ChallanId AS serviceId,strChallanId AS strServiceId,sd.itemId,
								                CASE WHEN taxTypeId =1 THEN
								                ((PRICE - DISCOUNT) + ((PRICE - DISCOUNT) * ISNULL(vat,0)/100)) * Quantity  
								                WHEN taxTypeId =2 THEN
								                ((PRICE - DISCOUNT) + ((PRICE - DISCOUNT) * ISNULL(cst,0)/100)) * Quantity 
								                WHEN taxTypeId =3 THEN
								                ((PRICE - DISCOUNT) + ((PRICE - DISCOUNT) * ISNULL(cstAgainstFormC,0)/100)) * Quantity  END AS taMT

								                FROM Lum_Erp_Soir SOIR,Lum_Erp_SoirItemDetails SD,Lum_Erp_item I,Lum_Erp_ChallancumInvoice C
								                WHERE SD.itemId = I.itemId AND SOIR.soirId = SD.soirId AND 
								                C.soirId = sd.soirId AND C.isDeleted =0) L9 GROUP BY ChallanId, customerId,serviceId,strServiceId )l2
				                ON L1.ChallanId= L2.serviceId)L10
				                INNER JOIN Lum_Erp_Customer L11 ON L11.customerId=L10.customerID

                ---------//1
                UNION ALL

                SELECT PaymentDate,CONO,CODATE,'SERVICE' InvoiceType ,strServiceId,customerName,(Billing_address1+ ' '+ Billing_address2 +' '+ Billing_zipCode) AS CAddress,TAmt,PaymentAmt,TdsAmount,OthDednAmount, 
                OthDednReason, paymentmode,
                BankName,BranchName,Chqdate,ChqNo
                FROM
	                (				

		                SELECT PaymentDate,CONO,CODATE,strServiceId,TAmt,PaymentAmt,TdsAmount,OthDednAmount, 
			                OthDednReason,CASE WHEN CASH = 1 THEN 'Cash' else 'Cheque' END AS paymentmode,
			                BankName,BranchName,Chqdate,ChqNo,customerId

				                FROM 
				                (SELECT ChallanTypeId,ChallanId,PaymentDate,PaymentAmt,TdsAmount,OthDednAmount,Cash,OthDednReason,BankName,BranchName,Chqdate,ChqNo
					                FROM Lum_Erp_Payment WHERE ChallanTypeId =1 AND  ISNULL(PayReverse,0) = 0 ) L1 LEFT JOIN

				                (SELECT CONO,CODATE,customerId,ChallanId,strServiceId,educationCessId,ISNULL(eduCess,0) AS eduCess,HigherEducationCessId,educationCess
					                ,serviceTaxId,serviceTax,price
					                ,price + ((serviceTax*.01) * price) + (((serviceTax*.01) * price)*(ISNULL(eduCess,0)*.01)) + (((serviceTax*.01) * price)*(educationCess*.01)) AS TAmt
					                FROM
					                (SELECT CONO,CODATE,customerId,ChallanId,strServiceId,HigherEducationCessId,educationCessId
						                ,serviceTaxId ,price,educationCess,ISNULL(serviceTax,0) AS serviceTax 
						                FROM
						                (SELECT CONO,CODATE,customerId,ChallanId,strServiceId,HigherEducationCessId,educationCessId,serviceTaxId ,price
							                ,ISNULL(educationCess,0) AS educationCess 
							                FROM
							                (SELECT customerOrderNo AS CONO,POdate AS CODATE,customerId,serviceId AS ChallanId ,strServiceId,HigherEducationCessId,educationCessId
								                ,serviceTaxId ,price                   
								                FROM Lum_Erp_Services) T1	LEFT OUTER JOIN
							                (SELECT higherEducationCessId AS HECId,educationCess 
								                FROM Lum_Erp_HigherEducationCess) T2
							                ON T1.higherEducationCessId=T2.HECId) T3	LEFT OUTER JOIN 
						                (SELECT serviceTaxId AS STId,serviceTax,description 
							                FROM Lum_Erp_ServiceTaxMaster) T4
						                ON T3.serviceTaxId=T4.STId)T5	LEFT OUTER JOIN
					                (SELECT educationCessId AS ECId,eduCess 
						                FROM Lum_Erp_EducationCess) T6
					                ON T5.educationCessId=T6.ECId)L2
			                ON L1.ChallanId= L2.ChallanId) L10
				                INNER JOIN Lum_Erp_Customer L11 ON L11.customerId=L10.customerID
                			
                ---------//3
                UNION ALL

                SELECT PaymentDate,CONO,CODATE,'CSA' InvoiceType ,strServiceId,customerName,CAddress,TAmt,PaymentAmt,TdsAmount,OthDednAmount, 
                OthDednReason,CASE WHEN CASH = 1 THEN 'Cash' else 'Cheque' END AS paymentmode,
                BankName,BranchName,Chqdate,ChqNo
	                FROM 
	                (SELECT ChallanTypeId,ChallanId,PaymentDate,PaymentAmt,TdsAmount,OthDednAmount,Cash,OthDednReason,BankName,BranchName,Chqdate,ChqNo
		                FROM Lum_Erp_Payment WHERE ChallanTypeId =3 AND  ISNULL(PayReverse,0) = 0 ) L1 LEFT JOIN

	                (SELECT L1.custMacId,L1.customerId,L1.SRNo,customerName ,(Billing_address1+ ' '+ Billing_address2 +' '+ Billing_zipCode) AS CAddress,
		                L4.Id ChallanId ,L5.contractNo as CONO,L4.ServiceBillNo AS strServiceId,CONVERT(VARCHAR,ServiceBillDate,106) AS CODATE,isnull(NetPayable,0) AS TAmt
		                FROM Lum_Erp_CustomerMachine L1 INNER JOIN Lum_Erp_Customer L2 ON L1.customerId=L2.customerID
			                INNER JOIN Lum_Erp_CSABill L4 ON L1.custMacId=L4.custMacId AND L4.isblocked=0
			                INNER JOIN LUM_ERP_MachineContract L5 ON L1.custMacId=L5.custMacId)L2
		                ON L1.ChallanId= L2.ChallanId) coll
	                WHERE PAYMENTDATE >= '{0}' AND PAYMENTDATE <= '{1}' 
                ORDER BY InvoiceType,PaymentDate", fromDate, toDate);
                DataSet _Ds = conn.GetDataSet(Sqlquery, "ReportBind");
                ReportGrid.DataSource = _Ds;
                ReportGrid.DataBind();
                if (_Ds.Tables[0].Rows.Count != 0)
                {
                    
                    btnexpToExcel.Enabled = true;
                }
                else
                    btnexpToExcel.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            if (Convert.ToString(ex.Message) == "String was not recognized as a valid DateTime.")
            {
                Page.RegisterClientScriptBlock("message", "<script>alert('Date was not in correct Format...');</script>");
            }
            else
            {
                lblMsg.Text = Convert.ToString(ex.Message);
            }

        }
    }

    protected void btnReport_Click(object sender, EventArgs e)
    {
        bindGrid();
    }


    private double CalcTotalAmount(string _netVal)
    {
        double retVal;
        if (double.TryParse(_netVal, out retVal))
        {
            retVal = Double.Parse(_netVal);

        }
        return retVal;
    }
    protected void btnexptoexcel_Click(object sender, EventArgs e)
    {

        GridViewExportUtil.Export("CollectionRegister.xls", ReportGrid);




    }

    protected void ReportGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }
}

