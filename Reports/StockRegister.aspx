﻿<%@ Page Language="C#" MasterPageFile="~/Reports/Reports.master" AutoEventWireup="true" CodeFile="StockRegister.aspx.cs" Inherits="Reports_StockRegister" Title="Lumen Erp ....Stock Register" EnableEventValidation="false"  %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="PageMargin">
<div class="PageHeading">Stock Register</div>
<div class="Line">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
                     </div>
<!--Content Table Start-->
<div class="PageContent">
   <table width="100%" border="0" cellspacing="5" cellpadding="0">
   
        <tr><td width="19%" align="center" valign="middle">Division</td>
            <td width="20%" align="center" valign="middle">SubDivision</td>
            <td width="20%" align="center" valign="middle">Item</td>
            <td width="10%" align="center" valign="middle">Store</td>
            <td width="10%" align="center" valign="middle">&nbsp;</td>            
            <td width="10%" align="center" valign="middle">&nbsp;</td>   
       </tr>
    <tr>
      <td align="center" valign="middle">
                <asp:DropDownList ID="ddlDivision" runat="server"  CssClass="DropdownSmall"
                 Width="100%"  />
            </td>
            <td align="center" valign="middle">
                <asp:DropDownList ID="ddlsubDivision" runat="server"  CssClass="DropdownExtraSmall"
                Width="100%"  />
            </td>
            <td align="center" valign="middle">
                <asp:DropDownList ID="ddlitem" runat="server"  CssClass="DropdownLarge"
                 Width="100%" />
            </td>
              <td align="center" valign="middle">
                <asp:DropDownList ID="ddlstore" runat="server"  CssClass="DropdownLarge"
                 Width="100%" />
            </td>    
            <td align="center" valign="middle">
               
                <asp:CheckBox  ID="chkqty" runat="server" Text="By Qty" />
            </td>
             <td align="center" valign="middle">   
                <asp:Button ID="addItemLine" runat="server" Text="Go" OnClick="addItemLine_Click"
                    ValidationGroup="addItem" CausesValidation="true" CssClass="Button" />
            </td>                  
      </tr>

       <cc1:CascadingDropDown ID="CascadingDropDown3"
            runat="server" 
            TargetControlID="ddlDivision" LoadingText="[Loading...]"
            Category="Division"
            PromptText="Select a Division"
            ServicePath ="~/divisionService.asmx"
            ServiceMethod="GetDivision" >   </cc1:CascadingDropDown>
            
                  
       <cc1:CascadingDropDown ID="CascadingDropDown1"
            runat="server" ParentControlID = "ddlDivision" 
            TargetControlID="ddlsubDivision" LoadingText="[Loading...]"
            Category="subDivision"
            PromptText="Select a subDivision"
            ServicePath ="~/divisionService.asmx"
            ServiceMethod="GetSubdividsionForDivision" >   </cc1:CascadingDropDown>

      <cc1:CascadingDropDown ID="CascadingDropDown2"
            runat="server" ParentControlID = "ddlsubDivision" 
            TargetControlID="ddlitem" LoadingText="[Loading...]"
            Category="Item"
            PromptText="Select Item"
            ServicePath ="~/divisionService.asmx"
            ServiceMethod="GetItemForSubdividsion" >   </cc1:CascadingDropDown>
            
    </table>
    <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
</div>
<!--Content Table End-->
<div class="Line"></div>
<div class="PageContent">
<table width="100%" align="center" cellpadding="0" cellspacing="5">
  <tr>
  	<td  align="left" valign="top">&nbsp;</td></tr>
  	<tr>
                <asp:GridView ID="GVstockRegister" runat="server" 
                    AutoGenerateColumns="False" CellPadding="0" CssClass="ReportGridHeading"
                    BorderColor="#6699CC" BorderStyle="Solid" BorderWidth="0px"
                    Width="100%" ShowFooter="True">
                  
                    <RowStyle HorizontalAlign="Center" />
                  
         <Columns>
                   <asp:BoundField DataField="ItemCode" HeaderText="Item Code" 
                            HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                            <HeaderStyle Font-Bold="True"></HeaderStyle>
                            <ItemStyle Width="10%"></ItemStyle>
                    </asp:BoundField>
                    
                   <asp:BoundField DataField="SerialNo" HeaderText="Serial No" 
                            HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                            <HeaderStyle Font-Bold="True"></HeaderStyle>
                            <ItemStyle Width="10%"></ItemStyle>
                    </asp:BoundField>                    
                        
                       <asp:BoundField DataField="quantity" HeaderText="Quantity" 
                            HeaderStyle-Font-Bold="true" ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                            <HeaderStyle Font-Bold="True"></HeaderStyle>
                            <ItemStyle Width="10%"></ItemStyle>
                    </asp:BoundField>  
           </Columns>
                   
                   
                   
<HeaderStyle CssClass="ReportGridHeading"></HeaderStyle>
                   
                   
                   
                </asp:GridView>
                
    </tr>
  	</table> 
</div>

<!--Page Body End-->
</div>
</asp:Content>

