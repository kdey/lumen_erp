﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;
using System.Data.SqlClient;

public partial class Reports_Outstanding : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");

        }
        if (GetLoggedinUser().UserType == 2)
        {
            Response.Redirect("./../Default.aspx");
        }
        if (!IsPostBack)
        {
           // bindControl();
            

        }
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        gridMessage.Text = "";
        Bind_outstanding();
    }
    public void Bind_outstanding()
    {
        DateTime date = DateTime.Today;
        
        if (string.IsNullOrEmpty(txtoutdate.Text))
        {
            txtoutdate.Text = DateTime.Today.ToString("dd/MM/yyyy");
        }
        else
        {
            date = DateTime.ParseExact(txtoutdate.Text, "dd/MM/yyyy", null);
        }

        using (IConnection conn = DataConnectionFactory.GetConnection())
        {

            try
            {
                string fromDate ;
                fromDate = date.ToString("dd MMM yyyy");
                SpParam[] _Sparray = new SpParam[2];

                _Sparray[0] = new SpParam("@OUTDATE", fromDate,SqlDbType.VarChar );
                _Sparray[1] = new SpParam("@ChallanTypeId", drInvoiceType.SelectedValue, SqlDbType.Int);

                DataSet _Ds = conn.GetDataSet("Lum_Erp_Stp_GetOutstanding", _Sparray);

                if (drInvoiceType.SelectedValue == "1")//service
                {
                    if (_Ds.Tables[0].Rows.Count != 0)
                    {
                        GVoutstandingService.Visible = true;
                        GVoutstandingSales.Visible = false;
                        GVoutstandingCSAService.Visible = false;
                        
                        GVoutstandingService.DataSource = _Ds;
                        GVoutstandingService.DataBind();
                        
                        btnexpToExcel.Enabled = true;
                        GVoutstandingSales.DataSource = null;
                        GVoutstandingCSAService.DataSource = null;
                        
                    }
                    else
                    {
                        GVoutstandingService.Visible = false;
                        btnexpToExcel.Enabled = false;

                   }
                }
                if (drInvoiceType.SelectedValue == "2")//sales
                {
                    if (_Ds.Tables[0].Rows.Count != 0)
                    {
                        GVoutstandingSales.Visible = true;
                        GVoutstandingService.Visible = false;
                        GVoutstandingCSAService.Visible = false;

                        GVoutstandingSales.DataSource = _Ds;
                        GVoutstandingSales.DataBind();
                        
                        btnexpToExcel.Enabled = true;
                        GVoutstandingService.DataSource = null;
                        GVoutstandingCSAService.DataSource = null;
                        
                    }
                    else
                    {
                        GVoutstandingSales.Visible = false;
                        btnexpToExcel.Enabled = false;

                    }
                }
                if (drInvoiceType.SelectedValue == "3")//CSA service
                {
                    if (_Ds.Tables[0].Rows.Count != 0)
                    {
                        GVoutstandingCSAService.Visible = true;
                        GVoutstandingService.Visible = false;
                        GVoutstandingSales.Visible = false;
                        

                        GVoutstandingCSAService.DataSource = _Ds;
                        GVoutstandingCSAService.DataBind();

                        btnexpToExcel.Enabled = true;
                        GVoutstandingSales.DataSource = null;
                        GVoutstandingService.DataSource = null;

                    }
                    else
                    {
                        GVoutstandingCSAService.Visible = false;
                        btnexpToExcel.Enabled = false;

                    }
                }
                              


            }

            catch (Exception ex)
            {
                gridMessage.Text = ex.Message;
                gridMessage.ForeColor =Color.Red;

            }

        }
    }
    protected void btnexptoexcel_Click(object sender, EventArgs e)
    {

        if (drInvoiceType.SelectedValue == "1")//service
        {
            GridViewExportUtil.Export("SegmentWiseBusinessVolume.xls", GVoutstandingService);
        }
        else
        {
            if (drInvoiceType.SelectedValue == "3")//CSAservice
            {
                GridViewExportUtil.Export("SegmentWiseBusinessVolume.xls", GVoutstandingCSAService);
            }
            else//Sales
            {
                GridViewExportUtil.Export("SegmentWiseBusinessVolume.xls", GVoutstandingSales);
            }
        }


    }
}
