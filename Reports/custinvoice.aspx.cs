﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;
using System.Data.SqlClient;


public partial class Reports_custinvoice : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");

        }
        if (!IsPostBack)
        {

        }
    }
    public void bindGrid()
    {
        try
        {

            DateTime fromDate1 = DateTime.ParseExact(txtFromDate.Text, "dd/MM/yyyy", null);
            DateTime toDate1 = DateTime.ParseExact(txtToDate.Text, "dd/MM/yyyy", null);
            string fromDate = fromDate1.ToString("dd MMM yyyy");
            string toDate = toDate1.ToString("dd MMM yyyy");

            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                SpParam[] SP = new SpParam[2];
                SP[0] = new SpParam("@Fdate", fromDate1, SqlDbType.SmallDateTime);
                SP[1] = new SpParam("@Tdate", toDate1, SqlDbType.SmallDateTime);
                using (DataSet DS = conn.GetDataSet("Lum_Erp_Stp_GetCustInvoiceDetails", SP))
                {
                    ReportGrid.DataSource = DS;
                    ReportGrid.DataBind();
                    if (DS.Tables[0].Rows.Count != 0)
                    {

                        btnexpToExcel.Enabled = true;
                    }
                    else
                        btnexpToExcel.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        {
            if (Convert.ToString(ex.Message) == "String was not recognized as a valid DateTime.")
            {
                Page.RegisterClientScriptBlock("message", "<script>alert('Date was not in correct Format...');</script>");
            }
            else
            {
                lblMsg.Text = Convert.ToString(ex.Message);
            }

        }
    }
    protected void btnReport_Click(object sender, EventArgs e)
    {
        bindGrid();
    }


    private double CalcTotalAmount(string _netVal)
    {
        double retVal;
        if (double.TryParse(_netVal, out retVal))
        {
            retVal = Double.Parse(_netVal);

        }
        return retVal;
    }
    protected void btnexptoexcel_Click(object sender, EventArgs e)
    {

        GridViewExportUtil.Export("CollectionRegister.xls", ReportGrid);

    }
}
