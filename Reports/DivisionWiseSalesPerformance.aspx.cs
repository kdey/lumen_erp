﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;
using System.Data.SqlClient;

public partial class Reports_DivisionWiseSalesPerformance :BasePage
{
    decimal GrandTotal = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");

        }
        if (!IsPostBack)
        {
            bindControl();
        }
    }
    public void bindControl()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DataSet _Ds = conn.GetDataSet("select * from Lum_Erp_Division order by divisionName", "Division");
            ddlDivision.ClearSelection();
            ddlDivision.Items.Clear();
            ddlDivision.AppendDataBoundItems = true;
            ddlDivision.Items.Add("--All--");
            ddlDivision.Items[0].Value = "0";
            ddlDivision.DataTextField = _Ds.Tables[0].Columns["divisionName"].ToString();
            ddlDivision.DataValueField = _Ds.Tables[0].Columns["divisionId"].ToString();
            ddlDivision.DataSource = _Ds;
            ddlDivision.DataBind();
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {

        String divisionIds = String.Empty;
        for (int _itemsCount = 0; _itemsCount <= ddlDivision.Items.Count - 1; _itemsCount++)
        {
            if (ddlDivision.Items[_itemsCount].Selected)
            {
                divisionIds += "," + ddlDivision.Items[_itemsCount].Value;
            }
        }
        if (string.IsNullOrEmpty(divisionIds))
        {
            return;
        }

        divisionIds = divisionIds.Substring(1);
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DateTime dt1 = Convert.ToDateTime(txtDatefrom.Text);
            DateTime dt2 = Convert.ToDateTime(txtDateTo.Text);

            String dtfrom = String.Format("{0:dd MMM yyyy}", dt1);
            String dtTo = String.Format("{0:dd MMM yyyy}", dt2);

            SpParam _spsalesPersonId = new SpParam("@divisionIds", divisionIds, SqlDbType.VarChar);
            SpParam _spdtfrom = new SpParam("@FROMDATE", dtfrom, SqlDbType.VarChar);
            SpParam _spdtTo = new SpParam("@TODATE", dtTo, SqlDbType.VarChar);

            DataSet _Ds = conn.GetDataSet("Lum_Erp_Stp_GetDivision_Performance", _spsalesPersonId, _spdtfrom, _spdtTo);

            decimal TotalAmount = 0;
            for (int _index1 = 0; _index1 < _Ds.Tables[0].Rows.Count; _index1++)
            {
                TotalAmount = TotalAmount + Convert.ToDecimal(_Ds.Tables[0].Rows[_index1]["Netamt"].ToString());
            }
            GrandTotal = GrandTotal + TotalAmount;
            if (_Ds.Tables[0].Rows.Count != 0)
            {
                GVSalesPersonSearch.DataSource = _Ds;
                GVSalesPersonSearch.DataBind();
                GVSalesPersonSearch.Visible = true;
                btnexpToExcel.Enabled = true;
            }
            else
            {
                GVSalesPersonSearch.Visible = false;
                btnexpToExcel.Enabled = false;
            }
        }
    }
    
    protected void btnexptoexcel_Click(object sender, EventArgs e)
    {
        GridViewExportUtil.Export("DivisionWiseBusinessVolume.xls", GVSalesPersonSearch);
    }
  }
