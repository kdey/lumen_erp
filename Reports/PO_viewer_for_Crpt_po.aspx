﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/Reports.master" AutoEventWireup="true" CodeFile="PO_viewer_for_Crpt_po.aspx.cs" Inherits="Reports_PO_viewer_for_Crpt_po" %>

<%@ Register assembly="CrystalDecisions.Web, Version=10.5.3700.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" 
    AutoDataBind="True" Height="50px" oninit="CrystalReportViewer1_Init" 
        onprerender="CrystalReportViewer1_PreRender" 
        ReportSourceID="CrystalReportSource1" Width="350px" />
    <CR:CrystalReportSource ID="CrystalReportSource1" runat="server">
        <Report FileName="Reports\Crpt_PO.rpt">
        </Report>
    </CR:CrystalReportSource>
</asp:Content>

