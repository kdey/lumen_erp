﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;
using System.Data.SqlClient;

public partial class Reports_ServiceBill : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
           
        }
        if (!IsPostBack)
        {
            bind_service_id();
        }
    }
    public void bind_service_id()
    {
        try
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                string Sqlquery = string.Format("SELECT * FROM LUM_ERP_Services");
                DataSet _Ds = conn.GetDataSet(Sqlquery, "ServiceBind");
                dr_serviceId.DataTextField = "strServiceId";
                dr_serviceId.DataValueField = "serviceId";
                dr_serviceId.DataSource = _Ds;
                dr_serviceId.DataBind();
                dr_serviceId.Items.Insert(0, "Please select");


            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            if (dr_serviceId.SelectedIndex != 0)
            {
                string serviceid = dr_serviceId.SelectedValue;
                Response.Redirect("ServiceBillPrint.aspx?serviceid=" + serviceid);
            }
            else
            {
                Label2.Text = "Please Select Service ID";
                Label2.Visible = true;
            }
        }
        catch (Exception ex)
        { 
            Response.Write(ex.Message);
        }
    }
    
}
