﻿<%@ Page Language="C#" Culture="en-GB" AutoEventWireup="true" MaintainScrollPositionOnPostback="true"
    MasterPageFile="~/Reports/Reports.master" CodeFile="PurchaseOrderRegister.aspx.cs"
    Inherits="Reports_PurchaseOrderRegister" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <!--Page Body Start-->
    <div class="PageMargin">
        <div class="PageHeading">
            Purchase Order Register</div>
        <div class="Line">
        </div>
        <!--Content Table Start-->
        <div class="PageContent">
            <table width="100%" border="0" cellspacing="5" cellpadding="0">
                <tr>
                    <td width="7%" align="left" valign="middle">
                        <strong>Select :</strong>
                    </td>
                    <td width="5%" align="right" valign="middle">
                        Vendor
                    </td>
                    <td width="18%" align="left" valign="middle">
                        <asp:DropDownList ID="ddlVendor" runat="server" CssClass="TextboxSmall">
                            <asp:ListItem Selected="True" Value="0">All</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td width="9%" align="left" valign="middle">
                        <strong>Select Date :</strong>
                    </td>
                    <td width="5%" align="right" valign="middle">
                        From&nbsp;&nbsp;<span style="color: Red; font-weight: bold">*</span>
                    </td>
                    <td width="18%" align="left" valign="middle">
                        <asp:TextBox ID="txtFromDate" runat="server" onkeyup="document.getElementById(this.id).value=''"
                            autocomplete='off' CssClass="TextboxSmall" />
                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="txtFromDate">
                        </cc1:CalendarExtender>
                        <br />
                    </td>
                    <td width="5%" align="right" valign="middle">
                        To&nbsp;&nbsp;<span style="color: Red; font-weight: bold">*</span>
                    </td>
                    <td width="18%" align="left" valign="middle">
                        <asp:TextBox ID="txtToDate" runat="server" onkeyup="document.getElementById(this.id).value=''"
                            autocomplete='off' CssClass="TextboxSmall"></asp:TextBox>
                        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" TargetControlID="txtToDate">
                        </cc1:CalendarExtender>
                        <br />
                    </td>
                    <td width="15%" align="center" valign="middle">
                        <asp:Button ID="btnReport" runat="server" Text="Report" CssClass="Button" OnClick="btnReport_Click" />&nbsp;&nbsp;<input
                            name="Submit" type="submit" causesvalidation="false" class="Button" value="Cancel" />
                    </td>
                </tr>
                <tr>
                    <td width="7%" align="left" valign="middle">
                        &nbsp;
                    </td>
                    <td width="5%" align="right" valign="middle">
                        &nbsp;
                    </td>
                    <td width="18%" align="left" valign="middle">
                        &nbsp;
                    </td>
                    <td width="9%" align="left" valign="middle">
                        &nbsp;
                    </td>
                    <td width="5%" align="right" valign="middle">
                        &nbsp;
                    </td>
                    <td width="18%" align="left" valign="middle">
                        <asp:RequiredFieldValidator ControlToValidate="txtFromDate" EnableClientScript="true"
                            ErrorMessage="From date cannot be empty" ID="RequiredFieldValidator1" runat="server"
                            Text="From date cannot be empty" />
                    </td>
                    <td width="5%" align="right" valign="middle">
                        &nbsp;
                    </td>
                    <td width="18%" align="left" valign="middle">
                        <asp:RequiredFieldValidator ControlToValidate="txtToDate" EnableClientScript="true"
                            ErrorMessage="To date cannot be empty" ID="RequiredFieldValidator2" runat="server"
                            Text="To date cannot be empty" />
                    </td>
                    <td width="15%" align="center" valign="middle">
                        <asp:Button ID="btnexpToExcel" runat="server" Text="Export to Excel" OnClick="btnexptoexcel_Click"
                            ValidationGroup="report" Style="width: 120px; height: 19px;" CssClass="Button"
                            Enabled="False" />
                    </td>
                </tr>
                <tr>
                    <td width="7%" align="left" valign="middle">
                        &nbsp;
                    </td>
                    <td width="5%" align="right" valign="middle">
                        &nbsp;
                    </td>
                    <td width="18%" align="left" valign="middle">
                        &nbsp;
                    </td>
                    <td width="9%" align="left" valign="middle">
                        &nbsp;
                    </td>
                    <td width="5%" align="right" valign="middle">
                        &nbsp;
                    </td>
                    <td width="18%" align="left" valign="middle">
                        &nbsp;
                    </td>
                    <td width="5%" align="right" valign="middle">
                        &nbsp;
                    </td>
                    <td width="18%" align="left" valign="middle" colspan="2" style="width: 33%">
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="End date should be greater than start date"
                            ControlToCompare="txtFromDate" ControlToValidate="txtToDate" Type="Date" Operator="GreaterThanEqual"></asp:CompareValidator>
                    </td>
                </tr>
            </table>
        </div>
        <!--Content Table End-->
        <div class="Line">
        </div>
        <div class="PageContent">
            <table width="100%" align="center" cellpadding="0" cellspacing="5">
                <tr>
                    <td width="15%" align="left" valign="top">
                        <b>Vendor Name:</b>
                    </td>
                    <td width="85%" align="left" valign="top">
                        <asp:Label ID="lblVendor" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <!--Page Body End-->
    </div>
    <!--Grid Table Start-->
    <asp:GridView ID="dgPORegister" runat="server" AutoGenerateColumns="false" CellPadding="0"
        CellSpacing="0" HeaderStyle-CssClass="ReportGridHeading" BorderColor="#6699CC"
        BorderStyle="Solid" BorderWidth="0px" ItemStyle-CssClass="ReportGridData" Width="100%"
        ItemStyle-Width="10%" CssClass="ReportGridHeading" Font-Bold="True" OnRowCreated="dgPORegister_RowCreated">
        <RowStyle CssClass="ReportGridData" Width="10%"></RowStyle>
        <Columns>
            <asp:BoundField DataField="activityDateTime" HeaderText="Date" HeaderStyle-Font-Bold="true"
                ItemStyle-CssClass="ReportGridData">
                <HeaderStyle Font-Bold="True"></HeaderStyle>
                <ItemStyle CssClass="ReportGridData"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="strPoId" HeaderText="P.O. Id" HeaderStyle-Font-Bold="true"
                ItemStyle-CssClass="ReportGridData">
                <HeaderStyle Font-Bold="True"></HeaderStyle>
                <ItemStyle CssClass="ReportGridData"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="itemCode" HeaderText="Item Part No." HeaderStyle-Font-Bold="true"
                ItemStyle-CssClass="ReportGridData">
                <HeaderStyle Font-Bold="True"></HeaderStyle>
                <ItemStyle CssClass="ReportGridData"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="quantity" HeaderText="Quantity" HeaderStyle-Font-Bold="true"
                ItemStyle-CssClass="ReportGridData">
                <HeaderStyle Font-Bold="True"></HeaderStyle>
                <ItemStyle CssClass="ReportGridData"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="ValBefore" HeaderText="Value before VAT (Rs.)" HeaderStyle-Font-Bold="true"
                ItemStyle-CssClass="ReportGridData">
                <HeaderStyle Font-Bold="True"></HeaderStyle>
                <ItemStyle CssClass="ReportGridData"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="Vat" HeaderText="VAT (Rs.)" HeaderStyle-Font-Bold="true"
                ItemStyle-CssClass="ReportGridData">
                <HeaderStyle Font-Bold="True"></HeaderStyle>
                <ItemStyle CssClass="ReportGridData"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="VaLAfter" HeaderText="Value after VAT (Rs.)" HeaderStyle-Font-Bold="true"
                ItemStyle-CssClass="ReportGridData">
                <HeaderStyle Font-Bold="True"></HeaderStyle>
                <ItemStyle CssClass="ReportGridData"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="strGrnId" HeaderText="GRN No." HeaderStyle-Font-Bold="true"
                ItemStyle-CssClass="ReportGridData">
                <HeaderStyle Font-Bold="True"></HeaderStyle>
                <ItemStyle CssClass="ReportGridData"></ItemStyle>
            </asp:BoundField>
        </Columns>
        <HeaderStyle CssClass="ReportGridHeading"></HeaderStyle>
    </asp:GridView>
    <div class="Line">
        <asp:Label ID="lblMsg" runat="server" ForeColor="Red" Font-Size="XX-Small"></asp:Label>
    </div>
    <br />
</asp:Content>
