﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ServiceBillPrint.aspx.cs" Inherits="Reports_ServiceBillPrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Lumen ERP Service Bill</title>
      <link href="./../style/style.css" rel="stylesheet" type="text/css" />

    <script src="../lumen.js" type="text/javascript"></script>
    <script type ="text/javascript">
        function print_page() {

            var control = document.getElementById("btnPrint");
           
                control.style.visibility = "hidden";

            var control1 = document.getElementById("btnCancel");
            
                control1.style.visibility = "hidden";
            // else control.style.visibility = "visible";

            /*    var OLECMDID = 7;
            /* OLECMDID values:
            * 6 - print
            * 7 - print preview
            * 1 - open window
            * 4 - Save As
         
         var PROMPT = 1; // 2 DONTPROMPTUSER 
            var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
            document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
            WebBrowser1.ExecWB(OLECMDID, PROMPT);
            WebBrowser1.outerHTML = "";*/
          //  var OLECMDID = 7;
            /* OLECMDID values:
            * 6 - print
            * 7 - print preview
            * 1 - open window
            * 4 - Save As
            */
           // var PROMPT = 1; // 2 DONTPROMPTUSER
          //  var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
           // document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
            //WebBrowser1.ExecWB(OLECMDID, PROMPT);
            //WebBrowser1.outerHTML = "";
        }
        function viewControl() {
            var control = document.getElementById("btnPrint");
            control.style.visibility = "visible";
            var control1 = document.getElementById("btnCancel");
            control1.style.visibility = "visible";
        }
    </script>
  
    <style type="text/css">
        .style1
        {
            width: 80%;
            }
        .style2
        {
            width: 20%;
            }
    </style>
  
</head>
<body>
    <form id="form1" runat="server" target="_top">
    <div class="Header">
        <div class="HeaderImg">
        </div>
    </div>
    <div class="PageContentDiv">
    <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalTextBlack" >
        <tr>
            <td colspan="4" style="font-size:large">Invoice</td>
        </tr>
        <tr>
            <td colspan="4" style="font-size:large">Service Bill</td>
        </tr>
   </table>   
    <table class="PageContent" width="100%"  style="border-color:Black; margin:auto">
         <tr class="NormalTextBlack">
            <td align="left" style="width:20%">Invoice No. :&nbsp;&nbsp;</td>
            <td align="left" style="width:30%"><asp:Label ID="lbl_billno" runat="server" Font-Bold="True"></asp:Label></td>
            <td align="left" style="width:20%">Date :&nbsp;</td>
            <td style="width:30%" align="left"><asp:Label ID="lbl_date" runat="server" Font-Bold="True"></asp:Label></td>
         </tr>
    </table>
    <table class="PageContent" width="100%" style="border-color:Black;">
         <tr class="NormalTextBlack">
         <td style="width:20%" align="left">Customer Name:</td><td style="width:80%" align="left">
             <asp:Label ID="lblcustomer" runat="server"></asp:Label>
             </td>
         </tr>
         <tr class="NormalTextBlack">
            <td style="width:20%" align="left">Customer Address:</td><td style="width:80%" align="left">
             <asp:Label ID="lblAdd1" runat="server"></asp:Label>
                ,<asp:Label ID="lbl_add2" runat="server"></asp:Label>
                ,Pin-
                <asp:Label ID="lblzip" runat="server"></asp:Label>
                <%--<br />
                <asp:Label ID="lblAdd2" runat="server"></asp:Label>
                ,<asp:Label ID="lbl_add3" runat="server"></asp:Label>
                ,Pin-<asp:Label ID="lbl_zip4" runat="server"></asp:Label>--%>
             </td>
         </tr>
    </table>
    <table class="PageContent" width="100%" style="border-color:Black;">
        <tr class="NormalTextBlack">
            <td align="left" style="width:20%">Customer Order No.:</td><td align="left" style="width:30%">
               <asp:Label ID="CustOrderNo" runat="server"></asp:Label>
               </td><td align="left" style="width:20%">Customer Order Date.:</td><td align="left" style="width:30%"><asp:Label ID="CustOrderDate" runat="server"></asp:Label></td>
         </tr>
    </table>
    <br />
    <br />  
    <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="PageContent" style="border-color:Black;">
        <tr class="NormalTextBlack">
            <td colspan="4" align="center" style="font-size:small; font-weight:bold">Transaction Details</td>
        </tr>
    </table>    
  <table width="100%" class="PageContent" style="border-color:Black;">
        <tr class="NormalTextBlack">
            <td colspan="3" >
                <asp:DataGrid ID="poLineGrid" runat="server" AutoGenerateColumns="False"
                    Width="100%" onitemdatabound="poLineGrid_ItemDataBound" ShowFooter="True"  ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                    <Columns>
                        <asp:BoundColumn DataField="serviceDesc" HeaderText="Service Description" HeaderStyle-HorizontalAlign ="Right">
                            <HeaderStyle Font-Bold="True"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="price" HeaderText="Rate" HeaderStyle-HorizontalAlign ="Right">
                            <HeaderStyle Font-Bold="True"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Amount" HeaderText="Amount(Rs.)" HeaderStyle-HorizontalAlign ="Right">
                            <HeaderStyle Font-Bold="True"></HeaderStyle>
                        </asp:BoundColumn>
                    </Columns>
                    
                </asp:DataGrid>
            
               <%-- <table border="0px" cellpadding="0px" cellspacing="opx" 
                    style="border-style: inherit; border-color: #000000;" width="100%" >
                   <tr class="NormalTextBlack">
                        <td align="center">Description of Service</td>
                        <td align="center">Rate(Rs.)</td>
                        <td align="center">Amount(Rs)</td>
                    </tr>
                   <tr class="NormalTextBlack">
                        <td align="center">&nbsp;</td>
                        <td align="center">&nbsp;</td>
                        <td align="center">&nbsp;</td>
                    </tr>
                    <tr class="NormalTextBlack">
                        <td align="center"><asp:Label ID="lbldesc" runat="server"></asp:Label></td>
                        <td align="center"><asp:Label ID="lblprice" runat="server"></asp:Label></td>
                        <td align="center"><asp:Label ID="lblamount" runat="server"></asp:Label></td>
                    </tr>
                    <tr class="NormalTextBlack">
                        <td align="center">Service Tax :</td>
                        <td align="center"><asp:Label ID="lbltax" runat="server"></asp:Label></td>
                        <td align="center"><asp:Label ID="lbltaxamount" runat="server"></asp:Label></td>
                    </tr>
                    <tr class="NormalTextBlack">
                        <td align="center">Total</td>
                        <td align="center"></td>
                        <td align="center"><asp:Label ID="lbltotal" runat="server" style="font-weight: 700"></asp:Label></td>
                    </tr>
                    <tr class="NormalTextBlack">
                        <td colspan="3">&nbsp;</td>
                    </tr>
                    <tr class="NormalTextBlack">
                        <td  colspan="3" align="left">Total In Words:<asp:Label ID="lblTotalWord" runat="server"></asp:Label></td>
                    </tr>
                    <tr class="NormalTextBlack">
                        <td  colspan="3" align="center">&nbsp;</td>
                    </tr>
                </table>--%>
            </td>
        </tr>
     </table>
     <table width="100%" class="PageContent" style="border-color:Black;height:20px">     
        <tr class="NormalTextBlack">
            <td align="left" colspan="4">Amount in Words :&nbsp;&nbsp;
                <asp:Label ID="lblNetAmtTextFormat" runat="server" Text=""></asp:Label></td>
        </tr>
    </table>
      <table class="NormalTextBlack" width="100%">

        <tr class="NormalTextBlack">
            <td colspan="2">
                <input  id="btnCancel" value="Cancel" type="button" runat="server"
                        style="width: 141px; cursor: hand;"  onserverclick="btnCancel_ServerClick"/>
                            <input  id="btnPrint" value="Print" type="button"
                        onclick="print_page();javascript:window.print(); viewControl();" 
                        style="width: 141px; cursor: hand;" />
            </td>
         </tr>
         <tr class="NormalTextBlack">
            <td align="left" style="width:13%"><strong>TIN (W.B.) :</strong></td>
            <td align="left" style="width:87%"><strong><asp:Label ID="lblTin" runat="server"></asp:Label></strong></td>
         </tr>
         <tr class="NormalTextBlack">
            <td align="left" style="width:13%"><strong>CST No. :</strong></td>
            <td align="left" style="width:87%"><strong><asp:Label ID="lblCSTNo" runat="server" Text=""></asp:Label></strong></td>
         </tr>
         <tr class="NormalTextBlack">
            <td align="left" style="width:13%"><strong>Service Tax Reg. No.</strong></td>
            <td align="left" style="width:87%"><strong><asp:Label ID="lblServiceTaxRegnNo" runat="server" Text=""></asp:Label></strong></td>
         </tr>
     </table>

      <table class="NormalTextBlack" width="100%"> 
         <tr class="NormalTextBlack">
            <td style="width:50%"></td>
            <td align="center" style="width:50%; font-style:italic"><strong>E.&.O.E</strong></td>
         </tr>   
         <tr class="NormalTextBlack">
            <td style="width:50%"></td>
            <td align="center" style="width:50%" >for&nbsp;<strong>Lumen Tele Systems (P) Limited</strong></td>
         </tr>
         <tr class="NormalTextBlack">
            <td style="width:50%" class="style1"></td>
            <td align="center" style="width:50%" class="style2"><strong><br /><br />Authorised Signatory</strong></td>
         </tr>
    </table>
    <table class="NormalTextBlackSmall" width="100%">
        <tr>
            <td align="left" style="text-decoration:underline">Terms & Conditions</td> 
        </tr>
    </table>
    <br />
    <table class="NormalTextBlackSmall" width="100%">
        <tr>
            <td align="left">Payment against this invoice should be made by A/C payee chequee/DD in the name of Lumen Tele-Systems (P) Ltd.</td> 
        </tr>
        <tr>
            <td align="left">Interest @24% per annum will be charged on this bill if not paid on presentation.</td> 
        </tr>
        <tr>
            <td align="left">Goods once sold will Not be exchanged.</td> 
        </tr>
    </table>
    <br />
    <table class="NormalTextBlackSmall" width="100%">
        <tr>
            <td align="center" colspan="4">SUBJECT TO KOLKATA JURIDICTION</td>
         </tr>
         <tr>
            <td align="center" colspan="4" style="font-style:italic">This is a Computer Generated Invoice</td>
         </tr>
         <tr>
            <td align="center" colspan="4">System Designed & Developed by Bitscrape Solution Pvt.Ltd.,www.bitscrape.com</td>
         </tr>
    </table>       
    <%--<p class="NormalTextBlack">
&nbsp;<span class="style5">Terms &amp; Conditions</span></p>
    <p class="NormalTextBlack">
        1. Payment against this invoice should be made by A/C payee cheque/DD
    </p>
    <p class="NormalTextBlack">
         in the name of Lumen Tele systems (P) Ltd.</p>
    <p class="NormalTextBlack">
        2.Interest @24% per annum will be charged on this bill if not paid on 
        presentation.</p>
    <p class="NormalTextBlack">
        3. Goods once sold will Not be exchanged</p>
    <p align="center" class="NormalTextBlack">
        <img id="btnPrint" alt="print" 
            onclick="print_page();javascript:window.print()" src="../Reports/print.jpg" 
            style="width: 45px; cursor: hand;" /></p>
<p align="center" class="NormalTextBlack">
        <span >Subject to Kolkata Jurisdiction.This is a Computer Generated Invoice 
        System Designed &amp; Developed by Industrial Software &amp; System</span></p>
<p align="center" class="NormalTextBlack">
        <span>www.bitscrape.com </span></p>--%>
      </div>  
 </form>
</body>
</html>
