﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;
using System.Data.SqlClient;

public partial class Reports_PurchaseOrderRegister : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");

        }
        if (!IsPostBack)
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                string Sqlquery = string.Format(@"select vendorId,vendorName from Lum_Erp_Vendor order by vendorName");
                DataSet _ds = conn.GetDataSet(Sqlquery, " ddlBind");
                if(_ds.Tables[0].Rows.Count>0)
                {
                    for(int _index=0;_index<_ds.Tables[0].Rows.Count;_index++)
                    {
                        ddlVendor.Items.Add(new ListItem(_ds.Tables[0].Rows[_index]["vendorName"].ToString(), _ds.Tables[0].Rows[_index]["vendorId"].ToString()));
                    }
                }
            }
           
        }
        lblMsg.Text = "";
        dgPORegister.DataSource = null;
    }
    public void bindGrid()
    {
        try
        {
            string fromDate = Convert.ToDateTime(txtFromDate.Text).ToString("dd MMM yyyy");
            // DateTime fromDate = DateTime.ParseExact(txtFromDate.Text, "dd/MM/yyyy", null);
            string toDate = Convert.ToDateTime(txtToDate.Text).ToString("dd MMM yyyy");
            int vendor = Convert.ToInt32(ddlVendor.Text);
            lblVendor.Text = ddlVendor.SelectedItem.ToString();
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                string Sqlquery = string.Empty;
                if (vendor == 0)
                {
                    Sqlquery = string.Format(@"SELECT Q1.vendorName,Q1.activityDateTime,Q1.strPoId,Q1.itemCode,Q1.quantity,CAST( Q1.ValBefore AS DECIMAL(18,2)) AS ValBefore,CAST(Q1.Vat AS DECIMAL(18,2)) AS VAT ,
                                                CAST((Q1.ValBefore+Q1.Vat) AS DECIMAL(18,2)) AS VaLAfter,Q1.strGrnId
                                                FROM(
                                                SELECT V.vendorName,POA.activityDateTime,PO.strPoId,I.itemCode,POD.quantity,(POD.quantity*POD.price) AS ValBefore,
                                                ((POD.quantity*POD.price)/100*( SELECT Vat
							                        FROM [dbo].[_Lum_Erp_VatDetails]
								                    Where 
								                    ((POA.activityDateTime>=FromDate AND POA.activityDateTime< =ToDate)) AND itemType=I.itemtype)) AS Vat,G.strGrnId
                                                FROM
                                                Lum_Erp_Vendor V,Lum_Erp_PurchaseOrderActivityLog POA,Lum_Erp_PurchaseOrder PO,Lum_Erp_PurchaseOrderDetails POD,Lum_Erp_Item I,Lum_Erp_Grn G
                                                WHERE
                                                V.vendorId=PO.vendorId AND PO.poId=POA.poId AND POA.activityTypeId=1 AND POA.poId=POD.poId 
                                                AND POD.itemId=I.itemId AND G.poId=POA.poId )AS Q1
                                                WHERE Q1.activityDateTime >='{0}' AND Q1.activityDateTime<=dateadd(d,1,'{1}')", fromDate, toDate);
                }
                else
                {
                    Sqlquery = string.Format(@"SELECT Q1.vendorName,Q1.activityDateTime,Q1.strPoId,Q1.itemCode,Q1.quantity,CAST( Q1.ValBefore AS DECIMAL(18,2)) AS ValBefore,CAST(Q1.Vat AS DECIMAL(18,2)) AS VAT,
                                                CAST((Q1.ValBefore+Q1.Vat) AS DECIMAL(18,2)) AS VaLAfter,Q1.strGrnId
                                                FROM(
                                                SELECT V.vendorName,POA.activityDateTime,PO.strPoId,I.itemCode,POD.quantity,(POD.quantity*POD.price) AS ValBefore,
                                                ((POD.quantity*POD.price)/100*( SELECT Vat
							                        FROM [dbo].[_Lum_Erp_VatDetails]
								                    Where 
								                    ((POA.activityDateTime>=FromDate AND POA.activityDateTime< =ToDate)) AND itemType=I.itemtype)) AS Vat,G.strGrnId
                                                FROM
                                                Lum_Erp_Vendor V,Lum_Erp_PurchaseOrderActivityLog POA,Lum_Erp_PurchaseOrder PO,Lum_Erp_PurchaseOrderDetails POD,Lum_Erp_Item I,Lum_Erp_Grn G
                                                WHERE
                                                V.vendorId=PO.vendorId AND V.vendorId='{0}' AND PO.poId=POA.poId AND POA.activityTypeId=1 AND POA.poId=POD.poId 
                                                AND POD.itemId=I.itemId AND G.poId=POA.poId )AS Q1
                                                WHERE activityDateTime>='{1}' AND Q1.activityDateTime<=dateadd(d,1,'{2}')", vendor, fromDate, toDate);
                }

                DataSet _Ds = conn.GetDataSet(Sqlquery, "ReportBind");
                dgPORegister.DataSource = _Ds;
                if (_Ds.Tables[0].Rows.Count != 0)
                {
                    dgPORegister.DataBind();

                    btnexpToExcel.Enabled = true;
                }
                else
                    btnexpToExcel.Enabled = false;
                
            }
        }
        catch (Exception ex)
        {
            //Response.Write(ex.Message);
            //if (Convert.ToString(ex.Message) == "String was not recognized as a valid DateTime.")
            //{
            //    Page.RegisterClientScriptBlock("message", "<script>alert('Date was not in correct Format...');</script>");
            //    //lblMsg.Text = "Date was not in correct Format";
            //}
            //else
            //{
            lblMsg.Text = Convert.ToString(ex.Message);
            //}

        }
    }
    protected void btnReport_Click(object sender, EventArgs e)
    {
        bindGrid();
    }
    protected void btnexptoexcel_Click(object sender, EventArgs e)
    {
        
        GridViewExportUtil.Export("PurchaseOrderRegister.xls",dgPORegister);



    }

    protected void dgPORegister_RowCreated(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType == DataControlRowType.Header)
        //{
        //    //Build custom header.
        //    GridView oGridView = (GridView)sender;
        //    GridViewRow oGridViewRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);
        //    TableCell oTableCell = new TableCell();

        //    //Add Department
        //    oTableCell.Text = "Department";
        //    oTableCell.ColumnSpan = 2;
        //    oGridViewRow.Cells.Add(oTableCell);

        //    //Add Employee
        //    oTableCell = new TableCell();
        //    oTableCell.Text = "Employee";
        //    oTableCell.ColumnSpan = 3;
        //    oGridViewRow.Cells.Add(oTableCell);
        //    oGridView.Controls[0].Controls.AddAt(0, oGridViewRow);
        //}
    }
}
