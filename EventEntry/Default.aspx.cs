﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Reporting.WebForms;
using Bitscrape.AppBlock.Database;

public partial class EventEntry_Default : BasePage
{
    protected int _poId = -1;
    protected int _poStateId = -1;
    private double totalAmount = 0;
    private double totalVATAmount = 0;
    private double GrandtotalAmount = 0;
    private double totalwithtax = 0;
    bool isdeleted = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        //if (!string.IsNullOrEmpty(Request["PurchaseOrderId"]))
        //{
        _poId = 74;// int.Parse(Request["PurchaseOrderId"]);
       // }

        if (ViewState["previousPage"] == null)
        {
            ViewState["previousPage"] = Convert.ToString(Request.UrlReferrer);
        }

        if (!this.IsPostBack)
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                SpParam _isdeleted1 = new SpParam("@isdeleted", 0, SqlDbType.Int);
                SpParam _poId1 = new SpParam("@_poId", _poId, SqlDbType.Int);
                DataSet _Ds = new DataSet();
                _Ds = conn.GetDataSet("Lum_Erp_Stp_GetPOforPrint", _isdeleted1, _poId1);

                ReportDataSource datasource = new
                        ReportDataSource("DataSet2_Lum_Erp_Stp_GetPOforPrint", _Ds.Tables[0]);

                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(datasource);
                if (_Ds.Tables[0].Rows.Count == 0)
                {
                    //lblMessage.Text = "Sorry, no products under this category!";
                }
            }
            
            ReportViewer1.LocalReport.Refresh();

        }
    }
   
}
