﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EventEntry/EventEntry.master" AutoEventWireup="true" CodeFile="GrnList.aspx.cs" Inherits="EventEntry_GrnList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="PageMargin">
<%--<table>
<tr><td><asp:Label ID="message" runat="server" ></asp:Label> </td></tr>
<tr><td>  <asp:RadioButtonList ID="rbGrnType" RepeatDirection="Horizontal" runat="server" 
                             >
                      <asp:ListItem Selected="True" Value="1">With Reference To PO</asp:ListItem>
                        <asp:ListItem Value="0">Without Reference To PO</asp:ListItem>
                  
                  </asp:RadioButtonList> </td><td> <asp:Button ID="newGrn" runat="server" Text="Create New" 
        onclick="newGrn_Click"/></td></tr></table>--%>
    <div class="PageHeading">Existing GRNs</div>
    <div class="Line"></div>
    <div class="PageContent">
    <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
    
    <tr><td width="35%" align="right" valign="middle">
            <asp:RadioButtonList ID="rbGrnType" RepeatDirection="Horizontal" runat="server" 
                CssClass="TextAreaLarge" >
                <asp:ListItem Selected="True" Value="1">With Reference To PO</asp:ListItem>
                <asp:ListItem Value="0">Without Reference To PO</asp:ListItem>
            </asp:RadioButtonList>
        </td>
        <td width="65%" align="left" valign="middle">
            <asp:Button ID="newGrn" runat="server" Text="Create New" onclick="newGrn_Click" CssClass="ButtonBig"/>
        </td>
        <td>
        <asp:CheckBox ID="Chkblock_despath" runat="server" Text="Show Blocked" /></td>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <td>
        <asp:Button ID="btngo" runat="server" Text="GO" CssClass="ButtonBig" onclick="btngo_Click"  /></td>       
        

    </tr>
    
    <tr><td colspan="2"><asp:Label ID="message" runat="server" ></asp:Label></td></tr>
    </table>
    </div>
    <div class="Line"></div>
    <asp:DataGrid ID="grnList" runat="server" AutoGenerateColumns="false" EnableViewState="false"
        CellPadding="0" HeaderStyle-CssClass="GridHeading" GridLines="None" Width="100%" 
        ItemStyle-CssClass="GridData" onitemcommand="grnList_ItemCommand" 
        onitemcreated="grnList_ItemCreated" onprerender="grnList_PreRender">
        <Columns>
            <asp:BoundColumn DataField="strGrn" HeaderText="Grn Number"
             HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true" />
            <asp:TemplateColumn>
            <ItemTemplate>
                <input type="hidden" id="grnId" value='<%#DataBinder.Eval(Container.DataItem, "grnId") %>' runat="server" />
            </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="strPoId" HeaderText="PO Number"
             HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true"/>
            <asp:TemplateColumn Visible="false">
            <ItemTemplate>
                <input type="hidden" id="poId" value='<%#DataBinder.Eval(Container.DataItem, "poId") %>' runat="server" />
            </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="dateCreated" HeaderText="Date Created"
             HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true"/>
             <asp:TemplateColumn ItemStyle-Width="10%">
            <ItemTemplate>
                <a href='<%#GetUrlView(DataBinder.Eval(Container.DataItem, "createdByUser"),DataBinder.Eval(Container.DataItem, "grnId"))%>' class="GridLink">View</a>
            </ItemTemplate>
            </asp:TemplateColumn>
             
            <asp:TemplateColumn ItemStyle-Width="10%">
            <ItemTemplate>
                <a href='<%#GetUrl(DataBinder.Eval(Container.DataItem, "createdByUser"),DataBinder.Eval(Container.DataItem, "grnId"))%>' class="GridLink">Edit</a>
            </ItemTemplate>
            </asp:TemplateColumn>
            <asp:ButtonColumn ButtonType="LinkButton" CommandName="Delete" Text="Block"
             ItemStyle-CssClass="GridLink" ItemStyle-Width="10%" />
             
                 <asp:TemplateColumn Visible="false">
              <ItemTemplate>
                <input type="hidden" id="isdeleted" value='<%#DataBinder.Eval(Container.DataItem, "isdeleted") %>' runat="server" />
            </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn Visible="false">
              <ItemTemplate>
                <input type="hidden" id="isLocked" value='<%#DataBinder.Eval(Container.DataItem, "isLocked") %>' runat="server" />
            </ItemTemplate>
            </asp:TemplateColumn>                      
       </Columns>
    </asp:DataGrid>
    <div class="Line"></div>
</div>
</asp:Content>

