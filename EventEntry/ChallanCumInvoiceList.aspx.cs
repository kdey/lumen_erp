﻿using System.Data;
using System;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;

public partial class EventEntry_ChallanCumInvoiceList : BasePage
{
    int challanId = -1;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!IsPostBack)
        {
            BindGrid();
            //if (GetLoggedinUser().UserType == 2)
            //{
            //    newChallan.Visible = false;
            //}

            
        }

    }
    private void BindGrid()
    {
        string chkcondition = string.Empty;
        if (Chkblock_despath.Items[0].Selected == false)
            chkcondition = " AND CI.isDispatch =0 ";
        else
            chkcondition = " AND CI.isDispatch = 1 ";
        if (Chkblock_despath.Items[1].Selected == false)
            chkcondition += " AND CI.isdeleted =0 " ;
        else
            chkcondition += " AND CI.isdeleted =1 ";

        if (Chkblock_despath.Items[2].Selected == true)
        {
            chkcondition = "";
            Chkblock_despath.Items[0].Selected = false;
            Chkblock_despath.Items[1].Selected = false;
        }

        chkcondition = @"SELECT S.strSoirId,S.createdByUser as chType,C.customerName,
                Ci.isDispatch,CI.challanId,Ci.strChallanId +'/'+cast(challanVersion as varchar(5)) as strChallanId,ci.isdeleted FROM Lum_Erp_ChallancumInvoice CI 
                Inner join Lum_Erp_SOIR S on S.soirId=CI.soirId 
                Inner join Lum_Erp_Customer C on C.customerId=S.customerId WHERE CI.challanId >=1"
                +
                chkcondition
                +
                @"order by isDispatch,case when ci.isdeleted =1 then 0 else 1 end,CI.strchallanId desc  ";
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(chkcondition ))
            {
                challanGrid.DataSource = reader;
                challanGrid.DataBind();
            }
        }

    }
    protected void challanGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            //using (IDataReader reader = conn.ExecuteQuery("select * from AdminUser where userName= 'demo'"))
            //{
            //    while (reader.Read())
            //    {
            //        AdminUser s = GetLoggedinUser();
            //        int a = s.UserId;
            //        if (a != Convert.ToInt32(reader["adminuserId"]))
            //        {
                        if (e.CommandName.Equals("Edit"))
                        {
                            challanId = (int)challanGrid.DataKeys[e.Item.ItemIndex];
                            string _type = ((HtmlInputHidden)e.Item.FindControl("Challantype")).Value;
                            Response.Redirect(GetUrl(_type, challanId));

                        }
                        if (e.CommandName.Equals("View"))
                        {
                            challanId = (int)challanGrid.DataKeys[e.Item.ItemIndex];
                            string _type = ((HtmlInputHidden)e.Item.FindControl("Challantype")).Value;
                            _type = GetUrl(_type, challanId);
                            Response.Redirect(_type + "&View=View");

                        }

                        else if (e.CommandName.Equals("Block"))
                        {
                            challanId = (int)challanGrid.DataKeys[e.Item.ItemIndex];

                           
                                try
                                {
                                    conn.ExecuteCommandProc("Lum_Erp_Stp_DeleteChallanCumInvoice",
                                    new SpParam("@challanId", challanId, SqlDbType.Int),
                                    new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int));

                                    message.Text = "Challan  has been Blocked";
                                    message.ForeColor = Color.Green;
                                }
                                catch(Exception ex)

                                {
                                    message.Text = ex.Message; //"Challan cannot be deleted because of existing related data";
                                    message.ForeColor = Color.Red;
                                }


                           
                            BindGrid();
                        }
                    }
        //        }
        //    }
        //}
    }
    
    
   
    protected void newChallan_Click1(object sender, EventArgs e)
    {
        if (rbChallanType.SelectedValue=="1")
        {
            Response.Redirect("ChallanWithRespectToSoir.aspx");
        }
        else
        {
             Response.Redirect("ChallanWithoutRespectToSoir.aspx");
        }
    }
    protected void challanGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton editbtn = (LinkButton)e.Item.Cells[4].Controls[0];
            LinkButton btn = (LinkButton)e.Item.Cells[5].Controls[0];
            if (btn.Text != "Block")
            {
                btn.Attributes.Add("onclick", "return confirm('Are you sure you want to Reverse this?')");
            }
            btn.CssClass = "GridLink";
            editbtn.CssClass = "GridLink";
        }
    }
    protected string GetUrl(object type, object challanId)
    {
        string retVal = "";
        bool isFlag = Convert.ToBoolean(type);
        if (isFlag)
        {
            retVal = "ChallanWithRespectToSoir.aspx?challanId=" + challanId;
        }
        else
        {
            retVal = "ChallanWithoutRespectToSoir.aspx?challanId=" + challanId;
        }
        return retVal;

    }
    protected void challanGrid_PreRender(object sender, EventArgs e)
    {
        for (int _index = 0; _index < challanGrid.Items.Count; _index++)
        {
            LinkButton btnview = (LinkButton)challanGrid.Items[_index].Cells[3].Controls[0];
            LinkButton btnedit = (LinkButton)challanGrid.Items[_index].Cells[4].Controls[0];
            LinkButton btn = (LinkButton)challanGrid.Items[_index].Cells[5].Controls[0];
            HtmlInputHidden _hd = (HtmlInputHidden)challanGrid.Items[_index].FindControl("isLocked");
            HtmlInputHidden _hdel = (HtmlInputHidden)challanGrid.Items[_index].FindControl("isdeleted");


            HtmlInputHidden _type = (HtmlInputHidden)challanGrid.Items[_index].FindControl("type");
            if (_hd.Value == "True")
            {
                btnedit.Text = "Despatched";
                btnedit.Enabled = false;
            }
            if (_hdel.Value == "True")
            {
                btn.Text = "Blocked";
                btn.Visible = false;
                if (btnedit.Text != "Despatched")
                {
                    btnedit.Text = "";
                    btnedit.Visible = false;
                }
            }
            else
            {
                if (_hd.Value == "True")
                {
                   // btn.Visible = false;
                }
            }
            if (GetLoggedinUser().UserType == 2)
            {
                btnedit.Visible = false;
                btn.Visible = false;
                btnview.Visible = true;
            }
        }
    }
    protected void btngo_Click(object sender, EventArgs e)
    {
        BindGrid();
    }
}
