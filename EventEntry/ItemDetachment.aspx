﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EventEntry/EventEntry.master" AutoEventWireup="true" CodeFile="ItemDetachment.aspx.cs" Inherits="EventEntry_ItemDetachment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="PageMargin">
        <div class="PageHeading">
          Detach Item <a style="float:right" href="IncompleteItemList.aspx">Incomplete Item List</a></div>
        <div class="Line">
        </div>
        <!--Content Table Start-->
        <div class="PageContent">
         <input type="hidden" name="grndetailsSerialNoId" value='<%=_grndetailsSerialNoId%>' />
              <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
                 <tr>
                 <td width="16%" align="right" valign="top">
                        Select Division
                    </td>
                 <td align="left" valign="top" width="20%">
                        <asp:DropDownList CssClass="TextboxSmall" ID="ddlDivision" AutoPostBack="true" runat="server" 
                            Width="150px"  onselectedindexchanged="ddlDivision_SelectedIndexChanged">
                     </asp:DropDownList>
                    <br />    <asp:RequiredFieldValidator ControlToValidate="ddlDivision" ErrorMessage="Select Division"
                            ID="divisionValidator" InitialValue="Please Select" runat="server" />
                    </td>
                 <td width="16%" align="right" valign="top">
                        Select Subdivision&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span>
                    </td>
                 <td align="left" valign="top" width="16%">
                        <asp:DropDownList CssClass="TextboxSmall" ID="ddlSubdivision" AutoPostBack="true" runat="server" Width="150px" 
                            onselectedindexchanged="ddlSubdivision_SelectedIndexChanged">
                            <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                     </asp:DropDownList>
                    <br />    <asp:RequiredFieldValidator ControlToValidate="ddlSubdivision" ErrorMessage="Select Division"
                            ID="RequiredFieldValidator1" InitialValue="0" runat="server" />
                    </td>
                 <td width="16%" align="right" valign="top">
                       Item Name/Part Code&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span>
                    </td>
                 <td width="70%" align="left" valign="top">
                        <asp:DropDownList CssClass="TextboxSmall" AutoPostBack="true" ID="ddlItem" runat="server" 
                            Width="150px" onselectedindexchanged="ddlItem_SelectedIndexChanged" >
                           <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                     </asp:DropDownList>
                    <br />   
                     <asp:RequiredFieldValidator InitialValue="0" ControlToValidate="ddlItem" ErrorMessage="Select Item Code"
                            ID="RequiredFieldValidator3"  runat="server" />
                    </td>
                </tr>
            <tr>
                       <td valign="top" align="right">
                        Select Store&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span>
                    </td>
                       <td valign="top" align="left">
                        <asp:DropDownList CssClass="TextboxSmall" AutoPostBack="true" ID="ddlStore" 
                            CausesValidation="false" runat="server" 
                            Width="150px" onselectedindexchanged="ddlStore_SelectedIndexChanged" >
                     </asp:DropDownList>
                     <br />
                        <asp:RequiredFieldValidator ControlToValidate="ddlStore" ErrorMessage="Select Store"
                            ID="RequiredFieldValidator2" InitialValue="0" runat="server" />
                    </td>
                       <td valign="top" align="right">
                        Select Serial No#&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span>
                    </td>
                       <td valign="top" align="left">
                        <asp:DropDownList CssClass="TextboxSmall" ID="ddlserialNo"  AutoPostBack="true" runat="server" 
                            Width="150px" onselectedindexchanged="ddlserialNo_SelectedIndexChanged" >
                            <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                     </asp:DropDownList>
                    
                        <asp:Label ID="lblSerialNo" Visible="false" runat="server"></asp:Label>
                     
                     <br />
                        <asp:RequiredFieldValidator ControlToValidate="ddlserialNo" ErrorMessage="Select SerialNo"
                            ID="RequiredFieldValidator4" InitialValue="0" runat="server" />
                    </td>
                       <td valign="top" align="right">
                        &nbsp;</td>
                    <td width="70%" align="left" valign="middle">
                     </td>
                     
                </tr>
                       
                       </table>
                </div>
                <div class="Line">
          
        </div>
        <div class="PageHeading">Detachable parts</div>
       <div class="Line">
       
       
        </div>
               <div class="PageContent">
         <table width="400" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
        <tr>      
                    <td align="center" colspan="2" valign="middle">
                           <asp:DataGrid ID="ditemGrid" runat="server" AutoGenerateColumns="FALSE" 
            CellPadding="0"   HeaderStyle-CssClass="GridHeading"
            ItemStyle-CssClass="GridData" onprerender="ditemGrid_PreRender"   >
            <Columns>
              <asp:TemplateColumn HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                   <input type="hidden" runat="server" id="itemId" value='<%# DataBinder.Eval(Container.DataItem, "itemId") %>' />
                 </ItemTemplate>
                </asp:TemplateColumn>
                
               <asp:TemplateColumn HeaderText="Attached" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                      <asp:CheckBox ID="chkStatus" Checked='<%#DataBinder.Eval(Container.DataItem, "Status") %>' runat="server"  />
                </ItemTemplate>
               </asp:TemplateColumn >
                               
                <asp:TemplateColumn HeaderText="Detachable Parts" ItemStyle-Width="150" HeaderStyle-Font-Bold="true" >
                      <ItemTemplate>
                          <asp:TextBox runat="server" ID="txtdetach" value='<%# DataBinder.Eval(Container.DataItem, "itemCode") %>' >
                          </asp:TextBox>
                      </ItemTemplate>
                </asp:TemplateColumn >
                  
                <asp:TemplateColumn HeaderText="Value" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <asp:TextBox runat="server" ID="txtValue" value='<%# DataBinder.Eval(Container.DataItem, "value") %>'>
                    </asp:TextBox>
                </ItemTemplate>
                </asp:TemplateColumn >
                
                <asp:TemplateColumn  HeaderStyle-HorizontalAlign="Left" >
                     <ItemTemplate>
                      <asp:TextBox runat="server" ID="txtgrndetailsSerialNoId"  value='<%# DataBinder.Eval(Container.DataItem, "grndetailsSerialNoId") %>' 
                       Height="0" Width="0" BorderStyle="none" >
                      </asp:TextBox>           
                     </ItemTemplate>
               </asp:TemplateColumn >
               
                   <asp:TemplateColumn   HeaderStyle-HorizontalAlign="Left"  >
                     <ItemTemplate>
                      <asp:TextBox runat="server" ID="txtsparesId"  value='<%# DataBinder.Eval(Container.DataItem, "sparesId") %>' 
                        Height="0" Width="0" BorderStyle="none">
                      </asp:TextBox>           
                     </ItemTemplate>
               </asp:TemplateColumn >                      
            </Columns>
        </asp:DataGrid>
                    </td>
                </tr>
                 <tr>
                    <td width="30%" align="left" valign="middle">
                        &nbsp;</td>
                    <td width="70%" align="center" valign="middle">
                        <asp:Button ID="add" Text="Submit" runat="server"  CssClass="Button" 
                            onclick="add_Click" Width="77px" /></td>
                </tr>
            </table>
            <asp:Label ID="message" runat="server" />
        </div>
          <!--Content Table End-->
        <div class="Line">
        </div>
        <!--Grid Table Start-->
        
       
        <!--Grid Table End-->
        <div class="Line">
        </div>
        <!--Page Body End-->
    </div>
</asp:Content>

