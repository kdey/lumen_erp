﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EventEntry/EventEntry.master" AutoEventWireup="true" CodeFile="ServiceList.aspx.cs" Inherits="EventEntry_ServiceList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="PageMargin">
      
       
            <asp:Button ID="newServiceList" runat="server" CssClass="ButtonBig" Text="Create New" 
        onclick="newGrn_Click"/>
            <div class="Line"></div>
         <div class="PageHeading">
            Service Bill List&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
            <asp:CheckBox ID="Chkblock_despath" runat="server" Text="Show Reversed" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btngo" runat="server" Text="GO" CssClass="ButtonBig" onclick="btngo_Click"  /> 
        </div>
        <!--Content Table Start-->
       
         <asp:Label ID="message" runat="server" />
         <div class="Line"></div>
               <asp:DataGrid ID="serviceGrid" runat="server" AutoGenerateColumns="False" DataKeyField="serviceId"
            CellPadding="0" OnItemCommand="serviceGrid_ItemCommand" HeaderStyle-CssClass="GridHeading"
            ItemStyle-CssClass="GridData" Width="100%" 
                onitemcreated="serviceGrid_ItemCreated" GridLines="None" 
                onprerender="serviceGrid_PreRender" >
<ItemStyle CssClass="GridData"></ItemStyle>
            <Columns>
                <asp:BoundColumn  HeaderStyle-HorizontalAlign="Left" DataField="serialNo" HeaderText="Serial No" 
                    HeaderStyle-Font-Bold="true" >
<HeaderStyle Font-Bold="True"></HeaderStyle>
                </asp:BoundColumn>
                <asp:BoundColumn  HeaderStyle-HorizontalAlign="Left" DataField="customerName" HeaderText="Customer Name" 
                    HeaderStyle-Font-Bold="true" >
<HeaderStyle Font-Bold="True"></HeaderStyle>
                </asp:BoundColumn>
                 <asp:BoundColumn  HeaderStyle-HorizontalAlign="Left" 
                    DataField="customerOrderNo" HeaderText="Customer  Order No" 
                    HeaderStyle-Font-Bold="true" >
<HeaderStyle Font-Bold="True"></HeaderStyle>
                </asp:BoundColumn>
                <asp:BoundColumn  HeaderStyle-HorizontalAlign="Left" 
                    DataField="servicebillno" HeaderText="Service Bill No" 
                    HeaderStyle-Font-Bold="true" >
<HeaderStyle Font-Bold="True"></HeaderStyle>
                </asp:BoundColumn>
                                
                   <asp:ButtonColumn ButtonType="LinkButton" HeaderStyle-HorizontalAlign="Left" 
                    CommandName="View" Text="View" ItemStyle-CssClass="GridLink" >
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<ItemStyle CssClass="GridLink"></ItemStyle>
                </asp:ButtonColumn>
                <asp:ButtonColumn  HeaderStyle-HorizontalAlign="Left" ButtonType="LinkButton" CommandName="Edit" Text="Edit" 
                    ItemStyle-CssClass="GridLink" >
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<ItemStyle CssClass="GridLink"></ItemStyle>
                </asp:ButtonColumn>
                <asp:ButtonColumn  HeaderStyle-HorizontalAlign="Left" ButtonType="LinkButton" CommandName="Delete" Text="Reverse" 
                    ItemStyle-CssClass="GridLink" >
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<ItemStyle CssClass="GridLink"></ItemStyle>
                </asp:ButtonColumn>
                 <asp:TemplateColumn Visible="false">
              <ItemTemplate>
                <input type="hidden" id="isdeleted" value='<%#DataBinder.Eval(Container.DataItem, "isdeleted") %>' runat="server" />
            </ItemTemplate>
            </asp:TemplateColumn>                
            </Columns>

<HeaderStyle CssClass="GridHeading"></HeaderStyle>
        </asp:DataGrid>
        
        <!--Grid Table Start-->
     
        <!--Grid Table End-->
        <div class="Line">
        </div>
        <!--Page Body End-->
    </div>
</asp:Content>

