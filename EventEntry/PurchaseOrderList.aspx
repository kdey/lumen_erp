﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EventEntry/EventEntry.master" AutoEventWireup="true"
    CodeFile="PurchaseOrderList.aspx.cs" Inherits="EventEntry_PurchaseOrderList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="PageMargin">
    <asp:Button ID="newPo" runat="server" Text="Create New" onclick="newPo_Click" CssClass="ButtonBig" />
      
    <div class="PageHeading">Existing Purchase Orders&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:CheckBox ID="Chkblock_despath" runat="server" Text="Show Blocked" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btngo" runat="server" Text="GO" CssClass="ButtonBig" onclick="btngo_Click"  />    
        
    </div>
    
    <div class="Line"></div>
    <table>
   <tr><td colspan="2"><asp:Label ID="message" runat="server" ></asp:Label></td></tr> 
    </table>
    <asp:DataGrid ID="poList" runat="server" AutoGenerateColumns="false" EnableViewState="false"
        CellPadding="0" HeaderStyle-CssClass="GridHeading" 
        ItemStyle-CssClass="GridData" GridLines="None" Width="100%" DataKeyField="poId"
        onitemcreated="poList_ItemCreated" onitemcommand="poList_ItemCommand" onprerender="poList_PreRender" >
        <Columns>
            <asp:BoundColumn DataField="strPoId" HeaderText="PO Number" HeaderStyle-Font-Bold="true"
                 HeaderStyle-HorizontalAlign="Left" />
            <asp:BoundColumn DataField="poDate" HeaderText="Date" HeaderStyle-Font-Bold="true"
                 HeaderStyle-HorizontalAlign="Left"/>
            <asp:BoundColumn DataField="vendorName" HeaderText="Vendor" HeaderStyle-Font-Bold="true"
                 HeaderStyle-HorizontalAlign="Left"/>
                     <asp:ButtonColumn ButtonType="LinkButton" HeaderStyle-HorizontalAlign="Left" CommandName="View" Text="View" ItemStyle-CssClass="GridLink" />
              <asp:ButtonColumn ButtonType="LinkButton" HeaderStyle-HorizontalAlign="Left" CommandName="Edit" Text="Edit" ItemStyle-CssClass="GridLink" />
           
            <asp:ButtonColumn ButtonType="LinkButton" CommandName="Delete" Text="Block"
             ItemStyle-CssClass="GridLink" ItemStyle-Width="10%" />
            <asp:TemplateColumn Visible="false">
            <ItemTemplate>
                <input type="hidden" id="poId" value='<%#DataBinder.Eval(Container.DataItem, "poId") %>' runat="server" />
            </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn Visible="false">
              <ItemTemplate>
                <input type="hidden" id="isLocked" value='<%#DataBinder.Eval(Container.DataItem, "isBlocked") %>' runat="server" />
            </ItemTemplate>
            </asp:TemplateColumn>
                 <asp:TemplateColumn Visible="false">
              <ItemTemplate>
                <input type="hidden" id="isdeleted" value='<%#DataBinder.Eval(Container.DataItem, "isdeleted") %>' runat="server" />
            </ItemTemplate>
            </asp:TemplateColumn>
           <asp:TemplateColumn Visible="false">
              <ItemTemplate>
                <input type="hidden" id="poStateId" value='<%#DataBinder.Eval(Container.DataItem, "poStateId") %>' runat="server" />
            </ItemTemplate>
            </asp:TemplateColumn>                    
       </Columns>
    </asp:DataGrid>
    <div class="Line"></div>
</div>
</asp:Content>
