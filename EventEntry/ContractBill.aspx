﻿<%@ Page Language="C#" MasterPageFile="~/EventEntry/EventEntry.master" AutoEventWireup="true" CodeFile="ContractBill.aspx.cs" Inherits="EventEntry_ContractBill" Title="Lumen Erp...CSA Billing" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script type="text/javascript">
    function isNumberKey_WO_Decimal(evt)   //Checks textbox contains only numeric value
    {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) 
        {
            return false;
        }
        return true;
    }
</script>
<div class="PageMargin">
    <div class="PageHeading">Bill Generation </div>
    <div class="Line"><asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager></div>
    <!--Content Table Start-->
    <div class="PageContent">
        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
        <tr><td align="right" valign="top" width="20%">Lookup Machine by Serial No&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td align="left" valign="top" width="15%"><asp:TextBox ID="txtSRNo" runat="server" 
                    CssClass="TextboxSmall" MaxLength="50" /></td>
            <td align="left" valign="top" width="15%"><asp:Button ID="lookup" runat="server" Text="Lookup" OnClick="lookup_Click"
                 CssClass="Button" CausesValidation="false" /></td>
            <td colspan="3" align="left" valign="top" width="50%"><input type="hidden" id="CustMacId" runat="server" />
                <asp:Label ID="lookupMessage" runat="server" /></td>
        </tr></table>
        <asp:Panel ID="Panel1" runat="server" Visible="false">
            <hr />
            <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
            <tr><td align="right" valign="top" width="15%"><b>Customer Name :</b></td>
                <td align="left" valign="top" width="35%"><asp:Label ID="customerName" runat="server" /></td>
                <td align="right" valign="top" width="15%"><b>Machine Model :</b></td>
                <td align="left" valign="top" width="35%"><asp:Label ID="machineModel" runat="server" /></td>
            </tr>
            <tr><td align="right" valign="top" width="15%"><b>Serial Number :</b></td>
                <td align="left" valign="top" width="35%"><asp:Label ID="serialNo" runat="server" /></td>
                <td align="right" valign="top" width="15%"><b>Agreement Number :</b></td>
                <td align="left" valign="top" width="35%"><asp:Label ID="agreeNo" runat="server" /></td>
            </tr></table>
            <hr />
            <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
            <tr><td align="right" valign="top" width="25%">
                    <b>Current Meter Reading :&nbsp;</b></td>
                <td align="left" valign="top" width="25%">
                    <asp:TextBox ID="txtCMR" runat="server" onkeypress="return isNumberKey_WO_Decimal(event);" CssClass="TextboxSmall"
                     MaxLength="12"></asp:TextBox><br />
                    </td>
                <td align="right" valign="top" width="25%">
                    <b>Current Meter Reading Date :&nbsp;&nbsp;<br />(dd mmm yyyy)&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
                <td align="left" valign="top" width="25%">
                    <asp:TextBox ID="txtCMRDate" onkeyup="document.getElementById(this.id).value=''" autocomplete='off' CssClass="TextboxSmall"
                     runat="server" />
                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd MMM yyyy" TargetControlID="txtCMRDate"></cc1:CalendarExtender>
                    <br /></td>
            </tr>
            <tr><td align="center" valign="middle" colspan="4">
                    <asp:Button ID="add" Text="Generate New CSA Bill" runat="server" OnClick="add_Click" CssClass="Button" Width="161px" />&nbsp;&nbsp;
                    <asp:Button ID="cancel" Text="Cancel" runat="server" OnClick="cancel_Click" CausesValidation="false" CssClass="Button" /></td>
            </tr></table>
            <hr />
            <asp:DataGrid ID="gvCSA" runat="server" AutoGenerateColumns="False" 
                DataKeyField="custMacId" CellPadding="0"
             HeaderStyle-CssClass="GridHeading" GridLines="None" Width="100%" 
                RowStyle-CssClass="GridData" 
                onitemcommand="gvCSA_ItemCommand" CssClass="GridData">
               
                <Columns>
                  <asp:BoundColumn DataField="currMeterReading" HeaderText="Meter Reading" HeaderStyle-Font-Bold="true"
                     HeaderStyle-HorizontalAlign="left" >
                        <HeaderStyle Font-Bold="True" HorizontalAlign="Left" />
                    </asp:BoundColumn>
                  <asp:BoundColumn DataField="ServiceBillNo" HeaderText="Meter Reading" HeaderStyle-Font-Bold="true"
                     HeaderStyle-HorizontalAlign="left" >
                        <HeaderStyle Font-Bold="True" HorizontalAlign="Left" />
                    </asp:BoundColumn>

                    
                    <asp:BoundColumn DataField="currMeterDate" HeaderText="Reading Date" HeaderStyle-Font-Bold="true"
                     HeaderStyle-HorizontalAlign="left" >
                        <HeaderStyle Font-Bold="True" HorizontalAlign="Left" />
                    </asp:BoundColumn>
                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Left" HeaderText="Edit">
                        <ItemTemplate>
                            <a class="GridLink" href='CSABill.aspx?Id=<%#DataBinder.Eval(Container.DataItem,"Id") %>'>CSA Bill Edit</a>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateColumn>   
                    <asp:ButtonColumn HeaderText="Action" CommandName="Block" Text="Block" ButtonType="LinkButton" ItemStyle-CssClass="GridLink" >
                        <ItemStyle CssClass="GridLink" />                       
                    </asp:ButtonColumn>            

                 <asp:TemplateColumn>
                  <ItemTemplate>
                    <input type="hidden" id="CSABillId" value='<%#DataBinder.Eval(Container.DataItem, "CSABillId") %>' runat="server" />
                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn >
                  <ItemTemplate>
                    <input type="hidden" id="PaymentDode" value='<%#DataBinder.Eval(Container.DataItem, "PaymentDode") %>' runat="server" />
                </ItemTemplate>
                </asp:TemplateColumn>  
                                
                </Columns>
                <HeaderStyle CssClass="GridHeading" />
                </asp:DataGrid> 
        </asp:Panel>
        <asp:Label ID="message" runat="server" ></asp:Label>
    </div>
    <!--Content Table End-->
    <div class="Line"></div>
    <!--Page Body End-->
</div>
</asp:Content>

