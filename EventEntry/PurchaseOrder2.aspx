<%@ Page Title="Purchase Order" Culture="en-GB" Language="C#" MasterPageFile="~/EventEntry/EventEntry.master"
    AutoEventWireup="true" CodeFile="PurchaseOrder2.aspx.cs" Inherits="EventEntry_PurchaseOrder2"  EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <input type="hidden" name="poId" value='<%=_poId%>' />
    <input type="hidden" name="poStateId" value='<%=_poStateId%>' />
        <input type="hidden" name="poVersion" value='<%=poVersion%>' />

    
 
        <div class="PageContent">
        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
        <tr><td width="16%" align="right" valign="middle">Purchase Order Number</td>
            <td align="left" valign="middle" width="20%"><asp:Label ID="poNumber" runat="server" /></td>
            <td width="16%" align="right" valign="middle">Purchase Order Type</td>
            <td align="left" valign="middle" width="16%"><asp:DropDownList ID="poTypeDdl" runat="server" CssClass="DropdownExtraSmall" /></td>
            <td width="16%" align="right" valign="middle">Purchase Order Date&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span><br /> (dd/MM/yyyy)</td>
            <td align="left" valign="middle" width="16%"><asp:TextBox ID="poDate" onkeyup="document.getElementById(this.id).value=''" autocomplete='off' runat="server" />
                <cc1:calendarextender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="poDate">
                </cc1:calendarextender><br />
                    <asp:RequiredFieldValidator ControlToValidate="poDate"   ErrorMessage="Field cannot be empty"
                    ID="RequiredFieldValidator2" runat="server" />
            </td>
        </tr>
        <tr><td align="right" valign="middle">Vendor Company</td>
            <td align="left" valign="middle"><asp:DropDownList ID="vendorDdl" runat="server" 
                    CssClass="DropdownLarge" Width="300px" 
                    onselectedindexchanged="vendorDdl_SelectedIndexChanged" 
                    AutoPostBack="True" /></td>
            <td align="right" valign="middle">Rate Contract Type</td>
            <td align="left" valign="middle"><asp:DropDownList ID="rateContractDdl" runat="server" CssClass="DropdownExtraSmall"  /></td>
            <td align="right" valign="middle">Vendor Address</td>
            <td > 
                        <asp:TextBox ID="txtvendoradd" runat="server" ReadOnly="True"  CssClass="TextAreaMiddle"
                        TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        </table></div>
        <div class="Line"></div>
         <div><asp:Label ID="message" runat="server" /></div>
        <div class="PageHeading">New Purchase Order Line</div>
        <div class="Line"></div>
        <div class="PageContent">
        <div id="tblItem" runat="server">
        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
        <tr><td width="19%" align="center" valign="middle">Division&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</td>
            <td width="10%" align="center" valign="middle">SubDivision&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</td>
            <td width="10%" align="center" valign="middle">Item&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</td>
            <td width="20%" align="center" valign="middle">Item Description</td>
            <td width="14%" align="center" valign="middle">Quantity&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</td>
            <td width="9%" align="center" valign="middle">Price&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</td>
            <td width="9%" align="center" valign="middle">VAT</td>
            <td width="9%" align="center" valign="middle">&nbsp;</td>
        </tr>
        <tr><td align="center" valign="middle">
                <asp:DropDownList ID="divisionDdl" runat="server"  CssClass="DropdownSmall" Width="100%"  />
            </td>
            <td align="center" valign="middle">
                <asp:DropDownList ID="subDivisionDdl" runat="server"  CssClass="DropdownExtraSmall"
                Width="100%"  />
            </td>
            <td align="center" valign="middle">
                <asp:DropDownList ID="itemDdl" runat="server" AutoPostBack="true" CssClass="DropdownLarge"
                 Width="100%" OnSelectedIndexChanged="itemDdl_SelectedIndexChanged" />
            </td>
            <td>
         <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" 
                     RenderMode="Inline">
                     <ContentTemplate>
                        <asp:TextBox ID="itemDescription" runat="server" ReadOnly="True" CssClass="TextAreaMiddle"
                        TextMode="MultiLine"></asp:TextBox>
                </ContentTemplate>        
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="itemDdl" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>  
         </td>            

            <td align="center" valign="middle">
                <asp:TextBox ID="quantity" runat="server" CssClass="Textbox2ExtraSmall" 
                    MaxLength="6" /><cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="quantity" ValidChars="1234567890." FilterMode="ValidChars" FilterType="Custom"></cc1:FilteredTextBoxExtender>
                    &nbsp;<asp:Label ID="uom" runat="server" />
            </td>
            <td align="center" valign="middle">
                <asp:TextBox ID="price" runat="server" CssClass="Textbox2ExtraSmall" 
                    Width="100%" MaxLength="10" />
               <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="price" ValidChars="1234567890." FilterMode="ValidChars" FilterType="Custom"></cc1:FilteredTextBoxExtender>     
            </td>
            <td align="center" valign="middle"><asp:Label ID="vat" runat="server" /></td>
            <td align="center" valign="middle">
               
                <asp:Button ID="addItemLine" runat="server" Text="Add" OnClick="addItemLine_Click"
                    ValidationGroup="addItem" CausesValidation="true" CssClass="Button" />
            </td>
            <td>
  </td>
       <cc1:CascadingDropDown ID="CascadingDropDown3"
            runat="server" 
            TargetControlID="divisionDdl" LoadingText="[Loading...]"
            Category="Division"
            PromptText="Select a Division"
            ServicePath ="~/divisionService.asmx"
            ServiceMethod="GetDivision" >   </cc1:CascadingDropDown>
              
       <cc1:CascadingDropDown ID="CascadingDropDown1"
            runat="server" ParentControlID = "divisionDdl" 
            TargetControlID="subDivisionDdl" LoadingText="[Loading...]"
            Category="subDivision"
            PromptText="Select a subDivision"
            ServicePath ="~/divisionService.asmx"
            ServiceMethod="GetSubdividsionForDivision" >   </cc1:CascadingDropDown>

      <cc1:CascadingDropDown ID="CascadingDropDown2"
            runat="server" ParentControlID = "subDivisionDdl" 
            TargetControlID="itemDdl" LoadingText="[Loading...]"
            Category="Item"
            PromptText="Select Item"
            ServicePath ="~/divisionService.asmx"
            ServiceMethod="GetItemForSubdividsion" >   </cc1:CascadingDropDown>            
        </tr>
        <tr><td colspan="4">                            

            </td>
            <td><asp:RequiredFieldValidator ID="quantityRequired" runat="server" EnableClientScript="true"
                    Display="None" ValidationGroup="addItem" ErrorMessage="Quantity cannot be empty."
                    Text="" ControlToValidate="quantity" />
                <asp:RegularExpressionValidator ID="quanityRegexValidator" runat="server" EnableClientScript="true"
                    Display="None" ValidationGroup="addItem" ErrorMessage="Quantity must be a integer"
                    Text="" ControlToValidate="quantity" ValidationExpression="\d+" /></td>
            <td><asp:RequiredFieldValidator ID="priceRequired" runat="server" EnableClientScript="true"
                    Display="None" ValidationGroup="addItem" ErrorMessage="Price cannot be empty."
                    Text="" ControlToValidate="price" />
                <%--<asp:RegularExpressionValidator ID="priceRegexValidator" runat="server" EnableClientScript="true"
                    Display="None" ValidationGroup="addItem" ErrorMessage="Invalid price"
                    Text="" ControlToValidate="price" ValidationExpression="^\d*\.?\d{2}$" />--%></td>
            <td colspan="2">&nbsp;</td>
        </tr><asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="Please correct the errors below:"
            ValidationGroup="addItem" DisplayMode="BulletList" />
            </table>
          
        </div></div>
       
        <div class="Line">
        </div>
        <%--<b>Purchase Order Items</b>--%>
    <!-- over here we should the bottom grid where the line details of this purchase order are shown -->
    <!--Grid Table Start-->
    <div class="PageHeading">Purchase Order Items</div>
       <%-- <%#DataBinder.Eval(Container.DataItem, "uomName")%>--%>
    <div class="Line"></div>
    <asp:DataGrid ID="poLineGrid" runat="server" AutoGenerateColumns="False"
        DataKeyField="poLineId" CellPadding="0" HeaderStyle-CssClass="GridHeading" ItemStyle-CssClass="GridData" 
        onitemcommand="poLineGrid_ItemCommand" onitemcreated="poLineGrid_ItemCreated" 
            GridLines="None" Width="100%" onitemdatabound="poLineGrid_ItemDataBound" 
                    onprerender="poLineGrid_PreRender">
<ItemStyle CssClass="GridData"></ItemStyle>
        <Columns>
            <asp:BoundColumn DataField="divisionName" HeaderText="Division" HeaderStyle-HorizontalAlign="Left"
             HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Left" >
<HeaderStyle HorizontalAlign="Left" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
            </asp:BoundColumn>
            <asp:BoundColumn DataField="subdivisionName" HeaderText="Sub Division" HeaderStyle-HorizontalAlign="Left"
             HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Left" >
<HeaderStyle HorizontalAlign="Left" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
            </asp:BoundColumn>
            <asp:BoundColumn DataField="itemCode" HeaderText="Item Name"></asp:BoundColumn>
            
           <asp:TemplateColumn HeaderText="Item Description" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Left"
                     ItemStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:TextBox ID="itemDescription" ReadOnly ="true" height = "50px" Width="175px" TextMode="MultiLine" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "itemDescription") %>' CssClass="Textbox2ExtraSmall_wo_border"  />
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" Font-Bold="True" Width="75px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
            </asp:TemplateColumn>
            
            <asp:TemplateColumn HeaderText="Quantity" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Left"
             ItemStyle-HorizontalAlign="Left">
            <ItemTemplate>
                <asp:TextBox ID="itemQty" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "quantity") %>' CssClass="Textbox2ExtraSmall" />
                <%#DataBinder.Eval(Container.DataItem, "uomName")%>
            </ItemTemplate>

<HeaderStyle HorizontalAlign="Left" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Price" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Left"
             ItemStyle-HorizontalAlign="Left">
            <ItemTemplate>
                <asp:TextBox ID="itemPrice" Width="75px" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "price") %>' CssClass="Textbox2ExtraSmall" />
            </ItemTemplate>

<HeaderStyle HorizontalAlign="Left" Font-Bold="True" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="vat" HeaderText="Vat %" HeaderStyle-HorizontalAlign="Left"
             HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Left" >
<HeaderStyle HorizontalAlign="Left" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
            </asp:BoundColumn>
            <asp:BoundColumn DataField="vatAmount" HeaderText="Vat Amount" HeaderStyle-HorizontalAlign="Left"
             HeaderStyle-Font-Bold="true" DataFormatString="{0:N}" 
                ItemStyle-HorizontalAlign="Left" >
<HeaderStyle HorizontalAlign="Left" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
            </asp:BoundColumn>
                 <asp:BoundColumn DataField="total" HeaderStyle-HorizontalAlign="Left" 
                ItemStyle-HorizontalAlign="Left"  HeaderText="Net Value" 
                HeaderStyle-Font-Bold="true" DataFormatString="{0:N}"  >
<HeaderStyle HorizontalAlign="Left" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
            </asp:BoundColumn>
            <asp:ButtonColumn ButtonType="PushButton" Text="Update" CommandName="Update" 
                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%" >
<ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
            </asp:ButtonColumn>
            <asp:ButtonColumn ButtonType="PushButton" Text="Delete"  CommandName="Delete" 
                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%" >
<ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
            </asp:ButtonColumn>
            
            
                 <asp:TemplateColumn Visible="false">
              <ItemTemplate>
                <input type="hidden" id="isDeleted" value='<%#DataBinder.Eval(Container.DataItem, "isDeleted") %>' runat="server" />
            </ItemTemplate>
            </asp:TemplateColumn>            
        </Columns>

<HeaderStyle CssClass="GridHeading"></HeaderStyle>
    </asp:DataGrid>
    <asp:Label ID="gridMessage" runat="server" />
    &nbsp;<asp:Button ID="btnPrint" runat="server" CssClass="Button" 
                onclick="btnPrint_Click" Text="Print" />
    <div class="Line">
              </div>
              
            <div class="PageContent">  
              <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
              <tr><td class="GridData" align="right"> Total Quantity :   <asp:Label ID="lblquantity" runat="server"></asp:Label></td><td class="GridData">
                     </td><td width="50px" class="GridData" align="right">Total</td><td  class="GridData" align="left">
                           <asp:Label ID="lblTotal" runat="server"></asp:Label></td></tr>
                           <tr><td align="right"  class="GridData" width="30%">Price in Words</td><td align="left"  class="GridData" colspan="3"><asp:Label ID="lblPrice" runat="server"></asp:Label> </td></tr></table>
                     
                     </div>
         
          <div class="Line">
            
           
               </div>
</asp:Content>
