﻿<%@ Page Title="Purchase Order" Language="C#" EnableEventValidation="false" Culture="en-GB"
    MasterPageFile="~/EventEntry/EventEntry.master" AutoEventWireup="true" CodeFile="PurchaseOrder.aspx.cs"
    Inherits="EventEntry_PurchaseOrder" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <script type="text/javascript">
        var prev = "";
        function ReceiveServerData(arg, context) {
            var str = arg.split("#");
            var type = str[0].split("_")[4];
            if (type == "ddlSubDivision") {
                var OptionArray = str[1].substring(1, str[1].length).split("~");
                var ddlsubdivision = document.getElementById(str[0]);
                for (var i = ddlsubdivision.options.length - 1; i > 0; i--) {
                    ddlsubdivision.remove(i);
                }
                if (OptionArray[0].split("|")[1] != "Please Select") {
                    for (var i = 0; i < OptionArray.length; i++) {

                        var option = document.createElement("OPTION");
                        ddlsubdivision.options.add(option);
                        ddlsubdivision.options[i + 1].innerHTML = OptionArray[i].split("|")[1];
                        ddlsubdivision.options[i + 1].value = OptionArray[i].split("|")[0];

                    }
                }

            }
            else if (type == "ddlItem") {
                var OptionArray = str[1].substring(1, str[1].length).split("~");
                var ddlitem = document.getElementById(str[0]);
                for (var i = ddlitem.options.length - 1; i > 0; i--) {
                    ddlitem.remove(i);
                }
                if (str[1] != "") {
                    if (OptionArray[0].split("|")[1] != "Please Select") {
                        for (var i = 0; i < OptionArray.length; i++) {

                            var option = document.createElement("OPTION");
                            ddlitem.options.add(option);
                            ddlitem.options[i + 1].innerHTML = OptionArray[i].split("|")[1];
                            ddlitem.options[i + 1].value = OptionArray[i].split("|")[0];

                        }
                    }
                }
            }
            else if (type == "ddlUOM") {
                var str2 = str[0].split("_");
                var str3 = str[1].split("~");
                var strId = "";
                for (var i = 0; i < str2.length - 1; i++) {
                    strId = strId + str2[i] + "_";
                }


                var OptionArray = str[1].split("~");
                var txtItemDesc = document.getElementById(strId + "txtItemDesc");
                var txtVat = document.getElementById(strId + "txtVat");
                var ddlUOM = document.getElementById(strId + "ddlUOM");
                //alert();
                if (str3[0] != "Please Select") {

                    txtItemDesc.value = str3[0];
                    txtVat.value = str3[1];
                    ddlUOM.value = str3[2];
                }
                else {
                    txtItemDesc.value = "";
                    txtVat.value = "";
                    ddlUOM.value = "0";
                }

            }

        }








        function CallSrv(id, val) {
            var str = id.split("_");
            var strId = "";
            for (var i = 0; i < str.length - 1; i++) {
                strId = strId + str[i] + "_";
            }
            document.getElementById(strId + "txtQty").value = "";
            document.getElementById(strId + "txtPrice").value = "0.00";
            document.getElementById(strId + "txtVat").value = "0.00";
            document.getElementById(strId + "txtVatAmount").value = "0.00";
            document.getElementById(strId + "txtTotal").value = "0.00";
            document.getElementById(strId + "ddlSubDivision").selectedIndex = 0;
            document.getElementById(strId + "ddlItem").selectedIndex = 0;
            document.getElementById(strId + "ddlUOM").selectedIndex = 0;
            strId = strId + "ddlSubDivision";
            CallServer(strId + "," + val);

        }

        function CallSrv1(id, val) {
            var str = id.split("_");
            var strId = "";
            for (var i = 0; i < str.length - 1; i++) {
                strId = strId + str[i] + "_";
            }
            var ddldivision = document.getElementById(strId + "ddlDivision");
            var _HdSubDivision = document.getElementById(strId + "HdSubDivision");
            _HdSubDivision.value = val;

            strId = strId + "ddlItem";
            var selectedval = ddldivision.value;
            CallServer(strId + "," + selectedval + "," + val);

        }
        function CallSrv2(id, val) {
            var str = id.split("_");
            var strId = "";
            for (var i = 0; i < str.length - 1; i++) {
                strId = strId + str[i] + "_";
            }

            //  var ddldivision=document.getElementById(strId+"ddlDivision");
            var ddlUOM = strId + "ddlUOM";
            var _HdItem = document.getElementById(strId + "HdItem");
            _HdItem.value = val;


            CallServer(ddlUOM + "," + val);

        }


        function CalculateVatAmount(id, val) {
            var str = id.split("_");
            var strId = "";
            for (var i = 0; i < str.length - 1; i++) {
                strId = strId + str[i] + "_";
            }
            var txtQty = document.getElementById(strId + "txtQty");
            var txtPrice = document.getElementById(strId + "txtPrice");
            var txtVat = document.getElementById(strId + "txtVat");
            var txtVatAmount = document.getElementById(strId + "txtVatAmount");
            var txtTotal = document.getElementById(strId + "txtTotal");

            txtVatAmount.value = round_decimals(txtQty.value * txtPrice.value * txtVat.value, 2);

            txtVatAmount.value = round_decimals(txtVatAmount.value / 100, 2);
            txtTotal.value = parseFloat(txtQty.value * txtPrice.value) + parseFloat(txtVatAmount.value);
            txtVatAmount.value = FormatCurrency(txtVatAmount.value);

        }

   

  
     
    </script>

    <div class="PageMargin">
        <div class="PageHeading">
            Purchase Order <a style="float: right" href="POrderList.aspx">Purchase Order List</a>
        </div>
        <div class="Line">
        </div>
        <!--Content Table Start-->
        <div class="PageContent">
            <table width="70%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
                <tr>
                    <td align="left" valign="middle">
                        Order Type&nbsp;&nbsp;<span style="color: Red; font-weight: bold">*</span>
                    </td>
                    <td align="left" valign="middle">
                        <asp:DropDownList CssClass="TextboxSmall" ID="ddlOrderType" runat="server" Width="154px">
                        </asp:DropDownList>
                        <br />
                        <asp:RequiredFieldValidator ControlToValidate="ddlOrderType" InitialValue="0" ErrorMessage="Please Select Order Type"
                            ID="RequiredFieldValidator1" runat="server" />
                    </td>
                    <td align="left" valign="middle">
                        PO date(dd/mm/yyyy)&nbsp;&nbsp;<span style="color: Red; font-weight: bold">*</span>
                    </td>
                    <td align="left" valign="middle">
                        <asp:TextBox ID="poDate" runat="server" CssClass="TextboxSmall" MaxLength="50" />
                        <br />
                        <asp:RequiredFieldValidator ControlToValidate="poDate" ErrorMessage="Field cannot be empty"
                            ID="RequiredFieldValidator2" runat="server" />
                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="poDate"
                            Format="dd/MM/yyyy">
                        </cc1:CalendarExtender>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle">
                        Vendor Name&nbsp;&nbsp;<span style="color: Red; font-weight: bold">*</span>
                    </td>
                    <td align="left" valign="middle">
                        <asp:DropDownList CssClass="TextboxSmall" ID="ddlVendor" runat="server" Width="154px">
                        </asp:DropDownList>
                        <br />
                        <asp:RequiredFieldValidator ControlToValidate="ddlVendor" InitialValue="0" ErrorMessage="Please Select Vendor"
                            ID="RequiredFieldValidator3" runat="server" />
                    </td>
                    <td align="left" valign="middle">
                        Rate Contract Type&nbsp;&nbsp;<span style="color: Red; font-weight: bold">*</span>
                    </td>
                    <td align="left" valign="middle">
                        <asp:DropDownList CssClass="TextboxSmall" ID="ddlRateContractType" runat="server"
                            Width="154px">
                        </asp:DropDownList>
                        <br />
                        <asp:RequiredFieldValidator ControlToValidate="ddlRateContractType" InitialValue="0"
                            ErrorMessage="Please Select Rate Contract Type" ID="RequiredFieldValidator4"
                            runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle" colspan="4">
                        <asp:Label ID="message" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
        <!--Content Table End-->
        <div class="Line">
        </div>
        <!--Grid Table Start-->
        <asp:GridView ID="poGridView" RowStyle-HorizontalAlign="Center" runat="server" HeaderStyle-CssClass="GridHeading"
            ItemStyle-CssClass="GridData" Width="100%" AutoGenerateColumns="false" OnPreRender="poGridView_PreRender">
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        Division
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:DropDownList ID="ddlDivision" CssClass="SmallField" onchange="CallSrv(this.id,this.value)"
                            runat="server">
                            <asp:ListItem Text="Please Select" Value="Please Select"></asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        Sub Division
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:DropDownList ID="ddlSubDivision" CssClass="SmallField" onchange="CallSrv1(this.id,this.value)"
                            runat="server">
                            <asp:ListItem Value="Please Select" Text="Please Select"></asp:ListItem>
                        </asp:DropDownList>
                        <input type="hidden" id="HdSubDivision" value='<%#Eval("SubDivision")%>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        Item Name/Part Code
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:DropDownList ID="ddlItem" CssClass="SmallField" onchange="CallSrv2(this.id,this.value)"
                            runat="server">
                            <asp:ListItem Value="Please Select" Text="Please Select"></asp:ListItem>
                        </asp:DropDownList>
                        <input type="hidden" id="HdItem" value='<%#Eval("ItemName")%>' runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        Item Description
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:TextBox ID="txtItemDesc" CssClass="SmallField" TextMode="MultiLine" runat="server"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        Quantity
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:TextBox ID="txtQty" onblur="CalculateVatAmount(this.id,this.value);" CssClass="SmallField"
                            MaxLength="10" onkeypress="return isNumberKey(event);" Width="100px" runat="server"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        UOM
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:DropDownList ID="ddlUOM" CssClass="SmallField" runat="server">
                            <asp:ListItem Text="Please Select"></asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        Price
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:TextBox ID="txtPrice" MaxLength="10" CssClass="SmallField" Width="70px" onkeypress="return InNumer(this.value,this.id);"
                            onblur="CalculateVatAmount(this.id,this.value);" runat="server"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        VAT(%)
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:TextBox ID="txtVat" CssClass="SmallField" Width="70px" runat="server"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        VAT Amount
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:TextBox ID="txtVatAmount" CssClass="SmallField" Width="70px" runat="server"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        Total
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:TextBox ID="txtTotal" CssClass="SmallField" Width="70px" runat="server"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <br />
        <table width="90%">
            <tr>
                <td align="center">
                    <asp:Button ID="btnAddNewItem" runat="server" Text="Add New Item" CssClass="Button"
                        OnClick="btnAddNewItem_Click" Width="150px" />&nbsp;
                    <asp:Button ID="add" Text="Submit" runat="server" CssClass="Button" OnClick="add_Click"
                        Width="52px" />&nbsp;
                    <asp:Button ID="cancel" Text="Cancel" runat="server" CausesValidation="false" CssClass="Button"
                        OnClick="cancel_Click" />
                </td>
            </tr>
        </table>
        <input type="hidden" value="0" runat="server" id="strPoId" />
        <!--Grid Table End-->
        <div class="Line">
        </div>
        <!--Page Body End-->
    </div>
</asp:Content>
