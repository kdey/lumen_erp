<%@ Page Title="" Language="C#" MasterPageFile="~/EventEntry/EventEntry.master" AutoEventWireup="true" CodeFile="Service.aspx.cs" Inherits="EventEntry_Service" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script type="text/javascript" language="javascript">
  function CalculateAmount() {
        var AMT = document.getElementById('<%=txtServiecPrice.ClientID%>');
        var AMT_ = AMT.value;
        var vatamt = document.getElementById('<%=txtvat.ClientID%>');
        var vat = vatamt.value
        var _ddlServiceTax = document.getElementById('<%=ddlServiceTax.ClientID%>');
        var _ddlEdCess = document.getElementById('<%=ddlEdCess.ClientID%>');
        var _ddlHedCess = document.getElementById('<%=ddlHedCess.ClientID%>');

        var _txtTotal = document.getElementById('<%=txtTotal.ClientID%>');
          
        var TAMT = AMT.value * ( _ddlServiceTax.options[_ddlServiceTax.selectedIndex].text) *.01    ;
        var cess1 = TAMT * ( _ddlEdCess.options[_ddlEdCess.selectedIndex].text) *.01    ;
        var cess2= TAMT * ( _ddlHedCess.options[_ddlHedCess.selectedIndex].text) *.01    ;
        
        var _t;
        if (isNaN(TAMT))
             TAMT = 0;
       if (isNaN(cess1))
             cess1 = 0
       if (isNaN(cess2))
             cess2 = 0
       if (isNaN(vat) || vat == "")
             vat = 0                                       

       if (isNaN(AMT_) || AMT_ == "")
             AMT_ = 0  
              
         _t = (parseFloat(TAMT) + parseFloat(AMT_) +parseFloat(cess1)+parseFloat(cess2)+ parseFloat(vat)) ;
        var _t1 =roundNumber(_t, 0);
        
        _txtTotal.innerHTML = _t1;
        
         var _lblprice = document.getElementById('<%=lblPrice.ClientID%>');
        test_skill(_t1);
    }    
function roundNumber(rnum, rlength) 
{ 

  var newnumber = Math.round(rnum*Math.pow(10,rlength))/Math.pow(10,rlength);
  return  newnumber;
}
      
function test_skill( _lblprice)  
{  
    var junkVal=_lblprice;  
    junkVal=Math.floor(junkVal);  
    var obStr=new String(junkVal);  
    numReversed=obStr.split("");  
    actnumber=numReversed.reverse();  

  
    if(Number(junkVal) >=0)  
    {  
        //do nothing  
    }  
    else  
    {  
       // alert('wrong Number cannot be converted');  
        return false;  
    }  
    if(Number(junkVal)==0)  
    {  
        document.getElementById('<%=lblPrice.ClientID%>').innerHTML=obStr+''+'Rupees Zero Only';  
        return false;  
    }  
    if(actnumber.length>9)  
    {  
       // alert('Oops!!!! the Number is too big to covertes');  
        return false;  
    }  
  
    var iWords=["Zero", " One", " Two", " Three", " Four", " Five", " Six", " Seven", " Eight", " Nine"];  
    var ePlace=['Ten', ' Eleven', ' Twelve', ' Thirteen', ' Fourteen', ' Fifteen', ' Sixteen', ' Seventeen', ' Eighteen', ' Nineteen'];  
    var tensPlace=['dummy', ' Ten', ' Twenty', ' Thirty', ' Forty', ' Fifty', ' Sixty', ' Seventy', ' Eighty', ' Ninety' ];  
  
    var iWordsLength=numReversed.length;  
    var totalWords="";  
    var inWords=new Array();  
    var finalWord="";  
    j=0;  
    for(i=0; i<iWordsLength; i++)  
    {  
        switch(i)  
        {  
        case 0:  
            if(actnumber[i]==0 || actnumber[i+1]==1 )  
            {  
                inWords[j]='';  
            }  
            else  
            {  
                inWords[j]=iWords[actnumber[i]];  
            }  
            inWords[j]=inWords[j]+' Only';  
            break;  
        case 1:  
            tens_complication();  
            break;  
        case 2:  
            if(actnumber[i]==0)  
            {  
                inWords[j]='';  
            }  
            else if(actnumber[i-1]!=0 && actnumber[i-2]!=0)  
            {  
                inWords[j]=iWords[actnumber[i]]+' Hundred and';  
            }  
            else  
            {  
                inWords[j]=iWords[actnumber[i]]+' Hundred';  
            }  
            break;  
        case 3:  
            if(actnumber[i]==0 || actnumber[i+1]==1)  
            {  
                inWords[j]='';  
            }  
            else  
            {  
                inWords[j]=iWords[actnumber[i]];  
            }  
            inWords[j]=inWords[j]+" Thousand";  
            break;  
        case 4:  
            tens_complication();  
            break;  
        case 5:  
            if(actnumber[i]==0 || actnumber[i+1]==1 )  
            {  
                inWords[j]='';  
            }  
            else  
            {  
                inWords[j]=iWords[actnumber[i]];  
            }  
            inWords[j]=inWords[j]+" Lakh";  
            break;  
        case 6:  
            tens_complication();  
            break;  
        case 7:  
            if(actnumber[i]==0 || actnumber[i+1]==1 )  
            {  
                inWords[j]='';  
            }  
            else  
            {  
                inWords[j]=iWords[actnumber[i]];  
            }  
            inWords[j]=inWords[j]+" Crore";  
            break;  
        case 8:  
            tens_complication();  
            break;  
        default:  
            break;  
        }  
        j++;  
    }  
  
    function tens_complication()  
    {  
        if(actnumber[i]==0)  
        {  
            inWords[j]='';  
        }  
        else if(actnumber[i]==1)  
        {  
            inWords[j]=ePlace[actnumber[i-1]];  
        }  
        else  
        {  
            inWords[j]=tensPlace[actnumber[i]];  
        }  
    }  
    inWords.reverse();  
    for(i=0; i<inWords.length; i++)  
    {  
        finalWord+=inWords[i];  
    }  

    //document.getElementById('<%=lblPrice.ClientID%>').innerHTML=obStr+''+finalWord;  
         var _lblprice = document.getElementById('<%=lblPrice.ClientID%>');
         _lblprice.innerHTML =finalWord;

             
}

</script>


 <div class="PageMargin">
      <div class="PageHeading">
             SERVICE<a style="float:right" href="ServiceList.aspx">Service Bill List</a> BILL</div>
      <div class="Line">
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
     </div>
    <!--Content Table Start-->
     <div class="PageContent">
        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">

             <tr>
                  <td valign="top" align="right">            
                       Customer Name&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span>
                  </td>
                  <td valign="top" align="left">
                       <asp:DropDownList CssClass="TextboxSmall"  ID="ddlCustomer"  runat="server" 
                            Width="100%" AutoPostBack="True" 
                           onselectedindexchanged="ddlCustomer_SelectedIndexChanged"    >
                       </asp:DropDownList>
                       <br />
                       <asp:RequiredFieldValidator ControlToValidate="ddlCustomer"  InitialValue="0"  ErrorMessage="Select Customer"
                            ID="RequiredFieldValidator1" runat="server" />
                   </td>
                   <td valign="top" align="right">Customer Order No#&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span>
                   </td>
                   <td valign="top" align="left">
                        <asp:TextBox ID="poReference" runat="server"   CssClass="TextboxSmall" 
                            TabIndex="1" />
                        <br />
                        <asp:RequiredFieldValidator ControlToValidate="poReference"  ErrorMessage="Field cannot be empty"
                            ID="RequiredFieldValidator6" runat="server" />
                   </td>                    
                   <td align="left" valign="middle" width="16%">
                        Customer Order Date&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span><br />
                        (dd/MM/yyyy)<cc1:calendarextender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="poDate">
                                    </cc1:calendarextender>                        
                   </td>
                   <td valign="top" align="left">
                        <asp:TextBox ID="poDate" onkeyup="document.getElementById(this.id).value=''" 
                            autocomplete='off' runat="server" CssClass="TextboxSmall" TabIndex="2"  /> 
                        <br />
                                    <asp:RequiredFieldValidator ControlToValidate="poDate"   ErrorMessage="Field cannot be empty"
                                    ID="RequiredFieldValidator5" runat="server" />
       
                    </td>
                  
             </tr>
              <tr>
                                <td valign="top" align="right">Billing Address</td>
                                <td valign="top" align="left"><asp:TextBox ID="billingAddress" runat="server" TextMode="MultiLine" Height="50px" CssClass="TextboxSmall"
                                     ReadOnly="True" /></td>
                                <td valign="top" align="right">Installation Address</td>
                                <td valign="top" align="left">
                                                <asp:TextBox ID="installationAddress" TextMode="MultiLine" Height="50px" runat="server"   CssClass="TextboxSmall" />
                                                            
                                                
                                              
                                </td>  
                               <td valign="top" align="right">
                                               Customer Segment
                               </td>
                               <td  align="left" valign="top">
                                                <asp:Label ID="CustomerSegment"   runat="server"    />
                               </td>                              
                            </tr>
             <tr>
                    <td valign="top" align="right">
                     Machine Serial No#&nbsp;&nbsp;
                    </td>
                    <td valign="top" align="left">
                         <asp:TextBox ID="txtSerialNo" runat="server"   CssClass="TextboxSmall" 
                             TabIndex="3" />
                         <br />    
                    </td>
                    <td valign="top" align="right">Service Contract Type&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span>
                    </td>
                    <td valign="top" align="left">
                         <asp:DropDownList ID="ddlContractType" CssClass="TextboxSmall" runat="server" 
                             TabIndex="4">
                         </asp:DropDownList>
                         <br />
                         <asp:RequiredFieldValidator InitialValue="0" ControlToValidate="ddlContractType" ErrorMessage="Select Order Type"
                            ID="RequiredFieldValidator7"  runat="server" />
                    </td>
                    <td valign="top" align="right">
                      Service Description
                    </td>
                    <td valign="top" align="left">
                        <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" 
                            Height="50px"  CssClass="TextboxSmall" TabIndex="5" />
                    </td>
             </tr>
              <tr>

                <td width="16%" align="right" valign="top">
                      Service Center&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span>
                 </td>
                 <td align="left" valign="top" width="20%">
                     <asp:DropDownList CssClass="TextboxSmall" ID="ddlServiceCenter" runat="server" 
                         Width="150px" TabIndex="6">
                     </asp:DropDownList>
                     <br />    
                     <asp:RequiredFieldValidator InitialValue="0" ControlToValidate="ddlServiceCenter" ErrorMessage="Select Billing Address"
                     ID="RequiredFieldValidator2"  runat="server" />
                  </td>

         
                   <td valign="top" align="right">
                       Service Charge&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span>
                    </td>
                    <td valign="top" align="left">
                        <asp:TextBox ID="txtServiecPrice" onblur="CalculateAmount()" runat="server"   
                            CssClass="TextboxSmall" TabIndex="7" />
                         <br />
                         <asp:RequiredFieldValidator ControlToValidate="txtServiecPrice"  ErrorMessage="Field cannot be empty"
                                  ID="RequiredFieldValidator9" runat="server" />
                         <br />
                         <asp:RangeValidator ID="RangeValidator2" ControlToValidate="txtServiecPrice" runat="server" ErrorMessage="Please give correct decimal number" Type="Double" MaximumValue="1000000"></asp:RangeValidator>
                     
                    </td>
                    <td valign="top" align="right">
                           Service Tax</td>
                    <td valign="top" align="left">
                            <asp:DropDownList ID="ddlServiceTax" onchange="CalculateAmount()" 
                                CssClass="TextboxSmall" runat="server" TabIndex="8">
                            </asp:DropDownList>
                    </td>
     

              </tr>
              <tr>

                 <td valign="top" align="right">
                   Education Cess</td>
                 <td valign="top" align="left">
                    <asp:DropDownList ID="ddlEdCess" onchange="CalculateAmount()" CssClass="TextboxSmall" 
                         runat="server" TabIndex="9">
                    </asp:DropDownList>
                 </td>
        
                 <td valign="top" align="right">
                     Higher Education Cess</td>
                 <td valign="top" align="left">
                    <asp:DropDownList ID="ddlHedCess" onchange="CalculateAmount()" CssClass="TextboxSmall" 
                         runat="server" TabIndex="10">
                    </asp:DropDownList>
                 </td>
                <td valign="top" align="right">VAT Amount<br /> (Rs)<%--&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span>--%>
                   </td>
                   <td valign="top" align="left">
                        <asp:TextBox ID="txtvat" runat="server"   CssClass="TextboxSmall"  onblur="CalculateAmount()"
                            TabIndex="11" />
                        <%--<br />
                        <asp:RequiredFieldValidator ControlToValidate="txtvat"  ErrorMessage="Field cannot be empty"
                            ID="RequiredFieldValidator4" runat="server" />--%>
                   </td>
              </tr>            
                
               <tr>
                    <td valign="top" align="right">
                         &nbsp;</td>
                    <td valign="top" align="left">
                        &nbsp;</td>
                         <td align="left" valign="middle" width="16%">&nbsp;</td>
                    <td valign="top" align="right">
                                    &nbsp;</td>
                    <td valign="top" align="left">&nbsp;</td>
                    <td valign="top" align="left">
                        &nbsp;</td>
                </tr>            
            
                 <tr>
                        <td valign="top" align="right">
                             &nbsp;</td>
                        <td valign="top" align="left">
                            &nbsp;</td>
                             <td align="left" valign="middle" width="16%">&nbsp;</td>
                        <td valign="top" align="right">
                                           Total
                         </td>
                          <td valign="top" align="left">
                                  <asp:Label ID="txtTotal" runat="server"  />
                          </td>
                          <td valign="top" align="left">
                                   <asp:Label ID="lbl_strServiceId" runat="server" Text="Label" Visible="False"></asp:Label>
                                    <asp:Label ID="message" runat="server" />
                            </td>
                   </tr>            
            </table>
       
          <!--Content Table End-->
        <div class="Line">
        </div>
        <div>Price in words&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
            <asp:Label ID="lblPrice" runat="server" ></asp:Label></div>
        <div class="Line">
        </div>    
                      
               
      </div>
      
               <table width="90%" >
                <tr><td align="center"> &nbsp; 
                    <asp:Button ID="add" 
                                  Text="Submit" runat="server"   CssClass="Button" 
                            onclick="add_Click" Width="52px" TabIndex="13" />&nbsp;
                       
                            <asp:Button ID="cancel" Text="Cancel" runat="server"  CausesValidation="false"
                            CssClass="Button" TabIndex="14"   /> 
                    <asp:Button ID="btnprint" 
                                  Text="Print" runat="server"   CssClass="Button" 
                             Width="52px" TabIndex="13" onclick="btnprint_Click" /></td>
                </tr>
               </table>
        <!--Page Body End-->
    </div>

</asp:Content>
 

