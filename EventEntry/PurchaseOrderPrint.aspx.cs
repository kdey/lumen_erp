﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Drawing;

using Bitscrape.AppBlock.Database;
using System.Globalization;

public partial class EventEntry_PurchaseOrderPrint :BasePage
{
    protected int _poId = -1;
    protected int _poStateId = -1;
    private double totalAmount = 0;
    private double totalVATAmount = 0;
    private double GrandtotalAmount = 0;
    private double totalwithtax = 0;
    bool isdeleted = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!string.IsNullOrEmpty(Request["PurchaseOrderId"]))
        {
            _poId = int.Parse(Request["PurchaseOrderId"]);
        }

        if (ViewState["previousPage"] == null)
        {
            ViewState["previousPage"] =Convert.ToString(Request.UrlReferrer);
        }

         if (!this.IsPostBack)
         {
             if (_poId > 0)
             {
                 LoadPo();
             }
         }
    }
    private void LoadPo()
    {
          int poTypeId = -1;
                    int vendorId =-1;
                    int rateContractTypeId = -1;
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select * from Lum_Erp_PurchaseOrder where poId = " + _poId))
            {
                if (reader.Read())
                {
                    poNumber.Text = reader["strPoId"].ToString();
                    _poStateId = Convert.ToInt32(reader["poStateId"]);
                    poTypeId = Convert.ToInt32(reader["poTypeId"].ToString());
                     vendorId = Convert.ToInt32(reader["vendorId"].ToString());
                    rateContractTypeId = Convert.ToInt32(reader["rateContractTypeId"].ToString());
                    isdeleted = Convert.ToBoolean(reader["isdeleted"]);
                    //if purchase order Is locekd
                    //if (_poStateId == 2)
                    //{
                    //    tblItem.Visible = false;
                    //}
                    // now set the poType in the drop down
                     poDate.Text = ((DateTime)reader["poDate"]).ToString("dd/MM/yyyy");
                }
                reader.Close();
            }
            // load the purchase order type
            using (IDataReader reader1 = conn.ExecuteQuery("select poTypeId,poTypeCode+'('+poTypeName+')' as poTypeCode from Lum_Erp_PurchaseOrderType where poTypeId='" + poTypeId + "'"))
            {
                        if(reader1.Read())
                        {
                    string poTypeCode = reader1["poTypeCode"].ToString();
                   // poTypeDdl.Text = poTypeCode.ToString();
                        }
                        reader1.Close();
            }    
                    // now set the vendor company in the drop down.
            using (IDataReader reader2 = conn.ExecuteQuery("select vendorId, vendorName,(address1+' '+address2)as address ,VATRegistrationNo from Lum_Erp_Vendor where vendorId='" + vendorId + "'"))
                    {
                        if(reader2.Read())
                        {
                            string vendorName = reader2["vendorName"].ToString();
                            string address=reader2["address"].ToString();
                            vendorDdl.Text = vendorName.ToString();
                            vendorAddress.Text = address.ToString();
                            supvat.Text = Convert.ToString(reader2["VATRegistrationNo"]);
                        }
                        reader2.Close();
                    }
                   
                    // get teh rate contract type
            using (IDataReader reader3 = conn.ExecuteQuery("select rateContractTypeId, contractType FROM Lum_Erp_RateContractType where rateContractTypeId='" + rateContractTypeId + "'"))
                    {
                        if(reader3.Read())
                        {
                        //rateContractDdl.Text = reader3["contractType"].ToString();
                        }
                        reader3.Close();
                    }
                    //vendorDdl.Enabled = false;
                    //poTypeDdl.Enabled = false;
                    //rateContractDdl.Enabled = false;
            
                using (IDataReader _reader = conn.ExecuteQuery(string.Format("select TIN,ServiceTaxRegnNo,* FROM Lum_Erp_CompanyMaster where fromdate <=DATEADD(MI,330,GETUTCDATE() ) and isnull(todate,DATEADD(MI,330,GETUTCDATE() )) >=DATEADD(MI,330,GETUTCDATE() )")))
                {
                    if (_reader.Read())
                    {
                        lblTinNo.Text = Convert.ToString(_reader["TIN"]);
                        lblServiceTaxRegistNo.Text = Convert.ToString(_reader["ServiceTaxRegnNo"]);
                        lblCSTNo.Text = Convert.ToString(_reader["cstNo"]);
                    }
                    _reader.Close();
                }
           

                    BindGrid(conn, isdeleted);
            //lblTotal.Text = string.Format("{0:N}", Math.Round(totalAmount));
            lblPrice.Text = changeCurrencyToWords(Math.Round(GrandtotalAmount));
            //lblquantity.Text = GetTotalQuantity();
            
        }
    }
    private void BindGrid(IConnection conn, bool isdeleted)
    {
        double t1 = 0;
        double t2 = 0;
        totalAmount = 0;
        totalVATAmount = 0;
        GrandtotalAmount = 0;
        string _staxrate = "";
        IDataReader _reader;
        DataTable aTable = new DataTable();

        DataRow dtRow;
        DataColumn dtCol;

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "poLineId";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "itemCode";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "itemDescription";
        aTable.Columns.Add(dtCol);


        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "quantity";
        aTable.Columns.Add(dtCol);


        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "uomName";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "price";
        aTable.Columns.Add(dtCol);
        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "total";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "vatAmount";
        aTable.Columns.Add(dtCol);

        DataRow drProduct;
        DateTime date = DateTime.ParseExact(poDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        string fromDate = Convert.ToDateTime(date).ToString("dd MMM yyyy");
        SpParam _isdeleted1 = new SpParam("@isdeleted", isdeleted, SqlDbType.Int);
        SpParam _poId1 = new SpParam("@_poId", _poId, SqlDbType.Int);
        SpParam _CDate = new SpParam("@CDate", fromDate, SqlDbType.SmallDateTime);
        DataSet _Ds = new DataSet();
        _Ds = conn.GetDataSet("Lum_Erp_Stp_GetPOforPrint", _isdeleted1, _poId1,_CDate);

        for (int _index = 0; _index < _Ds.Tables[0].Rows.Count; _index++)
        {
            drProduct = aTable.NewRow();

            drProduct["poLineId"] = _Ds.Tables[0].Rows[_index]["poLineId"].ToString();
            drProduct["itemCode"] = _Ds.Tables[0].Rows[_index]["itemCode"].ToString();
            drProduct["itemDescription"] = _Ds.Tables[0].Rows[_index]["itemDescription"].ToString();
            drProduct["quantity"] = _Ds.Tables[0].Rows[_index]["quantity"].ToString();
            drProduct["uomName"] = _Ds.Tables[0].Rows[_index]["uomName"].ToString();
            drProduct["price"] = string.Format("{0:#,##0.00}", (Convert.ToDouble(_Ds.Tables[0].Rows[_index]["price"])));
            drProduct["total"] = string.Format("{0:#,##0.00}", (Convert.ToDouble(_Ds.Tables[0].Rows[_index]["total"])));
            drProduct["vatAmount"] = _Ds.Tables[0].Rows[_index]["vatAmount"].ToString();
            CalcTotalAmount(drProduct["total"].ToString());
            CalcTotalVATAmount(drProduct["vatAmount"].ToString());
            CalcTotalwithtax(_Ds.Tables[0].Rows[_index]["totalwithtax"].ToString());
            _staxrate = (_Ds.Tables[0].Rows[0]["vat"].ToString());
            aTable.Rows.Add(drProduct);
        }
        drProduct = aTable.NewRow();
        drProduct["price"] = "Total";
        drProduct["total"] = string.Format("{0:#,##0.00}", (totalAmount));

        aTable.Rows.Add(drProduct);

        drProduct = aTable.NewRow();
        drProduct["price"] = "Tax @" + " " + _staxrate;
        drProduct["total"] = string.Format("{0:#,##0.00}", (totalVATAmount));

        aTable.Rows.Add(drProduct);

        GrandtotalAmount = totalAmount + totalVATAmount;

        drProduct = aTable.NewRow();
        drProduct["price"] = "GrandTotal";
        drProduct["total"] = string.Format("{0:#,##0.00}", (totalwithtax));

        aTable.Rows.Add(drProduct);

        t1 = Convert.ToDouble(totalwithtax);
        t2 = Math.Round(totalwithtax);

        if (t2 > t1)
        {
            drProduct = aTable.NewRow();
            drProduct["price"] = "Round Off ";
            drProduct["total"] = "(+)" + string.Format("{0:#,##0.00}", (t2 - t1));
            aTable.Rows.Add(drProduct);
        }
        if (t2 < t1)
        {
            drProduct = aTable.NewRow();
            drProduct["price"] = "Round Off ";
            drProduct["total"] = "(-)" + string.Format("{0:#,##0.00}", (t1 - t2));
            aTable.Rows.Add(drProduct);
        }

        drProduct = aTable.NewRow();
        drProduct["price"] = "Total";
        drProduct["total"] = string.Format("{0:#,##0.00}", (Math.Round(totalwithtax)));

        aTable.Rows.Add(drProduct);


        poLineGrid.DataSource = aTable;
        poLineGrid.DataBind();


        lblPrice.Text = changeCurrencyToWords(Math.Round(totalwithtax));

    }
    private void CalcTotalAmount(string _netVal)
    {
        if (_netVal != "")
        {
            totalAmount += Double.Parse(_netVal);
        }

    }
    private void CalcTotalVATAmount(string _VatAmount)
    {
        if (_VatAmount != "")
        {
            totalVATAmount += Double.Parse(_VatAmount);
        }

    }
    private void CalcTotalwithtax(string _totatwithtax)
    {
        if (_totatwithtax != "")
        {
            totalwithtax += Double.Parse(_totatwithtax);
        }

    }
    protected void poLineGrid_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        //if (e.Item.ItemType == ListItemType.Footer)
        //{
        //    e.Item.Cells[4].Text = Convert.ToString(totalAmount);
        //    e.Item.Cells[2].Text = Convert.ToString(totalVATAmount);
        //    e.Item.Cells[1].Text = "Total";
        //}
        //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //{
        //    CalcTotalAmount(e.Item.Cells[4].Text);

        //}
       
    }
    protected void btnCancel_ServerClick(object sender, EventArgs e)
    {
        if (ViewState["previousPage"] != null)
        {
            Response.Redirect(Convert.ToString(ViewState["previousPage"]));
        }
        
    }
}
