﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;


public partial class EventEntry_GRNPrint_WithPo : BasePage
{
    protected int _GRNId = -1;
    protected int _poId = -1;
    private double totalAmount = 0;
    private double TotalVat = 0;
    private double NetTotalAmount = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!string.IsNullOrEmpty(Request["GRNId"]))
        {
            _GRNId = int.Parse(Request["GRNId"]);
        }
        if (!string.IsNullOrEmpty(Request["poId"]))
        {
            _poId = int.Parse(Request["poId"]);
        }
        if (ViewState["previousPage"] == null)
        {
            ViewState["previousPage"] = Convert.ToString(Request.UrlReferrer);
        }

        if (!this.IsPostBack)
        {
            if (_GRNId > 0)
            {
                LoadGrn();
            }
        }
    }
    public void LoadGrn()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(@"select G.POID, grnId,strGrnId,vendorInvoiceNo,CONVERT(VARCHAR,invoiceDate,106) AS invoiceDate,
		                                                    storeId,grnVersion,CONVERT(VARCHAR,dateCreated,106) AS dateCreated,P.strPoId,CONVERT(VARCHAR,P.poDate,106) AS poDate ,P.poTypeId,P.poVersion,P.rateContractTypeId,V.vendorName,(V.address1+','+V.address2) AS vendorAdd ,V.VATRegistrationNo,V.CSTRegistrationNo from Lum_Erp_GRN G
                                                            inner join Lum_Erp_PurchaseOrder P on G.poId=P.poId
                                                            inner join Lum_Erp_vendor V on V.vendorId=P.vendorId
                                                            WHERE  G.grnId=" + _GRNId))
            {
                if (reader.Read())
                {


                    PONO.Text = Convert.ToString(reader["strPoId"]);// +"/" + Convert.ToString(reader["poVersion"]);
                    GRNDate.Text = Convert.ToString(reader["dateCreated"]);
                    VendorInvoiceNo.Text = Convert.ToString(reader["vendorInvoiceNo"]);
                    PODate.Text = Convert.ToString(reader["poDate"]);
                    _poId = Convert.ToInt32(reader["poId"]);
                    GRNNumber.Text = Convert.ToString(reader["strGrnId"]);// +"/" + Convert.ToString(reader["grnVersion"]);
                    VendorInvoiceDt.Text = Convert.ToString(reader["invoiceDate"]);
                    Vendorname.Text = Convert.ToString(reader["vendorName"]);
                    Vendoradd.Text = Convert.ToString(reader["vendorAdd"]);
                    VendorVATNo.Text = Convert.ToString(reader["VATRegistrationNo"]);
                    VendorCSTNo.Text = Convert.ToString(reader["CSTRegistrationNo"]);

                    BindGrid(conn);
                    reader.Close();
                }

            }
        }
    }
    private void BindGrid(IConnection conn)
    {
        totalAmount = 0;
        TotalVat = 0;
        NetTotalAmount = 0;
        //IDataReader _reader;
        DataTable aTable = new DataTable();

        //DataRow dtRow;
        DataColumn dtCol;

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "row_number";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "divisionName";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "itemCode";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "SerialNo";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "quantityReceived";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "qtydue";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "Amount";
        aTable.Columns.Add(dtCol);


        DataRow drProduct;
        string fromDate = Convert.ToDateTime(PODate.Text).ToString("dd MMM yyyy");
        SpParam _POId = new SpParam("@poId", _poId, SqlDbType.Int);
        SpParam _CDate = new SpParam("@CDate", fromDate, SqlDbType.SmallDateTime);
        SpParam _GrnId = new SpParam("@grnId", _GRNId, SqlDbType.Int);

        DataSet _Ds = new DataSet();
        using (IConnection conn1 = DataConnectionFactory.GetConnection())
        {
            _Ds = conn1.GetDataSet("Lum_erp_Stp_GetPurchaseOrder_WithGrnDetails_print", _POId, _CDate ,_GrnId);
        }
        for (int _index = 0; _index < _Ds.Tables[0].Rows.Count; _index++)
        {
            drProduct = aTable.NewRow();

            drProduct["row_number"] = _Ds.Tables[0].Rows[_index]["row_number"].ToString();
            drProduct["divisionName"] = _Ds.Tables[0].Rows[_index]["divisionName"].ToString();
            drProduct["itemCode"] = _Ds.Tables[0].Rows[_index]["itemCode"].ToString();
            drProduct["SerialNo"] = _Ds.Tables[0].Rows[_index]["SerialNo"].ToString();

            string _SerialNum = string.Empty;
            string _SlrNo = _Ds.Tables[0].Rows[_index]["SerialNo"].ToString();
            string[] _SerialNo = _SlrNo.Split(';');
            for (int i = 0; i < _SerialNo.Length; i++)
            {
                if (_SerialNum == "")
                {
                    if (_SerialNo[i].ToString() != "")
                        _SerialNum = _SerialNo[i].ToString();
                }
                else
                {
                    _SerialNum = _SerialNum + "&nbsp;" + "<br>" + _SerialNo[i].ToString();
                }
            }
            drProduct["SerialNo"] = _SerialNum;

            drProduct["quantityReceived"] = _Ds.Tables[0].Rows[_index]["quantityReceived"].ToString();
            drProduct["qtydue"] = _Ds.Tables[0].Rows[_index]["qtydue"].ToString();
            drProduct["Amount"] = _Ds.Tables[0].Rows[_index]["total"].ToString();


            CalcTotalAmount(_Ds.Tables[0].Rows[_index]["total"].ToString());
            CalcTotalVat(_Ds.Tables[0].Rows[_index]["vatAmount"].ToString());

            aTable.Rows.Add(drProduct);
        }
        drProduct = aTable.NewRow();
        drProduct["quantityReceived"] = "Total";
        drProduct["Amount"] = string.Format("{0:#,##0.00}", (totalAmount));
        aTable.Rows.Add(drProduct);

        drProduct = aTable.NewRow();
        drProduct["qtydue"] = "Tax";
        drProduct["Amount"] = string.Format("{0:#,##0.00}", (TotalVat));
        aTable.Rows.Add(drProduct);

        NetTotalAmount = totalAmount + TotalVat;
        //NetTotalAmount = totalAmount;
        drProduct = aTable.NewRow();
        drProduct["qtydue"] = "Net Total";
        drProduct["Amount"] = string.Format("{0:#,##0.00}", (Math.Round(NetTotalAmount)));
        aTable.Rows.Add(drProduct);

        poLineGrid.DataSource = aTable;
        poLineGrid.DataBind();

        lblNetAmtTextFormat.Text = changeCurrencyToWords(Math.Round(NetTotalAmount));
        //using (IConnection conn = DataConnectionFactory.GetConnection())
        //{

        //    IDataReader _reader;
        //    _reader = conn.ExecuteQueryProc("Lum_erp_Stp_GRNPRINT",
        //                         new SpParam("@poId", _poId, SqlDbType.Int),
        //                         new SpParam("@grnId", _GRNId, SqlDbType.Int));

        //    poLineGrid.DataSource = _reader;
        //    poLineGrid.DataBind();
        //}


    }
    private void CalcTotalAmount(string _netVal)
    {
        double ta = 0;
        if (double.TryParse(_netVal, out ta))
        {
            totalAmount += Double.Parse(_netVal);
        }

    }
    private void CalcTotalVat(string _netVal)
    {
        double ta = 0;
        if (double.TryParse(_netVal, out ta))
        {
            TotalVat += Double.Parse(_netVal);
        }

    }
    protected void poLineGrid_ItemDataBound(object sender, DataGridItemEventArgs e)
    {

    }
    protected void btnCancel_ServerClick(object sender, EventArgs e)
    {
        if (ViewState["previousPage"] != null)
        {
            Response.Redirect(Convert.ToString(ViewState["previousPage"]));
        }

    }
}

