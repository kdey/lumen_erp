﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EventEntry/EventEntry.master" Culture="en-GB" AutoEventWireup="true" CodeFile="GrnWithRefToPo.aspx.cs" Inherits="EventEntry_GrnWithRefToPo" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  
<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
  <script type="text/javascript">

      function letternumber(e) {
          var key;
          var keychar;

          if (window.event)
              key = window.event.keyCode;
          else if (e)
              key = e.which;
          else
              return true;
          keychar = String.fromCharCode(key);
          keychar = keychar.toLowerCase();

          // control keys
          if ((key == null) || (key == 0) || (key == 8) ||
    (key == 9) || (key == 13) || (key == 27))
              return true;

          // alphas and numbers
          else if ((("abcdefghijklmnopqrstuvwxyz0123456789;").indexOf(keychar) > -1))
              return true;
          else
              return false;
      }
      function isNumberKey_WO_Decimal(evt)   //Checks textbox contains only numeric value
      {
          var charCode = (evt.which) ? evt.which : event.keyCode;
          if (charCode > 31 && (charCode < 48 || charCode > 57)) {
              return false;
          }
          return true;
      }
</script>
<input type="hidden" name="grnId" value='<%=_grnId%>' />
<input type="hidden" name="poId" value='<%=_poId%>' />
<div class="PageMargin">
    <div class="PageHeading">Goods Receipt Note With Reference To Purchase Order<a style="float:right" href="GRNList.aspx">GRN List</a></div>
    <div class="Line"></div>
    <!--Content Table Start-->
    <div class="PageContent">
    <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
    <tr><td width="16%" align="right" valign="top">GRN No#</td>
        <td align="left" valign="top" width="20%"><asp:Label ID="grnNumber" runat="server" /></td>
        <td width="16%" align="right" valign="top">GRN Date (dd/mm/yyyy)&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
        <td align="left" valign="top" width="16%"><asp:TextBox ID="grnDate" runat="server" onkeyup="document.getElementById(this.id).value=''"
             autocomplete='off'  CssClass="TextboxSmall" /><br />
            <asp:RequiredFieldValidator ValidationGroup="addGrn" ControlToValidate="grnDate"  ErrorMessage="Field cannot be empty"
             ID="RequiredFieldValidator2" runat="server" />
            <asp:CompareValidator ID="CompareValidator1"  runat="server" ErrorMessage="GRN date Should be greater than  Purchase Order Date" 
             ControlToCompare="poDate" ControlToValidate="grnDate" ValidationGroup="addGrn" Type="Date" Operator="GreaterThanEqual"></asp:CompareValidator>
        </td>
        <td width="16%" align="right" valign="top">Vendor Name</td>
        <td valign="top" align="left" ><asp:Label  ID="ddlVendor" runat="server" Width="150px"></asp:Label></td>
    </tr>
    <tr><td width="16%" align="right" valign="top">Purchase Order No#</td>
        <td align="left" valign="top" width="16%"><asp:DropDownList CssClass="TextboxSmall"  ID="ddlPorder" AutoPostBack="true" runat="server" 
             Width="150px" onselectedindexchanged="ddlPorder_SelectedIndexChanged"  ></asp:DropDownList>
            <asp:Label ID="poNumber" runat="server" Visible="false"></asp:Label>
            <input type="hidden" id="hdPoid" runat="server" /></td>
        <td align="right" valign="top" >Purchase Order Date&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span><br /> (dd/mm/yyyy)</td>
        <td valign="top" align="left" ><asp:TextBox ID="poDate"  CssClass="TextboxSmall" style="border:0;"
             onkeyup="document.getElementById(this.id).value=''"  autocomplete='off' runat="server"></asp:TextBox>
            <cc1:calendarextender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" TargetControlID="poDate"></cc1:calendarextender><br />
            <asp:RequiredFieldValidator ControlToValidate="poDate"  ErrorMessage="Field cannot be empty" ID="RequiredFieldValidator1"
             ValidationGroup="addGrn" runat="server" /></td>
        <td  align="right" valign="top">Select Store&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
        <td  align="left" valign="top"><asp:DropDownList ID="ddlStore" CssClass="TextboxSmall" runat="server"></asp:DropDownList><br />
            <asp:RequiredFieldValidator ValidationGroup="addGrn" InitialValue="0" ControlToValidate="ddlStore" CssClass="ButtonBig"
             ErrorMessage="Select Store" ID="RequiredFieldValidator5"  runat="server" />
            <cc1:calendarextender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="grnDate"></cc1:calendarextender></td>
    </tr>
    <tr><td align="right" valign="top" >Vendor Invoice No#&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
        <td valign="top" align="left" ><asp:TextBox ID="vendorInvoiceNo" MaxLength="50"  CssClass="TextboxSmall" runat="server"></asp:TextBox>
            <br /><asp:RequiredFieldValidator ControlToValidate="vendorInvoiceNo"  ErrorMessage="Field cannot be empty"
             ID="RequiredFieldValidator4" ValidationGroup="addGrn" runat="server" /></td>
        <td  align="right" valign="top">Vendor Invoice Date&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span><br />(dd/mm/yyyy)</td>
        <td  align="left" valign="top"><asp:TextBox ID="invoiceDate" onkeyup="document.getElementById(this.id).value=''" autocomplete='off'
             runat="server"  CssClass="TextboxSmall"  />
            <cc1:calendarextender ID="CalendarExtender3" runat="server" Format="dd/MM/yyyy" TargetControlID="invoiceDate"></cc1:calendarextender>
            <br /><asp:RequiredFieldValidator ValidationGroup="addGrn" ControlToValidate="invoiceDate"  ErrorMessage="Field cannot be empty"
             ID="RequiredFieldValidator6" runat="server" /><br />
            <asp:CompareValidator ID="CompareValidator2"  runat="server" ErrorMessage="Invoice date Should be greater than  Purchase Order Date" 
             ControlToCompare="poDate" ControlToValidate="invoiceDate" ValidationGroup="addGrn" Type="Date" Operator="GreaterThanEqual"></asp:CompareValidator>
        </td>
        <td>&nbsp;</td>
        <td align="left">&nbsp;</td>
    </tr>
    <tr><td align="right" valign="top" >&nbsp;</td>
        <td valign="top" align="left" >&nbsp;</td>
        <td  align="center" valign="top"><asp:Button ID="btnUpdate" runat="server"  Text="Add" CssClass="ButtonBig" onclick="btnUpdate_Click" Visible="false"
             CausesValidation="true" ValidationGroup="addGrn"  /></td>
        <td  align="left" valign="top">&nbsp;</td>
        <td>&nbsp;</td>
        <td align="left"><asp:Label ID="message" runat="server" /></td>
    </tr></table>
    </div>
    <!--Content Table End-->
    <div class="Line"></div>
    <!--Grid Table Start-->
    <div class="PageHeading">Purchase Order Items</div>
    <div class="Line" align="center"></div>
    <div align="right">Serial Number will be  separated by (;)</div>
    <asp:DataGrid ID="poLineGrid" runat="server" Width="100%" AutoGenerateColumns="False" DataKeyField="poLineId"
     HeaderStyle-CssClass="GridHeading" ItemStyle-CssClass="GridData" onitemcommand="poLineGrid_ItemCommand" onprerender="poLineGrid_PreRender" 
     GridLines="None" onitemcreated="poLineGrid_ItemCreated" >
        <ItemStyle CssClass="GridData"></ItemStyle>
        <Columns>
            <asp:BoundColumn DataField="divisionName" HeaderStyle-HorizontalAlign="Left" HeaderText="Division" HeaderStyle-Font-Bold="true" >
                <HeaderStyle HorizontalAlign="Left" Font-Bold="True"></HeaderStyle>
            </asp:BoundColumn>
            <asp:BoundColumn DataField="subdivisionName" HeaderStyle-HorizontalAlign="Left" HeaderText="Sub Division" HeaderStyle-Font-Bold="true" >
                <HeaderStyle HorizontalAlign="Left" Font-Bold="True"></HeaderStyle>
            </asp:BoundColumn>
            <asp:BoundColumn DataField="itemCode"  HeaderStyle-HorizontalAlign="Left" HeaderText="Item" HeaderStyle-Font-Bold="true" >
                <HeaderStyle HorizontalAlign="Left" Font-Bold="True"></HeaderStyle>
            </asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Quantity Due" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true">
                <ItemTemplate>
                <input type="hidden" id="lineId" value='<%# DataBinder.Eval(Container.DataItem, "poLineId") %>' runat="server"  />
                <asp:Label ID="lblQtyOrderd"  Visible="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "quantity") %>' Width="50" />
                <asp:Label ID="lblqtydue"   runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "qtydue") %>' Width="50" />
                <asp:Label ID="LblquantityReceived"  Visible="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "quantityReceived") %>' Width="50" />

                <%#DataBinder.Eval(Container.DataItem, "uomName")%>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" Font-Bold="True"></HeaderStyle>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Quantity Received" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true">
                <ItemTemplate>
                <asp:TextBox ID="itemQtyReceived" MaxLength="6" runat="server" onkeypress="return isNumberKey_WO_Decimal(event);"
                 Text='<%# DataBinder.Eval(Container.DataItem, "quantityReceived") %>' Width="50" />
                <%#DataBinder.Eval(Container.DataItem, "uomName")%>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" Font-Bold="True"></HeaderStyle>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Price" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true">
                <ItemTemplate>
                <asp:Label ID="itemPrice" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "price") %>' Width="60" />
                <input type="hidden" runat="server" id="itemId" value='<%# DataBinder.Eval(Container.DataItem, "itemId") %>' />
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" Font-Bold="True"></HeaderStyle>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Vat %" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true">
                <ItemTemplate>
                <asp:Label ID="vat" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "vat") %>' Width="60" />
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" Font-Bold="True"></HeaderStyle>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="VAT Amount" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true">
                <ItemTemplate>
                <asp:Label ID="vatAmount" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "vatAmount") %>' Width="60" />
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" Font-Bold="True"></HeaderStyle>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Net Value" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true">
                <ItemTemplate>
                <asp:Label ID="total" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "total") %>' Width="60" />
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" Font-Bold="True"></HeaderStyle>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Serial No" HeaderStyle-Font-Bold="true">
                <ItemTemplate>
                <asp:TextBox ID="itemSerialNo" Text='<%# DataBinder.Eval(Container.DataItem, "SerialNo") %>'  runat="server"
                   Width="100" TextMode="MultiLine" Height="50" />
                        <asp:RegularExpressionValidator ID="REVitemSerialNo" runat="server" ControlToValidate="itemSerialNo"
                         ErrorMessage="Input text can contain only alphabets/numerics" SetFocusOnError="True" ValidationGroup="add" Display="None" 
                         ValidationExpression="^[A-Za-z0-9;]+$"></asp:RegularExpressionValidator>
                        <cc1:ValidatorCalloutExtender ID="VCE_REVtxtlname" TargetControlID="REVitemSerialNo" runat="server"></cc1:ValidatorCalloutExtender>

                   
                </ItemTemplate>
                <HeaderStyle Font-Bold="True"></HeaderStyle>
            </asp:TemplateColumn>
            <asp:ButtonColumn ButtonType="PushButton" Text="Update" CommandName="Update" />
            <asp:TemplateColumn Visible="false">
                <ItemTemplate>
                <input type="hidden" id="isDeleted" value='<%#DataBinder.Eval(Container.DataItem, "isDeleted") %>' runat="server" />
                </ItemTemplate>
            </asp:TemplateColumn> 
            <asp:TemplateColumn >
                <ItemTemplate>
                <asp:TextBox Height="0" Width="0" BorderStyle="None" ID="quantityReceived"
                 value='<%#DataBinder.Eval(Container.DataItem, "quantityReceived") %>' runat="server" />
                </ItemTemplate>
            </asp:TemplateColumn> 
        </Columns>
        <HeaderStyle CssClass="GridHeading"></HeaderStyle>
    </asp:DataGrid>
    <table cellspacing="3" class="GridData" cellpadding="0"  border="0"  style="width:100%;">
    <tr ><td width="62%"  colspan="6"> <asp:Label ID="gridMessage" runat="server" /></td>
        <td width="75px"  align="left">Total</td>
        <td width="100px" align="left"><span  runat="server"  id="totalValtamount" ></span></td>
        <td align="left"><span id="totalAmount" runat="server" ></span></td>
        <td></td><td></td>
    </tr>
    <tr><td colspan="11" align="center">
    <asp:Button ID="btnPrint" runat="server" CssClass="Button" onclick="btnPrint_Click" Text="Print" />
    </td></tr>
    </table>
    <input type="hidden" value="0" runat="server" id="strgrnId" />
    <!--Grid Table End-->
    <div class="Line"></div>
    <!--Page Body End-->
</div>
</asp:Content>
