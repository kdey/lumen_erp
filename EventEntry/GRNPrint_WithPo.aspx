﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GRNPrint_WithPo.aspx.cs" Inherits="EventEntry_GRNPrint_WithPo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Lumen Tele System Pvt. Ltd.</title>
    <link href="./../style/style.css" rel="stylesheet" type="text/css" />
 <script type ="text/javascript">
       function print_page() {

            var control = document.getElementById("btnPrint");
           
                control.style.visibility = "hidden";

            var control1 = document.getElementById("btnCancel");
            
                control1.style.visibility = "hidden";
            // else control.style.visibility = "visible";

            /*    var OLECMDID = 7;
            /* OLECMDID values:
            * 6 - print
            * 7 - print preview
            * 1 - open window
            * 4 - Save As
         
         var PROMPT = 1; // 2 DONTPROMPTUSER 
            var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
            document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
            WebBrowser1.ExecWB(OLECMDID, PROMPT);
            WebBrowser1.outerHTML = "";*/
          //  var OLECMDID = 7;
            /* OLECMDID values:
            * 6 - print
            * 7 - print preview
            * 1 - open window
            * 4 - Save As
            */
           // var PROMPT = 1; // 2 DONTPROMPTUSER
          //  var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
           // document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
            //WebBrowser1.ExecWB(OLECMDID, PROMPT);
            //WebBrowser1.outerHTML = "";
        }
        function viewControl() {
            var control = document.getElementById("btnPrint");
            control.style.visibility = "visible";
            var control1 = document.getElementById("btnCancel");
            control1.style.visibility = "visible";
			}
    </script>
    <style type="text/css">
        .style1
        {
            font-size: medium;
            font-style:italic;
            text-decoration:underline;
            font-weight:bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="Header">
        <div class="HeaderImg">
        </div>
    </div>
    <div class="PageContentDiv">
    <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalTextBlack" >
        <tr>
            <td colspan="4" style="font-size:large; text-decoration:underline">Goods Received Note</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
       <%-- <tr>
            <td colspan="4" style="font-size:large; text-decoration:underline">Lumen Tele-Systems Pvt. Ltd.</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="left" class="style1">Registered Office:</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td align="right" class="style1">Sales Office:</td>
        </tr>
        <tr>
            <td align="left">44/12 Garcha Road</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td align="right">30A,Lake Temple Road</td>
        </tr>
        <tr>
            <td align="left">Kolkata-700019</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td align="right">Ground Floor</td>
        </tr>
        <tr>
            <td align="left">Contact : 2461 9932,2476 8985</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td align="right">Kolkata-700029</td>
        </tr>
        <tr>
            <td align="left">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td align="right">Contact : 2461 9932,2476 8985</td>
        </tr>
        <tr>
            <td align="left">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td align="right">&nbsp;</td>
        </tr>--%>
    </table>
    <table class="PageContent" width="100%" border="0px" style="border-color:Black; margin:auto">    
        <tr class="NormalTextBlack">
            <td align="left"><b>GRN Number :&nbsp;<asp:Label ID="GRNNumber" runat="server" /></b></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td align="left">Date : <asp:Label ID="GRNDate" runat="server"></asp:Label></td>
        </tr>
        <%--<tr>
            <td align="left"></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td align="right">&nbsp;</td>
        </tr>--%>
        <tr class="NormalTextBlack">
            <td align="left">Our Ref. No# : <asp:Label ID="PONO" runat="server"></asp:Label>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td align="left">Date : <asp:Label ID="PODate" runat="server"></asp:Label></td>
        </tr>
        <tr class="NormalTextBlack">
            <td align="left">Supplier&#39;s Ref. No# : <b><asp:Label ID="VendorInvoiceNo" runat="server" /></b></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td align="left">Date : <asp:Label ID="VendorInvoiceDt" runat="server"></asp:Label></td>
        </tr>
        <tr class="NormalTextBlack">
            <td align="left">Supplier : <asp:Label ID="Vendorname" runat="server"></asp:Label></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td align="left">&nbsp;</td>
        </tr>
        <tr class="NormalTextBlack">
            <td align="left">Supplier&#39;s Address : <asp:Label ID="Vendoradd" runat="server"></asp:Label></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td align="left">&nbsp;</td>
        </tr>
    </table>
    <br />
    <br />
    <table class="PageContent" width="100%" border="0px" style="border-color:Black; margin:auto">    
        <tr class="NormalTextBlack">
            <td  colspan="4" align="center" style="font-size:small"><b>Transaction Details</b></td>
        </tr>
   </table>
   <table class="PageContent" width="100%" border="0px" style="border-color:Black; margin:auto">     
        <tr class="NormalTextBlack">
            <td colspan="4">
                <asp:DataGrid ID="poLineGrid" runat="server" AutoGenerateColumns="False"
         Width="100%" onitemdatabound="poLineGrid_ItemDataBound" ShowFooter="True" >
                    <Columns>
                        <asp:BoundColumn DataField="row_number" HeaderText="SL No.">
                            <HeaderStyle Font-Bold="True"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="divisionName" HeaderText="Division">
                            <HeaderStyle Font-Bold="True"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="itemCode" HeaderText="Item">
                            <HeaderStyle Font-Bold="True"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="SerialNo" HeaderText="Item Sl. No.">
                            <HeaderStyle Font-Bold="True"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="quantityReceived" HeaderText="Quantity Received">
                            <HeaderStyle Font-Bold="True"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="qtydue" HeaderText="Quantity Due">
                            <HeaderStyle Font-Bold="True"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Amount" HeaderText="Amount(Rs.)">
                            <HeaderStyle Font-Bold="True"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"/>
                        </asp:BoundColumn>
                    </Columns>
                    <HeaderStyle  HorizontalAlign="Center" Font-Size="Small"></HeaderStyle>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
  <table width="100%" class="PageContent" style="border-color:Black;height:20px">     
    <tr class="NormalTextBlack">
        <td align="left" colspan="4">Amount in Words :&nbsp;&nbsp;
            <asp:Label ID="lblNetAmtTextFormat" runat="server" Text=""></asp:Label></td>
    </tr>
</table>
  <br />
  <table class="NormalTextBlack" width="100%" border="0px">      
        <tr>
            <td align="center"   colspan="4" style="height: 23px">
                 <input  id="btnCancel" value="Cancel" type="button" runat="server"
           
            style="width: 141px; cursor: hand;" onserverclick="btnCancel_ServerClick" />
                <input  id="btnPrint" value="Print" type="button"
            onclick="print_page();javascript:window.print(); viewControl();" 
            style="width: 141px; cursor: hand;" /></td>
        </tr>
        <tr>
            <td align="right" colspan="4" style="width: 60%">for&nbsp;<b>Lumen Tele-Systems (P) Limited</b></td>
        </tr>
        <tr>
            <td align="right" colspan="4" style="width: 60%"><b><br /><br /><br /><br />Authorised Signatory</b></td>
        </tr>
        <tr>
            <td align="left" colspan="4" style="width: 60%"><b>Supplier&#39;s VAT No : </b>
                <asp:Label ID="VendorVATNo" runat="server" Text=""></asp:Label></td>
        </tr>
        <tr>
            <td align="left" colspan="4" style="width: 60%"><b>Supplier&#39;s CST No : </b>
                <asp:Label ID="VendorCSTNo" runat="server" Text=""></asp:Label></td>
        </tr>
        <tr>
            <td align="center" colspan="4" style="width: 60%; font-style:italic; font-weight:normal"><br /><br /><br /><br />This is Computer Generated GRN</td>
        </tr>
        <tr>
            <td align="center" colspan="4" style="width: 60%; font-style:italic; font-weight:normal">System Designed & Developed by Bitscrape Solution Pvt.Ltd.,www.bitscrape.com</td>
        </tr>
    </table>
</div>
  </form>
</body>
</html>
