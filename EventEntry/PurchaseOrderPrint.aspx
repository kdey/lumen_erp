﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PurchaseOrderPrint.aspx.cs" Inherits="EventEntry_PurchaseOrderPrint" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Lumen Tele System Pvt. Ltd.</title>
    <link href="./../style/style.css" rel="stylesheet" type="text/css" />
   
 <script type ="text/javascript">
       function print_page() {

            var control = document.getElementById("btnPrint");
           
                control.style.visibility = "hidden";

            var control1 = document.getElementById("btnCancel");
            
                control1.style.visibility = "hidden";
            // else control.style.visibility = "visible";

            /*    var OLECMDID = 7;
            /* OLECMDID values:
            * 6 - print
            * 7 - print preview
            * 1 - open window
            * 4 - Save As
         
         var PROMPT = 1; // 2 DONTPROMPTUSER 
            var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
            document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
            WebBrowser1.ExecWB(OLECMDID, PROMPT);
            WebBrowser1.outerHTML = "";*/
          //  var OLECMDID = 7;
            /* OLECMDID values:
            * 6 - print
            * 7 - print preview
            * 1 - open window
            * 4 - Save As
            */
           // var PROMPT = 1; // 2 DONTPROMPTUSER
          //  var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
           // document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
            //WebBrowser1.ExecWB(OLECMDID, PROMPT);
            //WebBrowser1.outerHTML = "";
        }
        function viewControl() {
            var control = document.getElementById("btnPrint");
            control.style.visibility = "visible";
            var control1 = document.getElementById("btnCancel");
            control1.style.visibility = "visible";
			}
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="Header">
        <div class="HeaderImg">
        </div>
    </div>
    <!--Header End-->
    <div class="PageContentDiv">
        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalTextBlack" >
            <tr class="NormalTextBlack">
                <td align="left" >&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td align="right">&nbsp;</td>
            </tr>
            <tr class="NormalTextBlack">
                <td colspan="4" align="center" style="font-size:large">Purchase Order</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <table class="PageContent" width="100%" border="0px" style="border-color:Black; margin:auto">
             <tr class="NormalTextBlack">
                <td align="left" style="width:50%"><b>PO Number :&nbsp;&nbsp;&nbsp; 
                    <asp:Label ID="poNumber" runat="server" /> </b></td>
                <td align="left" style="width:50%"><b>Date :&nbsp;&nbsp;&nbsp; </b><asp:Label ID="poDate" runat="server"></asp:Label>
                </td>
             </tr>
        </table>
        <table class="PageContent" width="100%" style="border-color:Black;">
             <tr class="NormalTextBlack">
                <td style="width:100%" align="left"><b>Supplier&#39;s Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b><asp:Label ID="vendorDdl" runat="server" Text=""></asp:Label></td>
             </tr>
             <tr class="NormalTextBlack">
                <td style="width:100%" align="left"><b>Supplier&#39;s Address:&nbsp;&nbsp;&nbsp; </b><asp:Label ID="vendorAddress" runat="server"></asp:Label></td>
             </tr>
             <tr class="NormalTextBlack">
             <td>&nbsp;</td><td>&nbsp;          
                   </td><td>&nbsp;</td><td>&nbsp;</td>
             </tr>
        </table>
        <br />
        <br />
        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0"class="PageContent" style="border-color:Black;">
             <tr class="NormalTextBlack">
                <td colspan="2">
                  <asp:DataGrid ID="poLineGrid" runat="server" AutoGenerateColumns="False"
        DataKeyField="poLineId" Width="100%" onitemdatabound="poLineGrid_ItemDataBound" ShowFooter="True" >
        <Columns>
            <asp:TemplateColumn  HeaderText="Model No/Part No" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Left"
             ItemStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <asp:Label ID="lblModelPartNo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "itemCode") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" Font-Bold="True" Height="30px"  Width= "15%"></HeaderStyle>
                <ItemStyle HorizontalAlign="Left"  Width= "15%"></ItemStyle>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Item Description" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Left"
             ItemStyle-HorizontalAlign="Left">
                <ItemTemplate>
                     <asp:Label ID="itemDescription" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "itemDescription") %>'></asp:Label>
                     <%--<asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "itd") %>'></asp:Label><br />
                     <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SerialNo") %>'></asp:Label><br />--%>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" Font-Bold="True" Height="30px"  Width= "20%"></HeaderStyle>
                <ItemStyle HorizontalAlign="Left" Width= "20%"></ItemStyle>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Quantity" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Bold="true">
                <ItemTemplate>
                    <asp:Label ID="itemQty" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "quantity") %>'  />&nbsp;
                    <%#DataBinder.Eval(Container.DataItem, "uomName")%>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Right" Font-Bold="True" Height="30px"  Width= "15%"></HeaderStyle>
                <ItemStyle HorizontalAlign="Right" Width= "15%"></ItemStyle>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Rate(Rs.)" >
                <ItemTemplate>
                    <asp:Label ID="itemPrice" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "price") %>'   />
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Right" Font-Bold="True" Height="30px" Width= "30%"></HeaderStyle>
                <ItemStyle   Width= "30%" HorizontalAlign="Right"></ItemStyle>
            </asp:TemplateColumn>
            <asp:TemplateColumn Visible="true" HeaderText="Amount(Rs.)" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Bold="true">
                <ItemTemplate>
                    <asp:Label ID="total" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "total") %>'  />
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Right" Font-Bold="True" Height="30px"  Width= "20%"></HeaderStyle>
                <ItemStyle HorizontalAlign="Right" Width= "20%"></ItemStyle>
            </asp:TemplateColumn>
        </Columns>
        <HeaderStyle  HorizontalAlign="Center"></HeaderStyle>
    </asp:DataGrid>
                 </td>
             </tr>
             <tr class="NormalTextBlack">
                <td align="right" style="width:85%">&nbsp;</td>
                <td align="right" style="width:100%">
                </td>
             </tr>
        </table>
        <table width="100%" class="PageContent" style="border-color:Black;height:20px">     
            <tr class="NormalTextBlack">
                <td align="left" colspan="4">Amount in Words :&nbsp;&nbsp;
                    <asp:Label ID="lblPrice" runat="server"></asp:Label></td>
            </tr>
        </table>         
       <table class="NormalTextBlack" width="100%">
                <tr class="NormalTextBlack">
                    <td colspan="2">&nbsp;
                        </td>
                    </tr>
                <tr class="NormalTextBlack">
                    <td colspan="2">
                        <input  id="btnCancel" value="Cancel" type="button" runat="server"
                                style="width: 141px; cursor: hand;"  onserverclick="btnCancel_ServerClick"/>
                                    <input  id="btnPrint" value="Print" type="button"
                                onclick="print_page();javascript:window.print(); viewControl();" 
                                style="width: 141px; cursor: hand;" />
                    </td>
                 </tr>
                 <tr class="NormalTextBlack">
                    <td align="left" style="width:20%"><b>TIN (W.B.) :</b> </td>
                    <td align="left" style="width:80%"><b><asp:Label ID="lblTinNo" runat="server" Text=""></asp:Label></b></td>
                 </tr>
                 <tr class="NormalTextBlack">
                    <td align="left" style="width:20%"><b>CST No. :</b></td>
                    <td align="left" style="width:80%"><b><asp:Label ID="lblCSTNo" runat="server" Text=""></asp:Label></b></td>
                 </tr>
                 <tr class="NormalTextBlack">
                    <td align="left" style="width:20%"><b>Service Tax No. :</b></td>
                    <td align="left" style="width:80%"><b><asp:Label ID="lblServiceTaxRegistNo" runat="server" Text=""></asp:Label></b></td>
                 </tr>
                 <tr class="NormalTextBlack">
                    <td colspan="2"></td>
                 </tr>
                 <tr class="NormalTextBlack">
                    <td colspan="2"></td>
                 </tr>
                 <tr class="NormalTextBlack">
                    <td align="left" style="width:20%"><b>Supplier&#39;s VAT No. :</b></td>
                    <td align="left" style="width:80%"><b><asp:Label ID="supvat" runat="server" Text=""></asp:Label></b></td>
                 </tr>
                 <tr class="NormalTextBlack">
                    <td colspan="2"></td>
                 </tr>
             </table>
             <br />
             <br />    
             <table class="NormalTextBlack" width="100%">    
                 <tr class="NormalTextBlack">
                    <td align="right" style="width:100%">for&nbsp;<strong>Lumen Tele System (P) Limited</strong></td>
                 </tr>
                 <tr class="NormalTextBlack">
                    <td align="right" style="width:100%"><strong><br /><br /><br /><br />Authorised Signatory</strong></td>
                 </tr>
                 <tr class="NormalTextBlack">
                    <td align="center" colspan="4" style="width: 60%; font-style:italic"><br /><br /><br /><br />This is Computer Generated Purchase Order</td>
                </tr>
                 <tr>
                    <td align="center" colspan="4" style="width: 60%; font-style:italic; font-weight:normal">System Designed & Developed by Bitscrape Solution Pvt.Ltd.,www.bitscrape.com</td>
                </tr>
            </table>         
    </div>
    <!--Footer Start-->
   
    </form>
</body>
</html>
