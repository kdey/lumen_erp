﻿using System.Data;
using System;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;


public partial class EventEntry_ServiceList : BasePage
{
    int serviceId = -1;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!IsPostBack)
        {
            BindGrid();
            
        }
        //if (GetLoggedinUser().UserType == 2)
        //{
        //    newServiceList.Visible  = false;
        //}
    }
    private void BindGrid()
    {
        string chkcondition = " AND S.isDeleted =0  ";
        if (Chkblock_despath.Checked == true)
            chkcondition = " AND S.isDeleted =1 ";




        chkcondition = @"SELECT S.serviceId,S.serialNo,S.customerOrderNo,C.customerName ,S.isDeleted,s.strServiceId as servicebillno
                                FROM Lum_Erp_Services S,Lum_Erp_Customer C
                                WHERE C.customerId=S.customerId  "
                +
                chkcondition
                +
                @" order by S.serialNo desc ";

        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(chkcondition))
            {
                serviceGrid.DataSource = reader;
                serviceGrid.DataBind();
            }
        }
    }
    protected void serviceGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
                     serviceId = (int)serviceGrid.DataKeys[e.Item.ItemIndex];
                        if (e.CommandName.Equals("View"))
                        {

                            Response.Redirect("service.aspx?serviceId=" + serviceId + "&View=View");
                        }  
            
                        else if (e.CommandName.Equals("Edit"))
                        {
                           

                            Response.Redirect("service.aspx?serviceId=" + serviceId);

                         }
                        else if (e.CommandName.Equals("Delete"))
                        {
                            serviceId = (int)serviceGrid.DataKeys[e.Item.ItemIndex];
                           
                            using (IConnection conn = DataConnectionFactory.GetConnection())
                                      {

                                try
                                {
                                   SpParam[] _Sparray = new SpParam[3];

                                _Sparray[0] = new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.VarChar);
                                _Sparray[1] = new SpParam("@activityDateTime", System.DateTime.Now, SqlDbType.DateTime);
                                _Sparray[2] = new SpParam("@serviceId", serviceId, SqlDbType.Int);
                                int _i = conn.ExecuteCommandProc("Lum_Erp_Stp_DeleteServices", _Sparray);
                                if (_i == -1)
                                {
                                    message.Text = "Reversal cannot be done , Payment is not reversed !!";
                                    message.ForeColor = Color.Red;


                                }
                                    if (_i == 2)
                                    {
                                        message.Text = "Service has been Blocked";
                                        message.ForeColor = Color.Green;

                                  
                                     }
                                }
                                catch (Exception ex)
                                {
                                    message.Text = ex.Message;
                                }
                           
                            BindGrid();
                        }
                    }
                
            
        
    }
    protected void serviceGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton btnview = (LinkButton)e.Item.Cells[4].Controls[0];
            LinkButton btnedit = (LinkButton)e.Item.Cells[5].Controls[0];
            LinkButton btn = (LinkButton)e.Item.Cells[6].Controls[0];
            btn.Attributes.Add("onclick", "return confirm('Are you sure you want to Reverse this?')");
            btn.CssClass = "GridLink";
            btnview.CssClass = "GridLink";
            btnedit.CssClass = "GridLink";

            if (GetLoggedinUser().UserType == 2)
            {
                btnedit.Visible = false;
                btn.Visible = false;
                btnview.Visible = true;
            }
        }
    }
    protected void newGrn_Click(object sender, EventArgs e)
    {
        Response.Redirect("Service.aspx");
    }


    protected void serviceGrid_PreRender(object sender, EventArgs e)
    {
        for (int _index = 0; _index < serviceGrid.Items.Count; _index++)
        {
            LinkButton btnview = (LinkButton)serviceGrid.Items[_index].Cells[4].Controls[0];
            LinkButton btnedit = (LinkButton)serviceGrid.Items[_index].Cells[5].Controls[0];
            LinkButton btn = (LinkButton)serviceGrid.Items[_index].Cells[6].Controls[0];

            HtmlInputHidden _hdel = (HtmlInputHidden)serviceGrid.Items[_index].FindControl("isdeleted");
            if (_hdel.Value == "True")
            {
                btnedit.Visible = false;
                btn.Visible = false;
            }
        }
    }
    protected void btngo_Click(object sender, EventArgs e)
    {
        BindGrid();
    }

}
