﻿using System.Data;
using System;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;


public partial class EventEntry_POrderList : BasePage
{
    int poId = -1;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!IsPostBack)
        {
            BindGrid();
        }

    }
    private void BindGrid()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(@"SELECT P.poId,P.strPoId,P.poDate,O.orderCode,V.vendorName FROM Lum_Erp_PoOrder P,Lum_Erp_OrderType O,Lum_Erp_Vendor V

WHERE P.vendorId=V.vendorId AND P.poType=O.orderTypeId"))
            {
                poGrid.DataSource = reader;
                poGrid.DataBind();
            }
        }

    }
    protected void poGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName.Equals("Edit"))
        {
            poId = (int)poGrid.DataKeys[e.Item.ItemIndex];

            Response.Redirect("PurchaseOrder.aspx?poId=" + poId);

        }
        else if (e.CommandName.Equals("Delete"))
        {
            poId = (int)poGrid.DataKeys[e.Item.ItemIndex];
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                try
                {
                    conn.ExecuteCommand("delete from Lum_Erp_PoDetails where poId = " + poId);
                    conn.ExecuteCommand("delete from Lum_Erp_PoOrder where poId = " + poId);
                   
                    message.Text = "Purchase Order deleted";
                    message.ForeColor = Color.Green;
                }
                catch
                {
                    message.Text = "Purchase Order cannot be deleted because of existing related data";
                    message.ForeColor = Color.Red;
                }

                poId = -1;
            }
            BindGrid();
        }
    }
}
