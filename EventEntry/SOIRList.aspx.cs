﻿using System.Data;
using System;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;

public partial class EventEntry_SOIRList : BasePage
{
    int soirId = -1;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!IsPostBack)
        {
            BindGrid();
            
        }
        //if (GetLoggedinUser().UserType == 2)
        //{

        //    newSoir.Enabled = false;

        //}
    }
    private void BindGrid()
    {
        string chkcondition = " AND S.isDeleted =0  ";
        if (Chkblock_despath.Checked == true)
            chkcondition = " AND S.isDeleted =1 ";




        chkcondition = @"SELECT S.isLocked,  S.soirId,S.raisedBy,(S.strSoirId  +'/'+convert(varchar,S.soirVersion)) as strSoirId,C.customerName ,
                        CONVERT(VARCHAR,activityDateTime,106) SOIRDT   ,S.isDeleted FROM Lum_Erp_SOIR S ,Lum_Erp_SoirActivityLog SL ,Lum_Erp_Customer C 
							WHERE C.customerId=S.customerId  AND
							S.soirId = SL.soirId   AND
							SL.activityTypeId =1 AND createdByUser =1 AND
                            S.soirid>=1 "
                +
                chkcondition
                +
                @"order by S.soirId ";

        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(chkcondition))
            {
                soirGrid.DataSource = reader;
                soirGrid.DataBind();

            }
        }


    }
    protected void soirGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {

                     soirId = (int)soirGrid.DataKeys[e.Item.ItemIndex];

                        if (e.CommandName.Equals("Edit"))
                        {
                           
                            Response.Redirect("SOIR.aspx?soirId=" + soirId);

                        }
                        else if (e.CommandName.Equals("View"))
                        {

                            Response.Redirect("SOIR.aspx?soirId=" + soirId+"&View=View");

                        }
                        else if (e.CommandName.Equals("Delete"))
                        {
                            soirId = (int)soirGrid.DataKeys[e.Item.ItemIndex];
                            
                                try
                                {
                                     using (IConnection conn = DataConnectionFactory.GetConnection())
                                   {
                                       int i = conn.ExecuteCommandProc("Lum_Erp_Stp_DeleteSoir",
                                                              new SpParam("@soirId", soirId, SqlDbType.Int),
                                                              new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int));
                                     }
                                    message.Text = "SOIR  has been Blocked";
                                    message.ForeColor = Color.Green;
                                }
                                catch(Exception ex)
                                {
                                    message.Text = ex.Message;
                                    message.ForeColor = Color.Red;
                                }

                                soirId = -1;
                            
                            BindGrid();
                        }
                   
            
        
    }
    protected void soirGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton btnview = (LinkButton)e.Item.Cells[4].Controls[0];
            LinkButton btnedit = (LinkButton)e.Item.Cells[5].Controls[0];
            LinkButton btn = (LinkButton)e.Item.Cells[6].Controls[0];
            btn.Attributes.Add("onclick", "return confirm('Are you sure you want to Block this?')");
            btn.CssClass = "GridLink";
            btnview.CssClass = "GridLink";
            btnedit.CssClass = "GridLink";
            if (GetLoggedinUser().UserType == 2)
            {
               
                btnedit.Visible = false;
                btn.Visible = false;
            }
        }

    }
    protected void newSoir_Click(object sender, EventArgs e)
    {
        Response.Redirect("soir.aspx");
    }
    protected void soirGrid_PreRender(object sender, EventArgs e)
    {
        for (int _index = 0; _index < soirGrid.Items.Count; _index++)
        {
            LinkButton btnview = (LinkButton)soirGrid.Items[_index].Cells[4].Controls[0];
            LinkButton btnedit = (LinkButton)soirGrid.Items[_index].Cells[5].Controls[0];
            LinkButton btn = (LinkButton)soirGrid.Items[_index].Cells[6].Controls[0];
            HtmlInputHidden _hd = (HtmlInputHidden)soirGrid.Items[_index].FindControl("isLocked");
            HtmlInputHidden _hdel = (HtmlInputHidden)soirGrid.Items[_index].FindControl("isdeleted");


            HtmlInputHidden _type = (HtmlInputHidden)soirGrid.Items[_index].FindControl("type");
            if (_hd.Value == "True")
            {
                btnedit.Text = "Locked";
                btnedit.Enabled = false;
            }
            if (_hdel.Value == "True")
            {
                btn.Text = "Blocked";
                btn.Enabled = false;
                if (btnedit.Text != "Locked")
                {
                    btnedit.Text = "";
                    btnedit.Visible = false;
                }
            }
            else
            {
                if (_hd.Value == "True")
                {
                    btn.Visible = false;
                }
            }
            if (GetLoggedinUser().UserType == 2)
            {
                btnedit.Visible = false;
                btn.Visible = false;
                btnview.Visible = true;
            }
        }


        //for (int _index = 0; _index < soirGrid.Items.Count; _index++)
        //{
        //    LinkButton btnedit = (LinkButton)soirGrid.Items[_index].Cells[4].Controls[0];
        //    HtmlInputHidden _hd = (HtmlInputHidden)soirGrid.Items[_index].FindControl("isLocked");
        //    if (_hd.Value == "True")
        //    {
        //        btnedit.Text = "View";
        //    }
        //}
    }
    protected void btngo_Click(object sender, EventArgs e)
    {
        BindGrid();
    }
}
