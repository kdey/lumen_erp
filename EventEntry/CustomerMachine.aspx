﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EventEntry/EventEntry.master" AutoEventWireup="true" CodeFile="CustomerMachine.aspx.cs" Inherits="EventEntry_CustomerMachine" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="PageMargin">
    <div class="PageHeading">Customer Machine </div>
    <div class="Line"><asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager></div>
    <!--Content Table Start-->
    <div class="PageContent">
        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
        <tr><td width="16%" align="Right" valign="top"> Select Customer&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="18%" align="left" valign="middle">
                <asp:DropDownList ID="ddlCustomer" CssClass="TextboxSmall" runat="server" Width="150px"></asp:DropDownList><br />
                <asp:RequiredFieldValidator ControlToValidate="ddlCustomer" ErrorMessage="Select Customer" ID="CustomerValidator"
                 InitialValue="0" runat="server" /></td>
            <td width="16%" align="Right" valign="top"> Select Model&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="17%" align="left" valign="middle">
                <asp:DropDownList CssClass="TextboxSmall" ID="ddlItem" runat="server" Width="150px"></asp:DropDownList><br />
                <asp:RequiredFieldValidator ControlToValidate="ddlItem" ErrorMessage="Select Model" ID="ModelValidator"
                 InitialValue="0" runat="server" /></td>
            <td width="16%" align="Right" valign="top"> Machine Serial No.&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="17%" align="left" valign="middle"><asp:TextBox ID="txtSRNo" runat="server" CssClass="TextboxSmall" MaxLength="50" /><br />
                <asp:RequiredFieldValidator ControlToValidate="txtSRNo" ErrorMessage="Field cannot be empty" ID="SRNoValidator" runat="server" /></td>
        </tr>
        <tr><td align="center" colspan="6" valign="middle">
                <asp:Button ID="add" Text="Add" runat="server" OnClick="add_Click" CssClass="Button" />&nbsp;
                <asp:Button ID="cancel" Text="Cancel" runat="server" OnClick="cancel_Click" CssClass="Button" /></td>
        </tr>
        <tr><td align="left" valign="middle" colspan="6"><input type="hidden" id="Id" runat="server" />
                <asp:Label ID="message" runat="server" /></td>
        </tr></table>
    </div>
    <!--Content Table End-->
    <div class="Line"></div>
    <!--Grid Table Start-->
    <asp:DataGrid ID="gvCustMachine" runat="server" AutoGenerateColumns="false" DataKeyField="custMacId" CellPadding="0"
     OnItemCommand="gvCustMachine_ItemCommand" HeaderStyle-CssClass="GridHeading" ItemStyle-CssClass="GridData" 
     onitemcreated="gvCustMachine_ItemCreated" GridLines="None" Width="100%">
        <ItemStyle CssClass="GridData" HorizontalAlign="Center"></ItemStyle>
        <Columns>
            <asp:BoundColumn DataField="customerName" HeaderText="Customer" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="left" />
            <asp:BoundColumn DataField="itemCode" HeaderText="Model" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="left" />
            <asp:BoundColumn DataField="SRNo" HeaderText="Serial No." HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="left" />
            <%--<asp:ButtonColumn  ButtonType="LinkButton" CausesValidation="false" 
            CommandName="View" Text="View" 
            ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="GridLink" 
            ItemStyle-Width="10%" >
            <ItemStyle HorizontalAlign="Left" CssClass="GridLink" Width="10%"></ItemStyle>
            </asp:ButtonColumn>--%>
            <asp:ButtonColumn  ButtonType="LinkButton" CommandName="Edit" Text="Edit" ItemStyle-CssClass="GridLink" ItemStyle-Width="10%">
                <ItemStyle CssClass="GridLink"></ItemStyle></asp:ButtonColumn>
            <asp:ButtonColumn ButtonType="LinkButton"  CommandName="Delete" Text="Delete" ItemStyle-CssClass="GridLink" ItemStyle-Width="10%">
                <ItemStyle CssClass="GridLink"></ItemStyle></asp:ButtonColumn>
        </Columns>
        <HeaderStyle CssClass="GridHeading"></HeaderStyle>
    </asp:DataGrid>
    <!--Grid Table End-->
    <div class="Line"></div>
    <!--Page Body End-->
</div>
</asp:Content>

