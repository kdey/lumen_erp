﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;

public partial class EventEntry_SOIRPrint :BasePage
{
    protected int _soirId = -1;
  //  private double totalAmount = 0;
    protected int _isLocked = -1;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!string.IsNullOrEmpty(Request["SOIRId"]))
        {
            _soirId = int.Parse(Request["SOIRId"]);
        }
        //if (!string.IsNullOrEmpty(Request["poId"]))
        //{
        //    _poId = int.Parse(Request["poId"]);
        //}
        if (ViewState["previousPage"] == null)
        {
            ViewState["previousPage"] = Convert.ToString(Request.UrlReferrer);
        }
        if (!string.IsNullOrEmpty(Request["isLocked"]))
        {
            _isLocked = int.Parse(Request["isLocked"]);
        }
        if (!this.IsPostBack)
        {
            try
            {
                raisedBy.Text = getUserName();
                if (_soirId > 0)
                {

                    LoadSoir();
                    using (IConnection conn = DataConnectionFactory.GetConnection())
                    {
                        BindGrid(conn);
                    }
                }
            }
            catch (Exception ex)
            { }
        }
    }
    public string getUserName()
    {
        string retVal = string.Empty;

        string sql = string.Format(@"SELECT (firstName+' '+lastName) as name FROM Lum_Erp_AdminUser WHERE adminuserId={0}  ", GetLoggedinUser().UserId);
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(sql))
            {
                if (reader.Read())
                {

                    retVal = Convert.ToString(reader["name"]);

                }
            }
        }
        return retVal;
    }
    private void BindGrid(IConnection conn)
    {
        string fromDate = Convert.ToDateTime(soirdt.Text).ToString("dd MMM yyyy");
//        string sql = @"select soirLineId,sd.itemId, d.divisionName, s.subdivisionName,so.*,i.itemDescription,i.itemCode, sd.quantity,u.uomName, sd.price, i.vat, 
//                        sd.discount,(sd.price * sd.quantity-sd.discount)* 0.01 * i.vat 'vatAmount',(sd.price * sd.quantity-sd.discount)+ (sd.price * sd.quantity-sd.discount)* 0.01 * i.vat as total 
//                        from Lum_Erp_SoirItemDetails sd 
//                        inner join Lum_Erp_Item i on sd.itemId = i.itemId
//                        inner join Lum_Erp_Subdivision s on s.subdivisionId = i.subdivisionId
//                        inner join Lum_Erp_Division d on d.divisionId = s.divisionId
//                        inner join Lum_Erp_UOM u on i.uomId = u.uomId
//inner join Lum_Erp_SOIR so on so.soirId = sd.soirId
//                        where sd.itemType=0 and so.soirId = " + _soirId;
        string sql1 = @"select soirLineId,sd.itemId, d.divisionName, s.subdivisionName,so.*,i.itemDescription,i.itemCode, sd.quantity,u.uomName, sd.price, ((   
                              
                             SELECT Vat
							 FROM [dbo].[_Lum_Erp_VatDetails]
								Where 
								(('" + fromDate + "'>=FromDate AND '" + fromDate + "'< =ToDate)) AND itemType=i.itemType) ) AS vat,  sd.discount, (sd.price * sd.quantity-sd.discount)* 0.01 * ( SELECT Vat FROM [dbo].[_Lum_Erp_VatDetails] WHERE (('" + fromDate + "'>=FromDate AND '" + fromDate + "'< =ToDate)) AND itemType=i.itemType) 'vatAmount',(sd.price * sd.quantity-sd.discount)+ (sd.price * sd.quantity-sd.discount)* 0.01 * ( SELECT Vat FROM [dbo].[_Lum_Erp_VatDetails] WHERE (('" + fromDate + "'>=FromDate AND '" + fromDate + "'< =ToDate)) AND itemType=i.itemType) as total from Lum_Erp_SoirItemDetails sd  inner join Lum_Erp_Item i on sd.itemId = i.itemId inner join Lum_Erp_Subdivision s on s.subdivisionId = i.subdivisionId inner join Lum_Erp_Division d on d.divisionId = s.divisionId inner join Lum_Erp_UOM u on i.uomId = u.uomId inner join Lum_Erp_SOIR so on so.soirId = sd.soirId  where sd.itemType=0 and so.soirId = " + _soirId;
        using (IDataReader reader = conn.ExecuteQuery(sql1))
        {
           
            poLineGrid.DataSource = reader;
            poLineGrid.DataBind();
        }
    }
    public void LoadSoir()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(@"select s.*,c.*,CONVERT(VARCHAR,installationDate,106) AS IDT from Lum_Erp_Soir s,Lum_Erp_Customer c 
                                            WHERE c.customerId=s.customerId and soirId=" + _soirId))
//            using (IDataReader reader = conn.ExecuteQuery(@"select SOIRID,raisedBy,CONVERT(VARCHAR,installationDate,106) AS installationDate,strSoirId ,customerName,Billing_address1,Shipping_address1,Billing_phoneNo,Shipping_phoneNo
//                            from Lum_Erp_Soir s,Lum_Erp_Customer c WHERE c.customerId=s.customerId and soirId=" + _soirId))

            {
                if (reader.Read())
                {
                    

                    raisedBy.Text = Convert.ToString(reader["raisedBy"]);
                    soirdt.Text = Convert.ToString(reader["IDT"]);

                    soirNumber.Text = Convert.ToString(reader["strSoirId"]);// +"/" + Convert.ToString(reader["soirVersion"]);

                    CustomerNameAdrress.Text = Convert.ToString(reader["customerName"]) + "&nbsp;" + "<br>" + Convert.ToString(reader["Billing_address1"]) + "&nbsp;" + "<br>" + Convert.ToString(reader["Billing_address2"]) + " , " + Convert.ToString(reader["Billing_zipCode"]);
                    CosigneeNameAddress.Text = Convert.ToString(reader["customerName"]) + "&nbsp;" + "<br>" + Convert.ToString(reader["Shipping_address1"]) + "&nbsp;" + "<br>" + Convert.ToString(reader["Shipping_address2"]) + " , " + Convert.ToString(reader["Shipping_zipCode"]);
                    ContactPersonWithPhoneNo.Text = Convert.ToString(reader["customerName"]) + "&nbsp;" + "<br>" + Convert.ToString(reader["Billing_phoneNo"]);
                    ContactPersonwithPhCosignee.Text = Convert.ToString(reader["customerName"]) + "&nbsp;" + "<br>" + Convert.ToString(reader["Shipping_phoneNo"]);
                    vat.Text = Convert.ToString(reader["vatRegistrationNo"]);
                    cst.Text = Convert.ToString(reader["cstRegistrationNo"]);

                }
            }
            string fromDate = Convert.ToDateTime(soirdt.Text).ToString("dd MMM yyyy");
            string sql = @"select soirLineId,sd.itemId, d.divisionName, s.subdivisionName,so.*,convert(varchar,so.installationDate,111)as Idate,i.itemDescription,i.itemCode, sd.quantity,u.uomName, sd.price, ( SELECT Vat FROM [dbo].[_Lum_Erp_VatDetails] WHERE (('" + fromDate + "'>=FromDate AND '" + fromDate + "'< =ToDate)) AND itemType=i.itemType) AS vat,i.cst,w.warrantyDescription, sd.discount,(sd.price * sd.quantity-sd.discount)* 0.01 * ( SELECT Vat FROM [dbo].[_Lum_Erp_VatDetails] WHERE (('" + fromDate + "'>=FromDate AND '" + fromDate + "'< =ToDate)) AND itemType=i.itemType) 'vatAmount',(sd.price * sd.quantity-sd.discount)+ (sd.price * sd.quantity-sd.discount)* 0.01 * ( SELECT Vat FROM [dbo].[_Lum_Erp_VatDetails] WHERE (('" + fromDate + "'>=FromDate AND '" + fromDate + "'< =ToDate)) AND itemType=i.itemType) as total from Lum_Erp_SoirItemDetails sd inner join Lum_Erp_Item i on sd.itemId = i.itemId inner join Lum_Erp_Subdivision s on s.subdivisionId = i.subdivisionId inner join Lum_Erp_Division d on d.divisionId = s.divisionId inner join Lum_Erp_UOM u on i.uomId = u.uomId inner join Lum_Erp_SOIR so on so.soirId = sd.soirId inner join Lum_Erp_Warranty w on w.warrantyId = so.warrantyId where sd.itemType=0 and so.soirId = " + _soirId;

                    DataSet _ds = conn.GetDataSet(sql, "Information");

                    if (_ds.Tables[0].Rows.Count != 0)
                    {
                        deliverydate.Text = _ds.Tables[0].Rows[0]["Idate"].ToString();
                        installationdate.Text = _ds.Tables[0].Rows[0]["Idate"].ToString();
                        afterSalesService.Text = "";
                        paymentterms.Text = _ds.Tables[0].Rows[0]["paymentTerms"].ToString();
                        warranty.Text = _ds.Tables[0].Rows[0]["warrantyDescription"].ToString();
                        FreeCopies.Text = "";
                    }
             
        }
    }

    protected void btnCancel_ServerClick(object sender, EventArgs e)
    {
        if (ViewState["previousPage"] != null)
        {
            Response.Redirect(Convert.ToString(ViewState["previousPage"]));
        }

    }
    protected void poLineGrid_ItemDataBound(object sender, DataGridItemEventArgs e)
    {

    }
    protected void poLineGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {

    }
}
