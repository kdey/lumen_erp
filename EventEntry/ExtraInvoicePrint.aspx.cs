﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;

public partial class EventEntry_ExtraInvoicePrint : BasePage
{
    protected int _soirId = -1;
    protected int _challanId = -1;
    private double totalprice = 0;
    private double totalAmount = 0;

    private double TotalVat = 0;
    private decimal TotalQty = 0;
    private int taxType = 1;
    private int _storeid = -1;
    private int customerId = -1;
    double Charge = 0;
    double Buyback = 0;
    private double totaldiscount = 0;
    private string buyBackDetails = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (ViewState["previousPage"] == null)
        {
            ViewState["previousPage"] = Convert.ToString(Request.UrlReferrer);
        }
        if (!string.IsNullOrEmpty(Request["challanId"]))
        {
            _challanId = int.Parse(Request["challanId"]);
        }

        if (!this.IsPostBack)
        {
            if (_challanId > 0)
            {

                LoadChallan();
            }
        }

    }
    public void LoadChallan()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(@"SELECT storeId,C.challanId,C.CNDetails,T.transporterName,CONVERT(VARCHAR,activityDateTime,106) CHALLANDATE,strChallanId,S.soirId,taxTypeId,FormRecoverableId,challanVersion,challanStatus,strSoirId
                        ,customerId,customerPoNo,CONVERT(VARCHAR,poDate,106) poDate,orderTypeId,packingCharges,buyBackAmount,buyBackDetails,installationDate
                        FROM Lum_Erp_ChallancumInvoice C , Lum_Erp_SOIR S,Lum_Erp_ChallanActivityLog CI,Lum_Erp_Transporter T
                        Where S.soirId=C.soirId AND C.transporterId=T.transporterId AND C.challanId = CI.challanId AND CI.activityTypeId=1 AND C.ChallanId=" + _challanId))
            {
                if (reader.Read())
                {
                    _storeid = Convert.ToInt32(reader["storeId"]);
                    taxType = Convert.ToInt32(reader["taxTypeId"]);
                    lblInvoiceNo.Text = Convert.ToString(reader["strChallanId"]); // +"/" + Convert.ToString(reader["challanVersion"]);
                    _soirId = Convert.ToInt32(reader["soirId"]);

                    invoicedate.Text = Convert.ToString(reader["CHALLANDATE"]);
                    CustOrderDate.Text = Convert.ToString(reader["poDate"]);
                    customerId = Convert.ToInt32(reader["customerId"]);
                    CustOrderNo.Text = Convert.ToString(reader["customerPoNo"]);
                    CustOrderDate.Text = Convert.ToString(reader["poDate"]);
                    Charge = Convert.ToDouble(reader["packingCharges"]);
                    Buyback = Convert.ToDouble(reader["buyBackAmount"]);
                    buyBackDetails = Convert.ToString(reader["buyBackDetails"]);
                    lblCNDetails.Text = (reader["CNDetails"]).ToString();
                    DespatchThrough.Text = (reader["transporterName"]).ToString();
                    reader.Close();
                    BindGrid(conn, taxType);

                }
            }

        }
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            IDataReader _reader = conn.ExecuteQuery(string.Format("select c.* from Lum_Erp_Customer c WHERE customerId = " + customerId));
            if (_reader.Read())
            {
                CustomerName.Text = Convert.ToString(_reader["customerName"]);
                CustomerAdrress.Text = Convert.ToString(_reader["Billing_address1"]) + " , " + Convert.ToString(_reader["Billing_address2"]) + " , " + Convert.ToString(_reader["Billing_zipCode"]);
                CosigneeName.Text = Convert.ToString(_reader["customerName"]);
                CosigneeAddress.Text = Convert.ToString(_reader["Shipping_address1"]) + " , " + Convert.ToString(_reader["Shipping_address2"]) + " , " + Convert.ToString(_reader["Shipping_zipCode"]);
                cvat.Text = Convert.ToString(_reader["vatRegistrationNo"]);

            }
        }
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            IDataReader _reader = conn.ExecuteQuery(string.Format("select TIN,ServiceTaxRegnNo,cstNo,* FROM Lum_Erp_CompanyMaster where fromdate <=DATEADD(MI,330,GETUTCDATE() ) and isnull(todate,DATEADD(MI,330,GETUTCDATE() )) >=DATEADD(MI,330,GETUTCDATE() )"));
            if (_reader.Read())
            {
                lblTinNo.Text = Convert.ToString(_reader["TIN"]);
                lblServiceTaxRegistNo.Text = Convert.ToString(_reader["ServiceTaxRegnNo"]);
                lblCSTNo.Text = Convert.ToString(_reader["cstNo"]);
            }
        }
    }

    private void BindGrid(IConnection conn, int taxType)
    {
        double t1 = 0;
        double t2 = 0;
        string sql = "";
        totalAmount = 0;
        IDataReader _reader;
        DataTable aTable = new DataTable();


        DataRow dtRow;
        DataColumn dtCol;

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "itemDescription";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "itd";

        aTable.Columns.Add(dtCol);
        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "SerialNo";

        aTable.Columns.Add(dtCol);
        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "qtyxrate";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "quantity";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "uomName";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "price";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "vatRate";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "vatAmount";
        aTable.Columns.Add(dtCol);
        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "discount";
        aTable.Columns.Add(dtCol);
        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "total";
        aTable.Columns.Add(dtCol);

        DataRow drProduct;
        //code modified for vat calculation by kasturi on 12 apr 2013 
        string _CDate = Convert.ToDateTime(invoicedate.Text).ToString("dd MMM yyyy");
        SpParam _spsoirId = new SpParam("@soirId", _soirId, SqlDbType.Int);
        SpParam _CaDate = new SpParam("@CDate", _CDate, SqlDbType.SmallDateTime);
        SpParam _spCHALLANID = new SpParam("@CHALLANID", _challanId, SqlDbType.Int);
        SpParam _spSTORID = new SpParam("@STORID", _storeid, SqlDbType.Int);
        SpParam _sptID = new SpParam("@tid", 0, SqlDbType.Int);
        DataSet _Ds = new DataSet();
        _Ds = conn.GetDataSet("Lum_erp_Stp_GetPurchaseOrder_WithGrnDetails_new", _spsoirId, _CaDate, _spCHALLANID, _spSTORID, _sptID);

        //if (taxType == 1)
        //{
        //    _Ds = conn.GetDataSet("Lum_erp_Stp_GetPurchaseOrder_WithGrnDetails_vat", _spsoirId, _spCHALLANID, _spSTORID);
        //    // poLineGrid.Columns[3].HeaderText = "VAT";
        //}
        //if (taxType == 2)
        //{
        //    _Ds = conn.GetDataSet("Lum_erp_Stp_GetPurchaseOrder_WithGrnDetails_cst", _spsoirId, _spCHALLANID, _spSTORID);
        //    //poLineGrid.Columns[3].HeaderText = "CST";
        //}
        //if (taxType == 3)
        //{
        //    _Ds = conn.GetDataSet("Lum_erp_Stp_GetPurchaseOrder_WithGrnDetails_cstAgainstFormC", _spsoirId, _spCHALLANID, _spSTORID);
        //    // poLineGrid.Columns[3].HeaderText = "CSTAgainstFormC";
        //}
        for (int _index = 0; _index < _Ds.Tables[0].Rows.Count; _index++)
        {
            string foc = "";
            string _SerialNum = string.Empty;
            if (Convert.ToBoolean(_Ds.Tables[0].Rows[_index]["itemType"]) == true)
            {
                foc = " (FOC)";
            }
            else
            {
                foc = " ";
            }
            drProduct = aTable.NewRow();
            drProduct["itemDescription"] = _Ds.Tables[0].Rows[_index]["itemDescription"].ToString() + foc;
            drProduct["itd"] = _Ds.Tables[0].Rows[_index]["itd"].ToString();
            string _SlrNo = _Ds.Tables[0].Rows[_index]["SerialNo"].ToString();
            string[] _SerialNo = _SlrNo.Split(';');
            for (int i = 0; i < _SerialNo.Length; i++)
            {
                if (_SerialNum == "")
                {
                    if (_SerialNo[i].ToString() != "")
                        _SerialNum = _SerialNo[i].ToString() + ",";
                }
                else
                {
                    _SerialNum = _SerialNum + "&nbsp;" + "<br>" + _SerialNo[i].ToString();
                }
            }
            drProduct["SerialNo"] = _SerialNum;
            //drProduct["SerialNo"] = _Ds.Tables[0].Rows[_index]["SerialNo"].ToString().Replace(';', ',');
            drProduct["quantity"] = _Ds.Tables[0].Rows[_index]["quantity"].ToString();
            drProduct["vatRate"] = _Ds.Tables[0].Rows[_index]["taxType"].ToString();
            drProduct["uomName"] = _Ds.Tables[0].Rows[_index]["uomName"].ToString();
            drProduct["price"] = string.Format("{0:#,##0.00}", (Convert.ToDouble(_Ds.Tables[0].Rows[_index]["price"])));
            drProduct["vatAmount"] = string.Format("{0:#,##0.00}", (Convert.ToDouble(_Ds.Tables[0].Rows[_index]["vatAmount"])));
            drProduct["discount"] = string.Format("{0:#,##0.00}", (Convert.ToDouble(_Ds.Tables[0].Rows[_index]["discount"])));
            drProduct["total"] = string.Format("{0:#,##0.00}", (Convert.ToDouble(_Ds.Tables[0].Rows[_index]["total"])));
            drProduct["qtyxrate"] = string.Format("{0:#,##0.00}", (Convert.ToDouble(_Ds.Tables[0].Rows[_index]["price"])) * Convert.ToDouble(_Ds.Tables[0].Rows[_index]["quantity"]));

            //CalcTotalprice(drProduct["price"].ToString());
            CalcTotalprice(drProduct["qtyxrate"].ToString());
            CalcTotalQty(drProduct["quantity"].ToString());
            CalcTotalVat(drProduct["vatAmount"].ToString());
            Calctotaldiscount(drProduct["discount"].ToString());
            CalcTotalAmount(drProduct["total"].ToString());



            aTable.Rows.Add(drProduct);


        }
        drProduct = aTable.NewRow();
        drProduct["itemDescription"] = "";
        drProduct["itemDescription"] = "Sub Total";
        drProduct["SerialNo"] = "";
        drProduct["quantity"] = string.Format("{0:#,##0.00}", TotalQty);
        drProduct["uomName"] = "";
        drProduct["qtyxrate"] = string.Format("{0:#,###.00}", (totalprice));
        drProduct["vatAmount"] = string.Format("{0:#,###.00}", (TotalVat));
        drProduct["discount"] = string.Format("{0:#,##0.00}", (totaldiscount));
        drProduct["total"] = string.Format("{0:#,##0.00}", (totalAmount));

        aTable.Rows.Add(drProduct);
        //discount
        if (Buyback > 0)
        {
            //drProduct = aTable.NewRow();
            //drProduct["itemDescription"] = "";
            //drProduct["itemDescription"] = "Discount: ";
            //drProduct["SerialNo"] = "";
            //drProduct["quantity"] = "";
            //drProduct["uomName"] = "";
            //drProduct["price"] = "";
            //drProduct["vatAmount"] = "";
            //drProduct["discount"] = "";
            //drProduct["total"] = string.Format("{0:#,##0.00}", (Convert.ToDouble(totaldiscount)));

            //aTable.Rows.Add(drProduct);

            //CalcTotalAmountminus(totaldiscount.ToString());

            //drProduct = aTable.NewRow();
            //drProduct["itemDescription"] = "";
            //drProduct["itemDescription"] = "Sub Total";
            //drProduct["SerialNo"] = "";
            //drProduct["total"] = string.Format("{0:#,##0.00}", (totalAmount));


        }
        //drProduct = aTable.NewRow();
        //drProduct["itemDescription"] = "";
        //drProduct["itemDescription"] = "Tax @ 12.5%";
        //drProduct["SerialNo"] = "";
        //drProduct["quantity"] = "";
        //drProduct["uomName"] = "";
        //drProduct["price"] = "";
        //drProduct["vatAmount"] = "";
        //drProduct["discount"] = "";
        //drProduct["total"] = string.Format("{0:#,##0.00}", (Convert.ToDouble(TotalVat)));

        //aTable.Rows.Add(drProduct);
        //CalcTotalAmount(TotalVat.ToString());

        if (Charge > 0)
        {
            drProduct = aTable.NewRow();
            drProduct["itemDescription"] = "";
            drProduct["itemDescription"] = "Packing & Forwarding";
            drProduct["SerialNo"] = "";
            drProduct["quantity"] = "";
            drProduct["uomName"] = "";
            drProduct["price"] = "";
            drProduct["vatAmount"] = "";
            drProduct["discount"] = "";
            drProduct["total"] = string.Format("{0:#,##0.00}", (Convert.ToDouble(Charge)));



            aTable.Rows.Add(drProduct);
            CalcTotalAmount(Charge.ToString());
        }
        if (Buyback > 0)
        {
            drProduct = aTable.NewRow();
            drProduct["itemDescription"] = "";
            drProduct["itemDescription"] = "Buyback Details: " + buyBackDetails;
            drProduct["SerialNo"] = "";
            drProduct["quantity"] = "";
            drProduct["uomName"] = "";
            drProduct["price"] = "";
            drProduct["vatAmount"] = "";
            drProduct["discount"] = "";
            drProduct["total"] = string.Format("{0:#,##0.00}", (Convert.ToDouble(Buyback)));

            aTable.Rows.Add(drProduct);
            CalcTotalAmountminus(Buyback.ToString());
        }



        t1 = Convert.ToDouble(totalAmount);
        t2 = Math.Round(totalAmount);

        if (t2 > t1)
        {
            drProduct = aTable.NewRow();
            drProduct["itemDescription"] = "";
            drProduct["itemDescription"] = "Round Off ";
            drProduct["SerialNo"] = "";
            drProduct["quantity"] = "";
            drProduct["uomName"] = "";
            drProduct["price"] = "";
            drProduct["vatAmount"] = "";
            drProduct["discount"] = "";
            drProduct["total"] = "(+)" + string.Format("{0:#,##0.00}", (t2 - t1));
            aTable.Rows.Add(drProduct);
        }
        if (t2 < t1)
        {
            drProduct = aTable.NewRow();
            drProduct["itemDescription"] = "";
            drProduct["itemDescription"] = "Round Off ";
            drProduct["SerialNo"] = "";
            drProduct["quantity"] = "";
            drProduct["uomName"] = "";
            drProduct["price"] = "";
            drProduct["vatAmount"] = "";
            drProduct["discount"] = "";
            drProduct["total"] = "(-)" + string.Format("{0:#,##0.00}", (t1 - t2));
            aTable.Rows.Add(drProduct);
        }

        drProduct = aTable.NewRow();
        drProduct["itemDescription"] = "";
        drProduct["itemDescription"] = "Total";
        drProduct["SerialNo"] = "";
        drProduct["quantity"] = "";// string.Format("{0:#,###}", TotalQty);
        drProduct["uomName"] = "";
        drProduct["price"] = "";// string.Format("{0:#,###.00}", (totalprice));
        drProduct["vatAmount"] = "";// string.Format("{0:#,###.00}", (TotalVat));
        drProduct["discount"] = "";// string.Format("{0:#,###.00}", (totaldiscount));
        drProduct["total"] = string.Format("{0:#,##0.00}", (Math.Round(totalAmount)));

        aTable.Rows.Add(drProduct);


        poLineGrid.DataSource = aTable;
        poLineGrid.DataBind();


        lblTotalintext.Text = changeCurrencyToWords(Math.Round(totalAmount));
    }

    private void CalcTotalprice(string _netVal)
    {
        double ta = 0;
        if (double.TryParse(_netVal, out ta))
        {
            totalprice += Double.Parse(_netVal);
        }

    }

    private void CalcTotalAmount(string _netVal)
    {
        double ta = 0;
        if (double.TryParse(_netVal, out ta))
        {
            totalAmount += Double.Parse(_netVal);
        }

    }

    private void CalcTotalQty(string _netVal)
    {
        double ta = 0;
        if (double.TryParse(_netVal, out ta))
        {
            TotalQty += Convert.ToDecimal(_netVal);
        }

    }
    private void CalcTotalVat(string _netVal)
    {
        double ta = 0;
        if (double.TryParse(_netVal, out ta))
        {
            TotalVat += Double.Parse(_netVal);
        }

    }
    private void Calctotaldiscount(string _netVal)
    {
        double ta = 0;
        if (double.TryParse(_netVal, out ta))
        {
            totaldiscount += Double.Parse(_netVal);
        }

    }
    private void CalcTotalAmountminus(string _netVal)
    {
        double ta = 0;
        if (double.TryParse(_netVal, out ta))
        {
            totalAmount -= Double.Parse(_netVal);
        }

    }

    protected void btnCancel_ServerClick(object sender, EventArgs e)
    {
        if (ViewState["previousPage"] != null)
        {
            Response.Redirect(Convert.ToString(ViewState["previousPage"]));
        }

    }
}
