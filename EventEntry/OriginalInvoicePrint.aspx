﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OriginalInvoicePrint.aspx.cs" Inherits="EventEntry_OriginalInvoicePrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>Lumen Tele System Pvt. Ltd.</title>
    <link href="./../style/style.css" rel="stylesheet" type="text/css" />
    <script type ="text/javascript">
       function print_page() {

            var control = document.getElementById("btnPrint");
           
                control.style.visibility = "hidden";

            var control1 = document.getElementById("btnCancel");
            
                control1.style.visibility = "hidden";
            // else control.style.visibility = "visible";

            /*    var OLECMDID = 7;
            /* OLECMDID values:
            * 6 - print
            * 7 - print preview
            * 1 - open window
            * 4 - Save As
         
         var PROMPT = 1; // 2 DONTPROMPTUSER 
            var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
            document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
            WebBrowser1.ExecWB(OLECMDID, PROMPT);
            WebBrowser1.outerHTML = "";*/
          //  var OLECMDID = 7;
            /* OLECMDID values:
            * 6 - print
            * 7 - print preview
            * 1 - open window
            * 4 - Save As
            */
           // var PROMPT = 1; // 2 DONTPROMPTUSER
          //  var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
           // document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
            //WebBrowser1.ExecWB(OLECMDID, PROMPT);
            //WebBrowser1.outerHTML = "";
        }
        function viewControl() {
            var control = document.getElementById("btnPrint");
            control.style.visibility = "visible";
            var control1 = document.getElementById("btnCancel");
            control1.style.visibility = "visible";
			}
    </script>
     <style type="text/css">
         .style1
         {
             height: 15px;
         }
     </style>
    </head>
<body>
    <form id="form1" runat="server">
        <div class="Header">
            <div class="HeaderImg">
            </div>
        </div>
        <div class="PageContentDivprint">
            <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalTextBlackSmall" >
                <tr class="NormalTextBlackSmall">
                    <td align="left" >&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td align="right">&nbsp;</td>
                </tr>
                <tr class="NormalTextBlackSmall">
                    <td colspan="4" style="font-size:large">Tax Invoice
                    </td>
                </tr>
                <tr class="NormalTextBlackSmall">
                    <td colspan="4" style="font-weight:bold; font-size:small">
                        Original - Buyer's Copy</td>
                </tr>
            </table>
        
                    <table class="PageContentprint" width="100%" >
                         <tr class="NormalTextBlackSmall">
                            <td align="left" style="width:10%">Date :&nbsp;</td>
                            <td align="left" style="width:35%"><asp:Label ID="invoicedate" runat="server"></asp:Label></td>
                            <td align="right" style="width:15%">Invoice No.:&nbsp;&nbsp;</td>
                             <td align="left" style="width:40%"><asp:Label ID="lblInvoiceNo" runat="server" Text=""></asp:Label></td>
                         </tr>
                    </table>
                    <table class="PageContentprint" width="100%" >
                             <tr class="NormalTextBlack">
                                <td align="left" valign="top" style="width:20%">Customer Name :</td> 
                                <td style="width:80%" align="left"  colspan="3"> 
                                    <asp:Label ID="CustomerName" runat="server"></asp:Label>
                                </td>
                             </tr>
                             <tr class="NormalTextBlack">
                                <td align="left" valign="top" style="width:20%">Customer Address :</td> 
                                <td style="width:80%" align="left"  colspan="3"><asp:Label ID="CustomerAdrress" runat="server"></asp:Label></td>
                             </tr>
                             <tr class="NormalTextBlack" valign="top">
                                <td style="width:20%" align="left">Cosignee Name:</td>
                                <td style="width:80%" align="left" colspan="3">
                                   <asp:Label ID="CosigneeName" runat="server"></asp:Label>
                                </td>
                             </tr>
                             <tr class="NormalTextBlack" valign="top">
                                <td style="width:20%" align="left">Cosignee Address:</td>
                                <td style="width:80%" align="left" colspan="3">
                                   <asp:Label ID="CosigneeAddress" runat="server"></asp:Label>
                                </td>
                             </tr>
                    </table>
                    <table class="PageContentprint" width="100%" >         
                         <tr class="NormalTextBlackSmall">
                            <td align="left" style="width:50%">Customer Order No.:&nbsp;&nbsp;<asp:Label ID="CustOrderNo" runat="server"></asp:Label></td>
                            <td align="left" style="width:50%">Customer Order Date.:&nbsp;&nbsp;<asp:Label ID="CustOrderDate" runat="server"></asp:Label></td>
                         </tr>
                    </table>         
                    <table class="PageContentprint" width="100%" >
                        <tr class="NormalTextBlackSmall">
                            <td align="left" style="width:25%">Despatch Through &nbsp;&nbsp;:</td>
                            <td align="left" style="width:25%"> <asp:Label ID="DespatchThrough" runat="server"></asp:Label>
                            </td><td align="left" style="width:25%">C/N Details:</td>
                            <td align="left" style="width:25%"><asp:Label ID="lblCNDetails" runat="server"></asp:Label></td>
                            
                         </tr>
                    </table>
                    <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="PageContentprint" style="border-color:Black;">
                        <tr class="NormalTextBlackSmall">
                            <td align="center" style="font-size:small; font-weight:bold">Transaction Details</td>
                        </tr>
                    </table> 
                    <table width="100%" class="PageContentprint" style="border-color:Black;">
                        <tr class="NormalTextBlackSmall">
                            <td colspan="4">
                                 <asp:DataGrid ID="poLineGrid" runat="server" AutoGenerateColumns="false" 
                                     Width="100%"  >
                                     <FooterStyle Font-Bold="True" HorizontalAlign="Right" VerticalAlign="Middle" />
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="Model No/Part No" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Left"
                                         ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                             <asp:Label ID="itemDescription" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "itemDescription") %>'></asp:Label><br />
                                                 <%--<asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "itd") %>'></asp:Label><br />     --%>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" Font-Bold="True" ></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:TemplateColumn>
                                     <asp:TemplateColumn HeaderText="Item Description" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Left"
                                         ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                
                                                <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "itd") %>'></asp:Label><br />
                                                
                                                 
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" Font-Bold="True" ></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:TemplateColumn>
                                                                                                                        
                                        <asp:TemplateColumn Visible="true" HeaderText="Item Serial No." HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Bold="true">
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SerialNo") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Right" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:TemplateColumn>
                                                                                
                                        <asp:TemplateColumn HeaderText="Quantity" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Bold="true">
                                            <ItemTemplate>
                                                <asp:Label ID="itemQty" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "quantity") %>' />&nbsp;
                                                <%#DataBinder.Eval(Container.DataItem, "uomName")%>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Right" Font-Bold="True" ></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Rate(Rs.)" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true">
                                            <ItemTemplate>
                                                <asp:Label ID="itemPrice" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "price") %>'   />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Right" Font-Bold="True" ></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:TemplateColumn>

                                         <asp:TemplateColumn Visible="true" HeaderText="Amount(Rs.)" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true">
                                            <ItemTemplate>
                                                <asp:Label ID="qtyxrate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "qtyxrate") %>'  />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Right" Font-Bold="True" ></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:TemplateColumn>
                                        
                                       <asp:TemplateColumn Visible="true" HeaderText="Discount(Rs.)" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true">
                                            <ItemTemplate>
                                                <asp:Label ID="itemDiscount" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "discount") %>'  />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Right" Font-Bold="True" ></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:TemplateColumn>
                                        
                                                                                
                                         <asp:TemplateColumn Visible="true" HeaderText="VAT(%)" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Bold="true">
                                            <ItemTemplate>
                                                <asp:Label ID="vatRate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "vatRate") %>'  />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Right" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:TemplateColumn>
                                                                                                                       
                                        <asp:TemplateColumn Visible="true" HeaderText="VAT(Rs.)" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Bold="true">
                                            <ItemTemplate>
                                                <asp:Label ID="vatAmount" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "vatAmount") %>'  />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Right" Font-Bold="True"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:TemplateColumn>
                                                                                
                                 
                                        <asp:TemplateColumn Visible="true" HeaderText="Total Amount(Rs.)" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Bold="true">
                                            <ItemTemplate>
                                                <asp:Label ID="total" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "total") %>'  />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Right" Font-Bold="True" ></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        </asp:TemplateColumn>
                                        
                                     </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>    
 <%--                   <table width="100%" class="PageContentprint" style="border-color:Black;">
                        <tr class="NormalTextBlackSmall">

                           <td align="right" style="width:39%">&nbsp;
                               <asp:Label ID="lblVatPer" runat="server" Text=""></asp:Label>
                           </td>
                           <td align="center" style="width:31%"><asp:Label ID="lblVatAmount" runat="server" Text=""></asp:Label></td>
                        </tr>
                    </table> --%>   
                    <table width="100%" class="PageContentprint" style="border-color:Black;">    
                        <tr class="NormalTextBlackSmall">
                           <td align="left" style="width:27%"><asp:Label ID="lblTotalintext" runat="server"></asp:Label></td>
                        </tr>
                    </table>    
              <%--      <table width="100%" class="PageContentprint" style="border-color:Black;height:20px">     
                        <tr class="NormalTextBlackSmall">
                            <td align="left" colspan="4">
                                <asp:Label ID="lblNetAmtTextFormat" runat="server" Text=""></asp:Label></td>
                        </tr>
                    </table>--%>
             <table class="NormalTextBlackSmall" width="100%">
                <tr class="NormalTextBlackSmall">
                    <td colspan="2">
                        <input  id="btnCancel" value="Cancel" type="button" runat="server"
                                style="width: 141px; cursor: hand;" onserverclick="btnCancel_ServerClick" />
                                    <input  id="btnPrint" value="Print" type="button"
                                onclick="print_page();javascript:window.print(); viewControl();" 
                                style="width: 141px; cursor: hand;" />
                    </td>
                 </tr>
                 <tr class="NormalTextBlackSmall">
                    <td align="left" style="width:13%"><strong>TIN (W.B.) :</strong></td>
                    <td align="left" style="width:87%"><strong><asp:Label ID="lblTinNo" runat="server" Text=""></asp:Label></strong></td>
                 </tr>
                 <tr class="NormalTextBlackSmall">
                    <td align="left" style="width:13%"><strong>CST No. :</strong></td>
                    <td align="left" style="width:87%"><strong><asp:Label ID="lblCSTNo" runat="server" Text=""></asp:Label></strong></td>
                 </tr>
                 <tr class="NormalTextBlackSmall">
                    <td align="left" style="width:13%"><strong>Service Tax Reg. No.</strong></td>
                    <td align="left" style="width:87%"><strong><asp:Label ID="lblServiceTaxRegistNo" runat="server" Text=""></asp:Label></strong></td>
                 </tr>
                 <tr class="NormalTextBlackSmall">
                    <td align="left" style="width:30%"><strong>Customer VAT Reg. No.</strong></td>
                    <td align="left" style="width:70%"><strong><asp:Label ID="cvat" runat="server" Text=""></asp:Label></strong></td>
                 </tr>
             </table>
              <table class="NormalTextBlackSmall" width="100%"> 
                 <tr class="NormalTextBlackSmall">
                    <td style="width:50%"></td>
                    <td align="center" style="width:50%"><strong>E.&.O.E</strong></td>
                 </tr>   
                 <tr class="NormalTextBlackSmall">
                    <td style="width:50%"></td>
                    <td align="center" style="width:50%" colspan="2">for&nbsp;<strong>Lumen Tele Systems (P) Limited</strong></td>
                 </tr>
                 <tr class="NormalTextBlackSmall">
                    <td style="width:50%"></td>
                    <td align="center" style="width:50%" colspan="2"><strong><br /><br />Authorised Signatory</strong></td>
                 </tr>
            </table>   
            <table class="NormalTextBlackSmall" width="100%">
                <tr>
                    <td align="left" style="text-decoration:underline">Terms & Conditions</td> 
                </tr>
            </table>
            <br />
            <table class="NormalTextBlackSmall" width="100%">
                <tr>
                    <td align="left">Payment against this invoice should be made by A/C payee chequee/DD in the name of Lumen Tele-Systems (P) Ltd.</td> 
                </tr>
                <tr>
                    <td align="left">Interest @24% per annum will be charged on this bill if not paid on presentation.</td> 
                </tr>
                <tr>
                    <td align="left">Goods once sold will Not be exchanged.</td> 
                </tr>
            </table>
            <br />
            <table class="NormalTextBlackSmall" width="100%">
                <tr>
                    <td align="center" colspan="4">SUBJECT TO KOLKATA JURIDICTION</td>
                 </tr>
                 <tr>
                    <td align="center" colspan="4" style="font-style:italic">This is a Computer Generated Invoice</td>
                 </tr>
                 <tr>
                    <td align="center" colspan="4">System Designed & Developed by Bitscrape Solutions Pvt.Ltd.,www.bitscrape.com</td>
                 </tr>
            </table>      
        </div>
    </form>
</body>
</html>
