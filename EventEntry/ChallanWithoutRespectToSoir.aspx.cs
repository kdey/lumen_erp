﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;


public partial class EventEntry_ChallanWithoutRespectToSoir : BasePage
{
    protected int _soirId = -1;
    protected int _challanId = -1;
    private double totalAmount = 0;
    protected bool _isDispach = false;
    //ArrayList used for caching purpose
    public ArrayList list;
    public ArrayList list1;
    public ArrayList listSerial;
    //object of Props class to store Id, FirstName, LastName, City
    public Props props;
    public Serialno srno;
    public string Action = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!string.IsNullOrEmpty(Request["View"]))
        {
            Action = (Request["View"]);
        }
        if (!string.IsNullOrEmpty(Request["soirId"]))
        {
            _soirId = int.Parse(Request["soirId"]);
        }
        if (!string.IsNullOrEmpty(Request["challanId"]))
        {
            _challanId = int.Parse(Request["challanId"]);
        }
        if (!string.IsNullOrEmpty(Request["isDispach"]))
        {
            _isDispach = bool.Parse(Request["isDispach"]);
        }
        if (isDispachable())
        {
            btnDispatch.Enabled = true;
        }
        else
        {
            btnDispatch.Enabled = false;
        }
        if (!this.IsPostBack)
        {
            bindControl();
            raisedBy.Text = getUserName();
            if (_challanId > 0)
            {

                LoadChallan();
            }
        }
        else
        {
            list = new ArrayList();

        }
        if (GetLoggedinUser().UserType == 2)
        {
            //addItemLine.Enabled = false;
            //btnDispatch.Enabled = true;
            // btnaddChallan.Enabled = false;
        }
    }
    public void bindControl()
    {
        BindListControl _control = new BindListControl();
        _control.BindTransport(ddlTransporter);
        _control.BindDivision(divisionDdl);
        _control.BindSalesPerson(ddlprimary);
        _control.BindSalesPerson(ddlAditional);
        _control.BindCustomerAll(ddlCustomer);
        _control.BindOrderType(ddlOrder);
        _control.BindFormRecoverable(dllformrecoveralbe);
        _control.BindDivision(divisionDdl);
        _control.BindStores(ddlStorelocation);
        //     _control.BindRateContractType(rateContractDdl);

    }
    protected void savechallan()
    {
        int id;
        string installmentDate;
        string installmentamt;
        decimal _discount = 0;
        if (!string.IsNullOrEmpty(discount.Text))
        {
            _discount = Convert.ToDecimal(discount.Text);
        }

        // check to see if we need to create a new Soir.
        // get the date 
        DateTime date = DateTime.Today;
        if (string.IsNullOrEmpty(poDate.Text))
        {
            poDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
        }
        else
        {
            date = DateTime.ParseExact(poDate.Text, "dd/MM/yyyy", null);
        }

        object obj = Convert.DBNull;
        if (ddlAditional.SelectedIndex != 0)
        {
            obj = ddlAditional.SelectedValue;
        }
        object _objFormR = Convert.DBNull;

        if (rbTaxType.SelectedValue == "3")
        {
            if (dllformrecoveralbe.SelectedIndex != 0)
            {
                _objFormR = dllformrecoveralbe.SelectedValue;
            }
        }
        if (Convert.ToDecimal(txtquantity.Text) == 0)
        {
            return;
        }
        if (chkItemType.Checked == false)
        {
            if (Convert.ToDecimal(price.Text) == 0)
            {
                return;
            }
        }
        decimal packingCharges = 0;
        if (!string.IsNullOrEmpty(txtCharge.Text))
        {
            packingCharges = Convert.ToDecimal(txtCharge.Text);
        }
        decimal buyBackAmount = 0;
        if (!string.IsNullOrEmpty(txtBuyBackamount.Text))
        {
            buyBackAmount = Convert.ToDecimal(txtBuyBackamount.Text);
        }
        #region checking
        int lineId;
        decimal _quantity;
        int itemId;
        string serialNo;
        gridMessage.Text = "";
        _quantity = Convert.ToDecimal(txtquantity.Text);
        lineId = 0;
        itemId = Convert.ToInt32(itemDdl.SelectedValue);
        serialNo = txtserialNo.Text;
        if (divisionDdl.Items[divisionDdl.SelectedIndex].Text.ToUpper() != "FINISHED GOODS")
        {
            serialNo = string.Empty;
        }
        else
        {
            serialNo = txtserialNo.Text;
        }

        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            try
            {
                if (divisionDdl.Items[divisionDdl.SelectedIndex].Text.ToUpper() == "FINISHED GOODS")
                {
                    if (serialNo != string.Empty)
                    {
                        string[] param = serialNo.Split(';');
                        if (_quantity != param.Length)
                        {
                            gridMessage.Text = "quantity is not equal to total serialNo";
                            gridMessage.ForeColor = Color.Red;
                            return;
                        }
                    }
                    if (IsValidSerialNo(conn, serialNo, itemId, _quantity, int.Parse(ddlStorelocation.SelectedValue), _challanId, lineId))
                    {
                        gridMessage.Text = "";
                        gridMessage.ForeColor = Color.Green;

                        if (isDispachable())
                        {
                            btnDispatch.Enabled = true;
                        }
                        else
                        {
                            btnDispatch.Enabled = false;
                        }
                    }
                    else
                    {
                        // gridMessage.Text = "Serial Number/quantity is not available";
                        // gridMessage.ForeColor = Color.Red;
                        return;
                    }
                }

                else
                {
                    if (IsValidSerialNo(conn, serialNo, itemId, _quantity, int.Parse(ddlStorelocation.SelectedValue), _challanId, lineId))
                    {
                        gridMessage.Text = "";
                        gridMessage.ForeColor = Color.Green;
                        if (isDispachable())
                        {
                            btnDispatch.Enabled = true;
                        }
                        else
                        {
                            btnDispatch.Enabled = false;
                        }
                    }
                    else
                    {
                        //gridMessage.Text = "Items are not available in store";
                        // gridMessage.ForeColor = Color.Red;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                gridMessage.Text = ex.Message; ;
                gridMessage.ForeColor = Color.Red;
                return;
            }

        }
        #endregion
        if (_soirId <= 0)
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                IDataReader _reader = conn.ExecuteQueryProc("Lum_Erp_Stp_CreateSoirForChallan",
                                            new SpParam("@strSoirId", string.Empty, SqlDbType.VarChar),
                                            new SpParam("@raisedBy", raisedBy.Text, SqlDbType.VarChar),
                                            new SpParam("@primarySalesPersonId", ddlprimary.SelectedValue, SqlDbType.Int),
                                            new SpParam("@aditionalSalesPersonId", obj, SqlDbType.Int),
                                            new SpParam("@customerId", ddlCustomer.SelectedValue, SqlDbType.Int),
                                            new SpParam("@billingAddressId", DBNull.Value, SqlDbType.Int),
                                            new SpParam("@shippingAddressId", DBNull.Value, SqlDbType.Int),
                                            new SpParam("@customerPoNo", poReference.Text, SqlDbType.VarChar),
                                            new SpParam("@poDate", date, SqlDbType.DateTime),
                                            new SpParam("@orderTypeId", ddlOrder.SelectedValue, SqlDbType.Int),
                                            new SpParam("@installationAddress", installationAddress.Text, SqlDbType.VarChar),
                                            new SpParam("@strChallanId", string.Empty, SqlDbType.VarChar),
                                            new SpParam("@transporterId", ddlTransporter.SelectedValue, SqlDbType.Int),
                                            new SpParam("@CNDetails", txtCNDetails.Text, SqlDbType.VarChar),
                                            new SpParam("@isDispatch", 0, SqlDbType.Bit),
                                            new SpParam("@taxTypeId", rbTaxType.SelectedValue, SqlDbType.Int),
                                            new SpParam("@FormRecoverableId", _objFormR, SqlDbType.Int),
                                            new SpParam("@storeId", ddlStorelocation.SelectedValue, SqlDbType.Int),
                                            new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int),
                                        new SpParam("@packingCharges", packingCharges, SqlDbType.Decimal),
                                        new SpParam("@buyBackAmount", buyBackAmount, SqlDbType.Decimal),
                                        new SpParam("@buyBackDetails", txtDetails.Text, SqlDbType.VarChar));

                if (_reader.Read())
                {

                    _soirId = Convert.ToInt32(_reader["soirId"]);
                    _challanId = Convert.ToInt32(_reader["challanId"]);
                    challanNumber.Text = Convert.ToString(_reader["strChallanId"]);
                    _reader.Close();
                    ddlCustomer.Enabled = false;
                    lblMsg.Text = "Challan is created";
                    lblMsg.ForeColor = Color.Green;

                }
            }
        }


        if (_soirId > 0)
        {
            string fromDate = Convert.ToDateTime(poDate.Text).ToString("dd MMM yyyy");
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                conn.ExecuteCommandProc("lum_erp_stp_CreateSoirItemDetails", new SpParam("@soirId", _soirId, SqlDbType.Int),
                                            new SpParam("@itemId", itemDdl.SelectedValue, SqlDbType.Int),
                                            new SpParam("@CDate", fromDate, SqlDbType.SmallDateTime),
                                            new SpParam("@quantity", txtquantity.Text, SqlDbType.Decimal),
                                            new SpParam("@price", price.Text, SqlDbType.Decimal),
                                            new SpParam("@discount", _discount, SqlDbType.Decimal),
                                            new SpParam("@itemType", chkItemType.Checked, SqlDbType.Bit),
                                            new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int),
                                             new SpParam("@challanId", _challanId, SqlDbType.Int));

            }
        }
        #region insertserialno
        _quantity = Convert.ToDecimal(txtquantity.Text);
        lineId = poLineGrid.Items.Count + 1; ;
        itemId = Convert.ToInt32(itemDdl.SelectedValue);

        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            try
            {
                if (divisionDdl.Items[divisionDdl.SelectedIndex].Text.ToUpper() == "FINISHED GOODS")
                {
                    if (serialNo != string.Empty)
                    {
                        string[] param = serialNo.Split(';');
                        if (serialNo != string.Empty)
                        {
                            if (_quantity != param.Length)
                            {
                                gridMessage.Text = "quantity is not equal to total serialNo";
                                gridMessage.ForeColor = Color.Red;
                                return;
                            }
                        }

                        if (IsValidSerialNo(conn, serialNo, itemId, _quantity, int.Parse(ddlStorelocation.SelectedValue), _challanId, lineId))
                        {
                            InsertItemSerialNo(lineId, _challanId, itemId, serialNo, conn, _quantity, int.Parse(ddlStorelocation.SelectedValue));

                            gridMessage.Text = "";
                            gridMessage.ForeColor = Color.Green;

                            if (isDispachable())
                            {
                                btnDispatch.Enabled = true;
                            }
                            else
                            {
                                btnDispatch.Enabled = false;
                            }
                        }
                        else
                        {
                            //gridMessage.Text = "Serial Number is not available";
                            // gridMessage.ForeColor = Color.Red;
                            return;
                        }
                    }
                }

                else
                {
                    if (IsValidSerialNo(conn, serialNo, itemId, _quantity, int.Parse(ddlStorelocation.SelectedValue), _challanId, lineId))
                    {
                        InsertItemSerialNo(lineId, _challanId, itemId, serialNo, conn, _quantity, int.Parse(ddlStorelocation.SelectedValue));
                        gridMessage.Text = "";
                        gridMessage.ForeColor = Color.Green;
                        if (isDispachable())
                        {
                            btnDispatch.Enabled = true;
                        }
                        else
                        {
                            btnDispatch.Enabled = false;
                        }
                    }
                    else
                    {
                        // gridMessage.Text = "Items are not available in store";
                        //  gridMessage.ForeColor = Color.Red;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                gridMessage.Text = ex.Message; ;
                gridMessage.ForeColor = Color.Red;
                return;
            }

        }

        #endregion
        #region installment
        if (_challanId > 0)
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                conn.ExecuteCommand("Delete from Lum_Erp_InstallmentDetails  WHERE challanId =" + _challanId);
            }
            for (int i = 0; i < DataGrid1.Items.Count; i++)
            {
                //id = ((Label)DataGrid1.Items[i].Cells[1].FindControl("Label1")).Text;
                installmentDate = ((Label)DataGrid1.Items[i].Cells[2].FindControl("Label2")).Text;
                installmentamt = ((Label)DataGrid1.Items[i].Cells[3].FindControl("Label3")).Text;

                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    try
                    {
                        conn.ExecuteCommandProc("Lum_Erp_Stp_CreateInstallmentDetails", new SpParam("@challanId", _challanId, SqlDbType.Int),
                                                                        new SpParam("@price", installmentamt, SqlDbType.Decimal),
                                                                        new SpParam("@installmentDate", installmentDate, SqlDbType.DateTime),
                                                                        new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int));
                    }
                    catch (Exception ex)
                    {
                        errmsg.InnerHtml = ex.ToString();
                    }
                }
            }
        }

        #endregion

        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            BindGrid(conn, int.Parse(rbTaxType.SelectedValue), 0);
        }
        lblquantity.Text = GetTotalQuantity();
        if (isDispachable())
        {
            btnDispatch.Enabled = true;
        }
        else
        {
            btnDispatch.Enabled = false;
        }
        btncreate.Visible = true;
        btncreate.Text = "Update Challan";
        resetControl();
    }

  
    protected void itemDdl_SelectedIndexChanged(object sender, EventArgs e)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            LoadItemDescription(conn);
        }
        lblAvailable.Text = getAvailableItem();
        hmaxprice.Value = GetItemPrice();
    }
    private void LoadItemDescription(IConnection conn)
    {
        string fromDate = Convert.ToDateTime(poDate.Text).ToString("dd MMM yyyy");
        if (!string.IsNullOrEmpty(itemDdl.SelectedValue))
        {
            using (IDataReader reader = conn.ExecuteQuery(
                        @"select itemDescription, uomName, v.Vat as vat 
                          from Lum_Erp_Item i
                          inner join Lum_Erp_Uom u on u.uomId = i.uomId 
                          inner join dbo._Lum_Erp_VatDetails v on i.itemType=v.itemType
                          where i.itemId = '" + itemDdl.SelectedValue + "' and '" + fromDate + "' >=v.FromDate and '" + fromDate + "' <= v.ToDate"))
            {
                if (reader.Read())
                {
                    itemDescription.Text = reader.GetString(0);
                    uom.Text = reader.GetString(1);
                }
            }
        }

    }

    protected void rbTaxType_SelectedIndexChanged(object sender, EventArgs e)
    {
        IConnection conn = DataConnectionFactory.GetConnection();

        if (rbTaxType.SelectedValue == "1")
        {

            BindGrid(conn, 1, 1);
            dllformrecoveralbe.SelectedIndex = 0;
            dllformrecoveralbe.Enabled = false;

        }
        else if (rbTaxType.SelectedValue == "2")
        {
            BindGrid(conn, 2, 2);
            dllformrecoveralbe.SelectedIndex = 0;
            dllformrecoveralbe.Enabled = false;

        }
        else if (rbTaxType.SelectedValue == "3")
        {
            BindGrid(conn, 3, 3);
            dllformrecoveralbe.Enabled = true;

        }


    }
    //private void BindGrid_forrbtn(IConnection conn, int taxType)
    //{
    //    string sql = "";
    //    totalAmount = 0;
    //    IDataReader _reader;
    //    if (taxType == 1)
    //    {
    //        _reader = conn.ExecuteQueryProc("Lum_erp_Stp_GetPurchaseOrder_WithGrnDetails_vat",
    //                             new SpParam("@soirId", _soirId, SqlDbType.Int),
    //                             new SpParam("@CHALLANID", _challanId, SqlDbType.Int),
    //                             new SpParam("@STORID", int.Parse(ddlStorelocation.SelectedValue), SqlDbType.Int));
    //        poLineGrid.Columns[7].HeaderText = "VAT(%)";

    //        poLineGrid.DataSource = _reader;
    //        poLineGrid.DataBind();
    //        _reader.Close();
    //    }
    //    if (taxType == 2)
    //    {
    //        _reader = conn.ExecuteQueryProc("Lum_erp_Stp_GetPurchaseOrder_WithGrnDetails_cst",
    //                             new SpParam("@soirId", _soirId, SqlDbType.Int),
    //                             new SpParam("@CHALLANID", _challanId, SqlDbType.Int),
    //                             new SpParam("@STORID", int.Parse(ddlStorelocation.SelectedValue), SqlDbType.Int));
    //        poLineGrid.Columns[7].HeaderText = "CST(%)";            
    //        poLineGrid.DataSource = _reader;
    //        poLineGrid.DataBind();
    //        _reader.Close();
    //    }

    //    if (taxType == 3)
    //    {
    //        _reader = conn.ExecuteQueryProc("Lum_erp_Stp_GetPurchaseOrder_WithGrnDetails_cstAgainstFormC",
    //                             new SpParam("@soirId", _soirId, SqlDbType.Int),
    //                             new SpParam("@CHALLANID", _challanId, SqlDbType.Int),
    //                             new SpParam("@STORID", int.Parse(ddlStorelocation.SelectedValue), SqlDbType.Int));
    //        poLineGrid.Columns[7].HeaderText = "CST Against Form(%)"; 

    //        poLineGrid.DataSource = _reader;
    //        poLineGrid.DataBind();
    //        _reader.Close();
    //    }
    //    double Charge = 0.0;
    //    if (!string.IsNullOrEmpty(txtCharge.Text))
    //    {
    //        Charge = Convert.ToDouble(txtCharge.Text);
    //    }

    //    double Buyback = 0.0;
    //    if (!string.IsNullOrEmpty(txtBuyBackamount.Text))
    //    {
    //        Buyback = Convert.ToDouble(txtBuyBackamount.Text);
    //    }

    //    totalAmount = totalAmount + Charge - Buyback;

    //    lblTotal.Text = string.Format("{0:#,###.##}", Math.Round(totalAmount));
    //    lblPrice.Text = changeCurrencyToWords(Math.Round(totalAmount));
    //}
    private void BindGrid(IConnection conn, int taxType, int tid)
    {
        string fromDate = Convert.ToDateTime(poDate.Text).ToString("dd MMM yyyy");
        string sql = "";
        totalAmount = 0;
        try
        {
            IDataReader _reader;

            _reader = conn.ExecuteQueryProc("Lum_erp_Stp_GetPurchaseOrder_WithGrnDetails_new",
                                 new SpParam("@soirId", _soirId, SqlDbType.Int),
                                 new SpParam("@CDate", fromDate, SqlDbType.SmallDateTime),
                                 new SpParam("@CHALLANID", _challanId, SqlDbType.Int),
                                 new SpParam("@STORID", int.Parse(ddlStorelocation.SelectedValue), SqlDbType.Int),
                                 new SpParam("@tid", tid, SqlDbType.Int));
            poLineGrid.Columns[7].HeaderText = "Tax(%)";
            poLineGrid.DataSource = _reader;
            poLineGrid.DataBind();

        }
        catch (Exception ex)
        {
            errmsg.InnerHtml = ex.ToString();


        }
        double Charge = 0.0;
        if (!string.IsNullOrEmpty(txtCharge.Text))
        {
            Charge = Convert.ToDouble(txtCharge.Text);
        }

        double Buyback = 0.0;
        if (!string.IsNullOrEmpty(txtBuyBackamount.Text))
        {
            Buyback = Convert.ToDouble(txtBuyBackamount.Text);
        }

        totalAmount = totalAmount + Charge - Buyback;

        lblTotal.Text = string.Format("{0:#,###.##}", Math.Round(totalAmount));
        lblPrice.Text = changeCurrencyToWords(Math.Round(totalAmount));
    }

    protected void chkItemType_CheckedChanged(object sender, EventArgs e)
    {
        if (chkItemType.Checked)
        {
            price.Text = "0.00";
            discount.Text = "0.00";
            price.Enabled = false;
            discount.Enabled = false;
        }
        else
        {
            price.Text = "";
            discount.Text = "";
            price.Enabled = true;
            discount.Enabled = true;
        }
    }
    protected void poLineGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {

        //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //{

        //    Button btnEdit = (Button)e.Item.Cells[10].Controls[0];


        //    btnEdit.CssClass = "ButtonBig";
        //    if (GetLoggedinUser().UserType == 2)
        //    {
        //        btnEdit.Enabled = false;

        //    }
        //}
    }

    protected void poLineGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {

        if (e.CommandName.Equals("Delete"))
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                HtmlInputHidden _soirLineId = (HtmlInputHidden)poLineGrid.Items[e.Item.ItemIndex].FindControl("soirLineId");

                conn.ExecuteCommandProc("Lum_Erp_Stp_SoirDeleteItem_challan",
                    new SpParam("@soirId", _soirId, SqlDbType.Int),
                    new SpParam("@soirLineId", Convert.ToInt32(_soirLineId.Value), SqlDbType.Int),
                    new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int),
                    new SpParam("@challanId", _challanId, SqlDbType.Int));
                gridMessage.Text = "";
                gridMessage.ForeColor = Color.Green;
                BindGrid(conn, int.Parse(rbTaxType.SelectedValue), 0);
            }

        }
    }
    protected void poLineGrid_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        CalcTotalAmount(e.Item.Cells[9].Text);
    }

    /// <summary>
    /// Load Challan detail
    /// </summary>
    public void LoadChallan()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(@"select C.*,S.strSoirId ,s.packingCharges,s.buyBackAmount,s.buyBackDetails from Lum_Erp_ChallancumInvoice C inner join Lum_Erp_SOIR S on S.soirId=C.soirId  Where C.ChallanId=" + _challanId))
            {
                if (reader.Read())
                {
                    ddlTransporter.SelectedValue = Convert.ToString(reader["transporterId"]);
                    txtCNDetails.Text = Convert.ToString(reader["CNDetails"]);
                    if (reader["storeId"] != Convert.DBNull)
                    {
                        ddlStorelocation.SelectedValue = Convert.ToString(reader["storeId"]);
                    }
                    if (Convert.ToString(reader["isDispatch"]) == "False")
                    {
                        _isDispach = false;
                    }
                    else
                    {
                        _isDispach = true;
                        btnDispatch.Enabled = false;
                        addItemLine.Enabled = false;
                    }

                    rbTaxType.SelectedValue = Convert.ToString(reader["taxTypeId"]);

                    challanNumber.Text = Convert.ToString(reader["strChallanId"]) + "/" + Convert.ToString(reader["challanVersion"]);
                    txtCharge.Text = Convert.ToString(reader["packingCharges"]);
                    txtBuyBackamount.Text = Convert.ToString(reader["buyBackAmount"]);
                    txtDetails.Text = Convert.ToString(reader["buyBackDetails"]);
                    if (rbTaxType.SelectedValue == "3")
                    {
                        dllformrecoveralbe.Enabled = true;
                        dllformrecoveralbe.SelectedValue = Convert.ToString(reader["FormRecoverableId"]);
                    }
                    _soirId = Convert.ToInt32(reader["soirId"]);
                    reader.Close();
                    LoadSoir();
                    BindInstallmentGrid(conn);
                    ddlStorelocation.Enabled = false;
                    btncreate.Visible = true;
                }
            }

        }
    }
    /// <summary>
    /// Load SOIR detail
    /// </summary>
    public void LoadSoir()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(@"select* from Lum_Erp_Soir WHERE  soirId=" + _soirId))
            {
                if (reader.Read())
                {
                    raisedBy.Text = Convert.ToString(reader["raisedBy"]);
                    ddlprimary.SelectedValue = Convert.ToString(reader["primarySalesPersonId"]);
                    if (Convert.ToString(reader["aditionalSalesPersonId"]) != "")
                        ddlAditional.SelectedValue = Convert.ToString(reader["aditionalSalesPersonId"]);
                    ddlCustomer.SelectedValue = Convert.ToString(reader["customerId"]);
                    CustomerSegment.Text = getCustomerSegment(int.Parse(Convert.ToString(reader["customerId"])));
                    BindListControl _control = new BindListControl();
                    //installationAddress.Text = Convert.ToString(reader["installationAddress"]);
                    poReference.Text = Convert.ToString(reader["customerPoNo"]);
                    poDate.Text = Convert.ToString(reader["poDate"]).Split(' ')[0];
                    ddlOrder.SelectedValue = Convert.ToString(reader["orderTypeId"]);
                    double Charge = Convert.ToDouble(reader["packingCharges"]);
                    double Buyback = Convert.ToDouble(reader["buyBackAmount"]);
                    reader.Close();
                    BindGrid(conn, int.Parse(rbTaxType.SelectedValue), 0);

                    lblquantity.Text = GetTotalQuantity();
                    //totalAmount = totalAmount + Charge - Buyback;
                    //lblTotal.Text = string.Format("{0:N}", totalAmount);
                }
            }
        }
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            IDataReader _reader = conn.ExecuteQuery(string.Format("select Billing_address1+' '+Billing_address2  as billingAddress,Shipping_address1+' '+Shipping_address2 as shippingAddress from Lum_Erp_Customer WHERE customerId = " + Convert.ToInt32(ddlCustomer.SelectedValue)));
            if (_reader.Read())
            {
                billingAddress.Text = Convert.ToString(_reader["billingAddress"]);
                installationAddress.Text = Convert.ToString(_reader["shippingAddress"]);
            }
        }


    }
    protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindListControl _control = new BindListControl();
        //CustomerSegment.Text = getCustomerSegment(int.Parse(ddlCustomer.SelectedValue));
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            IDataReader _reader = conn.ExecuteQuery(string.Format(@"select segmentType, Billing_address1+' '+Billing_address2  as billingAddress,Shipping_address1+' '+Shipping_address2 as 
                            shippingAddress from Lum_Erp_Customer C , Lum_Erp_CustomerSegment S 
                            WHERE S.segmentId=C.customerSegmentId and customerId = " + Convert.ToInt32(ddlCustomer.SelectedValue)));
            if (_reader.Read())
            {
                billingAddress.Text = Convert.ToString(_reader["billingAddress"]);
                installationAddress.Text = Convert.ToString(_reader["shippingAddress"]);
                CustomerSegment.Text = Convert.ToString(_reader["segmentType"]);
            }
        }
    }

    public string GetTotalQuantity()
    {
        string retVal = "";
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(@"select Sum(quantity) as TotalQty from Lum_Erp_SoirItemDetails WHERE  soirId=" + _soirId))
            {
                if (reader.Read())
                {


                    retVal = Convert.ToString(reader["TotalQty"]);
                }
            }
        }
        return retVal;
    }
    protected void addChallan_Click(object sender, EventArgs e)
    {
        DateTime installmentdate = DateTime.Today;
        if (string.IsNullOrEmpty(txtDate.Text))
        {
            txtDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
        }
        else
        {
            installmentdate = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", null);
        }

        props = new Props();
        props.id = "0";
        props.installmentDate = Convert.ToString(installmentdate).Split(' ')[0];
        //Convert.ToString(installmentdate);
        props.installmentamt = txtinstAmt.Text;

        list.Add(props);

        NewBindData();
        txtDate.Text = "";
        txtinstAmt.Text = "";


    }
    /// <summary>
    /// Bind Installment Details
    /// </summary>
    /// <param name="conn"></param>
    private void BindInstallmentGrid(IConnection conn)
    {
        DateTime installmentdate = DateTime.Today;
        list1 = new ArrayList();
        string sql = @"select challanId,installmentDate ,price from Lum_Erp_InstallmentDetails  where challanId= " + _challanId;
        using (IDataReader reader = conn.ExecuteQuery(sql))
        {
            while (reader.Read())
            {
                props = new Props();
                props.id = Convert.ToString(reader["challanId"]);
                props.installmentDate = Convert.ToString(reader["installmentDate"]).Split(' ')[0];
                props.installmentamt = Convert.ToString(reader["price"]);

                list1.Add(props);
            }


            DataGrid1.DataSource = list1;
            DataGrid1.DataBind();

        }
    }
    private void CalcTotalAmount(string _netVal)
    {
        double ta = 0;
        if (double.TryParse(_netVal, out ta))
        {
            totalAmount += Double.Parse(_netVal);
        }

    }

    protected void poLineGrid_PreRender(object sender, EventArgs e)
    {

        for (int _index = 0; _index < poLineGrid.Items.Count; _index++)
        {
            TextBox _serialNo = (TextBox)poLineGrid.Items[_index].FindControl("itemSerialNo");
            Button btn = (Button)poLineGrid.Items[_index].Cells[13].Controls[0];
            HtmlInputHidden _hlocked = (HtmlInputHidden)poLineGrid.Items[_index].FindControl("isLocked");


            int lineId = (int)poLineGrid.DataKeys[_index];
            int itemId = int.Parse(((HtmlInputHidden)poLineGrid.Items[_index].FindControl("itemId")).Value);
            if (poLineGrid.Items[_index].Cells[0].Text.ToUpper() != "FINISHED GOODS")
            {
                _serialNo.Attributes.Add("readonly", "readonly");

            }
            else
            {
                _serialNo.Text = GetItemSerialNo(_challanId, itemId, lineId);
            }
            if (_hlocked.Value == "True")
            {
                rbTaxType.Enabled = false;
                addChallan.Visible = false;
                btncreate.Visible = false;
                btn.Visible = false;
                addItemLine.Visible = false;
            }
            if (Action == "View")
            {
                addItemLine.Visible = false;
                rbTaxType.Enabled = false;
                addChallan.Visible = false;
                btncreate.Visible = false;
                btn.Visible = false;
                btnDispatch.Visible = false;
            }
        }
    }
    /// <summary>
    /// </summary>
    /// <param name="conn"></param>
    /// <param name="serialNumber"></param>
    /// <param name="itemId"></param>
    /// <param name="receiveQty"></param>
    /// <param name="storeId"></param>
    /// <returns></returns>
    protected bool IsValidSerialNo(IConnection conn, string serialNumber, int itemId, decimal receiveQty, int storeId, int challanId, int challanLineid)
    {
        bool retVal = true;
        string sql = string.Empty;
        if (serialNumber != string.Empty)
        {
            string[] param = serialNumber.Split(';');
            try
            {


                foreach (string str in param)
                {
                    srno = new Serialno();
                    srno.itemId = itemId.ToString();
                    srno.srno = str;

                    //  using (IConnection conn = DataConnectionFactory.GetConnection())
                    //  {
                    using (IDataReader reader = conn.ExecuteQueryProc("Lum_Erp_Stp_CheckItemSerialValid",
                                           new SpParam("@challanLineId", challanLineid, SqlDbType.Int),
                                           new SpParam("@challanId", challanId, SqlDbType.Int),
                                           new SpParam("@itemId", itemId, SqlDbType.Int),
                                           new SpParam("@storeId", storeId, SqlDbType.Int),
                                           new SpParam("@serialNo", str, SqlDbType.VarChar)))
                    {
                        if (reader.Read())
                        {
                            //NOAVL
                            // SRAVL
                            if (Convert.ToInt32(reader["NOAVL"]) <= 0)
                            {
                                gridMessage.Text = "Quantity Not Available in Store";
                                gridMessage.ForeColor = Color.Red;
                                retVal = false;
                                break;
                            }
                            if (str.Length > 0 && (Convert.ToInt32(reader["SRAVL"]) <= 0))
                            {
                                gridMessage.Text = "This Serial No ( " + str + " ) Not Available in Store";
                                gridMessage.ForeColor = Color.Red;
                                retVal = false;
                                break;
                            }
                        }

                    }
                    //  }



                    //object obj = conn.ExecuteScalarProc("Lum_Erp_Stp_CheckItemSerialValid", 
                    //    new SpParam("@challanLineId", challanLineid, SqlDbType.Int),
                    //                           new SpParam("@challanId", challanId, SqlDbType.Int),
                    //                           new SpParam("@itemId", itemId, SqlDbType.Int),
                    //                           new SpParam("@storeId", storeId, SqlDbType.Int),
                    //                           new SpParam("@serialNo", str, SqlDbType.VarChar));

                    //if ((int)obj == 0)
                    //{
                    //    retVal = false;
                    //    break;
                    //}



                }
            }
            catch (Exception ex)
            {
                retVal = false;
                gridMessage.Text = ex.Message;
            }

        }
        else
        {
            object obj = conn.ExecuteScalarProc("Lum_Erp_Stp_CheckSPare_Exists",
                                              new SpParam("@challanLineId", challanLineid, SqlDbType.Int),
                                              new SpParam("@challanId", challanId, SqlDbType.Int),
                                              new SpParam("@itemId", itemId, SqlDbType.Int),
                                              new SpParam("@storeId", storeId, SqlDbType.Int));
            if (obj != null)
            {
                if ((decimal)obj < receiveQty)
                {
                    gridMessage.Text = "Quantity Not Available in Store";
                    gridMessage.ForeColor = Color.Red;
                    retVal = false;

                }
            }
            else
            {
                retVal = false;
            }
        }
        return retVal;
    }


    public void InsertItemSerialNo(int challanlineId, int challanId, int itemId, string serialNo, IConnection conn, decimal receiveQty, int storeId)
    {


        #region FG
        if (serialNo != string.Empty)
        {
            string[] param = serialNo.Split(';');
            try
            {
                using (IConnection conn1 = DataConnectionFactory.GetConnection())
                {

                    IDataReader reader = conn1.ExecuteQuery(@"update Lum_Erp_GrndetailsSerialNumber set challanId=null,challanLineId=null,isLocked=0 
                                    where challanId = " + challanId + "  and challanLineId=" + challanlineId + " and itemId=" + itemId + " ");
                }
                foreach (string str in param)
                {

                    conn.ExecuteCommandProc("Lum_Erp_Stp_UpdateGendetailsSerialNumberForChallan",
                                           new SpParam("@challanLineId", challanlineId, SqlDbType.Int),
                                           new SpParam("@challanId", challanId, SqlDbType.Int),
                                           new SpParam("@itemId", itemId, SqlDbType.Int),
                                           new SpParam("@storeId", storeId, SqlDbType.Int),
                                           new SpParam("@serialNo", str, SqlDbType.VarChar));

                }

            }
            catch (Exception ex)
            {
                gridMessage.Text = ex.Message;
                gridMessage.ForeColor = Color.Red;
            }
        }
        #endregion
        #region nonfg
        else
        {
            try
            {

                using (IConnection conn1 = DataConnectionFactory.GetConnection())
                {

                    IDataReader reader = conn1.ExecuteQuery(@"update Lum_Erp_GrndetailsSerialNumber set challanId=null,challanLineId=null,isLocked=0 
                                    where challanId = " + challanId + "  and challanLineId=" + challanlineId + " and itemId=" + itemId + " ");
                }
                // for (int _i = 0; _i < Math.Floor(receiveQty); _i++)
                //{
                conn.ExecuteCommandProc("Lum_Erp_Stp_UpdateSpareForChallan",
                    new SpParam("@challanLineId", challanlineId, SqlDbType.Int),
                    new SpParam("@challanId", challanId, SqlDbType.Int),
                    new SpParam("@itemId", itemId, SqlDbType.Int),
                    new SpParam("@storeId", storeId, SqlDbType.Int),
                    new SpParam("@serialNo", DBNull.Value, SqlDbType.VarChar),
                    new SpParam("@qty", receiveQty, SqlDbType.Decimal));

                // }




            }
        #endregion
            catch (Exception ex)
            {
                gridMessage.Text = ex.Message;
                gridMessage.ForeColor = Color.Red;
                return;
            }
        }

    }

    public string GetItemSerialNo(int challanId, int itemId, int lineId)
    {
        string retVal = "";

        string sql = string.Format(@"SELECT serialNo FROM Lum_Erp_GrndetailsSerialNumber WHERE challanId={0} AND challanLineId={1} AND  itemId={2} ", challanId, lineId, itemId);
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(sql))
            {
                while (reader.Read())
                {
                    if (retVal == "")
                    {
                        retVal = Convert.ToString(reader["serialNo"]);
                    }
                    else
                    {
                        retVal = retVal + ";" + Convert.ToString(reader["serialNo"]);
                    }
                }
            }
        }
        return retVal;

    }

    public bool CheckChallanStatus()
    {
        bool retVal = false;
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {

            if (_challanId > 0)
            {

                int _i = conn.ExecuteCommandProc("Lum_Erp_Stp_DispatchChallanItems",
                    new SpParam("@challanId", _challanId, SqlDbType.Int),
                    new SpParam("@storeId", int.Parse(ddlStorelocation.SelectedValue), SqlDbType.Decimal)
                                                                   );
                if (_i > 0)
                {
                    retVal = true;
                }
            }


        }
        return retVal;

    }



    public string getCustomerSegment(int customerId)
    {
        string retVal = "";
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {



            IDataReader _reader = conn.ExecuteQuery(string.Format("select CS.segmentType from Lum_Erp_CustomerSegment CS inner join Lum_Erp_Customer C on C.customerSegmentId=CS.segmentId"));
            if (_reader.Read())
            {
                retVal = Convert.ToString(_reader["segmentType"]);
            }



        }
        return retVal;
    }


    public bool isDispachable()
    {
        bool retVal = false;
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {

            if (_challanId > 0)
            {
                using (IDataReader reader = conn.ExecuteQuery(string.Format("select challanStatus from Lum_Erp_ChallancumInvoice where challanId={0}", _challanId)))
                {
                    if (reader.Read())
                    {
                        retVal = Convert.ToBoolean(reader["challanStatus"]);
                    }
                }
            }


        }
        return retVal;

    }
    protected void btnDispatch_Click(object sender, EventArgs e)
    {
        decimal packingCharges = 0;
        if (!string.IsNullOrEmpty(txtCharge.Text))
        {
            packingCharges = Convert.ToDecimal(txtCharge.Text);
        }
        decimal buyBackAmount = 0;
        if (!string.IsNullOrEmpty(txtBuyBackamount.Text))
        {
            buyBackAmount = Convert.ToDecimal(txtBuyBackamount.Text);
        }
        #region update
        DateTime date = DateTime.Today;
        if (string.IsNullOrEmpty(poDate.Text))
        {
            poDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
        }
        else
        {
            date = DateTime.ParseExact(poDate.Text, "dd/MM/yyyy", null);
        }

        object obj = Convert.DBNull;
        if (ddlAditional.SelectedIndex != 0)
        {
            obj = ddlAditional.SelectedValue;
        }
        object _objFormR = Convert.DBNull;

        if (rbTaxType.SelectedValue == "3")
        {
            _objFormR = dllformrecoveralbe.SelectedValue;
        }
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            if (_soirId > 0)
            {
                int i = conn.ExecuteCommandProc("Lum_Erp_Stp_UpdateChallanWithOutRespectToSoir",
                                           new SpParam("@soirId", _soirId, SqlDbType.Int),
                                           new SpParam("@challanId", _challanId, SqlDbType.Int),
                                           new SpParam("@raisedBy", raisedBy.Text, SqlDbType.VarChar),
                                           new SpParam("@primarySalesPersonId", ddlprimary.SelectedValue, SqlDbType.Int),
                                           new SpParam("@aditionalSalesPersonId", obj, SqlDbType.Int),
                                           new SpParam("@customerId", ddlCustomer.SelectedValue, SqlDbType.Int),
                                           new SpParam("@customerPoNo", poReference.Text, SqlDbType.VarChar),
                                           new SpParam("@poDate", date, SqlDbType.DateTime),
                                           new SpParam("@orderTypeId", ddlOrder.SelectedValue, SqlDbType.Int),
                                           new SpParam("@installationAddress", installationAddress.Text, SqlDbType.VarChar),
                                           new SpParam("@transporterId", ddlTransporter.SelectedValue, SqlDbType.Int),
                                           new SpParam("@CNDetails", txtCNDetails.Text, SqlDbType.VarChar),
                                           new SpParam("@isDispatch", 0, SqlDbType.Bit),
                                           new SpParam("@taxTypeId", rbTaxType.SelectedValue, SqlDbType.Int),
                                           new SpParam("@FormRecoverableId", _objFormR, SqlDbType.Int),
                                           new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int),
                                        new SpParam("@packingCharges", packingCharges, SqlDbType.Decimal),
                                        new SpParam("@buyBackAmount", buyBackAmount, SqlDbType.Decimal),
                                        new SpParam("@buyBackDetails", txtDetails.Text, SqlDbType.VarChar));


                lblMsg.Text = "Challan is updated";
                lblMsg.ForeColor = Color.Green;

            }

        }
        #endregion

        #region checking
        int lineId;
        decimal _quantity;
        int itemId;
        string serialNo;

        for (int i = 0; i < poLineGrid.Items.Count; i++)
        {
            _quantity = decimal.Parse(((Label)poLineGrid.Items[i].FindControl("itemQty")).Text);
            lineId = int.Parse(((HtmlInputHidden)poLineGrid.Items[i].FindControl("soirLineId")).Value);
            itemId = int.Parse(((HtmlInputHidden)poLineGrid.Items[i].FindControl("itemId")).Value);
            serialNo = ((TextBox)poLineGrid.Items[i].FindControl("itemSerialNo")).Text;
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                try
                {
                    if (poLineGrid.Items[i].Cells[0].Text.ToUpper() == "FINISHED GOODS")
                    {
                        if (serialNo != string.Empty)
                        {
                            string[] param = serialNo.Split(';');
                            if (serialNo != string.Empty)
                            {
                                if (_quantity != param.Length)
                                {
                                    gridMessage.Text = "quantity is not equal to total serialNo";
                                    gridMessage.ForeColor = Color.Red;
                                    return;
                                }
                            }
                            if (IsValidSerialNo(conn, serialNo, itemId, _quantity, int.Parse(ddlStorelocation.SelectedValue), _challanId, lineId))
                            {
                                gridMessage.Text = "";
                                gridMessage.ForeColor = Color.Green;

                                if (isDispachable())
                                {
                                    btnDispatch.Enabled = true;
                                }
                                else
                                {
                                    btnDispatch.Enabled = false;
                                }
                            }
                            else
                            {
                                //gridMessage.Text = "Serial Number is not available";
                                // gridMessage.ForeColor = Color.Red;
                                return;
                            }
                        }

                    }

                    else
                    {
                        if (IsValidSerialNo(conn, serialNo, itemId, _quantity, int.Parse(ddlStorelocation.SelectedValue), _challanId, lineId))
                        {
                            gridMessage.Text = "";
                            gridMessage.ForeColor = Color.Green;
                            if (isDispachable())
                            {
                                btnDispatch.Enabled = true;
                            }
                            else
                            {
                                btnDispatch.Enabled = false;
                            }
                        }
                        else
                        {
                            // gridMessage.Text = "Items are not available in store";
                            // gridMessage.ForeColor = Color.Red;
                            return;
                        }
                    }
                }
                catch (Exception ex)
                {
                    gridMessage.Text = ex.Message; ;
                    gridMessage.ForeColor = Color.Red;
                    return;
                }

            }
        }
        #endregion
        #region insertserialno
        for (int i = 0; i < poLineGrid.Items.Count; i++)
        {
            _quantity = decimal.Parse(((Label)poLineGrid.Items[i].FindControl("itemQty")).Text);
            lineId = int.Parse(((HtmlInputHidden)poLineGrid.Items[i].FindControl("soirLineId")).Value);
            itemId = int.Parse(((HtmlInputHidden)poLineGrid.Items[i].FindControl("itemId")).Value);
            serialNo = ((TextBox)poLineGrid.Items[i].FindControl("itemSerialNo")).Text;
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                try
                {
                    if (poLineGrid.Items[i].Cells[0].Text.ToUpper() == "FINISHED GOODS")
                    {
                        if (serialNo != string.Empty)
                        {
                            string[] param = serialNo.Split(';');
                            if (serialNo != string.Empty)
                            {
                                if (_quantity != param.Length)
                                {
                                    gridMessage.Text = "quantity is not equal to total serialNo";
                                    gridMessage.ForeColor = Color.Red;
                                    return;
                                }
                            }
                            if (IsValidSerialNo(conn, serialNo, itemId, _quantity, int.Parse(ddlStorelocation.SelectedValue), _challanId, lineId))
                            {
                                InsertItemSerialNo(lineId, _challanId, itemId, serialNo, conn, _quantity, int.Parse(ddlStorelocation.SelectedValue));

                                gridMessage.Text = "";
                                gridMessage.ForeColor = Color.Green;

                                if (isDispachable())
                                {
                                    btnDispatch.Enabled = true;
                                }
                                else
                                {
                                    btnDispatch.Enabled = false;
                                }
                            }
                            else
                            {
                                //gridMessage.Text = "Serial Number is not available";
                                //gridMessage.ForeColor = Color.Red;
                                return;
                            }
                        }
                    }

                    else
                    {
                        if (IsValidSerialNo(conn, serialNo, itemId, _quantity, int.Parse(ddlStorelocation.SelectedValue), _challanId, lineId))
                        {
                            InsertItemSerialNo(lineId, _challanId, itemId, serialNo, conn, _quantity, int.Parse(ddlStorelocation.SelectedValue));
                            gridMessage.Text = "";
                            gridMessage.ForeColor = Color.Green;
                            if (isDispachable())
                            {
                                btnDispatch.Enabled = true;
                            }
                            else
                            {
                                btnDispatch.Enabled = false;
                            }
                        }
                        else
                        {
                            //gridMessage.Text = "Items are not available in store";
                            //gridMessage.ForeColor = Color.Red;
                            return;
                        }
                    }
                }
                catch (FormatException)
                {
                    gridMessage.Text = "Invalid quantity or price.";
                    gridMessage.ForeColor = Color.Red;
                    return;
                }

            }
        }
        #endregion
        #region installment
        int id;
        string installmentDate;
        string installmentamt;
        if (_challanId > 0)
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                conn.ExecuteCommand("Delete from Lum_Erp_InstallmentDetails  WHERE challanId =" + _challanId);
            }
            for (int i = 0; i < DataGrid1.Items.Count; i++)
            {
                //id = ((Label)DataGrid1.Items[i].Cells[1].FindControl("Label1")).Text;
                installmentDate = ((Label)DataGrid1.Items[i].Cells[2].FindControl("Label2")).Text;
                installmentamt = ((Label)DataGrid1.Items[i].Cells[3].FindControl("Label3")).Text;

                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    try
                    {
                        conn.ExecuteCommandProc("Lum_Erp_Stp_CreateInstallmentDetails", new SpParam("@challanId", _challanId, SqlDbType.Int),
                                                                        new SpParam("@price", installmentamt, SqlDbType.Decimal),
                                                                        new SpParam("@installmentDate", installmentDate, SqlDbType.DateTime),
                                                                        new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int));
                    }
                    catch (Exception ex)
                    {
                        errmsg.InnerHtml = ex.ToString();
                    }
                }
            }
        }

        #endregion

        if (CheckChallanStatus())
        {
            lblMsg.Text = "Challan status is dispach u cant update challan";
            errmsg.InnerHtml = "Challan status is dispach u cant update challan";
            gridMessage.ForeColor = Color.Green;
            Response.Redirect("ChallanCumInvoiceList.aspx");
            //  LoadChallan();
        }
        Response.Redirect("ChallanCumInvoiceList.aspx");
    }

    public string getUserName()
    {
        string retVal = string.Empty;

        string sql = string.Format(@"SELECT (firstName+' '+lastName) as name FROM Lum_Erp_AdminUser WHERE adminuserId={0}  ", GetLoggedinUser().UserId);
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(sql))
            {
                if (reader.Read())
                {

                    retVal = Convert.ToString(reader["name"]);

                }
            }
        }
        return retVal;
    }

    public string getAvailableItem()
    {
        string retVal = string.Empty;

        //string sql = string.Format(@"SELECT ISNULL(SUM(closingQuantity),0) as total FROM Lum_Erp_Stock WHERE storeId={0} and itemId={1}  ", ddlStorelocation.SelectedValue, itemDdl.SelectedValue);
        //using (IConnection conn = DataConnectionFactory.GetConnection())
        //{
        //    using (IDataReader reader = conn.ExecuteQuery(sql))
        //    {
        //        if (reader.Read())
        //        {

        //            retVal = Convert.ToString(reader["total"]);

        //        }
        //    }
        //}
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQueryProc("Lum_Erp_Stp_GetStockRegister",
                            new SpParam("@divisionId", divisionDdl.SelectedValue, SqlDbType.Int),
                            new SpParam("@subdivisionId",subDivisionDdl.SelectedValue, SqlDbType.Int),
                            new SpParam("@itemId", itemDdl.SelectedValue, SqlDbType.Int),
                            new SpParam("@storeId", ddlStorelocation.SelectedValue, SqlDbType.Int),
                            new SpParam("@BYQTY", 1, SqlDbType.TinyInt)))
            {
                if (reader.Read())
                {

                    retVal = Convert.ToString(reader["quantity"]);

                }
            }
        }

        return retVal;
    }


    public string GetItemPrice()
    {
        string fromDate = Convert.ToDateTime(poDate.Text).ToString("dd MMM yyyy");
        string retVal = "0";
        string sqlstr = string.Empty;


        if (rbTaxType.SelectedValue == "1")
        {

            sqlstr = string.Format("SELECT i.MRP,i.MRP+(i.MRP * ISNULL((SELECT Vat FROM [dbo].[_Lum_Erp_VatDetails]Where (('" + fromDate + "'>=FromDate AND '" + fromDate + "'< =ToDate)) AND itemType=i.itemType),0))/100 AS maxPrice, (SELECT Vat FROM [dbo].[_Lum_Erp_VatDetails]Where (('" + fromDate + "'>=FromDate AND '" + fromDate + "'< =ToDate)) AND itemType=i.itemType) as tax from Lum_Erp_Item i where i.itemId={0}", itemDdl.SelectedValue);

        }
        else if (rbTaxType.SelectedValue == "2")
        {
            sqlstr = string.Format("SELECT MRP,MRP+(MRP * ISNULL(cst,0))/100 AS maxPrice, cst as tax from Lum_Erp_Item where itemId={0}", itemDdl.SelectedValue);

        }
        else if (rbTaxType.SelectedValue == "3")
        {
            sqlstr = string.Format("SELECT MRP,MRP+(MRP * ISNULL(cstAgainstFormC,0))/100 AS maxPrice, cstAgainstFormC as tax from Lum_Erp_Item where itemId={0}", itemDdl.SelectedValue);

        }


        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(sqlstr))
            {
                if (reader.Read())
                {
                    retVal = Convert.ToString(reader["maxPrice"]);
                    hvat.Value = Convert.ToString(reader["tax"]);
                }
            }
        }
        return retVal;

    }
    public void resetControl()
    {
        divisionDdl.SelectedIndex = -1;
        subDivisionDdl.SelectedIndex = -1;
        itemDdl.SelectedIndex = -1;
        txtquantity.Text = string.Empty;
        lblAvailable.Text = "";
        txtserialNo.Text = "";
        price.Text = string.Empty;
        txtinstAmt.Text = string.Empty;
        itemDescription.Text = string.Empty;
        discount.Text = string.Empty;
    }
    protected void OnEdit(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
    {
        DataGrid1.EditItemIndex = e.Item.DataSetIndex;

        NewBindData();
    }

    private void OnUpdate(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
    {
        DataGrid1.EditItemIndex = -1;

        NewBindData();
    }

    //On clicking 'cancel' and bind data
    private void OnCancel(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
    {
        DataGrid1.EditItemIndex = -1;

        NewBindData();
    }

    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.DataGrid1.CancelCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.OnCancel);
        this.DataGrid1.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.OnEdit);
        this.DataGrid1.UpdateCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.OnUpdate);
        this.Load += new System.EventHandler(this.Page_Load);

    }
    #endregion
    private void NewBindData()
    {
        //for each row in our datagrid
        foreach (DataGridItem item in DataGrid1.Items)
        {
            props = new Props();

            //to read the ItemTemplate
            if (!item.ItemType.ToString().Equals("EditItem"))
            {
                props.id = ((Label)item.Cells[1].FindControl("Label1")).Text;
                props.installmentDate = ((Label)item.Cells[2].FindControl("Label2")).Text;
                props.installmentamt = ((Label)item.Cells[3].FindControl("Label3")).Text;
            }
            //to read the EditItemTemplate
            else if (item.ItemType.ToString().Equals("EditItem"))
            {
                props.id = ((TextBox)item.Cells[1].FindControl("Textbox1")).Text;
                props.installmentDate = ((TextBox)item.Cells[2].FindControl("Textbox2")).Text;
                props.installmentamt = ((TextBox)item.Cells[3].FindControl("Textbox3")).Text;

            }

            list.Add(props);
        }

        //datasoure will be the arraylist with props object.
        DataGrid1.DataSource = list;
        DataGrid1.DataBind();
    }
    protected void btncreate_Click(object sender, EventArgs e)
    {
        decimal packingCharges = 0;
        if (!string.IsNullOrEmpty(txtCharge.Text))
        {
            packingCharges = Convert.ToDecimal(txtCharge.Text);
        }
        decimal buyBackAmount = 0;
        if (!string.IsNullOrEmpty(txtBuyBackamount.Text))
        {
            buyBackAmount = Convert.ToDecimal(txtBuyBackamount.Text);
        }
        #region update
        DateTime date = DateTime.Today;
        if (string.IsNullOrEmpty(poDate.Text))
        {
            poDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
        }
        else
        {
            date = DateTime.ParseExact(poDate.Text, "dd/MM/yyyy", null);
        }

        object obj = Convert.DBNull;
        if (ddlAditional.SelectedIndex != 0)
        {
            obj = ddlAditional.SelectedValue;
        }
        object _objFormR = Convert.DBNull;

        if (rbTaxType.SelectedValue == "3")
        {
            _objFormR = dllformrecoveralbe.SelectedValue;
        }
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            if (_soirId > 0)
            {
                int i = conn.ExecuteCommandProc("Lum_Erp_Stp_UpdateChallanWithOutRespectToSoir",
                                           new SpParam("@soirId", _soirId, SqlDbType.Int),
                                           new SpParam("@challanId", _challanId, SqlDbType.Int),
                                           new SpParam("@raisedBy", raisedBy.Text, SqlDbType.VarChar),
                                           new SpParam("@primarySalesPersonId", ddlprimary.SelectedValue, SqlDbType.Int),
                                           new SpParam("@aditionalSalesPersonId", obj, SqlDbType.Int),
                                           new SpParam("@customerId", ddlCustomer.SelectedValue, SqlDbType.Int),
                                           new SpParam("@customerPoNo", poReference.Text, SqlDbType.VarChar),
                                           new SpParam("@poDate", date, SqlDbType.DateTime),
                                           new SpParam("@orderTypeId", ddlOrder.SelectedValue, SqlDbType.Int),
                                           new SpParam("@installationAddress", installationAddress.Text, SqlDbType.VarChar),
                                           new SpParam("@transporterId", ddlTransporter.SelectedValue, SqlDbType.Int),
                                           new SpParam("@CNDetails", txtCNDetails.Text, SqlDbType.VarChar),
                                           new SpParam("@isDispatch", 0, SqlDbType.Bit),
                                           new SpParam("@taxTypeId", rbTaxType.SelectedValue, SqlDbType.Int),
                                           new SpParam("@FormRecoverableId", _objFormR, SqlDbType.Int),
                                           new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int),
                                        new SpParam("@packingCharges", packingCharges, SqlDbType.Decimal),
                                        new SpParam("@buyBackAmount", buyBackAmount, SqlDbType.Decimal),
                                        new SpParam("@buyBackDetails", txtDetails.Text, SqlDbType.VarChar));


                lblMsg.Text = "Challan is updated";
                lblMsg.ForeColor = Color.Green;

            }

        }
        #endregion

        #region checking
        int lineId;
        decimal _quantity;
        int itemId;
        string serialNo;

        for (int i = 0; i < poLineGrid.Items.Count; i++)
        {
            _quantity = decimal.Parse(((Label)poLineGrid.Items[i].FindControl("itemQty")).Text);
            lineId = int.Parse(((HtmlInputHidden)poLineGrid.Items[i].FindControl("soirLineId")).Value);
            itemId = int.Parse(((HtmlInputHidden)poLineGrid.Items[i].FindControl("itemId")).Value);
            serialNo = ((TextBox)poLineGrid.Items[i].FindControl("itemSerialNo")).Text;
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                try
                {
                    if (poLineGrid.Items[i].Cells[0].Text.ToUpper() == "FINISHED GOODS")
                    {
                        if (serialNo != string.Empty)
                        {
                            string[] param = serialNo.Split(';');
                            if (serialNo != string.Empty)
                            {
                                if (_quantity != param.Length)
                                {
                                    gridMessage.Text = "quantity is not equal to total serialNo";
                                    gridMessage.ForeColor = Color.Red;
                                    return;
                                }
                            }
                            if (IsValidSerialNo(conn, serialNo, itemId, _quantity, int.Parse(ddlStorelocation.SelectedValue), _challanId, lineId))
                            {
                                gridMessage.Text = "";
                                gridMessage.ForeColor = Color.Green;

                                if (isDispachable())
                                {
                                    btnDispatch.Enabled = true;
                                }
                                else
                                {
                                    btnDispatch.Enabled = false;
                                }
                            }
                            else
                            {
                                //gridMessage.Text = "Serial Number is not available";
                                //gridMessage.ForeColor = Color.Red;
                                return;
                            }
                        }

                    }

                    else
                    {
                        if (IsValidSerialNo(conn, serialNo, itemId, _quantity, int.Parse(ddlStorelocation.SelectedValue), _challanId, lineId))
                        {
                            gridMessage.Text = "";
                            gridMessage.ForeColor = Color.Green;
                            if (isDispachable())
                            {
                                btnDispatch.Enabled = true;
                            }
                            else
                            {
                                btnDispatch.Enabled = false;
                            }
                        }
                        else
                        {
                            //gridMessage.Text = "Items are not available in store";
                            //gridMessage.ForeColor = Color.Red;
                            return;
                        }
                    }
                }
                catch (Exception ex)
                {
                    gridMessage.Text = ex.Message; ;
                    gridMessage.ForeColor = Color.Red;
                    return;
                }

            }
        }
        #endregion
        #region insertserialno
        for (int i = 0; i < poLineGrid.Items.Count; i++)
        {
            _quantity = decimal.Parse(((Label)poLineGrid.Items[i].FindControl("itemQty")).Text);
            lineId = int.Parse(((HtmlInputHidden)poLineGrid.Items[i].FindControl("soirLineId")).Value);
            itemId = int.Parse(((HtmlInputHidden)poLineGrid.Items[i].FindControl("itemId")).Value);
            serialNo = ((TextBox)poLineGrid.Items[i].FindControl("itemSerialNo")).Text;
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                try
                {
                    if (poLineGrid.Items[i].Cells[0].Text.ToUpper() == "FINISHED GOODS")
                    {
                        if (serialNo != string.Empty)
                        {
                            string[] param = serialNo.Split(';');
                            if (serialNo != string.Empty)
                            {
                                if (_quantity != param.Length)
                                {
                                    gridMessage.Text = "quantity is not equal to total serialNo";
                                    gridMessage.ForeColor = Color.Red;
                                    return;
                                }
                            }
                            if (IsValidSerialNo(conn, serialNo, itemId, _quantity, int.Parse(ddlStorelocation.SelectedValue), _challanId, lineId))
                            {
                                InsertItemSerialNo(lineId, _challanId, itemId, serialNo, conn, _quantity, int.Parse(ddlStorelocation.SelectedValue));

                                gridMessage.Text = "";
                                gridMessage.ForeColor = Color.Green;

                                if (isDispachable())
                                {
                                    btnDispatch.Enabled = true;
                                }
                                else
                                {
                                    btnDispatch.Enabled = false;
                                }
                            }
                            else
                            {
                                // gridMessage.Text = "Serial Number is not available";
                                // gridMessage.ForeColor = Color.Red;
                                return;
                            }
                        }
                    }

                    else
                    {
                        if (IsValidSerialNo(conn, serialNo, itemId, _quantity, int.Parse(ddlStorelocation.SelectedValue), _challanId, lineId))
                        {
                            InsertItemSerialNo(lineId, _challanId, itemId, serialNo, conn, _quantity, int.Parse(ddlStorelocation.SelectedValue));
                            gridMessage.Text = "";
                            gridMessage.ForeColor = Color.Green;
                            if (isDispachable())
                            {
                                btnDispatch.Enabled = true;
                            }
                            else
                            {
                                btnDispatch.Enabled = false;
                            }
                        }
                        else
                        {
                            // gridMessage.Text = "Items are not available in store";
                            // gridMessage.ForeColor = Color.Red;
                            return;
                        }
                    }
                }
                catch (FormatException)
                {
                    gridMessage.Text = "Invalid quantity or price.";
                    gridMessage.ForeColor = Color.Red;
                    return;
                }

            }
        }
        #endregion
        #region installment
        int id;
        string installmentDate;
        string installmentamt;
        if (_challanId > 0)
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                conn.ExecuteCommand("Delete from Lum_Erp_InstallmentDetails  WHERE challanId =" + _challanId);
            }
            for (int i = 0; i < DataGrid1.Items.Count; i++)
            {
                //id = ((Label)DataGrid1.Items[i].Cells[1].FindControl("Label1")).Text;
                installmentDate = ((Label)DataGrid1.Items[i].Cells[2].FindControl("Label2")).Text;
                installmentamt = ((Label)DataGrid1.Items[i].Cells[3].FindControl("Label3")).Text;

                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    try
                    {
                        conn.ExecuteCommandProc("Lum_Erp_Stp_CreateInstallmentDetails", new SpParam("@challanId", _challanId, SqlDbType.Int),
                                                                        new SpParam("@price", installmentamt, SqlDbType.Decimal),
                                                                        new SpParam("@installmentDate", installmentDate, SqlDbType.DateTime),
                                                                        new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int));
                    }
                    catch (Exception ex)
                    {
                        errmsg.InnerHtml = ex.ToString();
                    }
                }
            }
        }

        #endregion
        Response.Redirect("ChallanCumInvoiceList.aspx");
    }
    protected void addItemLine_Click(object sender, EventArgs e)
    {
        if (divisionDdl.Items[divisionDdl.SelectedIndex].Text.ToUpper() == "FINISHED GOODS")
        {

            if (txtquantity.Text.IndexOf(".") >= 0)
            {
                gridMessage.Text = "FINISHED GOODS QUANTITY CANNOT BE DECIMAL ";
                gridMessage.ForeColor = Color.Red;
                return;
            }
        }
        savechallan();

    }
    protected void btnPrintChallan_Click(object sender, EventArgs e)
    {
        if (_challanId > 1)
        {
            Response.Redirect("ChallanToSoirPrint.aspx?ChallanId=" + _challanId);
        }
        else
        {
            lblMsg.Text = "Create Challan before Print.";
            lblMsg.ForeColor = Color.Red;
        }
    }
    protected void btnPrintInvoice_Click(object sender, EventArgs e)
    {
        if (_challanId > 1)
        {
            if (RbtnSelectInvoice.SelectedValue == "1")
                Response.Redirect("OriginalInvoicePrint.aspx?ChallanId=" + _challanId);
            if (RbtnSelectInvoice.SelectedValue == "2")
                Response.Redirect("DuplicateInvoicePrint.aspx?ChallanId=" + _challanId);
            if (RbtnSelectInvoice.SelectedValue == "3")
                Response.Redirect("ExtraInvoicePrint.aspx?ChallanId=" + _challanId);
        }
        else
        {
            lblMsg.Text = "Create Challan before Print.";
            lblMsg.ForeColor = Color.Red;
        }
    }
}
