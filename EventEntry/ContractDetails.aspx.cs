﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Bitscrape.AppBlock.Database;

public partial class EventEntry_ContractDetails : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        BindGrid();
    }
    protected void lookup_Click(object sender, EventArgs e)
    {
        ResetAll();
        LoadDate();
    }
    protected void add_Click(object sender, EventArgs e)
    {
        try
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                //DateTime _txtCMRDate = DateTime.Today;
                //if (string.IsNullOrEmpty(txtCMRDate.Text))
                //{
                //    txtCMRDate.Text = DateTime.Today.ToString("dd MMM yyyy");
                //}
                //else
                //{
                //    _txtCMRDate = DateTime.ParseExact(txtCMRDate.Text, "dd MMM yyyy", null);
                //}
                string _txtCMRDate = DateTime.Today.ToString("dd MMM yyyy");
                if (string.IsNullOrEmpty(txtCMRDate.Text))
                {
                }
                else
                {
                    _txtCMRDate = txtCMRDate.Text;
                }

                if (ConDetailId.Value == "")
                {
                    SpParam[] sp = new SpParam[13];
                    sp[0] = new SpParam("@custMacId", Convert.ToInt32(CustMacId.Value), SqlDbType.Int);
                    sp[1] = new SpParam("@serviceContractId", Convert.ToInt32(ddlConType.SelectedValue), SqlDbType.Int);
                    sp[2] = new SpParam("@currMeterReading", Convert.ToDecimal(txtCurMeterRead.Text.Trim()), SqlDbType.Decimal);
                    sp[3] = new SpParam("@currMeterDate", _txtCMRDate, SqlDbType.DateTime);
                    sp[4] = new SpParam("@contractNo", txtConNo.Text.Trim(), SqlDbType.VarChar);
                    sp[5] = new SpParam("@contractStartDate", DateTime.ParseExact(txtCSDate.Text.Trim(), "dd MMM yyyy", null), SqlDbType.DateTime);
                    sp[6] = new SpParam("@meterOnContractStart", Convert.ToDecimal(txtMeterReadCSD.Text.Trim()), SqlDbType.Decimal);
                    sp[7] = new SpParam("@contractEndDate", DateTime.ParseExact(txtCEDate.Text.Trim(), "dd MMM yyyy", null), SqlDbType.DateTime);
                    sp[8] = new SpParam("@lifeCopy", Convert.ToInt32(txtLifeCopy.Text.Trim()), SqlDbType.Int);
                    sp[9] = new SpParam("@freeCopy", Convert.ToInt32(txtFreeCopy.Text.Trim()), SqlDbType.Int);
                    sp[10] = new SpParam("@serviceCopy", Convert.ToInt32(txtServiceCopy.Text.Trim()), SqlDbType.Int);
                    sp[11] = new SpParam("@copyRate", Convert.ToDecimal(txtCopyRate.Text.Trim()), SqlDbType.Decimal);
                    sp[12] = new SpParam("@minCopy", Convert.ToInt32(txtMinCopy.Text.Trim()), SqlDbType.Int);
                
                    int i = conn.ExecuteCommandProc("Lum_Erp_Stp_AddContractDetails", sp);
                    if (i > 0)
                    {
                        ResetAll();
                        lookupMessage.Text = "Contract Details Added.";
                        message.ForeColor = Color.Green;
                    }
                }
                else
                {
                    SpParam[] sp = new SpParam[14];
                    sp[0] = new SpParam("@custMacId", Convert.ToInt32(CustMacId.Value), SqlDbType.Int);
                    sp[1] = new SpParam("@serviceContractId", Convert.ToInt32(ddlConType.SelectedValue), SqlDbType.Int);
                    sp[2] = new SpParam("@currMeterReading", Convert.ToDecimal(txtCurMeterRead.Text.Trim()), SqlDbType.Decimal);
                    sp[3] = new SpParam("@currMeterDate", DateTime.ParseExact(txtCMRDate.Text.Trim(), "dd MMM yyyy", null), SqlDbType.DateTime);
                    sp[4] = new SpParam("@contractNo", txtConNo.Text.Trim(), SqlDbType.VarChar);
                    sp[5] = new SpParam("@contractStartDate", DateTime.ParseExact(txtCSDate.Text.Trim(), "dd MMM yyyy", null), SqlDbType.DateTime);
                    sp[6] = new SpParam("@meterOnContractStart", Convert.ToDecimal(txtMeterReadCSD.Text.Trim()), SqlDbType.Decimal);
                    sp[7] = new SpParam("@contractEndDate", DateTime.ParseExact(txtCEDate.Text.Trim(), "dd MMM yyyy", null), SqlDbType.DateTime);
                    sp[8] = new SpParam("@lifeCopy", Convert.ToInt32(txtLifeCopy.Text.Trim()), SqlDbType.Int);
                    sp[9] = new SpParam("@freeCopy", Convert.ToInt32(txtFreeCopy.Text.Trim()), SqlDbType.Int);
                    sp[10] = new SpParam("@serviceCopy", Convert.ToInt32(txtServiceCopy.Text.Trim()), SqlDbType.Int);
                    sp[11] = new SpParam("@copyRate", Convert.ToDecimal(txtCopyRate.Text.Trim()), SqlDbType.Decimal);
                    sp[12] = new SpParam("@minCopy", Convert.ToInt32(txtMinCopy.Text.Trim()), SqlDbType.Int);                
                    sp[13] = new SpParam("@contractId", Convert.ToInt32(ConDetailId.Value), SqlDbType.Int);
                    int i = conn.ExecuteCommandProc("Lum_Erp_Stp_UpdateContractDetails", sp);
                    if (i > 0)
                    {
                        ResetAll();
                        lookupMessage.Text = "Contract Details Updated.";
                        message.ForeColor = Color.Green;
                    }
                }
                txtSRNo.Text = "";
            }
        }
        catch (SqlException ex)
        {
            lookupMessage.Text = ex.ToString();
            lookupMessage.ForeColor = Color.Red;
        }
    }
    protected void cancel_Click(object sender, EventArgs e)
    {
        ResetAll();
    }
    public void LoadDate()
    {
        bool isSerial = false;
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            SpParam[] sp=new SpParam[2];
            sp[0] = new SpParam("@custMacId", DBNull.Value, SqlDbType.Int);
            sp[1] = new SpParam("@SRNo", txtSRNo.Text.ToString(), SqlDbType.VarChar);
            using (IDataReader reader = conn.ExecuteQueryProc("Lum_Erp_Stp_GetCustomerMachine", sp))
            {
                if (reader.Read())
                {
                    customerName.Text = Convert.ToString(reader["customerName"]);
                    machineModel.Text = Convert.ToString(reader["itemCode"]);
                    iAddress.Text = Convert.ToString(reader["SAdd"]);
                    bAddress.Text = Convert.ToString(reader["BAdd"]);
                    CustMacId.Value = Convert.ToString(reader["custMacId"]);
                    lookupMessage.Text = "Serial No. found. Customer/Machine data loaded";
                    lookupMessage.ForeColor = Color.Green;
                    isSerial = true;
                }
                else
                {
                    customerName.Text = "";
                    machineModel.Text = "";
                    iAddress.Text = "";
                    bAddress.Text = "";
                    CustMacId.Value = "";
                    lookupMessage.Text = "Serial No. not found.";
                    lookupMessage.ForeColor = Color.Red;
                }
            }
            if (isSerial)
            {
                Panel1.Visible = true;
                bindControl();
                SpParam _sp = new SpParam("@custMacId", Convert.ToInt32(CustMacId.Value), SqlDbType.Int);
                using (IDataReader reader1 = conn.ExecuteQueryProc("Lum_Erp_Stp_GetContractDetails", _sp))
                {
                    if (reader1.Read())
                    {
                        contractLookupMessage.Text = "Details of the existing active contract loaded.";
                        contractLookupMessage.ForeColor = Color.Green;
                        ConDetailId.Value = Convert.ToString(reader1["contractId"]);
                        //machineModel.Text = Convert.ToString(reader1["custMacId"]);
                        ddlConType.SelectedValue = Convert.ToString(reader1["serviceContractId"]);
                        txtCurMeterRead.Text = Convert.ToString(reader1["currMeterReading"]);
                        txtCMRDate.Text = Convert.ToString(reader1["currMeterDate"]);
                        txtConNo.Text = Convert.ToString(reader1["contractNo"]);
                        txtCSDate.Text = Convert.ToString(reader1["contractStartDate"]);
                        txtMeterReadCSD.Text = Convert.ToString(reader1["meterOnContractStart"]);
                        txtCEDate.Text = Convert.ToString(reader1["contractEndDate"]);
                        txtLifeCopy.Text = Convert.ToString(reader1["lifeCopy"]);
                        txtFreeCopy.Text = Convert.ToString(reader1["freeCopy"]);
                        txtServiceCopy.Text = Convert.ToString(reader1["serviceCopy"]);
                        txtCopyRate.Text = Convert.ToString(reader1["copyRate"]);
                        txtMinCopy.Text = Convert.ToString(reader1["minCopy"]);
                        Submit.Text = "Update";
                    }
                    else
                    {
                        contractLookupMessage.Text = "No existing active contract for this serial number. Enter one below.";
                        contractLookupMessage.ForeColor = Color.Red;
                        ConDetailId.Value = "";
                        //machineModel.Text = Convert.ToString(reader1["custMacId"]);
                        ddlConType.SelectedValue = "0";
                        txtCurMeterRead.Text = "";
                        txtCMRDate.Text = "";
                        txtConNo.Text = "";
                        txtCSDate.Text = "";
                        txtMeterReadCSD.Text = "";
                        txtCEDate.Text = "";
                        txtLifeCopy.Text = "";
                        txtFreeCopy.Text = "";
                        txtServiceCopy.Text = "";
                        txtCopyRate.Text = "";
                        txtMinCopy.Text = "";
                    }
                }
            }
            else
            {
                Panel1.Visible = false;
            }
        }
    }
    public void bindControl()
    {
        BindListControl _control = new BindListControl();
        _control.BindServiceContract(ddlConType);
    }
    public void ResetAll()
    {
        CustMacId.Value = "";
        lookupMessage.Text = "";
        customerName.Text = "";
        machineModel.Text = "";
        iAddress.Text = "";
        bAddress.Text = "";
        contractLookupMessage.Text = "";
        ddlConType.Items.Clear();
        txtCurMeterRead.Text = "";
        txtCMRDate.Text = "";
        txtConNo.Text = "";
        txtCSDate.Text = "";
        txtMeterReadCSD.Text = "";
        txtCEDate.Text = "";
        txtLifeCopy.Text = "";
        txtFreeCopy.Text = "";
        txtServiceCopy.Text = "";
        txtCopyRate.Text = "";
        txtMinCopy.Text = "";
        Submit.Text = "Add";
        ConDetailId.Value = "";
        message.Text = "";
        Panel1.Visible = false;
    }
    public void BindGrid()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (DataSet ds = conn.GetDataSet("Lum_erp_stp_getallcontract", "table"))
            {
                //gvCSA.AutoGenerateColumns=
                gvCSA.DataSource = ds;
                gvCSA.DataBind();
            }
        }
    }
}
