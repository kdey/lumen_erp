﻿using System.Data;
using System;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;

public partial class EventEntry_PurchaseOrder2 : BasePage
{
    protected int _poId = -1;
    protected int _poStateId = -1;
    private double totalAmount = 0;
    public string Action = string.Empty;
    public int poVersion = -1;
    bool isdeleted = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!string.IsNullOrEmpty(Request["poId"]))
        {
            _poId = int.Parse(Request["poId"]);
        }
        if (!string.IsNullOrEmpty(Request["poVersion"]))
        {
            poVersion = int.Parse(Request["poVersion"]);
        }
        if (!string.IsNullOrEmpty(Request["View"]))
        {
            Action =(Request["View"]);
        }
        if (!this.IsPostBack)
        {
            LoadControls();
            if (_poId > 0)
            {
                LoadPo();
            }
           
        }
        if (GetLoggedinUser().UserType == 2)
        {

            //btnPrint.Enabled = false;
            //addItemLine.Enabled = false;
        }
    }

    /// <summary>
    /// Loads all the relevant details of this purchase order 
    /// </summary>
    private void LoadPo()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(@"select poId,strPoId,poTypeId,PO.vendorId,poDate,rateContractTypeId,poStateId,isBlocked,poVersion,
	                            createdByUser,isDeleted,grnFulfilled,(address1+ '  '+ address2) address
	                            FROM Lum_Erp_PurchaseOrder PO, Lum_Erp_Vendor V
	                            WHERE PO.vendorId = V.vendorId
	                            AND  poId  = " + _poId))
            {
                if (reader.Read())
                {
                    poNumber.Text = reader["strPoId"].ToString() + "/" + reader["poVersion"].ToString();
                    txtvendoradd.Text = reader["address"].ToString();
                    poVersion = int.Parse(reader["poVersion"].ToString());
                    _poStateId = Convert.ToInt32(reader["poStateId"]);
                    isdeleted = Convert.ToBoolean(reader["isdeleted"]);
                    //if purchase order Is locekd

                    if (Action == "View")
                    {
                        tblItem.Visible = false;
                    }

                    if (_poStateId==2)
                    {
                        tblItem.Visible=false;
                    }
                    // now set the poType in the drop down
                    string poTypeId = reader["poTypeId"].ToString();
                    ListItem li = poTypeDdl.Items.FindByValue(poTypeId);
                    if (li != null)
                    {
                        poTypeDdl.SelectedIndex = -1;
                        li.Selected = true;
                    }
                    // now set the vendor company in the drop down.
                    string vendorId = reader["vendorId"].ToString();
                    li = vendorDdl.Items.FindByValue(vendorId);
                    if (li != null)
                    {
                        vendorDdl.SelectedIndex = -1;
                        li.Selected = true;
                    }
                    poDate.Text = ((DateTime)reader["poDate"]).ToString("dd/MM/yyyy");
                    // get teh rate contract type
                    string p = reader["rateContractTypeId"].ToString();
                    li = rateContractDdl.Items.FindByValue(p);
                    if (li != null)
                    {
                        rateContractDdl.SelectedIndex = -1;
                        li.Selected = true;
                    }
                    vendorDdl.Enabled = false;
                    poTypeDdl.Enabled = false;
                    rateContractDdl.Enabled = false;
                }
            }
            BindGrid(conn, isdeleted);
            lblTotal.Text = string.Format("{0:N}", Math.Round(totalAmount));
            lblPrice.Text = changeCurrencyToWords(Math.Round(totalAmount));
            lblquantity.Text = GetTotalQuantity();
        }
    }

    /// <summary>
    /// Loads the initial item entry grid drop down controls and other controls.
    /// </summary>
    private void LoadControls()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            // load the purchase order type
            //8th march
            using (IDataReader reader = conn.ExecuteQuery("select poTypeId,poTypeCode as poTypeCode from Lum_Erp_PurchaseOrderType Order By poTypeCode"))
            {
                poTypeDdl.DataSource = reader;
                poTypeDdl.DataTextField = "poTypeCode";
                poTypeDdl.DataValueField = "poTypeId";
                poTypeDdl.DataBind();
            }
            // load the vendors.
            using (IDataReader reader = conn.ExecuteQuery("select vendorId, vendorName from Lum_Erp_Vendor Order By vendorName"))
            {
                vendorDdl.DataSource = reader;
                vendorDdl.DataValueField = "vendorId";
                vendorDdl.DataTextField = "vendorName";
                vendorDdl.DataBind();

            }
            // load the rate Contracts
            using (IDataReader reader = conn.ExecuteQuery("select rateContractTypeId, contractType FROM Lum_Erp_RateContractType Order  By contractType"))
            {
                rateContractDdl.DataSource = reader;
                rateContractDdl.DataTextField = "contractType";
                rateContractDdl.DataValueField = "rateContractTypeId";
                rateContractDdl.DataBind();

            }
            // load the divisions
            //using (IDataReader reader = conn.ExecuteQuery("select divisionId, divisionName from Lum_Erp_Division Order By divisionName"))
            //{
            //    divisionDdl.DataSource = reader;
            //    divisionDdl.DataTextField = "divisionName";
            //    divisionDdl.DataValueField = "divisionId";
            //    divisionDdl.DataBind();
            //}
            //if (divisionDdl.Items.Count > 0)
            //{
            //    divisionDdl.SelectedIndex = 0;
            //}
            //// load the subvisions
            //LoadSubDivisions(conn);
            //// load the items
            //LoadItems(conn);

        }
    }

    /// <summary>
    /// Loads the item drop down control and the associated item text and description
    /// for the selected item
    /// </summary>
    /// <param name="conn"></param>
    private void LoadItems(IConnection conn)
    {
        if (subDivisionDdl.SelectedItem != null)
        {
            using (IDataReader reader = conn.ExecuteQuery("select itemId, itemCode from Lum_Erp_Item where subDivisionId = " + subDivisionDdl.SelectedValue + " Order By itemCode"))
            {
                itemDdl.DataSource = reader;
                itemDdl.DataTextField = "itemCode";
                itemDdl.DataValueField = "itemId";
                itemDdl.DataBind();
            }
            if (itemDdl.Items.Count > 0)
            {
                itemDdl.SelectedIndex = 0;
            }
            LoadItemDescription(conn);
        }
    }

    /// <summary>
    /// Loads the item description of the selected item in the drop down.
    /// </summary>
    /// <param name="conn"></param>
    private void LoadItemDescription(IConnection conn)
    {
         string fromDate = Convert.ToDateTime(poDate.Text).ToString("dd MMM yyyy");
        if (!string.IsNullOrEmpty(itemDdl.SelectedValue))
        {
            using (IDataReader reader = conn.ExecuteQuery(
                        @"select itemDescription, uomName, v.Vat as vat 
                          from Lum_Erp_Item i
                          inner join Lum_Erp_Uom u on u.uomId = i.uomId 
                          inner join dbo._Lum_Erp_VatDetails v on i.itemType=v.itemType
                          where i.itemId = '" + itemDdl.SelectedValue +"' and '" + fromDate + "' >=v.FromDate and '" + fromDate + "' <= v.ToDate"))
            {
                if (reader.Read())
                {
                    itemDescription.Text = reader.GetString(0);
                    uom.Text = reader.GetString(1);
                    vat.Text = reader["vat"].ToString();
                    vat.Visible = true;
                }
            }
        }
    }

    /// <summary>
    /// Loads the subdivions that are the children of the current selected division.
    /// </summary>
    /// <param name="conn"></param>
    private void LoadSubDivisions(IConnection conn)
    {
        if (divisionDdl.SelectedItem != null)
        {
            using (IDataReader reader = conn.ExecuteQuery("select subDivisionId, subDivisionName from Lum_Erp_SubDivision where divisionId = " + divisionDdl.SelectedValue + " Order By subDivisionName "))
            {
                subDivisionDdl.DataSource = reader;
                subDivisionDdl.DataValueField = "subDivisionId";
                subDivisionDdl.DataTextField = "subDivisionName";
                subDivisionDdl.DataBind();
            }
            if (subDivisionDdl.Items.Count > 0)
            {
                subDivisionDdl.SelectedIndex = 0;
            }
        }
    }

    /// <summary>
    /// Event handler for division ddl. changes the subdivisions, and the items  accordingly.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void divisionDdl_SelectedIndexChanged(object sender, EventArgs e)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            LoadSubDivisions(conn);
            LoadItems(conn);
        }
    }

    /// <summary>
    /// subdivision ddl eventhandler. Loads the items for this subdivision.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void subDivisionDdl_SelectedIndexChanged(object sender, EventArgs e)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            LoadItems(conn);
        }
    }

    /// <summary>
    /// item ddl event handler. Load the item description for the selected item.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void itemDdl_SelectedIndexChanged(object sender, EventArgs e)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            LoadItemDescription(conn);
        }
    }

    /// <summary>
    /// Adds the item line to the PO.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void addItemLine_Click(object sender, EventArgs e)
    {
        // check to see if we need to create a new po.
        // get the date 
        if (quantity.Text == "" || Convert.ToDecimal(quantity.Text) <= 0)
        {
            message.Text = "Please Check the Quantity";
            message.ForeColor = Color.Red ;
            return;
        }
        if (price.Text == "" || Convert.ToDecimal(price.Text) <= 0)
        {
            message.Text = "Please Check the Price";
            message.ForeColor = Color.Red;
            return;
        }

        DateTime date = DateTime.Today;
        if (string.IsNullOrEmpty(poDate.Text))
        {
            poDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
        }
        else
        {
            date = DateTime.ParseExact(poDate.Text.Trim(), "dd/MM/yyyy", null);
        }
        Validate();
        if (IsValid)
        {

                if (_poId <= 0)
                {

                    try
                    {
                        using (IConnection conn = DataConnectionFactory.GetConnection())
                        {
                            using (IDataReader reader = conn.ExecuteQueryProc("Lum_Erp_stp_CreatePurchaseOrder",
                                                        new SpParam("@strPoId", string.Empty, SqlDbType.VarChar),
                                                        new SpParam("@poTypeId", poTypeDdl.SelectedValue, SqlDbType.Int),
                                                        new SpParam("@vendorId", vendorDdl.SelectedValue, SqlDbType.Int),
                                                        new SpParam("@poDate", date, SqlDbType.DateTime),
                                                        new SpParam("@rateContractTypeId", rateContractDdl.SelectedValue, SqlDbType.Int),
                                                        new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int),
                                                        new SpParam("@createdByUser", true, SqlDbType.Bit)))
                            {
                                if (reader.Read())
                                {

                                    _poId = Convert.ToInt32(reader["POID"]);

                                    poNumber.Text = Convert.ToString(reader["STRPOID"]); ;
                                }
                                message.Text = "Purchase Order is added ";
                                message.ForeColor = Color.Green;
                                vendorDdl.Enabled = false;
                                poTypeDdl.Enabled = false;
                                rateContractDdl.Enabled = false;

                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message.Text = ex.Message;
                        message.ForeColor = Color.Red;
                        return;
                    }
               }
               if (_poId > 0)
                    {
                        try
                        {
                            using (IConnection conn = DataConnectionFactory.GetConnection())
                            {
                                string fromDate = Convert.ToDateTime(poDate.Text).ToString("dd MMM yyyy");
                                conn.ExecuteCommandProc("Lum_Erp_Stp_CreatePurchaseOrderDetail", new SpParam("@poId", _poId, SqlDbType.Int),
                                                          new SpParam("@itemId", itemDdl.SelectedValue, SqlDbType.Int),
                                                          new SpParam("@CDate", fromDate, SqlDbType.SmallDateTime),
                                                          new SpParam("@quantity", quantity.Text, SqlDbType.Decimal),
                                                          new SpParam("@price", price.Text, SqlDbType.Decimal),
                                                          new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int));

                                if (poVersion > 0)
                                {
                                    int i = conn.ExecuteCommand(string.Format("update Lum_Erp_PurchaseOrder set poVersion = poVersion+1 where poId = {0}", _poId));
                                    if (i > 0)
                                    {
                                        poVersion = -1;
                                    }
                                }
                                BindGrid(conn, false);
                            }
                        }
                        catch (Exception ex)
                        {
                            message.Text = ex.Message;
                            message.ForeColor = Color.Red;
                            return;
                        }

                    }
                    message.Text = "Purchase order item added.";
                    message.ForeColor = Color.Green;
                    lblTotal.Text = string.Format("{0:N}",Math.Round(totalAmount));
                    lblPrice.Text = changeCurrencyToWords(Math.Round(totalAmount));
                    lblquantity.Text = GetTotalQuantity();
                    price.Text = "";
                    quantity.Text = "";
                }
       
    }
        


    

    /// <summary>
    /// Binds the po details grid
    /// </summary>
    /// <param name="conn"></param>
    private void BindGrid(IConnection conn, bool isdeleted)
    {
        string fromDate = Convert.ToDateTime(poDate.Text).ToString("dd MMM yyyy");
        string sql = string.Empty;
        if (isdeleted == false)
        {
            sql = @"select poLineId, d.divisionName, sd.subdivisionName,i.itemCode, i.itemDescription, pd.quantity,  u.uomName, pd.price, (SELECT Vat FROM [dbo].[_Lum_Erp_VatDetails]Where (('"+fromDate+"'>=FromDate AND '"+fromDate+"'< =ToDate)) AND itemType=i.itemType) as vat, (pd.price * pd.quantity * 0.01 * (SELECT Vat FROM [dbo].[_Lum_Erp_VatDetails]Where (('"+fromDate+"'>=FromDate AND '"+fromDate+"'< =ToDate)) AND itemType=i.itemType)) 'vatAmount' ,(pd.price * pd.quantity+pd.price * pd.quantity * 0.01 * (SELECT Vat FROM [dbo].[_Lum_Erp_VatDetails]Where (('"+fromDate+"'>=FromDate AND '"+fromDate+"'< =ToDate)) AND itemType=i.itemType)) AS total , 0 AS isDeleted from Lum_Erp_PurchaseOrderDetails pd inner join Lum_Erp_Item i on pd.itemId = i.itemId inner join Lum_Erp_Subdivision sd on sd.subdivisionId = i.subdivisionId inner join Lum_Erp_Division d on d.divisionId = sd.divisionId inner join Lum_Erp_UOM u on i.uomId = u.uomId where poId = " + _poId;
        }
        else
        {
            sql = @"select poLineId, d.divisionName, sd.subdivisionName,i.itemCode, i.itemDescription, pd.quantity,  u.uomName, pd.price, (SELECT Vat FROM [dbo].[_Lum_Erp_VatDetails]Where (('"+fromDate+"'>=FromDate AND '"+fromDate+"'< =ToDate)) AND itemType=i.itemType) as vat, (pd.price * pd.quantity * 0.01 * (SELECT Vat FROM [dbo].[_Lum_Erp_VatDetails]Where (('"+fromDate+"'>=FromDate AND '"+fromDate+"'< =ToDate)) AND itemType=i.itemType)) 'vatAmount' ,(pd.price * pd.quantity+pd.price * pd.quantity * 0.01 * (SELECT Vat FROM [dbo].[_Lum_Erp_VatDetails]Where (('"+fromDate+"'>=FromDate AND '"+fromDate+"'< =ToDate)) AND itemType=i.itemType)) AS total , 0 AS isDeleted from Lum_Erp_PurchaseOrderDetails_Block pd inner join Lum_Erp_Item i on pd.itemId = i.itemId inner join Lum_Erp_Subdivision sd on sd.subdivisionId = i.subdivisionId inner join Lum_Erp_Division d on d.divisionId = sd.divisionId inner join Lum_Erp_UOM u on i.uomId = u.uomId where poId = " + _poId;
        }
        using (IDataReader reader = conn.ExecuteQuery(sql))
        {
            poLineGrid.DataSource = reader;
            poLineGrid.DataBind();
        }
    }

    /// <summary>
    /// Event handler for item commands of the po line item grid. 
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void poLineGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {

        if (e.CommandName.Equals("Delete"))
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                int poLineId = (int) poLineGrid.DataKeys[e.Item.ItemIndex];
                conn.ExecuteCommandProc("Lum_Erp_Stp_DeletePurchaseOrderLine",
                    new SpParam("@poId", _poId, SqlDbType.Int),
                    new SpParam("@lineId", poLineId, SqlDbType.Int),
                    new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int));
                gridMessage.Text = "Line deleted";
                gridMessage.ForeColor = Color.Green;

            }
        }
        else if (e.CommandName.Equals("Update"))
        {
            int poLineId = (int)poLineGrid.DataKeys[e.Item.ItemIndex];
            decimal quantity = decimal.Parse(((TextBox)e.Item.FindControl("itemQty")).Text);
            decimal price = decimal.Parse(((TextBox)e.Item.FindControl("itemPrice")).Text);

            if (price == 0)
            {
                gridMessage.Text = "Please Check the Price before Update !!";
                gridMessage.ForeColor = Color.Red ;
                return;
            }

            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                try
                {
                    
                    if (quantity == 0)
                    {
                        conn.ExecuteCommandProc("Lum_Erp_Stp_DeletePurchaseOrderLine",
                                new SpParam("@poId", _poId, SqlDbType.Int),
                                new SpParam("@lineId", poLineId, SqlDbType.Int),
                                new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int));
                            gridMessage.Text = "Line deleted";
                            gridMessage.ForeColor = Color.Green;
                    }
                    else
                    {
                        conn.ExecuteCommandProc("Lum_Erp_Stp_UpdatePurchaseOrderLine",
                            new SpParam("@poId", _poId, SqlDbType.Int),
                            new SpParam("@poLineId", poLineId, SqlDbType.Int),
                            new SpParam("@quantity", quantity, SqlDbType.Decimal),
                            new SpParam("@price", price, SqlDbType.Decimal),
                            new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int));
                    }
                    gridMessage.Text = "Line updated";
                    gridMessage.ForeColor = Color.Green;
                }
                catch(FormatException)
                {
                    gridMessage.Text = "Invalid quantity or price.";
                    gridMessage.ForeColor = Color.Red;
                }
            }
        }
        LoadPo();

    }
    protected void poLineGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //if purchase order Is locekd
            Button btnEdit = (Button)e.Item.Cells[9].Controls[0];
            Button btnDel = (Button)e.Item.Cells[10].Controls[0];
            if (_poStateId==2)
            {

                btnEdit.Enabled = false;

                btnDel.Enabled = false;
            }
            if (Action == "View")
            {

                btnEdit.Enabled = false;

                btnDel.Enabled = false;
            }
           
            btnDel.CssClass = "ButtonBig";
            btnEdit.CssClass = "ButtonBig";
            if (GetLoggedinUser().UserType == 2)
            {
                btnEdit.Enabled = false;
                btnDel.Enabled = false;
            }
        }
    }
   
    public string GetTotalQuantity()
    {
        string retVal = "";
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(@"select Sum(quantity) as TotalQty from Lum_Erp_PurchaseOrderDetails WHERE  poId=" + _poId))
            {
                if (reader.Read())
                {


                    retVal = Convert.ToString(reader["TotalQty"]);
                }
            }
        }
        return retVal;
    }
    private void CalcTotalAmount(string _netVal)
    {
        if (_netVal != "")
        {
            totalAmount += Double.Parse(_netVal);
        }

    }
    protected void poLineGrid_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CalcTotalAmount(e.Item.Cells[8].Text);

        }
    }
    protected void btnPrint_Click(object sender, EventArgs e)
    {
        if (_poId > 1)
        {
            Response.Redirect("PurchaseOrderPrint.aspx?PurchaseOrderId=" + _poId);
        }
        else
        {
            message.Text = "Create purchase order before Print.";
            message.ForeColor = Color.Red;
        }
    }
    protected void poLineGrid_PreRender(object sender, EventArgs e)
    {
        for (int _index = 0; _index < poLineGrid.Items.Count; _index++)
        {
            Button  btnupdate = (Button)poLineGrid.Items[_index].Cells[9].Controls[0];
            Button btndelete = (Button)poLineGrid.Items[_index].Cells[10].Controls[0];
            HtmlInputHidden _hdel = (HtmlInputHidden)poLineGrid.Items[_index].FindControl("isDeleted");

            if (_hdel.Value == "1")
            {
                
                btnupdate.Visible = false;
                btndelete.Visible = false;
            }
        }
    }
    protected void vendorDdl_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (vendorDdl.SelectedIndex != 0)
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                DataSet _ds = conn.GetDataSet(@"select vendorId,vendorName,(address1+ '  '+ address2) address
                                                from Lum_Erp_Vendor where vendorId = '" + vendorDdl.SelectedValue + "'", "GetAddress");


                if (_ds.Tables[0].Rows.Count != 0)
                {
                    txtvendoradd.Text = _ds.Tables[0].Rows[0]["address"].ToString() ;
                }


            }
        }

    }
}
