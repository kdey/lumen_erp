﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Bitscrape.AppBlock.Database;

public partial class EventEntry_ContractBill : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
    }
    protected void lookup_Click(object sender, EventArgs e)
    {
        LoadDate();
    }
    protected void add_Click(object sender, EventArgs e)
    {
        decimal _cmr = 0;
        if (txtCMR.Text.Trim() != "")
            _cmr = Convert.ToDecimal(txtCMR.Text.Trim());
        //DateTime date = DateTime.Today;
        //if (string.IsNullOrEmpty(txtCMRDate.Text))
        //{
        //    txtCMRDate.Text = DateTime.Today.ToString("dd MMM yyyy");
        //}
        //else
        //{
        //    date = DateTime.ParseExact(txtCMRDate.Text, "dd MMM yyyy", null);
        //}
        message.Text = "";
        string date = DateTime.Today.ToString("dd MMM yyyy");
        if (string.IsNullOrEmpty(txtCMRDate.Text))
        {
        }
        else
        {
            date = txtCMRDate.Text;
        }

        try
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                SpParam[] sp = new SpParam[3];
                sp[0] = new SpParam("@custMacId", Convert.ToInt32(CustMacId.Value), SqlDbType.Int);
                sp[1] = new SpParam("@currMeterReading",_cmr , SqlDbType.Decimal);
                sp[2] = new SpParam("@currMeterDate", date,  SqlDbType.DateTime);
                using (IDataReader reader = conn.ExecuteQueryProc("Lum_Erp_Stp_AddCSABill", sp))
                {
                    if (reader.Read())
                    {
                        string Id=Convert.ToString(reader["Id"]);
                        Response.Redirect("CSABill.aspx?Id="+Id);
                    }
                    else
                    {
                        lookupMessage.Text = "Duplicate Entry";
                        lookupMessage.ForeColor = Color.Red;
                    }
                }
            }
        }
        catch (SqlException ex)
        {
            lookupMessage.Text = ex.ToString();
            lookupMessage.ForeColor = Color.Red;
        }
    }
    protected void cancel_Click(object sender, EventArgs e)
    {

    }
    public void LoadDate()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            SpParam sp = new SpParam("@SRNo", txtSRNo.Text.ToString(), SqlDbType.VarChar);
            using (IDataReader reader = conn.ExecuteQueryProc("Lum_Erp_Stp_GetContractDetails_ForBill", sp))
            {
                if (reader.Read())
                {
                    customerName.Text = Convert.ToString(reader["customerName"]);
                    machineModel.Text = Convert.ToString(reader["itemCode"]);
                    CustMacId.Value = Convert.ToString(reader["custMacId"]);
                    serialNo.Text = Convert.ToString(reader["SRNo"]);
                    agreeNo.Text = Convert.ToString(reader["contractNo"]);
                    lookupMessage.Text = "Serial No. found.";
                    lookupMessage.ForeColor = Color.Green;
                    Panel1.Visible = true;
                    BindGrid();
                }
                else
                {
                    customerName.Text = "";
                    machineModel.Text = "";
                    CustMacId.Value = "";
                    serialNo.Text = "";
                    agreeNo.Text = "";
                    lookupMessage.Text = "Serial No. not found.";
                    lookupMessage.ForeColor = Color.Red;
                    Panel1.Visible = false;
                }
            }
        }
    }
    public void BindGrid()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            SpParam sp = new SpParam("@SRNo", txtSRNo.Text.ToString(), SqlDbType.VarChar);
            using (DataSet ds = conn.GetDataSet("Lum_Erp_Stp_GetCSABill", sp))
            {
                gvCSA.DataSource = ds;
                gvCSA.DataBind();
            }
        }
    }

    protected void gvCSA_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName.Equals("Block"))
        {
            int CSABillId = int.Parse(((HtmlInputHidden)e.Item.FindControl("CSABillId")).Value);
            int PaymentDode = int.Parse(((HtmlInputHidden)e.Item.FindControl("PaymentDode")).Value);
            if (PaymentDode > 0)
            {
                message.Text = "CSA Bill Cannot be Reversed, Payment already Done";
                message.ForeColor = Color.Red;
                return;
            }
            try
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    conn.ExecuteCommandProc("Lum_Erp_Stp_BlockCSABILL",
                    new SpParam("@CSABillId", CSABillId, SqlDbType.Int));

                    message.Text = "CSA BILL has been Blocked";
                    message.ForeColor = Color.Green;
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
                message.ForeColor = Color.Red;
            }

        }
    }
}
