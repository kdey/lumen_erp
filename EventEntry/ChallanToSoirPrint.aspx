﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ChallanToSoirPrint.aspx.cs" Inherits="EventEntry_ChallanWithoutRespectToSoirPrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Lumen Tele System Pvt. Ltd.</title>
    <link href="./../style/style.css" rel="stylesheet" type="text/css" />
   <script type ="text/javascript">
       function print_page() {

            var control = document.getElementById("btnPrint");
           
                control.style.visibility = "hidden";

            var control1 = document.getElementById("btnCancel");
            
                control1.style.visibility = "hidden";
            // else control.style.visibility = "visible";

            /*    var OLECMDID = 7;
            /* OLECMDID values:
            * 6 - print
            * 7 - print preview
            * 1 - open window
            * 4 - Save As
         
         var PROMPT = 1; // 2 DONTPROMPTUSER 
            var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
            document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
            WebBrowser1.ExecWB(OLECMDID, PROMPT);
            WebBrowser1.outerHTML = "";*/
          //  var OLECMDID = 7;
            /* OLECMDID values:
            * 6 - print
            * 7 - print preview
            * 1 - open window
            * 4 - Save As
            */
           // var PROMPT = 1; // 2 DONTPROMPTUSER
          //  var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
           // document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
            //WebBrowser1.ExecWB(OLECMDID, PROMPT);
            //WebBrowser1.outerHTML = "";
        }
        function viewControl() {
            var control = document.getElementById("btnPrint");
            control.style.visibility = "visible";
            var control1 = document.getElementById("btnCancel");
            control1.style.visibility = "visible";
			}
    </script>
    <style type="text/css">
        .style2
        {
            font-size: small;
        }
        .style3
        {
            height: 16px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="Header">
        <div class="HeaderImg">
        </div>
    </div>
    <div class="PageContentDiv">
    <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalTextBlack" >
        <tr class="NormalTextBlack">
            <td align="left" >&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td align="right">&nbsp;</td>
        </tr>
        <tr class="NormalTextBlack">
            <td colspan="4" align="center" style="font-size:large">CHALLAN</td>
        </tr>
    </table>
    
                <table class="PageContent" width="100%" border="0px" style="border-color:Black; margin:auto">
                     <tr class="NormalTextBlack">
                        <td align="left" style="width:20%">Date Of Challan :&nbsp;</td>
                        <td style="width:30%" align="left"><asp:Label ID="soirdt" runat="server"></asp:Label></td>
                        <td align="right" style="width:20%">Challan No.:&nbsp;&nbsp;</td>
                        <td align="left" style="width:30%"><asp:Label ID="lblChallanNo" runat="server" Text=""></asp:Label></td>
                     </tr>
                </table>
                <table class="PageContent" width="100%" style="border-color:Black;">
                         <tr class="NormalTextBlack">
                         <td class="style3">&nbsp;</td><td class="style3">&nbsp;</td><td class="style3">&nbsp;</td>
                             <td class="style3">&nbsp;</td>
                         </tr>
                         <tr class="NormalTextBlack">
                         <td style="width:20%" align="left">Customer Name:</td><td style="width:80%" align="left">
                             <asp:Label ID="CustomerName" runat="server"></asp:Label>
                             </td>
                         </tr>
                         <%--<tr class="NormalTextBlack">
                            <td style="width:20%" align="left">Contact Person with Ph. No.</td><td align="left" style="width:30%">
                               <asp:Label ID="ContactPersonWithPhoneNo" runat="server"></asp:Label>
                               </td><td style="width:20%" align="left">Contact Person with Ph. No.</td><td align="left" style="width:30%">
                               <asp:Label ID="ContactPersonwithPhCosignee" runat="server"></asp:Label>
                               </td>
                         </tr>--%>
                         <tr class="NormalTextBlack">
                            <td style="width:20%" align="left">Customer Address:</td><td style="width:80%" align="left">
                             <asp:Label ID="CustomerAddress" runat="server"></asp:Label>
                             </td>
                         </tr>
                         <tr class="NormalTextBlack">
                            <td style="width:20%" align="left">Cosignee Name:</td><td style="width:80%" align="left">
                             <asp:Label ID="CosigneeName" runat="server"></asp:Label>
                             </td>
                         </tr>
                         <tr class="NormalTextBlack">
                            <td style="width:20%" align="left">Cosignee Address:</td><td style="width:80%" align="left">
                             <asp:Label ID="CosigneeAddress" runat="server"></asp:Label>
                             </td>
                         </tr>
                         <tr class="NormalTextBlack">
                         </tr>
                         <tr class="NormalTextBlack">
                         <td>&nbsp;</td><td>&nbsp;          
                               </td><td>&nbsp;</td><td>&nbsp;</td>
                         </tr>
                </table>
                <table class="PageContent" width="100%" style="border-color:Black;">
                    <tr class="NormalTextBlack">
                        <td align="left" style="width:20%">Customer Order No.:</td><td align="left" style="width:30%">
                           <asp:Label ID="CustOrderNo" runat="server"></asp:Label>
                           </td><td align="left" style="width:20%">Customer Order Date.:</td><td align="left" style="width:30%"><asp:Label ID="CustOrderDate" runat="server"></asp:Label></td>
                     </tr>
                </table>
                 <table class="PageContent" width="100%" style="border-color:Black;">
                    <tr class="NormalTextBlack">
                        <td align="left" style="width:20%">Despatch Through : </td><td align="left" style="width:30%">
                           <asp:Label ID="DespatchThrough" runat="server"></asp:Label>
                           </td><td align="left" style="width:20%">C/N Details : </td><td align="left" style="width:30%"><asp:Label ID="lblCNDetails" runat="server"></asp:Label></td>
                     </tr>
                </table>
            <br />
            <br />  
        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="PageContent" style="border-color:Black;">
            <tr class="NormalTextBlack">
                <td colspan="4" align="center" style="font-size:small; font-weight:bold">Details of Materials</td>
            </tr>
        </table>  
        <table width="100%" class="PageContent" style="border-color:Black;">
            <tr class="NormalTextBlack">
                <td colspan="2">
                     <asp:DataGrid ID="poLineGrid" runat="server" AutoGenerateColumns="false"  GridLines="None" Width="100%">
                        <Columns>
                            <asp:TemplateColumn HeaderText="Model No/Part No" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Left"
                             ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                     <asp:Label ID="itemCode" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "itemDescription") %>'></asp:Label><br />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" Font-Bold="True" Width="20%" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Item Description" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Left"
                             ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                     <asp:Label ID="itemDescription" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "itd") %>'></asp:Label><br />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" Font-Bold="True" Width="40%" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="40%"></ItemStyle>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="false" HeaderText="FOC" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="foc" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "itemType") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" Font-Bold="True"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Serial No" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true">
                                <ItemTemplate>
                                    <asp:Label ID="itemSerialNo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SerialNo") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" Font-Bold="True" Width="20%" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Price" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true">
                                <ItemTemplate>
                                    <asp:Label ID="itemPrice" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "price") %>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="false" HeaderText="Discount" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true">
                                <ItemTemplate>
                                    <asp:Label ID="itemDiscount" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "discount") %>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"  HeaderStyle-Font-Bold="true">
                                <ItemTemplate>
                                    <asp:Label ID="lblTxtType" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "vatRate") %>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Quantity" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Bold="true">
                                <ItemTemplate>
                                  
                                    <asp:Label ID="itemQty" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "quantity") %>' Width="50" />
                                    <%#DataBinder.Eval(Container.DataItem, "uomName")%>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Right" Font-Bold="True" Width="30%" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="20%"></ItemStyle>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="false" DataField="total" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"  HeaderText="Total" HeaderStyle-Font-Bold="true"
                             DataFormatString="{0:#,###.##}" />
                            <asp:TemplateColumn Visible="false" >
                                <%--<ItemTemplate>
                                    <input type="hidden" id="soirLineId" value='<%#DataBinder.Eval(Container.DataItem, "soirLineId") %>' runat="server" />
                                </ItemTemplate>--%>
                            </asp:TemplateColumn>          
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
            <tr class="NormalTextBlack">
                <td style="width:70%" align="center">Total</td>
                <td style="width:30%" align="right"><asp:Label ID="lblTotalQty" runat="server" Text=""></asp:Label></td>
            </tr>
        </table>
        <table class="NormalTextBlack" width="100%">
            <tr class="NormalTextBlack">
                <td colspan="2" align="center">
                    <input  id="btnCancel" value="Cancel" type="button" runat="server"
                            style="width: 141px; cursor: hand;" onserverclick="btnCancel_ServerClick" />
                                <input  id="btnPrint" value="Print" type="button"
                            onclick="print_page();javascript:window.print(); viewControl();" 
                            style="width: 141px; cursor: hand;" />
                </td>
             </tr>
             <tr class="NormalTextBlack">
                <td style="width:70%"></td>
                <td style="width:30%">E.&.O.E</td>
             </tr>
             <tr class="NormalTextBlack">
                <td align="right" style="width:70%"></td>
                <td style="width:30%">for&nbsp;&nbsp;&nbsp;&nbsp;<strong>Lumen Tele Systems (P) Limited</strong></td>
             </tr>
             <tr class="NormalTextBlack">
                <td style="width:70%"></td>
                <td style="width:30%"></td>
             </tr>
             <tr class="NormalTextBlack">
                <td style="width:70%"></td>
                <td style="width:30%"></td>
             </tr>
         </table>
         <br />
         <br />    
        <table class="NormalTextBlack" width="100%">    
             <tr class="NormalTextBlack">
                <td align="left">Signature of Receiver with Rubber Stamp & Date</td>
                <td>Authorised Signatory</td>
             </tr>
             <tr class="NormalTextBlack">
                <td ></td>
                <td>
                    <asp:HiddenField ID="storeid" runat="server" />
                    <asp:HiddenField ID="taxType" runat="server" />
                </td>
             </tr>
        </table>
        <br />
        <br />
        <table class="NormalTextBlackSmall" width="100%">
             <tr>
                <td align="center" colspan="4">This is a Computer Generated Challan</td>
             </tr>
             <tr>
                <td align="center" colspan="4">System Designed & Developed by Industrial Software & Systems, www.bitscrape.com</td>
             </tr>
        </table>     
    </div>
    </form>
</body>
</html>
