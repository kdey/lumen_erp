﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;

public partial class EventEntry_Service : BasePage
{
    protected int serviceid = -1;
    protected int _isDeleted = -1;


    string Number;
    string deciml;
    string _number;
    string _deciml;
    string[] US = new string[1003];
    string[] SNu = new string[20];
    string[] SNt = new string[10];


    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.Params["serviceId"] != null)
        { serviceid = int.Parse(Request.Params["serviceId"]); 
        }
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!IsPostBack)
        {
            bindControl();
            if (Request.Params["serviceId"] != null)
            {
                serviceid = int.Parse(Request.Params["serviceId"]);
                //  bindControl_service();
                bindinfo();
            }
            
        }
      
        

        //if (GetLoggedinUser().UserType == 2)
        //{
        //    add.Visible  = false;
        //}
    }
    public void bindControl()
    {
         BindListControl _control = new BindListControl();
        _control.BindServiceCenter(ddlServiceCenter);
        //_control.BindDivision(ddlDivision);
        _control.BindCustomer(ddlCustomer);
        _control.BindServiceTax(ddlServiceTax);
        _control.BindServiceContract(ddlContractType);
        _control.BindHEducationCess(ddlHedCess);
        _control.BindEducationCess(ddlEdCess);
    }
    //public void bindControl_service()
    //{
    //    BindListControl _control = new BindListControl();
    //    _control.BindServiceCenter(ddlServiceCenter);
    //    _control.BindDivision(ddlDivision);
    //    _control.BindCustomer(ddlCustomer);
    //    _control.BindServiceTax(ddlServiceTax);
    //    _control.BindServiceContract(ddlContractType);
    //    _control.BindHEducationCess(ddlHedCess);
    //    _control.BindEducationCess(ddlEdCess);
    //}
    public void bindinfo()
    {
        serviceid = int.Parse(Request.Params["serviceId"]);
        //SpParam[] _Sparray = new SpParam[1];

        SpParam _Sparray = new SpParam("@serviceId", serviceid, SqlDbType.Int);

        try
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {


                DataSet ds = conn.GetDataSet("Lum_Erp_Stp_SelectServiceInfo_serviceid", _Sparray);

                if (ds.Tables[0].Rows.Count!=0)
                {
                    BindListControl _control = new BindListControl();

                    ddlServiceCenter.SelectedValue = ds.Tables[0].Rows[0]["serviceCenterId"].ToString();
                    //ddlDivision.SelectedValue=ds.Tables[0].Rows[0]["divisionId"].ToString();
                    
                    //_control.BindSubDivision(ddlSubdivision, ddlDivision.SelectedValue);
                    //ddlSubdivision.SelectedValue = ds.Tables[0].Rows[0]["subdivisionId"].ToString();
                    //_control.BindItems(ddlItem, ddlSubdivision.SelectedValue);
                    //ddlItem.SelectedValue = ds.Tables[0].Rows[0]["itemId"].ToString();
                    txtSerialNo.Text = ds.Tables[0].Rows[0]["serialNo"].ToString();
                    ddlCustomer.SelectedValue = ds.Tables[0].Rows[0]["customerId"].ToString();
                    poReference.Text = ds.Tables[0].Rows[0]["customerOrderNo"].ToString();

                    ddlContractType.SelectedValue = ds.Tables[0].Rows[0]["serviceContractId"].ToString();

                    txtDescription.Text = ds.Tables[0].Rows[0]["serviceDesc"].ToString();
                    txtServiecPrice.Text = ds.Tables[0].Rows[0]["price"].ToString();

                    if (ds.Tables[0].Rows[0]["servicetaxId"].ToString() == "")
                        ddlServiceTax.SelectedValue = "0";
                    else
                        ddlServiceTax.SelectedValue = ds.Tables[0].Rows[0]["servicetaxId"].ToString();

                    if (ds.Tables[0].Rows[0]["educationCessId"].ToString() == "")
                        ddlEdCess.SelectedValue = "0";
                    else
                        ddlEdCess.SelectedValue = ds.Tables[0].Rows[0]["educationCessId"].ToString();

                    if (ds.Tables[0].Rows[0]["higherEducationCessId"].ToString() == "")
                        ddlHedCess.SelectedValue = "0";
                    else
                        ddlHedCess.SelectedValue = ds.Tables[0].Rows[0]["higherEducationCessId"].ToString();

                    txtvat.Text = ds.Tables[0].Rows[0]["vat"].ToString();
                    lbl_strServiceId.Text = ds.Tables[0].Rows[0]["strServiceId"].ToString();

                    double _serviceTax =0.0;
                    if (ds.Tables[0].Rows[0]["servicetaxId"].ToString() == "")
                        _serviceTax = 0.0;
                    else
                        _serviceTax = Convert.ToDouble(ddlServiceTax.Items[ddlServiceTax.SelectedIndex].Text) * .01;


                    _serviceTax = Convert.ToDouble(ds.Tables[0].Rows[0]["price"].ToString()) * _serviceTax;

                    double educationCess =0.0;
                    double heducationCess =0.0;
                    if (ds.Tables[0].Rows[0]["educationCessId"].ToString() == "")
                        educationCess =0.0;
                    else
                        educationCess = Convert.ToDouble(ddlEdCess.Items[ddlEdCess.SelectedIndex].Text) * .01;// *_serviceTax;
                    
                    educationCess = _serviceTax * educationCess;

                    if (ds.Tables[0].Rows[0]["higherEducationCessId"].ToString() == "")
                        heducationCess = 0.0;
                    else 
                        heducationCess = Convert.ToDouble(ddlHedCess.Items[ddlHedCess.SelectedIndex].Text) * .01;// *_serviceTax;
                    
                    heducationCess = _serviceTax * heducationCess;
                    double vat = 0;
                    if (txtvat.Text.Length > 0)
                        vat = Convert.ToDouble(txtvat.Text);

                    double _total = Convert.ToDouble(ds.Tables[0].Rows[0]["price"].ToString()) + _serviceTax + educationCess + heducationCess+vat;
                    txtTotal.Text = string.Format("{0:N}", Math.Round(_total,0));
                    lblPrice.Text = changeCurrencyToWords(Convert.ToString(Math.Round(_total, 0)));
                    poDate.Text = Convert.ToString(ds.Tables[0].Rows[0]["pdate"]).Split(' ')[0]; 
                    
                    _isDeleted = Convert.ToInt16(ds.Tables[0].Rows[0]["isDeleted"]);
                    if (_isDeleted == 1)
                        add.Visible = false;
                }

            }
        }
        catch (Exception ex)
        {
            message.Text = ex.Message;
        }
    
    }
    //protected void ddlDivision_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    BindListControl _control = new BindListControl();
    //    _control.BindSubDivision(ddlSubdivision,ddlDivision.SelectedValue);
    //}
    //protected void ddlSubdivision_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    BindListControl _control = new BindListControl();
    //   // _control.BindItems(ddlItem,  ddlSubdivision.SelectedValue);
    //}
    protected void add_Click(object sender, EventArgs e)
    {
        if (serviceid == -1)
        {
            InsertService();
        }
        else
            UpdateService();
    }

    public void InsertService()
    {

        //int divisionId =Convert.ToInt32( ddlDivision.SelectedValue);
        //string division = ddlDivision.SelectedItem.Text;
        //int subdivisionId =Convert.ToInt32( ddlSubdivision.SelectedValue);
        //int itemId =Convert.ToInt32( ddlItem.SelectedValue);
        int customerId =Convert.ToInt32( ddlCustomer.SelectedValue);
        int serviceContactId = Convert.ToInt32(ddlContractType.SelectedValue);
        int serviceCenterId = Convert.ToInt32(ddlServiceCenter.SelectedValue);
        int higherEducationCessId = Convert.ToInt32(ddlHedCess.SelectedValue);
        DateTime pdate = DateTime.Today;
        if (string.IsNullOrEmpty(poDate.Text))
        {
            poDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
        }
        else
        {
            pdate = DateTime.ParseExact(poDate.Text, "dd/MM/yyyy", null);
        }
        object _objH = Convert.DBNull;

        if (higherEducationCessId != 0)
        {
            _objH = higherEducationCessId;
        }
        int educationCessId = Convert.ToInt32(ddlEdCess.SelectedValue);
        object _objE = Convert.DBNull;

        if (educationCessId != 0)
        {
            _objE = educationCessId;
        }
        int servicetaxId=Convert.ToInt32(ddlServiceTax.SelectedValue);
        object _objS = Convert.DBNull;

        if (servicetaxId != 0)
        {
            _objS = servicetaxId;
        }
        string serialNo = txtSerialNo.Text;
        string customerOrderNo = poReference.Text;
        string serviceDesc = txtDescription.Text;
        decimal price=0;
        price = Convert.ToDecimal(txtServiecPrice.Text);
        //double _serviceTax = double.Parse(ddlServiceTax.Items[ddlServiceTax.SelectedIndex].Text) * .01;
        //double educationCess = double.Parse(ddlEdCess.Items[ddlEdCess.SelectedIndex].Text) * .01 * _serviceTax;
        //double heducationCess = double.Parse(ddlServiceTax.Items[ddlHedCess.SelectedIndex].Value) * .01 * _serviceTax;
        //double _total = Convert.ToDouble(price) + _serviceTax + educationCess + heducationCess;
        //txtTotal.Text = string.Format("{0:N}", _total);
        //lblPrice.Text = changeCurrencyToWords(Convert.ToString(_total));

        string strServiceId = ""; // "Lum/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + serialNo;

        SpParam[] _Sparray = new SpParam[16];

      
        _Sparray[0] = new SpParam("@itemId", 0, SqlDbType.Int);
        _Sparray[1] = new SpParam("@serialNo", serialNo, SqlDbType.VarChar);
        _Sparray[2] = new SpParam("@customerId", customerId, SqlDbType.Int);

        _Sparray[3] = new SpParam("@serviceContractId", serviceContactId, SqlDbType.Int);
        _Sparray[4] = new SpParam("@serviceDesc", serviceDesc, SqlDbType.VarChar);
        _Sparray[5] = new SpParam("@price", price, SqlDbType.Decimal);
        _Sparray[6] = new SpParam("@servicetaxId", _objS, SqlDbType.Int);
        _Sparray[7] = new SpParam("@serviceCenterId", serviceCenterId, SqlDbType.Int);
        _Sparray[8] = new SpParam("@higherEducationCessId", _objH, SqlDbType.Int);
        _Sparray[9] = new SpParam("@educationCessId", _objE, SqlDbType.Int);
        _Sparray[10] = new SpParam("@customerOrderNo", customerOrderNo, SqlDbType.VarChar);
        _Sparray[11] = new SpParam("@strServiceId", strServiceId, SqlDbType.VarChar);
        _Sparray[12] = new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.VarChar);
        _Sparray[13] = new SpParam("@activityDateTime", System.DateTime.Now, SqlDbType.DateTime);
        _Sparray[14] = new SpParam("@date", pdate, SqlDbType.DateTime);
        if (txtvat.Text != "")
        {
            _Sparray[15] = new SpParam("@vat", Convert.ToDecimal(txtvat.Text), SqlDbType.Decimal);
        }
        else
        {
            _Sparray[15] = new SpParam("@vat", 0, SqlDbType.Decimal);
        }


        try
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                using (IDataReader reader = conn.ExecuteQueryProc("Lum_Erp_Stp_AddService", _Sparray))
                {
                    if (reader.Read())
                    {

                        message.Text = "Service has been added. No : " + Convert.ToString(reader["SERVICEID"]);
                        message.ForeColor = Color.Green;
                    }

                }
            }


        }
        catch (Exception ex)
        {
            message.Text = ex.Message;
        }

    }
    public void ResetControl()
    {            
       // ddlDivision.SelectedIndex = 0;
        ddlCustomer.SelectedIndex = 0;
        ddlServiceTax.SelectedIndex = 0;
        ddlServiceCenter.SelectedIndex = 0;
        ddlEdCess.SelectedIndex = 0;
        ddlHedCess.SelectedIndex = 0;
       // ddlSubdivision.Items.Clear();
       // ddlSubdivision.Items.Add("--Select--");
       // ddlSubdivision.Items[0].Value = "0";
        //ddlItem.Items.Clear();
        //ddlItem.Items.Add("--Select--");
        //ddlItem.Items[0].Value = "0";
       // ddlItem.SelectedIndex = 0;
        txtSerialNo.Text = string.Empty;
        txtDescription.Text = string.Empty;
        txtServiecPrice.Text = string.Empty;
        ddlContractType.SelectedIndex = 0;
        poReference.Text = string.Empty;
        txtTotal.Text = string.Empty;
       
    }

    public void UpdateService()
    {
        string servicebill = lbl_strServiceId.Text;
        DateTime pdate = DateTime.Today;
        if (string.IsNullOrEmpty(poDate.Text))
        {
            poDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
        }
        else
        {
            pdate = DateTime.ParseExact(poDate.Text, "dd/MM/yyyy", null);
        }

        int serviceId = int.Parse(Request.Params["serviceId"]); 
        int customerId = Convert.ToInt32(ddlCustomer.SelectedValue);
        int serviceContactId = Convert.ToInt32(ddlContractType.SelectedValue);
        int serviceCenterId = Convert.ToInt32(ddlServiceCenter.SelectedValue);
        int higherEducationCessId = Convert.ToInt32(ddlHedCess.SelectedValue);

        object _objH = Convert.DBNull;

        if (higherEducationCessId != 0)
        {
            _objH = higherEducationCessId;
        }
        int educationCessId = Convert.ToInt32(ddlEdCess.SelectedValue);
        object _objE = Convert.DBNull;

        if (educationCessId != 0)
        {
            _objE = educationCessId;
        }
        int servicetaxId = Convert.ToInt32(ddlServiceTax.SelectedValue);
        object _objS = Convert.DBNull;

        if (servicetaxId != 0)
        {
            _objS = servicetaxId;
        }
        string serialNo = txtSerialNo.Text;
        string customerOrderNo = poReference.Text;
        string serviceDesc = txtDescription.Text;
        decimal price = 0;
        price = Convert.ToDecimal(txtServiecPrice.Text);
        decimal total = 0;
        if (txtTotal.Text.Length > 0)
        {

            total = Convert.ToDecimal(txtTotal.Text);
        }

        string strServiceId = "";// "Lum/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + serialNo;

        SpParam[] _Sparray = new SpParam[17];

     
        _Sparray[0] = new SpParam("@itemId", 0, SqlDbType.Int);
        _Sparray[1] = new SpParam("@serialNo", serialNo, SqlDbType.VarChar);
        _Sparray[2] = new SpParam("@customerId", customerId, SqlDbType.Int);

        _Sparray[3] = new SpParam("@serviceContactId", serviceContactId, SqlDbType.Int);
        _Sparray[4] = new SpParam("@serviceDesc", serviceDesc, SqlDbType.VarChar);
        _Sparray[5] = new SpParam("@price", price,SqlDbType.Decimal);
        _Sparray[6] = new SpParam("@servicetaxId", _objS, SqlDbType.Int);
        _Sparray[7] = new SpParam("@serviceCenterId", serviceCenterId, SqlDbType.Int);
        _Sparray[8] = new SpParam("@higherEducationCessId", _objH, SqlDbType.Int);
        _Sparray[9] = new SpParam("@educationCessId", _objE, SqlDbType.Int);

        //_Sparray[6] = new SpParam("@serviceTaxId", servicetaxId, SqlDbType.Int);
        //_Sparray[7] = new SpParam("@servicecenterId", serviceCenterId, SqlDbType.Int);
        //_Sparray[8] = new SpParam("@highereducationcessId", higherEducationCessId, SqlDbType.Int);
        //_Sparray[9] = new SpParam("@educationcessid", educationCessId, SqlDbType.Int);
        _Sparray[10] = new SpParam("@customerOrderNo", customerOrderNo, SqlDbType.VarChar);
        _Sparray[11] = new SpParam("@serviceId", serviceId, SqlDbType.VarChar);
        _Sparray[12] = new SpParam("@strServiceId", strServiceId, SqlDbType.VarChar);
        _Sparray[13] = new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.VarChar);
        _Sparray[14] = new SpParam("@activityDateTime", System.DateTime.Now, SqlDbType.DateTime);

        _Sparray[15] = new SpParam("@date", pdate, SqlDbType.DateTime);
        _Sparray[16] = new SpParam("@vat", txtvat.Text, SqlDbType.Decimal);
        try
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {


                int _i = conn.ExecuteCommandProc("Lum_Erp_Stp_UpdateServices", _Sparray);

                if (_i >0)
                {


                    message.Text = "Service has been Updated.";
                    message.ForeColor = Color.Green;
                   // ResetControl();

                }

            }
        }
        catch (Exception ex)
        {
            message.Text = ex.Message;
        }

    }

    protected void btnprint_Click(object sender, EventArgs e)
    {
        Response.Redirect("../Reports/ServiceBillPrint.aspx?serviceid=" + serviceid);
    }
    protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            IDataReader _reader = conn.ExecuteQuery(string.Format(@"select segmentType, Billing_address1+' '+Billing_address2  as billingAddress,Shipping_address1+' '+Shipping_address2 as 
                            shippingAddress from Lum_Erp_Customer C , Lum_Erp_CustomerSegment S 
                            WHERE S.segmentId=C.customerSegmentId and customerId = " + Convert.ToInt32(ddlCustomer.SelectedValue)));
            if (_reader.Read())
            {
                billingAddress.Text = Convert.ToString(_reader["billingAddress"]);
                installationAddress.Text = Convert.ToString(_reader["shippingAddress"]);
                CustomerSegment.Text = Convert.ToString(_reader["segmentType"]);
            }
        }
    }
}
