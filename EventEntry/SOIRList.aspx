﻿<%@ Page Title="SOIR List" Language="C#" MasterPageFile="~/EventEntry/EventEntry.master" AutoEventWireup="true" CodeFile="SOIRList.aspx.cs" Inherits="EventEntry_SOIRList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="PageMargin">
 <asp:Button ID="newSoir" runat="server" Text="Create New"  
                Width="138px" onclick="newSoir_Click"  CssClass="ButtonBig" />    
                  <div class="Line"></div>
 <div class="PageHeading">Existing SOIR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     <asp:Label ID="message" runat="server" />
 <asp:CheckBox ID="Chkblock_despath" runat="server" Text="Show Blocked" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btngo" runat="server" Text="GO" CssClass="ButtonBig" onclick="btngo_Click"  />         
    </div>
          <div class="Line"></div>
   
 
      
        
       
       
        
         
               <asp:DataGrid ID="soirGrid" runat="server" AutoGenerateColumns="false" DataKeyField="soirId"
            CellPadding="0" CellSpacing="0" OnItemCommand="soirGrid_ItemCommand" HeaderStyle-CssClass="GridHeading"
            ItemStyle-CssClass="GridData" GridLines="None" 
            onitemcreated="soirGrid_ItemCreated"  Width="100%" 
            onprerender="soirGrid_PreRender" >
          
            <Columns>
                <asp:BoundColumn DataField="strSoirId" HeaderStyle-HorizontalAlign="Left" HeaderText="SOIR" HeaderStyle-Font-Bold="true" />
                <asp:BoundColumn DataField="SOIRDT" HeaderStyle-HorizontalAlign="Left" HeaderText="SOIR Dt" HeaderStyle-Font-Bold="true" />

                <asp:BoundColumn DataField="raisedBy" HeaderStyle-HorizontalAlign="Left" HeaderText="raised By" HeaderStyle-Font-Bold="true" />
                
                <asp:BoundColumn DataField="customerName" HeaderStyle-HorizontalAlign="Left" HeaderText="Customer Name"  HeaderStyle-Font-Bold="true" />
                  <asp:ButtonColumn ButtonType="LinkButton" HeaderStyle-HorizontalAlign="Left" CommandName="View" Text="View" ItemStyle-CssClass="GridLink" />
                <asp:ButtonColumn ButtonType="LinkButton" HeaderStyle-HorizontalAlign="Left" CommandName="Edit" Text="Edit" ItemStyle-CssClass="GridLink" />
                <asp:ButtonColumn ButtonType="LinkButton" HeaderStyle-HorizontalAlign="Left" CommandName="Delete" Text="Block" ItemStyle-CssClass="GridLink" />
                 <asp:TemplateColumn Visible="false">
              <ItemTemplate>
                <input type="hidden" id="isLocked" value='<%#DataBinder.Eval(Container.DataItem, "isLocked") %>' runat="server" />
            </ItemTemplate>
            </asp:TemplateColumn>
            
                 <asp:TemplateColumn Visible="false">
              <ItemTemplate>
                <input type="hidden" id="isDeleted" value='<%#DataBinder.Eval(Container.DataItem, "isDeleted") %>' runat="server" />
            </ItemTemplate>
            </asp:TemplateColumn>            
            </Columns>
        </asp:DataGrid>
       
        <!--Content Table End-->
        <div class="Line">
        </div>
        <!--Grid Table Start-->
     
        <!--Grid Table End-->
        <div class="Line">
        </div>
        <!--Page Body End-->
    </div>
</asp:Content>

