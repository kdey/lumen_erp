<%@ Page Language="C#"  AutoEventWireup="true" CodeFile="CSABill.aspx.cs" Inherits="EventEntry_CSABill" Title="Lumen Tele Systems .....Erp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Lumen Tele System Pvt. Ltd.</title>
    <link href="./../style/style.css" rel="stylesheet" type="text/css" />
    
 <script type ="text/javascript">
     function print_page() {

         var control = document.getElementById("btnPrint");
         if (control.style.visibility == "visible" || control.style.visibility == "")
             control.style.visibility = "hidden";

         var control1 = document.getElementById("btnCancel");
         if (control1.style.visibility == "visible" || control1.style.visibility == "")
             control1.style.visibility = "hidden";
         // else control.style.visibility = "visible";

         /*    var OLECMDID = 7;
         /* OLECMDID values:
         * 6 - print
         * 7 - print preview
         * 1 - open window
         * 4 - Save As
         
         var PROMPT = 1; // 2 DONTPROMPTUSER 
         var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
         document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
         WebBrowser1.ExecWB(OLECMDID, PROMPT);
         WebBrowser1.outerHTML = "";*/
         if (navigator.appName == "Microsoft Internet Explorer") {
         var OLECMDID = 7;
         /* OLECMDID values:
         * 6 - print
         * 7 - print preview
         * 1 - open window
         * 4 - Save As
         */
         var PROMPT = 1; // 2 DONTPROMPTUSER
         var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
         document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
         WebBrowser1.ExecWB(OLECMDID, PROMPT);
         WebBrowser1.outerHTML = "";
         }
         else {
             javascript: window.print();
         }
     }
     function viewControl() {
         var control = document.getElementById("btnPrint");
         control.style.visibility = "visible";
         var control1 = document.getElementById("btnCancel");
         control1.style.visibility = "visible";
     }
 </script>
 </head>
<body>
    <form id="form1" runat="server">
    <div class="Header">
        <div class="HeaderImg">
        </div>
    </div>
<div class="PageContent">
<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="NormalTextBlack" >
        <tr><td colspan="4" align="left" style="font-size:12px"><b>Contact Name & Address</b></td></tr>
        <tr><td colspan="2" rowspan="3" align="left" valign="top"><strong><asp:Label ID="lblName" runat="server" Font-Size="14px"></asp:Label></strong><br /><br />
                <asp:Label ID="lblAddress" runat="server"></asp:Label><br />
            <br />
            Installation Address:<br /><asp:Label ID="lblPh" runat="server"></asp:Label><br /><br />
                <asp:Label ID="lblMob" runat="server"></asp:Label></td>
            <td width="15%" align="left" valign="top">Inv No :</td>
            <td width="35%" align="left" valign="top"><asp:Label ID="lblInvNo" runat="server"></asp:Label></td>
        </tr>
        <tr><td width="15%" align="left" valign="top">Invoice Date :</td>
            <td width="35%" align="left" valign="top"><asp:Label ID="lblInvDate" runat="server"></asp:Label></td>
        </tr>
        <tr><td width="15%" align="left" valign="top">Model :</td>
            <td width="35%" align="left" valign="top"><asp:Label ID="lblModel" runat="server"></asp:Label></td>
        </tr>
        <tr><td width="20%" align="left" valign="top">Contact Person :</td>
            <td width="30%" align="left" valign="top"><asp:Label ID="lblContactPerson" runat="server"></asp:Label></td>
            <td width="15%" align="left" valign="top">M/C Sr. No. :</td>
            <td width="35%" align="left" valign="top"><asp:Label ID="lblMCSrlNo" runat="server"></asp:Label></td>
        </tr>
        <tr><td colspan="4" align="left"><b>LCSA INVOICE</b></td></tr>
        <tr>
            <td colspan="4" align="left">
                <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2" class="NormalTextBlack" >
                    <tr>
                        <td width="22%" align="left" valign="top">&nbsp;</td>
                        <td width="18%" align="left" valign="top">Date</td>
                        <td width="23%" align="left" valign="top">Meter Reading</td>
                        <td width="22%" align="left" valign="top">Agreement No.</td>
                        <td width="15%" align="left" valign="top"><asp:Label ID="lblAgreementNo" runat="server" Text=""></asp:Label></td>
                    </tr>
                    <tr>
                        <td width="22%" align="left" valign="top">Current Reading</td>
                        <td width="18%" align="left" valign="top">
                            <asp:TextBox ID="txtlCurrentReadingDate" runat="server" ReadOnly="true" CssClass="TextboxSmall"></asp:TextBox>
                            <%--<cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd MMM yyyy" TargetControlID="txtlCurrentReadingDate">
                            </cc1:CalendarExtender>--%>
                        </td>
                        <td width="23%" align="left" valign="top"> 
                            <asp:TextBox ID="txtCurrentReading" runat="server" ReadOnly="true" AutoPostBack="true" CssClass="TextboxSmall"
                                OnTextChanged="txtAgreedCopyRate_TextChanged"></asp:TextBox><cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtCurrentReading" ValidChars="1234567890." FilterMode="ValidChars" FilterType="Custom">
                                </cc1:FilteredTextBoxExtender>
                        </td>
                        <td width="22%" align="left" valign="top">Agreed Copy<br />Rate (Rs.)</td>
                        <td width="15%" align="left" valign="top">
                        <asp:TextBox ID="txtAgreedCopyRate" 
                                runat="server" CssClass="TextboxSmall" AutoPostBack="true" 
                                ontextchanged="txtAgreedCopyRate_TextChanged"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtAgreedCopyRate" ValidChars="1234567890." FilterMode="ValidChars" FilterType="Custom"></cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td width="22%" align="left" valign="top">Previous Reading</td>
                        <td width="18%" align="left" valign="top"><asp:TextBox ID="lblPreviousReadingDate" runat="server" ReadOnly="true" CssClass="TextboxSmall"></asp:TextBox></td>
                        <td width="23%" align="left" valign="top"><asp:TextBox ID="lblPreviousReading" runat="server" ReadOnly="true" CssClass="TextboxSmall"></asp:TextBox></td>
                        <td width="22%" align="left" valign="top">Agreed Min.<br />Monthly Copies</td>
                        <td width="15%" align="left" valign="top">
                            <asp:TextBox ID="txtAgreedMinMonthlyCopies" runat="server" 
                                CssClass="TextboxSmall" AutoPostBack="true" ontextchanged="txtAgreedMinMonthlyCopies_TextChanged"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtAgreedMinMonthlyCopies" ValidChars="1234567890" FilterMode="ValidChars" FilterType="Custom"></cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left" valign="top">Free Copies</td>
                        <td width="80%" align="left" valign="top" colspan="4">
                            <asp:TextBox ID="txtFreeCopies" runat="server" CssClass="TextboxSmall" AutoPostBack="true" 
                                ontextchanged="txtFreeCopies_TextChanged"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtFreeCopies" ValidChars="1234567890" FilterMode="ValidChars" FilterType="Custom"></cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left" valign="top">Gross Copies</td>
                        <td width="20%" align="left" valign="top" colspan="2">
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" RenderMode="Inline" 
                                    UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:TextBox ID="lblGrossCopies" runat="server" ReadOnly="true" CssClass="TextboxSmall"></asp:TextBox>
                                    </ContentTemplate> 
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtFreeCopies" EventName="TextChanged" />
                                    </Triggers>
                             </asp:UpdatePanel>
                        </td>
                        <td width="20%" align="left" valign="top">Net Billable<br />Charges</td>
                        <td width="20%" align="left" valign="top">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" RenderMode="Inline" 
                                    UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:TextBox ID="lblNetBillableCharges" runat="server" ReadOnly="true" AutoPostBack="true" CssClass="TextboxSmall"></asp:TextBox>
<%--                                        <asp:Label ID="lblNetBillableCharges" runat="server" CssClass="TextboxSmall"></asp:Label>
--%>                                        <br />
                                    </ContentTemplate> 
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtAgreedCopyRate" EventName="TextChanged">
                                        </asp:AsyncPostBackTrigger>
                                        <asp:AsyncPostBackTrigger ControlID="txtAgreedMinMonthlyCopies" EventName="TextChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="txtFreeCopies" EventName="TextChanged" />
                                    </Triggers>
                             </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left" valign="top">Less Service<br />Copies @1%</td>
                        <td width="20%" align="left" valign="top" colspan="2">
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server" RenderMode="Inline" 
                                    UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:TextBox ID="lblServiceCopies" runat="server" ReadOnly="true" CssClass="TextboxSmall"></asp:TextBox>
                                    </ContentTemplate> 
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtFreeCopies" EventName="TextChanged" />
                                    </Triggers>
                             </asp:UpdatePanel>
                        </td>
                        <td width="20%" align="left" valign="top">Minimum<br />Billing Charges</td>
                        <td width="20%" align="left" valign="top">
                             <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline" 
                                    UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtMinBillingCharges" runat="server" ReadOnly="true" AutoPostBack="true" CssClass="TextboxSmall"></asp:TextBox>                                            
<%--                                        <asp:Label ID="lblMinBillingCharges" runat="server" CssClass="TextboxSmall"></asp:Label>
--%>                                    </ContentTemplate> 
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtAgreedCopyRate" EventName="TextChanged">
                                        </asp:AsyncPostBackTrigger>
                                        <asp:AsyncPostBackTrigger ControlID="txtAgreedMinMonthlyCopies" EventName="TextChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="txtFreeCopies" EventName="TextChanged" />
                                    </Triggers>
                             </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left" valign="top">Net Billable<br />Copies</td>
                        <td width="20%" align="left" valign="top" colspan="2">
                        <asp:UpdatePanel ID="UpdatePanel6" runat="server" RenderMode="Inline" 
                                    UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:TextBox ID="lblNetBillableCopies" runat="server" ReadOnly="true" CssClass="TextboxSmall"></asp:TextBox>
                                    </ContentTemplate> 
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtFreeCopies" EventName="TextChanged" />
                                    </Triggers>
                             </asp:UpdatePanel>
                        </td>
                        <td width="20%" align="left" valign="top"><strong>Billable<br />Amount Rs.</strong></td>
                        <td width="20%" align="left" valign="top">
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" RenderMode="Inline" 
                                    UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:TextBox ID="lblBillableAmount" runat="server" ReadOnly="true" CssClass="TextboxSmall"></asp:TextBox>
                                    </ContentTemplate> 
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtAgreedCopyRate" EventName="TextChanged">
                                        </asp:AsyncPostBackTrigger>
                                        <asp:AsyncPostBackTrigger ControlID="txtAgreedMinMonthlyCopies" EventName="TextChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="txtFreeCopies" EventName="TextChanged" />
                                    </Triggers>
                             </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="left">
                <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2" class="NormalTextBlack" >
                    <tr>
                        <td width="20%" align="left" valign="top">APPLICABLE TAXES</td>
                        <td width="60%" align="left" valign="top">VAT @ 4.00% on 80% of Billable Amount</td>
                        <td width="20%" align="left" valign="top">
                        <asp:UpdatePanel ID="UpdatePanel7" runat="server" RenderMode="Inline" 
                                    UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:TextBox ID="lblVat" runat="server" ReadOnly="true" CssClass="TextboxSmall"></asp:TextBox>
                                    </ContentTemplate> 
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtAgreedCopyRate" EventName="TextChanged">
                                        </asp:AsyncPostBackTrigger>
                                        <asp:AsyncPostBackTrigger ControlID="txtAgreedMinMonthlyCopies" EventName="TextChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="txtFreeCopies" EventName="TextChanged" />
                                    </Triggers>
                             </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left" valign="top"></td>
                        <td width="60%" align="left" valign="top">SERVICE TAX @ 
                            <asp:Label ID="lblCurrentServiceTax" runat="server"></asp:Label>  on 100% of Billable Amount</td>
                        <td width="20%" align="left" valign="top">
                        <asp:UpdatePanel ID="UpdatePanel8" runat="server" RenderMode="Inline" 
                                    UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:TextBox ID="lblServiceTax" runat="server" ReadOnly="true" CssClass="TextboxSmall"></asp:TextBox>
                                    </ContentTemplate> 
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtAgreedCopyRate" EventName="TextChanged">
                                        </asp:AsyncPostBackTrigger>
                                        <asp:AsyncPostBackTrigger ControlID="txtAgreedMinMonthlyCopies" EventName="TextChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="txtFreeCopies" EventName="TextChanged" />
                                    </Triggers>
                             </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left" valign="top"></td>
                        <td width="60%" align="left" valign="top">Educational CESS @ 3.00% on Service Tax</td>
                        <td width="20%" align="left" valign="top">
                        <asp:UpdatePanel ID="UpdatePanel9" runat="server" RenderMode="Inline" 
                                    UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:TextBox ID="lblEducationalCess" runat="server" ReadOnly="true" CssClass="TextboxSmall"></asp:TextBox>
                                    </ContentTemplate> 
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtAgreedCopyRate" EventName="TextChanged">
                                        </asp:AsyncPostBackTrigger>
                                        <asp:AsyncPostBackTrigger ControlID="txtAgreedMinMonthlyCopies" EventName="TextChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="txtFreeCopies" EventName="TextChanged" />
                                    </Triggers>
                             </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left" valign="top"></td>
                        <td width="60%" align="left" valign="top"><br />Invoice Amount : Rs.</td>
                        <td width="20%" align="left" valign="top"><br />
                        <asp:UpdatePanel ID="UpdatePanel10" runat="server" RenderMode="Inline" 
                                    UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:TextBox ID="lblInvoiceAmount" runat="server" ReadOnly="true" CssClass="TextboxSmall"></asp:TextBox>
                                    </ContentTemplate> 
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtAgreedCopyRate" EventName="TextChanged">
                                        </asp:AsyncPostBackTrigger>
                                        <asp:AsyncPostBackTrigger ControlID="txtAgreedMinMonthlyCopies" EventName="TextChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="txtFreeCopies" EventName="TextChanged" />
                                    </Triggers>
                             </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td width="20%" align="left" valign="top"></td>
                        <td width="60%" align="left" valign="top">Net Payable Amount(Round Off)</td>
                        <td width="20%" align="left" valign="top">
                        <asp:UpdatePanel ID="UpdatePanel11" runat="server" RenderMode="Inline" 
                                    UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:TextBox ID="lblNetPayable" runat="server" ReadOnly="true" CssClass="TextboxSmall"></asp:TextBox>
                                    </ContentTemplate> 
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtAgreedCopyRate" EventName="TextChanged">
                                        </asp:AsyncPostBackTrigger>
                                        <asp:AsyncPostBackTrigger ControlID="txtAgreedMinMonthlyCopies" EventName="TextChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="txtFreeCopies" EventName="TextChanged" />
                                    </Triggers>
                             </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="NormalTextBlack">
                <td colspan="4" align="center">
                    <input  id="btnCancel" value="Cancel" type="button" runat="server"
                            style="width: 141px; cursor: hand;" onserverclick="btnCancel_ServerClick" />
                                <input  id="btnPrint" value="Print" type="button"
                            onclick="print_page();viewControl();javascript:window.print();" 
                            style="width: 141px; cursor: hand;" />
                </td>
        </tr>
        <tr>
            <td align="left" colspan="4">
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="NormalTextBlack" >
                    <tr>
                        <td width="20%" align="left" valign="top">Amount in Words :</td>
                        <td width="80%" align="left" valign="top" colspan="3">
                        <asp:UpdatePanel ID="UpdatePanel12" runat="server" RenderMode="Inline" 
                                    UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Label ID="lblAmountInWords" runat="server"></asp:Label>
                                    </ContentTemplate> 
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtAgreedCopyRate" EventName="TextChanged">
                                        </asp:AsyncPostBackTrigger>
                                        <asp:AsyncPostBackTrigger ControlID="txtAgreedMinMonthlyCopies" EventName="TextChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="txtFreeCopies" EventName="TextChanged" />
                                    </Triggers>
                             </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="4">
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="NormalTextBlack" >
                    <tr style="height:20px">
                        <td align="left" valign="top">Authorized Signature :</td><br />
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="NormalTextBlack">
            <td align="left" style="width:23%"><strong>TIN (W.B.) :</strong></td>
            <td align="left" style="width:77%"><strong><asp:Label ID="lblTinNo" runat="server" Text=""></asp:Label></strong></td>
         </tr>
         <tr class="NormalTextBlack">
            <td align="left" style="width:23%"><strong>CST No. :</strong></td>
            <td align="left" style="width:77%"><strong><asp:Label ID="lblCSTNo" runat="server" Text=""></asp:Label></strong></td>
         </tr>
         <tr class="NormalTextBlack">
            <td align="left" style="width:35%"><strong>Service Tax Reg. No.</strong></td>
            <td align="left" style="width:65%"><strong><asp:Label ID="lblServiceTaxRegistNo" runat="server" Text=""></asp:Label></strong></td>
        </tr>
    </table>
</div>
</form>
</body>
</html>

