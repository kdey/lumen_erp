﻿<%@ Page Title="Lumen Tele System Pvt.Ltd." Language="C#" MasterPageFile="~/EventEntry/EventEntry.master" AutoEventWireup="true" CodeFile="ContractDetails.aspx.cs" Inherits="EventEntry_ContractDetails" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript">
    function isNumberKey(evt)   //Checks textbox contains only decimal value
    {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode != 46)) {
            return false;
        }
        return true;
    }
    function isNumberKey_WO_Decimal(evt)   //Checks textbox contains only numeric value
    {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
</script>
<div class="PageMargin">
    <div class="PageHeading">Customer Machine </div>
    <div class="Line"><asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager></div>
    <!--Content Table Start-->
    <div class="PageContent">
        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
        <tr><td align="right" valign="top" width="20%">Lookup Machine by Serial No&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td align="left" valign="top" width="15%"><asp:TextBox ID="txtSRNo" runat="server" 
                    CssClass="TextboxSmall" MaxLength="50" /></td>
            <td align="left" valign="top" width="15%"><asp:Button ID="lookup" runat="server" Text="Lookup" OnClick="lookup_Click"
                 CssClass="Button" CausesValidation="false" /></td>
            <td colspan="3" align="left" valign="top" width="50%"><input type="hidden" id="CustMacId" runat="server" />
                <asp:Label ID="lookupMessage" runat="server" /></td>
        </tr></table>
        <asp:Panel ID="Panel1" runat="server" Visible="false">
            <hr />
            <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
            <tr><td align="right" valign="top" width="15%"><b>Customer Name :</b></td>
                <td align="left" valign="top" width="35%"><asp:Label ID="customerName" runat="server" /></td>
                <td align="right" valign="top" width="15%"><b>Machine Model :</b></td>
                <td align="left" valign="top" width="35%"><asp:Label ID="machineModel" runat="server" /></td>
            </tr>
            <tr><td align="right" valign="top" width="15%"><b>Installation Address :</b></td>
                <td align="left" valign="top" width="35%"><asp:Label ID="iAddress" runat="server" /></td>
                <td align="right" valign="top" width="15%"><b>Billing Address :</b></td>
                <td align="left" valign="top" width="35%"><asp:Label ID="bAddress" runat="server" /></td>
            </tr>
            </table>
            <hr />
            <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
            <tr><td align="right" valign="top" width="10%" class="PageHeading">Contract Details</td>
                <td align="left" valign="top"><asp:Label ID="contractLookupMessage" runat="server" /></td>
            </tr></table>
            <hr />
            <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
            <tr><td align="right" valign="top"><b>Contract Type&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></b></td>
                <td align="left" valign="top"><asp:DropDownList ID="ddlConType" runat="server" CssClass="DropdownSmall" /><br />
                    <asp:RequiredFieldValidator ControlToValidate="ddlConType" ErrorMessage="Select Contract Type" ID="CustomerValidator"
                     InitialValue="0" runat="server" /></td>
                <td align="right" valign="top"><b>Current Meter Reading&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></b></td>
                <td align="left" valign="top">
                    <asp:TextBox ID="txtCurMeterRead" runat="server" onkeypress="return isNumberKey_WO_Decimal(event);"
                     CssClass="TextboxSmall" MaxLength="12" /><br />
                    <asp:RequiredFieldValidator ControlToValidate="txtCurMeterRead" ErrorMessage="Field cannot be empty" ID="CurMeterReadValidator"
                     runat="server" /></td>
                <td align="right" valign="top"><b>Current Meter Reading Date&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span> <br />
                    (dd MMM yyyy)</b></td>
                <td align="left" valign="top"><asp:TextBox ID="txtCMRDate" onkeyup="document.getElementById(this.id).value=''" autocomplete='off'
                     CssClass="TextboxSmall" runat="server" />
                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd MMM yyyy" TargetControlID="txtCMRDate"></cc1:CalendarExtender>
                    <br /><asp:RequiredFieldValidator ControlToValidate="txtCMRDate" ErrorMessage="Field cannot be empty"
                     ID="CMRDateValidator" runat="server" /></td>
            </tr>
            <tr><td align="right" valign="top"><b>Contract Number&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></b></td>
                <td align="left" valign="top"><asp:TextBox ID="txtConNo" runat="server" 
                        CssClass="TextboxSmall" MaxLength="50"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ControlToValidate="txtConNo" ErrorMessage="Field cannot be empty" ID="ConNoValidator"
                     runat="server" /></td>
                <td align="right" valign="top"><b>Contract Start Date&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span> <br />
                    (dd MMM yyyy)</b></td>
                <td align="left" valign="top"><asp:TextBox ID="txtCSDate" onkeyup="document.getElementById(this.id).value=''" autocomplete='off'
                     runat="server" CssClass="TextboxSmall" />
                    <cc1:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd MMM yyyy" TargetControlID="txtCSDate"></cc1:CalendarExtender>
                    <br /><asp:RequiredFieldValidator ControlToValidate="txtCSDate" ErrorMessage="Field cannot be empty"
                     ID="CSDateValidator" runat="server" /></td>
                <td align="right" valign="top"><b>Meter Reading on Contract Start Date &nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></b></td>
                <td align="left" valign="top">
                    <asp:TextBox ID="txtMeterReadCSD" runat="server" onkeypress="return isNumberKey_WO_Decimal(event);"
                     CssClass="TextboxSmall" MaxLength="12"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ControlToValidate="txtMeterReadCSD" ErrorMessage="Field cannot be empty" ID="MeterReadCSDValidator"
                     runat="server" /></td>
            </tr>
            <tr><td align="right" valign="top"><b>Contract End Date&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span> <br />(dd/mm/yyyy)</b></td>
                <td align="left" valign="top"><asp:TextBox ID="txtCEDate" onkeyup="document.getElementById(this.id).value=''" autocomplete='off'
                     runat="server" CssClass="TextboxSmall" />
                    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd MMM yyyy" TargetControlID="txtCEDate"></cc1:CalendarExtender><br />
                    <asp:RequiredFieldValidator ControlToValidate="txtCEDate" ErrorMessage="Field cannot be empty"
                     ID="CEDateValidator" runat="server" /></td>
                <td align="right" valign="top"><b>Life in Copies&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></b></td>
                <td align="left" valign="top">
                    <asp:TextBox ID="txtLifeCopy" runat="server" onkeypress="return isNumberKey_WO_Decimal(event);"
                     CssClass="TextboxSmall" MaxLength="7"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ControlToValidate="txtLifeCopy" ErrorMessage="Field cannot be empty" ID="LifeCopyValidator"
                     runat="server" /></td>
                <td align="right" valign="top"><b>Number of Free Copies&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></b></td>
                <td align="left" valign="top">
                    <asp:TextBox ID="txtFreeCopy" runat="server" onkeypress="return isNumberKey_WO_Decimal(event);"
                     CssClass="TextboxSmall" MaxLength="7"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ControlToValidate="txtFreeCopy" ErrorMessage="Field cannot be empty" ID="FreeCopyValidator"
                     runat="server" /></td>
            </tr>
            <tr><td align="right" valign="top"><b>Service Copies&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></b></td>
                <td align="left" valign="top">
                    <asp:TextBox ID="txtServiceCopy" runat="server" onkeypress="return isNumberKey_WO_Decimal(event);"
                     CssClass="TextboxSmall" MaxLength="3"></asp:TextBox>&nbsp;&nbsp;<b>%</b><br />
                    <asp:RequiredFieldValidator ControlToValidate="txtServiceCopy" ErrorMessage="Field cannot be empty" ID="ServiceCopyValidator"
                     runat="server" /><br />
                    <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtServiceCopy"
                     ErrorMessage="Cannot be Greater than 100" MaximumValue="100" MinimumValue="1" Type="Integer"></asp:RangeValidator></td>
                <td align="right" valign="top"><b>Agreed Copy Rate&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></b></td>
                <td align="left" valign="top">
                    <asp:TextBox ID="txtCopyRate" runat="server" onkeypress="return isNumberKey(event);"
                     CssClass="TextboxSmall" MaxLength="7"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ControlToValidate="txtCopyRate" ErrorMessage="Field cannot be empty" ID="CopyRateValidator"
                     runat="server" /><br />
                    <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtCopyRate"
                     ErrorMessage="Must be Less Than 10000 and Greater than 0.00" MaximumValue="9999.99" MinimumValue="0.01" 
                     Type="Double"></asp:RangeValidator></td>
                <td align="right" valign="top"><b>Agreed Minimum Copies&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></b></td>
                <td align="left" valign="top">
                    <asp:TextBox ID="txtMinCopy" runat="server" onkeypress="return isNumberKey_WO_Decimal(event);"
                     CssClass="TextboxSmall" MaxLength="7"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ControlToValidate="txtMinCopy" ErrorMessage="Field cannot be empty" ID="MinCopyValidator"
                     runat="server" /></td>
            </tr>
            <tr><td align="center" valign="middle" colspan="6">
                    <asp:Button ID="Submit" Text="Submit" runat="server" OnClick="add_Click" CssClass="Button" />&nbsp;&nbsp;
                    <asp:Button ID="cancel" Text="Cancel" runat="server" OnClick="cancel_Click" CausesValidation="false" CssClass="Button" /></td>
            </tr>
            <tr><td align="left" valign="middle" colspan="6"><input type="hidden" id="ConDetailId" runat="server" />
                    <asp:Label ID="message" runat="server" /></td>
            </tr></table>


                        
        </asp:Panel>
            <hr />        
        <asp:Panel runat=server>
            <asp:GridView ID="gvCSA" runat="server" CellPadding="0"
             HeaderStyle-CssClass="GridHeading" GridLines="None" Width="100%" 
                RowStyle-CssClass="GridData">
                <RowStyle CssClass="GridData" />
                <HeaderStyle CssClass="GridHeading"></HeaderStyle>
            </asp:GridView>        
        </asp:Panel>
    </div>
    <!--Content Table End-->
    <div class="Line"></div>
    <!--Page Body End-->
</div>
</asp:Content>

