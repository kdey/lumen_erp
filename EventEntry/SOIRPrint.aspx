﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SOIRPrint.aspx.cs" Inherits="EventEntry_SOIRPrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Lumen Tele System Pvt. Ltd.</title>
    <link href="./../style/style.css" rel="stylesheet" type="text/css" />
 <script type ="text/javascript">
       function print_page() {

            var control = document.getElementById("btnPrint");
           
                control.style.visibility = "hidden";

            var control1 = document.getElementById("btnCancel");
            
                control1.style.visibility = "hidden";
            // else control.style.visibility = "visible";

            /*    var OLECMDID = 7;
            /* OLECMDID values:
            * 6 - print
            * 7 - print preview
            * 1 - open window
            * 4 - Save As
         
         var PROMPT = 1; // 2 DONTPROMPTUSER 
            var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
            document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
            WebBrowser1.ExecWB(OLECMDID, PROMPT);
            WebBrowser1.outerHTML = "";*/
          //  var OLECMDID = 7;
            /* OLECMDID values:
            * 6 - print
            * 7 - print preview
            * 1 - open window
            * 4 - Save As
            */
           // var PROMPT = 1; // 2 DONTPROMPTUSER
          //  var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
           // document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
            //WebBrowser1.ExecWB(OLECMDID, PROMPT);
            //WebBrowser1.outerHTML = "";
        }
        function viewControl() {
            var control = document.getElementById("btnPrint");
            control.style.visibility = "visible";
            var control1 = document.getElementById("btnCancel");
            control1.style.visibility = "visible";
			}
    </script>
    <style type="text/css">
        .style2
        {
            font-size: small;
        }
        .style3
        {
            height: 16px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="Header">
        <div class="HeaderImg">
        </div>
    </div>
        <div class="PageContent">
    <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalTextBlack" >
        <tr>
            <td colspan="4" style="font-size:large"  >
                Lumen Tele-Systems Pvt. Ltd.</td>
        </tr>
        <tr>
            <td colspan="4" class="style2"  >
                Sales Oreder and Installation Request Form</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="left" >&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td align="right">&nbsp;</td>
        </tr>
    </table>
     <table class="NormalTextBlack" width="100%" border="0px" style="border-color:Black;">
     <tr>
     <td align="left">Date Of SOIR :&nbsp;
         <asp:Label ID="soirdt" runat="server"></asp:Label>
         </td>
       <td></td>
        <td>Challan No.:<br />
            (To be filled in by Accounts)</td>
         <td>
             <asp:TextBox ID="txtChallanNo" runat="server" CssClass="TextboxLarge"></asp:TextBox>
         </td>
     </tr>
      <tr>
            <td align="left" >
                SOIR No.:
              <asp:Label ID="soirNumber" runat="server" />
            </td>
            <td >&nbsp;
                    </td>
            <td align="center">
                         Invoice No.:<br />
                         (To be filled in by Accounts)<br />
            </td>
            <td>
             <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="TextboxLarge"></asp:TextBox>
            </td>
        </tr>
      <tr>
            <td align="left" >
                <b>SOIR raised by:&nbsp; 
                    <asp:Label ID="raisedBy" runat="server" />
                </b>
            </td>
            <td>&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
            <td align="left">&nbsp;</td>
            <td>&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      <tr>
            <td align="left">&nbsp;</td>
            <td>&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
 </table>
 <table class="NormalTextBlack" width="100%">
     <tr>
     <td class="style3">&nbsp;</td><td class="style3">&nbsp;</td><td class="style3">&nbsp;</td>
         <td class="style3">&nbsp;</td>
     </tr>
     <tr>
     <td class="style3">Customer Name &amp; Address:</td><td class="style3">
         <asp:Label ID="CustomerNameAdrress" runat="server"></asp:Label>
         </td><td class="style3">Cosignee Name &amp; Address:</td><td class="style3">
         <asp:Label ID="CosigneeNameAddress" runat="server"></asp:Label>
         </td>
     </tr>
     <tr>
        <td>Contact Person with Ph. No.</td><td>
           <asp:Label ID="ContactPersonWithPhoneNo" runat="server"></asp:Label>
           </td><td>Contact Person with Ph. No.</td><td>
           <asp:Label ID="ContactPersonwithPhCosignee" runat="server"></asp:Label>
           </td>
     </tr>
     <tr>
        <td>Customer CST No.:</td><td>
           <asp:Label ID="cst" runat="server"></asp:Label>
           </td><td>&nbsp;</td><td>&nbsp;</td>
     </tr>
     <tr>
     <td>Customer VAT No.:</td><td>
           <asp:Label ID="vat" runat="server"></asp:Label>
           </td><td>&nbsp;</td><td>&nbsp;</td>
     </tr>
     <tr>
     <td>&nbsp;</td><td>&nbsp;
           </td><td>&nbsp;</td><td>&nbsp;</td>
     </tr>
     <tr>
     <td>&nbsp;</td><td>&nbsp;
           </td><td>&nbsp;</td><td>&nbsp;</td>
     </tr>
 </table>
 <table width="100%" class="NormalTextBlack">
   <tr>
     <td colspan="4">
      <asp:DataGrid ID="poLineGrid" runat="server" AutoGenerateColumns="False"
        DataKeyField="soirLineId"  
              Width="100%" 
            onitemdatabound="poLineGrid_ItemDataBound" onitemcreated="poLineGrid_ItemCreated">
          <ItemStyle HorizontalAlign="Center" />
        <Columns>
            <asp:BoundColumn DataField="itemCode" HeaderText="Model" 
                 HeaderStyle-Font-Bold="true" >
            </asp:BoundColumn>
              <asp:TemplateColumn HeaderText="Quantity" HeaderStyle-Font-Bold="true" >
                <ItemTemplate>
                <input type="hidden" id="lineId" value='<%# DataBinder.Eval(Container.DataItem, "soirLineId") %>' runat="server"  />
                <input type="hidden" id="itemId" value='<%# DataBinder.Eval(Container.DataItem, "itemId") %>' runat="server"  />
                    <asp:Label ID="itemQty" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "quantity") %>' />
                    <%#DataBinder.Eval(Container.DataItem, "uomName")%>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Basic Price" HeaderStyle-Font-Bold="true" >
                <ItemTemplate>
                    <asp:Label ID="itemPrice" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "price") %>' Width="60" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn HeaderText="Freight/Insurance" DataField="packingCharges"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Discount(if any)" DataField="discount"></asp:BoundColumn>
<asp:BoundColumn HeaderText="Buy Back Amount" DataField="buyBackAmount"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Buy Back Details" DataField="buyBackDetails"></asp:BoundColumn>
             <asp:BoundColumn DataField="total"   HeaderText="Total" 
                HeaderStyle-Font-Bold="true"  
                DataFormatString="{0:#,###.##}">
            </asp:BoundColumn>
        </Columns>
          <HeaderStyle HorizontalAlign="Center" />
    </asp:DataGrid>
     </td>
     </tr>
          <tr>
          <td colspan="4">&nbsp;</td>
          </tr>  
          <tr>
          <td colspan="4">Special Instructions</td>
          </tr>  
          <tr>
          <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
          </tr>  
          <tr>
          <td>Delivery on</td><td>
              <asp:Label ID="deliverydate" runat="server"></asp:Label>
              </td><td>Payment Terms</td><td>
              <asp:Label ID="paymentterms" runat="server"></asp:Label>
              </td>
          </tr>  
          <tr>
          <td>Installation Date Committed</td><td>
              <asp:Label ID="installationdate" runat="server"></asp:Label>
              </td><td>Warranty</td><td>
              <asp:Label ID="warranty" runat="server"></asp:Label>
              </td>
          </tr>  
          <tr>
          <td>Free Toner</td><td>&nbsp;</td><td>Free Copies</td><td>
              <asp:Label ID="FreeCopies" runat="server" Visible="False"></asp:Label>
              </td>
          </tr>  
          <tr>
          <td>After Sales Service</td><td>
              <asp:Label ID="afterSalesService" runat="server"></asp:Label>
              </td><td>&nbsp;</td><td>&nbsp;</td>
          </tr>  
          <tr>
          <td>Rate of AMC</td><td>&nbsp;</td><td>
              <asp:RadioButtonList ID="RadioButtonList1" runat="server" 
                  RepeatDirection="Horizontal" Visible="False">
                  <asp:ListItem Selected="True">CSA</asp:ListItem>
                  <asp:ListItem>SSA</asp:ListItem>
                  <asp:ListItem>LSA</asp:ListItem>
                  <asp:ListItem>None</asp:ListItem>
              </asp:RadioButtonList>
              </td><td>&nbsp;</td>
          </tr>  
    </table>
    <table width="100%" class="NormalTextBlack">
    <tr>
    <td colspan="3">&nbsp;
        </td>
    </tr>
    <tr>
    <td colspan="3">
    <input  id="btnCancel" value="Cancel" type="button" runat="server"
            style="width: 141px; cursor: hand;" onserverclick="btnCancel_ServerClick" />
                <input  id="btnPrint" value="Print" type="button"
            onclick="print_page();javascript:window.print(); viewControl();" 
            style="width: 141px; cursor: hand;" />
                </td>
    </tr>
    <tr>
    <td align="left">&nbsp;
        </td><td>&nbsp;</td><td>&nbsp;</td>
    </tr>
    <tr>
    <td align="left">&nbsp;
        </td><td>&nbsp;</td><td>&nbsp;</td>
    </tr>
    <tr>
    <td align="left">
        Signature Of The Sales Person</td><td>&nbsp;</td><td>Approved By</td>
    </tr>
    <tr>
    <td align="left">
        Enclosures:</td><td>&nbsp;</td><td>&nbsp;</td>
    </tr>
    <tr>
    <td align="left">&nbsp;
        </td><td>&nbsp;</td><td>&nbsp;</td>
    </tr>
    <tr>
    <td align="left">&nbsp;
        </td><td>&nbsp;</td><td>&nbsp;</td>
    </tr>
 </table>
</div>
</form>
</body>
</html>
