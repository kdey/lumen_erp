﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;
using System.Data.SqlClient;

public partial class EventEntry_CustomerMachine : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!this.IsPostBack)
        {
            bindControl();
            bindItems();
            bindGrid();
        }
    }
    protected void add_Click(object sender, EventArgs e)
    {
        try
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                if (Id.Value == "")
                {
                    SpParam[] sp = new SpParam[3];
                    sp[0] = new SpParam("@customerId", Convert.ToInt32(ddlCustomer.SelectedValue), SqlDbType.Int);
                    sp[1] = new SpParam("@itemId", Convert.ToInt32(ddlItem.SelectedValue), SqlDbType.Int);
                    sp[2] = new SpParam("@SRNo", txtSRNo.Text.ToString(), SqlDbType.VarChar);
                    int i = conn.ExecuteCommandProc("Lum_Erp_Stp_AddCustomerMachine", sp);
                    if (i > 0)
                    {
                        message.Text = "Customer Machine Added.";
                        message.ForeColor = Color.Green;
                        ddlCustomer.SelectedValue = "0";
                        ddlItem.SelectedValue = "0";
                        txtSRNo.Text = "";
                    }
                    else
                    {
                        message.Text = "This Item Serial No. already exists.";
                        message.ForeColor = Color.Red;
                        txtSRNo.Text = "";
                    }
                }
                else
                {
                    SpParam[] sp = new SpParam[4];
                    sp[0] = new SpParam("@customerId", Convert.ToInt32(ddlCustomer.SelectedValue), SqlDbType.Int);
                    sp[1] = new SpParam("@itemId", Convert.ToInt32(ddlItem.SelectedValue), SqlDbType.Int);
                    sp[2] = new SpParam("@SRNo", txtSRNo.Text.ToString(), SqlDbType.VarChar);
                    sp[3] = new SpParam("@custMacId", Convert.ToInt32(Id.Value), SqlDbType.Int);
                    int i = conn.ExecuteCommandProc("Lum_Erp_Stp_UpdateCustomerMachine", sp);
                    if (i > 0)
                    {
                        message.Text = "Customer Machine Updated.";
                        message.ForeColor = Color.Green;
                        ddlCustomer.SelectedValue = "0";
                        ddlItem.SelectedValue = "0";
                        txtSRNo.Text = "";
                        Id.Value = "";
                        add.Text = "Add";
                    }
                    else
                    {
                        message.Text = "This Item Serial No. already exists.";
                        message.ForeColor = Color.Red;
                    }
                }
            }
            bindGrid();
        }
        catch(SqlException ex)
        {
            message.Text = ex.ToString();
            message.ForeColor = Color.Red;
        }
    }
    protected void cancel_Click(object sender, EventArgs e)
    {

    }
    public void bindControl()
    {
        BindListControl _control = new BindListControl();
        _control.BindCustomer(ddlCustomer);
    }
    public void bindItems()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DataSet _Ds = conn.GetDataSet("SELECT itemId,itemCode FROM Lum_Erp_Item order by itemCode", "Item");
            ddlItem.ClearSelection();
            ddlItem.Items.Clear();
            ddlItem.AppendDataBoundItems = true;
            ddlItem.Items.Add("Please Select");
            ddlItem.Items[0].Value = "0";
            ddlItem.DataTextField = _Ds.Tables[0].Columns["itemCode"].ToString();
            ddlItem.DataValueField = _Ds.Tables[0].Columns["itemId"].ToString();
            ddlItem.DataSource = _Ds;
            ddlItem.DataBind();
        }
    }
    protected void gvCustMachine_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            if (e.CommandName.Equals("Edit"))
            {
                int M_Id = (int)gvCustMachine.DataKeys[e.Item.ItemIndex];
                message.Text = "";
                LoadMachine(M_Id);
            }
        }
    }
    protected void gvCustMachine_ItemCreated(object sender, DataGridItemEventArgs e)
    {

    }
    public void bindGrid()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (DataSet ds = conn.GetDataSet("Lum_Erp_Stp_GetCustomerMachine", "table"))
            {
                gvCustMachine.DataSource = ds;
                gvCustMachine.DataBind();
            }
        }
    }
    public void LoadMachine(int M_Id)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            SpParam sp = new SpParam("@custMacId", M_Id, SqlDbType.Int);
            using (IDataReader reader = conn.ExecuteQueryProc("Lum_Erp_Stp_GetCustomerMachine", sp))
            {
                if (reader.Read())
                {
                    ddlCustomer.SelectedValue = Convert.ToString(reader["customerId"]);
                    ddlItem.SelectedValue = Convert.ToString(reader["itemId"]);
                    txtSRNo.Text = Convert.ToString(reader["SRNo"]);
                    Id.Value = Convert.ToString(reader["custMacId"]);
                    add.Text = "Update";
                }
            }
        }
    }
}
