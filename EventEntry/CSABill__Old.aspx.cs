﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Bitscrape.AppBlock.Database;

public partial class EventEntry_CSABill : BasePage
{
    string _id = string.Empty;
    public string Action = string.Empty;
    //modified on 09-04-2012 by Sahammad & Shubhadeep
    DateTime current = DateTime.ParseExact("04/01/2012", "MM/dd/yyyy", null);
    DateTime prev = new DateTime();
    //-------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (ViewState["previousPage"] == null)
        {
            ViewState["previousPage"] = Convert.ToString(Request.UrlReferrer);
        }
        if (!string.IsNullOrEmpty(Request["View"]))
        {
            Action = (Request["View"]);
        }
        if (!IsPostBack)
        {
            BindData();
            BindPostDate();
        }
    }
    protected void btnCancel_ServerClick(object sender, EventArgs e)
    {
        if (ViewState["previousPage"] != null)
        {
            Response.Redirect(Convert.ToString(ViewState["previousPage"]));
        }

    }
    public void BindData()
    {
        _id = Request.QueryString["Id"].ToString();
        
        if (Action == "View")
        {
            txtCurrentReading.Enabled = false;
            txtlCurrentReadingDate.Enabled = false;
            lblPreviousReading.Enabled = false;
            lblPreviousReadingDate.Enabled = false;
            txtFreeCopies.Enabled = false;
            lblGrossCopies.Enabled = false;
            lblServiceCopies.Enabled = false;
            lblNetBillableCopies.Enabled = false;
            txtAgreedCopyRate.Enabled = false;
            txtAgreedMinMonthlyCopies.Enabled = false;
            lblNetBillableCharges.Enabled = false;
            txtMinBillingCharges.Enabled = false;
            lblBillableAmount.Enabled = false;
            lblVat.Enabled = false;
            lblServiceTax.Enabled = false;
            lblEducationalCess.Enabled = false;
            lblInvoiceAmount.Enabled = false;
            lblNetPayable.Enabled = false;
        }
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            SpParam sp = new SpParam("@Id",_id, SqlDbType.Decimal);
            using (IDataReader reader = conn.ExecuteQueryProc("Lum_Erp_Stp_GetCSABilling",sp))
            {
                if (reader.Read())
                {
                    lblName.Text = Convert.ToString(reader["customerName"]);
                    lblAddress.Text = Convert.ToString(reader["BAdd"]);
                    lblPh.Text = Convert.ToString(reader["Shipping_address1"]);
                    lblMob.Text = Convert.ToString(reader["Shipping_address2"]);
                    lblModel.Text = Convert.ToString(reader["itemCode"]);
                    lblMCSrlNo.Text = Convert.ToString(reader["SRNo"]);
                    lblContactPerson.Text = Convert.ToString(reader["contactPersonName"]);
                    //lblCurrentReadingDate.Text = Convert.ToString(reader["currMeterDate"]);
                    txtlCurrentReadingDate.Text = Convert.ToString(reader["currMeterDate"]);
                    //lblCurrentReading.Text = Convert.ToString(reader["currMeterReading"]);
                    txtCurrentReading.Text = Convert.ToString(reader["currMeterReading"]);

                    txtFreeCopies.Text = Convert.ToString(reader["freeCopy"]);
                    txtAgreedCopyRate.Text = Convert.ToString(reader["copyRate"]);
                    txtAgreedMinMonthlyCopies.Text = Convert.ToString(reader["minCopy"]);
                    lblAgreementNo.Text = Convert.ToString(reader["contractNo"]);
                    lblInvNo.Text = Convert.ToString(reader["ServiceBillNo"]);
                    lblInvDate.Text = Convert.ToString(reader["ServiceBillDate"]);
                    prev = Convert.ToDateTime(reader["ServiceBillDate"]);
                    
                }
            }
            using (IDataReader reader1 = conn.ExecuteQueryProc("Lum_Erp_Stp_GetCSABilling_Prev", sp))
            {

                if (reader1.Read())
                {
                    lblPreviousReadingDate.Text = Convert.ToString(reader1["MeterDate"]);
                    lblPreviousReading.Text = Convert.ToString(reader1["currMeterReading"]);
                }
                else
                {
                    lblPreviousReading.Text = "0";
                }
            }
            using (IDataReader _reader = conn.ExecuteQuery(string.Format("select TIN,ServiceTaxRegnNo,cstNo,* FROM Lum_Erp_CompanyMaster where fromdate <=DATEADD(MI,330,GETUTCDATE() ) and isnull(todate,DATEADD(MI,330,GETUTCDATE() )) >=DATEADD(MI,330,GETUTCDATE() )")))
            {
                if (_reader.Read())
                {
                    lblTinNo.Text = Convert.ToString(_reader["TIN"]);
                    lblServiceTaxRegistNo.Text = Convert.ToString(_reader["ServiceTaxRegnNo"]);
                    lblCSTNo.Text = Convert.ToString(_reader["cstNo"]);
                }
            }
        }
        double _NetBillableCharges = 0;
        double _BillableAmount = 0;

        double _MinBillingCharges = 0;
        double _Vat = 0;
        double _ServiceTax = 0;
        double _EducationalCess = 0;
        double _InvoiceAmount = 0;

        //lblGrossCopies.Text = (Convert.ToInt32(lblCurrentReading.Text) - (Convert.ToInt32(lblPreviousReading.Text) + Convert.ToInt32(txtFreeCopies.Text))).ToString();
        //changed 7th jan as per duke lblGrossCopies.Text = ((Convert.ToInt32(txtCurrentReading.Text) - (Convert.ToInt32(lblPreviousReading.Text)) + Convert.ToInt32(txtFreeCopies.Text))).ToString();
        lblGrossCopies.Text = ((Convert.ToInt32(txtCurrentReading.Text) - (Convert.ToInt32(lblPreviousReading.Text)) - Convert.ToInt32(txtFreeCopies.Text))).ToString();
        lblServiceCopies.Text = (Convert.ToInt32(Convert.ToInt32(lblGrossCopies.Text) * .01)).ToString();
        lblNetBillableCopies.Text = ((Convert.ToInt32(lblGrossCopies.Text)) - (Convert.ToInt32(lblServiceCopies.Text))).ToString();
        txtMinBillingCharges.Text = (Convert.ToInt32(txtAgreedMinMonthlyCopies.Text) * Convert.ToDecimal(txtAgreedCopyRate.Text)).ToString();
        _MinBillingCharges =Convert.ToDouble((Convert.ToInt32(txtAgreedMinMonthlyCopies.Text) * Convert.ToDecimal(txtAgreedCopyRate.Text)));

        if (Convert.ToInt32(lblNetBillableCopies.Text) > Convert.ToInt32(txtAgreedMinMonthlyCopies.Text))
        {
            lblNetBillableCharges.Text = ((Convert.ToInt32(lblNetBillableCopies.Text)) * (Convert.ToDecimal(txtAgreedCopyRate.Text))).ToString();
            _NetBillableCharges = Convert.ToDouble((Convert.ToInt32(lblNetBillableCopies.Text)) * (Convert.ToDecimal(txtAgreedCopyRate.Text)));
        }
        else 
        {
            lblNetBillableCharges.Text = (Convert.ToInt32(txtAgreedMinMonthlyCopies.Text) * Convert.ToDecimal(txtAgreedCopyRate.Text)).ToString();
            _NetBillableCharges = Convert.ToDouble(Convert.ToInt32(txtAgreedMinMonthlyCopies.Text) * Convert.ToDecimal(txtAgreedCopyRate.Text));
        }
        if (Convert.ToDecimal(lblNetBillableCharges.Text) > Convert.ToDecimal(txtMinBillingCharges.Text))
        {
            lblBillableAmount.Text = Math.Round(Convert.ToDecimal(lblNetBillableCharges.Text),2).ToString();
            _BillableAmount = Math.Round(_NetBillableCharges, 2);
        }
        else 
        {
            lblBillableAmount.Text = Math.Round(Convert.ToDecimal(txtMinBillingCharges.Text),2).ToString();
            _BillableAmount = Math.Round(_MinBillingCharges, 2);

        }
        lblVat.Text = Math.Round(((Convert.ToDouble(lblBillableAmount.Text) * .8) * .04),2).ToString();
        _Vat = Math.Round(((_BillableAmount * .8) * .04), 2);
        //modified on 09-04-2012 by Sahammad & Shubhadeep
        //Service Tax was increased by 2% (changed from 10% to 12%)from 01-04-2012 by WB Govt.
        if (prev >= current)
        {
            lblCurrentServiceTax.Text = "12%";
            lblServiceTax.Text = Math.Round((Convert.ToDouble(lblBillableAmount.Text) * .12), 2).ToString();
            _ServiceTax = Math.Round((_BillableAmount * .12), 2);
        }
        else
        {
            lblCurrentServiceTax.Text = "10%";
            lblServiceTax.Text = Math.Round((Convert.ToDouble(lblBillableAmount.Text) * .1), 2).ToString();
            _ServiceTax = Math.Round((_BillableAmount * .1), 2);
        }
        //------------
        lblEducationalCess.Text = Math.Round((Convert.ToDouble(lblServiceTax.Text) * .03),2).ToString();
        _EducationalCess = Math.Round((_ServiceTax * .03), 2);

        lblInvoiceAmount.Text = Math.Round((Convert.ToDecimal(lblBillableAmount.Text) + Convert.ToDecimal(lblVat.Text) + Convert.ToDecimal(lblServiceTax.Text) + Convert.ToDecimal(lblEducationalCess.Text)),2).ToString();
        _InvoiceAmount = Math.Round((_BillableAmount + _Vat + _ServiceTax + _EducationalCess), 2);

        lblNetPayable.Text = Math.Round(Convert.ToDecimal(lblInvoiceAmount.Text),0).ToString() + ".00";
        lblAmountInWords.Text = changeCurrencyToWords(Math.Round(_InvoiceAmount));
        if (Convert.ToInt32(lblGrossCopies.Text) < 0)
            lblGrossCopies.Text = "";
        if (Convert.ToInt32(lblServiceCopies.Text) < 0)
            lblServiceCopies.Text = "";
        if (Convert.ToInt32(lblNetBillableCopies.Text) < 0)
            lblNetBillableCopies.Text = "";
        if (Convert.ToInt32(txtCurrentReading.Text) == 0)
        {
            lblVat.Text  = "";
            lblInvoiceAmount.Text = "";
            lblNetPayable.Text = "";
            lblBillableAmount.Text = "";
            txtMinBillingCharges.Text = "";
            txtCurrentReading.Text = "";
            lblServiceTax.Text = "";
            lblEducationalCess.Text = "";
            lblAmountInWords.Text = "";
            lblNetPayable.Text = "";
        }
    }
    protected void txtAgreedCopyRate_TextChanged(object sender, EventArgs e)
    {
        BindPostDate();

        //DateTime date = DateTime.Today;
        //if (string.IsNullOrEmpty(txtlCurrentReadingDate.Text))
        //{
        //    txtlCurrentReadingDate.Text = DateTime.Today.ToString("dd MMM yyyy");
        //}
        //else
        //{
        //    date = DateTime.ParseExact(txtlCurrentReadingDate.Text, "dd MMM yyyy", null);
        //}

        //_id = Request.QueryString["Id"].ToString();
        //int _CurrentReading = 0;
        //if (string.IsNullOrEmpty(txtCurrentReading.Text))
        //{
        //}
        //else
        //{
        //    _CurrentReading = Convert.ToInt32(txtCurrentReading.Text);
        //}


        //    SpParam[] _Sparray = new SpParam[7];
        //    _Sparray[0] = new SpParam("@Id", _id, SqlDbType.Decimal);
        //    _Sparray[1] = new SpParam("@freeCopy", Convert.ToInt32(txtFreeCopies.Text), SqlDbType.Int);
        //    _Sparray[2] = new SpParam("@copyRate", Convert.ToDecimal(txtAgreedCopyRate.Text), SqlDbType.Decimal);
        //    _Sparray[3] = new SpParam("@minCopy", Convert.ToInt32(txtAgreedMinMonthlyCopies.Text), SqlDbType.Int);
        //    _Sparray[4] = new SpParam("@currMeterReading",_CurrentReading, SqlDbType.Int);
        //    _Sparray[5] = new SpParam("@currMeterDate", date, SqlDbType.DateTime);
        //    _Sparray[6] = new SpParam("@NetPayable", Convert.ToDecimal(lblNetPayable.Text) , SqlDbType.Decimal );
        
        
        //    try
        //    {
        //        using (IConnection conn = DataConnectionFactory.GetConnection())
        //        {


        //            int _i = conn.ExecuteCommandProc("Lum_Erp_Stp_UpdateCSABillOnlyThree", _Sparray);

        //            if (_i == 1)
        //            {

        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Page.RegisterClientScriptBlock("message", "<script>alert('" + ex.Message + ".');</script>");
        //    }


        //    double _NetBillableCharges = 0;
        //    double _BillableAmount = 0;

        //    double _MinBillingCharges = 0;
        //    double _Vat = 0;
        //    double _ServiceTax = 0;
        //    double _EducationalCess = 0;
        //    double _InvoiceAmount = 0;
        //    if (string.IsNullOrEmpty(txtCurrentReading.Text))
        //    {
        //        txtCurrentReading.Text = "0";
        //    }
        
        //    //lblGrossCopies.Text = (Convert.ToInt32(lblCurrentReading.Text) - (Convert.ToInt32(lblPreviousReading.Text) + Convert.ToInt32(txtFreeCopies.Text))).ToString();
        //    lblGrossCopies.Text = (Convert.ToInt32(txtCurrentReading.Text) - (Convert.ToInt32(lblPreviousReading.Text) + Convert.ToInt32(txtFreeCopies.Text))).ToString();
        //    lblServiceCopies.Text = (Convert.ToInt32(Convert.ToInt32(lblGrossCopies.Text) * .01)).ToString();
        //    lblNetBillableCopies.Text = ((Convert.ToInt32(lblGrossCopies.Text)) - (Convert.ToInt32(lblServiceCopies.Text))).ToString();
        //    txtMinBillingCharges.Text = (Convert.ToInt32(txtAgreedMinMonthlyCopies.Text) * Convert.ToDecimal(txtAgreedCopyRate.Text)).ToString();
        //    _MinBillingCharges = Convert.ToDouble((Convert.ToInt32(txtAgreedMinMonthlyCopies.Text) * Convert.ToDecimal(txtAgreedCopyRate.Text)));

        //    if (Convert.ToInt32(lblNetBillableCopies.Text) > Convert.ToInt32(txtAgreedMinMonthlyCopies.Text))
        //    {
        //        lblNetBillableCharges.Text = ((Convert.ToInt32(lblNetBillableCopies.Text)) * (Convert.ToDecimal(txtAgreedCopyRate.Text))).ToString();
        //        _NetBillableCharges = Convert.ToDouble((Convert.ToInt32(lblNetBillableCopies.Text)) * (Convert.ToDecimal(txtAgreedCopyRate.Text)));
        //    }
        //    else
        //    {
        //        lblNetBillableCharges.Text = (Convert.ToInt32(txtAgreedMinMonthlyCopies.Text) * Convert.ToDecimal(txtAgreedCopyRate.Text)).ToString();
        //        _NetBillableCharges = Convert.ToDouble(Convert.ToInt32(txtAgreedMinMonthlyCopies.Text) * Convert.ToDecimal(txtAgreedCopyRate.Text));
        //    }
        //    if (Convert.ToDecimal(lblNetBillableCharges.Text) > Convert.ToDecimal(txtMinBillingCharges.Text))
        //    {
        //        lblBillableAmount.Text = Math.Round(Convert.ToDecimal(lblNetBillableCharges.Text), 2).ToString();
        //        _BillableAmount = Math.Round(_NetBillableCharges, 2);
        //    }
        //    else
        //    {
        //        lblBillableAmount.Text = Math.Round(Convert.ToDecimal(txtMinBillingCharges.Text), 2).ToString();
        //        _BillableAmount = Math.Round(_MinBillingCharges, 2);

        //    }
        //    lblVat.Text = Math.Round(((Convert.ToDouble(lblBillableAmount.Text) * .8) * .04), 2).ToString();
        //    _Vat = Math.Round(((_BillableAmount * .8) * .04), 2);

        //    lblServiceTax.Text = Math.Round((Convert.ToDouble(lblBillableAmount.Text) * .1), 2).ToString();
        //    _ServiceTax = Math.Round((_BillableAmount * .1), 2);

        //    lblEducationalCess.Text = Math.Round((Convert.ToDouble(lblServiceTax.Text) * .03), 2).ToString();
        //    _EducationalCess = Math.Round((_ServiceTax * .03), 2);

        //    lblInvoiceAmount.Text = Math.Round((Convert.ToDecimal(lblBillableAmount.Text) + Convert.ToDecimal(lblVat.Text) + Convert.ToDecimal(lblServiceTax.Text) + Convert.ToDecimal(lblEducationalCess.Text)), 2).ToString();
        //    _InvoiceAmount = Math.Round((_BillableAmount + _Vat + _ServiceTax + _EducationalCess), 2);

        //    lblNetPayable.Text = Math.Round(Convert.ToDecimal(lblInvoiceAmount.Text), 0).ToString() + ".00";
        //    lblAmountInWords.Text = changeCurrencyToWords(Math.Round(_InvoiceAmount));
        //    if (Convert.ToInt32(lblGrossCopies.Text) < 0)
        //        lblGrossCopies.Text = "";
        //    if (Convert.ToInt32(lblServiceCopies.Text) < 0)
        //        lblServiceCopies.Text = "";
        //    if (Convert.ToInt32(lblNetBillableCopies.Text) < 0)
        //        lblNetBillableCopies.Text = "";
        //    if (Convert.ToInt32(txtCurrentReading.Text) == 0)
        //    {
        //        lblVat.Text = "";
        //        lblInvoiceAmount.Text = "";
        //        lblNetPayable.Text = "";
        //        lblBillableAmount.Text = "";
        //        txtMinBillingCharges.Text = "";
        //        txtCurrentReading.Text = "";
        //        lblServiceTax.Text = "";
        //        lblEducationalCess.Text = "";
        //        lblAmountInWords.Text = "";
        //        lblNetPayable.Text = "";
        //    }

    }
    protected void txtAgreedMinMonthlyCopies_TextChanged(object sender, EventArgs e)
    {
        BindPostDate();
        //DateTime date = DateTime.Today;
        //if (string.IsNullOrEmpty(txtlCurrentReadingDate.Text))
        //{
        //    txtlCurrentReadingDate.Text = DateTime.Today.ToString("dd MMM yyyy");
        //}
        //else
        //{
        //    date = DateTime.ParseExact(txtlCurrentReadingDate.Text, "dd MMM yyyy", null);
        //}
        //_id = Request.QueryString["Id"].ToString();

        //SpParam[] _Sparray = new SpParam[7];
        //_Sparray[0] = new SpParam("@Id", _id, SqlDbType.Decimal);
        //_Sparray[1] = new SpParam("@freeCopy", Convert.ToInt32(txtFreeCopies.Text), SqlDbType.Int);
        //_Sparray[2] = new SpParam("@copyRate", Convert.ToDecimal(txtAgreedCopyRate.Text), SqlDbType.Decimal);
        //_Sparray[3] = new SpParam("@minCopy", Convert.ToInt32(txtAgreedMinMonthlyCopies.Text), SqlDbType.Int);
        //_Sparray[4] = new SpParam("@currMeterReading", Convert.ToInt32(txtCurrentReading.Text), SqlDbType.Int);
        //_Sparray[5] = new SpParam("@currMeterDate", date, SqlDbType.DateTime);
        //_Sparray[6] = new SpParam("@NetPayable", Convert.ToDecimal(lblNetPayable.Text), SqlDbType.Decimal);
        //try
        //{
        //    using (IConnection conn = DataConnectionFactory.GetConnection())
        //    {


        //        int _i = conn.ExecuteCommandProc("Lum_Erp_Stp_UpdateCSABillOnlyThree", _Sparray);

        //        if (_i == 1)
        //        {

        //        }

        //    }
        //}
        //catch (Exception ex)
        //{
        //    Page.RegisterClientScriptBlock("message", "<script>alert('" + ex.Message + ".');</script>");
        //}


        //double _NetBillableCharges = 0;
        //double _BillableAmount = 0;

        //double _MinBillingCharges = 0;
        //double _Vat = 0;
        //double _ServiceTax = 0;
        //double _EducationalCess = 0;
        //double _InvoiceAmount = 0;

        ////lblGrossCopies.Text = (Convert.ToInt32(lblCurrentReading.Text) - (Convert.ToInt32(lblPreviousReading.Text) + Convert.ToInt32(txtFreeCopies.Text))).ToString();
        //lblGrossCopies.Text = (Convert.ToInt32(txtCurrentReading.Text) - (Convert.ToInt32(lblPreviousReading.Text) + Convert.ToInt32(txtFreeCopies.Text))).ToString();
        //lblServiceCopies.Text = (Convert.ToInt32(Convert.ToInt32(lblGrossCopies.Text) * .01)).ToString();
        //lblNetBillableCopies.Text = ((Convert.ToInt32(lblGrossCopies.Text)) - (Convert.ToInt32(lblServiceCopies.Text))).ToString();
        //txtMinBillingCharges.Text = (Convert.ToInt32(txtAgreedMinMonthlyCopies.Text) * Convert.ToDecimal(txtAgreedCopyRate.Text)).ToString();
        //_MinBillingCharges = Convert.ToDouble((Convert.ToInt32(txtAgreedMinMonthlyCopies.Text) * Convert.ToDecimal(txtAgreedCopyRate.Text)));

        //if (Convert.ToInt32(lblNetBillableCopies.Text) > Convert.ToInt32(txtAgreedMinMonthlyCopies.Text))
        //{
        //    lblNetBillableCharges.Text = ((Convert.ToInt32(lblNetBillableCopies.Text)) * (Convert.ToDecimal(txtAgreedCopyRate.Text))).ToString();
        //    _NetBillableCharges = Convert.ToDouble((Convert.ToInt32(lblNetBillableCopies.Text)) * (Convert.ToDecimal(txtAgreedCopyRate.Text)));
        //}
        //else
        //{
        //    lblNetBillableCharges.Text = (Convert.ToInt32(txtAgreedMinMonthlyCopies.Text) * Convert.ToDecimal(txtAgreedCopyRate.Text)).ToString();
        //    _NetBillableCharges = Convert.ToDouble(Convert.ToInt32(txtAgreedMinMonthlyCopies.Text) * Convert.ToDecimal(txtAgreedCopyRate.Text));
        //}
        //if (Convert.ToDecimal(lblNetBillableCharges.Text) > Convert.ToDecimal(txtMinBillingCharges.Text))
        //{
        //    lblBillableAmount.Text = Math.Round(Convert.ToDecimal(lblNetBillableCharges.Text), 2).ToString();
        //    _BillableAmount = Math.Round(_NetBillableCharges, 2);
        //}
        //else
        //{
        //    lblBillableAmount.Text = Math.Round(Convert.ToDecimal(txtMinBillingCharges.Text), 2).ToString();
        //    _BillableAmount = Math.Round(_MinBillingCharges, 2);

        //}
        //lblVat.Text = Math.Round(((Convert.ToDouble(lblBillableAmount.Text) * .8) * .04), 2).ToString();
        //_Vat = Math.Round(((_BillableAmount * .8) * .04), 2);

        //lblServiceTax.Text = Math.Round((Convert.ToDouble(lblBillableAmount.Text) * .1), 2).ToString();
        //_ServiceTax = Math.Round((_BillableAmount * .1), 2);

        //lblEducationalCess.Text = Math.Round((Convert.ToDouble(lblServiceTax.Text) * .03), 2).ToString();
        //_EducationalCess = Math.Round((_ServiceTax * .03), 2);

        //lblInvoiceAmount.Text = Math.Round((Convert.ToDecimal(lblBillableAmount.Text) + Convert.ToDecimal(lblVat.Text) + Convert.ToDecimal(lblServiceTax.Text) + Convert.ToDecimal(lblEducationalCess.Text)), 2).ToString();
        //_InvoiceAmount = Math.Round((_BillableAmount + _Vat + _ServiceTax + _EducationalCess), 2);

        //lblNetPayable.Text = Math.Round(Convert.ToDecimal(lblInvoiceAmount.Text), 0).ToString() + ".00";
        //lblAmountInWords.Text = changeCurrencyToWords(Math.Round(_InvoiceAmount));
    }
    protected void txtFreeCopies_TextChanged(object sender, EventArgs e)
    {
        BindPostDate();
    }
    private void BindPostDate()
    {
         double _NetBillableCharges = 0;
        double _BillableAmount = 0;

        double _MinBillingCharges = 0;
        double _Vat = 0;
        double _ServiceTax = 0;
        double _EducationalCess = 0;
        double _InvoiceAmount = 0;

        //lblGrossCopies.Text = (Convert.ToInt32(lblCurrentReading.Text) - (Convert.ToInt32(lblPreviousReading.Text) + Convert.ToInt32(txtFreeCopies.Text))).ToString();
        //7th jan lblGrossCopies.Text = ((Convert.ToInt32(txtCurrentReading.Text) - (Convert.ToInt32(lblPreviousReading.Text)) + Convert.ToInt32(txtFreeCopies.Text))).ToString();
        lblGrossCopies.Text = ((Convert.ToInt32(txtCurrentReading.Text) - (Convert.ToInt32(lblPreviousReading.Text)) - Convert.ToInt32(txtFreeCopies.Text))).ToString();

        lblServiceCopies.Text = (Convert.ToInt32(Convert.ToInt32(lblGrossCopies.Text) * .01)).ToString();
        lblNetBillableCopies.Text = ((Convert.ToInt32(lblGrossCopies.Text)) - (Convert.ToInt32(lblServiceCopies.Text))).ToString();
        txtMinBillingCharges.Text = (Convert.ToInt32(txtAgreedMinMonthlyCopies.Text) * Convert.ToDecimal(txtAgreedCopyRate.Text)).ToString();
        _MinBillingCharges = Convert.ToDouble((Convert.ToInt32(txtAgreedMinMonthlyCopies.Text) * Convert.ToDecimal(txtAgreedCopyRate.Text)));

        if (Convert.ToInt32(lblNetBillableCopies.Text) > Convert.ToInt32(txtAgreedMinMonthlyCopies.Text))
        {
            lblNetBillableCharges.Text = ((Convert.ToInt32(lblNetBillableCopies.Text)) * (Convert.ToDecimal(txtAgreedCopyRate.Text))).ToString();
            _NetBillableCharges = Convert.ToDouble((Convert.ToInt32(lblNetBillableCopies.Text)) * (Convert.ToDecimal(txtAgreedCopyRate.Text)));
        }
        else
        {
            lblNetBillableCharges.Text = (Convert.ToInt32(txtAgreedMinMonthlyCopies.Text) * Convert.ToDecimal(txtAgreedCopyRate.Text)).ToString();
            _NetBillableCharges = Convert.ToDouble(Convert.ToInt32(txtAgreedMinMonthlyCopies.Text) * Convert.ToDecimal(txtAgreedCopyRate.Text));
        }
        if (Convert.ToDecimal(lblNetBillableCharges.Text) > Convert.ToDecimal(txtMinBillingCharges.Text))
        {
            lblBillableAmount.Text = Math.Round(Convert.ToDecimal(lblNetBillableCharges.Text), 2).ToString();
            _BillableAmount = Math.Round(_NetBillableCharges, 2);
        }
        else
        {
            lblBillableAmount.Text = Math.Round(Convert.ToDecimal(txtMinBillingCharges.Text), 2).ToString();
            _BillableAmount = Math.Round(_MinBillingCharges, 2);

        }
        lblVat.Text = Math.Round(((Convert.ToDouble(lblBillableAmount.Text) * .8) * .04), 2).ToString();
        _Vat = Math.Round(((_BillableAmount * .8) * .04), 2);

        //modified on 09-04-2012 by Sahammad & Shubhadeep
        //Service Tax was increased by 2% (changed from 10% to 12%)from 01-04-2012 by WB Govt.
        if (prev >= current)
        {
            lblCurrentServiceTax.Text = "12%";
            lblServiceTax.Text = Math.Round((Convert.ToDouble(lblBillableAmount.Text) * .12), 2).ToString();
            _ServiceTax = Math.Round((_BillableAmount * .12), 2);
        }
        else
        {
            lblCurrentServiceTax.Text = "10%";
            lblServiceTax.Text = Math.Round((Convert.ToDouble(lblBillableAmount.Text) * .1), 2).ToString();
            _ServiceTax = Math.Round((_BillableAmount * .1), 2);
        }
        //------------

        lblEducationalCess.Text = Math.Round((Convert.ToDouble(lblServiceTax.Text) * .03), 2).ToString();
        _EducationalCess = Math.Round((_ServiceTax * .03), 2);

        lblInvoiceAmount.Text = Math.Round((Convert.ToDecimal(lblBillableAmount.Text) + Convert.ToDecimal(lblVat.Text) + Convert.ToDecimal(lblServiceTax.Text) + Convert.ToDecimal(lblEducationalCess.Text)), 2).ToString();
        _InvoiceAmount = Math.Round((_BillableAmount + _Vat + _ServiceTax + _EducationalCess), 2);

        lblNetPayable.Text = Math.Round(Convert.ToDecimal(lblInvoiceAmount.Text), 0).ToString() + ".00";
        lblAmountInWords.Text = changeCurrencyToWords(Math.Round(_InvoiceAmount));


        DateTime date = DateTime.Today;
        if (string.IsNullOrEmpty(txtlCurrentReadingDate.Text))
        {
            txtlCurrentReadingDate.Text = DateTime.Today.ToString("dd MMM yyyy");
        }
        else
        {
            date = DateTime.ParseExact(txtlCurrentReadingDate.Text, "dd MMM yyyy", null);
        }
        _id = Request.QueryString["Id"].ToString();

        SpParam[] _Sparray = new SpParam[7];
        _Sparray[0] = new SpParam("@Id", _id, SqlDbType.Decimal);
        _Sparray[1] = new SpParam("@freeCopy", Convert.ToInt32(txtFreeCopies.Text), SqlDbType.Int);
        _Sparray[2] = new SpParam("@copyRate", Convert.ToDecimal(txtAgreedCopyRate.Text), SqlDbType.Decimal);
        _Sparray[3] = new SpParam("@minCopy", Convert.ToInt32(txtAgreedMinMonthlyCopies.Text), SqlDbType.Int);
        _Sparray[4] = new SpParam("@currMeterReading", Convert.ToInt32(txtCurrentReading.Text), SqlDbType.Int);
        _Sparray[5] = new SpParam("@currMeterDate", date, SqlDbType.DateTime);
        _Sparray[6] = new SpParam("@NetPayable", Convert.ToDecimal(lblNetPayable.Text), SqlDbType.Decimal);
        try
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {


                int _i = conn.ExecuteCommandProc("Lum_Erp_Stp_UpdateCSABillOnlyThree", _Sparray);

                if (_i == 1)
                {

                }

            }
        }
        catch (Exception ex)
        {
            Page.RegisterClientScriptBlock("message", "<script>alert('" + ex.Message + ".');</script>");
        }

    }
}
