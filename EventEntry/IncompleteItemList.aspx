﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EventEntry/EventEntry.master" AutoEventWireup="true" CodeFile="IncompleteItemList.aspx.cs" Inherits="EventEntry_IncompleteItemList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="PageMargin">
         <asp:Button ID="DetachItem" runat="server" Text="Detach Item"  
             CssClass="ButtonBig" onclick="DetachItem_Click" />
    <div class="Line"></div>
    <div class="PageHeading">Incomplete items</div>
    <asp:Label ID="message" runat="server" />
    <div class="Line"></div>
       
    
         
       
         
               <asp:DataGrid ID="itemGrid" runat="server" AutoGenerateColumns="false" Width="100%" DataKeyField="grndetailsSerialNoId"
            CellPadding="0" HeaderStyle-CssClass="GridHeading" GridLines="None"
            ItemStyle-CssClass="GridData" OnItemCreated="itemGrid_ItemCreated"   >
            <Columns>
                <asp:BoundColumn  HeaderStyle-HorizontalAlign="Left" DataField="itemCode" HeaderText="Item Code" HeaderStyle-Font-Bold="true" />
                <asp:BoundColumn  HeaderStyle-HorizontalAlign="Left" DataField="serialNo" HeaderText="serialNo#" HeaderStyle-Font-Bold="true" />
                <asp:TemplateColumn>
                <ItemTemplate>
                
                    <a href='<%#GetUrl(DataBinder.Eval(Container.DataItem, "grndetailsSerialNoId"))%>' class="GridLink">Edit</a>
                </ItemTemplate>
            </asp:TemplateColumn>
            
            </Columns>
        </asp:DataGrid>
     
     
     
        <!--Grid Table End-->
        <div class="Line">
        </div>
        <!--Page Body End-->
    </div>

</asp:Content>

