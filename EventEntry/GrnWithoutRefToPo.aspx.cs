﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;

public partial class EventEntry_GrnWithoutRefToPo : BasePage
{
    protected int _grnId = -1;
    protected int _poId = -1;
    private double totalVatAmount = 0;
    private double totalAmount = 0;
    public string Action = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!string.IsNullOrEmpty(Request["grnId"]))
        {
            _grnId = int.Parse(Request["grnId"]);
        }
        if (!string.IsNullOrEmpty(Request["poId"]))
        {
            _poId = int.Parse(Request["poId"]);
        }
        if (!string.IsNullOrEmpty(Request["View"]))
        {
            Action = (Request["View"]);
        }
        if (!this.IsPostBack)
        {
            bindControl();
            if (_grnId > 0)
            {
                if (Action == "View")
                {
                    btnUpdate.Visible = false;
                    addItemLine.Enabled = false;
                }
                else
                {
                    btnUpdate.Visible = true;
                }
                btnUpdate.Text = "Update";
                LoadGrn();
            }
           

        }
       
        if (GetLoggedinUser().UserType == 2)
        {

            btnUpdate.Visible = false;
            //addItemLine.Enabled = false;
        }
    }
    public void LoadGrn()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(@"select G.*,P.strPoId,P.poDate,P.poTypeId,P.rateContractTypeId,P.vendorId from Lum_Erp_GRN G
                                                            inner join Lum_Erp_PurchaseOrder P on G.poId=P.poId
                                                            WHERE  G.grnId=" + _grnId))
            {
                if (reader.Read())
                {
                   
                    ddlVendor.SelectedValue = Convert.ToString(reader["vendorId"]);
                    ddlStore.SelectedValue = Convert.ToString(reader["storeId"]);
                    poTypeDdl.SelectedValue = Convert.ToString(reader["poTypeId"]);
                    rateContractDdl.SelectedValue = Convert.ToString(reader["rateContractTypeId"]);
                    grnDate.Text = Convert.ToString(reader["dateCreated"]).Split(' ')[0];
                    vendorInvoiceNo.Text = Convert.ToString(reader["vendorInvoiceNo"]);
                    _poId = Convert.ToInt32(reader["poId"]);
                    
                    grnNumber.Text = Convert.ToString(reader["strGrnId"]) + "/" + Convert.ToString(reader["grnVersion"]); ;
                    invoiceDate.Text = Convert.ToString(Convert.ToDateTime(reader["invoiceDate"])).Split(' ')[0];
                    poNumber.Text = Convert.ToString(reader["strPoId"]);
                    if (Convert.ToString(reader["strPoId"]).Length > 0)
                        poDate.Text = Convert.ToString(Convert.ToDateTime(reader["poDate"])).Split(' ')[0];
                    else
                        poDate.Text = Convert.ToString(Convert.ToDateTime(reader["poDate"])).Split(' ')[0];
                    ddlStore.Enabled = false;


                    BindGrid();
                }
            }
        }
    }

    public void bindControl()
    {
         BindListControl _control = new BindListControl();
        _control.BindVendor(ddlVendor);
        _control.BindStores(ddlStore);
        _control.BindDivision(divisionDdl);
        _control.BindRateContractType(rateContractDdl);
        _control.BindOrderType(poTypeDdl);
    }
    protected void addItemLine_Click(object sender, EventArgs e)
    {
        message.Text = "";
        DateTime date = DateTime.Today;
        if (string.IsNullOrEmpty(poDate.Text))
        {
            //poDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
            date = DateTime.ParseExact((DateTime.Today.ToString("dd/MM/yyyy")), "dd/MM/yyyy", null);
        }
        else
        {
            date = DateTime.ParseExact(poDate.Text, "dd/MM/yyyy", null);
        }
        DateTime dtmGrnDate = Convert.ToDateTime(grnDate.Text);
        DateTime dtmInvoiceDate = Convert.ToDateTime(invoiceDate.Text);
        Validate();

        if (Convert.ToDecimal(quantity.Text) == 0)
        {
            return;
        }
        else if (Convert.ToDecimal(price.Text) == 0)
        {
            return;
        }

        if (IsValid)
        {


            try
            {
                if (_grnId < 0)
                {

                    using (IConnection conn = DataConnectionFactory.GetConnection())
                    {

                        IDataReader _reader = conn.ExecuteQueryProc("Lum_Erp_Stp_CreateGrnWithOutRespectToPurchaseOrder",
                                                     new SpParam("@strPoId", string.Empty, SqlDbType.VarChar),
                                                     new SpParam("@poTypeId", int.Parse(poTypeDdl.SelectedValue), SqlDbType.Int),
                                                     new SpParam("@vendorId", ddlVendor.SelectedValue, SqlDbType.Int),
                                                     new SpParam("@poDate", date, SqlDbType.DateTime),
                                                     new SpParam("@rateContractTypeId", int.Parse(rateContractDdl.SelectedValue), SqlDbType.Int),
                                                     new SpParam("@createdByUser", 0, SqlDbType.Bit),
                                                     new SpParam("@strGrnId", string.Empty, SqlDbType.VarChar),
                                                     new SpParam("@dateCreated", dtmGrnDate, SqlDbType.DateTime),
                                                     new SpParam("@vendorInvoiceNo", vendorInvoiceNo.Text, SqlDbType.VarChar),
                                                     new SpParam("@invoiceDate", dtmInvoiceDate, SqlDbType.DateTime),
                                                     new SpParam("@storeId", int.Parse(ddlStore.SelectedValue), SqlDbType.Int),
                                                     new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int));


                        if (_reader.Read())
                        {
                            _poId = Convert.ToInt32(_reader["poId"]);
                            _grnId = Convert.ToInt32(_reader["grnId"]);
                            grnNumber.Text = Convert.ToString(_reader["strGRNId"]);
                            _reader.Close();
                            // that we have the poId we can create the po number
                            // Purchase Order Number Format: PO Type_ Code/Financial Year/Month/Serial No/Amendment No.
                            //string p = string.Format("{0}", poNumber.Text + " (" + _poId.ToString() + ")");
                            //conn.ExecuteCommand(string.Format("update Lum_Erp_PurchaseOrder set strPoId = '{0}' where poId = {1}", DataUtility.CleanString(p), _poId));
                            // GRN Number Format: Store-Location/Financial Year/Month/GRN Number/Amendment No.

                            //string getFinacilaYear = string.Empty;
                            //if (date.Month > 3)
                            //{
                            //    getFinacilaYear = string.Format("{0}-{1}", dtmGrnDate.Year, dtmGrnDate.Year + 1);
                            //}
                            //else
                            //{
                            //    getFinacilaYear = string.Format("{0}-{1}", dtmGrnDate.Year - 1, dtmGrnDate.Year);
                            //}
                            //string grn = string.Format("{0}/{1}/{2}/{3}",
                            //                                           ddlStore.SelectedItem.Text,
                            //                                           getFinacilaYear,
                            //                                           dtmGrnDate.Month,
                            //                                           _grnId);
                            //conn.ExecuteCommand(string.Format("update Lum_Erp_GRN set strGrnId = '{0}' where grnId = {1}", DataUtility.CleanString(grn), _grnId));
                            //grnNumber.Text = grn;
                            message.Text = "GRN   for Vendor Name " + ddlVendor.Items[ddlVendor.SelectedIndex].Text + " Added";
                            message.ForeColor = Color.Green;
                            ReadOnlyFiled();


                        }
                    }
                }
                if (_poId > 0)
                {
                    string fromDate = Convert.ToDateTime(poDate.Text).ToString("dd MMM yyyy");
                    //check if its Finsished good
                    using (IConnection conn = DataConnectionFactory.GetConnection())
                    {
                        if (divisionDdl.Items[divisionDdl.SelectedIndex].Text.ToUpper() == "FINISHED GOODS")
                        {

                            object obj = conn.ExecuteScalarProc("Lum_Erp_Stp_CreateGrnDetailsWithOutRespectToPurchaseOrder", new SpParam("@poId", _poId, SqlDbType.Int),
                                                                 new SpParam("@grnId", _grnId, SqlDbType.Int),
                                                                  new SpParam("@itemId", Convert.ToInt32(itemDdl.SelectedValue), SqlDbType.Int),
                                                                  new SpParam("@CDate", fromDate, SqlDbType.SmallDateTime),
                                                                  new SpParam("@quantity", quantity.Text, SqlDbType.Decimal),
                                                                  new SpParam("@price", price.Text, SqlDbType.Decimal),
                                                                  new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int));
                            //Populate grn item serial number talbe
                            if (obj != null)
                            {
                                int grnlineId = (int)obj;
                                InsertItemSerialNo(grnlineId, _grnId, Convert.ToInt32(itemDdl.SelectedValue), txtserialNo.Text, conn, Convert.ToDecimal(quantity.Text));

                            }
                            resetControl();
                            BindGrid();
                        }
                        else
                        {
                            //Populate GRN Details Table
                            object obj = conn.ExecuteScalarProc("Lum_Erp_Stp_CreateGrnDetailsWithOutRespectToPurchaseOrder", new SpParam("@poId", _poId, SqlDbType.Int),
                                                         new SpParam("@grnId", _grnId, SqlDbType.Int),
                                                          new SpParam("@itemId", Convert.ToInt32(itemDdl.SelectedValue), SqlDbType.Int),
                                                          new SpParam("@CDate", fromDate, SqlDbType.SmallDateTime),
                                                          new SpParam("@quantity", quantity.Text, SqlDbType.Decimal),
                                                          new SpParam("@price", price.Text, SqlDbType.Decimal),
                                                          new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int));
                            //Populate grn item serial number talbe
                            if (obj != null)
                            {
                                int grnlineId = (int)obj;
                                InsertItemSerialNo(grnlineId, _grnId, Convert.ToInt32(itemDdl.SelectedValue), txtserialNo.Text, conn, Convert.ToDecimal(quantity.Text));
                            }
                            resetControl();
                            BindGrid();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
                message.ForeColor = Color.Red;
                return;
            }

        }
    }
    /// <summary>
    /// Create the GRN 
    /// </summary>
    /// <param name="poId"></param>
    /// <param name="conn"></param>
  
    protected void itemDdl_SelectedIndexChanged(object sender, EventArgs e)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            LoadItemDescription(conn);
        }
    }
    /// <summary>
    /// Loads the item description of the selected item in the drop down.
    /// </summary>
    /// <param name="conn"></param>
    private void LoadItemDescription(IConnection conn)
    {
        string fromDate = Convert.ToDateTime(poDate.Text).ToString("dd MMM yyyy");
        try
        {
            if (!string.IsNullOrEmpty(itemDdl.SelectedValue))
            {
                using (IDataReader reader = conn.ExecuteQuery(
                           @"select itemDescription, uomName, v.Vat as vat 
                          from Lum_Erp_Item i
                          inner join Lum_Erp_Uom u on u.uomId = i.uomId 
                          inner join dbo._Lum_Erp_VatDetails v on i.itemType=v.itemType
                          where i.itemId = '" + itemDdl.SelectedValue + "' and '" + fromDate + "' >=v.FromDate and '" + fromDate + "' <= v.ToDate"))
                {
                    if (reader.Read())
                    {
                        itemDescription.Text = reader.GetString(0);
                        uom.Text = reader.GetString(1);
                        vat.Text = reader["vat"].ToString();
                    }
                }
            }

        }
        catch (Exception ex)
        {
            message.Text = ex.ToString();
            message.ForeColor = Color.Red;
        }
    }
    private void BindGrid()
    {
        string fromDate = Convert.ToDateTime(poDate.Text).ToString("dd MMM yyyy");
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            try
            {
                IDataReader _reader = conn.ExecuteQueryProc("Lum_erp_Stp_GetPurchaseOrder_WithGrnDetails_For_WithoutPO",
                                 new SpParam("@poId", _poId, SqlDbType.Int),
                                 new SpParam("@CDate", fromDate, SqlDbType.SmallDateTime),
                                 new SpParam("@grnId", _grnId, SqlDbType.Int));


                poLineGrid.DataSource = _reader;
                poLineGrid.DataBind();
            }
            catch (Exception ex)
            {
                message.Text = ex.ToString();


            }
        }
//        string sql = "";

//        sql = @"select poLineId, d.divisionName, sd.subdivisionName,i.itemCode, i.itemDescription,pd.itemId , pd.quantity,pd.quantityReceived,  u.uomName, pd.price, i.vat, 
//                        pd.price * pd.quantityReceived * 0.01 *i.vat 'vatAmount', (pd.price * pd.quantityReceived+pd.price * pd.quantityReceived * 0.01 * i.vat) AS total
//                        from Lum_Erp_PurchaseOrderDetails pd 
//                        inner join Lum_Erp_Item i on pd.itemId = i.itemId
//                        inner join Lum_Erp_Subdivision sd on sd.subdivisionId = i.subdivisionId
//                        inner join Lum_Erp_Division d on d.divisionId = sd.divisionId
//                        inner join Lum_Erp_UOM u on i.uomId = u.uomId
//                        where poId = " + _poId;
        
//        using (IConnection conn = DataConnectionFactory.GetConnection())
//        {
//            using (IDataReader reader = conn.ExecuteQuery(sql))
//            {
//                poLineGrid.DataSource = reader;
//                poLineGrid.DataBind();
//            }
//        }
    }
    protected void poLineGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
       if (e.CommandName.Equals("Delete"))
        {

            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                int lineId = (int) poLineGrid.DataKeys[e.Item.ItemIndex];
                conn.ExecuteCommandProc("Lum_Erp_Stp_DeleteGrnDetailsLine_WithOutRespectToPurchaseOrder",
                    new SpParam("@poId", _poId, SqlDbType.Int),
                    new SpParam("@lineId", lineId, SqlDbType.Int),
                    new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int),
                    new SpParam("@grnId", _grnId, SqlDbType.Int));
                gridMessage.Text = "";
                gridMessage.ForeColor = Color.Green;

            }
        }
            
       else if (e.CommandName.Equals("Update"))
            {
                int lineId;
                decimal quantity;
                int itemId;
                string serialNo;
                
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    for (int i = 0; i < poLineGrid.Items.Count; i++)
                    {
                        
                        lineId = i+1;// (int)poLineGrid.DataKeys[e.Item.ItemIndex];
                        quantity = decimal.Parse(((TextBox)poLineGrid.Items[i].FindControl("itemQty")).Text);
                        itemId = int.Parse(((HtmlInputHidden)poLineGrid.Items[i].FindControl("itemId")).Value);
                        serialNo = ((TextBox)poLineGrid.Items[i].FindControl("itemSerialNo")).Text;
                        decimal price = decimal.Parse(((TextBox)poLineGrid.Items[i].FindControl("itemPrice")).Text);
                        try
                        {
                            conn.ExecuteCommandProc("Lum_Erp_stp_UpdateGrnDetailsWithOutRespectToPurchaseOrder",
                                new SpParam("@grnId", _grnId, SqlDbType.Int),
                                new SpParam("@price", price, SqlDbType.Decimal),
                                new SpParam("@poId", _poId, SqlDbType.Int),
                                new SpParam("@lineId", lineId, SqlDbType.Int),
                                 new SpParam("@itemId", itemId, SqlDbType.Int),
                                new SpParam("@quantity", quantity, SqlDbType.Decimal),
                                new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int));
                            InsertItemSerialNo(lineId, _grnId, itemId, serialNo, conn, quantity);
                            gridMessage.Text = "Line updated";
                            gridMessage.ForeColor = Color.Green;

                        }
                        catch (Exception ex)
                        {
                            gridMessage.Text = ex.ToString(); // "Invalid quantity or price.";
                            gridMessage.ForeColor = Color.Red;
                        }
                    }


                    


                    //try
                    //{
                    //    int lineId = (int)poLineGrid.DataKeys[e.Item.ItemIndex];
                    //    int quantity = int.Parse(((TextBox)e.Item.FindControl("itemQty")).Text);
                    //    decimal price = decimal.Parse(((TextBox)e.Item.FindControl("itemPrice")).Text);
                    //    int itemId=int.Parse(((HtmlInputHidden)e.Item.FindControl("itemId")).Value);
                    //    string serialNo=((TextBox)e.Item.FindControl("itemSerialNo")).Text;


                        //if (poLineGrid.Items[e.Item.ItemIndex].Cells[0].Text.ToUpper() == "FINISHED GOODS")
                        //{
                        //    //Check if item is FG then user  have to give serial no.
                        //    if (serialNo != string.Empty)
                        //    {
                        //        string[] param = serialNo.Split(';');
                        //        if (quantity == param.Length)
                        //        {
                        //            conn.ExecuteCommandProc("Lum_Erp_stp_UpdateGrnDetailsWithOutRespectToPurchaseOrder",
                        //            new SpParam("@grnId", _grnId, SqlDbType.Int),
                        //            new SpParam("@price", price, SqlDbType.Decimal),
                        //            new SpParam("@poId", _poId, SqlDbType.Int),
                        //            new SpParam("@lineId", lineId, SqlDbType.Int),
                        //            new SpParam("@itemId", itemId, SqlDbType.Int),
                        //            new SpParam("@quantity", quantity, SqlDbType.Int),
                        //            new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int));
                        //            InsertItemSerialNo(lineId, _grnId, itemId, serialNo, conn, quantity);
                        //        //    gridMessage.Text = "Line updated";
                        //            gridMessage.Text = "";
                        //            gridMessage.ForeColor = Color.Green;
                        //        }
                        //        else
                        //        {
                        //            gridMessage.Text = "quantity is not equal to total serialNo";
                        //            gridMessage.ForeColor = Color.Red;
                        //        }
                        //    }
                        //    else
                        //    {
                        //        gridMessage.Text = "quantity is not equal to total serialNo";
                        //        gridMessage.ForeColor = Color.Red;
                        //    }
                        //}
                        //else
                        //{

                    //    conn.ExecuteCommandProc("Lum_Erp_stp_UpdateGrnDetailsWithOutRespectToPurchaseOrder",
                    //        new SpParam("@grnId", _grnId, SqlDbType.Int),
                    //        new SpParam("@price", price, SqlDbType.Decimal),
                    //        new SpParam("@poId", _poId, SqlDbType.Int),
                    //        new SpParam("@lineId", lineId, SqlDbType.Int),
                    //         new SpParam("@itemId", itemId, SqlDbType.Int),
                    //        new SpParam("@quantity", quantity, SqlDbType.Int),
                    //        new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int));
                    //        InsertItemSerialNo(lineId, _grnId, itemId, serialNo, conn, quantity);
                    //        //gridMessage.Text = "Line updated";
                    //        gridMessage.Text = "";
                    //        gridMessage.ForeColor = Color.Green;
                    //    //}
                     
                    //}
                    //catch (FormatException)
                    //{
                    //    gridMessage.Text = "Invalid quantity or price.";
                    //    gridMessage.ForeColor = Color.Red;
                    //}
                }
        }
       BindGrid();
    }
    /// <summary>
    /// Add ItemSerial No during GRN
    /// </summary>
    /// <param name="polineId"></param>
    /// <param name="grnId"></param>
    /// <param name="itemId"></param>
    /// <param name="serialNo"></param>
    /// <param name="conn"></param>
    /// <param name="receiveQty"></param>
    public void InsertItemSerialNo(int grnlineId, int grnId, int itemId, string serialNo, IConnection conn,decimal receiveQty)
    {
        conn.ExecuteCommandProc("Lum_Erp_Stp_DeleteGrnItemSerialNo", new SpParam("@grnLineId", grnlineId, SqlDbType.Int),
                                      new SpParam("@grnId", grnId, SqlDbType.Int));
        
                                     

        if (serialNo != string.Empty)
        {
            string[] param = serialNo.Split(';');
            try
            {


                foreach (string str in param)
                {

                    conn.ExecuteCommandProc("Lum_Erp_Stp_InsertGendetailsSerialNumber", new SpParam("@grnLineId", grnlineId, SqlDbType.Int),
                                              new SpParam("@grnId", grnId, SqlDbType.Int),
                                               new SpParam("@itemId", itemId, SqlDbType.Int),
                                               new SpParam("@serialNo", str, SqlDbType.VarChar));

                }

            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }
        else
        {
            try
            {


                for (int _i = 0; _i < receiveQty; _i++)
                {
                    conn.ExecuteCommandProc("Lum_Erp_Stp_InsertGendetailsSerialNumber", new SpParam("@grnLineId", grnlineId, SqlDbType.Int),
                                          new SpParam("@grnId", grnId, SqlDbType.Int),
                                           new SpParam("@itemId", itemId, SqlDbType.Int),
                                           new SpParam("@serialNo", string.Empty, SqlDbType.VarChar));

                }

            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }
     
    }
    protected void poLineGrid_PreRender(object sender, EventArgs e)
    {
        for (int _index = 0; _index < poLineGrid.Items.Count; _index++)
        {
            Button btnupdate = (Button)poLineGrid.Items[_index].Cells[9].Controls[0];
            Button btndelete = (Button)poLineGrid.Items[_index].Cells[10].Controls[0];
            HtmlInputHidden _hdel = (HtmlInputHidden)poLineGrid.Items[_index].FindControl("isDeleted");

            if (_hdel.Value == "True")
            {
                btndelete.Visible = false;
                btnupdate.Visible = false;
            }


            TextBox _serialNo = (TextBox)poLineGrid.Items[_index].FindControl("itemSerialNo");
            int itemId = int.Parse(((HtmlInputHidden)poLineGrid.Items[_index].FindControl("itemId")).Value);
            int lineId = int.Parse(((HtmlInputHidden)poLineGrid.Items[_index].FindControl("lineId")).Value);   
           
            if (poLineGrid.Items[_index].Cells[0].Text.ToUpper() != "FINISHED GOODS")
            {
                _serialNo.Attributes.Add("readonly", "readonly");

            }
            //else
            //{

            //    _serialNo.Text = GetItemSerialNo(_grnId, itemId, lineId);
            //}
           


        }
    }
    /// <summary>
    /// Bind Serial Number Text Box in datagrid
    /// </summary>
    /// <param name="grnId"></param>
    /// <param name="itemId"></param>
    /// <param name="lineId"></param>
    /// <returns></returns>
    public string GetItemSerialNo(int grnId, int itemId, int lineId)
    {
        string retVal = "";

        string sql = string.Format(@"SELECT serialNo FROM Lum_Erp_GrndetailsSerialNumber WHERE grnLineId={0} AND grnId={1} AND  itemId={2} ", lineId, grnId, itemId);
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(sql))
            {
                while (reader.Read())
                {
                    if (retVal == "")
                    {
                        retVal = Convert.ToString(reader["serialNo"]);
                    }
                    else
                    {
                        retVal = retVal + ";" + Convert.ToString(reader["serialNo"]);
                    }
                }
            }
        }
        return retVal;

    }

    protected void ReadOnlyFiled()
    {
        grnDate.Enabled = false;
        poDate.Enabled = false;
        poNumber.Enabled = false;
        invoiceDate.Enabled = false;
        vendorInvoiceNo.Enabled = false;
        ddlStore.Enabled = false;
        rateContractDdl.Enabled = false;
        poTypeDdl.Enabled = false;
        ddlVendor.Enabled = false;
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        
            DateTime date = DateTime.Today;
            if (string.IsNullOrEmpty(poDate.Text))
            {
                poDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
            }
            else
            {
                date = DateTime.ParseExact(poDate.Text, "dd/MM/yyyy", null);
            }
            DateTime dtmGrnDate = Convert.ToDateTime(grnDate.Text);
            DateTime dtmInvoiceDate = Convert.ToDateTime(invoiceDate.Text);

            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                if (_grnId > 0)
                
                {
                    IDataReader _reader = conn.ExecuteQueryProc("Lum_Erp_Stp_UpdateGrnWithOutRespectToPurchaseOrder",
                                                 new SpParam("@grnId", _grnId, SqlDbType.Int),
                                                 new SpParam("@poId", _poId, SqlDbType.Int),
                                                  new SpParam("@strPoId", poNumber.Text, SqlDbType.VarChar),
                                                 new SpParam("@poTypeId", int.Parse(poTypeDdl.SelectedValue), SqlDbType.Int),
                                                 new SpParam("@vendorId", int.Parse(ddlVendor.SelectedValue), SqlDbType.Int),
                                                 new SpParam("@poDate", date, SqlDbType.DateTime),
                                                 new SpParam("@rateContractTypeId", int.Parse(rateContractDdl.SelectedValue), SqlDbType.Int),
                                                 new SpParam("@dateCreated", dtmGrnDate, SqlDbType.DateTime),
                                                 new SpParam("@vendorInvoiceNo", vendorInvoiceNo.Text, SqlDbType.VarChar),
                                                 new SpParam("@invoiceDate", dtmInvoiceDate, SqlDbType.DateTime),
                                                 new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int));

                    message.Text = "GRN   for Vendor Name " + ddlVendor.Items[ddlVendor.SelectedIndex].Text + " Updated";
                    message.ForeColor = Color.Green;


                }

                                 
                


            }
        }
    private void CalcTotalVat(string _vatAmount)
    {
        if (_vatAmount != "&nbsp;")
        {
            totalVatAmount += Double.Parse(_vatAmount);
        }
        
        
    }
    private void CalcTotalAmount(string _netVal)
    {
        if (_netVal != "&nbsp;")
        {
            totalAmount += Double.Parse(_netVal);
        }
        
    }
    protected void poLineGrid_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CalcTotalVat(e.Item.Cells[6].Text);
            CalcTotalAmount(e.Item.Cells[7].Text);
     
        }
        else if (e.Item.ItemType == ListItemType.Footer)
        {
            e.Item.Cells[5].Text = "Total";
            e.Item.Cells[6].Text = string.Format("{0:#,###.##}", Math.Round(totalVatAmount));
            e.Item.Cells[7].Text = string.Format("{0:#,###.##}", Math.Round(totalAmount));
        } 
    }

    protected void poLineGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            
            Button btnEdit = (Button)e.Item.Cells[9].Controls[0];
            Button btnDel = (Button)e.Item.Cells[10].Controls[0];
            btnDel.CssClass = "ButtonBig";
            btnEdit.CssClass = "ButtonBig";
            if (Action == "View")
            {
                btnEdit.Enabled = false;
                btnDel.Enabled = false;
            }
            if (GetLoggedinUser().UserType == 2)
            {

                btnEdit.Enabled = false;
                btnDel.Enabled = false;
            }
        }
    }
    protected void btnPrint_Click(object sender, EventArgs e)
    {
        if (_grnId > 1)
        {
            Response.Redirect("GRNPrint.aspx?GRNId=" + _grnId);
        }
        else
        {
            message.Text = "Create GRN before Print.";
            message.ForeColor = Color.Red;
        }
    }

    public void resetControl()
    {
        divisionDdl.SelectedIndex = -1;
        subDivisionDdl.SelectedIndex = -1;
        itemDdl.SelectedIndex = -1;
        quantity.Text = string.Empty;
        price.Text = string.Empty;
        itemDescription.Text = string.Empty;
    }
    protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlVendor.SelectedIndex != 0)
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                DataSet _ds = conn.GetDataSet(@"select vendorId,vendorName,(address1+ '  '+ address2) address
                                                from Lum_Erp_Vendor where vendorId = '" + ddlVendor.SelectedValue + "'", "GetAddress");


                if (_ds.Tables[0].Rows.Count != 0)
                {
                    txtvendoradd.Text = _ds.Tables[0].Rows[0]["address"].ToString();
                }


            }
        }

    }
}
