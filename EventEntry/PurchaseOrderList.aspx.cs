﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;

using Bitscrape.AppBlock.Database;

public partial class EventEntry_PurchaseOrderList : BasePage
{
    int poId = -1;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        BindGrid();
       
        //if (GetLoggedinUser().UserType == 2)
        //{

        //    newPo.Enabled = false;
        //}
    }

    private void BindGrid()
    {
        string chkcondition = " AND isDeleted =0  ";
        if (Chkblock_despath.Checked  == true )
            chkcondition = " AND isDeleted =1 ";




        chkcondition = @"select isBlocked, poId, poStateId,(strPoId +'/'+convert(varchar,poVersion)) as strPoId, isDeleted, CONVERT(VARCHAR,poDate,106) AS  poDate, v.vendorName
                            from Lum_Erp_PurchaseOrder po
                            inner join Lum_Erp_Vendor v on po.vendorId = v.vendorId and createdByUser=1 
                            WHERE po.poId >=1"
                +
                chkcondition
                +
                @"Order By Po.poDate DESC,V.vendorName ";

        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(chkcondition))
            {
                poList.DataSource = reader;
                poList.DataBind();
            }
        }
    }
    protected void newPo_Click(object sender, EventArgs e)
    {
        Response.Redirect("PurchaseOrder2.aspx");
    }
    protected void poList_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton btnview = (LinkButton)e.Item.Cells[3].Controls[0];
            LinkButton btnedit = (LinkButton)e.Item.Cells[4].Controls[0];
            LinkButton btn = (LinkButton)e.Item.Cells[5].Controls[0];


            btn.Attributes.Add("onclick", "return confirm('Are you sure you want to Block this?')");
            btn.CssClass = "GridLink";
            btnedit.CssClass = "GridLink";
            btnview.CssClass = "GridLink";

            if (GetLoggedinUser().UserType == 2)
            {
                btnedit.Visible = false;
                btn.Visible = false;
            }

        }

    }
    protected void poList_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        

       if (e.CommandName.Equals("View"))
       {
         poId = (int)poList.DataKeys[e.Item.ItemIndex];
         Response.Redirect("PurchaseOrder2.aspx?poId=" + poId + "&View=View");
       }        
       else if (e.CommandName.Equals("Edit"))
        {
            poId = (int)poList.DataKeys[e.Item.ItemIndex];

            Response.Redirect("PurchaseOrder2.aspx?poId=" + poId);

        }
        else if (e.CommandName.Equals("Delete"))
        {

            poId = (int)poList.DataKeys[e.Item.ItemIndex];
           
                try
                {
                    using (IConnection conn = DataConnectionFactory.GetConnection())
                    {
                        conn.ExecuteCommandProc("Lum_Erp_Stp_DeletePurchaseOrder", new SpParam("@poId", poId, SqlDbType.Int), new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int));

                        message.Text = "Purchase Order has been Blocked";
                        message.ForeColor = Color.Green;
                    }
                }
                catch(Exception ex)
                {
                    message.Text = ex.Message;
                    message.ForeColor = Color.Red;
                }


           
            BindGrid();
        }
              
        
    }

    protected void poList_PreRender(object sender, EventArgs e)
    {
        for (int _index = 0; _index < poList.Items.Count; _index++)
        {
            LinkButton btnview = (LinkButton)poList.Items[_index].Cells[3].Controls[0];
            LinkButton btnedit = (LinkButton)poList.Items[_index].Cells[4].Controls[0];
            LinkButton btn = (LinkButton)poList.Items[_index].Cells[5].Controls[0];

            HtmlInputHidden _hdel = (HtmlInputHidden)poList.Items[_index].FindControl("isdeleted");
            HtmlInputHidden _poStateId = (HtmlInputHidden)poList.Items[_index].FindControl("poStateId");
            
            if (_hdel.Value == "True")
            {
                btnedit.Visible = false;
                btn.Visible = false;
            }
            if (_poStateId.Value == "2")
            {
                btnedit.Visible = false;
                btn.Visible = false;
            }


        }
    }
    protected void btngo_Click(object sender, EventArgs e)
    {
        BindGrid();
    }
}
