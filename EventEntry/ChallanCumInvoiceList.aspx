﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EventEntry/EventEntry.master" AutoEventWireup="true" CodeFile="ChallanCumInvoiceList.aspx.cs" Inherits="EventEntry_ChallanCumInvoiceList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="PageMargin">
        <div class="PageHeading">Existing Challan-Cum-Invoice</div>
    <div class="Line"></div>
       
        <div class="PageContent" align="center">
        <table width "100%"><tr><td>
                 <table >
                        <tr><td>  <asp:RadioButtonList ID="rbChallanType" RepeatDirection="Horizontal" 
                                runat="server" CssClass="TextAreaLarge" >
                        <asp:ListItem Selected="True" Value="1">With Reference To SOIR</asp:ListItem>
                        <asp:ListItem Value="0">Without Reference To SOIR</asp:ListItem>
                        </asp:RadioButtonList> </td><td> <asp:Button ID="newChallan" 
                            runat="server" Text="Create New" CssClass="ButtonBig" onclick="newChallan_Click1" /></td></tr>
                  </table>
       </td> <td>
                   <table >
                        <tr><td>&nbsp;&nbsp;</td>
                        <td align="right">
                               &nbsp;
                               <asp:CheckBoxList ID="Chkblock_despath" runat="server" 
                                   RepeatDirection="Horizontal" CssClass="TextAreaLarge">
                                   <asp:ListItem Value="D">Show Despatched</asp:ListItem>
                                   <asp:ListItem Value="B">Show Blocked</asp:ListItem>
                                   <asp:ListItem Value="S" Selected="True">Show All</asp:ListItem>
                               </asp:CheckBoxList></td>
                        <td> 
                            <asp:Button ID="btngo" 
                            runat="server" Text="GO" CssClass="ButtonBig" onclick="btngo_Click"  /></td>       
                               </tr></table>
       </td></tr>
       </table>
 </div>
   <asp:Label ID="message" runat="server" />
   <div class="Line"></div>
       
         
         
               <asp:DataGrid ID="challanGrid" runat="server" 
            AutoGenerateColumns="false" DataKeyField="challanId" Width="100%"
            OnItemCommand="challanGrid_ItemCommand" HeaderStyle-CssClass="GridHeading"
            ItemStyle-CssClass="GridData" onitemcreated="challanGrid_ItemCreated" 
            GridLines="None" onprerender="challanGrid_PreRender"  >
            <Columns>
                <asp:BoundColumn DataField="strChallanId" HeaderStyle-HorizontalAlign="Left" HeaderText="Challan#" HeaderStyle-Font-Bold="true" />
                <asp:BoundColumn DataField="strSoirId" HeaderStyle-HorizontalAlign="Left" HeaderText="Soir#" HeaderStyle-Font-Bold="true" />
                <asp:BoundColumn DataField="customerName" HeaderStyle-HorizontalAlign="Left" HeaderText="Customer Name" HeaderStyle-Font-Bold="true" />
                 <asp:ButtonColumn ButtonType="LinkButton" HeaderStyle-HorizontalAlign="Left" CommandName="View" Text="View" ItemStyle-CssClass="GridLink" />
               
                 <asp:ButtonColumn ButtonType="LinkButton" HeaderStyle-HorizontalAlign="Left" CommandName="Edit" Text="Edit" ItemStyle-CssClass="GridLink" />
               
                <asp:ButtonColumn ButtonType="LinkButton" CommandName="Block" Text="Reverse" ItemStyle-CssClass="GridLink" />
                 <asp:TemplateColumn Visible="false">
              <ItemTemplate>
                <input type="hidden" id="isLocked" value='<%#DataBinder.Eval(Container.DataItem, "isDispatch") %>' runat="server" />
            </ItemTemplate>
            </asp:TemplateColumn>
                 <asp:TemplateColumn Visible="false">
              <ItemTemplate>
                <input type="hidden" id="isdeleted" value='<%#DataBinder.Eval(Container.DataItem, "isdeleted") %>' runat="server" />
            </ItemTemplate>
            </asp:TemplateColumn>
                        
             <asp:TemplateColumn >
              <ItemTemplate>
                <input type="hidden" id="Challantype" value='<%#DataBinder.Eval(Container.DataItem, "chType") %>' runat="server" />
            </ItemTemplate>
            </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
        </div>
        <!--Content Table End-->
        <div class="Line">
        </div>
        <!--Grid Table Start-->
     
        <!--Grid Table End-->
        <div class="Line">
        </div>
        <!--Page Body End-->
   
</asp:Content>

