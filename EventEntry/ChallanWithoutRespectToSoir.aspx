
<%@ Page Title="" Language="C#" Culture="en-GB" MasterPageFile="~/EventEntry/EventEntry.master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" EnableEventValidation="false" CodeFile="ChallanWithoutRespectToSoir.aspx.cs" Inherits="EventEntry_ChallanWithoutRespectToSoir" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">

      function letternumber(e) {
          var key;
          var keychar;

          if (window.event)
              key = window.event.keyCode;
          else if (e)
              key = e.which;
          else
              return true;
          keychar = String.fromCharCode(key);
          keychar = keychar.toLowerCase();

          // control keys
          if ((key == null) || (key == 0) || (key == 8) ||
    (key == 9) || (key == 13) || (key == 27))
              return true;

          // alphas and numbers
          else if ((("abcdefghijklmnopqrstuvwxyz0123456789;").indexOf(keychar) > -1))
              return true;
          else
              return false;
      }
      
        function CalculateVatAmount() {
        var txtPrice = document.getElementById('<%=price.ClientID%>');
        var maxprice = document.getElementById('<%=hmaxprice.ClientID%>');
        if (parseFloat(txtPrice.value) > parseFloat(maxprice.value))
        {
            alert("Price is greater than max price (" +  Math.round(maxprice.value) +")");
        }       
    }
     
      function round_decimals(original_number, decimals) {
         var result1 = original_number * Math.pow(10, decimals)
         var result2 = Math.round(result1)
         var result3 = result2 / Math.pow(10, decimals)
         return result3;

     }
     
</script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
<input type="hidden" name="soirId" value='<%=_soirId%>' />
<input type="hidden" name="challanId" value='<%=_challanId%>' />
<input type="hidden" name="isDispach" value='<%=_isDispach%>' />
  <div class="PageMargin">
        <div class="PageHeading">
       Challan-Cum-Invoice<a style="float:right" href="ChallanCumInvoiceList.aspx"> Challan-Cum-Invoice List</a></div>
        <div class="Line">
        </div>
        <!--Content Table Start-->
       <div class="PageContent">
          <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
                 <tr>
        <td width="16%" align="right" valign="top">
                Challan No.</td>
        <td align="left" valign="top" width="20%">
              <asp:Label ID="challanNumber" runat="server" />
            </td>
        <td width="16%" align="right" valign="top">
                        Raised By
                    </td>
        <td align="left" valign="top" width="16%">
                        <asp:Label ID="raisedBy" runat="server"   CssClass="TextboxSmall" />
                      
                      
                    </td>
     <td valign="top" align="right">
         Customer Order Type&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span> </td>
      <td  align="left" valign="top">
        <asp:DropDownList ID="ddlOrder" CssClass="TextboxSmall" runat="server">
        </asp:DropDownList>
        
         <br />
         <asp:RequiredFieldValidator InitialValue="0" ControlToValidate="ddlOrder" ValidationGroup="addItem" EnableClientScript="true" ErrorMessage="Select Order Type"
                            ID="RequiredFieldValidator8"  runat="server" />
        </td>
                </tr>
           

                 <tr>
                    <td width="16%" align="right" valign="top">
                      Customer Name&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span>
                    </td>
        <td  align="left" valign="top" colspan =3>
                          <asp:DropDownList CssClass="TextboxSmall"  ID="ddlCustomer" AutoPostBack="true"  runat="server" 
                            Width="100%" onselectedindexchanged="ddlCustomer_SelectedIndexChanged"   >
                     </asp:DropDownList>
                    <br />
                        <br />
                    <asp:RequiredFieldValidator ControlToValidate="ddlCustomer" ValidationGroup="addItem" EnableClientScript="true"  InitialValue="0"  ErrorMessage="Select Customer"
                            ID="RequiredFieldValidator4"  runat="server" />
                        
                      
                    </td>

                   <td valign="top" align="right">
                                              &nbsp;</td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>                      

                  
                </tr>
                 <tr>
        <td valign="top" align="right">Billing Address</td>
        <td valign="top" align="left"><asp:TextBox ID="billingAddress" runat="server" TextMode="MultiLine" Height="50px" CssClass="TextboxSmall"
             ReadOnly="True" /></td>
        <td valign="top" align="right">Installation Address</td>
        <td valign="top" align="left">
                        <asp:TextBox ID="installationAddress" TextMode="MultiLine" Height="50px" runat="server"   CssClass="TextboxSmall" />
                                    
                        
                      
                                </td>  
                    
                                     
   
                 <td valign="top" align="right">
                       Customer Segment
                    </td>
                 <td  align="left" valign="top">
                        <asp:Label ID="CustomerSegment"   runat="server"    />
                     
                          
                        
                      
                    </td>
                    
                </tr>
           
                 <tr >
                <td valign="top" align="right">Customer Order No.&nbsp;
                    </td>
                    <td align="left" valign="top">
                        <asp:TextBox ID="poReference" runat="server"   CssClass="TextboxSmall" MaxLength="50" />
                       <br />
                        
                      
                    </td>
                    <td valign="top" align="right">
                        Customer Order Date(dd/mm/yyyy)&nbsp;&nbsp;
                    </td>
                    <td  align="left" valign="top">
                        <asp:TextBox ID="poDate" runat="server" onkeyup="document.getElementById(this.id).value=''" autocomplete='off' CssClass="TextboxSmall" />
                                            
                      <cc1:CalendarExtender ID="CalendarExtender4" runat="server" Format="dd/MM/yyyy" 
                            TargetControlID="poDate">
                        </cc1:CalendarExtender>
                      <br />
                    </td>
                                     
                                   

               
                
     <td valign="top" align="right">
       Transporter&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
          <td  align="left" valign="top">
        <asp:DropDownList ID="ddlTransporter" runat="server" CssClass="TextboxSmall">
        </asp:DropDownList> <br />
         <asp:RequiredFieldValidator InitialValue="0"  ValidationGroup="addItem" ControlToValidate="ddlTransporter" ErrorMessage="Select Transporter"
                            ID="RequiredFieldValidator1"  runat="server" /></td>
    </tr>
                 <tr>
                         <td valign="top" align="right">
                      Primary Sales Person 
                    </td>
                    <td  align="left" valign="top">
                        <asp:DropDownList CssClass="TextboxSmall"  ID="ddlprimary"  runat="server" 
                             >
                     </asp:DropDownList>
                   </td>
                    <td valign="top" align="right">
                         Aditional Sales Person
                    </td>
                    <td  align="left" valign="top">
                    <asp:DropDownList CssClass="TextboxSmall"  ID="ddlAditional"  runat="server" 
                            >
                     </asp:DropDownList>
                     </td>
    <td valign="top" align="right">  Store Location&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
           <td  align="left" valign="top">
     <asp:DropDownList ID="ddlStorelocation" runat="server" CssClass="TextboxSmall">
        </asp:DropDownList> <br />
         <asp:RequiredFieldValidator InitialValue="0"  ValidationGroup="addItem" ControlToValidate="ddlStorelocation" ErrorMessage="Select Store"
                            ID="RequiredFieldValidator10"  runat="server" /></td>
                </tr>
      <tr>
      
       <td valign="top" align="right">
     C/N Details</td>
      <td  align="left" valign="top">
        <asp:TextBox ID="txtCNDetails" TextMode="MultiLine"  runat="server" CssClass="TextboxSmall" 
                             Height="50px"></asp:TextBox></td>
            
    <td colspan="2" valign="top" align="right" >
        <asp:RadioButtonList ID="rbTaxType" AutoPostBack="true" runat="server" 
            RepeatDirection="Horizontal" 
            onselectedindexchanged="rbTaxType_SelectedIndexChanged" 
            CssClass="TextAreaLarge"  >
            <asp:ListItem Selected="True" Value="1">VAT</asp:ListItem><asp:ListItem Value="2">CST</asp:ListItem><asp:ListItem Value="3">CST Against Form</asp:ListItem></asp:RadioButtonList></td>
            
    <td valign="top" align="right">
      Form Recoverable</td>
    <td valign="top" align="left">
      <asp:DropDownList ID="dllformrecoveralbe" Enabled="false" runat="server" 
            CssClass="TextboxSmall">
        </asp:DropDownList> 
        </td>
    </tr>    
     <tr>
            <td colspan="6">
                <asp:Label ID="lblMsg" runat="server"></asp:Label>
            </td>
            </tr>
          </table>
    <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
    <tr><td  align="right" valign="top">Packing/Forwarding/Insurance Charge</td>
        <td align="left"><asp:TextBox ID="txtCharge" runat="server"  MaxLength="10" CssClass="TextboxSmall"
             onkeypress="return isNumberKey(event);" /><br />
            <asp:RangeValidator ID="RangeValidator2" ControlToValidate="txtCharge" runat="server"
             ErrorMessage="Please give correct decimal number" Type="Double" MaximumValue="1000000"></asp:RangeValidator></td>
        <td  align="right" valign="top">Buy back amount</td>
        <td  align="left" valign="top"><asp:TextBox ID="txtBuyBackamount" runat="server" MaxLength="10" CssClass="TextboxSmall"
             onkeypress="return isNumberKey(event);" /><br />
            <asp:RangeValidator ID="RangeValidator1" ControlToValidate="txtBuyBackamount" runat="server"
             ErrorMessage="Please give correct decimal number" Type="Double" MaximumValue="1000000"></asp:RangeValidator></td>
        <td align="right" valign="top">Buy back Details</td>
        <td  align="left" valign="top"><asp:TextBox ID="txtDetails" TextMode="MultiLine" Height="50px" runat="server" CssClass="TextboxSmall"/></td>
    </tr></table>
              
          </div>
          <div class="Line">
          
        </div>
        <div class="PageHeading">Add New Customer Order Details</div>
       <div class="Line">
       
       
        </div>
          <!-- the po line input table -->
          
    <div class="PageContent">
        <div id="tblItem" runat="server">
        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
       
          <tr>
          <td width="19%" align="center" valign="middle">
                Division&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span>
            </td>
             <td width="10%" align="center" valign="middle">SubDivision&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span>
            </td>
           <td width="10%" align="center" valign="middle">Item&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span>
            </td>
           <td width="20%" align="center" valign="middle">
                Item Description
            </td>
            <td width="14%" align="center" valign="middle">
                Quantity&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span>
            </td>
            <td width="9%" align="center" valign="middle">
               Item type FOC
            </td>
           <td width="9%" align="center" valign="middle">
                Price&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span>
            </td>
           <td width="9%" align="center" valign="middle">
                Discount&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span>
            </td>
               
            <td>
                Serial No</td>
        </tr>
         <tr>
            <td>
                <asp:DropDownList ID="divisionDdl" runat="server" CssClass="TextboxSmall" />
            </td>
            <td>
                <asp:DropDownList ID="subDivisionDdl" runat="server" CssClass="TextboxSmall"></asp:DropDownList></td><td>
                <asp:DropDownList ID="itemDdl" runat="server" AutoPostBack="true" 
                     OnSelectedIndexChanged="itemDdl_SelectedIndexChanged" CssClass="TextboxSmall" ></asp:DropDownList></td><td>
                     
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" 
                     RenderMode="Inline">
                     <ContentTemplate>
                        <asp:TextBox ID="itemDescription" runat="server" ReadOnly="True" 
                        TextMode="MultiLine"></asp:TextBox>
                <br />
                  <asp:Label ID="lblAvailable" runat="server" />                        
                   <input type="hidden" value="0" runat="server" id="hmaxprice" />
                   <input type="hidden" value="0" runat="server" id="hvat" />                        
                </ContentTemplate>        
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="itemDdl" EventName="SelectedIndexChanged" />
                </Triggers>
                </asp:UpdatePanel>  

               
            </td>
            <td>
                <asp:TextBox ID="txtquantity" runat="server" MaxLength="6" Width="50" />
                &nbsp;<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtquantity" ValidChars="1234567890." FilterMode="ValidChars" FilterType="Custom"></cc1:FilteredTextBoxExtender>
                <asp:Label ID="uom" runat="server" />
            </td>
              <td>
                <asp:CheckBox ID="chkItemType" AutoPostBack="true" runat="server" 
                oncheckedchanged="chkItemType_CheckedChanged" />
            </td>
            <td>
                <asp:TextBox ID="price" runat="server" MaxLength="10" onblur="CalculateVatAmount();" Width="60" />
                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="price" 
                    ValidChars="1234567890." FilterMode="ValidChars" FilterType="Custom"></cc1:FilteredTextBoxExtender>    
            </td>
            <td>
                  <asp:TextBox ID="discount" MaxLength="10" runat="server" Width="60" />          
            </td>
            <td>
                  <asp:TextBox ID="txtserialNo" TextMode="MultiLine"  runat="server" />          
            </td>  
            <td>
                <asp:Button ID="addItemLine" runat="server" Text="Add"
                    ValidationGroup="addItem" CausesValidation="true" 
                    onclick="addItemLine_Click" style="height: 26px" CssClass="ButtonBig" />
            </td>
            
            <cc1:CascadingDropDown ID="CascadingDropDown3"
            runat="server" 
            TargetControlID="divisionDdl" LoadingText="[Loading...]"
            Category="Division"
            PromptText="Select a Division"
            ServicePath ="~/divisionService.asmx"
            ServiceMethod="GetDivision" >   </cc1:CascadingDropDown>
              
           <cc1:CascadingDropDown ID="CascadingDropDown1"
                runat="server" ParentControlID = "divisionDdl" 
                TargetControlID="subDivisionDdl" LoadingText="[Loading...]"
                Category="subDivision"
                PromptText="Select a subDivision"
                ServicePath ="~/divisionService.asmx"
                ServiceMethod="GetSubdividsionForDivision" >   </cc1:CascadingDropDown>

          <cc1:CascadingDropDown ID="CascadingDropDown2"
                runat="server" ParentControlID = "subDivisionDdl" 
                TargetControlID="itemDdl" LoadingText="[Loading...]"
                Category="Item"
                PromptText="Select Item"
                ServicePath ="~/divisionService.asmx"
                ServiceMethod="GetItemForSubdividsion" >   </cc1:CascadingDropDown>
        </tr>
        
        <asp:Label ID="Label1" runat="server" /><tr>
            <td>
                 <asp:RequiredFieldValidator InitialValue="0" ValidationGroup="addItem" EnableClientScript="true" ControlToValidate="divisionDdl" ErrorMessage="Select Division"
                            ID="divisionRequired"  Display="Dynamic" runat="server" />
            </td>
            <td>
                 <asp:RequiredFieldValidator InitialValue="0" ValidationGroup="addItem" EnableClientScript="true" ControlToValidate="subDivisionDdl" ErrorMessage="Select SubDivision"
                            ID="subDivisionRequired"  Display="Dynamic" runat="server" />
            </td>
            <td>
                   <asp:RequiredFieldValidator InitialValue="0" ValidationGroup="addItem" EnableClientScript="true" ControlToValidate="itemDdl" ErrorMessage="Select Item"
                            ID="itemRequired"  Display="Dynamic" runat="server" />
            </td>
            <td colspan="2">
                <asp:RequiredFieldValidator ID="quantityRequired" runat="server" EnableClientScript="true"
                    Display="Dynamic" ValidationGroup="addItem" ErrorMessage="Quantity cannot be empty."
                    Text="" ControlToValidate="txtquantity" />
                <br />
                <%--<asp:RegularExpressionValidator ID="quanityRegexValidator" runat="server" EnableClientScript="true"
                    Display="none" ValidationGroup="addItem" ErrorMessage="Quantity must be a integer"
                    Text="" ControlToValidate="txtquantity" ValidationExpression="\d+" />--%>
            </td>
            <td colspan="2">
                <asp:RequiredFieldValidator ID="priceRequired" runat="server" EnableClientScript="true"
                    Display="Dynamic" ValidationGroup="addItem" ErrorMessage="Price cannot be empty."
                    Text="" ControlToValidate="price" />
                <br />
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" EnableClientScript="true"
                    Display="Dynamic" ValidationGroup="addItem" ErrorMessage="Invalid price"
                    Text="" ControlToValidate="price" ValidationExpression="^\d*\.?\d{2}$" />
                    
            </td>
            <td>
              
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" EnableClientScript="true"
                    Display="Dynamic" ValidationGroup="addItem" ErrorMessage="Invalid discount"
                    Text="" ControlToValidate="discount" ValidationExpression="^\d*\.?\d{2}$" />
            </td>
     
            <td>&nbsp;
               </td>
                <td>&nbsp;
                </td></tr></table>
         <!-- over here we should the bottom grid where the line details of this purchase order are shown --><!--Grid Table Start-->
         </div>
         </div>
         
    
   <div class="Line">
          
        &nbsp;</div>
       <div class="PageHeading">Purchase Order Items</div>
       <div class="Line">
       
       
        </div>
         <div align="right">Serial Number will be  separated by (;)</div>
    <asp:DataGrid ID="poLineGrid" runat="server" AutoGenerateColumns="false" 
                        DataKeyField="soirLineId" GridLines="None" Width="100%"
     FooterStyle-CssClass="GridData" HeaderStyle-CssClass="GridHeading" 
                        ItemStyle-CssClass="GridData" onitemdatabound="poLineGrid_ItemDataBound" 
     onprerender="poLineGrid_PreRender" onitemcommand="poLineGrid_ItemCommand" >
        <Columns>
            <asp:BoundColumn DataField="divisionName" HeaderText="Division" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true" />
            <asp:BoundColumn DataField="subdivisionName" HeaderText="Sub Division" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true" />
            <asp:TemplateColumn HeaderText="Item Description" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Left"
             ItemStyle-HorizontalAlign="Left">
                <ItemTemplate>
                <asp:TextBox ID="itemDescription0" ReadOnly ="true" height = "50px" Width="175px" 
                        TextMode="MultiLine" runat="server"
                 Text='<%# DataBinder.Eval(Container.DataItem, "itemDescription") %>' 
                        CssClass="Textbox2ExtraSmall_wo_border"  />
                <asp:Label ID="QtyAvlInSTore" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "QtyAvlInSTore") %>' Width="60" />
                 
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" Font-Bold="True" Width="75px"></HeaderStyle>
                <ItemStyle HorizontalAlign="Left"></ItemStyle>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="FOC" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                <ItemTemplate>
                  <%--<asp:Label ID="foc" runat="server" Enabled="false"
                        Text='<%# DataBinder.Eval(Container.DataItem, "itemType") %>' Width="60" />--%>
                <asp:CheckBox Enabled="false" runat="server" ID="foc" Checked ='<%# DataBinder.Eval(Container.DataItem, "itemType") %>' />
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" Font-Bold="True"></HeaderStyle>
                <ItemStyle HorizontalAlign="Left"></ItemStyle>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Quantity" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true">
                <ItemTemplate>
                <input type="hidden" id="itemId" 
                        value='<%# DataBinder.Eval(Container.DataItem, "itemId") %>' runat="server"  />
                    <asp:Label ID="itemQty" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "quantity") %>' Width="50" />
                          <%#DataBinder.Eval(Container.DataItem, "uomName")%>
                

                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Price" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true">
                <ItemTemplate>
                <asp:Label ID="itemPrice0" runat="server" 
                        Text='<%# DataBinder.Eval(Container.DataItem, "price") %>' Width="60" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Discount" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true">
                <ItemTemplate>
                <asp:Label ID="itemDiscount" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "discount") %>' Width="60" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderStyle-HorizontalAlign="Left"  HeaderStyle-Font-Bold="true">
                <ItemTemplate>
                <asp:Label ID="lblTxtType" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "taxType") %>' Width="60" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Serial No" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true">
                <ItemTemplate>
                <asp:TextBox ID="itemSerialNo" EnableViewState="true" Text='<%# DataBinder.Eval(Container.DataItem, "SerialNo") %>'
                 runat="server" onkeypress="return letternumber(event);"  Width="100" TextMode="MultiLine" Height="50" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="total" HeaderStyle-HorizontalAlign="Left"  HeaderText="Total" HeaderStyle-Font-Bold="true"
             DataFormatString="{0:#,###.##}" />
            <asp:ButtonColumn ButtonType="PushButton"  Visible = "false" Text="Update" CommandName="Edit" ItemStyle-Width="10%" />
            <asp:TemplateColumn Visible="false">
                <ItemTemplate>
                <input type="hidden" id="isDeleted" value='<%#DataBinder.Eval(Container.DataItem, "isDeleted") %>' runat="server" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn >
                <ItemTemplate>
                <input type="hidden" id="soirLineId" value='<%#DataBinder.Eval(Container.DataItem, "soirLineId") %>' runat="server" />
                </ItemTemplate>
            </asp:TemplateColumn>  
            
           <asp:ButtonColumn ButtonType="PushButton" Text="Delete" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%" CommandName="Delete" />
          <asp:TemplateColumn Visible="false">
                <ItemTemplate>
                <input type="hidden" id="isLocked" value='<%#DataBinder.Eval(Container.DataItem, "isDispatch") %>' runat="server" />
                </ItemTemplate>
            </asp:TemplateColumn>
                    
        </Columns>
    </asp:DataGrid>
     
     <div align="center">
    <asp:Label ID="gridMessage" runat="server" />
 </div>
  
            
            <div class="PageContent">  
              <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
              <tr><td class="GridData" align="right"> Total Quantity :   <asp:Label ID="lblquantity" runat="server"></asp:Label></td><td class="GridData">
                     </td><td width="50px" class="GridData" align="right">Total</td><td  class="GridData" align="left">
                           <asp:Label ID="lblTotal" runat="server"></asp:Label></td></tr>
                           <tr><td align="right"  class="GridData" width="30%">Price in Words</td><td align="left"  class="GridData" colspan="3"><asp:Label ID="lblPrice" runat="server"></asp:Label> </td></tr></table>
                     
                     </div>
         
          <div class="Line">
            
           
               </div>
         
      </div>

      
        
       <div class="PageHeading"> Installment Dates</div>
        <div class="Line">
              </div>
        <div class="PageContent">
<table width="400" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
    <tr>
        <td  width="50%" align="left" valign="middle" >Date(dd/mm/yyyy)</td>
        <td width="30%">Value&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
        <td width="20%"><asp:Button ID="addChallan" runat="server" CssClass="ButtonBig" Text="Add" Width="78px" onclick="addChallan_Click" /></td>
    </tr>

    <tr>
        <td colspan="3">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional" 
            RenderMode="Inline">
            <ContentTemplate>    
            <table width="400" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
                <tr>
                    <td align="left" valign="top"><asp:TextBox ID="txtDate" CssClass="TextboxSmall"  runat="server"></asp:TextBox></td>
                        <td valign="top"><asp:TextBox ID="txtinstAmt"  CssClass="TextboxSmall" 
                                runat="server" onkeypress="return isNumberKey(event);"></asp:TextBox></td>
                        <td  width="20%">&nbsp;</td>
                </tr>
                <tr>
                        <td valign="top"  > 
                            <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="txtDate"></cc1:CalendarExtender> </td>
                        <td valign="top">
                            <asp:RegularExpressionValidator ID="priceRegexValidator" runat="server" ErrorMessage="Invalid Installment Amount"
                             ValidationGroup="addinstallment" ControlToValidate="price" ValidationExpression="^\d*\.?\d{2}$" /></td>
                </tr>
                <tr>
                    <td colspan="2" valign="top"> 
     
                        <asp:DataGrid id="DataGrid1" runat="server" Width="785px" BorderColor="#ffffff" AutoGenerateColumns="False" GridLines="None"
         FooterStyle-CssClass="GridData" HeaderStyle-CssClass="GridHeading" ItemStyle-CssClass="GridData" BorderStyle="Inset" BorderWidth="2px" 
         CellPadding="2" CellSpacing="2" EditItemStyle-BackColor="#ff6600" EditItemStyle-ForeColor="black">
            <Columns>
                <asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Edit"></asp:EditCommandColumn>
                <asp:TemplateColumn HeaderText="Challan Id" Visible="false">
                    <ItemTemplate>
                    <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.id") %>' ID="Label1"></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.id") %>' ID="Textbox1"></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Installment Date">
                    <ItemTemplate>
                    <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.installmentDate") %>' ID="Label2">
                    </asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                    <asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.installmentDate") %>' ID="Textbox2">
                    </asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Installment Amount">
                    <ItemTemplate>
                    <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.installmentamt") %>' ID="Label3">
                    </asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                    <asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.installmentamt") %>' ID="Textbox3"
                     onkeypress="return isNumberKey(event);">
                    </asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>   
                        
                    </td>
                 </tr>
 
            </table>
                </ContentTemplate>        
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="addChallan" EventName="Click" />
                </Triggers>
                </asp:UpdatePanel>              
         </td>
    </tr>
    <tr>

        <td colspan="3">
        <table><tr>
        <td align="center">
            <asp:Button ID="btncreate" runat="server" CssClass="ButtonBig" 
                Text="Generate/Update Challan" onclick="btncreate_Click" Visible="False" /></td>
        <td><asp:Button ID="btnDispatch" runat="server" CssClass="ButtonBig" Text="Despatch" onclick="btnDispatch_Click" /></td>
         </tr></table>   
      </td>
  </tr>
      <tr>

        <td colspan="3"><span id="errmsg" runat="server" >
            </asp:Label></span></td>
        </tr>
 </table>
    
<table>
     <tr><td  align="center"> 
            <asp:Button ID="btnPrintChallan" runat="server" Width="100px" 
            CssClass="Button" Text="Print Challan" onclick="btnPrintChallan_Click"/> 
    </TD>
    <td colspan="2" align="center">      
            <asp:RadioButtonList CssClass="TextAreaLarge" ID="RbtnSelectInvoice" runat="server" 
                RepeatDirection="Horizontal">
                <asp:ListItem Selected="True" Value="1">Org.Buyers Copy</asp:ListItem>
                <asp:ListItem Value="2">Dupl.Sellers Copy</asp:ListItem>
                <asp:ListItem Value="3">Extra</asp:ListItem>
            </asp:RadioButtonList>
            &nbsp;&nbsp;
            <asp:Button ID="btnPrintInvoice" runat="server" Width="100px" 
            CssClass="Button" Text="Print Invoice" onclick="btnPrintInvoice_Click"/>
            
            </td>
            </tr>
          </table>
           
       </div>
        <div class="Line">
        
              </div>
              
           
              
      
            
       
        <!--Page Body End-->

        <input type="hidden" value="0" runat="server" id="strsoirId" />

</asp:Content>

