<%@ Page Language="C#" MasterPageFile="~/EventEntry/EventEntry.master" AutoEventWireup="true" CodeFile="IncompleteItem.aspx.cs" Inherits="EventEntry_IncompleteItem" Title="Incomplete Item List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="PageMargin">
    <div class="PageHeading">Incomplete Item List</div>
    <div class="Line"></div>
    <!--Content Table Start-->
    <div class="PageContent" align="center">
    <asp:Label ID="message" runat="server" /><br /><br />
        <asp:GridView CellPadding="0"  GridLines="None" ID="gvDetach" runat="server" AutoGenerateColumns="false" Width="70%">
            <HeaderStyle CssClass="GridHeading" HorizontalAlign="Center" />
            <RowStyle CssClass="GridData" HorizontalAlign="Center"/>
            <Columns>
                <asp:TemplateField HeaderText="Division" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <asp:Label ID="lblDiv" runat="server" Text ='<%#DataBinder.Eval(Container.DataItem,"divisionName") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Sub Division" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <asp:Label ID="lblSDiv" runat="server" Text ='<%#DataBinder.Eval(Container.DataItem,"subdivisionName") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Item Code" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <asp:Label ID="lblItem" runat="server" Text ='<%#DataBinder.Eval(Container.DataItem,"itemCode") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Serial No." HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <asp:Label ID="lblSNo" runat="server" Text ='<%#DataBinder.Eval(Container.DataItem,"serialNo") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField  HeaderText="Classes" ItemStyle-Width="15%" HeaderStyle-Width="15%">
                <ItemTemplate>
                    <asp:HyperLink ID="hplItem" runat="server" Text="Edit"
                     NavigateUrl='<%#"Detachment.aspx?div=" + DataBinder.Eval(Container.DataItem,"divisionId") + "&sub=" + DataBinder.Eval(Container.DataItem,"subdivisionId") + "&item=" + DataBinder.Eval(Container.DataItem,"itemId") + "&sno=" + DataBinder.Eval(Container.DataItem,"Id")%>'></asp:HyperLink>
                </ItemTemplate>
                </asp:TemplateField>
             </Columns>
            <PagerSettings Position="TopAndBottom" />
            <PagerStyle Font-Bold="False" HorizontalAlign="Right" />
        </asp:GridView>
    </div>
    <!--Content Table End-->
    <div class="Line"></div>
    <!--Grid Table Start--><!--Grid Table End-->
    <div class="Line"></div>
    <!--Page Body End-->
    </div>
</asp:Content>

