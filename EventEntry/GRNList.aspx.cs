﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;

public partial class EventEntry_GrnList : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        BindGrid();
       
        //if (GetLoggedinUser().UserType == 2)
        //{
        //    newGrn.Enabled = false;
             
        //}
    }

    private void BindGrid()
    {
        string chkcondition = " AND Grn.isDeleted =0  ";
        if (Chkblock_despath.Checked  == true )
            chkcondition = " AND Grn.isDeleted =1 ";

        chkcondition = @"select grnId,strGrnId,Grn.poId,vendorInvoiceNo,CONVERT(VARCHAR,invoiceDate,106) AS invoiceDate,
                        storeId,grnVersion,CONVERT(VARCHAR,dateCreated,106) AS dateCreated,Grn.isDeleted,strPoId
                        ,isLockedgrn as isLocked,(grn.strGrnId +'/'+convert(varchar,grn.grnVersion)) as strGrn,  (po.strPoId +'/'+convert(varchar,po.poVersion)) as strPoId  ,po.createdByUser
                            from Lum_Erp_Grn Grn
                            left outer join Lum_Erp_PurchaseOrder po on grn.poId = po.poId  
                            WHERE Grn.grnId >=1"
                +
                chkcondition
                +
                @"order by Grn.dateCreated,grnId ";

        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(chkcondition))
            {

               grnList.DataSource = reader;
                grnList.DataBind();
            }
        }
    }

    protected void newGrn_Click(object sender, EventArgs e)
    {
        if (rbGrnType.SelectedValue == "1")
        {
            Response.Redirect("GrnWithRefToPo.aspx");
        }
        else
        {
            Response.Redirect("GrnWithoutRefToPo.aspx");
            
        }
    }
    protected string GetUrl(object type,object grnId)
    {
        string retVal = "";
        bool isFlag = Convert.ToBoolean(type);
        if (isFlag)
        {
            retVal = "GrnWithRefToPo.aspx?grnId=" + grnId;
        }
        else
        {
            retVal = "GrnWithoutRefToPo.aspx?grnId=" + grnId;
        }
        return retVal;
        
    }
    protected string GetUrlView(object type, object grnId)
    {
        string retVal = "";
        bool isFlag = Convert.ToBoolean(type);
        if (isFlag)
        {
            retVal = "GrnWithRefToPo.aspx?grnId=" + grnId+"&View=View";
        }
        else
        {
            retVal = "GrnWithoutRefToPo.aspx?grnId=" + grnId + "&View=View"; 
        }
        return retVal;
        
    }

    protected void grnList_ItemCommand(object source, DataGridCommandEventArgs e)
    {
       

        if (e.CommandName.Equals("Delete"))
        {
            int grnId = int.Parse(((HtmlInputHidden)e.Item.FindControl("grnId")).Value);
            int poId = int.Parse(((HtmlInputHidden)e.Item.FindControl("poId")).Value);

            try
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    conn.ExecuteCommandProc("Lum_Erp_Stp_DeleteGrnAndPurchaseOrder",
                    new SpParam("@grnId", grnId, SqlDbType.Int),
                    new SpParam("@poId", poId, SqlDbType.Int),
                    new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int));

                    message.Text = "GRN has been Blocked";
                    message.ForeColor = Color.Green;
                }
            }
            catch(Exception ex)
            {
                message.Text = ex.Message;
                message.ForeColor = Color.Red;
            }



            BindGrid();
        }


        
    }
    protected void grnList_ItemCreated(object sender, DataGridItemEventArgs e)
    {
      
                        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                        {
                           
                            LinkButton btn = (LinkButton)e.Item.Cells[7].Controls[0];
                            btn.Attributes.Add("onclick", "return confirm('Are you sure you want to Block this?')");
                            btn.CssClass = "GridLink";
                            if (GetLoggedinUser().UserType == 2)
                            {
                             
                               e.Item.Cells[6].Controls[0].Visible=false;
                                btn.Visible = false;
                            }
                        }
                   
        }


    protected void btngo_Click(object sender, EventArgs e)
    {
        BindGrid();
    }
    protected void grnList_PreRender(object sender, EventArgs e)
    {
        for (int _index = 0; _index < grnList.Items.Count; _index++)
        {
            DataBoundLiteralControl btnview = (DataBoundLiteralControl)grnList.Items[_index].Cells[5].Controls[0];
            DataBoundLiteralControl btnedit = (DataBoundLiteralControl)grnList.Items[_index].Cells[6].Controls[0];
            LinkButton btn = (LinkButton)grnList.Items[_index].Cells[7].Controls[0];

            HtmlInputHidden _hdel = (HtmlInputHidden)grnList.Items[_index].FindControl("isdeleted");
            if (_hdel.Value == "True")
            {
                btnedit.Visible = false;
                btn.Visible = false;
            }
            HtmlInputHidden _hlock = (HtmlInputHidden)grnList.Items[_index].FindControl("isLocked");
            if (_hlock.Value == "True")
            {
                btnedit.Visible = false;
                btn.Visible = false;
            }
        }
    }
}
