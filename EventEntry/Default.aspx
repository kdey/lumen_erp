﻿<%@ Page Language="C#" MasterPageFile="~/Reports/Reports.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="EventEntry_Default" Title="Untitled Page" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
    Font-Size="8pt" Height="400px" Width="645px">
    <LocalReport ReportPath="Reports\Rpt_SalesRegister.rdlc" 
            EnableExternalImages="True" EnableHyperlinks="True">
        <DataSources>
            <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" 
                Name="DataSet2_Lum_Erp_Stp_GetPOforPrint" />
        </DataSources>
    </LocalReport>
</rsweb:ReportViewer>
<asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
    SelectMethod="GetData" 
    TypeName="DataSet2TableAdapters.Lum_Erp_Stp_GetPOforPrintTableAdapter">
</asp:ObjectDataSource>
</asp:Content>

