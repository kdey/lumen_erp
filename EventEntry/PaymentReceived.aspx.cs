﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;

public partial class EventEntry_PaymentReceived : BasePage
{
    protected int _payId = -1;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!this.IsPostBack)
        {
            bindControl();
        }
        else
        {
         
        }
        //   // addItemLine.Enabled = false;
        //   // btnDispatch.Enabled = false;
        //rbtnCash.Checked = false;
        //rbtnCheque.Checked = true;
        //    txtbankbranch.ReadOnly = true;
        //    txtBank.ReadOnly = true;
        //    txtchqdate.ReadOnly = true;
        //    txtchqNo.ReadOnly = true;
    }

    protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
    {

        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            SpParam _spcId = new SpParam("@CustomerId", ddlCustomer.SelectedValue, SqlDbType.Int);
            SpParam _spitype = new SpParam("@ChallanTypeId", ddlInvoiceType.SelectedValue, SqlDbType.Int);
            SpParam _cid = new SpParam("@challanId", DBNull.Value, SqlDbType.Int);

            DataSet _Ds = conn.GetDataSet("Lum_Erp_Stp_GetInvoice_for_Payment", _spcId, _spitype, _cid);

            ddlInvoiceNo.ClearSelection();
            ddlInvoiceNo.Items.Clear();
            ddlInvoiceNo.AppendDataBoundItems = true;
            ddlInvoiceNo.Items.Add("Please Select");
            ddlInvoiceNo.Items[0].Value = "0";
            ddlInvoiceNo.DataTextField = _Ds.Tables[0].Columns["strServiceId"].ToString();
            ddlInvoiceNo.DataValueField = _Ds.Tables[0].Columns["ChallanId"].ToString();
            ddlInvoiceNo.DataSource = _Ds;
            ddlInvoiceNo.DataBind();

       }
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            IDataReader _reader = conn.ExecuteQuery(string.Format(@"select segmentType, Billing_address1+' '+Billing_address2  as billingAddress,Shipping_address1+' '+Shipping_address2 as 
                            shippingAddress from Lum_Erp_Customer C , Lum_Erp_CustomerSegment S 
                            WHERE S.segmentId=C.customerSegmentId and customerId = " + Convert.ToInt32(ddlCustomer.SelectedValue)));
            if (_reader.Read())
            {
                billingAddress.Text = Convert.ToString(_reader["billingAddress"]);
                installationAddress.Text = Convert.ToString(_reader["shippingAddress"]);
                CustomerSegment.Text = Convert.ToString(_reader["segmentType"]);
            }
        }

    }
    public void bindControl()
    {
        BindListControl _control = new BindListControl();
         _control.BindCustomerAll(ddlCustomer);
      }

    protected void ddlInvoiceType_SelectedIndexChanged(object sender, EventArgs e)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            SpParam _spcId = new SpParam("@CustomerId", ddlCustomer.SelectedValue, SqlDbType.Int);
            SpParam _spitype = new SpParam("@ChallanTypeId", ddlInvoiceType.SelectedValue, SqlDbType.Int);
            SpParam _cid = new SpParam("@challanId", DBNull.Value, SqlDbType.Int);

            DataSet _Ds = conn.GetDataSet("Lum_Erp_Stp_GetInvoice_for_Payment", _spcId, _spitype, _cid);
            ddlInvoiceNo.ClearSelection();
            ddlInvoiceNo.Items.Clear();
            ddlInvoiceNo.AppendDataBoundItems = true;
            ddlInvoiceNo.Items.Add("Please Select");
            ddlInvoiceNo.Items[0].Value = "0";
            ddlInvoiceNo.DataTextField = _Ds.Tables[0].Columns["strServiceId"].ToString();
            ddlInvoiceNo.DataValueField = _Ds.Tables[0].Columns["ChallanId"].ToString();
            ddlInvoiceNo.DataSource = _Ds;
            ddlInvoiceNo.DataBind();
        }
    }
    protected void ddlInvoiceNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            SpParam _spcId = new SpParam("@CustomerId", ddlCustomer.SelectedValue, SqlDbType.Int);
            SpParam _spitype = new SpParam("@ChallanTypeId", ddlInvoiceType.SelectedValue, SqlDbType.Int);
            SpParam _cid = new SpParam("@challanId", ddlInvoiceNo.SelectedValue, SqlDbType.Int);

            DataSet _Ds = conn.GetDataSet("Lum_Erp_Stp_GetInvoice_for_Payment", _spcId, _spitype, _cid);
            if (_Ds.Tables[0].Rows.Count != 0)
            {
                txtAmounttot.Text = _Ds.Tables[0].Rows[0]["TAmt"].ToString();
                txtAmountpaid.Text = _Ds.Tables[0].Rows[0]["PAmt"].ToString();
                txtAmountdue.Text = _Ds.Tables[0].Rows[0]["DAmt"].ToString();
                txtCustomerOredrNo.Text = _Ds.Tables[0].Rows[0]["cono"].ToString();
                txtPODate.Text = _Ds.Tables[0].Rows[0]["codate"].ToString();
            }
            DataSet _Ds1 = conn.GetDataSet("Lum_Erp_Stp_GetInvoice_for_Payment_detail",  _spitype, _cid);

            GVPaymentDetails.DataSource = null;
            GVPaymentDetails.DataSource = _Ds1;
            GVPaymentDetails.DataBind();
        }
    }
    protected void btnexptoexcel_Click(object sender, EventArgs e)
    {
        DateTime date = DateTime.Today;
        DateTime chqdate = DateTime.Today;
        if (string.IsNullOrEmpty(txtpaymentdate.Text))
        {
           txtpaymentdate.Text = DateTime.Today.ToString("dd/MM/yyyy");
        }
        else
        {
            date = DateTime.ParseExact(txtpaymentdate.Text, "dd/MM/yyyy", null);
        }

        if (string.IsNullOrEmpty(txtchqdate.Text))
        {
            txtchqdate.Text = DateTime.Today.ToString("dd/MM/yyyy");
        }
        else
        {
            chqdate = DateTime.ParseExact(txtchqdate.Text, "dd/MM/yyyy", null);
        }

        if (ddlCustomer.SelectedValue == "0")
        {
            lblMsg.Text = "Select Customer";
            lblMsg.ForeColor = Color.Red;
            return;
        }
        if (ddlInvoiceNo.SelectedValue == "0")
        {
            lblMsg.Text = "Select Invoice No";
            lblMsg.ForeColor = Color.Red;
            return;
        }
        if (txtpaymentamount.Text == "")
        {
            lblMsg.Text = "Enter Payment Amount";
            lblMsg.ForeColor = Color.Red;
            return;
        }
        if (rbtnCheque.Checked == true )
        {
            if (txtBank.Text == "")
            {
                lblMsg.Text = "Enter Bank Name";
                lblMsg.ForeColor = Color.Red ;
                return;
            }
            if (txtbankbranch.Text == "")
            {
                lblMsg.Text = "Enter Bank Branch Name";
                lblMsg.ForeColor = Color.Red;
                return;
            }
            if (txtchqdate.Text == "")
            {
                lblMsg.Text = "Enter Cheque Date";
                lblMsg.ForeColor = Color.Red;
                return;
            }
            if (txtchqNo.Text == "")
            {
                lblMsg.Text = "Enter Cheque No";
                lblMsg.ForeColor = Color.Red;
                return;
            }


        }
        int PaymentAmt=0;
        int tds = 0;
        int othdedn = 0;
        PaymentAmt = Convert.ToInt32(txtpaymentamount.Text);
        if (txttdsdedn.Text !="")
                tds = Convert.ToInt32(txttdsdedn.Text);
        if (txtothdedn.Text != "")
            othdedn = Convert.ToInt32(txtothdedn.Text);

        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            if (_payId < 0)
            {
                try
                {
                    if (rbtnCash.Checked == true)
                    {
                        int i = conn.ExecuteCommandProc("Lum_Erp_Stp_InsertPayment",
                               new SpParam("@CustomerId", ddlCustomer.SelectedValue, SqlDbType.Int),
                               new SpParam("@ChallanTypeId", ddlInvoiceType.SelectedValue, SqlDbType.Int),
                               new SpParam("@ChallanId", ddlInvoiceNo.SelectedValue, SqlDbType.Int),
                               new SpParam("@Cash", "true", SqlDbType.Bit),
                               new SpParam("@Paymentdate", date, SqlDbType.DateTime),
                               new SpParam("@PaymentAmt", PaymentAmt, SqlDbType.Int),
                               new SpParam("@TdsAmount", tds, SqlDbType.Int),
                               new SpParam("@OthDednAmount", othdedn, SqlDbType.Int),
                               new SpParam("@OthDednReason", txtothdednreason.Text, SqlDbType.VarChar),
                               new SpParam("@BankName", DBNull.Value, SqlDbType.VarChar),
                               new SpParam("@BranchName", DBNull.Value, SqlDbType.VarChar),
                               new SpParam("@Chqdate", DBNull.Value, SqlDbType.DateTime),
                               new SpParam("@ChqNo", DBNull.Value, SqlDbType.Int));

                    }

                    else
                    {
                        int i = conn.ExecuteCommandProc("Lum_Erp_Stp_InsertPayment",
                               new SpParam("@CustomerId", ddlCustomer.SelectedValue, SqlDbType.Int),
                               new SpParam("@ChallanTypeId", ddlInvoiceType.SelectedValue, SqlDbType.Int),
                               new SpParam("@ChallanId", ddlInvoiceNo.SelectedValue, SqlDbType.Int),
                               new SpParam("@Cash", "false", SqlDbType.Bit),
                               new SpParam("@Paymentdate", date, SqlDbType.DateTime),
                               new SpParam("@PaymentAmt", PaymentAmt, SqlDbType.Int),
                               new SpParam("@TdsAmount", tds, SqlDbType.Int),
                               new SpParam("@OthDednAmount", othdedn, SqlDbType.Int),
                               new SpParam("@OthDednReason", txtothdednreason.Text, SqlDbType.VarChar),
                               new SpParam("@BankName", txtBank.Text, SqlDbType.VarChar),
                               new SpParam("@BranchName", txtbankbranch.Text, SqlDbType.VarChar),
                               new SpParam("@Chqdate", chqdate, SqlDbType.DateTime),
                               new SpParam("@ChqNo", txtchqNo.Text, SqlDbType.Int));

                    }
                }
                catch (Exception ex)
                {
                    lblMsg.Text = ex.Message;
                    lblMsg.ForeColor = Color.Red ;
                }
                lblMsg.Text = "Payment is updated";
                lblMsg.ForeColor = Color.Green;
                Bindgrid();
            }

        }
    }
    private void Bindgrid()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {

            rbtnCheque.Checked = true;
            rbtnCash.Checked = false;
            txtpaymentamount.Text = "";
            txtpaymentdate.Text = "";
            txttdsdedn.Text = "";
            txtothdedn.Text = "";
            txtbankbranch.Text = "";
            txtBank.Text = "";
            txtchqdate.Text = "";
            txtchqNo.Text = "";

            SpParam _spcId = new SpParam("@CustomerId", ddlCustomer.SelectedValue, SqlDbType.Int);
            SpParam _spitype = new SpParam("@ChallanTypeId", ddlInvoiceType.SelectedValue, SqlDbType.Int);
            SpParam _cid = new SpParam("@challanId", ddlInvoiceNo.SelectedValue, SqlDbType.Int);

            DataSet _Ds = conn.GetDataSet("Lum_Erp_Stp_GetInvoice_for_Payment", _spcId, _spitype, _cid);
            if (_Ds.Tables[0].Rows.Count != 0)
            {
                txtAmounttot.Text = _Ds.Tables[0].Rows[0]["TAmt"].ToString();
                txtAmountpaid.Text = _Ds.Tables[0].Rows[0]["PAmt"].ToString();
                txtAmountdue.Text = _Ds.Tables[0].Rows[0]["DAmt"].ToString();
                txtCustomerOredrNo.Text = _Ds.Tables[0].Rows[0]["cono"].ToString();
                txtPODate.Text = _Ds.Tables[0].Rows[0]["codate"].ToString();
            }
            DataSet _Ds1 = conn.GetDataSet("Lum_Erp_Stp_GetInvoice_for_Payment_detail", _spitype, _cid);

            GVPaymentDetails.DataSource = null;
            GVPaymentDetails.DataSource = _Ds1;
            GVPaymentDetails.DataBind();

        }
    }
    protected void rbtnCheque_CheckedChanged(object sender, EventArgs e)
    {

        if (rbtnCheque.Checked ==true )
        {
            rbtnCash.Checked = false;
            txtbankbranch.ReadOnly = false;
            txtBank.ReadOnly = false;
            txtchqdate.ReadOnly = false;
            txtchqNo.ReadOnly = false;
        }
    }

    protected void rbtnCash_CheckedChanged(object sender, EventArgs e)
    {
        if (rbtnCash.Checked == true)
        {
            rbtnCheque.Checked = false;
            txtbankbranch.Text = "";
            txtBank.Text = "";
            txtchqdate.Text = "";
            txtchqNo.Text = "";

            txtbankbranch.ReadOnly = true;
            txtBank.ReadOnly = true;
            txtchqdate.ReadOnly = true;
            txtchqNo.ReadOnly = true;
        }
    }
    protected void GVPaymentDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("Reverse"))
        {
            TextBox _PaymentId = (TextBox)GVPaymentDetails.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("txtPaymentId");
            _payId = Convert.ToInt32(_PaymentId.Text);
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                conn.ExecuteCommand("UPDATE Lum_Erp_Payment SET PayReverse = 1 WHERE PaymentId =" + _payId);
            }
            lblMsg.Text = "Payment Reversed";
            lblMsg.ForeColor = Color.Green;
            Bindgrid();
        }

    }
}
