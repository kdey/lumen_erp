﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;
public partial class EventEntry_ChallanWithRespectToSoir : BasePage
{
    protected int _challanId = -1;
    protected int _soirId = -1;
    protected bool _isDispach = false;
    private double totalAmount = 0;
        //ArrayList used for caching purpose
		public ArrayList list;
        public ArrayList list1;
        public ArrayList listSerial;
		//object of Props class to store Id, FirstName, LastName, City
		public Props props;
        public Serialno srno;
        public string Action = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsLoggedIn())
            {
                Response.Redirect("./../Login.aspx");
            }
            if (!string.IsNullOrEmpty(Request["View"]))
            {
                Action = (Request["View"]);
            }
            if (!string.IsNullOrEmpty(Request["challanId"]))
            {
                _challanId = int.Parse(Request["challanId"]);
            }
            if (!string.IsNullOrEmpty(Request["soirId"]))
            {
                _soirId = int.Parse(Request["soirId"]);
            }
            if (!string.IsNullOrEmpty(Request["isDispach"]))
            {
                _isDispach = bool.Parse(Request["isDispach"]);
            }
            if (isDispachable())
            {
                btnDispatch.Enabled = true;
            }
            else
            {
                btnDispatch.Visible = false;
            }
            if (isgeneratable())
            {
                btncreate.Visible = false;
            }
            else
            {

                btncreate.Enabled = true;
            }

            if (!this.IsPostBack)
            {
                bindControl();
                if (_challanId > 0)
                {
                   LoadChallan();
                }
            }
            else
            {
                list = new ArrayList();

            }
            if (GetLoggedinUser().UserType == 2)
            {
                btnDispatch.Enabled = true;
                //addChallan.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
        }
    }

    public void LoadChallan()
    {
        try
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                using (IDataReader reader = conn.ExecuteQuery(@"select C.*,S.strSoirId from Lum_Erp_ChallancumInvoice C inner join Lum_Erp_SOIR S on 
                                                    S.soirId=C.soirId  Where C.ChallanId=" + _challanId))
                {
                    if (reader.Read())
                    {
                        ddlTransporter.SelectedValue = Convert.ToString(reader["transporterId"]);
                        txtCNDetails.Text = Convert.ToString(reader["CNDetails"]);
                        if (Convert.ToString(reader["isDispatch"]) == "False")
                        {
                            _isDispach = false;
                        }
                        else
                        {
                            _isDispach = true;
                            btnDispatch.Enabled = false;
                        }
                        if (reader["storeId"] != Convert.DBNull)
                        {
                            ddlStorelocation.SelectedValue = Convert.ToString(reader["storeId"]);
                            ddlStorelocation.Enabled = false;
                        }

                        rbTaxType.SelectedValue = Convert.ToString(reader["taxTypeId"]);
                        ddlSoir.Visible = false;
                        RequiredFieldValidator5.Enabled = false;
                        lblSoir.Visible = true;
                        lblSoir.Text = Convert.ToString(reader["strSoirId"]);
                        Label1.Text = "SOIR No.";
                        challanNumber.Text = Convert.ToString(reader["strChallanId"]) + "/" + Convert.ToString(reader["challanVersion"]);
                        if (rbTaxType.SelectedValue == "3")
                        {
                            dllformrecoveralbe.Enabled = true;
                            if (reader["FormRecoverableId"] != Convert.DBNull)
                            {
                                dllformrecoveralbe.SelectedValue = Convert.ToString(reader["FormRecoverableId"]);
                            }
                        }
                        _soirId = Convert.ToInt32(reader["soirId"]);
                        reader.Close();
                        LoadSoir();
                        BindInstallmentGrid(conn);
                        ddlSoir.Enabled = false;
                    }
                }

            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
        }
    }
    //On clicking 'edit' EditItemIndex of the current item is set and bind data
    protected void OnEdit(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
    {
        DataGrid1.EditItemIndex = e.Item.DataSetIndex;

        NewBindData();
    }

    //On clicking 'update' and bind data
    private void OnUpdate(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
    {
        DataGrid1.EditItemIndex = -1;

        NewBindData();
    }

    //On clicking 'cancel' and bind data
    private void OnCancel(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
    {
        DataGrid1.EditItemIndex = -1;

        NewBindData();
    }

    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.DataGrid1.CancelCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.OnCancel);
        this.DataGrid1.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.OnEdit);
        this.DataGrid1.UpdateCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.OnUpdate);
        this.Load += new System.EventHandler(this.Page_Load);

    }
    #endregion

    /// <summary>
    /// On clicking "Insert Data", create an object of Props and assign the
    /// imput values to the properties in Props class.
    /// Add the props object to an arraylist and call bind data.
    /// </summary>
    protected void Button_Click(object sender, System.EventArgs e)
    {
 
    }

    ///Method for binding the data in the datagrid.
    ///Read each item(row) in the datagrid and get the cell values.
    ///Assign it to the properties of the Props class and add it to the
    ///list until all the items are read. The DataSource is the list containing
    ///objects of props then bind the data.
    private void NewBindData()
    {
        //for each row in our datagrid
        foreach (DataGridItem item in DataGrid1.Items)
        {
            props = new Props();

            //to read the ItemTemplate
            if (!item.ItemType.ToString().Equals("EditItem"))
            {
                props.id = ((Label)item.Cells[1].FindControl("Label1")).Text;
                props.installmentDate  = ((Label)item.Cells[2].FindControl("Label2")).Text;
                props.installmentamt  = ((Label)item.Cells[3].FindControl("Label3")).Text;
            }
            //to read the EditItemTemplate
            else if (item.ItemType.ToString().Equals("EditItem"))
            {
                props.id = ((TextBox)item.Cells[1].FindControl("Textbox1")).Text;
                props.installmentDate = ((TextBox)item.Cells[2].FindControl("Textbox2")).Text;
                props.installmentamt = ((TextBox)item.Cells[3].FindControl("Textbox3")).Text;

            }

            list.Add(props);
        }

        //datasoure will be the arraylist with props object.
        DataGrid1.DataSource = list;
        DataGrid1.DataBind();
    }
    public void bindControl()
    {
        BindListControl _control = new BindListControl();
        _control.BindSoir(ddlSoir);
        _control.BindTransport(ddlTransporter);
        _control.BindFormRecoverable(dllformrecoveralbe);
        _control.BindStores(ddlStorelocation);
        //ddlStorelocation.SelectedIndex = 1;
        //ddlTransporter.SelectedIndex = 1;
    }
    protected void ddlSoir_SelectedIndexChanged(object sender, EventArgs e)
    {
        _soirId = int.Parse(ddlSoir.SelectedValue);
        LoadSoir();
    }
    /// <summary>
    /// Load Soir Details
    /// </summary>
    public void LoadSoir()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(@"SELECT L1.soirId,customerPoNo,packingCharges,buyBackAmount,poDate,customerName,segmentType,billingAddress,shippingAddress,poTypeCode,primarySalesPerson,aditionalSalesPerson,ISNULL(isDeleted,0) AS isDeleted
	                                                        FROM (select So.soirId,So.customerPoNo,So.packingCharges,So.buyBackAmount,So.poDate,C.customerName,S.segmentType,
	                                                        Billing_address1+' '+Billing_address2  as billingAddress, 
	                                                        Shipping_address1+' '+Shipping_address2 as shippingAddress,
	                                                        O.poTypeCode,(Sa.initial+Sa.firstName+' '+Sa.lastName) as primarySalesPerson,
	                                                        (Sad.initial+Sad.firstName+' '+Sad.lastName) as aditionalSalesPerson from Lum_Erp_Soir So

                                                            inner join Lum_Erp_Customer C on C.customerId=So.customerId
                                                            inner join Lum_Erp_CustomerSegment S on S.segmentId=C.customerSegmentId
                                                            inner join Lum_Erp_PurchaseOrderType O on O.poTypeId=So.orderTypeId
                                                            inner join Lum_Erp_SalesPerson Sa on Sa.salesPersonId=So.primarySalesPersonId
                                                            left join Lum_Erp_SalesPerson Sad on Sad.salesPersonId=So.aditionalSalesPersonId) L1
                                                            LEFT JOIN Lum_Erp_ChallancumInvoice L2
                                                            ON L1.soirId = L2.soirId WHERE L1.soirId=" + _soirId))
            {
                if (reader.Read())
                {
                    PrimarySalesPerson.Text = Convert.ToString(reader["primarySalesPerson"]);
                    AditionalSalesPerson.Text = Convert.ToString(reader["aditionalSalesPerson"]);
                    customerName.Text = Convert.ToString(reader["customerName"]);
                    billingAddress.Text = Convert.ToString(reader["billingAddress"]);
                    shippingAddress.Text = Convert.ToString(reader["shippingAddress"]);
                    poReference.Text = Convert.ToString(reader["customerPoNo"]);
                    poDate.Text = Convert.ToString(reader["poDate"]).Split(' ')[0];
                    orderType.Text = Convert.ToString(reader["poTypeCode"]);
                    CustomerSegment.Text = Convert.ToString(reader["segmentType"]);
                    double Charge = Convert.ToDouble(reader["packingCharges"]);
                    double Buyback = Convert.ToDouble(reader["buyBackAmount"]);
                    lblCharge.Text = string.Format("{0:#,###.##}", Charge);
                    lblBuybackamount.Text = string.Format("{0:#,###.##}", Buyback);
                    reader.Close();
                    BindGrid(conn, int.Parse(rbTaxType.SelectedValue),0);
                    BindFocGrid(conn);
                    lblquantity.Text = GetTotalQuantity();
                    //totalAmount = totalAmount + Charge - Buyback;
                    //lblTotal.Text = string.Format("{0:N}", totalAmount);
                }
            }

        }

    }
    /// <summary>
    /// Bind purchase order items
    /// </summary>
    /// <param name="conn"></param>
    /// <param name="taxType"></param>
    //private void BindGrid_forrbtn(IConnection conn, int taxType)
    //{
    //    string sql = "";
    //    totalAmount = 0;
    //    try
    //    {
    //        IDataReader _reader;
    //        if (taxType == 1)
    //        {
    //            _reader = conn.ExecuteQueryProc("Lum_erp_Stp_GetPurchaseOrder_WithGrnDetails_vat",
    //                                 new SpParam("@soirId", _soirId, SqlDbType.Int),
    //                                 new SpParam("@CHALLANID", _challanId , SqlDbType.Int),
    //                                 new SpParam("@STORID", int.Parse(ddlStorelocation.SelectedValue), SqlDbType.Int));
    //            poLineGrid.Columns[7].HeaderText = "VAT(%)";
    //            focGrid.Columns[7].HeaderText = "VAT(%)";
    //            poLineGrid.DataSource = _reader;
    //            poLineGrid.DataBind();
    //        }
    //        if (taxType == 2)
    //        {
    //            _reader = conn.ExecuteQueryProc("Lum_erp_Stp_GetPurchaseOrder_WithGrnDetails_cst",
    //                                 new SpParam("@soirId", _soirId, SqlDbType.Int),
    //                                 new SpParam("@CHALLANID", _challanId, SqlDbType.Int),
    //                                 new SpParam("@STORID", int.Parse(ddlStorelocation.SelectedValue), SqlDbType.Int));
 
    //            poLineGrid.Columns[7].HeaderText = "CST(%)";
    //            focGrid.Columns[7].HeaderText = "CST(%)";
    //            poLineGrid.DataSource = _reader;
    //            poLineGrid.DataBind();
    //        }

    //        if (taxType == 3)
    //        {
    //            _reader = conn.ExecuteQueryProc("Lum_erp_Stp_GetPurchaseOrder_WithGrnDetails_cstAgainstFormC",
    //                                 new SpParam("@soirId", _soirId, SqlDbType.Int),
    //                                 new SpParam("@CHALLANID", _challanId, SqlDbType.Int),
    //                                 new SpParam("@STORID", int.Parse(ddlStorelocation.SelectedValue), SqlDbType.Int));

    //            poLineGrid.Columns[7].HeaderText = "CST Against Form(%)";
    //            focGrid.Columns[7].HeaderText = "CST Against Form(%)";
    //            poLineGrid.DataSource = _reader;
    //            poLineGrid.DataBind();
    //        }


    //    }
    //    catch (Exception ex)
    //    {
    //       errmsg.InnerHtml = ex.ToString();


    //    }

    //    if (string.IsNullOrEmpty(lblCharge.Text))
    //    {
    //        lblCharge.Text = "0";
    //    }
    //    if (string.IsNullOrEmpty(lblBuybackamount.Text))
    //    {
    //        lblBuybackamount.Text = "0";
    //    }
    //    totalAmount = totalAmount + Convert.ToDouble(lblCharge.Text) - Convert.ToDouble(lblBuybackamount.Text);
    //    lblTotal.Text = string.Format("{0:#,###.##}", Math.Round(totalAmount));
    //    lblPrice.Text = changeCurrencyToWords(Math.Round(totalAmount));
    //}
    private void BindGrid(IConnection conn, int taxType ,int tid)
    {
        //code modified for vat calculation by kasturi on 12 apr 2013 
        string fromDate = Convert.ToDateTime(poDate.Text).ToString("dd MMM yyyy");
        string sql = "";
        totalAmount = 0;
        try
        {
            IDataReader _reader;
                  
                _reader = conn.ExecuteQueryProc("Lum_erp_Stp_GetPurchaseOrder_WithGrnDetails_new",
                                     new SpParam("@soirId", _soirId, SqlDbType.Int),
                                     new SpParam("@CDate", fromDate, SqlDbType.SmallDateTime),
                                     new SpParam("@CHALLANID", _challanId, SqlDbType.Int),
                                     new SpParam("@STORID", int.Parse(ddlStorelocation.SelectedValue), SqlDbType.Int),
                                     new SpParam("@tid", tid, SqlDbType.Int));
                poLineGrid.Columns[7].HeaderText = "Tax(%)";
                focGrid.Columns[7].HeaderText = "Tax(%)";
                poLineGrid.DataSource = _reader;
                poLineGrid.DataBind();
  
        }
        catch (Exception ex)
        {
            errmsg.InnerHtml = ex.ToString();


        }

        if (string.IsNullOrEmpty(lblCharge.Text))
        {
            lblCharge.Text = "0";
        }
        if (string.IsNullOrEmpty(lblBuybackamount.Text))
        {
            lblBuybackamount.Text = "0";
        }
        totalAmount = totalAmount + Convert.ToDouble(lblCharge.Text) - Convert.ToDouble(lblBuybackamount.Text);
        lblTotal.Text = string.Format("{0:#,###.##}", Math.Round(totalAmount));
        lblPrice.Text = changeCurrencyToWords(Math.Round(totalAmount));
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void poLineGrid_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //if (_isDispach)
            //{
            //    Button _btn1 = (Button)e.Item.Cells[10].Controls[0];

            //    _btn1.Enabled = false;

            //}
            if (e.Item.Cells[9].Text != " ")
             CalcTotalAmount(e.Item.Cells[9].Text);

        }
    }
    /// <summary>
    /// Find Total Quantity.
    /// </summary>
    /// <returns></returns>
    public string GetTotalQuantity()
    {
        string retVal = "";
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(@"select Sum(quantity) as TotalQty from Lum_Erp_SoirItemDetails WHERE  soirId=" + _soirId))
            {
                if (reader.Read())
                {


                    retVal = Convert.ToString(reader["TotalQty"]);
                }
            }
        }
        return retVal;
    }
    /// <summary>
    /// Find Total amount
    /// </summary>
    /// <param name="_netVal"></param>
    private void CalcTotalAmount(string _netVal)
    {
        if (_netVal != "" && _netVal !="&nbsp;")
        {
            totalAmount += Double.Parse(_netVal);
        }

    }
    /// <summary>
    /// Bind FOC items
    /// </summary>
    /// <param name="conn"></param>
    private void BindFocGrid(IConnection conn)
    {
//        string sql = @"SELECT soirLineId,itemId,divisionName,subdivisionName,itemDescription,quantity,uomName,price,vat,discount,total,ISNULL(isDeleted,0) AS isDeleted
//                    FROM (
//	                    SELECT SD.soirId, soirLineId,sd.itemId, d.divisionName, s.subdivisionName, i.itemDescription, sd.quantity,  u.uomName, 0.00 as price,0.00 as vat, 
//                        0.00 as discount,price*discount as total 
//                        from Lum_Erp_SoirItemDetails sd 
//                        inner join Lum_Erp_Item i on sd.itemId = i.itemId
//                        inner join Lum_Erp_Subdivision s on s.subdivisionId = i.subdivisionId
//                        inner join Lum_Erp_Division d on d.divisionId = s.divisionId
//                        inner join Lum_Erp_UOM u on i.uomId = u.uomId
//                        where itemType=1 ) L1
//	                    LEFT JOIN Lum_Erp_ChallancumInvoice L2
//	                    ON L1.soirId = L2.soirId WHERE L1.soirId= " + _soirId;
//        using (IDataReader reader = conn.ExecuteQuery(sql))
//        {
//            focGrid.DataSource = reader;
//            focGrid.DataBind();
//        }
    }
    protected void rbTaxType_SelectedIndexChanged(object sender, EventArgs e)
    {

        IConnection conn = DataConnectionFactory.GetConnection();
        if (rbTaxType.SelectedValue == "1")
        {

            BindGrid(conn, 1,1);
            dllformrecoveralbe.SelectedIndex = 0;
            dllformrecoveralbe.Enabled = false;

        }
        else if (rbTaxType.SelectedValue == "2")
        {
            BindGrid(conn, 2,2);
            dllformrecoveralbe.SelectedIndex = 0;
            dllformrecoveralbe.Enabled = false;

        }
        else if (rbTaxType.SelectedValue == "3")
        {
            BindGrid(conn, 3,3);
            dllformrecoveralbe.Enabled = true;

        }

    }
    protected void addChallan_Click(object sender, EventArgs e)
    {
        DateTime installmentdate = DateTime.Today;
        if (string.IsNullOrEmpty(txtDate.Text))
        {
            txtDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
        }
        else
        {
            installmentdate = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", null);
        }

        props = new Props();
        props.id = "0";
        props.installmentDate = Convert.ToString(installmentdate).Split(' ')[0]; 
        props.installmentamt = price.Text;

        list.Add(props);

        NewBindData();
    }

    private void BindInstallmentGrid(IConnection conn)
    {
        DateTime installmentdate = DateTime.Today;
        list1 = new ArrayList();
        string sql = @"select challanId,installmentDate ,price from Lum_Erp_InstallmentDetails  where challanId= " + _challanId;
        using (IDataReader reader = conn.ExecuteQuery(sql))
        {
            while (reader.Read())
            {
                props = new Props();
                props.id = Convert.ToString(reader["challanId"]);
                props.installmentDate = Convert.ToString(reader["installmentDate"]).Split(' ')[0];
                props.installmentamt = Convert.ToString(reader["price"]);

                list1.Add(props);
            }


            DataGrid1.DataSource = list1;
            DataGrid1.DataBind();

            //InstallmentGrid.DataSource = reader;
            // InstallmentGrid.DataBind();
        }
    }
    protected void InstallmentGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
    }

    protected void ReadOnlyFiled()
    {
        ddlTransporter.Enabled = false;
        txtCNDetails.ReadOnly = true;
        rbTaxType.Enabled = false;
        //   rbDispatch.Enabled = false;
        dllformrecoveralbe.Enabled = false;
    }


    protected void poLineGrid_PreRender(object sender, EventArgs e)
    {
        for (int _index = 0; _index < poLineGrid.Items.Count; _index++)
        {
            Button btnedit = (Button)poLineGrid.Items[_index].Cells[10].Controls[0];

            TextBox _serialNo = (TextBox)poLineGrid.Items[_index].FindControl("itemSerialNo");
            HtmlInputHidden _hdel = (HtmlInputHidden)poLineGrid.Items[_index].FindControl("isdeleted");
            int lineId = (int)poLineGrid.DataKeys[_index];
            int itemId = Convert.ToInt32(Convert.ToDecimal(((HtmlInputHidden)poLineGrid.Items[_index].FindControl("itemId")).Value));

            if (_hdel.Value == "True")
            {
                btnedit.Visible = false;
            }

            if (poLineGrid.Items[_index].Cells[0].Text.ToUpper() != "FINISHED GOODS")
            {
                _serialNo.Attributes.Add("readonly", "readonly");
                btnedit.Visible = false;
            }
            else
            {
               // _serialNo.Text = GetItemSerialNo(_challanId, itemId, lineId);
            }
            if (Action == "View")
            {
                rbTaxType.Enabled = false;
                addChallan.Visible = false;
                btncreate.Visible = false;
                btnDispatch.Visible = false;
            }
        }
    }
    protected void focGrid_PreRender(object sender, EventArgs e)
    {

        for (int _index = 0; _index < focGrid.Items.Count; _index++)
        {
            Button btnedit = (Button)focGrid.Items[_index].Cells[8].Controls[0];

            //HtmlInputHidden _hdel = (HtmlInputHidden)focGrid.Items[_index].FindControl("isdeleted");
 
            //if (_hdel.Value == "True")
            //{
            //    btnedit.Visible = false;
            //}

        }
    }

    protected void poLineGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {

       
    }
    private int savechallan()
    {
        int id;
        string installmentDate;
        string installmentamt;
        object _obj = Convert.DBNull;

        if (rbTaxType.SelectedValue == "3")
        {
            if (dllformrecoveralbe.SelectedIndex != 0)
            {
                _obj = dllformrecoveralbe.SelectedValue;
            }
        }

        int lineId;
        int quantity;
        int itemId;
        string serialNo;
        int quantityOrder;
        listSerial = new ArrayList();
        Validate();
        #region checking
        for (int i = 0; i < poLineGrid.Items.Count; i++)
        {
            quantity = Convert.ToInt32(Convert.ToDecimal (((Label)poLineGrid.Items[i].FindControl("itemQty")).Text));
            lineId = Convert.ToInt32(((HtmlInputHidden)poLineGrid.Items[i].FindControl("soirLineId")).Value);
            itemId = Convert.ToInt32(((HtmlInputHidden)poLineGrid.Items[i].FindControl("itemId")).Value);
            serialNo = ((TextBox)poLineGrid.Items[i].FindControl("itemSerialNo")).Text;
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                try
                {
                    if (poLineGrid.Items[i].Cells[0].Text.ToUpper() == "FINISHED GOODS")
                    {
                        if (serialNo != string.Empty)
                        {
                            string[] param = serialNo.Split(';');
                            if (quantity == param.Length)
                            {
                                if (IsValidSerialNo(conn, serialNo, itemId, quantity, int.Parse(ddlStorelocation.SelectedValue), _challanId, lineId))
                                {
                                    gridMessage.Text = "";
                                    gridMessage.ForeColor = Color.Green;

                                    if (isDispachable())
                                    {
                                        btnDispatch.Enabled = true;
                                    }
                                    else
                                    {
                                        btnDispatch.Enabled = false;
                                    }
                                }
                                else
                                {
                                    gridMessage.Text += "Serial Number is not available 1";
                                    gridMessage.ForeColor = Color.Red;
                                    return 0;
                                }
                            }
                            else
                            {
                                gridMessage.Text = "quantity is not equal to total serialNo";
                                gridMessage.ForeColor = Color.Red;
                                return 0;
                            }
                        }

                    }

                    else
                    {
                        if (IsValidSerialNo(conn, serialNo, itemId, quantity, int.Parse(ddlStorelocation.SelectedValue), _challanId, lineId))
                        {
                            gridMessage.Text = "";
                            gridMessage.ForeColor = Color.Green;
                            if (isDispachable())
                            {
                                btnDispatch.Enabled = true;
                            }
                            else
                            {
                                btnDispatch.Enabled = false;
                            }
                        }
                        else
                        {
                            gridMessage.Text = "Items are not available in store";
                            gridMessage.ForeColor = Color.Red;
                            return 0;
                        }
                    }               
                }
                    catch (Exception ex )
                    {
                        gridMessage.Text = ex.Message; ;
                        gridMessage.ForeColor = Color.Red;
                        return 0;
                    }

            }
            
        }
        #endregion
        //Boolean x = GetDuplicates(listSerial);

        Validate();

        if (IsValid)
        {
            gridMessage.Text = "";
            if (_challanId <= 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    IDataReader _reader = conn.ExecuteQueryProc("Lum_Erp_Stp_AddChallanWithRespectToSoir",
                                                new SpParam("@soirId", _soirId, SqlDbType.Int),
                                                new SpParam("@strChallanId", string.Empty, SqlDbType.VarChar),
                                                new SpParam("@transporterId", ddlTransporter.SelectedValue, SqlDbType.Int),
                                                new SpParam("@CNDetails", txtCNDetails.Text, SqlDbType.VarChar),
                                                new SpParam("@isDispatch", 0, SqlDbType.Bit),
                                                new SpParam("@taxTypeId", rbTaxType.SelectedValue, SqlDbType.Int),
                                                new SpParam("@FormRecoverableId", _obj, SqlDbType.Int),
                                                new SpParam("@storeId", ddlStorelocation.SelectedValue, SqlDbType.Int),
                                                new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int));

                    
                if (_reader.Read())
                {

                    
                    _challanId = Convert.ToInt32(_reader["challanId"]);
                    challanNumber.Text = Convert.ToString(_reader["strChallanId"]);
                    _reader.Close();

                    //if (o != null)
                    //{
                    //    _challanId = (int)o;

                    //    string getFinacilaYear = string.Empty;
                    //    if (DateTime.Today.Month > 3)
                    //    {
                    //        getFinacilaYear = string.Format("{0}-{1}", DateTime.Today.Year, DateTime.Today.Year + 1);
                    //    }
                    //    else
                    //    {
                    //        getFinacilaYear = string.Format("{0}-{1}", DateTime.Today.Year - 1, DateTime.Today.Year);
                    //    }
                    //    string challanFormat = string.Format("{0}/{1}/{2}/{3}", "LUM",
                    //                                                getFinacilaYear,
                    //                                                DateTime.Today.Month,
                    //                                                _challanId);
                    //    conn.ExecuteCommand(string.Format("update Lum_Erp_ChallancumInvoice set strChallanId = '{0}' where challanId = {1}", DataUtility.CleanString(challanFormat), _challanId));
                    //    challanNumber.Text = challanFormat;
                        ddlSoir.Enabled = false;
                        lblMsg.Text = "Challan is created";
                        lblMsg.ForeColor = Color.Green;
                        ddlStorelocation.Enabled = false;
                    }
                }
            }
            else
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                   // if (_challanId <= 0)
                    //{
                        object o = conn.ExecuteScalarProc("Lum_Erp_Stp_UpdateChallanWithRespectToSoir",
                                                    new SpParam("@challanId", _challanId, SqlDbType.Int),
                                                    new SpParam("@transporterId", ddlTransporter.SelectedValue, SqlDbType.Int),
                                                    new SpParam("@CNDetails", txtCNDetails.Text, SqlDbType.VarChar),
                                                    new SpParam("@isDispatch", 0, SqlDbType.Bit),
                                                    new SpParam("@taxTypeId", rbTaxType.SelectedValue, SqlDbType.Int),
                                                    new SpParam("@FormRecoverableId", _obj, SqlDbType.Int),
                                                    new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int));




                   // }
                }
            }
            #region insertserialno
            for (int i = 0; i < poLineGrid.Items.Count; i++)
            {
                quantity = Convert.ToInt32(Convert.ToDecimal (((Label)poLineGrid.Items[i].FindControl("itemQty")).Text));
                lineId = Convert.ToInt32(Convert.ToDecimal(((HtmlInputHidden)poLineGrid.Items[i].FindControl("soirLineId")).Value));
                itemId = Convert.ToInt32(Convert.ToDecimal(((HtmlInputHidden)poLineGrid.Items[i].FindControl("itemId")).Value));
                serialNo = ((TextBox)poLineGrid.Items[i].FindControl("itemSerialNo")).Text;
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    try
                    {
                        if (poLineGrid.Items[i].Cells[0].Text.ToUpper() == "FINISHED GOODS")
                        {
                            if (serialNo != string.Empty)
                            {
                                string[] param = serialNo.Split(';');
                                if (quantity == param.Length)
                                {
                                    if (IsValidSerialNo(conn, serialNo, itemId, quantity, int.Parse(ddlStorelocation.SelectedValue), _challanId, lineId))
                                    {
                                        InsertItemSerialNo(lineId, _challanId, itemId, serialNo, conn, quantity, int.Parse(ddlStorelocation.SelectedValue));

                                        gridMessage.Text = "";
                                        gridMessage.ForeColor = Color.Green;

                                        if (isDispachable())
                                        {
                                            btnDispatch.Enabled = true;
                                        }
                                        else
                                        {
                                            btnDispatch.Enabled = false;
                                        }
                                    }
                                    else
                                    {
                                        gridMessage.Text = "Serial Number is not available 2";
                                        gridMessage.ForeColor = Color.Red;
                                        return 0;
                                    }
                                }
                                else
                                {
                                    gridMessage.Text = "quantity is not equal to total serialNo";
                                    gridMessage.ForeColor = Color.Red;
                                    return 0;
                                }
                            }

                        }

                        else
                        {
                            if (IsValidSerialNo(conn, serialNo, itemId, quantity, int.Parse(ddlStorelocation.SelectedValue), _challanId, lineId))
                            {
                                InsertItemSerialNo(lineId, _challanId, itemId, serialNo, conn, quantity, int.Parse(ddlStorelocation.SelectedValue));
                                gridMessage.Text = "";
                                gridMessage.ForeColor = Color.Green;
                                if (isDispachable())
                                {
                                    btnDispatch.Enabled = true;
                                }
                                else
                                {
                                    btnDispatch.Enabled = false;
                                }
                            }
                            else
                            {
                                gridMessage.Text = "Items are not available in store";
                                gridMessage.ForeColor = Color.Red;
                                return 0;
                            }
                        }
                    }
                    catch (FormatException)
                    {
                        gridMessage.Text = "Invalid quantity or price.";
                        gridMessage.ForeColor = Color.Red;
                        return 0;
                    }

                }
            }
            #endregion
            #region installment
            if (_challanId > 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    conn.ExecuteCommand("Delete from Lum_Erp_InstallmentDetails  WHERE challanId =" + _challanId);
                }
                for (int i = 0; i < DataGrid1.Items.Count; i++)
                {
                    //id = ((Label)DataGrid1.Items[i].Cells[1].FindControl("Label1")).Text;
                    installmentDate = ((Label)DataGrid1.Items[i].Cells[2].FindControl("Label2")).Text;
                    installmentamt = ((Label)DataGrid1.Items[i].Cells[3].FindControl("Label3")).Text;

                    using (IConnection conn = DataConnectionFactory.GetConnection())
                    {
                        try
                        {
                            conn.ExecuteCommandProc("Lum_Erp_Stp_CreateInstallmentDetails", new SpParam("@challanId", _challanId, SqlDbType.Int),
                                                                            new SpParam("@price", installmentamt, SqlDbType.Decimal),
                                                                            new SpParam("@installmentDate", installmentDate, SqlDbType.DateTime),
                                                                            new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int));
                        }
                        catch (Exception ex)
                        {
                            errmsg.InnerHtml = ex.ToString();
                        }
                    }
                }
            }

            #endregion
        }
        return 1;
    }
    /// <summary>
    /// Check if serial number and quantity of item for a paerticular stock is available
    /// </summary>
    /// <param name="conn"></param>
    /// <param name="serialNumber"></param>
    /// <param name="itemId"></param>
    /// <param name="receiveQty"></param>
    /// <param name="storeId"></param>
    /// <returns></returns>
    protected bool IsValidSerialNo(IConnection conn, string serialNumber, int itemId, int receiveQty, int storeId, int challanId, int challanLineid)
    {
        bool retVal = true;
        string sql = string.Empty;
        if (serialNumber != string.Empty)
        {
            string[] param = serialNumber.Split(';');
            try
            {


                foreach (string str in param)
                {
                    srno = new Serialno();
                    srno.itemId = itemId.ToString();
                    srno.srno = str;

                    listSerial.Add(srno);
                    using (IDataReader reader = conn.ExecuteQueryProc("Lum_Erp_Stp_CheckItemSerialValid",
                                            new SpParam("@challanLineId", challanLineid, SqlDbType.Int),
                                            new SpParam("@challanId", challanId, SqlDbType.Int),
                                            new SpParam("@itemId", itemId, SqlDbType.Int),
                                            new SpParam("@storeId", storeId, SqlDbType.Int),
                                            new SpParam("@serialNo", str, SqlDbType.VarChar)))
                    {
                        if (reader.Read())
                        {
                            //NOAVL
                            // SRAVL
                            if (Convert.ToInt32(reader["NOAVL"]) <= 0)
                            {
                                gridMessage.Text += "Quantity Not Available in Store";
                                gridMessage.ForeColor = Color.Red;
                                retVal = false;
                                break;
                            }
                            if (str.Length > 0 && (Convert.ToInt32(reader["SRAVL"]) <= 0))
                            {
                                gridMessage.Text += "This Serial No ( " + challanLineid + "," + challanId + "," + itemId + "," + storeId + "," + str + " ) Not Available in Store";
                                gridMessage.ForeColor = Color.Red;
                                retVal = false;
                                break;
                            }
                        }

                    }

                    //object obj = conn.ExecuteScalarProc("Lum_Erp_Stp_CheckItemSerialValid", new SpParam("@challanLineId", challanLineid, SqlDbType.Int),
                    //                           new SpParam("@challanId", challanId, SqlDbType.Int),
                    //                           new SpParam("@itemId", itemId, SqlDbType.Int),
                    //                           new SpParam("@storeId", storeId, SqlDbType.Int),
                    //                           new SpParam("@serialNo", str, SqlDbType.VarChar));

                    //if ((int)obj == 0)
                    //{
                    //    retVal = false;
                    //    break;
                    //}



                }
            }
            catch (Exception ex)
            {
                retVal = false;
                gridMessage.Text = ex.Message;
            }

        }
        else
        {
            object obj = conn.ExecuteScalarProc("Lum_Erp_Stp_CheckSPare_Exists", new SpParam("@challanLineId", challanLineid, SqlDbType.Int),
                                              new SpParam("@challanId", challanId, SqlDbType.Int),
                                              new SpParam("@itemId", itemId, SqlDbType.Int),
                                              new SpParam("@storeId", storeId, SqlDbType.Int));
            if (obj != null)
            {
                if ((decimal)obj < receiveQty)
                {
                    gridMessage.Text = "Quantity Not Available in Store";
                    gridMessage.ForeColor = Color.Red;
                    retVal = false;
                   

                }
            }
        }
        return retVal;
    }


    /// <summary>
    /// Update GrndetailsSerialNumber set detach  for Challan
    /// </summary>
    /// <param name="polineId"></param>
    /// <param name="grnId"></param>
    /// <param name="itemId"></param>
    /// <param name="serialNo"></param>
    /// <param name="conn"></param>
    /// <param name="receiveQty"></param>
    public void InsertItemSerialNo(int challanlineId, int challanId, int itemId, string serialNo, IConnection conn, int receiveQty, int storeId)
    {

        if (serialNo != string.Empty)
        {
            string[] param = serialNo.Split(';');
            try
            {


                foreach (string str in param)
                {

                    conn.ExecuteCommandProc("Lum_Erp_Stp_UpdateGendetailsSerialNumberForChallan", new SpParam("@challanLineId", challanlineId, SqlDbType.Int),
                                               new SpParam("@challanId", challanId, SqlDbType.Int),
                                               new SpParam("@itemId", itemId, SqlDbType.Int),
                                               new SpParam("@storeId", storeId, SqlDbType.Int),
                                               new SpParam("@serialNo", str, SqlDbType.VarChar));

                }

            }
            catch (Exception ex)
            {
                gridMessage.Text = ex.Message;
                gridMessage.ForeColor = Color.Red;
            }
        }
        else
        {
            try
            {


                for (int _i = 0; _i < receiveQty; _i++)
                {
                    conn.ExecuteCommandProc("Lum_Erp_Stp_UpdateGendetailsSerialNumberForChallan", new SpParam("@challanLineId", challanlineId, SqlDbType.Int),
                                                new SpParam("@challanId", challanId, SqlDbType.Int),
                                                new SpParam("@itemId", itemId, SqlDbType.Int),
                                                new SpParam("@storeId", storeId, SqlDbType.Int),
                                                new SpParam("@serialNo", DBNull.Value , SqlDbType.VarChar));

                }

            }
            catch (Exception ex)
            {
                gridMessage.Text = ex.Message;
                gridMessage.ForeColor = Color.Red;
            }
        }

    }

    public string GetItemSerialNo(int challanId, int itemId, int lineId)
    {
        string retVal = "";

        string sql = string.Format(@"SELECT serialNo FROM Lum_Erp_GrndetailsSerialNumber WHERE challanId={0} AND challanLineId={1} AND  itemId={2} ", challanId, lineId, itemId);
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(sql))
            {
                while (reader.Read())
                {
                    if (retVal == "")
                    {
                        retVal = Convert.ToString(reader["serialNo"]);
                    }
                    else
                    {
                        retVal = retVal + ";" + Convert.ToString(reader["serialNo"]);
                    }
                }
            }
        }
        return retVal;

    }
    protected void focGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
     }


    public bool CheckChallanStatus()
    {
        bool retVal = false;
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {

            if (_challanId > 0)
            {

                int _i = conn.ExecuteCommandProc("Lum_Erp_Stp_DispatchChallanItems", new SpParam("@challanId", _challanId, SqlDbType.Int),
                                                                       new SpParam("@storeId", int.Parse(ddlStorelocation.SelectedValue), SqlDbType.Int)
                                                                   );
                if (_i > 0)
                {
                    retVal = true;
                }
            }


        }
        return retVal;

    }
    protected void focGrid_ItemDataBound(object sender, DataGridItemEventArgs e)
    {

    }

    protected void poLineGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {


    }
    protected void focGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {

    }
    protected void InstallmentGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {


    }

    public bool isDispachable()
    {
        bool retVal = false;
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {

            if (_challanId > 0)
            {
                using (IDataReader reader = conn.ExecuteQuery(string.Format("select challanStatus from Lum_Erp_ChallancumInvoice where challanId={0} AND isDispatch = 0", _challanId)))
                {
                    if (reader.Read())
                    {
                        retVal = Convert.ToBoolean(reader["challanStatus"]);
                    }
                }
            }


        }
        return retVal;

    }

    public bool isgeneratable()
    {
        bool retVal = false;
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {

            if (_challanId > 0)
            {
                using (IDataReader reader = conn.ExecuteQuery(string.Format("Select case when isdispatch = 1 then 1 when isdeleted =1 then 1 else 0 end AS challanStatus from Lum_Erp_ChallancumInvoice where challanId = {0} ", _challanId)))
                {
                    if (reader.Read())
                    {
                        retVal = Convert.ToBoolean(reader["challanStatus"]);
                    }
                }
            }


        }
        return retVal;

    }

    protected void btnDispatch_Click(object sender, EventArgs e)
    {
        if (CheckChallanStatus())
        {
            lblMsg.Text = "Challan status is dispach u cant update challan";
            errmsg.InnerHtml = "Challan status is dispach u cant update challan";
            gridMessage.ForeColor = Color.Green;
            Response.Redirect("ChallanCumInvoiceList.aspx");
            // LoadChallan();
        }
    }
    protected void btncreate_Click(object sender, EventArgs e)
    {


        if (savechallan() == 1)
            Response.Redirect("ChallanCumInvoiceList.aspx");
    }

    public static Boolean GetDuplicates(ArrayList arr)
    {
        ArrayList result = new ArrayList();
        Hashtable tblSameCount = new Hashtable();
        foreach (object o in arr)
        {
            if (tblSameCount[o] == null)
                tblSameCount[o] = 0;
            tblSameCount[o] = ((int)tblSameCount[o]) + 1;
        }
        foreach (object key in tblSameCount.Keys)
        {
            if (((int)tblSameCount[key]) > 1)
                result.Add(key);
        }
        if (result.Count > 0)
            return false;
        else
            return true;
    }
    protected void btnPrint_Click(object sender, EventArgs e)
    {
        //if (_challanId > 1)
        //{
        //    Response.Redirect("ChallanToSoirPrint.aspx?ChallanId=" + _challanId);
        //}
        //else
        //{
        //    message.Text = "Create Challan before Print.";
        //    message.ForeColor = Color.Red;
        //}
    }
    protected void btnPrintChallan_Click(object sender, EventArgs e)
    {
        if (_challanId > 1)
        {
            Response.Redirect("ChallanToSoirPrint.aspx?ChallanId=" + _challanId);
        }
        else
        {
            lblMsg.Text = "Create Challan before Print.";
            lblMsg.ForeColor = Color.Red;
        }
    }
    protected void btnPrintInvoice_Click(object sender, EventArgs e)
    {
        if (_challanId > 1)
        {
            if (RbtnSelectInvoice.SelectedValue == "1")
                Response.Redirect("OriginalInvoicePrint.aspx?ChallanId=" + _challanId);
            if (RbtnSelectInvoice.SelectedValue == "2")
                Response.Redirect("DuplicateInvoicePrint.aspx?ChallanId=" + _challanId);
            if (RbtnSelectInvoice.SelectedValue == "3")
                Response.Redirect("ExtraInvoicePrint.aspx?ChallanId=" + _challanId);
        }
        else
        {
            lblMsg.Text = "Create Challan before Print.";
            lblMsg.ForeColor = Color.Red;
        }
    }
}
