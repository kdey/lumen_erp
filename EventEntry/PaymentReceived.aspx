﻿<%@ Page Language="C#" MasterPageFile="~/EventEntry/EventEntry.master" AutoEventWireup="true"
    CodeFile="PaymentReceived.aspx.cs" Inherits="EventEntry_PaymentReceived" Title="Lumen.....Payment Received" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="PageMargin">
        <div class="PageHeading">
            Payment Received</div>
        <div class="Line">
        </div>
        <!--Content Table Start-->
        <div class="PageContent">
            <table width="100%" border="0" cellspacing="5" cellpadding="0">
                <tr>
                    <td align="left" valign="middle" colspan="2">
                        Select Customer :&nbsp;&nbsp;<span style="color: Red; font-weight: bold">*</span>
                    </td>
                    <td align="left" valign="middle" colspan="3">
                        <asp:DropDownList ID="ddlCustomer" runat="server" Width="100%" CssClass="TextboxSmall"
                            AutoPostBack="True" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged"
                            CausesValidation="True" ValidationGroup="a">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="middle">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlCustomer"
                            ErrorMessage="Please Select Customer" InitialValue="--Please Select--" SetFocusOnError="True"
                            ValidationGroup="a"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td valign="top" align="right">
                        Billing Address
                    </td>
                    <td valign="top" align="left">
                        <asp:TextBox ID="billingAddress" runat="server" TextMode="MultiLine" Height="50px"
                            CssClass="TextboxSmall" ReadOnly="True" />
                    </td>
                    <td valign="top" align="right">
                        Installation Address
                    </td>
                    <td valign="top" align="left">
                        <asp:TextBox ID="installationAddress" TextMode="MultiLine" Height="50px" runat="server"
                            CssClass="TextboxSmall" />
                    </td>
                    <td valign="top" align="right">
                        Customer Segment
                    </td>
                    <td align="left" valign="top">
                        <asp:Label ID="CustomerSegment" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle">
                        Invoice Type :
                    </td>
                    <td align="left" valign="middle" colspan="2">
                        <asp:DropDownList ID="ddlInvoiceType" runat="server" Width="150px" AutoPostBack="True"
                            OnSelectedIndexChanged="ddlInvoiceType_SelectedIndexChanged" CssClass="TextboxSmall">
                            <asp:ListItem Value="2">Sales</asp:ListItem>
                            <asp:ListItem Value="1">Services</asp:ListItem>
                            <asp:ListItem Value="3">CSA Services</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="middle">
                        &nbsp;
                    </td>
                    <td align="left" valign="middle">
                        &nbsp;
                    </td>
                    <td align="right" valign="middle">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle">
                        Invoice No#&nbsp;&nbsp;<span style="color: Red; font-weight: bold">*</span>
                    </td>
                    <td align="left" valign="middle">
                        <asp:DropDownList ID="ddlInvoiceNo" runat="server" Width="100%" AutoPostBack="True"
                            OnSelectedIndexChanged="ddlInvoiceNo_SelectedIndexChanged" CssClass="TextboxSmall"
                            ValidationGroup="b">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlInvoiceNo"
                            ErrorMessage="Select Invoice No." InitialValue="--Please Select--" SetFocusOnError="True"
                            ValidationGroup="b"></asp:RequiredFieldValidator>
                    </td>
                    <td align="left" valign="middle">
                        Customer Order No#
                    </td>
                    <td align="left" valign="middle">
                        <asp:TextBox ID="txtCustomerOredrNo" runat="server" ReadOnly="True" Width="150px"
                            CssClass="TextboxSmall"></asp:TextBox>
                    </td>
                    <td align="left" valign="middle">
                        Purchase Order Date :
                    </td>
                    <td align="right" valign="middle">
                        <asp:TextBox ID="txtPODate" runat="server" CssClass="TextboxSmall" Font-Bold="False"
                            ReadOnly="True" Width="150px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle">
                        Total Amount:
                    </td>
                    <td align="left" valign="middle">
                        <asp:TextBox ID="txtAmounttot" runat="server" CssClass="TextboxSmall" ReadOnly="True"
                            Width="150px"></asp:TextBox>
                    </td>
                    <td align="left" valign="middle">
                        Amount Paid:
                    </td>
                    <td align="left" valign="middle">
                        <asp:TextBox ID="txtAmountpaid" runat="server" CssClass="TextboxSmall" ReadOnly="True"
                            Width="150px"></asp:TextBox>
                    </td>
                    <td align="left" valign="middle">
                        Amount Due:
                    </td>
                    <td align="right" valign="middle">
                        <asp:TextBox ID="txtAmountdue" runat="server" CssClass="TextboxSmall" ReadOnly="True"
                            Width="150px"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <div class="Line">
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </div>
            <table>
                <tr>
                    <td align="left" valign="middle" colspan="8">
                        <asp:RadioButton ID="rbtnCash" runat="server" Text="Cash" AutoPostBack="True" OnCheckedChanged="rbtnCash_CheckedChanged" />
                        <asp:RadioButton ID="rbtnCheque" runat="server" Checked="True" Text="Cheque" AutoPostBack="True"
                            OnCheckedChanged="rbtnCheque_CheckedChanged" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle">
                        Payment Date
                    </td>
                    <td>
                        <asp:TextBox ID="txtpaymentdate" runat="server" onkeyup="document.getElementById(this.id).value=''"
                            autocomplete='off' CssClass="TextboxSmall" Width="150px"></asp:TextBox>
                        <br />
                        (dd/MM/yyyy)<cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy"
                            TargetControlID="txtpaymentdate">
                        </cc1:CalendarExtender>
                    </td>
                    <td align="left" valign="middle">
                        Payment Amount
                    </td>
                    <td align="left" valign="middle">
                        <asp:TextBox ID="txtpaymentamount" runat="server" CssClass="TextboxSmall" Width="150px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="quanityRegexValidator" runat="server" EnableClientScript="true"
                            ErrorMessage="Enter Amount Only" Text="" ControlToValidate="txtpaymentamount"
                            ValidationExpression="\d+" />
                    </td>
                    <td align="left" valign="middle">
                        &nbsp;
                    </td>
                    <td align="left" valign="middle">
                        &nbsp;
                    </td>
                    <td align="left" valign="middle">
                        &nbsp;
                    </td>
                    <td align="left" valign="middle">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle">
                        TDS Deduction
                    </td>
                    <td align="left" valign="middle">
                        <asp:TextBox ID="txttdsdedn" runat="server" CssClass="TextboxSmall" Width="150px"></asp:TextBox>
                    </td>
                    <td align="left" valign="middle">
                        Other Deduction
                    </td>
                    <td align="left" valign="middle">
                        <asp:TextBox ID="txtothdedn" runat="server" CssClass="TextboxSmall" Width="150px"></asp:TextBox>
                    </td>
                    <td align="left" valign="middle">
                        Reason
                    </td>
                    <td align="left" valign="middle">
                        <asp:TextBox ID="txtothdednreason" runat="server" CssClass="TextboxSmall" Width="150px"></asp:TextBox>
                    </td>
                    <td align="left" valign="middle">
                        &nbsp;
                    </td>
                    <td align="left" valign="middle">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle">
                        Bank
                    </td>
                    <td align="left" valign="middle">
                        <asp:TextBox ID="txtBank" runat="server" CssClass="TextboxSmall" Width="150px"></asp:TextBox>
                    </td>
                    <td align="left" valign="middle">
                        Branch
                    </td>
                    <td align="left" valign="middle">
                        <asp:TextBox ID="txtbankbranch" runat="server" CssClass="TextboxSmall" Width="150px"></asp:TextBox>
                    </td>
                    <td align="left" valign="middle">
                        CheqDate
                    </td>
                    <td align="left" valign="middle">
                        <asp:TextBox ID="txtchqdate" runat="server" CssClass="TextboxSmall" Width="150px"></asp:TextBox><br />
                        (dd/MM/yyyy)<cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy"
                            TargetControlID="txtchqdate">
                        </cc1:CalendarExtender>
                    </td>
                    <td align="left" valign="middle">
                        CheqNo
                    </td>
                    <td align="left" valign="middle">
                        <asp:TextBox ID="txtchqNo" runat="server" CssClass="TextboxSmall" Width="150px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                    </td>
                    <td align="center" valign="middle" colspan="8">
                        <asp:Button ID="btnsave" runat="server" Text="Save" OnClick="btnexptoexcel_Click"
                            ValidationGroup="report" Style="width: 120px; height: 19px;" CssClass="Button" />
                    </td>
                </tr>
            </table>
        </div>
        <!--Content Table End-->
    </div>
    <div class="Line">
    </div>
    <div class="PageContent">
        <table width="100%" align="center" cellpadding="0" cellspacing="5">
            <tr>
                <td width="15%" align="left" valign="top">
                    Payment Details:
                </td>
            </tr>
            <tr>
                <td width="15%" align="left" valign="top">
                    <asp:GridView ID="GVPaymentDetails" runat="server" AutoGenerateColumns="False" BorderColor="#6699CC"
                        BorderStyle="Solid" BorderWidth="0px" CellPadding="0" CssClass="ReportGridHeading"
                        ShowFooter="True" Width="100%" OnRowCommand="GVPaymentDetails_RowCommand">
                        <RowStyle HorizontalAlign="Center" />
                        <Columns>
                            <asp:BoundField DataField="installmentDate" HeaderText="Installment Date" HeaderStyle-Font-Bold="true"
                                ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData" DataFormatString="{0:dd/MMM/yyyy}">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="price" HeaderText="Value" HeaderStyle-Font-Bold="true"
                                ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="receivedDate" HeaderText="Received Date" HeaderStyle-Font-Bold="true"
                                ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData" DataFormatString="{0:dd/MMM/yyyy}">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="receivedValue" HeaderText="Received Value" HeaderStyle-Font-Bold="true"
                                ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="TdsAmount" HeaderText="TDS" HeaderStyle-Font-Bold="true"
                                ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="OthDednAmount" HeaderText="Oth.Dedn" HeaderStyle-Font-Bold="true"
                                ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="paymentTypeDesc" HeaderText="Payment Type" HeaderStyle-Font-Bold="true"
                                ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="chequeNo" HeaderText="Cheque No#" HeaderStyle-Font-Bold="true"
                                ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="chequeIssueDate" HeaderText="Cheque Issue Date" HeaderStyle-Font-Bold="true"
                                ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData" DataFormatString="{0:dd/MMM/yyyy}">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="BankName" HeaderText="Bank Name" HeaderStyle-Font-Bold="true"
                                ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="BranchName" HeaderText="BranchName" HeaderStyle-Font-Bold="true"
                                ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="PReverse" HeaderText="Reversed" HeaderStyle-Font-Bold="true"
                                ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData">
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:BoundField>
                            <asp:ButtonField CommandName="Reverse" ShowHeader="True" Text="Reverse" HeaderStyle-Font-Bold="true"
                                ItemStyle-Width="10%" ItemStyle-CssClass="ReportGridData" />
                            <asp:TemplateField ShowHeader="true" HeaderStyle-Width="0%" HeaderText="OthDednReason">
                                <ItemTemplate>
                                    <asp:TextBox  BorderStyle="None" ID="TextBox1" runat="server" CssClass ="ReportGridData" ReadOnly="true"
                                        Text='<%# DataBinder.Eval(Container.DataItem, "OthDednReason") %>' />
                                    <asp:TextBox Height="0" Width="0" BorderStyle="None" ID="txtPaymentId" runat="server"
                                        Text='<%# DataBinder.Eval(Container.DataItem, "PaymentId") %>' />
                                </ItemTemplate>
                                <HeaderStyle Font-Bold="True"></HeaderStyle>
                                <ItemStyle Width="10%" CssClass="ReportGridData"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle CssClass="ReportGridHeading" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
