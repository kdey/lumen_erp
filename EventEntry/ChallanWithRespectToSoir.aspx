<%@ Page Title="" Language="C#" MasterPageFile="~/EventEntry/EventEntry.master" Culture="en-GB" AutoEventWireup="true" CodeFile="ChallanWithRespectToSoir.aspx.cs" Inherits="EventEntry_ChallanWithRespectToSoir" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script type="text/javascript">
    function isNumberKey(evt)   //Checks textbox contains only numeric value
    {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode != 46)) {
            return false;
        }
        return true;
    }
    function isNumberKey_WO_Decimal(evt)   //Checks textbox contains only numeric value
    {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    function letternumber(e) {
        var key;
        var keychar;

        if (window.event)
            key = window.event.keyCode;
        else if (e)
            key = e.which;
        else
            return true;
        keychar = String.fromCharCode(key);
        keychar = keychar.toLowerCase();

    // control keys
        if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
            return true;

    // alphas and numbers
        else if ((("abcdefghijklmnopqrstuvwxyz0123456789;").indexOf(keychar) > -1))
            return true;
        else
            return false;
    }
     
</script>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<input type="hidden" name="soirId" value='<%=_soirId%>' />
<input type="hidden" name="challanId" value='<%=_challanId%>' />
<input type="hidden" name="isDispach" value='<%=_isDispach%>' />
<div class="PageMargin">
    <div class="PageHeading">Challan-Cum-Invoice<a style="float:right" href="ChallanCumInvoiceList.aspx"> Challan-Cum-Invoice List</a></div>
    <div class="Line"></div>
    <!--Content Table Start-->
    <div class="PageContent">
    <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
    <tr><td width="16%" align="right" valign="top">Challan No.</td>
        <td align="left" valign="top" width="20%"><asp:Label ID="challanNumber" runat="server" /></td>
        <td width="16%" align="right" valign="top"><asp:Label ID="Label1" runat="server"  Text="Select SOIR"></asp:Label>&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
        <td align="left" valign="top" width="16%"><asp:DropDownList ID="ddlSoir" runat="server" AutoPostBack="true"
         onselectedindexchanged="ddlSoir_SelectedIndexChanged" CssClass="TextboxSmall"></asp:DropDownList>
            <asp:Label ID="lblSoir" runat="server" Visible="false"></asp:Label><br />
            <asp:RequiredFieldValidator InitialValue="0" ControlToValidate="ddlSoir" ErrorMessage="Select Soir" ID="RequiredFieldValidator5"
             runat="server" /></td>
        <td >Customer Order Type </td>
        <td align="left" ><asp:Label ID="orderType" runat="server" ReadOnly="True" /></td>
    </tr>
    <tr><td width="16%" align="right" valign="top">Customer Name</td>
        <td  align="left" valign="top"><asp:Label ID="customerName" runat="server" ReadOnly="True" /></td>
        <td valign="top" align="right">Billing Address</td>
        <td valign="top" align="left"><asp:TextBox ID="billingAddress" runat="server" TextMode="MultiLine" Height="50px" CssClass="TextboxSmall"
             ReadOnly="True" /></td>
        <td valign="top" align="right">Installation Address</td>
        <td valign="top" align="left"><asp:TextBox ID="shippingAddress" runat="server" TextMode="MultiLine" Height="50px" CssClass="TextboxSmall"
         ReadOnly="True" /></td>
    </tr>
    <tr><td valign="top" align="right">Customer Order No.</td>
        <td valign="top" align="left"><asp:Label ID="poReference" runat="server" ReadOnly="True" /></td>
        <td valign="top" align="right" >Customer Order Date <br />(dd/mm/yyyy)</td>
        <td valign="top" align="left" ><asp:Label ID="poDate" runat="server" ReadOnly="True" /></td>
        <td align="right" >Customer Segment</td>
        <td align="left"><asp:Label ID="CustomerSegment"   runat="server"   ReadOnly="True" /></td>
    </tr>
    <tr><td valign="top" align="right">Primary Sales Person </td>
        <td valign="top" align="left"><asp:Label ID="PrimarySalesPerson" runat="server"   ReadOnly="True" /></td>
        <td valign="top" align="right">Aditional Sales Person</td>
        <td valign="top" align="left"><asp:Label ID="AditionalSalesPerson" runat="server"  ReadOnly="True" /></td>
        <td align="right" valign="top" >Store Location&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
        <td align="left" valign="top" ><asp:DropDownList ID="ddlStorelocation" runat="server" CssClass="TextboxSmall"></asp:DropDownList> <br />
            <asp:RequiredFieldValidator InitialValue="0" ControlToValidate="ddlStorelocation" ErrorMessage="Select Store"
             ID="RequiredFieldValidator10"  runat="server" /></td>
    </tr>
    <tr><td align="right" >C/N Details</td>
        <td align="left"><asp:TextBox ID="txtCNDetails" TextMode="MultiLine" runat="server" CssClass="TextboxSmall" Height="50px"></asp:TextBox></td>
        <td align="right" valign="top">Transporter&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
        <td align="left"  valign="top"><asp:DropDownList ID="ddlTransporter" runat="server" CssClass="TextboxSmall"></asp:DropDownList> <br />
            <asp:RequiredFieldValidator InitialValue="0" ControlToValidate="ddlTransporter" ErrorMessage="Select Transporter"
             ID="RequiredFieldValidator1"  runat="server" /></td>
    </tr>
    <tr><td colspan="2" align="center"><asp:RadioButtonList ID="rbTaxType" AutoPostBack="true" runat="server" RepeatDirection="Horizontal"
         onselectedindexchanged="rbTaxType_SelectedIndexChanged">
            <asp:ListItem Selected="True" Value="1">VAT</asp:ListItem><asp:ListItem Value="2">CST</asp:ListItem>
            <asp:ListItem Value="3">CST Against Form</asp:ListItem></asp:RadioButtonList></td>
        <td align="right">Form Recoverable</td>
        <td align="left"><asp:DropDownList ID="dllformrecoveralbe" Enabled="false" runat="server" CssClass="TextboxSmall"></asp:DropDownList></td>
        <td>&nbsp;</td>
        <td align="left"><asp:Label ID="lblMsg" runat="server"></asp:Label></td>
    </tr>
     <tr>
            <td colspan="6">
                <asp:Label ID="Label4" runat="server"></asp:Label>
            </td>
            </tr>
    </table>
    </div>
    <div class="Line"></div>
    <!-- the po line input table -->
    <!-- over here we should the bottom grid where the line details of this purchase order are shown -->
    <!--Grid Table Start-->
    <div class="PageHeading">Add New Customer Order Details</div>
    <div class="Line"></div>
    <div align="right">Serial Number will be  separated by (;)</div>
    <asp:DataGrid ID="poLineGrid" runat="server" AutoGenerateColumns="false" DataKeyField="soirLineId" GridLines="None" Width="100%"
     FooterStyle-CssClass="GridData" HeaderStyle-CssClass="GridHeading" ItemStyle-CssClass="GridData" onitemdatabound="poLineGrid_ItemDataBound" 
     onprerender="poLineGrid_PreRender" onitemcommand="poLineGrid_ItemCommand" onitemcreated="poLineGrid_ItemCreated" >
        <Columns>
            <asp:BoundColumn DataField="divisionName" HeaderText="Division" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true" />
            <asp:BoundColumn DataField="subdivisionName" HeaderText="Sub Division" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true" />
            <asp:TemplateColumn HeaderText="Item Description" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Left"
             ItemStyle-HorizontalAlign="Left">
                <ItemTemplate>
                <asp:TextBox ID="itemDescription" ReadOnly ="true" height = "50px" Width="175px" TextMode="MultiLine" runat="server"
                 Text='<%# DataBinder.Eval(Container.DataItem, "itemDescription") %>' CssClass="Textbox2ExtraSmall_wo_border"  />
                <asp:Label ID="QtyAvlInSTore" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "QtyAvlInSTore") %>' Width="60" />
                 
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" Font-Bold="True" Width="75px"></HeaderStyle>
                <ItemStyle HorizontalAlign="Left"></ItemStyle>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="FOC" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                <ItemTemplate>
                 <%--<asp:Label ID="foc" runat="server" Enabled="false"
                        Text='<%# DataBinder.Eval(Container.DataItem, "itemType") %>' Width="60" />--%>
                <asp:CheckBox Enabled="false" runat="server" ID="foc" Checked ='<%# DataBinder.Eval(Container.DataItem, "itemType") %>' />
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" Font-Bold="True"></HeaderStyle>
                <ItemStyle HorizontalAlign="Left"></ItemStyle>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Quantity" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true">
                <ItemTemplate>
                <input type="hidden" id="itemId" value='<%# DataBinder.Eval(Container.DataItem, "itemId") %>' runat="server"  />
                <asp:Label ID="itemQty" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "quantity") %>' Width="50" />
                <%#DataBinder.Eval(Container.DataItem, "uomName")%>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Price" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true">
                <ItemTemplate>
                <asp:Label ID="itemPrice" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "price") %>' Width="60" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Discount" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true">
                <ItemTemplate>
                <asp:Label ID="itemDiscount" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "discount") %>' Width="60" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderStyle-HorizontalAlign="Left"  HeaderStyle-Font-Bold="true">
                <ItemTemplate>
                <asp:Label ID="lblTxtType" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "taxType") %>' Width="60" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Serial No" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true">
                <ItemTemplate>
                <asp:TextBox ID="itemSerialNo" EnableViewState="true" Text='<%# DataBinder.Eval(Container.DataItem, "SerialNo") %>'
                 runat="server" onkeypress="return letternumber(event);"  Width="100" TextMode="MultiLine" Height="50" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="total" HeaderStyle-HorizontalAlign="Left"  HeaderText="Total" HeaderStyle-Font-Bold="true"
             DataFormatString="{0:#,###.##}" />
            <asp:ButtonColumn ButtonType="PushButton"  Visible = "false" Text="Update" CommandName="Edit" ItemStyle-Width="10%" />
            <asp:TemplateColumn Visible="false">
                <ItemTemplate>
                <input type="hidden" id="isDeleted" value='<%#DataBinder.Eval(Container.DataItem, "isDeleted") %>' runat="server" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn >
                <ItemTemplate>
                <input type="hidden" id="soirLineId" value='<%#DataBinder.Eval(Container.DataItem, "soirLineId") %>' runat="server" />
                </ItemTemplate>
            </asp:TemplateColumn>          
        </Columns>
    </asp:DataGrid>
    <div class="Line"></div>
    <div align="center"><asp:Label ID="gridMessage" runat="server" /></div>
<!--<div class="PageHeading">FOC Item Details</div>

<div class="Line"></div>

<asp:DataGrid ID="focGrid" runat="server" AutoGenerateColumns="false"
DataKeyField="soirLineId" GridLines="None" Width="100%"
HeaderStyle-CssClass="GridHeading" ItemStyle-CssClass="GridData" 
onitemcommand="focGrid_ItemCommand" 
onitemdatabound="focGrid_ItemDataBound" onitemcreated="focGrid_ItemCreated" onprerender="focGrid_PreRender" 

>
<Columns>
<asp:BoundColumn DataField="divisionName" HeaderStyle-HorizontalAlign="Left" HeaderText="Division" HeaderStyle-Font-Bold="true" />
<asp:BoundColumn DataField="subdivisionName" HeaderStyle-HorizontalAlign="Left" HeaderText="Sub Division" HeaderStyle-Font-Bold="true" />
<asp:BoundColumn DataField="itemDescription" HeaderStyle-HorizontalAlign="Left" HeaderText="Item" HeaderStyle-Font-Bold="true" />
<asp:TemplateColumn HeaderText="Quantity" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true">
<ItemTemplate>
<input type="hidden" id="itemId" value='<%# DataBinder.Eval(Container.DataItem, "itemId") %>' runat="server"  />
<asp:Label ID="itemQty" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "quantity") %>' Width="50" />
<%#DataBinder.Eval(Container.DataItem, "uomName")%>


</ItemTemplate>
</asp:TemplateColumn>
<asp:BoundColumn DataField="price" HeaderStyle-HorizontalAlign="Left" HeaderText="Price" HeaderStyle-Font-Bold="true" />
<asp:BoundColumn DataField="discount" HeaderStyle-HorizontalAlign="Left" HeaderText="Discount" HeaderStyle-Font-Bold="true" />            


<asp:TemplateColumn  HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Left">
<ItemTemplate>
<asp:Label ID="lblTxtType" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "vat") %>' Width="60" />
</ItemTemplate>
</asp:TemplateColumn>


<asp:BoundColumn DataField="total" HeaderStyle-HorizontalAlign="Left"  HeaderText="Total" HeaderStyle-Font-Bold="true" DataFormatString="0.00" />

<asp:ButtonColumn ButtonType="PushButton" Text="Update" ItemStyle-Width="10%" CommandName="Edit" />

</Columns>

</asp:DataGrid>
-->
    <div class="Line"></div>
    <div align="center"><asp:Label ID="itemGridMsg" runat="server" /></div>
    <div class="Line"></div>
    <div class="PageHeading"> Installment Dates</div>
    <div class="Line"></div>
    <div class="PageContent">
    <table width="400" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
    <tr><td  width="50%" align="left" valign="middle" >Date(dd/mm/yyyy)</td>
        <td width="30%">Value&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
        <td width="20%"></td>
    </tr>
    <tr><td align="left" valign="top"><asp:TextBox ID="txtDate" CssClass="TextboxSmall"  runat="server"></asp:TextBox></td>
        <td valign="top"><asp:TextBox ID="price"  CssClass="TextboxSmall" runat="server" onkeypress="return isNumberKey(event);"></asp:TextBox></td>
        <td  width="20%"><asp:Button ID="addChallan" runat="server" CssClass="ButtonBig" Text="Add" Width="78px" onclick="addChallan_Click" /></td>
    </tr>
    <tr><td valign="top"  > 
<%--<asp:RequiredFieldValidator ControlToValidate="txtDate" ValidationGroup="addinstallment"  ErrorMessage="Field cannot be empty"
ID="RequiredFieldValidator2" runat="server" />--%>
        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="txtDate"></cc1:CalendarExtender> </td>
        <td valign="top">
<%--<asp:RequiredFieldValidator ControlToValidate="price" ValidationGroup="addinstallment"  ErrorMessage="Field cannot be empty"
ID="RequiredFieldValidator3" runat="server" />--%><br />   
            <asp:RegularExpressionValidator ID="priceRegexValidator" runat="server" ErrorMessage="Invalid Installment Amount"
             ValidationGroup="addinstallment" ControlToValidate="price" ValidationExpression="^\d*\.?\d{2}$" /></td>
        <td width="20%"></td>
    </tr>
    <tr><td colspan="3" valign="top"> 
        <asp:DataGrid id="DataGrid1" runat="server" Width="785px" BorderColor="#ffffff" AutoGenerateColumns="False" GridLines="None"
         FooterStyle-CssClass="GridData" HeaderStyle-CssClass="GridHeading" ItemStyle-CssClass="GridData" BorderStyle="Inset" BorderWidth="2px" 
         CellPadding="2" CellSpacing="2" EditItemStyle-BackColor="#ff6600" EditItemStyle-ForeColor="black">
            <Columns>
                <asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Edit"></asp:EditCommandColumn>
                <asp:TemplateColumn HeaderText="Challan Id" Visible="false">
                    <ItemTemplate>
                    <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.id") %>' ID="Label1"></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.id") %>' ID="Textbox1"></asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Installment Date">
                    <ItemTemplate>
                    <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.installmentDate") %>' ID="Label2">
                    </asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                    <asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.installmentDate") %>' ID="Textbox2">
                    </asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Installment Amount">
                    <ItemTemplate>
                    <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.installmentamt") %>' ID="Label3">
                    </asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                    <asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.installmentamt") %>' ID="Textbox3"
                     onkeypress="return isNumberKey(event);">
                    </asp:TextBox>
                    </EditItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
<!--<asp:DataGrid ID="InstallmentGrid" runat="server" AutoGenerateColumns="false"
DataKeyField="installmentLineId" GridLines="None" Width="100%"   FooterStyle-Font-Bold="true"
HeaderStyle-CssClass="GridHeading" ItemStyle-CssClass="GridData" 
onitemcommand="InstallmentGrid_ItemCommand" onitemcreated="InstallmentGrid_ItemCreated" 

>
<Columns>
<asp:BoundColumn DataField="installmentDate" HeaderStyle-HorizontalAlign="Left" DataFormatString="{0:dd/MM/yyyy}"  HeaderText="Installment Date" HeaderStyle-Font-Bold="true" />

<asp:TemplateColumn HeaderText="Price" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true">
<ItemTemplate>
<asp:TextBox ID="itemPrice" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "price") %>' Width="60" />
</ItemTemplate>
</asp:TemplateColumn>
<asp:ButtonColumn ButtonType="PushButton" Text="Edit" ItemStyle-Width="10%" CommandName="Edit" />
<asp:ButtonColumn ButtonType="PushButton" Text="Delete" ItemStyle-Width="10%" CommandName="Delete" />

</Columns>

</asp:DataGrid>--></td>
    </tr>
    <tr>
<%-- <asp:RadioButtonList ID="rbDispatch" AutoPostBack="true" runat="server" 
RepeatDirection="Horizontal" 
onselectedindexchanged="rbDispatch_SelectedIndexChanged"  >
<asp:ListItem  Value="1">Dispatch</asp:ListItem>
<asp:ListItem  Value="0"  >Not Dispatch</asp:ListItem>

</asp:RadioButtonList>--%>
    <br /><span id="errmsg" runat="server" ></span>
        <td align="center"><asp:Button ID="btncreate" runat="server" CssClass="ButtonBig" Text="Generate/Update Challan" onclick="btncreate_Click" /></td>
        <td><asp:Button ID="btnDispatch" runat="server" CssClass="ButtonBig" Text="Despatch" onclick="btnDispatch_Click" /></td>
    </tr></table>
    </div>
    <div class="Line"></div>
    <div class="PageContent">
    <table width="100%" class="GridData">
    <tr><td align="center">Packing/Forwarding/Insurance Charge:   <asp:Label ID="lblCharge" runat="server"></asp:Label></td>
        <td></td>
        <td align="right"> Buy back amount</td>
        <td><asp:Label ID="lblBuybackamount" runat="server"></asp:Label></td>
    </tr>
    <tr><td align="center"> Total Quantity :   <asp:Label ID="lblquantity" runat="server"></asp:Label></td>
        <td></td>
        <td align="right"> Total</td>
        <td><asp:Label ID="lblTotal" runat="server"></asp:Label></td>
    </tr>
    <tr><td width="30%">Price in Words</td>
        <td colspan="3"><asp:Label ID="lblPrice" runat="server"></asp:Label> </td>
    </tr></table>
<table>
     <tr><td  align="center"> 
            <asp:Button ID="btnPrintChallan" runat="server" Width="100px" 
            CssClass="Button" Text="Print Challan" onclick="btnPrintChallan_Click"/> 
    </TD>
    <td colspan="2" align="center">      
            <asp:RadioButtonList ID="RbtnSelectInvoice" runat="server" 
                RepeatDirection="Horizontal">
                <asp:ListItem Selected="True" Value="1">Org.Buyers Copy</asp:ListItem>
                <asp:ListItem Value="2">Dupl.Sellers Copy</asp:ListItem>
                <asp:ListItem Value="3">Extra</asp:ListItem>
            </asp:RadioButtonList>
            &nbsp;&nbsp;
            <asp:Button ID="btnPrintInvoice" runat="server" Width="100px" 
            CssClass="Button" Text="Print Invoice" onclick="btnPrintInvoice_Click"/>
            
            </td>
            </tr>
          </table>
              
    </div>
    <!--Page Body End-->
</div>
<input type="hidden" value="0" runat="server" id="strsoirId" />
</asp:Content>

