﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;



public partial class EventEntry_GrnWithRefToPo : BasePage
{


    protected int _grnId = -1;
    protected int _poId = -1;
    private double _totalVatAmount = 0;
    private double _totalAmount = 0;
    public string Action = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {


        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!string.IsNullOrEmpty(Request["grnId"]))
        {
            _grnId = int.Parse(Request["grnId"]);
        }
        if (!string.IsNullOrEmpty(Request["poId"]))
        {
            _poId = int.Parse(Request["poId"]);
        }
        if (!string.IsNullOrEmpty(Request["View"]))
        {
            Action = (Request["View"]);
        }
        if (!this.IsPostBack)
        {
            bindControl();
            if (_grnId > 0)
            {
                if (Action == "View")
                {
                    btnUpdate.Visible = false;
                }
                else
                {

                    btnUpdate.Visible = true;
                }
                btnUpdate.Text = "Update";
                LoadGrn();
            }
            

        }
        

        if (GetLoggedinUser().UserType == 2)
        {

            btnUpdate.Visible = false;
           
        }

    }
    /// <summary>
    /// Binds the po details grid
    /// </summary>
    /// <param name="conn"></param>
    private void BindGrid()
    {
        string fromDate = Convert.ToDateTime(poDate.Text).ToString("dd MMM yyyy");
        string sql = "";


//        sql = @"select poLineId, d.divisionName, sd.subdivisionName,i.itemCode, i.itemDescription,pd.itemId , (pd.quantity-pd.quantityReceived) AS quantity ,(pd.quantityReceived-pd.quantityReceived) as quantityReceived,  u.uomName, pd.price, i.vat, 
//                          0 as vatAmount, 0 AS total
//                        from Lum_Erp_PurchaseOrderDetails pd 
//                        inner join Lum_Erp_Item i on pd.itemId = i.itemId
//                        inner join Lum_Erp_Subdivision sd on sd.subdivisionId = i.subdivisionId
//                        inner join Lum_Erp_Division d on d.divisionId = sd.divisionId
//                        inner join Lum_Erp_UOM u on i.uomId = u.uomId
//                        where  poId = " + _poId;


        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
           

            try
            {
                IDataReader _reader;
               _reader = conn.ExecuteQueryProc("Lum_erp_Stp_GetPurchaseOrder_WithGrnDetails",
                                    new SpParam("@poId", _poId, SqlDbType.Int),
                                    new SpParam("@CDate", fromDate, SqlDbType.SmallDateTime),
                                    new SpParam("@grnId", _grnId, SqlDbType.Int));

                poLineGrid.DataSource = _reader;
                poLineGrid.DataBind();
            }
            catch (Exception ex)
            {
                message.Text = ex.ToString();


            }
        }
    }


    public void bindControl()
    {
        BindListControl _control = new BindListControl();
     //   _control.BindVendor(ddlVendor);
        _control.BindStores(ddlStore);
        _control.BindPOrder(ddlPorder);
    }
    /// <summary>
    /// 
    /// </summary>
    public void LoadGrn()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(@"select G.*,P.strPoId,P.poDate,P.poTypeId,P.poVersion,P.rateContractTypeId,V.vendorName from Lum_Erp_GRN G
                                                            inner join Lum_Erp_PurchaseOrder P on G.poId=P.poId
                                                            inner join Lum_Erp_vendor V on V.vendorId=P.vendorId
                                                            WHERE  G.grnId=" + _grnId))
            {
                if (reader.Read())
                {

                    ddlVendor.Text = Convert.ToString(reader["vendorName"]);
                    ddlStore.SelectedValue = Convert.ToString(reader["storeId"]);
                    poNumber.Visible = true;
                    poNumber.Text = Convert.ToString(reader["strPoId"]) + "/" + Convert.ToString(reader["poVersion"]);
                    grnDate.Text = Convert.ToString(reader["dateCreated"]).Split(' ')[0];
                    vendorInvoiceNo.Text = Convert.ToString(reader["vendorInvoiceNo"]);
                    poDate.Text = Convert.ToString(Convert.ToDateTime(reader["poDate"])).Split(' ')[0];
                    _poId = Convert.ToInt32(reader["poId"]);
                    grnNumber.Text = Convert.ToString(reader["strGrnId"]) + "/" + Convert.ToString(reader["grnVersion"]); 
                    invoiceDate.Text = Convert.ToString(Convert.ToDateTime(reader["invoiceDate"])).Split(' ')[0];
                    ddlPorder.Visible = false;
                    poDate.Enabled = false;
                    
                    ddlStore.Enabled = false;


                    BindGrid();
                }
            }
        }
    }
    /// <summary>
    ///Bind The grid respect to Purchase Order.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlPorder_SelectedIndexChanged(object sender, EventArgs e)
    {
        _grnId = -1;
        _poId = -1;
        if (ddlPorder.SelectedIndex != 0)
        {
            _poId = int.Parse(ddlPorder.SelectedValue);
            BindPoData();
        //    ddlVendor.Enabled = false;
            poDate.Enabled = false;
            BindGrid();
        }
    }
    /// <summary>
    /// Bind Vendor and Purchase order date
    /// </summary>
    public void BindPoData()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            string Sqlquery = string.Format(@"SELECT V.VendorName, P.poDate FROM Lum_Erp_PurchaseOrder P
                inner join Lum_erp_vendor V on V.vendorId=P.vendorId  where P.poId ='{0}'", _poId);
            DataSet _Ds = conn.GetDataSet(Sqlquery, "Description");

            if (_Ds.Tables[0].Rows.Count > 0)
            {

                poDate.Text = Convert.ToString(Convert.ToDateTime(_Ds.Tables[0].Rows[0]["poDate"])).Split(' ')[0];
                ddlVendor.Text = _Ds.Tables[0].Rows[0]["VendorName"].ToString();
            }
        }
    }
    /// <summary>
    /// Event handler for item commands of the po line item grid. 
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void poLineGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        int lineId;
        int quantity;
        int itemId;
        string serialNo;
        int quantityOrder;
        int qtydue =0 ;
        int qtyrcv = 0;
        int quantityReceived = 0;
        Validate();
        for (int i = 0; i < poLineGrid.Items.Count; i++)
        {
            TextBox _itemQtyReceived = (TextBox)poLineGrid.Items[i].FindControl("itemQtyReceived");
            if ( _itemQtyReceived.Text == "")
                quantity = 0;
            else
                quantity = Convert.ToInt32(_itemQtyReceived.Text);

            lineId = i+1;// (int)poLineGrid.DataKeys[e.Item.ItemIndex];
            itemId = Convert.ToInt32(((HtmlInputHidden)poLineGrid.Items[i].FindControl("itemId")).Value);
            serialNo = ((TextBox)poLineGrid.Items[i].FindControl("itemSerialNo")).Text;
            quantityOrder = Convert.ToInt32(Convert.ToDecimal (((Label)poLineGrid.Items[i].FindControl("lblQtyOrderd")).Text));
            qtydue = Convert.ToInt32(Convert.ToDecimal (((Label)poLineGrid.Items[i].FindControl("lblqtydue")).Text));

            Label _LblquantityReceived = (Label)poLineGrid.Items[i].FindControl("LblquantityReceived");
            if (_LblquantityReceived.Text == "")
                qtyrcv = 0;
            else
                qtyrcv = Convert.ToInt32(Convert.ToDecimal (_LblquantityReceived.Text));

           // qtyrcv = Convert.ToInt32(((Label)poLineGrid.Items[i].FindControl("LblquantityReceived")).Text);

            
            TextBox _quantityReceived = (TextBox)poLineGrid.Items[i].FindControl("quantityReceived");

            if (_quantityReceived.Text != "")
                quantityReceived = Convert.ToInt32(Convert.ToDecimal (_quantityReceived.Text));

            //if ((quantityOrder+quantityReceived) < quantity)
            if (quantity > qtydue + qtyrcv)
            {
                gridMessage.Text = "receive quantity can't be more than Ordered/Due quantity.";
                gridMessage.ForeColor = Color.Red;
                return;

            }
            if (serialNo != string.Empty)
            {
                string[] param = serialNo.Split(';');
                if ((quantityOrder + quantityReceived) < param.Length)
                {
                    gridMessage.Text = "Serial Number(s) are more than Quantity Received !!!";
                    gridMessage.ForeColor = Color.Red;
                    return;
                }
            }
        }

        if (IsValid)
        {
            DateTime date = DateTime.Today;
            if (string.IsNullOrEmpty(poDate.Text))
            {
                poDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
            }
            else
            {
                date = DateTime.ParseExact(poDate.Text, "dd/MM/yyyy", null);
            }

            DateTime dtmGrnDate = Convert.ToDateTime(grnDate.Text);
            DateTime dtmInvoiceDate = Convert.ToDateTime(invoiceDate.Text);
            if (_grnId <= 0)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    IDataReader _reader = conn.ExecuteQueryProc("Lum_Erp_Stp_CreateGrnWithRespectToPurchaseOrder",
                                                 new SpParam("@poId", _poId, SqlDbType.Int),
                                                 new SpParam("@strGrnId", string.Empty, SqlDbType.VarChar),
                                                 new SpParam("@dateCreated", dtmGrnDate, SqlDbType.DateTime),
                                                 new SpParam("@vendorInvoiceNo", vendorInvoiceNo.Text, SqlDbType.VarChar),
                                                 new SpParam("@invoiceDate", dtmInvoiceDate, SqlDbType.DateTime),
                                                 new SpParam("@storeId", int.Parse(ddlStore.SelectedValue), SqlDbType.Int),
                                                 new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int));
                    if (_reader.Read())
                    {
                        _grnId = Convert.ToInt32(_reader["grnId"]);
                        grnNumber.Text = Convert.ToString(_reader["strGRNId"]);
                        _reader.Close();

                        //string getFinacilaYear = string.Empty;
                        //if (date.Month > 3)
                        //{
                        //    getFinacilaYear = string.Format("{0}-{1}", dtmGrnDate.Year, dtmGrnDate.Year + 1);
                        //}
                        //else
                        //{
                        //    getFinacilaYear = string.Format("{0}-{1}", dtmGrnDate.Year - 1, dtmGrnDate.Year);
                        //}
                        //// GRN Number Format: Store-Location/Financial Year/Month/GRN Number/Amendment No.
                        //string grn = string.Format("{0}/{1}/{2}/{3}",
                        //                                           ddlStore.SelectedItem.Text,
                        //                                          getFinacilaYear,
                        //                                          dtmGrnDate.Month,
                        //                                           _grnId);
                        //conn.ExecuteCommand(string.Format("update Lum_Erp_GRN set strGrnId = '{0}' where grnId = {1}", DataUtility.CleanString(grn), _grnId));
                        //grnNumber.Text = grn;
                        message.Text = "GRN   for Vendor Name " + ddlVendor.Text + " Added";
                        message.ForeColor = Color.Green;
                        ReadOnlyFiled();
                    }
                }
            }

            if (_grnId > 0)
            {
                if (e.CommandName.Equals("Update"))
                {
                   for (int i = 0; i < poLineGrid.Items.Count; i++)
                    {
                        if (((TextBox)poLineGrid.Items[i].FindControl("itemQtyReceived")).Text != "")
                            quantity = Convert.ToInt32(((TextBox)poLineGrid.Items[i].FindControl("itemQtyReceived")).Text);
                        else
                            quantity = 0;

                        lineId = i+1;// (int)poLineGrid.DataKeys[e.Item.ItemIndex];
                        //quantity = Convert.ToInt32(((TextBox)poLineGrid.Items[i].FindControl("itemQtyReceived")).Text);
                        itemId = Convert.ToInt32(((HtmlInputHidden)poLineGrid.Items[i].FindControl("itemId")).Value);
                        serialNo = ((TextBox)poLineGrid.Items[i].FindControl("itemSerialNo")).Text;
                        quantityOrder = Convert.ToInt32(Convert.ToDecimal (((Label)poLineGrid.Items[i].FindControl("lblQtyOrderd")).Text));


                        using (IConnection conn = DataConnectionFactory.GetConnection())
                        {
                            try
                            {
                                conn.ExecuteCommandProc("Lum_Erp_Stp_CreateGrnDetails_WithRespectToPurchaseOrder",
                                                new SpParam("@grnId", _grnId, SqlDbType.Int),
                                                new SpParam("@poId", _poId, SqlDbType.Int),
                                                new SpParam("@lineId", lineId, SqlDbType.Int),
                                                new SpParam("@itemId", itemId, SqlDbType.Int),
                                                new SpParam("@quantity", quantity, SqlDbType.Int),
                                                new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int));
                                InsertItemSerialNo(lineId, _grnId, itemId, serialNo, conn, quantity);
                                gridMessage.Text = "Line updated";
                                gridMessage.ForeColor = Color.Green;
                            }
                            catch (Exception ex)
                            {
                                gridMessage.Text = ex.ToString(); // "Invalid quantity or price.";
                                gridMessage.ForeColor = Color.Red;
                            }
                        }
                    }
                }
                BindGrid();
            }
            else
            {
                gridMessage.Text = "Before receive quantity please raised the grn.";
                gridMessage.ForeColor = Color.Red;
            }
        }

    }
    protected void poLineGrid_PreRender(object sender, EventArgs e)
    {
        
        for (int _index = 0; _index < poLineGrid.Items.Count; _index++)
        {

            TextBox _serialNo = (TextBox)poLineGrid.Items[_index].FindControl("itemSerialNo");
            int itemId = int.Parse(((HtmlInputHidden)poLineGrid.Items[_index].FindControl("itemId")).Value);
            int lineId = int.Parse(((HtmlInputHidden)poLineGrid.Items[_index].FindControl("lineId")).Value);
            TextBox _itemQtyReceived = (TextBox)poLineGrid.Items[_index].FindControl("itemQtyReceived");
            Label _QtyOrderd = (Label)poLineGrid.Items[_index].FindControl("lblQtyOrderd");
            Label lblvat = (Label)poLineGrid.Items[_index].FindControl("vat");
            Label lblvatAmount = (Label)poLineGrid.Items[_index].FindControl("vatAmount");
            Label lbltotal = (Label)poLineGrid.Items[_index].FindControl("total");
            Label itemPrice = (Label)poLineGrid.Items[_index].FindControl("itemPrice");
            double price = Convert.ToDouble(itemPrice.Text);
            double vat = Convert.ToDouble(lblvat.Text);
            //added 
            //Button btnupdate = (Button)poLineGrid.Items[_index].Cells[9].Controls[0];
            Button btndelete = (Button)poLineGrid.Items[_index].Cells[10].Controls[0];
            HtmlInputHidden _hdel = (HtmlInputHidden)poLineGrid.Items[_index].FindControl("isDeleted");

            if (_hdel.Value == "True")
            { 
                btndelete.Visible = false;
            }

            if (_grnId > 0)
            {
                int receiveQty = getReceiveQuantity(_grnId, lineId);
                double totalVat = price * receiveQty * 0.01 * vat;
                double total = price * receiveQty + totalVat;
                lblvatAmount.Text = string.Format("{0:#,###.##}", totalVat);
                lbltotal.Text = string.Format("{0:#,###.##}", total);
                _itemQtyReceived.Text = Convert.ToString(receiveQty);
                int quantityOrder = Convert.ToInt32(Convert.ToDecimal (_QtyOrderd.Text)) + receiveQty;
                _QtyOrderd.Text = Convert.ToString(quantityOrder);
                CalcTotalVat(lblvatAmount.Text);

                CalcTotalAmount(lbltotal.Text);
            }


            if (poLineGrid.Items[_index].Cells[0].Text.ToUpper() != "FINISHED GOODS")
            {
                _serialNo.Attributes.Add("readonly", "readonly");

            }
            else
            {

                //_serialNo.Text = GetItemSerialNo(_grnId, itemId, lineId);
            }

           

        }
        
            totalValtamount.InnerHtml =Convert.ToString( Math.Round( _totalVatAmount));
           totalAmount.InnerHtml = Convert.ToString( Math.Round(_totalAmount));
        
    }
    /// <summary>
    /// Add ItemSerial No during GRN
    /// </summary>
    /// <param name="polineId"></param>
    /// <param name="grnId"></param>
    /// <param name="itemId"></param>
    /// <param name="serialNo"></param>
    /// <param name="conn"></param>
    /// <param name="receiveQty"></param>
    public void InsertItemSerialNo(int grnlineId, int grnId, int itemId, string serialNo, IConnection conn, int receiveQty)
    {
        conn.ExecuteCommandProc("Lum_Erp_Stp_DeleteGrnItemSerialNo", new SpParam("@grnLineId", grnlineId, SqlDbType.Int),
                                      new SpParam("@grnId", grnId, SqlDbType.Int));


        if (serialNo != string.Empty)
        {
            string[] param = serialNo.Split(';');
            try
            {


                foreach (string str in param)
                {

                    conn.ExecuteCommandProc("Lum_Erp_Stp_InsertGendetailsSerialNumber", new SpParam("@grnLineId", grnlineId, SqlDbType.Int),
                                              new SpParam("@grnId", grnId, SqlDbType.Int),
                                               new SpParam("@itemId", itemId, SqlDbType.Int),
                                               new SpParam("@serialNo", str, SqlDbType.VarChar));

                }

            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }
        else
        {
            try
            {


                for (int _i = 0; _i < receiveQty; _i++)
                {
                    conn.ExecuteCommandProc("Lum_Erp_Stp_InsertGendetailsSerialNumber", new SpParam("@grnLineId", grnlineId, SqlDbType.Int),
                                          new SpParam("@grnId", grnId, SqlDbType.Int),
                                           new SpParam("@itemId", itemId, SqlDbType.Int),
                                           new SpParam("@serialNo", string.Empty, SqlDbType.VarChar));

                }

            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }
       
    }

    public string GetItemSerialNo(int grnId, int itemId, int lineId)
    {
        string retVal = "";

        string sql = string.Format(@"SELECT serialNo FROM Lum_Erp_GrndetailsSerialNumber WHERE grnLineId={0} AND grnId={1} AND  itemId={2} ", lineId, grnId, itemId);
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(sql))
            {
                while (reader.Read())
                {
                    if (retVal == "")
                    {
                        retVal = Convert.ToString(reader["serialNo"]);
                    }
                    else
                    {
                        retVal = retVal + ";" + Convert.ToString(reader["serialNo"]);
                    }
                }
            }
        }
        return retVal;

    }

    protected void ReadOnlyFiled()
    {
        grnDate.Enabled = false;
        poDate.Enabled = false;
        ddlPorder.Enabled = false;
        invoiceDate.Enabled = false;
        vendorInvoiceNo.Enabled = false;
        ddlStore.Enabled = false;
        //ddlVendor.Enabled = false;
        btnUpdate.Enabled = false;
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {

        DateTime date = DateTime.Today;
        if (string.IsNullOrEmpty(poDate.Text))
        {
            poDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
        }
        else
        {
            date = DateTime.ParseExact(poDate.Text, "dd/MM/yyyy", null);
        }
        DateTime dtmGrnDate = Convert.ToDateTime(grnDate.Text);
        DateTime dtmInvoiceDate = Convert.ToDateTime(invoiceDate.Text);

        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            if (_grnId > 0)
            {
                IDataReader _reader = conn.ExecuteQueryProc("UpdateGrnWithRespectToPurchaseOrder",
                                             new SpParam("@grnId", _grnId, SqlDbType.Int),
                                             new SpParam("@dateCreated", dtmGrnDate, SqlDbType.DateTime),
                                             new SpParam("@vendorInvoiceNo", vendorInvoiceNo.Text, SqlDbType.VarChar),
                                             new SpParam("@invoiceDate", dtmInvoiceDate, SqlDbType.DateTime),
                                             new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int));

                message.Text = "GRN   for Vendor Name " + ddlVendor.Text + " Updated";
                message.ForeColor = Color.Green;

                BindGrid();
            }





        }

    }

    public int getReceiveQuantity(int grnId, int grnlineid)
    {
        int retVal = 0;
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            string Sqlquery = string.Format("SELECT quantity FROM Lum_Erp_GrnDetails WHERE grnId ={0} AND grnLineId={1}",
              grnId, grnlineid);
            DataSet _Ds = conn.GetDataSet(Sqlquery, "Description");
            if (_Ds.Tables[0].Rows.Count>0)
            {
            retVal = Convert.ToInt32(_Ds.Tables[0].Rows[0]["quantity"]);
            }



        }


        return retVal;
    }

    private void CalcTotalVat(string _vatAmount)
    {
       if (_vatAmount != string.Empty)
        {
            _totalVatAmount += Double.Parse(_vatAmount);
        }
        
    }
    private void CalcTotalAmount(string _netVal)
    {
        if (_netVal != string.Empty)
        {
            _totalAmount += Double.Parse(_netVal);
        }
     
    }






    protected void poLineGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            Button btnEdit = (Button)e.Item.Cells[10].Controls[0];
          
            btnEdit.CssClass = "ButtonBig";
            if (Action == "View")
            {
                btnEdit.Enabled = false;
            }

            if (GetLoggedinUser().UserType == 2)
            {

                btnEdit.Enabled = false;
            }
        }
    }
    protected void btnPrint_Click(object sender, EventArgs e)
    {
        if (_grnId > 1)
        {
            Response.Redirect("GRNPrint_WithPo.aspx?GRNId=" + _grnId);
        }
        else
        {
            message.Text = "Create GRN before Print.";
            message.ForeColor = Color.Red;
        }
    }
    protected void poLineGrid_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
