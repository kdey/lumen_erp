<%@ Page Title="SOIR" Language="C#" MasterPageFile="~/EventEntry/EventEntry.master" Culture="en-GB" AutoEventWireup="true"  MaintainScrollPositionOnPostback="true" EnableEventValidation="false"  CodeFile="SOIR.aspx.cs" Inherits="EventEntry_SOIR" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<script runat="server"></script>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript">
    function isNumberKey(evt)   //Checks textbox contains only numeric value
    {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode != 46)) {
            return false;
        }
        return true;
    }
    function isNumberKey_WO_Decimal(evt)   //Checks textbox contains only numeric value
    {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    } 
    function CalculateVatAmount() {
        var txtPrice = document.getElementById('<%=price.ClientID%>');
        var maxprice = document.getElementById('<%=hmaxprice.ClientID%>');
        if (parseFloat(txtPrice.value) > parseFloat(maxprice.value))
        {
            alert("Price is greater than max price (" +  Math.round(maxprice.value) +")");
        }       
    }
    function round_decimals(original_number, decimals) {
        var result1 = original_number * Math.pow(10, decimals)
        var result2 = Math.round(result1)
        var result3 = result2 / Math.pow(10, decimals)
        return result3;
    }     
</script>
<input type="hidden" name="soirId" value='<%=_soirId%>' />
<input type="hidden" name="isLocked" value='<%=_isLocked%>' />
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<div class="PageMargin">
    <div class="PageHeading">SOIR<a style="float:right" href="SOIRList.aspx">SOIR List</a></div>
    <div class="Line"></div>
    <!--Content Table Start-->
    <div class="PageContent">
    <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
    <tr><td width="16%" align="right" valign="top">SOIR No.</td>
        <td align="left" valign="top" width="20%"><asp:Label ID="soirNumber" runat="server" /></td>
        <td width="16%" align="right" valign="top">SOIR Raised By</td>
        <td align="left" valign="top" width="16%"><asp:Label ID="raisedBy" runat="server"  /></td>
        <td align="right" valign="top">Customer Order Type &nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
        <td align="left" valign="top"><asp:DropDownList ID="ddlOrder" CssClass="TextboxSmall" runat="server"></asp:DropDownList><br />
            <asp:RequiredFieldValidator InitialValue="0" ControlToValidate="ddlOrder" ValidationGroup="addItem" EnableClientScript="true"
             ErrorMessage="Select Order Type" ID="RequiredFieldValidator5"  runat="server" /></td>
    </tr>
    <tr><td width="16%" align="right" valign="top">Primary Sales Person </td>
        <td  align="left" valign="top"><asp:DropDownList CssClass="TextboxSmall" ID="ddlprimary" runat="server" Width ="100%"></asp:DropDownList>
        </td>
        <td align="right" valign="top">Aditional Sales Person</td>
        <td align="left" valign="top"><asp:DropDownList Width ="100%" CssClass="TextboxSmall" ID="ddlAditional" runat="server"></asp:DropDownList>
        </td>
        <td align="right" valign="top">Rate Contract Type&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
        <td align="left" valign="middle"><asp:DropDownList ID="rateContractDdl" runat="server" CssClass="DropdownExtraSmall"  /><br />
            <asp:RequiredFieldValidator ControlToValidate="rateContractDdl" ValidationGroup="addItem" EnableClientScript="true" InitialValue="0"
             ErrorMessage="Select Rate Contract Type" ID="RequiredFieldValidator1"  runat="server" /></td>
    </tr>
    <tr><td align="right" valign="top">Customer Name&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
        <td align="left" valign="top"><asp:DropDownList CssClass="TextboxSmall" ID="ddlCustomer" AutoPostBack="true" runat="server"
             Width ="100%" onselectedindexchanged="ddlCustomer_SelectedIndexChanged"></asp:DropDownList><br />
            <asp:RequiredFieldValidator ControlToValidate="ddlCustomer" ValidationGroup="addItem" EnableClientScript="true" InitialValue="0"
             ErrorMessage="Select Customer" ID="RequiredFieldValidator4"  runat="server" /></td>
        <td align="right" valign="top">Customer Order No.&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
        <td align="left" valign="top"><asp:TextBox  Width ="100%" ID="poReference" runat="server"   CssClass="TextboxSmall" /><br />
            <asp:RequiredFieldValidator ControlToValidate="poReference" ValidationGroup="addItem" EnableClientScript="true"
             ErrorMessage="Field cannot be empty" ID="RequiredFieldValidator6" runat="server" /></td>
        <td align="right" valign="top">Customer Order Date &nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span><br />(dd/mm/yyyy)</td>
        <td align="left" valign="top"><asp:TextBox ID="poDate" onkeyup="document.getElementById(this.id).value=''" autocomplete='off'
             runat="server"  CssClass="TextboxSmall" />
            <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="poDate"></cc1:CalendarExtender><br />
            <asp:RequiredFieldValidator ControlToValidate="poDate" ValidationGroup="addItem" EnableClientScript="true"
             ErrorMessage="Po date cannot be empty" ID="RequiredFieldValidator7" runat="server" /></td>
    </tr>
    <tr><td align="right" valign="top">Installation Address</td>
        <td align="left" valign="top"><asp:TextBox ID="installationAddress" TextMode="MultiLine" Height="50px" Width ="100%" runat="server"
             CssClass="TextboxSmall" ReadOnly="True" /></td>
        <td align="right" valign="top">Billing Address</td>
        <td align="left" valign="top"><asp:TextBox ID="BillingAddress" TextMode="MultiLine" Height="50px" Width ="100%" runat="server"
             CssClass="TextboxSmall" ReadOnly="True" /></td>
        <td align="right" valign="top"></td>
        <td ><asp:Label ID="message1" runat="server"></asp:Label></td>
    </tr>
    <tr><td align="right" valign="top">&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="right" valign="top">&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="right" valign="top">&nbsp;</td>
        <td >&nbsp;</td>
    </tr></table>
    </div>
    <!--Content Table End-->
    <div class="Line"></div>
    <div class="PageHeading">Add New Customer Order Details</div>
    <div class="Line"></div>
    <!-- the po line input table -->
    <div class="PageContent">
        <div id="tblItem" runat="server">
        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
        <tr><td width="19%" align="center" valign="middle">Division&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="10%" align="center" valign="middle">SubDivision&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="10%" align="center" valign="middle">Item&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="20%" align="center" valign="middle">Item Description</td>
            <td width="14%" align="center" valign="middle">Quantity&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="9%" align="center" valign="middle">Item type FOC</td>
            <td width="9%" align="center" valign="middle">Price&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="9%" align="center" valign="middle">Discount&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td>&nbsp;</td>
        </tr>
        <tr><td><asp:DropDownList ID="divisionDdl" CssClass="TextboxSmall" runat="server"/></td>
            <td><asp:DropDownList ID="subDivisionDdl" CssClass="TextboxSmall" runat="server"></asp:DropDownList></td>
            <td><asp:DropDownList ID="itemDdl" runat="server" CssClass="TextboxSmall" AutoPostBack="true"
             OnSelectedIndexChanged="itemDdl_SelectedIndexChanged" ></asp:DropDownList></td>
            <td>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" 
                     RenderMode="Inline">
                     <ContentTemplate>
                        <asp:TextBox ID="itemDescription" runat="server" ReadOnly="True" 
                        TextMode="MultiLine"></asp:TextBox>
                        <input type="hidden" value="0" runat="server" id="hmaxprice" />
                        <input type="hidden" value="0" runat="server" id="hvat" />  
                        <asp:Label ID="lblAvailable" runat="server" />                      
                </ContentTemplate>        
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="itemDdl" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>  
                </td>
            <td><asp:TextBox ID="quantity" MaxLength="6" runat="server" Width="50"  />&nbsp;
            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="quantity" ValidChars="1234567890." FilterMode="ValidChars" FilterType="Custom"></cc1:FilteredTextBoxExtender>
                <asp:Label ID="uom" runat="server" /></td>
            <td><asp:CheckBox ID="chkItemType" AutoPostBack="true" runat="server" oncheckedchanged="chkItemType_CheckedChanged" /></td>
            <td><asp:TextBox ID="price" MaxLength="10"  onblur="CalculateVatAmount();" 
                    runat="server" Width="60"
              ontextchanged="price_TextChanged" />
             <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="price" ValidChars="1234567890." FilterMode="ValidChars" FilterType="Custom"></cc1:FilteredTextBoxExtender></td>
            <td><asp:TextBox ID="discount" MaxLength="10" runat="server" Width="60" onkeypress="return isNumberKey(event);" /></td>
            <td><asp:Button ID="addItemLine" runat="server" Text="Add" ValidationGroup="addItem" CssClass="ButtonBig" CausesValidation="true"
             onclick="addItemLine_Click" /></td>
             
             <cc1:CascadingDropDown ID="CascadingDropDown3"
            runat="server" 
            TargetControlID="divisionDdl" LoadingText="[Loading...]"
            Category="Division"
            PromptText="Select a Division"
            ServicePath ="~/divisionService.asmx"
            ServiceMethod="GetDivision" >   </cc1:CascadingDropDown>
              
           <cc1:CascadingDropDown ID="CascadingDropDown1"
                runat="server" ParentControlID = "divisionDdl" 
                TargetControlID="subDivisionDdl" LoadingText="[Loading...]"
                Category="subDivision"
                PromptText="Select a subDivision"
                ServicePath ="~/divisionService.asmx"
                ServiceMethod="GetSubdividsionForDivision" >   </cc1:CascadingDropDown>

          <cc1:CascadingDropDown ID="CascadingDropDown2"
                runat="server" ParentControlID = "subDivisionDdl" 
                TargetControlID="itemDdl" LoadingText="[Loading...]"
                Category="Item"
                PromptText="Select Item"
                ServicePath ="~/divisionService.asmx"
                ServiceMethod="GetItemForSubdividsion" >   </cc1:CascadingDropDown>
        </tr>
        <tr><td width="19%" align="center" valign="top"><asp:RequiredFieldValidator InitialValue="0" ValidationGroup="addItem" runat="server" 
             EnableClientScript="true" ControlToValidate="divisionDdl" ErrorMessage="Select Division" ID="divisionRequired" Display="Dynamic" />
            &nbsp;</td>
            <td width="10%" align="center" valign="top"><asp:RequiredFieldValidator InitialValue="0" ValidationGroup="addItem"
             EnableClientScript="true" ControlToValidate="subDivisionDdl" ErrorMessage="Select SubDivision" ID="subDivisionRequired" 
             Display="Dynamic" runat="server" /></td>
            <td width="10%" align="center" valign="top"><asp:RequiredFieldValidator InitialValue="0" ValidationGroup="addItem" Display="Dynamic" 
             EnableClientScript="true" ControlToValidate="itemDdl" ErrorMessage="Select Item" ID="itemRequired" runat="server" /></td>
            <td width="20%" align="center" valign="middle"></td>
            <td width="14%" align="center" valign="middle"><asp:RequiredFieldValidator ID="quantityRequired" runat="server" Text=""
             EnableClientScript="true" Display="Dynamic" ValidationGroup="addItem" ErrorMessage="Quantity cannot be empty." 
             ControlToValidate="quantity" />
                <%--<asp:RegularExpressionValidator ID="quanityRegexValidator" runat="server" EnableClientScript="true" Display="none"
                 ValidationGroup="addItem" ErrorMessage="Quantity must be a integer" Text="" ControlToValidate="quantity" 
                 ValidationExpression="\d+" />--%></td>
            <td width="9%" align="center" valign="middle"></td>
            <td width="9%" align="center" valign="middle"><asp:RequiredFieldValidator ID="priceRequired" runat="server" Text="" Display="Dynamic"
             EnableClientScript="true" ValidationGroup="addItem" ErrorMessage="Price cannot be empty." ControlToValidate="price" />
                <asp:RegularExpressionValidator ID="priceRegexValidator" runat="server" EnableClientScript="true" Display="Dynamic"
                 ValidationGroup="addItem" ErrorMessage="Invalid price" Text="" ControlToValidate="price" ValidationExpression="^\d*\.?\d{2}$" />
            </td>
            <td width="9%" align="center" valign="middle"><asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
             EnableClientScript="true" Display="Dynamic" ValidationGroup="addItem" ErrorMessage="Invalid discount" Text="" 
             ControlToValidate="discount" ValidationExpression="^\d*\.?\d{2}$" /></td>
            <td>&nbsp;</td>
        </tr><asp:Label ID="Label1" runat="server" /></table>
        </div>
    </div>
    <!-- over here we should the bottom grid where the line details of this purchase order are shown -->
    <!--Grid Table Start-->
    <div class="Line"></div>
    <div class="PageHeading">Purchase Order Items</div>
    <%--<%#DataBinder.Eval(Container.DataItem, "uomName")%>--%>
    <div class="Line"></div>
    <asp:DataGrid ID="poLineGrid" runat="server" AutoGenerateColumns="false" DataKeyField="soirLineId" CellPadding="0"
     HeaderStyle-CssClass="GridHeading" ItemStyle-CssClass="GridData" GridLines="None" Width="100%"   onitemcommand="poLineGrid_ItemCommand" 
     onitemdatabound="poLineGrid_ItemDataBound" onitemcreated="poLineGrid_ItemCreated" onprerender="poLineGrid_PreRender" >
        <Columns>
            <asp:BoundColumn DataField="divisionName" HeaderText="Division" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true" />
            <asp:BoundColumn DataField="subdivisionName" HeaderText="Sub Division" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true" />
            <asp:BoundColumn DataField="itemCode" HeaderText="Item" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true" />
            <asp:TemplateColumn HeaderText="Quantity" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                <input type="hidden" id="lineId" value='<%# DataBinder.Eval(Container.DataItem, "soirLineId") %>' runat="server"  />
                <input type="hidden" id="itemId" value='<%# DataBinder.Eval(Container.DataItem, "itemId") %>' runat="server"  />
                <asp:TextBox ID="itemQty" CssClass="TextboxSmall" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "quantity") %>' Width="50"
                  />
                  <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="itemQty" ValidChars="1234567890." FilterMode="ValidChars" FilterType="Custom"></cc1:FilteredTextBoxExtender>
                <%#DataBinder.Eval(Container.DataItem, "uomName")%>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Price" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                <asp:TextBox ID="itemPrice" runat="server" CssClass="TextboxSmall" Text='<%# DataBinder.Eval(Container.DataItem, "price") %>' Width="60"
                 onkeypress="return isNumberKey(event);" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn Visible =false  DataField="vat"  HeaderText="VAT %" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Left" />
            <asp:TemplateColumn HeaderText="Discount" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                <asp:TextBox ID="itemDiscount" CssClass="TextboxSmall" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "discount") %>' Width="60"
                 onkeypress="return isNumberKey(event);" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="total" HeaderText="Total" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Left"
             DataFormatString="{0:#,###.##}" />
            <asp:ButtonColumn ButtonType="PushButton" Text="Update" CommandName="Edit" />
            <asp:ButtonColumn ButtonType="PushButton" Text="Delete" CommandName="Delete" />
            <asp:TemplateColumn Visible="false">
                <ItemTemplate>
                <input type="hidden" id="isDeleted" value='<%#DataBinder.Eval(Container.DataItem, "isDeleted") %>' runat="server" />
                </ItemTemplate>
            </asp:TemplateColumn>  
            <asp:TemplateColumn Visible="false">
                <ItemTemplate>
                <input type="hidden" id="isLocked" value='<%#DataBinder.Eval(Container.DataItem, "isLocked") %>' runat="server" />
                </ItemTemplate>
            </asp:TemplateColumn>  
        </Columns>
    </asp:DataGrid>
    <asp:Label ID="gridMessage" runat="server" />
    <div class="Line"></div>
    <div class="PageHeading">FOC Item Details</div>
    <div class="Line"></div>
    <asp:DataGrid ID="focGrid" runat="server" AutoGenerateColumns="false" DataKeyField="soirLineId" CellPadding="0"
     HeaderStyle-CssClass="GridHeading" ItemStyle-CssClass="GridData" GridLines="None" Width="100%" onitemcommand="focGrid_ItemCommand" 
     onitemcreated="focGrid_ItemCreated" onprerender="focGrid_PreRender" >
        <Columns>
            <asp:BoundColumn DataField="divisionName" HeaderText="Division" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Left" />
            <asp:BoundColumn DataField="subdivisionName" HeaderText="Sub Division" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Left" />
            <asp:BoundColumn DataField="itemCode" HeaderText="Item" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Left" />
            <asp:TemplateColumn HeaderText="Quantity" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                <input type="hidden" id="lineId" value='<%# DataBinder.Eval(Container.DataItem, "soirLineId") %>' runat="server"  />
                <input type="hidden" id="itemId" value='<%# DataBinder.Eval(Container.DataItem, "itemId") %>' runat="server"  />
                <asp:TextBox ID="itemQty" CssClass="TextboxSmall" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "quantity") %>' Width="50"
                 onkeypress="return isNumberKey_WO_Decimal(event);" />
                <%#DataBinder.Eval(Container.DataItem, "uomName")%>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="price" HeaderText="Price" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Left" />
            <asp:BoundColumn DataField="discount" HeaderText="Discount" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Left" />
            <asp:BoundColumn DataField="total" HeaderText="Total" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Left"
             DataFormatString="{0:#,###.##}" />
            <asp:ButtonColumn ButtonType="PushButton" Text="Update" CommandName="Edit" />
            <asp:ButtonColumn ButtonType="PushButton" Text="Delete" CommandName="Delete" />
            <asp:TemplateColumn Visible="false">
                <ItemTemplate>
                <input type="hidden" id="isDeleted" value='<%#DataBinder.Eval(Container.DataItem, "isDeleted") %>' runat="server" />
                </ItemTemplate>
            </asp:TemplateColumn>              
        </Columns>
    </asp:DataGrid>
    <asp:Label ID="focMessage" runat="server" />
    <div class="Line"></div>
    <div class="PageContent">
    <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
    <tr><td  align="right" valign="top">Packing/Forwarding/Insurance Charge</td>
        <td align="left"><asp:TextBox ID="txtCharge" runat="server"  MaxLength="10" CssClass="TextboxSmall"
             onkeypress="return isNumberKey(event);" /><br />
            <asp:RangeValidator ID="RangeValidator2" ControlToValidate="txtCharge" runat="server"
             ErrorMessage="Please give correct decimal number" Type="Double" MaximumValue="1000000"></asp:RangeValidator></td>
        <td  align="right" valign="top">Buy back amount</td>
        <td  align="left" valign="top"><asp:TextBox ID="txtBuyBackamount" runat="server" MaxLength="10" CssClass="TextboxSmall"
             onkeypress="return isNumberKey(event);" /><br />
            <asp:RangeValidator ID="RangeValidator1" ControlToValidate="txtBuyBackamount" runat="server"
             ErrorMessage="Please give correct decimal number" Type="Double" MaximumValue="1000000"></asp:RangeValidator></td>
        <td align="right" valign="top">Buy back Details</td>
        <td  align="left" valign="top"><asp:TextBox ID="txtDetails" TextMode="MultiLine" Height="50px" runat="server" CssClass="TextboxSmall"/></td>
    </tr></table>
    </div>
    <div class="Line"></div>
    <div class="PageContent">  
    <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
    <tr><td class="GridData" align="right"> Total Quantity :   <asp:Label ID="lblquantity" runat="server"></asp:Label></td>
        <td class="GridData"></td>
        <td width="50px" class="GridData" align="right">Total</td>
        <td  class="GridData" align="left"><asp:Label ID="lblTotal" runat="server"></asp:Label></td>
    </tr>
    <tr><td align="right" class="GridData" width="30%">Price in Words</td>
        <td align="left" class="GridData" colspan="3"><asp:Label ID="lblPrice" runat="server"></asp:Label></td>
    </tr></table>
    </div>
    <div class="Line"></div>
    <div class="PageHeading">Service Instruction/Payment Details</div>
    <div class="Line"></div>
</div>
<div class="PageContent">  
<table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
<tr><td  align="right" valign="top">Installation Required on</td>
    <td  align="left" valign="top"><asp:TextBox ID="txtInstRdate" runat="server" onkeyup="document.getElementById(this.id).value=''"
     autocomplete='off'  CssClass="TextboxSmall" />
        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" TargetControlID="txtInstRdate"></cc1:CalendarExtender></td>
    <td  align="right" valign="top">Special Installation Advice</td>
    <td align="left" valign="top"><asp:TextBox ID="txtInstAdvice" runat="server" CssClass="TextboxSmall" /></td>
    <td align="right" valign="top">Service Agreement type</td>
    <td align="left" valign="top"><asp:DropDownList CssClass="TextboxSmall" ID="ddlServiceAgreement" runat="server" Width="150px"></asp:DropDownList>
    </td>    
</tr>
<tr><td align="right" valign="top">Warranty Terms</td>
    <td align="left" valign="top"><asp:DropDownList CssClass="TextboxSmall" ID="ddlWarrantity" runat="server" Width="150px"></asp:DropDownList></td>
    <td  align="right" valign="top">Service Rate</td>
    <td  align="left" valign="top"><asp:TextBox ID="txtServiceRate" runat="server" MaxLength="10"  CssClass="TextboxSmall"
         onkeypress="return isNumberKey(event);" /><br />
        <asp:RangeValidator ID="RangeValidator3" ControlToValidate="txtServiceRate" runat="server"
         ErrorMessage="Please give correct decimal number" Type="Double" MaximumValue="1000000"></asp:RangeValidator></td>
    <td  align="right" valign="top">Payment Terms</td>
    <td  align="left" valign="top"><asp:TextBox ID="txtpaymentTerms" runat="server"   CssClass="TextboxSmall" /></td>
</tr>
<tr><td  align="right" valign="top">Payment due Date(dd/mm/yyyy)</td>
    <td  align="left" valign="top"><asp:TextBox ID="txtDueDate" runat="server" onkeyup="document.getElementById(this.id).value=''"
         autocomplete='off' CssClass="TextboxSmall" />
        <cc1:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MM/yyyy" TargetControlID="txtDueDate"></cc1:CalendarExtender></td>
</tr></table>
</div>
<!--Content Table End-->
<div class="Line"></div>
<div align="center"><asp:Label ID="message" runat="server" /></div>
<div class="Line"></div>
<table width="90%">
<tr><td align="center">&nbsp;<asp:Button ID="add" Text="Submit" runat="server" CssClass="Button" Width="96px" onclick="add_Click" />&nbsp;
        <asp:Button ID="cancel" Text="Cancel" runat="server"  CausesValidation="false" CssClass="Button" Width="90px" />&nbsp; <asp:Button ID="btnPrint" runat="server" CssClass="Button" onclick="btnPrint_Click" Text="Print" /></td>
</tr></table>
<!--Page Body End-->
<input type="hidden" value="0" runat="server" id="strsoirId" />

</asp:Content>

