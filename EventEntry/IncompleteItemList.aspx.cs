﻿using System.Data;
using System;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;

public partial class EventEntry_IncompleteItemList : BasePage
{
  
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }

        if (!IsPostBack)
        {
            BindGrid();
            
        }
        if (GetLoggedinUser().UserType == 2)
        {

            DetachItem.Enabled = false;
        }

    }
    private void BindGrid()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(@"SELECT g.grndetailsSerialNoId,g.serialNo,I.itemCode FROM Lum_Erp_GrndetailsSerialNumber g inner join Lum_Erp_Item I on I.itemId=g.itemId where inComplete=1 AND From_Detach IS NULL order by g.grndetailsSerialNoId desc"))
            {
                itemGrid.DataSource = reader;
                itemGrid.DataBind();
            }
        }

    }
    protected string GetUrl(object grndetailsSerialNoId)
    {
        string retVal = "";

        retVal = "ItemDetachment.aspx?grndetailsSerialNoId=" + grndetailsSerialNoId;
        
        return retVal;

    }
    protected void DetachItem_Click(object sender, EventArgs e)
    {
        Response.Redirect("ItemDetachment.aspx");
    }
    protected void itemGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

                   
            if (GetLoggedinUser().UserType == 2)
            {

                 e.Item.Cells[2].Controls[0].Visible=false;
            }
        }
    }
}
