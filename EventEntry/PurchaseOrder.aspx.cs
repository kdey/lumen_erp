﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Drawing;
using Bitscrape.AppBlock.Database;


public partial class EventEntry_PurchaseOrder : BasePage, ICallbackEventHandler
{
    public DataTable aTable = new DataTable("PoOrder");
    string RenderedOutput = "";
    protected int poId = -1;
    protected void Page_Load(object sender, EventArgs e)
    {
        message.Text = "";
        ClientScriptManager scriptMgr = Page.ClientScript;

        String cbReference = scriptMgr.GetCallbackEventReference(this, "arg", "ReceiveServerData", "");

        String callbackScript = "function CallServer(arg, context) {" + cbReference + "; }";

        scriptMgr.RegisterClientScriptBlock(this.GetType(), "CallServer", callbackScript, true);
        try
        {
        if (Request.Params["poId"] != null)
        {
            poId = int.Parse(Request.Params["poId"]);
        }
        } 
        catch(Exception ex)
            {
                message.Text = ex.Message;
                message.ForeColor = Color.Green;
            }
       
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!IsPostBack)
        {

            BindControl();
            
            

                if (poId != -1)
                {
                    BindPorder();
                    BindPorderDetails();
                }
                else
                {
                    CreateDataTable();
                }
           
           
        }

        
    }

    public void BindControl()
    {
        BindListControl _control = new BindListControl();
        _control.BindOrderType(ddlOrderType);
        _control.BindVendor(ddlVendor);
        _control.BindRateContractType(ddlRateContractType);
    }
    public void CreateDataTable()
    {
        
        DataRow dtRow;
        DataColumn dtCol;

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "Division";
        aTable.Columns.Add(dtCol);
        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "SubDivision";
        aTable.Columns.Add(dtCol);
        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "ItemName";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "ItemDesc";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "Qty";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "UOM";
        aTable.Columns.Add(dtCol);


        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "Price";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "VAT";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "VATamount";
        aTable.Columns.Add(dtCol);
        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "Total";
        aTable.Columns.Add(dtCol);


        dtRow = aTable.NewRow();
        dtRow["Division"] = "";
        dtRow["SubDivision"] = "";
        dtRow["ItemName"] = "";
        dtRow["ItemDesc"] = "";
        dtRow["Qty"] = "";
        dtRow["Price"] = "0.00";
        dtRow["VAT"] = "0.00";
        dtRow["VATamount"] = "0.00";
        dtRow["Total"] = "0.00";

        aTable.Rows.Add(dtRow);

        poGridView.DataSource = aTable;
       poGridView.DataBind();
    }
    protected void poGridView_PreRender(object sender, EventArgs e)
    {
        if (aTable.Rows.Count > 0)
        {
            for (int _index = 0; _index < poGridView.Rows.Count; _index++)
            {
                DropDownList _ddlDivision = (DropDownList)poGridView.Rows[_index].FindControl("ddlDivision");
                DropDownList _ddlSubDivision = (DropDownList)poGridView.Rows[_index].FindControl("ddlSubDivision");
                DropDownList _ddlItem = (DropDownList)poGridView.Rows[_index].FindControl("ddlItem");
                DropDownList _ddlUOM = (DropDownList)poGridView.Rows[_index].FindControl("ddlUOM");

                TextBox _txtItemDesc = (TextBox)poGridView.Rows[_index].FindControl("txtItemDesc");
                TextBox _txtQty = (TextBox)poGridView.Rows[_index].FindControl("txtQty");
                TextBox _txtVatAmount = (TextBox)poGridView.Rows[_index].FindControl("txtVatAmount");
                TextBox _txtVat = (TextBox)poGridView.Rows[_index].FindControl("txtVat");
                TextBox _txtTotal = (TextBox)poGridView.Rows[_index].FindControl("txtTotal");
                TextBox _txtPrice = (TextBox)poGridView.Rows[_index].FindControl("txtPrice");
                _txtItemDesc.Text = aTable.Rows[_index]["ItemDesc"].ToString();
                _txtQty.Text = aTable.Rows[_index]["Qty"].ToString();
                _txtVat.Text = aTable.Rows[_index]["VAT"].ToString();
                _txtVatAmount.Text = aTable.Rows[_index]["VATamount"].ToString();
                _txtTotal.Text = aTable.Rows[_index]["Total"].ToString();
                _txtPrice.Text = aTable.Rows[_index]["Price"].ToString();
                _txtItemDesc.Attributes.Add("readonly", "readonly");
                _txtTotal.Attributes.Add("readonly", "readonly");
                _txtVat.Attributes.Add("readonly", "readonly");
                _txtVatAmount.Attributes.Add("readonly", "readonly");
                BindListControl _control = new BindListControl();

                _control.BindDivision(_ddlDivision);
                _control.BindpOUM(_ddlUOM);
                _ddlDivision.SelectedValue = aTable.Rows[_index]["Division"].ToString();
                _ddlUOM.SelectedValue = aTable.Rows[_index]["UOM"].ToString();

                if (_ddlDivision.SelectedValue != "0")
                {

                    _control.BindSubDivision(_ddlSubDivision, aTable.Rows[_index]["Division"].ToString());
                    _ddlSubDivision.SelectedValue = aTable.Rows[_index]["SubDivision"].ToString();
                    _control.BindItems(_ddlItem,  aTable.Rows[_index]["SubDivision"].ToString());
                    _ddlItem.SelectedValue = aTable.Rows[_index]["ItemName"].ToString();
                }
            }
        }
    }
    public void RaiseCallbackEvent(string param)
    {

        string[] commands = param.Split(',');

        DropDownList _ddlDivision = (DropDownList)poGridView.Rows[0].FindControl("ddlDivision");
        DropDownList _ddlSubDivision = (DropDownList)poGridView.Rows[0].FindControl("ddlSubDivision");
        DropDownList _ddlItem = (DropDownList)poGridView.Rows[0].FindControl("ddlItem");
        BindListControl _control = new BindListControl();
        if (commands[0].Contains("ddlSubDivision"))
        {
            if (commands[1] != "0")
            {
                _control.BindSubDivision(_ddlSubDivision, commands[1]);
                RenderedOutput = commands[0] + "#";
                for (int index = 1; index < _ddlSubDivision.Items.Count; index++)
                {
                    this.RenderedOutput = RenderedOutput + "~" + _ddlSubDivision.Items[index].Value + "|" + _ddlSubDivision.Items[index].Text;

                }
            }
            else
            {
                RenderedOutput = commands[0] + "#";
             this.RenderedOutput = RenderedOutput + "~" + "Please Select" + "|" + "Please Select";
            }

        }
        else if (commands[0].Contains("ddlItem"))
        {
            
            if (!commands[2].Contains("Please Select"))
            {
                _control.BindItems(_ddlItem,  commands[2]);
                RenderedOutput = commands[0] + "#";
                for (int index = 1; index < _ddlItem.Items.Count; index++)
                {
                    this.RenderedOutput = RenderedOutput + "~" + _ddlItem.Items[index].Value + "|" + _ddlItem.Items[index].Text;

                }
            }
            else
            {
                RenderedOutput = commands[0] + "#";
           this.RenderedOutput = RenderedOutput + "~" + "Please Select" + "|" + "Please Select";
            }
        }
        else if (commands[0].Contains("ddlUOM"))
        {
            if (!commands[1].Contains("Please Select"))
            {

                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    string Sqlquery = string.Format("SELECT itemDescription,uomId,vat FROM Item WHERE itemId={0}", commands[1]);
                    DataSet _Ds = conn.GetDataSet(Sqlquery, "Description");

                    if (_Ds.Tables[0].Rows.Count > 0)
                    {
                        RenderedOutput = commands[0] + "#";
                        RenderedOutput = RenderedOutput + _Ds.Tables[0].Rows[0]["itemDescription"].ToString();
                        RenderedOutput = RenderedOutput + "~" + _Ds.Tables[0].Rows[0]["vat"].ToString();
                        RenderedOutput = RenderedOutput + "~" + _Ds.Tables[0].Rows[0]["uomId"].ToString();
                    }
                }
            }
            else
            {
                RenderedOutput = commands[0] + "#";
                this.RenderedOutput = RenderedOutput + "Please Select" + "~" + "Please Select";
            }
            
        }

    }
    public string GetCallbackResult()
    {

        //return rendered string with command name

        return RenderedOutput;

    }
    protected void btnAddNewItem_Click(object sender, EventArgs e)
    {
        StoreData();
    }
    public void StoreData()
    {
        DataRow dtRow;
        DataColumn dtCol;

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "Division";
        aTable.Columns.Add(dtCol);
        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "SubDivision";
        aTable.Columns.Add(dtCol);
        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "ItemName";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "ItemDesc";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "Qty";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "UOM";
        aTable.Columns.Add(dtCol);


        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "Price";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "VAT";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "VATamount";
        aTable.Columns.Add(dtCol);
        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "Total";
        aTable.Columns.Add(dtCol);

        bool chkBlankfield = false;
        for (int _index = 0; _index < poGridView.Rows.Count; _index++)
        {
            DropDownList _ddlDivision = (DropDownList)poGridView.Rows[_index].FindControl("ddlDivision");
            HtmlInputHidden _HdSubDivision = (HtmlInputHidden)poGridView.Rows[_index].FindControl("HdSubDivision");
            HtmlInputHidden _HdItem = (HtmlInputHidden)poGridView.Rows[_index].FindControl("HdItem");
            DropDownList _ddlUOM = (DropDownList)poGridView.Rows[_index].FindControl("ddlUOM");

            TextBox _txtItemDesc = (TextBox)poGridView.Rows[_index].FindControl("txtItemDesc");
            TextBox _txtQty = (TextBox)poGridView.Rows[_index].FindControl("txtQty");
            TextBox _txtVatAmount = (TextBox)poGridView.Rows[_index].FindControl("txtVatAmount");
            TextBox _txtVat = (TextBox)poGridView.Rows[_index].FindControl("txtVat");
            TextBox _txtTotal = (TextBox)poGridView.Rows[_index].FindControl("txtTotal");
            TextBox _txtPrice = (TextBox)poGridView.Rows[_index].FindControl("txtPrice");

           
                dtRow = aTable.NewRow();

                dtRow["Division"] = _ddlDivision.SelectedValue;
                dtRow["SubDivision"] = _HdSubDivision.Value;
                dtRow["ItemName"] = _HdItem.Value;
                dtRow["ItemDesc"] = _txtItemDesc.Text;
                dtRow["Qty"] = _txtQty.Text;
                dtRow["UOM"] = _ddlUOM.SelectedValue;
                dtRow["Price"] = _txtPrice.Text;
                dtRow["VAT"] = _txtVat.Text;
                dtRow["VATamount"] = _txtVatAmount.Text;
                dtRow["Total"] = _txtTotal.Text;
                aTable.Rows.Add(dtRow);

                if ((_HdItem.Value.Length > 0) && ((_txtQty.Text.Length > 0)))
                {
                    chkBlankfield = false;

                }
                else
                {
                    chkBlankfield = true;
                    break;
                }
            

        }
        if (!chkBlankfield)
        {
            dtRow = aTable.NewRow();
            dtRow["Division"] = "";
            dtRow["SubDivision"] = "";
            dtRow["ItemName"] = "";
            dtRow["ItemDesc"] = "";
            dtRow["Qty"] = "";
            dtRow["UOM"] = "";
            dtRow["Price"] = "0.00";
            dtRow["VAT"] = "0.00";
            dtRow["VATamount"] = "0.00";
            dtRow["Total"] = "0.00";
            aTable.Rows.Add(dtRow);
        }
        else
        {
            Page.RegisterClientScriptBlock("alert", "<script>alert('filed cant be blank')</script>");
        }
        poGridView.DataSource = aTable;
        poGridView.DataBind();

    }
    protected void add_Click(object sender, EventArgs e)
    {
        UpdateTable();
        if (poId == -1)
        {
            if (aTable.Rows.Count > 0)
            {
                AddPorder();
                ResetControl();
            }
            else
            {
                message.Text = "Purchase Order Cant be created with zero item";
                message.ForeColor = Color.Red; 
            }
           
        }
        else
        {
            if (aTable.Rows.Count > 0)
            {
                UpdatePorder();
            }
            ResetControl();
        }
      
    }
    public void UpdatePorder()
    {
        int intPOtype = Convert.ToInt32(ddlOrderType.SelectedValue);
        int intVendorId = Convert.ToInt32(ddlVendor.SelectedValue);
        int intRateContractType = Convert.ToInt32(ddlRateContractType.SelectedValue);
        System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-GB");
        DateTime dtPoDate = Convert.ToDateTime(poDate.Text.Split(' ')[0]);
        string[] _param = poDate.Text.Split('/');

        string []getVersion = strPoId.Value.Split('/');
        int version = Convert.ToInt32(getVersion[getVersion.Length - 1]) + 1;
        string _PoOrderId = "";

        for (int index = 0; index < getVersion.Length - 1;index++ )
        {
            if (_PoOrderId == "")
            {
                _PoOrderId = getVersion[index];
            }
            else
            {
                _PoOrderId = _PoOrderId + "/" + getVersion[index];
            }
        }
        _PoOrderId = _PoOrderId + "/" + Convert.ToString(version);

        try
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                SpParam _spPoId = new SpParam("@strpoId", _PoOrderId, SqlDbType.VarChar);
                SpParam _spPOtype = new SpParam("@poType", intPOtype, SqlDbType.Int);
                SpParam _spVendorId = new SpParam("@vendorId", intVendorId, SqlDbType.Int);
                SpParam _spDate = new SpParam("@poDate", dtPoDate, SqlDbType.DateTime);
                SpParam _spContractType = new SpParam("@rateContractTypeId", intRateContractType, SqlDbType.Int);
                SpParam _poId = new SpParam("@poId", poId, SqlDbType.Int);
                string Sqlquery = "UpdatePurchaseOrder";
                int i = conn.ExecuteCommandProc(Sqlquery, _spPoId, _spPOtype, _spVendorId, _spDate, _spContractType, _poId);


            }
        }
        catch (Exception ex)
        {
            message.Text = ex.Message;
        }
        if (poId != -1)
        {
            try
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    SpParam _spPoid = new SpParam("@poId", poId, SqlDbType.Int);
                    
                    int i = conn.ExecuteCommandProc("DeletePoDetails",_spPoid);
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }
        if(poGridView.Rows.Count>0)
        {
            for (int _index = 0; _index < poGridView.Rows.Count; _index++)
            {
                int intDivisionId = Convert.ToInt32(aTable.Rows[_index]["Division"].ToString());
                int intSubDivisionId = Convert.ToInt32(aTable.Rows[_index]["SubDivision"].ToString());
                int intItemCode = Convert.ToInt32(aTable.Rows[_index]["ItemName"].ToString());
                int intQuantity = Convert.ToInt32(aTable.Rows[_index]["Qty"].ToString());
                int intUOMId = Convert.ToInt32(aTable.Rows[_index]["UOM"].ToString());
                decimal dmlPrice = Convert.ToDecimal(aTable.Rows[_index]["Price"].ToString());
                decimal dmlVAT = Convert.ToDecimal(aTable.Rows[_index]["VAT"].ToString());

                decimal dmlVatAmount = Convert.ToDecimal(aTable.Rows[_index]["VATamount"].ToString());
                decimal dmlTotal = dmlPrice * Convert.ToDecimal(intQuantity) + dmlVatAmount;
                try
                {
                    using (IConnection conn = DataConnectionFactory.GetConnection())
                    {
                        SpParam _spOderId = new SpParam("@poId", poId, SqlDbType.Int);
                        SpParam _spDivisionId = new SpParam("@divisionId", intDivisionId, SqlDbType.Int);
                        SpParam _spSubDivisionId = new SpParam("@subdivisionId", intSubDivisionId, SqlDbType.Int);
                        SpParam _spItemCode = new SpParam("@itemId", intItemCode, SqlDbType.Int);
                        SpParam _spQuantity = new SpParam("@quantity", intQuantity, SqlDbType.Int);
                        SpParam _spUOMId = new SpParam("@uomId", intUOMId, SqlDbType.Int);
                        SpParam _spPrice = new SpParam("@price", dmlPrice, SqlDbType.Decimal);
                        SpParam _spVAT = new SpParam("@vat", dmlVAT, SqlDbType.Decimal);
                        SpParam _spdmlTotal = new SpParam("@total", dmlTotal, SqlDbType.Decimal);
                        SpParam _spdmlVatAmount = new SpParam("@vatAmount", dmlVatAmount, SqlDbType.Decimal);
                        string Sqlquery = "AddPOrderDetails";
                        int _i = conn.ExecuteCommandProc(Sqlquery, _spOderId, _spDivisionId, _spSubDivisionId, _spItemCode, _spQuantity, _spUOMId, _spPrice, _spVAT, _spdmlTotal, _spdmlVatAmount);

                        message.Text = "Purchase Order " + _PoOrderId + "  for Vendor Name " + ddlVendor.Items[ddlVendor.SelectedIndex].Text;
                        message.ForeColor = Color.Green;
                    }
                }
                catch (Exception ex)
                {
                    message.Text = ex.Message;
                }

            }
        }
    }
    public void AddPorder()
    {
        int intPOtype = Convert.ToInt32(ddlOrderType.SelectedValue);
        int intVendorId = Convert.ToInt32(ddlVendor.SelectedValue);
        int intRateContractType = Convert.ToInt32(ddlRateContractType.SelectedValue);
        System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-GB");
        DateTime dtPoDate = Convert.ToDateTime(poDate.Text.Split(' ')[0]);
        string[] _param = poDate.Text.Split('/');
        object _obj = null;
        string _PoOrderId = ddlOrderType.Items[ddlOrderType.SelectedIndex].Text +"/"+ _param[2] +"/"+ _param[1];
        try
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                SpParam _spPoId = new SpParam("@strpoId", _PoOrderId, SqlDbType.VarChar);
                SpParam _spPOtype = new SpParam("@poType", intPOtype, SqlDbType.Int);
                SpParam _spVendorId = new SpParam("@vendorId", intVendorId, SqlDbType.Int);
                SpParam _spDate = new SpParam("@poDate", dtPoDate, SqlDbType.DateTime);
                SpParam _spContractType = new SpParam("@rateContractTypeId", intRateContractType, SqlDbType.Int);

                string Sqlquery = "AddPurchaseOrder";
                _obj = conn.ExecuteScalarProc(Sqlquery, _spPoId, _spPOtype, _spVendorId, _spDate, _spContractType);


            }
        }
        catch (Exception ex)
        {
            message.Text = ex.Message;
        }

        for (int _index = 0; _index < aTable.Rows.Count; _index++)
        {
           
                int intDivisionId = Convert.ToInt32(aTable.Rows[_index]["Division"].ToString());
                int intSubDivisionId = Convert.ToInt32(aTable.Rows[_index]["SubDivision"].ToString());
                int intItemCode = Convert.ToInt32(aTable.Rows[_index]["ItemName"].ToString());
                int intQuantity = Convert.ToInt32(aTable.Rows[_index]["Qty"].ToString());
                int intUOMId = Convert.ToInt32(aTable.Rows[_index]["UOM"].ToString());
                decimal dmlPrice = Convert.ToDecimal(aTable.Rows[_index]["Price"].ToString());
                decimal dmlVAT = Convert.ToDecimal(aTable.Rows[_index]["VAT"].ToString());

                decimal dmlVatAmount = Convert.ToDecimal(aTable.Rows[_index]["VATamount"].ToString());
                decimal dmlTotal = dmlPrice * Convert.ToDecimal(intQuantity) + dmlVatAmount;
                try
                {
                    using (IConnection conn = DataConnectionFactory.GetConnection())
                    {
                        SpParam _spOderId = new SpParam("@poId", Convert.ToInt32(_obj), SqlDbType.Int);
                        SpParam _spDivisionId = new SpParam("@divisionId", intDivisionId, SqlDbType.Int);
                        SpParam _spSubDivisionId = new SpParam("@subdivisionId", intSubDivisionId, SqlDbType.Int);
                        SpParam _spItemCode = new SpParam("@itemId", intItemCode, SqlDbType.Int);
                        SpParam _spQuantity = new SpParam("@quantity", intQuantity, SqlDbType.Int);
                        SpParam _spUOMId = new SpParam("@uomId", intUOMId, SqlDbType.Int);
                        SpParam _spPrice = new SpParam("@price", dmlPrice, SqlDbType.Decimal);
                        SpParam _spVAT = new SpParam("@vat", dmlVAT, SqlDbType.Decimal);
                        SpParam _spdmlTotal = new SpParam("@total", dmlTotal, SqlDbType.Decimal);
                        SpParam _spdmlVatAmount = new SpParam("@vatAmount", dmlVatAmount, SqlDbType.Decimal);
                        string Sqlquery = "AddPOrderDetails";
                        int _i = conn.ExecuteCommandProc(Sqlquery, _spOderId, _spDivisionId, _spSubDivisionId, _spItemCode, _spQuantity, _spUOMId, _spPrice, _spVAT, _spdmlTotal, _spdmlVatAmount);

                        message.Text = "Purchase Order " + _PoOrderId + "  for Vendor Name " + ddlVendor.Items[ddlVendor.SelectedIndex].Text;
                        message.ForeColor = Color.Green;
                    }
                }
                catch (Exception ex)
                {
                    message.Text = ex.Message;
                }

            }
        
    }
    public void UpdateTable()
    {
        DataRow dtRow;
        DataColumn dtCol;

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "Division";
        aTable.Columns.Add(dtCol);
        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "SubDivision";
        aTable.Columns.Add(dtCol);
        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "ItemName";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "ItemDesc";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "Qty";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "UOM";
        aTable.Columns.Add(dtCol);


        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "Price";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "VAT";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "VATamount";
        aTable.Columns.Add(dtCol);
        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "Total";
        aTable.Columns.Add(dtCol);
        for (int _index = 0; _index < poGridView.Rows.Count; _index++)
        {
            DropDownList _ddlDivision = (DropDownList)poGridView.Rows[_index].FindControl("ddlDivision");
            HtmlInputHidden _HdSubDivision = (HtmlInputHidden)poGridView.Rows[_index].FindControl("HdSubDivision");
            HtmlInputHidden _HdItem = (HtmlInputHidden)poGridView.Rows[_index].FindControl("HdItem");
            DropDownList _ddlUOM = (DropDownList)poGridView.Rows[_index].FindControl("ddlUOM");

            TextBox _txtItemDesc = (TextBox)poGridView.Rows[_index].FindControl("txtItemDesc");
            TextBox _txtQty = (TextBox)poGridView.Rows[_index].FindControl("txtQty");
            TextBox _txtVatAmount = (TextBox)poGridView.Rows[_index].FindControl("txtVatAmount");
            TextBox _txtVat = (TextBox)poGridView.Rows[_index].FindControl("txtVat");
            TextBox _txtTotal = (TextBox)poGridView.Rows[_index].FindControl("txtTotal");
            TextBox _txtPrice = (TextBox)poGridView.Rows[_index].FindControl("txtPrice");
            if (_ddlDivision.SelectedIndex != 0)
            {
                if (_HdItem.Value.Length > 0)
                {
                    if (_txtQty.Text.Length > 0)
                    {
                        if (_txtPrice.Text.Length > 0)
                        {
                            dtRow = aTable.NewRow();

                            dtRow["Division"] = _ddlDivision.SelectedValue;
                            dtRow["SubDivision"] = _HdSubDivision.Value;
                            dtRow["ItemName"] = _HdItem.Value;
                            dtRow["ItemDesc"] = _txtItemDesc.Text;
                            dtRow["Qty"] = _txtQty.Text;
                            dtRow["UOM"] = _ddlUOM.SelectedValue;
                            dtRow["Price"] = _txtPrice.Text;
                            dtRow["VAT"] = _txtVat.Text;
                            dtRow["VATamount"] = _txtVatAmount.Text;
                            dtRow["Total"] = _txtTotal.Text;

                            aTable.Rows.Add(dtRow);
                        }
                    }
                }
            }
        }


    }
    protected void cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("PurchaseOrder.aspx");
    }
    private void BindPorder()
    {

        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery("select * from PoOrder where poId = " + poId))
            {
                if (reader.Read())
                {
                    ddlVendor.SelectedValue = Convert.ToString(reader["vendorId"]);
                    ddlOrderType.SelectedValue = Convert.ToString(reader["poType"]);
                    ddlRateContractType.SelectedValue = Convert.ToString(reader["rateContractTypeId"]);
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-GB");
                    poDate.Text = Convert.ToString(Convert.ToDateTime( reader["poDate"])).Split(' ')[0];
                    strPoId.Value = Convert.ToString(reader["strPoId"]);
                    add.Text = "Edit";
                    cancel.Visible = true;
                }
            }
        }
    }
    public void BindPorderDetails()
    {
        DataRow dtRow;
        DataColumn dtCol;

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "Division";
        aTable.Columns.Add(dtCol);
        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "SubDivision";
        aTable.Columns.Add(dtCol);
        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "ItemName";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "ItemDesc";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "Qty";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "UOM";
        aTable.Columns.Add(dtCol);


        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "Price";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "VAT";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "VATamount";
        aTable.Columns.Add(dtCol);
        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "Total";
        aTable.Columns.Add(dtCol);


        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            try
            {
                using (DataSet _ds = conn.GetDataSet("select * from PoDetails where poId =" + poId, "Porder"))
                {

                    if (_ds.Tables[0].Rows.Count > 0)
                    {
                        for (int _index = 0; _index < _ds.Tables[0].Rows.Count; _index++)
                        {
                            dtRow = aTable.NewRow();
                            dtRow["Division"] = Convert.ToString(_ds.Tables[0].Rows[_index]["divisionId"]);
                            dtRow["SubDivision"] = Convert.ToString(_ds.Tables[0].Rows[_index]["subdivisionId"]) ;
                            dtRow["ItemName"] = Convert.ToString(_ds.Tables[0].Rows[_index]["itemId"]);
                            dtRow["ItemDesc"] =GetItemDescription(Convert.ToInt32(_ds.Tables[0].Rows[_index]["itemId"]));
                            dtRow["Qty"] = Convert.ToString(_ds.Tables[0].Rows[_index]["quantity"]);
                            dtRow["Price"] = Convert.ToString(_ds.Tables[0].Rows[_index]["price"]);
                            dtRow["VAT"] = Convert.ToString(_ds.Tables[0].Rows[_index]["vat"]);
                            dtRow["VATamount"] = Convert.ToString(_ds.Tables[0].Rows[_index]["vatAmount"]);
                            dtRow["Total"] = Convert.ToString(_ds.Tables[0].Rows[_index]["total"]);
                            dtRow["UOM"] = Convert.ToString(_ds.Tables[0].Rows[_index]["uomId"]);
                            aTable.Rows.Add(dtRow);
                        }
                        poGridView.DataSource = aTable;
                        poGridView.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
            }
        }



    }

    public void ResetControl()
    {
        ddlOrderType.SelectedIndex = 0;
        ddlRateContractType.SelectedIndex = 0;
        ddlVendor.SelectedIndex = 0;
        poDate.Text = string.Empty;
        poId = -1;
        poGridView.DataSource = null;
        poGridView.DataBind();
    }

    public string GetItemDescription(int _itemid)
    {
        string retVal = "";


        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            string Sqlquery = string.Format("SELECT itemDescription FROM Item WHERE itemId={0}", _itemid);
            DataSet _Ds = conn.GetDataSet(Sqlquery, "Description");

            if (_Ds.Tables[0].Rows.Count > 0)
            {
                retVal = Convert.ToString(_Ds.Tables[0].Rows[0]["itemDescription"]);
            }
        }

        return retVal;
    }
    
}
