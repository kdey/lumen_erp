﻿<%@ Page Title="" Language="C#" Culture="en-GB" MasterPageFile="~/EventEntry/EventEntry.master" AutoEventWireup="true" CodeFile="GrnWithoutRefToPo.aspx.cs" Inherits="EventEntry_GrnWithoutRefToPo" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
 <script type="text/javascript">
     function isNumberKey(evt)   //Checks textbox contains only numeric value
     {
         var charCode = (evt.which) ? evt.which : event.keyCode;
         if (charCode > 31 && (charCode < 48 || charCode > 57)) {
             return false;
         }
         return true;
     }
     function isNumberKey_WO_Decimal(evt)   //Checks textbox contains only numeric value
     {
         var charCode = (evt.which) ? evt.which : event.keyCode;
         if (charCode > 31 && (charCode < 48 || charCode > 57)) {
             return false;
         }
         return true;
     }
     function letternumber(e) {
         var key;
         var keychar;

         if (window.event)
             key = window.event.keyCode;
         else if (e)
             key = e.which;
         else
             return true;
         keychar = String.fromCharCode(key);
         keychar = keychar.toLowerCase();

         // control keys
         if ((key == null) || (key == 0) || (key == 8) ||
    (key == 9) || (key == 13) || (key == 27))
             return true;

         // alphas and numbers
         else if ((("abcdefghijklmnopqrstuvwxyz0123456789;").indexOf(keychar) > -1))
             return true;
         else
             return false;
     }
     
</script>
<input type="hidden" name="grnId" value='<%=_grnId%>' />
<input type="hidden" name="poId" value='<%=_poId%>' />
<div class="PageMargin">
    <div class="PageHeading">Goods Receipt Note Without Reference To Purchase Order<a style="float:right" href="GRNList.aspx">GRN List</a></div>
    <div class="Line"></div>
    <!--Content Table Start-->
    <div class="PageContent">
    <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
    <tr><td width="16%" align="right" valign="top">GRN No#</td>
        <td align="left" valign="top" width="20%"><asp:Label ID="grnNumber" runat="server" /></td>
        <td width="16%" align="right" valign="top">GRN Date (dd/mm/yyyy)&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
        <td align="left" valign="top" width="16%"><asp:TextBox ID="grnDate" runat="server" onkeyup="document.getElementById(this.id).value=''"
             autocomplete='off'  CssClass="TextboxSmall" /><br />    
            <cc1:calendarextender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="grnDate"></cc1:calendarextender><br />
            <asp:RequiredFieldValidator ControlToValidate="grnDate" ValidationGroup="addItem"  ErrorMessage="Field cannot be empty"
             ID="RequiredFieldValidator2" runat="server" /></td>
        <td  align="right" valign="top">Purchase Order Type&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
        <td align="left" valign="top" ><asp:DropDownList ID="poTypeDdl" CssClass="TextboxSmall"   runat="server" >
            <asp:ListItem Value="0" Text="--Select--"></asp:ListItem></asp:DropDownList><br />
            <asp:RequiredFieldValidator InitialValue="0" ControlToValidate="poTypeDdl" ValidationGroup="addItem"
             ErrorMessage="Select purchase order type." ID="RequiredFieldValidator7" runat="server" /></td>
    </tr>
    <tr><td  align="right" valign="top">Purchase Order No</td>
        <td  align="left" valign="top"><asp:TextBox ID="poNumber" CssClass="TextboxSmall" runat="server" MaxLength="50"></asp:TextBox>
            <input type="hidden" id="hdPoid" runat="server" /></td>
        <td  align="right" valign="top">Purchase Order Date &nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span><br />(dd/mm/yyyy)</td>
        <td  align="left" valign="top"><asp:TextBox ID="poDate" onkeyup="document.getElementById(this.id).value=''" CssClass="TextboxSmall"
             autocomplete='off' runat="server"></asp:TextBox>
            <cc1:calendarextender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" TargetControlID="poDate"></cc1:calendarextender><br />
               <asp:RequiredFieldValidator ControlToValidate="poDate" ValidationGroup="addItem"  ErrorMessage="Field cannot be empty"
             ID="RequiredFieldValidator1" runat="server" />                   </td>
        <td valign="top" align="right" >Rate Contract Type&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
        <td valign="top" align="left" ><asp:DropDownList ID="rateContractDdl" CssClass="TextboxSmall"  runat="server" >
            <asp:ListItem Value="0" Text="--Select--"></asp:ListItem></asp:DropDownList><br />
            <asp:RequiredFieldValidator InitialValue="0" ControlToValidate="rateContractDdl" ValidationGroup="addItem"
             ErrorMessage="Select rate contract type" ID="RequiredFieldValidator8"  runat="server" /></td>
    </tr>
    <tr><td align="right" valign="top">Vendor Invoice No#&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
        <td  align="left" valign="top"><asp:TextBox ID="vendorInvoiceNo" CssClass="TextboxSmall" runat="server" MaxLength="50"></asp:TextBox><br />
            <asp:RequiredFieldValidator ControlToValidate="vendorInvoiceNo" ValidationGroup="addItem"   ErrorMessage="Field cannot be empty"
             ID="RequiredFieldValidator4" runat="server" /></td>
        <td align="right" valign="top">Vendor Invoice Date&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span><br />(dd/mm/yyyy)</td>
        <td  align="left" valign="middle"><asp:TextBox ID="invoiceDate" onkeyup="document.getElementById(this.id).value=''" autocomplete='off'
             runat="server"  CssClass="TextboxSmall"  />
            <cc1:calendarextender ID="CalendarExtender3" runat="server" Format="dd/MM/yyyy" TargetControlID="invoiceDate"></cc1:calendarextender>
            <br /><asp:RequiredFieldValidator ControlToValidate="invoiceDate" ValidationGroup="addItem"   ErrorMessage="Field cannot be empty"
             ID="RequiredFieldValidator6" runat="server" /><br />
                                </td>
        <td  align="right" valign="top">Vendor Name&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
        <td align="left" valign="top"><asp:DropDownList CssClass="DropdownLarge"  
                ID="ddlVendor" runat="server" AutoPostBack="True" 
                onselectedindexchanged="ddlVendor_SelectedIndexChanged" >
            <asp:ListItem Text="Please Select" Value="0"></asp:ListItem></asp:DropDownList><br />
            <asp:RequiredFieldValidator InitialValue="0" ValidationGroup="addItem"  ControlToValidate="ddlVendor" ErrorMessage="Select Vendor"
             ID="RequiredFieldValidator3"  runat="server" /></td>
    </tr>
    <tr><td  align="right" valign="top" >Select Store&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
        <td align="left" ><asp:DropDownList ID="ddlStore" CssClass="TextboxSmall" runat="server"></asp:DropDownList><br />
            <asp:RequiredFieldValidator InitialValue="0" ControlToValidate="ddlStore" ValidationGroup="addItem"  ErrorMessage="Select Store"
             ID="RequiredFieldValidator5"  runat="server" /></td>
            <td align="right" valign="middle">Vendor Address</td>
            <td > 
                        <asp:TextBox ID="txtvendoradd" runat="server" ReadOnly="True" 
                        TextMode="MultiLine"></asp:TextBox>
            </td>
        <td><asp:Button ID="btnUpdate" runat="server" CssClass="Button" Text="Add" onclick="btnUpdate_Click" CausesValidation="true"
             Visible="false"   /></td>
        <td   align="left" ><asp:Label ID="message" runat="server" /></td>
    </tr></table>
    </div>
    <!--Content Table End-->
    <div class="Line"></div>
    <div class="PageHeading">New Purchase Order Line</div>
    <div class="Line"></div>
    <!-- the po line input table -->
    <div class="PageContent">
        <div id="tblItem" runat="server">
        <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" class="NormalText">
        <tr><td width="19%" align="center" valign="middle">Division&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="10%" align="center" valign="middle">SubDivision&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="10%" align="center" valign="middle">Item&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="20%" align="center" valign="middle">Item Description</td>
            <td width="14%" align="center" valign="middle">Quantity&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="9%" align="center" valign="middle">Price&nbsp;&nbsp;<span style="color:Red; font-weight:bold">*</span></td>
            <td width="9%" align="center" valign="middle">VAT</td>
            <td align="center">Serial No</td>
            <td></td>
        </tr>
        <tr><td valign="top"><asp:DropDownList ID="divisionDdl" CssClass="TextboxSmall" runat="server"/></td>
            <td valign="top"><asp:DropDownList ID="subDivisionDdl" CssClass="TextboxSmall" runat="server">
                    </asp:DropDownList></td>
            <td valign="top"><asp:DropDownList ID="itemDdl" CssClass="DropdownLarge" runat="server" AutoPostBack="true"
                 OnSelectedIndexChanged="itemDdl_SelectedIndexChanged" ></asp:DropDownList></td>
            <td valign="top">
            
                     <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" 
                     RenderMode="Inline">
                     <ContentTemplate>
                        <asp:TextBox ID="itemDescription" runat="server" ReadOnly="True" 
                        TextMode="MultiLine"></asp:TextBox>
                </ContentTemplate>        
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="itemDdl" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>  
            
</td>
            <td valign="top"><asp:TextBox ID="quantity" CssClass="TextboxSmall" MaxLength="6"  runat="server" Width="50"
                  />&nbsp;<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="quantity" ValidChars="1234567890." FilterMode="ValidChars" FilterType="Custom"></cc1:FilteredTextBoxExtender>
                 <asp:Label ID="uom" runat="server" /></td>
            <td valign="top"><asp:TextBox ID="price" CssClass="TextboxSmall" MaxLength="10" runat="server" Width="60" />
                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="price" ValidChars="1234567890." FilterMode="ValidChars" FilterType="Custom"></cc1:FilteredTextBoxExtender>
                 </td>
            <td valign="top"><asp:Label ID="vat" runat="server" /></td>
            <td valign="top"><asp:TextBox ID="txtserialNo" TextMode="MultiLine" onkeypress="return letternumber(event);" Width="100"
                 runat="server" CssClass="TextAreaSmall" /><br />Serial Number will be  separated by (;)</td>
            <td valign="top"><asp:Button ID="addItemLine" runat="server" Text="Add" OnClick="addItemLine_Click" ValidationGroup="addItem"
                 CssClass="ButtonBig" CausesValidation="true" /></td>
                 
            <cc1:CascadingDropDown ID="CascadingDropDown3"
            runat="server" 
            TargetControlID="divisionDdl" LoadingText="[Loading...]"
            Category="Division"
            PromptText="Select a Division"
            ServicePath ="~/divisionService.asmx"
            ServiceMethod="GetDivision" >   </cc1:CascadingDropDown>
              
           <cc1:CascadingDropDown ID="CascadingDropDown1"
                runat="server" ParentControlID = "divisionDdl" 
                TargetControlID="subDivisionDdl" LoadingText="[Loading...]"
                Category="subDivision"
                PromptText="Select a subDivision"
                ServicePath ="~/divisionService.asmx"
                ServiceMethod="GetSubdividsionForDivision" >   </cc1:CascadingDropDown>

          <cc1:CascadingDropDown ID="CascadingDropDown2"
                runat="server" ParentControlID = "subDivisionDdl" 
                TargetControlID="itemDdl" LoadingText="[Loading...]"
                Category="Item"
                PromptText="Select Item"
                ServicePath ="~/divisionService.asmx"
                ServiceMethod="GetItemForSubdividsion" >   </cc1:CascadingDropDown>     
        </tr>
        <tr><td valign="top"><asp:RequiredFieldValidator InitialValue="0" ValidationGroup="addItem" EnableClientScript="true"
             ControlToValidate="divisionDdl" ErrorMessage="Select Division" ID="divisionRequired"  Display="Dynamic" runat="server" /></td>
            <td valign="top"><asp:RequiredFieldValidator InitialValue="0" ValidationGroup="addItem" EnableClientScript="true" runat="server"
             ControlToValidate="subDivisionDdl" ErrorMessage="Select SubDivision" ID="subDivisionRequired"  Display="Dynamic" /></td>
            <td valign="top"><asp:RequiredFieldValidator InitialValue="0" ValidationGroup="addItem" EnableClientScript="true"
             ControlToValidate="itemDdl" ErrorMessage="Select Item" ID="itemRequired"  Display="Dynamic" runat="server" /></td>
            <td valign="top"></td>
            <td valign="top"><asp:RequiredFieldValidator ID="quantityRequired" runat="server" EnableClientScript="true" Display="Dynamic"
             ValidationGroup="addItem" ErrorMessage="Quantity cannot be empty." Text="" ControlToValidate="quantity" /><br />
             <%--<asp:RegularExpressionValidator ID="quanityRegexValidator" runat="server" EnableClientScript="true" Display="None"
              ValidationGroup="addItem" ErrorMessage="Quantity must be a integer" Text="" ControlToValidate="quantity" 
              ValidationExpression="\d+" />--%></td>
            <td valign="top"><asp:RequiredFieldValidator ID="priceRequired" runat="server" EnableClientScript="true" Display="Dynamic"
             ValidationGroup="addItem" ErrorMessage="Price cannot be empty." Text="" ControlToValidate="price" /><br />
             <asp:RegularExpressionValidator ID="priceRegexValidator" runat="server" EnableClientScript="true" Display="Dynamic"
              ValidationGroup="addItem" ErrorMessage="Invalid price" Text="" ControlToValidate="price" ValidationExpression="^\d*\.?\d{2}$" /></td>
            <td></td><td ></td><td></td>
        </tr><asp:Label ID="Label1" runat="server" /></table>
        </div>
    </div>
    <!-- over here we should the bottom grid where the line details of this purchase order are shown -->
    <!--Grid Table Start-->
    <div class="Line"></div>
    <div class="PageHeading">Purchase Order Items</div>
    <div class="Line"></div>
    <asp:DataGrid ID="poLineGrid" runat="server" AutoGenerateColumns="False" DataKeyField="poLineId" CellPadding="0"
     HeaderStyle-CssClass="GridHeading" ItemStyle-CssClass="GridData" onitemcommand="poLineGrid_ItemCommand" onprerender="poLineGrid_PreRender" 
     ShowFooter="True" onitemdatabound="poLineGrid_ItemDataBound" GridLines="None" Width="100%" onitemcreated="poLineGrid_ItemCreated" 
     FooterStyle-CssClass="GridData">
        <FooterStyle CssClass="GridData"></FooterStyle>
        <ItemStyle CssClass="GridData"></ItemStyle>
        <Columns>
            <asp:BoundColumn DataField="divisionName" ItemStyle-HorizontalAlign="Left" HeaderText="Division" HeaderStyle-HorizontalAlign="Left"
             HeaderStyle-Font-Bold="true" />
            <asp:BoundColumn DataField="subdivisionName" ItemStyle-HorizontalAlign="Left" HeaderText="Sub Division"
             HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Bold="true" />
            <asp:BoundColumn DataField="itemCode" HeaderText="Item" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
             HeaderStyle-Font-Bold="true" />
            <asp:TemplateColumn HeaderText="Quantity" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Left"
             ItemStyle-HorizontalAlign="Left">
                <ItemTemplate>
                <input type="hidden" id="lineId" value='<%# DataBinder.Eval(Container.DataItem, "poLineId") %>' runat="server"  />
                <input type="hidden" id="itemId" value='<%# DataBinder.Eval(Container.DataItem, "itemId") %>' runat="server"  />
                <asp:TextBox ID="itemQty" runat="server" MaxLength="50"  CssClass="TextboxSmall"
                 onkeypress="return isNumberKey_WO_Decimal(event);" Text='<%# DataBinder.Eval(Container.DataItem, "quantity") %>' Width="50" />
                <%#DataBinder.Eval(Container.DataItem, "uomName")%>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Price" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
             HeaderStyle-Font-Bold="true">
                <ItemTemplate>
                <asp:TextBox ID="itemPrice" runat="server" CssClass="TextboxSmall" MaxLength="10" onkeypress="return isNumberKey(event);"
                 Text='<%# DataBinder.Eval(Container.DataItem, "price") %>' Width="60" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="vat" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"  HeaderText="Vat %"
             HeaderStyle-Font-Bold="true"/>
            <asp:BoundColumn DataField="vatAmount" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" HeaderText="Vat Amount"
             HeaderStyle-Font-Bold="true" DataFormatString="{0:#,###.##}" />
            <asp:BoundColumn DataField="total" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"  HeaderText="Net Value"
             HeaderStyle-Font-Bold="true" DataFormatString="{0:#,###.##}" />
            <asp:TemplateColumn HeaderText="Serial No" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
             HeaderStyle-Font-Bold="true">
                <ItemTemplate>
                <asp:TextBox ID="itemSerialNo"  CssClass="TextAreaSmall" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SerialNo") %>' onkeypress="return letternumber(event);" Width="100"
                 TextMode="MultiLine" Height="50" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:ButtonColumn ButtonType="PushButton" Text="Update" CommandName="Update" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%" />
            <asp:ButtonColumn ButtonType="PushButton" Text="Delete" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%" CommandName="Delete" />
            <asp:TemplateColumn Visible="false">
                <ItemTemplate>
                <input type="hidden" id="isDeleted" value='<%#DataBinder.Eval(Container.DataItem, "isDeleted") %>' runat="server" />
                </ItemTemplate>
            </asp:TemplateColumn>             
        </Columns>
        <HeaderStyle CssClass="GridHeading"></HeaderStyle>
    </asp:DataGrid>
    <asp:Label ID="gridMessage" runat="server" />
    <table cellspacing="3" class="GridData" cellpadding="0"  border="0"  style="width:100%;">

    <tr><td colspan="11" align="center">
    <asp:Button ID="btnPrint" runat="server" CssClass="Button" onclick="btnPrint_Click" Text="Print" />
    </td></tr>
    </table>
        
    
</div>
<div class="Line"></div>
</asp:Content>

