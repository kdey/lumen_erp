﻿<%@ Page Language="C#" Culture="en-GB" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Reports/Reports.master" CodeFile="PurchaseOrderRegister.aspx.cs" Inherits="Reports_PurchaseOrderRegister" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
        <style type="text/css">
        .style1
        {
            width: 100%;
        }
        </style>
    
 </asp:Content>
 <asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
      <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    
    <!--Page Body Start-->
<div class="PageMargin">
<div class="PageHeading">Purchase Order Register</div>
<div class="Line"></div>
<!--Content Table Start-->
<div class="PageContent">
   <table width="100%" border="0" cellspacing="5" cellpadding="0">
    <tr>
      <td width="7%" align="left" valign="middle"><strong>Select :</strong></td>
      <td width="5%" align="right" valign="middle">Vendor</td>
      <td width="18%" align="left" valign="middle">
      <asp:DropDownList ID="ddlVendor" runat="server" >
      <asp:ListItem Selected="True" Value="0">All</asp:ListItem>
      </asp:DropDownList></td>
      <td width="9%" align="left" valign="middle"><strong>Select Date :</strong></td>
      <td width="5%" align="right" valign="middle">From</td>
      <td width="18%" align="left" valign="middle">
          <asp:TextBox ID="txtFromDate" runat="server" CssClass="TextboxSmall" />
          <cc1:CalendarExtender id="CalendarExtender1" runat="server" format="dd/MM/yyyy" 
              targetcontrolid="txtFromDate">
          </cc1:CalendarExtender>
        </td>
      <td width="5%" align="right" valign="middle">To</td>
      <td width="18%" align="left" valign="middle"><asp:TextBox ID="txtToDate"  runat="server"  CssClass="TextboxSmall"></asp:TextBox>
      <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" TargetControlID="txtToDate">
      </cc1:CalendarExtender></td>
      <td width="15%" align="center" valign="middle"><asp:Button ID="btnReport" 
              runat="server" Text="Report" CssClass="Button" onclick="btnReport_Click"/>&nbsp;&nbsp;<input name="Submit" type="submit" class="Button" value="Cancel" /></td>
    </tr>
  </table>
</div>
<!--Content Table End-->
<div class="Line"></div>
<div class="PageContent">
<table width="100%" align="center" cellpadding="0" cellspacing="5">
  <tr>
  	<td width="15%" align="left" valign="top">Vendor Name:</td>
	<td width="85%" align="left" valign="top"><asp:Label ID="lblVendor" runat="server"></asp:Label></td>
  </tr>
</table> 
</div>

<!--Page Body End-->
</div>
    
    
    <!--Grid Table Start-->
<asp:DataGrid ID="dgPORegister" runat="server" AutoGenerateColumns="false" 
        CellPadding="0" CellSpacing="0" 
HeaderStyle-CssClass="ReportGridHeading" BorderColor="#6699CC" BorderStyle="Solid" BorderWidth="1px"
   ItemStyle-CssClass="ReportGridData" Width="100%" ItemStyle-Width="10%"         
        >
            <Columns>
                <asp:BoundColumn DataField="activityDateTime" HeaderText="Date" HeaderStyle-Font-Bold="true"  />
                <asp:BoundColumn DataField="strPoId" HeaderText="P.O. Id" HeaderStyle-Font-Bold="true"  />
                <asp:BoundColumn DataField="itemCode" HeaderText="Item Part No." HeaderStyle-Font-Bold="true" />
                
                <asp:BoundColumn DataField="quantity" HeaderText="Quantity" HeaderStyle-Font-Bold="true"  />
                <asp:BoundColumn DataField="ValBefore" HeaderText="Value before VAT (Rs.)" HeaderStyle-Font-Bold="true"  />
                <asp:BoundColumn DataField="Vat" HeaderText="VAT (Rs.)" HeaderStyle-Font-Bold="true"  />
                <asp:BoundColumn DataField="VaLAfter" HeaderText="Value after VAT (Rs.)" HeaderStyle-Font-Bold="true"  />
                <asp:BoundColumn DataField="strGrnId" HeaderText="GRN No." HeaderStyle-Font-Bold="true"  />
                                
            </Columns>            
</asp:DataGrid>
    <div class="Line"></div>
    <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
    <br />
 </asp:Content>


