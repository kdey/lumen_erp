﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EventEntry/EventEntry.master" AutoEventWireup="true" CodeFile="POrderList.aspx.cs" Inherits="EventEntry_POrderList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="PageMargin">
        <div class="PageHeading">
            Puechase Order List</div>
        <div class="Line">
        </div>
        <!--Content Table Start-->
        <div class="PageContent" align="center">
         <asp:Label ID="message" runat="server" />
         <br />
          <br />
               <asp:DataGrid ID="poGrid" runat="server" AutoGenerateColumns="false" DataKeyField="poId"
            CellPadding="3" CellSpacing="3" OnItemCommand="poGrid_ItemCommand" HeaderStyle-CssClass="GridHeading"
            ItemStyle-CssClass="GridData" >
            <Columns>
                <asp:BoundColumn DataField="strPoId" HeaderText="PO#" HeaderStyle-Font-Bold="true" />
                <asp:BoundColumn DataField="vendorName" HeaderText="Vendor Name" HeaderStyle-Font-Bold="true" />
                <asp:BoundColumn DataField="orderCode" HeaderText="Order Type" HeaderStyle-Font-Bold="true" />
                 <asp:BoundColumn DataField="poDate" HeaderText="PO Date" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-Font-Bold="true" />
                <asp:ButtonColumn ButtonType="LinkButton" CommandName="Edit" Text="Edit" ItemStyle-CssClass="GridLink" />
                <asp:ButtonColumn ButtonType="LinkButton" CommandName="Delete" Text="Delete" ItemStyle-CssClass="GridLink" />
            </Columns>
        </asp:DataGrid>
        </div>
        <!--Content Table End-->
        <div class="Line">
        </div>
        <!--Grid Table Start-->
     
        <!--Grid Table End-->
        <div class="Line">
        </div>
        <!--Page Body End-->
    </div>
</asp:Content>

