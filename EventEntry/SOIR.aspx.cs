﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;


public partial class EventEntry_SOIR : BasePage
{
    protected int _soirId = -1;
    private double totalAmount = 0;
    protected int _isLocked = -1;
    protected int _isDeleted = -1;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!string.IsNullOrEmpty(Request["soirId"]))
        {
            _soirId = int.Parse(Request["soirId"]);
        }
        if (!string.IsNullOrEmpty(Request["isLocked"]))
        {
            _isLocked = int.Parse(Request["isLocked"]);
        }
        if (!this.IsPostBack)
        {
            bindControl();
            raisedBy.Text = getUserName();
            if (_soirId > 0)
            {

                LoadSoir();
            }
        }
        if (GetLoggedinUser().UserType == 2)
        {
            //add.Enabled = false;
            //addItemLine.Enabled = false;
           
        }
    }

    public void bindControl()
    {
        BindListControl _control = new BindListControl();
        _control.BindSalesPerson(ddlprimary);
        _control.BindSalesPerson(ddlAditional);
        _control.BindRateContractType(rateContractDdl);
        _control.BindCustomerAll(ddlCustomer);
        _control.BindOrderType(ddlOrder);
        _control.BindWarranty(ddlWarrantity);
        _control.BindServiceContract(ddlServiceAgreement);
        _control.BindDivision(divisionDdl);
    }
    protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCustomer.SelectedIndex != 0)
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                DataSet _ds = conn.GetDataSet("select Billing_address1,Billing_address2, Shipping_address1,Shipping_address2 from Lum_Erp_customer where customerId = '" + ddlCustomer.SelectedValue + "'", "GetAddress");


                if (_ds.Tables[0].Rows.Count != 0)
                {
                    BillingAddress.Text = _ds.Tables[0].Rows[0]["Billing_address1"].ToString() + "" + _ds.Tables[0].Rows[0]["Billing_address2"].ToString();
                    installationAddress.Text = _ds.Tables[0].Rows[0]["Shipping_address1"].ToString() + "" + _ds.Tables[0].Rows[0]["Shipping_address2"].ToString();

                }


            }
        }

    }
    //protected void divisionDdl_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    BindListControl _control = new BindListControl();
    //    _control.BindSubDivision(subDivisionDdl, divisionDdl.SelectedValue);
    //    itemDdl.Items.Clear();
    //    itemDdl.Items.Add("Please Select");
    //    itemDdl.Items[0].Value = "0";

    //}
    /// <summary>
    /// Create the GRN 
    /// </summary>
    /// <param name="poId"></param>
    /// <param name="conn"></param>

    protected void itemDdl_SelectedIndexChanged(object sender, EventArgs e)
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            LoadItemDescription(conn);
        }
        //lblAvailable.Text = getAvailableItem();
        hmaxprice.Value = GetItemPrice();
    }
    //protected void subDivisionDdl_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    BindListControl _control = new BindListControl();
    //    _control.BindItems(itemDdl, subDivisionDdl.SelectedValue);
    //}
    private void LoadItemDescription(IConnection conn)
    {

        if (!string.IsNullOrEmpty(itemDdl.SelectedValue))
        {
            using (IDataReader reader = conn.ExecuteQuery(
                        @"select itemDescription, uomName, vat
                          from Lum_Erp_Item i
                          inner join Lum_Erp_Uom u on u.uomId = i.uomId 
                          where i.itemId = " + itemDdl.SelectedValue))
            {
                if (reader.Read())
                {
                    itemDescription.Text = reader.GetString(0);
                    uom.Text = reader.GetString(1);
                }
            }
        }
    }
    protected void chkItemType_CheckedChanged(object sender, EventArgs e)
    {
        if (chkItemType.Checked)
        {
            price.Text = "0.00";
            discount.Text = "0.00";
            price.Enabled = false;
            discount.Enabled = false;
        }
        else
        {
            price.Text = "";
            discount.Text = "";
            price.Enabled = true;
            discount.Enabled = true;
        }
    }
    protected void addItemLine_Click(object sender, EventArgs e)
    {

        decimal _discount = 0;
        if (!string.IsNullOrEmpty(discount.Text))
        {
            _discount = Convert.ToDecimal(discount.Text);
        }


        DateTime date = DateTime.Today;
        if (string.IsNullOrEmpty(poDate.Text))
        {
            poDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
        }
        else
        {
            date = DateTime.ParseExact(poDate.Text, "dd/MM/yyyy", null);
        }

        object obj = Convert.DBNull;
        if (ddlAditional.SelectedIndex != 0)
        {
            obj = ddlAditional.SelectedValue;
        }
        object obj1 = Convert.DBNull;
        if (ddlprimary.SelectedIndex != 0)
        {
            obj = ddlprimary.SelectedValue;
        }
        if (!chkItemType.Checked)
        {
            if (Convert.ToDecimal(quantity.Text) == 0)
            {
                return;
            }
            else if (Convert.ToDecimal(price.Text) == 0)
            {
                return;
            }
        }
        if (_soirId <= 0)
        {

                   using (IConnection conn = DataConnectionFactory.GetConnection())
                    {

                        using (IDataReader _reader = conn.ExecuteQueryProc("[Lum_Erp_Stp_AddNewSoir]",
                                            new SpParam("@strSoirId", string.Empty, SqlDbType.VarChar),
                                            new SpParam("@raisedBy", raisedBy.Text, SqlDbType.VarChar),
                                            new SpParam("@primarySalesPersonId", obj, SqlDbType.Int),
                                            new SpParam("@aditionalSalesPersonId", obj, SqlDbType.Int),
                                            new SpParam("@customerId", ddlCustomer.SelectedValue, SqlDbType.Int),
                                            new SpParam("@billingAddressId", 0, SqlDbType.Int),
                                            new SpParam("@shippingAddressId", 0, SqlDbType.Int),
                                            new SpParam("@customerPoNo", poReference.Text, SqlDbType.VarChar),
                                            new SpParam("@poDate", date, SqlDbType.DateTime),
                                            new SpParam("@orderTypeId", ddlOrder.SelectedValue, SqlDbType.Int),
                                            new SpParam("@rateContractTypeId", rateContractDdl.SelectedValue, SqlDbType.Int),
                                            new SpParam("@installationAddress", installationAddress.Text, SqlDbType.VarChar),
                                            new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int)))
                        {
                            if (_reader.Read())
                            {
                                _soirId = Convert.ToInt32(_reader["SOIRID"]);
                                soirNumber.Text = Convert.ToString(_reader["STRSOIRID"]);
                                _reader.Close();

                                message1.Text = "SOIR is Added.";
                                message1.ForeColor = Color.Green;
                            }
                        }
                   }
        }







        if (_soirId > 0)
        {
            using (IConnection conn = DataConnectionFactory.GetConnection())
            {
                string fromDate = Convert.ToDateTime(poDate.Text).ToString("dd MMM yyyy");
                conn.ExecuteCommandProc("Lum_Erp_Stp_CreateSoirItemDetails", new SpParam("@soirId", _soirId, SqlDbType.Int),
                                            new SpParam("@itemId", itemDdl.SelectedValue, SqlDbType.Int),
                                            new SpParam("@CDate", fromDate, SqlDbType.SmallDateTime),
                                            new SpParam("@quantity", quantity.Text, SqlDbType.Decimal),
                                            new SpParam("@price", price.Text, SqlDbType.Decimal),
                                            new SpParam("@discount", _discount, SqlDbType.Decimal),
                                            new SpParam("@itemType", chkItemType.Checked, SqlDbType.Bit),
                                            new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int));
                BindGrid(conn);
                BindFocGrid(conn);



                lblquantity.Text = GetTotalQuantity();
                double Charge = 0.0;
                if (!string.IsNullOrEmpty(txtCharge.Text))
                {
                    Charge = Convert.ToDouble(txtCharge.Text);
                }

                double Buyback = 0.0;
                if (!string.IsNullOrEmpty(txtBuyBackamount.Text))
                {
                    Buyback = Convert.ToDouble(txtBuyBackamount.Text);
                }

                totalAmount = totalAmount + Charge - Buyback;
                lblTotal.Text = string.Format("{0:#,###.##}", totalAmount);
                lblPrice.Text = changeCurrencyToWords(totalAmount);
            }
        }
        resetControl();
    }
    private void BindGrid(IConnection conn)
    {
        string fromDate = Convert.ToDateTime(poDate.Text).ToString("dd MMM yyyy");

        try
        {
            using (IDataReader reader =
                conn.ExecuteQueryProc("Lum_Erp_Stp_GetSoirItemsDetails",
                                         new SpParam("@soirid", _soirId, SqlDbType.Int),
                                         new SpParam("@CDate", fromDate, SqlDbType.SmallDateTime)
                                         )) 
            {
                poLineGrid.DataSource = reader;
                poLineGrid.DataBind();
            }
        }
        catch (Exception ex)
        {
            message.Text = ex.ToString();


        }
    }
    private void BindFocGrid(IConnection conn)
    {

        try
        {
            using (IDataReader reader = conn.ExecuteQueryProc("Lum_Erp_Stp_GetSoirItemsDetails_FOC",
                             new SpParam("@soirid", _soirId, SqlDbType.Int)))
            {

                focGrid.DataSource = reader;
                focGrid.DataBind();
            }
        }
        catch (Exception ex)
        {
            message.Text = ex.ToString();


        }

    }
    protected void poLineGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (_isLocked < 0)
        {
            if (e.CommandName.Equals("Delete"))
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    int soirLineId = (int)poLineGrid.DataKeys[e.Item.ItemIndex];
                    conn.ExecuteCommandProc("Lum_Erp_Stp_SoirDeleteItem",
                        new SpParam("@soirId", _soirId, SqlDbType.Int),
                        new SpParam("@soirLineId", soirLineId, SqlDbType.Int),
                        new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int));
                    //gridMessage.Text = "Line deleted";
                    gridMessage.Text = "";
                    gridMessage.ForeColor = Color.Green;
                    BindGrid(conn);
                }
            }
            else if (e.CommandName.Equals("Edit"))
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    try
                    {
                        int soirLineId = (int)poLineGrid.DataKeys[e.Item.ItemIndex];
                        decimal quantity = decimal.Parse(((TextBox)e.Item.FindControl("itemQty")).Text);
                        decimal price = decimal.Parse(((TextBox)e.Item.FindControl("itemPrice")).Text);
                        decimal discount = decimal.Parse(((TextBox)e.Item.FindControl("itemDiscount")).Text);
                        conn.ExecuteCommandProc("Lum_Erp_Stp_UpdateSoirItemDetails",
                            new SpParam("@soirId", _soirId, SqlDbType.Int),
                            new SpParam("@soirLineId", soirLineId, SqlDbType.Int),
                            new SpParam("@quantity", quantity, SqlDbType.Decimal),
                            new SpParam("@price", price, SqlDbType.Decimal),
                            new SpParam("@discount", discount, SqlDbType.Decimal),
                            new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int));
                        // gridMessage.Text = "Line updated";
                        gridMessage.Text = "";
                        gridMessage.ForeColor = Color.Green;
                    }
                    catch (Exception ex)
                    {
                        gridMessage.Text = ex.ToString();
                        gridMessage.ForeColor = Color.Red;
                    }
                    BindGrid(conn);
                }
            }
            lblquantity.Text = GetTotalQuantity();
            double Charge = 0.0;
            if (!string.IsNullOrEmpty(txtCharge.Text))
            {
                Charge = Convert.ToDouble(txtCharge.Text);
            }

            double Buyback = 0.0;
            if (!string.IsNullOrEmpty(txtBuyBackamount.Text))
            {
                Buyback = Convert.ToDouble(txtBuyBackamount.Text);
            }

            totalAmount = totalAmount + Charge - Buyback;
            lblTotal.Text = string.Format("{0:#,###.##}", totalAmount);
            lblPrice.Text = changeCurrencyToWords(totalAmount);
        }
        else
        {
            message1.Text = "Soir is locked,you cant modify it";
            message1.ForeColor = Color.Red;
        }
    }
    protected void focGrid_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (_isLocked < 0)
        {
            if (e.CommandName.Equals("Delete"))
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    int soirLineId = (int)focGrid.DataKeys[e.Item.ItemIndex];
                    conn.ExecuteCommandProc("Lum_Erp_Stp_SoirDeleteItem",
                        new SpParam("@soirId", _soirId, SqlDbType.Int),
                        new SpParam("@soirLineId", soirLineId, SqlDbType.Int),
                        new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int));
                    //focMessage.Text = "Line deleted";
                    focMessage.Text = "";
                    focMessage.ForeColor = Color.Green;
                    BindFocGrid(conn);
                }
            }
            else if (e.CommandName.Equals("Edit"))
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    try
                    {
                        int soirLineId = (int)focGrid.DataKeys[e.Item.ItemIndex];
                        decimal quantity = decimal.Parse(((TextBox)e.Item.FindControl("itemQty")).Text);
                        decimal price = 0;
                        decimal discount = 0;
                        conn.ExecuteCommandProc("Lum_Erp_Stp_UpdateSoirItemDetails",
                            new SpParam("@soirId", _soirId, SqlDbType.Int),
                            new SpParam("@soirLineId", soirLineId, SqlDbType.Int),
                            new SpParam("@quantity", quantity, SqlDbType.Decimal),
                            new SpParam("@price", price, SqlDbType.Decimal),
                            new SpParam("@discount", discount, SqlDbType.Decimal),
                            new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int));

                        focMessage.Text = "";
                        focMessage.ForeColor = Color.Green;
                    }
                    catch (FormatException)
                    {
                        focMessage.Text = "Invalid quantity";
                        focMessage.ForeColor = Color.Red;
                    }
                BindGrid(conn);
                BindFocGrid(conn);



                lblquantity.Text = GetTotalQuantity();
                double Charge = 0.0;
                if (!string.IsNullOrEmpty(txtCharge.Text))
                {
                    Charge = Convert.ToDouble(txtCharge.Text);
                }

                double Buyback = 0.0;
                if (!string.IsNullOrEmpty(txtBuyBackamount.Text))
                {
                    Buyback = Convert.ToDouble(txtBuyBackamount.Text);
                }

                totalAmount = totalAmount + Charge - Buyback;
                lblTotal.Text = string.Format("{0:#,###.##}", totalAmount);
                lblPrice.Text = changeCurrencyToWords(totalAmount);
                }
            }
        }
        else
        {
            message1.Text = "Soir is locked,you cant modify it";
            message1.ForeColor = Color.Red;
        }
    }
    protected void add_Click(object sender, EventArgs e)
    {
        // we have already created soir here we updating with some aditional value.
        // get the date 
        DateTime pdate = DateTime.Today;
        DateTime idate = DateTime.Today;
        DateTime pDdate = DateTime.Today;
        if (string.IsNullOrEmpty(poDate.Text))
        {
            poDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
        }
        else
        {
            pdate = DateTime.ParseExact(poDate.Text, "dd/MM/yyyy", null);
        }
        if (string.IsNullOrEmpty(txtInstRdate.Text))
        {
            txtInstRdate.Text = DateTime.Today.ToString("dd/MM/yyyy");
        }
        else
        {
            idate = DateTime.ParseExact(txtInstRdate.Text, "dd/MM/yyyy", null);
        }
        if (string.IsNullOrEmpty(txtDueDate.Text))
        {
            txtDueDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
        }
        else
        {
            pDdate = DateTime.ParseExact(txtDueDate.Text, "dd/MM/yyyy", null);
        }
        decimal _discount = 0;
        if (!string.IsNullOrEmpty(discount.Text))
        {
            _discount = Convert.ToDecimal(discount.Text);
        }
        decimal packingCharges = 0;
        if (!string.IsNullOrEmpty(txtCharge.Text))
        {
            packingCharges = Convert.ToDecimal(txtCharge.Text);
        }
        decimal buyBackAmount = 0;
        if (!string.IsNullOrEmpty(txtBuyBackamount.Text))
        {
            buyBackAmount = Convert.ToDecimal(txtBuyBackamount.Text);
        }
        decimal ServiceRate = 0;
        if (!string.IsNullOrEmpty(txtServiceRate.Text))
        {
            ServiceRate = Convert.ToDecimal(txtServiceRate.Text);
        }
        object objAditional = Convert.DBNull;
        if (ddlAditional.SelectedIndex != 0)
        {
            objAditional = ddlAditional.SelectedValue;
        }
        object objWarrantity = Convert.DBNull;
        if (ddlWarrantity.SelectedIndex != 0)
        {
            objWarrantity = ddlWarrantity.SelectedValue;
        }
        object objSeviceAgreement = Convert.DBNull;
        if (ddlServiceAgreement.SelectedIndex != 0)
        {
            objSeviceAgreement = ddlServiceAgreement.SelectedValue;
        }
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            try
            {
                if (_soirId > 0)
                {
                    int i = conn.ExecuteCommandProc("Lum_Erp_Stp_UpdateSoirDetails",
                                                new SpParam("@soirId", _soirId, SqlDbType.Int),
                                                new SpParam("@raisedBy", raisedBy.Text, SqlDbType.VarChar),
                                                new SpParam("@primarySalesPersonId", ddlprimary.SelectedValue, SqlDbType.Int),
                                                new SpParam("@aditionalSalesPersonId", objAditional, SqlDbType.Int),
                                                new SpParam("@customerId", ddlCustomer.SelectedValue, SqlDbType.Int),
                                                new SpParam("@billingAddressId", 0, SqlDbType.Int),
                                                new SpParam("@shippingAddressId", 0, SqlDbType.Int),
                                                new SpParam("@customerPoNo", poReference.Text, SqlDbType.VarChar),
                                                new SpParam("@poDate", pdate, SqlDbType.DateTime),
                                                new SpParam("@orderTypeId", ddlOrder.SelectedValue, SqlDbType.Int),
                                                new SpParam("@rateContractTypeId", rateContractDdl.SelectedValue, SqlDbType.Int),
                                                new SpParam("@installationAddress", installationAddress.Text, SqlDbType.VarChar),
                                                new SpParam("@packingCharges", packingCharges, SqlDbType.Decimal),
                                                new SpParam("@buyBackAmount", buyBackAmount, SqlDbType.Decimal),
                                                new SpParam("@buyBackDetails", txtDetails.Text, SqlDbType.VarChar),
                                                new SpParam("@installationDate", idate, SqlDbType.DateTime),
                                                new SpParam("@specialInAdvice", txtInstAdvice.Text, SqlDbType.VarChar),
                                                new SpParam("@serviceContractId", objSeviceAgreement, SqlDbType.Int),
                                                new SpParam("@warrantyId", objWarrantity, SqlDbType.Int),
                                                new SpParam("@serviceRate", ServiceRate, SqlDbType.Decimal),
                                                new SpParam("@paymentTerms", txtpaymentTerms.Text, SqlDbType.VarChar),
                                                new SpParam("@paymentDueDate", pDdate, SqlDbType.DateTime),
                                                new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int));


                    if (i > 0)
                    {
                        Response.Redirect("SOIRList.aspx");
                        message1.Text = "SOIR Updated.";
                        message.Text = "SOIR Updated.";
                        message.ForeColor = Color.Green;
                        message1.ForeColor = Color.Green;
                    }

                }

                else
                {
                    message1.Text = "Add Customer Order Details first";
                    message.Text = "Add Customer Order Details first";
                    message.ForeColor = Color.Red;
                    message1.ForeColor = Color.Red;
                }
            }
            catch (Exception ex)
            {
                message.Text = ex.ToString();
            }
        }
        LoadSoir();

    }

    public void LoadSoir()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(@"select* from Lum_Erp_Soir WHERE  soirId=" + _soirId))
            {
                if (reader.Read())
                {


                    raisedBy.Text = Convert.ToString(reader["raisedBy"]);
                    soirNumber.Text = Convert.ToString(reader["strSoirId"]) + "/" + Convert.ToString(reader["soirVersion"]);
                    ddlprimary.SelectedValue = Convert.ToString(reader["primarySalesPersonId"]);
                    if (Convert.ToString(reader["aditionalSalesPersonId"]) != "")
                        ddlAditional.SelectedValue = Convert.ToString(reader["aditionalSalesPersonId"]);
                    ddlCustomer.SelectedValue = Convert.ToString(reader["customerId"]);
                    rateContractDdl.SelectedValue = Convert.ToString(reader["rateContractTypeId"]);
                    installationAddress.Text = Convert.ToString(reader["installationAddress"]);
                    poReference.Text = Convert.ToString(reader["customerPoNo"]);
                    poDate.Text = Convert.ToString(reader["poDate"]).Split(' ')[0];
                    ddlOrder.SelectedValue = Convert.ToString(reader["orderTypeId"]);
                    txtCharge.Text = Convert.ToString(reader["packingCharges"]);
                    txtBuyBackamount.Text = Convert.ToString(reader["buyBackAmount"]);
                    txtDetails.Text = Convert.ToString(reader["buyBackDetails"]);
                    txtInstRdate.Text = Convert.ToString(reader["installationDate"]).Split(' ')[0];
                    txtInstAdvice.Text = Convert.ToString(reader["specialInAdvice"]);
                    _isDeleted =  Convert.ToInt16(reader["isDeleted"]);
                    if (_isDeleted == 1)
                        addItemLine.Visible = false;

                    if (reader["serviceContractId"] != Convert.DBNull)
                    {
                        ddlServiceAgreement.SelectedValue = Convert.ToString(reader["serviceContractId"]);
                    }
                    if (reader["warrantyId"] != Convert.DBNull)
                    {
                        ddlWarrantity.SelectedValue = Convert.ToString(reader["warrantyId"]);
                    }
                    txtServiceRate.Text = Convert.ToString(reader["serviceRate"]);
                    txtpaymentTerms.Text = Convert.ToString(reader["paymentTerms"]);
                    txtDueDate.Text = Convert.ToString(reader["paymentDueDate"]).Split(' ')[0];
                    reader.Close();

                    BindGrid(conn);
                    BindFocGrid(conn);

                    if (CheckSoirIsLocked())
                    {
                        _isLocked = 1;
                        add.Enabled = false;
                        addItemLine.Enabled = false;
                        message1.Text = "SOIR is locked,you cant modify it";
                        message1.ForeColor = Color.Red;
                    }
                }
            }
        }
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            DataSet _ds = conn.GetDataSet("select Billing_address1,Billing_address2, Shipping_address1,Shipping_address2 from Lum_Erp_customer where customerId = '" + ddlCustomer.SelectedValue + "'", "GetAddress");


            if (_ds.Tables[0].Rows.Count != 0)
            {
                BillingAddress.Text = _ds.Tables[0].Rows[0]["Billing_address1"].ToString() + "" + _ds.Tables[0].Rows[0]["Billing_address2"].ToString();
                installationAddress.Text = _ds.Tables[0].Rows[0]["Shipping_address1"].ToString() + "" + _ds.Tables[0].Rows[0]["Shipping_address2"].ToString();

            }

        }
        lblquantity.Text = GetTotalQuantity();
        double Charge = 0.0;
        if (!string.IsNullOrEmpty(txtCharge.Text))
        {
            Charge = Convert.ToDouble(txtCharge.Text);
        }

        double Buyback = 0.0;
        if (!string.IsNullOrEmpty(txtBuyBackamount.Text))
        {
            Buyback = Convert.ToDouble(txtBuyBackamount.Text);
        }

        totalAmount = totalAmount + Charge - Buyback;
        lblTotal.Text = string.Format("{0:#,###.##}", totalAmount);
        lblPrice.Text = changeCurrencyToWords(totalAmount);
    }

    public string GetTotalQuantity()
    {
        string retVal = "";
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(@"select Sum(quantity) as TotalQty from Lum_Erp_SoirItemDetails WHERE  soirId=" + _soirId))
            {
                if (reader.Read())
                {


                    retVal = Convert.ToString(reader["TotalQty"]);
                }
            }
        }
        return retVal;
    }
    private void CalcTotalAmount(string _netVal)
    {
        if (_netVal != "")
        {
            totalAmount += Double.Parse(_netVal);
        }

    }
    protected void poLineGrid_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CalcTotalAmount(e.Item.Cells[7].Text);

        }
    }




    public bool CheckSoirIsLocked()
    {
        bool retVal = false;

        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(@"select* from Lum_Erp_Soir WHERE isLocked=1 and soirId=" + _soirId))
            {
                if (reader.Read())
                {
                    retVal = true;
                }
            }
        }
        return retVal;

    }
    protected void poLineGrid_PreRender(object sender, EventArgs e)
    {
        for (int _index = 0; _index < poLineGrid.Items.Count; _index++)
        {
            Button btnedit = (Button)poLineGrid.Items[_index].Cells[8].Controls[0];
            Button btn = (Button)poLineGrid.Items[_index].Cells[9].Controls[0];

            HtmlInputHidden _hdel = (HtmlInputHidden)poLineGrid.Items[_index].FindControl("isdeleted");
            HtmlInputHidden _hlocked = (HtmlInputHidden)poLineGrid.Items[_index].FindControl("isLocked");
            
            if (_hdel.Value == "True")
            {
                btnedit.Visible = false;
                btn.Visible = false;
            }
            if (_hlocked.Value == "True")
            {
                btnedit.Visible = false;
                btn.Visible = false;
            }
        }
    }

    protected void poLineGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            Button btnEdit = (Button)e.Item.Cells[8].Controls[0];
            Button btnDel = (Button)e.Item.Cells[9].Controls[0];
            btnDel.CssClass = "ButtonBig";
            btnEdit.CssClass = "ButtonBig";
            if (GetLoggedinUser().UserType == 2)
            {
                btnEdit.Visible  = false;
                btnDel.Visible = false;

            }

        }
    }
    protected void focGrid_PreRender(object sender, EventArgs e)
    { 
        for (int _index = 0; _index < focGrid.Items.Count; _index++)
        {

            Button btnedit = (Button)focGrid.Items[_index].Cells[7].Controls[0];
            Button btn = (Button)focGrid.Items[_index].Cells[8].Controls[0];

            HtmlInputHidden _hdel = (HtmlInputHidden)focGrid.Items[_index].FindControl("isdeleted");
           // HtmlInputHidden _hlocked = (HtmlInputHidden)poLineGrid.Items[_index].FindControl("isLocked");

            if (_hdel.Value == "True")
            {
                btnedit.Visible = false;
                btn.Visible = false;
            }
            //if (_hlocked.Value == "True")
            //{
            //    btnedit.Visible = false;
            //    btn.Visible = false;
            //}

        }
    }
    protected void focGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            Button btnEdit = (Button)e.Item.Cells[7].Controls[0];
            Button btnDel = (Button)e.Item.Cells[8].Controls[0];
            btnDel.CssClass = "ButtonBig";
            btnEdit.CssClass = "ButtonBig";
            if (GetLoggedinUser().UserType == 2)
            {
                btnEdit.Enabled = false;
                btnDel.Enabled = false;

            }
        }
    }
    public string getUserName()
    {
        string retVal = string.Empty;

        string sql = string.Format(@"SELECT (firstName+' '+lastName) as name FROM Lum_Erp_AdminUser WHERE adminuserId={0}  ", GetLoggedinUser().UserId);
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(sql))
            {
                if (reader.Read())
                {

                    retVal = Convert.ToString(reader["name"]);

                }
            }
        }
        return retVal;
    }




    public string GetItemPrice()
    {
        string retVal = "0";
        string sqlstr = string.Empty;

        sqlstr = string.Format("SELECT MRP,MRP+(MRP * ISNULL(vat,0))/100 AS maxPrice, vat as tax from Lum_Erp_Item where itemId={0}", itemDdl.SelectedValue);



        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(sqlstr))
            {
                if (reader.Read())
                {
                    //   decimal _quantity = Convert.ToDecimal(quantity.Text);
                    retVal = Convert.ToString(reader["maxPrice"]);
                    hvat.Value = Convert.ToString(reader["tax"]);
                    
                }
            }
        }
        return retVal;
    }

    protected void btnPrint_Click(object sender, EventArgs e)
    {
        if (_soirId > 1)
        {
            Response.Redirect("SOIRPrint.aspx?SOIRId=" + _soirId);
        }
        else
        {
            message.Text = "Create purchase order before Print.";
            message.ForeColor = Color.Red;
        }
    }

    public void resetControl()
    {
        divisionDdl.SelectedIndex = -1;
        subDivisionDdl.SelectedIndex = -1;
        itemDdl.SelectedIndex = -1;
        quantity.Text = string.Empty;
        price.Text = string.Empty;
        itemDescription.Text = string.Empty;
        discount.Text = string.Empty;
        chkItemType.Checked = false;
    }
    protected void price_TextChanged(object sender, EventArgs e)
    {

    }
}
