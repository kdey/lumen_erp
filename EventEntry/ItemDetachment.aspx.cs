﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;
using System.Data.SqlClient;

public partial class EventEntry_ItemDetachment :BasePage
{
    protected int _grndetailsSerialNoId = -1;
    public ArrayList listspare;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!string.IsNullOrEmpty(Request["grndetailsSerialNoId"]))
        {
            _grndetailsSerialNoId = int.Parse(Request["grndetailsSerialNoId"]);
        }
        if (!this.IsPostBack)
        {
            bindControl();
            if (_grndetailsSerialNoId>0)
            {
            BindDetachItem();
            }
        }
        if (GetLoggedinUser().UserType == 2)
        {

            add.Enabled = false;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlStore_SelectedIndexChanged(object sender, EventArgs e)
    {


        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            SpParam _spdivision = new SpParam("@divisionId", ddlDivision.SelectedValue, SqlDbType.Int);
            SpParam _spsubdivision = new SpParam("@subdivisionId", ddlSubdivision.SelectedValue, SqlDbType.Int);
            SpParam _spitemId = new SpParam("@itemId",ddlItem.SelectedValue, SqlDbType.Int);
            SpParam _spSTORID = new SpParam("@storeId", ddlStore.SelectedValue, SqlDbType.Int);
            SpParam _spbyqty = new SpParam("@BYQTY", 0, SqlDbType.Int);

            DataSet _Ds = conn.GetDataSet("Lum_Erp_Stp_GetStockRegister", _spdivision, _spsubdivision, _spitemId, _spSTORID, _spbyqty);

            ddlserialNo.ClearSelection();
            ddlserialNo.Items.Clear();
            ddlserialNo.AppendDataBoundItems = true;
            ddlserialNo.Items.Add("Please Select");
            ddlserialNo.Items[0].Value = "0";
            ddlserialNo.DataTextField = _Ds.Tables[0].Columns["serialNo"].ToString();
            ddlserialNo.DataValueField = _Ds.Tables[0].Columns["grndetailsSerialNoId"].ToString();
            ddlserialNo.DataSource = _Ds;
            ddlserialNo.DataBind();

        }
    }
    /// <summary>
    /// 
    /// </summary>
    public void bindControl()
    {
        BindListControl _control = new BindListControl();
        _control.BindDivision(ddlDivision);
        _control.BindStores(ddlStore);

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlDivision_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindListControl _control = new BindListControl();
        if (ddlDivision.SelectedIndex != 0)
        {
            _control.BindSubDivision(ddlSubdivision, ddlDivision.SelectedValue);
        }
        ddlItem.Items.Clear();
        ddlItem.Items.Add("--Please Select--");
        ddlItem.Items[0].Value = "0";
       
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlSubdivision_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindListControl _control = new BindListControl();
        if (ddlSubdivision.SelectedIndex != 0)
        {
            if (ddlDivision.SelectedIndex != 0)
            {
                _control.BindItems(ddlItem, ddlSubdivision.SelectedValue);
                ddlStore.SelectedIndex = 0;
                ddlserialNo.SelectedIndex = 0;
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public void BindSerialNo()
    {

        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            string  spserialNo = "";

            if (ddlserialNo.SelectedIndex > 0)
                spserialNo = ddlserialNo.Items[ddlserialNo.SelectedIndex].Text;
            else
                spserialNo = lblSerialNo.Text;

            SpParam _spitemId = new SpParam("@itemId1", ddlItem.SelectedValue, SqlDbType.Int);
            SpParam _spserialNo = new SpParam("@serialNo", spserialNo, SqlDbType.VarChar);

            DataSet _Ds = conn.GetDataSet("Lum_Erp_Stp_GetDetachItems_withId", _spitemId, _spserialNo);



            ditemGrid.DataSource = _Ds; //aTable;
            ditemGrid.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlserialNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindSerialNo();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void add_Click(object sender, EventArgs e)
    {
        int _i = 0;
        bool flagFIRST = false;
        bool flag = false;
        if (ddlserialNo.SelectedIndex > 0)
            flagFIRST = true; //first time
        else
            flagFIRST = false;

        if (flagFIRST == true)
        {
            message.Text +=" true1";
            for (int _index = 0; _index < ditemGrid.Items.Count; _index++)
            {

                CheckBox _chkBox = (CheckBox)ditemGrid.Items[_index].FindControl("chkStatus");
                decimal _value = 0;
                decimal txtgrndetailsSerialNoId = 0;
                TextBox _txtValue = (TextBox)ditemGrid.Items[_index].FindControl("txtValue");
                TextBox _txtgrndetailsSerialNoId = (TextBox)ditemGrid.Items[_index].FindControl("txtgrndetailsSerialNoId");
                TextBox _txtsparesId = (TextBox)ditemGrid.Items[_index].FindControl("txtsparesId");
                
                if (!string.IsNullOrEmpty(_txtValue.Text))
                {
                    _value = decimal.Parse(_txtValue.Text);
                }
                txtgrndetailsSerialNoId = Convert.ToDecimal (_txtgrndetailsSerialNoId.Text);

                int itemId = int.Parse(((HtmlInputHidden)ditemGrid.Items[_index].FindControl("itemId")).Value);
                int sparesId1 = Convert.ToInt32(_txtsparesId.Text);

                if (!_chkBox.Checked)
                {
                    flag = true;
                    try
                    {
                        using (IConnection conn = DataConnectionFactory.GetConnection())
                        {
                            conn.ExecuteCommandProc("Lum_Erp_Stp_AddnewItemOnStock", new SpParam("@itemId", ddlItem.SelectedValue, SqlDbType.Int),
                                        new SpParam("@sparesId", sparesId1, SqlDbType.Int),
                                        new SpParam("@storeId", int.Parse(ddlStore.SelectedValue), SqlDbType.Int),
                                        new SpParam("@serialNo", ddlserialNo.Items[ddlserialNo.SelectedIndex].Text, SqlDbType.VarChar),
                                        new SpParam("@value", _value, SqlDbType.Decimal),

                                        new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int),
                                        new SpParam("@grndetailsSerialNoId", ddlserialNo.SelectedValue, SqlDbType.Decimal),
                                       new SpParam("@grndetailsSerialNoId_SPARE", txtgrndetailsSerialNoId, SqlDbType.Decimal));
                        }
                        message.Text = "item updated";
                        message.ForeColor = Color.Green;
                        add.Enabled = false;
                    }
                    catch (SqlException EX)
                    {
                        message.Text = EX.Message;
                        message.ForeColor = Color.Red;
                        return;
                    }
                }

            }
            if (flag == true)
            {
                using (IConnection conn = DataConnectionFactory.GetConnection())
                {
                    conn.ExecuteCommandProc("Lum_Erp_Stp_UpdateSerialNoCompleteStatus",
                                  new SpParam("@grndetailsSerialNoId", ddlserialNo.SelectedValue, SqlDbType.Int),
                                  new SpParam("@itemId", ddlItem.SelectedValue, SqlDbType.Int),
                                  new SpParam("@storeId", int.Parse(ddlStore.SelectedValue), SqlDbType.Int),
                                  new SpParam("@inComplete", 1, SqlDbType.Bit));
                }
            }
        }
        if (flagFIRST == false)
        {
            message.Text += " false1";
            flag = true;
            for (int _index = 0; _index < ditemGrid.Items.Count; _index++)
            {

                CheckBox _chkBox = (CheckBox)ditemGrid.Items[_index].FindControl("chkStatus");
                decimal _value = 0;
                decimal txtgrndetailsSerialNoId = 0;
                TextBox _txtValue = (TextBox)ditemGrid.Items[_index].FindControl("txtValue");
                TextBox _txtgrndetailsSerialNoId = (TextBox)ditemGrid.Items[_index].FindControl("txtgrndetailsSerialNoId");
                TextBox _txtsparesId = (TextBox)ditemGrid.Items[_index].FindControl("txtsparesId");

                if (!string.IsNullOrEmpty(_txtValue.Text))
                {
                    _value = decimal.Parse(_txtValue.Text);
                }
                txtgrndetailsSerialNoId = decimal.Parse(_txtgrndetailsSerialNoId.Text);

                int itemId = int.Parse(((HtmlInputHidden)ditemGrid.Items[_index].FindControl("itemId")).Value);
                int sparesId1 = Convert.ToInt32(_txtsparesId.Text);
                if (_chkBox.Checked)
                {
                    try
                    {
                        using (IConnection conn = DataConnectionFactory.GetConnection())
                        {
                            conn.ExecuteCommandProc("Lum_Erp_Stp_UpdateSerialNoComplete",
                                    new SpParam("@grndetailsSerialNoId", _grndetailsSerialNoId, SqlDbType.Decimal),
                                //new SpParam("@itemId", ddlItem.SelectedValue, SqlDbType.Int),
                                    new SpParam("@storeId", int.Parse(ddlStore.SelectedValue), SqlDbType.Int),
                                    new SpParam("@serialNo", lblSerialNo.Text, SqlDbType.VarChar),
                                    new SpParam("@sparesId", sparesId1, SqlDbType.Int),
                                    new SpParam("@grndetailsSerialNoId_SPARE", txtgrndetailsSerialNoId, SqlDbType.Decimal));
                        }
                        message.Text = "item updated";
                        message.ForeColor = Color.Green;
                        add.Enabled = false;
                    }
                    catch (SqlException EX)
                    {
                        message.Text = EX.Message;
                        message.ForeColor = Color.Red;
                        return;
                    }
                }
                if (!_chkBox.Checked)
                {
                    flag = false;
                    try
                    {
                        using (IConnection conn = DataConnectionFactory.GetConnection())
                        {
                            conn.ExecuteCommandProc("Lum_Erp_Stp_AddnewItemOnStock", new SpParam("@itemId", ddlItem.SelectedValue, SqlDbType.Int),
                                        new SpParam("@sparesId", sparesId1, SqlDbType.Int),
                                        new SpParam("@storeId", int.Parse(ddlStore.SelectedValue), SqlDbType.Int),
                                        new SpParam("@serialNo", lblSerialNo.Text, SqlDbType.VarChar),
                                        new SpParam("@value", _value, SqlDbType.Decimal),

                                        new SpParam("@adminUserId", GetLoggedinUser().UserId, SqlDbType.Int),
                                        new SpParam("@grndetailsSerialNoId", _grndetailsSerialNoId, SqlDbType.Decimal),
                                       new SpParam("@grndetailsSerialNoId_SPARE", txtgrndetailsSerialNoId, SqlDbType.Decimal));
                        }
                        message.Text = "item updated";
                        message.ForeColor = Color.Green;
                        add.Enabled = false;
                    }
                    catch (SqlException EX)
                    {
                        message.Text = EX.Message;
                        message.ForeColor = Color.Red;
                        return;
                    }
                }

            }
            if (flag == true)
            {
                try
                {
                    using (IConnection conn = DataConnectionFactory.GetConnection())
                    {
                        conn.ExecuteCommandProc("Lum_Erp_Stp_UpdateSerialNoCompleteStatus",
                               new SpParam("@grndetailsSerialNoId", _grndetailsSerialNoId, SqlDbType.Int),
                               new SpParam("@itemId", ddlItem.SelectedValue, SqlDbType.Int),
                               new SpParam("@storeId", int.Parse(ddlStore.SelectedValue), SqlDbType.Int),
                               new SpParam("@inComplete", 0, SqlDbType.Bit));
                    }
                }
                catch (SqlException  EX)
                {
                    message.Text = EX.Message;
                    message.ForeColor = Color.Red;
                    return;
                }
            }
        }

        Response.Redirect("IncompleteItemList.aspx");
        
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ditemGrid_PreRender(object sender, EventArgs e)
    {
 
    }
    /// <summary>
    /// 
    /// </summary>
    public void BindDetachItem()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(@"select D.divisionId,S.subdivisionId,SL.storeId,I.itemId,Gs.serialNo from Lum_Erp_GrndetailsSerialNumber GS
                                                            left join  Lum_Erp_Grn G on G.grnId=GS.grnId
                                                            left join  Lum_Erp_Storelocation  SL on SL.storeId =G.storeId
                                                            inner join  Lum_Erp_item I on  I.itemId=GS.itemId 
                                                            inner join  Lum_Erp_Subdivision S on S.subdivisionId =I.subdivisionId  
                                                            inner join  Lum_Erp_Division D on D.divisionId=S.divisionId
                                                            where grndetailsSerialNoId=" + _grndetailsSerialNoId))
            {
                if (reader.Read())
                {

                    ddlDivision.SelectedValue = Convert.ToString(reader["divisionId"]);
                    ddlStore.SelectedValue = Convert.ToString(reader["storeId"]);
                    BindListControl _control = new BindListControl();
                    _control.BindSubDivision(ddlSubdivision, Convert.ToString(reader["divisionId"]));
                    ddlSubdivision.SelectedValue = Convert.ToString(reader["subdivisionId"]);
                    _control.BindItems(ddlItem, Convert.ToString(reader["subdivisionId"]));
                    ddlItem.SelectedValue = Convert.ToString(reader["itemId"]);
                    ddlStore.Enabled = false;
                    ddlDivision.Enabled = false;
                    ddlStore.Enabled = false;
                    ddlItem.Enabled = false;
                    ddlSubdivision.Enabled = false;
                    ddlserialNo.Visible = false;
                    lblSerialNo.Visible = true;
                    lblSerialNo.Text = Convert.ToString(reader["serialNo"]);
                    BindSerialNo();
                    add.CausesValidation = false;
                  }
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlItem_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlStore.SelectedIndex = 0;
        ddlserialNo.SelectedIndex = 0;
    }
}
