using System.Data;
using System;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Bitscrape.AppBlock.Database;
using System.Drawing;

public partial class EventEntry_IncompleteItem : BasePage
{
    int div = -1;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!IsPostBack)
        {
            BindGrid();
        }        
    }
    private void BindGrid()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(@"SELECT L2.divisionName,L3.subdivisionName,L1.itemCode,L4.serialNo,L1.divisionId,
                    L1.subdivisionId,L1.itemId,L4.Id
	                FROM Item L1 INNER JOIN Lum_Erp_Division L2 ON L1.divisionId=L2.divisionId
		                INNER JOIN Lum_Erp_Subdivision L3 ON L1.subdivisionId=L3.subdivisionId
		                INNER JOIN Lum_Erp_ItemSerialNo L4 ON L1.itemId=L4.itemId
	                WHERE L4.status='False'"))
            {
                gvDetach.DataSource = reader;
                gvDetach.DataBind();
            }
        }

    }
}
