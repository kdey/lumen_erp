﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bitscrape.AppBlock.Database;
using System.Drawing;

public partial class EventEntry_ChallanWithoutRespectToSoirPrint : BasePage
{
    private double totalAmount = 0;
    protected int _challanId = -1;
    protected int _soirId = -1;


    private double totalprice = 0;

    private double TotalVat = 0;
    private decimal TotalQty = 0;
   
    double Charge = 0;
    double Buyback = 0;
    private double totaldiscount = 0;
    private string buyBackDetails = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsLoggedIn())
        {
            Response.Redirect("./../Login.aspx");
        }
        if (!string.IsNullOrEmpty(Request["challanId"]))
        {
            _challanId = int.Parse(Request["challanId"]);
        }
        if (!string.IsNullOrEmpty(Request["soirId"]))
        {
            _soirId = int.Parse(Request["soirId"]);
        }
        if (ViewState["previousPage"] == null)
        {
            ViewState["previousPage"] = Convert.ToString(Request.UrlReferrer);
        }
        if (!this.IsPostBack)
        {
            try
            {
                if (_challanId > 0)
                {
                    using (IConnection conn = DataConnectionFactory.GetConnection())
                    {
                        LoadChallan();
                        BindGrid(conn, Convert.ToInt32(taxType.Value));
                    }
                }
            }
            catch (Exception ex)
            {
                Page.RegisterClientScriptBlock("message", "<script>alert('" + ex.Message + ".');</script>");
            }
        }
    }
    protected void btnCancel_ServerClick(object sender, EventArgs e)
    {
        if (ViewState["previousPage"] != null)
        {
            Response.Redirect(Convert.ToString(ViewState["previousPage"]));
        }

    }

    private void _BindGrid(IConnection conn, int taxType)
    {
        totalAmount = 0;
        try
        {
            IDataReader _reader;
            if (taxType == 1)
            {
                _reader = conn.ExecuteQueryProc("Lum_erp_Stp_GetPurchaseOrder_WithGrnDetails_vat",
                                     new SpParam("@soirId", _soirId, SqlDbType.Int),
                                     new SpParam("@CHALLANID", _challanId, SqlDbType.Int),
                                     new SpParam("@STORID", int.Parse(storeid.Value), SqlDbType.Int));
                poLineGrid.Columns[8].HeaderText = "VAT(%)";
                poLineGrid.DataSource = _reader;
                poLineGrid.DataBind();
                GetTotalQuantity();
            }
            if (taxType == 2)
            {
                _reader = conn.ExecuteQueryProc("Lum_erp_Stp_GetPurchaseOrder_WithGrnDetails_cst",
                                     new SpParam("@soirId", _soirId, SqlDbType.Int),
                                     new SpParam("@CHALLANID", _challanId, SqlDbType.Int));
                poLineGrid.Columns[8].HeaderText = "CST(%)";
                poLineGrid.DataSource = _reader;
                poLineGrid.DataBind();
                GetTotalQuantity();
            }

            if (taxType == 3)
            {
                _reader = conn.ExecuteQueryProc("Lum_erp_Stp_GetPurchaseOrder_WithGrnDetails_cstAgainstFormC",
                                     new SpParam("@soirId", _soirId, SqlDbType.Int),
                                     new SpParam("@CHALLANID", _challanId, SqlDbType.Int));
                poLineGrid.Columns[8].HeaderText = "CST Against Form(%)";
                poLineGrid.DataSource = _reader;
                poLineGrid.DataBind();
                GetTotalQuantity();
            }


        }
        catch (Exception ex)
        {
            Page.RegisterClientScriptBlock("message", "<script>alert('" + ex.Message + ".');</script>");
        }

    }
    
    private void BindGrid(IConnection conn, int taxType)
    {
        double t1 = 0;
        double t2 = 0;
        string sql = "";
        totalAmount = 0;
        IDataReader _reader;
        DataTable aTable = new DataTable();


        DataRow dtRow;
        DataColumn dtCol;

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "itemDescription";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "itd";

        aTable.Columns.Add(dtCol);
        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "SerialNo";

        aTable.Columns.Add(dtCol);
        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "qtyxrate";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "quantity";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "uomName";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "price";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "vatRate";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "vatAmount";
        aTable.Columns.Add(dtCol);
        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "discount";
        aTable.Columns.Add(dtCol);
        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "total";
        aTable.Columns.Add(dtCol);

        dtCol = new DataColumn();
        dtCol.DataType = System.Type.GetType("System.String");
        dtCol.ColumnName = "itemType";
        aTable.Columns.Add(dtCol);
        DataRow drProduct;
        //code modified for vat calculation by kasturi on 12 apr 2013 
        string _CDate = Convert.ToDateTime(CustOrderDate.Text).ToString("dd MMM yyyy");
        SpParam _spsoirId = new SpParam("@soirId", _soirId, SqlDbType.Int);
        SpParam _CaDate = new SpParam("@CDate", _CDate, SqlDbType.SmallDateTime);
        SpParam _spCHALLANID = new SpParam("@CHALLANID", _challanId, SqlDbType.Int);
        SpParam _spSTORID = new SpParam("@STORID", int.Parse(storeid.Value), SqlDbType.Int);
        SpParam _sptID = new SpParam("@tid", 0, SqlDbType.Int);
        DataSet _Ds = new DataSet();
        _Ds = conn.GetDataSet("Lum_erp_Stp_GetPurchaseOrder_WithGrnDetails_new", _spsoirId, _CaDate, _spCHALLANID, _spSTORID, _sptID);


        if (taxType == 1)
        {
            //_Ds = conn.GetDataSet("Lum_erp_Stp_GetPurchaseOrder_WithGrnDetails_vat", _spsoirId, _spCHALLANID, _spSTORID);
            poLineGrid.Columns[3].HeaderText = "VAT";
        }
        if (taxType == 2)
        {
            //_Ds = conn.GetDataSet("Lum_erp_Stp_GetPurchaseOrder_WithGrnDetails_cst", _spsoirId, _spCHALLANID, _spSTORID);
            poLineGrid.Columns[3].HeaderText = "CST";
        }
        if (taxType == 3)
        {
            //_Ds = conn.GetDataSet("Lum_erp_Stp_GetPurchaseOrder_WithGrnDetails_cstAgainstFormC", _spsoirId, _spCHALLANID, _spSTORID);
            poLineGrid.Columns[3].HeaderText = "CSTAgainstFormC";
        }
        for (int _index = 0; _index < _Ds.Tables[0].Rows.Count; _index++)
        {
            string foc = "";
            string _SerialNum = string.Empty;
            if (Convert.ToBoolean(_Ds.Tables[0].Rows[_index]["itemType"]) == true)
                foc = " (FOC)";
            drProduct = aTable.NewRow();
            drProduct["itemDescription"] = _Ds.Tables[0].Rows[_index]["itemDescription"].ToString() + foc;
            drProduct["itd"] = _Ds.Tables[0].Rows[_index]["itd"].ToString();
            drProduct["itemType"] = foc;
            string _SlrNo = _Ds.Tables[0].Rows[_index]["SerialNo"].ToString();
            string[] _SerialNo = _SlrNo.Split(';');
            for (int i = 0; i < _SerialNo.Length; i++)
            {
                if (_SerialNum == "")
                {
                    if (_SerialNo[i].ToString() != "")
                        _SerialNum = _SerialNo[i].ToString() + ",";
                }
                else
                {
                    _SerialNum = _SerialNum + "&nbsp;" + "<br>" + _SerialNo[i].ToString();
                }
            }
            drProduct["SerialNo"] = _SerialNum;
            //drProduct["SerialNo"] = _Ds.Tables[0].Rows[_index]["SerialNo"].ToString().Replace(';', ',');
            drProduct["quantity"] = _Ds.Tables[0].Rows[_index]["quantity"].ToString();
            drProduct["vatRate"] = _Ds.Tables[0].Rows[_index]["taxType"].ToString();
            drProduct["uomName"] = _Ds.Tables[0].Rows[_index]["uomName"].ToString();
            drProduct["price"] = string.Format("{0:#,##0.00}", (Convert.ToDouble(_Ds.Tables[0].Rows[_index]["price"])));
            drProduct["vatAmount"] = string.Format("{0:#,##0.00}", (Convert.ToDouble(_Ds.Tables[0].Rows[_index]["vatAmount"])));
            drProduct["discount"] = string.Format("{0:#,##0.00}", (Convert.ToDouble(_Ds.Tables[0].Rows[_index]["discount"])));
            drProduct["total"] = string.Format("{0:#,##0.00}", (Convert.ToDouble(_Ds.Tables[0].Rows[_index]["total"])));
            drProduct["qtyxrate"] = string.Format("{0:#,##0.00}", (Convert.ToDouble(_Ds.Tables[0].Rows[_index]["price"])) * Convert.ToDouble(_Ds.Tables[0].Rows[_index]["quantity"]));

            //CalcTotalprice(drProduct["price"].ToString());
            CalcTotalprice(drProduct["qtyxrate"].ToString());
            CalcTotalQty(drProduct["quantity"].ToString());
            CalcTotalVat(drProduct["vatAmount"].ToString());
            Calctotaldiscount(drProduct["discount"].ToString());
            CalcTotalAmount(drProduct["total"].ToString());



            aTable.Rows.Add(drProduct);


        }
        drProduct = aTable.NewRow();
        drProduct["itemDescription"] = "";
        drProduct["itemDescription"] = "Sub Total";
        drProduct["SerialNo"] = "";
        drProduct["quantity"] = string.Format("{0:#,##0.00}", TotalQty);
        drProduct["uomName"] = "";
        drProduct["qtyxrate"] = string.Format("{0:#,###.00}", (totalprice));
        drProduct["vatAmount"] = string.Format("{0:#,###.00}", (TotalVat));
        drProduct["discount"] = string.Format("{0:#,##0.00}", (totaldiscount));
        drProduct["total"] = string.Format("{0:#,##0.00}", (totalAmount));

        aTable.Rows.Add(drProduct);
        //discount
        if (Buyback > 0)
        {
            //drProduct = aTable.NewRow();
            //drProduct["itemDescription"] = "";
            //drProduct["itemDescription"] = "Discount: ";
            //drProduct["SerialNo"] = "";
            //drProduct["quantity"] = "";
            //drProduct["uomName"] = "";
            //drProduct["price"] = "";
            //drProduct["vatAmount"] = "";
            //drProduct["discount"] = "";
            //drProduct["total"] = string.Format("{0:#,##0.00}", (Convert.ToDouble(totaldiscount)));

            //aTable.Rows.Add(drProduct);

            //CalcTotalAmountminus(totaldiscount.ToString());

            //drProduct = aTable.NewRow();
            //drProduct["itemDescription"] = "";
            //drProduct["itemDescription"] = "Sub Total";
            //drProduct["SerialNo"] = "";
            //drProduct["total"] = string.Format("{0:#,##0.00}", (totalAmount));


        }
        //drProduct = aTable.NewRow();
        //drProduct["itemDescription"] = "";
        //drProduct["itemDescription"] = "Tax @ 12.5%";
        //drProduct["SerialNo"] = "";
        //drProduct["quantity"] = "";
        //drProduct["uomName"] = "";
        //drProduct["price"] = "";
        //drProduct["vatAmount"] = "";
        //drProduct["discount"] = "";
        //drProduct["total"] = string.Format("{0:#,##0.00}", (Convert.ToDouble(TotalVat)));

        //aTable.Rows.Add(drProduct);
        //CalcTotalAmount(TotalVat.ToString());

        if (Charge > 0)
        {
            drProduct = aTable.NewRow();
            drProduct["itemDescription"] = "";
            drProduct["itemDescription"] = "Packing & Forwarding";
            drProduct["SerialNo"] = "";
            drProduct["quantity"] = "";
            drProduct["uomName"] = "";
            drProduct["price"] = "";
            drProduct["vatAmount"] = "";
            drProduct["discount"] = "";
            drProduct["total"] = string.Format("{0:#,##0.00}", (Convert.ToDouble(Charge)));



            aTable.Rows.Add(drProduct);
            CalcTotalAmount(Charge.ToString());
        }
        if (Buyback > 0)
        {
            drProduct = aTable.NewRow();
            drProduct["itemDescription"] = "";
            drProduct["itemDescription"] = "Buyback Details: " + buyBackDetails;
            drProduct["SerialNo"] = "";
            drProduct["quantity"] = "";
            drProduct["uomName"] = "";
            drProduct["price"] = "";
            drProduct["vatAmount"] = "";
            drProduct["discount"] = "";
            drProduct["total"] = string.Format("{0:#,##0.00}", (Convert.ToDouble(Buyback)));

            aTable.Rows.Add(drProduct);
            CalcTotalAmountminus(Buyback.ToString());
        }



        t1 = Convert.ToDouble(totalAmount);
        t2 = Math.Round(totalAmount);

        if (t2 > t1)
        {
            drProduct = aTable.NewRow();
            drProduct["itemDescription"] = "";
            drProduct["itemDescription"] = "Round Off ";
            drProduct["SerialNo"] = "";
            drProduct["quantity"] = "";
            drProduct["uomName"] = "";
            drProduct["price"] = "";
            drProduct["vatAmount"] = "";
            drProduct["discount"] = "";
            drProduct["total"] = "(+)" + string.Format("{0:#,##0.00}", (t2 - t1));
            aTable.Rows.Add(drProduct);
        }
        if (t2 < t1)
        {
            drProduct = aTable.NewRow();
            drProduct["itemDescription"] = "";
            drProduct["itemDescription"] = "Round Off ";
            drProduct["SerialNo"] = "";
            drProduct["quantity"] = "";
            drProduct["uomName"] = "";
            drProduct["price"] = "";
            drProduct["vatAmount"] = "";
            drProduct["discount"] = "";
            drProduct["total"] = "(-)" + string.Format("{0:#,##0.00}", (t1 - t2));
            aTable.Rows.Add(drProduct);
        }

        drProduct = aTable.NewRow();
        drProduct["itemDescription"] = "";
        drProduct["itemDescription"] = "Total";
        drProduct["SerialNo"] = "";
        drProduct["quantity"] = "";// string.Format("{0:#,###}", TotalQty);
        drProduct["uomName"] = "";
        drProduct["price"] = "";// string.Format("{0:#,###.00}", (totalprice));
        drProduct["vatAmount"] = "";// string.Format("{0:#,###.00}", (TotalVat));
        drProduct["discount"] = "";// string.Format("{0:#,###.00}", (totaldiscount));
        drProduct["total"] = string.Format("{0:#,##0.00}", (Math.Round(totalAmount)));

        aTable.Rows.Add(drProduct);


        poLineGrid.DataSource = aTable;
        poLineGrid.DataBind();


        //lblTotalintext.Text = changeCurrencyToWords(Math.Round(totalAmount));
    }
    private void CalcTotalprice(string _netVal)
    {
        double ta = 0;
        if (double.TryParse(_netVal, out ta))
        {
            totalprice += Double.Parse(_netVal);
        }

    }

    private void CalcTotalAmount(string _netVal)
    {
        double ta = 0;
        if (double.TryParse(_netVal, out ta))
        {
            totalAmount += Double.Parse(_netVal);
        }

    }

    private void CalcTotalQty(string _netVal)
    {
        double ta = 0;
        if (double.TryParse(_netVal, out ta))
        {
            TotalQty += Convert.ToDecimal(_netVal);
        }

    }
    private void CalcTotalVat(string _netVal)
    {
        double ta = 0;
        if (double.TryParse(_netVal, out ta))
        {
            TotalVat += Double.Parse(_netVal);
        }

    }
    private void Calctotaldiscount(string _netVal)
    {
        double ta = 0;
        if (double.TryParse(_netVal, out ta))
        {
            totaldiscount += Double.Parse(_netVal);
        }

    }
    private void CalcTotalAmountminus(string _netVal)
    {
        double ta = 0;
        if (double.TryParse(_netVal, out ta))
        {
            totalAmount -= Double.Parse(_netVal);
        }

    }
    public void LoadChallan()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(@"select C.*,T.transporterName,S.strSoirId ,CONVERT(VARCHAR,ci.activityDateTime,106) AS challandate,activityLogId
                     from Lum_Erp_ChallancumInvoice C,dbo.Lum_Erp_ChallanActivityLog ci ,Lum_Erp_SOIR S,Lum_Erp_Transporter T
                     WHERE S.soirId=C.soirId and C.transporterId=T.transporterId and C.Challanid = ci.challanid and ci.activityTypeId=1 AND C.ChallanId=" + _challanId))
            {
                if (reader.Read())
                {
                    if (reader["storeId"] != Convert.DBNull)
                    {
                        storeid.Value = Convert.ToString(reader["storeId"]);
                    }
                    taxType.Value = Convert.ToString(reader["taxTypeId"]);
                    _soirId = Convert.ToInt32(reader["soirId"]);
                    soirdt.Text = (reader["challandate"]).ToString();
                    lblChallanNo.Text = (reader["strChallanId"]).ToString();
                    lblCNDetails.Text = (reader["CNDetails"]).ToString();
                    DespatchThrough.Text = (reader["transporterName"]).ToString();
                    reader.Close();
                    LoadSoir();

                }
            }

        }
    }
    public void LoadSoir()
    {
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(@"select s.*,c.*,Convert(VARCHAR(11),poDate,106) as CustDate,ch.challanId from Lum_Erp_Soir s,Lum_Erp_Customer c,Lum_Erp_ChallancumInvoice ch WHERE c.customerId=s.customerId and 
                                                            s.soirId = ch.soirId and ch.challanId=" + _challanId))
            {
                if (reader.Read())
                {

                    CustomerName.Text = Convert.ToString(reader["customerName"]);
                    CustomerAddress.Text = Convert.ToString(reader["Billing_address1"]) + " , " + Convert.ToString(reader["Billing_address2"]) + " , " + Convert.ToString(reader["Billing_zipCode"]);
                    CosigneeName.Text = Convert.ToString(reader["customerName"]);
                    CosigneeAddress.Text = Convert.ToString(reader["Shipping_address1"]) + " , " + Convert.ToString(reader["Shipping_address2"]) + " , " + Convert.ToString(reader["Shipping_zipCode"]);
                    //ContactPersonWithPhoneNo.Text = Convert.ToString(reader["customerName"]) + " , " + Convert.ToString(reader["Billing_phoneNo"]);
                    //ContactPersonwithPhCosignee.Text = Convert.ToString(reader["customerName"]) + " , " + Convert.ToString(reader["Shipping_phoneNo"]);
                    CustOrderNo.Text = Convert.ToString(reader["customerPoNo"]);
                    CustOrderDate.Text = Convert.ToString(reader["CustDate"]);

                }
            }

        }
    }
    public string GetTotalQuantity()
    {
        string retVal = "";
        using (IConnection conn = DataConnectionFactory.GetConnection())
        {
            using (IDataReader reader = conn.ExecuteQuery(@"select Sum(quantity) as TotalQty from Lum_Erp_SoirItemDetails WHERE  soirId=" + _soirId))
            {
                if (reader.Read())
                {
                    lblTotalQty.Text = Convert.ToString(reader["TotalQty"]);
                }
            }
        }
        return retVal;
    }
}
